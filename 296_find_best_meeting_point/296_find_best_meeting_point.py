"""
Inputs
    grid (List[List[int]]): locations of peoples homes
Outputs
    int: minimum travel distance for group
Notes
    - input is all zeros or ones
        - 1: marks the location of a persons home
    - distances are calculated use manhattan distance
    - find the minimum distance 

Ideas
    - compute the manhattan distance for everyone
        + compute a full grid of manhattan distances
        + sum the distances for each person
        + save the minimum total sum

    - similar to finding the minimum average
    - does finding the mean location help narrow things down
        + we know the optimal location will likely be the middle of the points
        + if points are highly weighted to one side, the meeting point
        will probably be close to that side

    + use a median since this more closely aligns with the midpoint of normal
    distribution

        + the mean minimizes the sum of squared deviations
        + the median minimizes the sum of absolute deviations

        https://en.wikipedia.org/wiki/Geometric_median
        https://www.quora.com/Why-does-the-median-minimize-the-sum-of-absolute-deviations?share=1

        + median for odd numbered elements is middle
        + median for even numbered is average.. 
        
        here we can't pick the average of the two so 
        we must pick one or the other
"""

class Solution:
    def minTotalDistance(self, grid: List[List[int]]) -> int:
        """Compute minimum total manhattan distance

        Time Complexity
            O(n*m*p)        
        Space Complexity
            O(p)
        """

        n_rows: int = len(grid)
        n_cols: int = len(grid[0])

        # Find the locations of every person
        people = [(r,c) for r in range(n_rows) for c in range(n_cols) if grid[r][c] == 1]

        min_dist: int = float('inf')

        # Compute the total manhattan distance for every location on the grid
        # for every person, and sum their contribution
        for r in range(n_rows):
            for c in range(n_cols):

                total_dist = 0

                for r2, c2 in people:

                    total_dist += abs(r2 - r) + abs(c2 - c)

                min_dist = min(min_dist, total_dist)

        return min_dist

    ''' Median Method '''

    def median(self, grid: List[List[int]]) -> int:
        """Compute minimum total manhattan distance

        - find where everyone lives
        - calculate the 1D median in both directions
        - compute the total manhattan distance to the median

        Time Complexity
            O(n*m)        
        Space Complexity
            O(p)

        Notes
            - we can gather the points in a sorted order, to prevent
            the need for sorting
        References:
            
        """

        n_rows: int = len(grid)
        n_cols: int = len(grid[0])

        # Find the locations of every person
        people = [(r,c) for r in range(n_rows) for c in range(n_cols) if grid[r][c] == 1]

        # Calculate the median location in x & y
        rows_srt = sorted([r for r,c in people])
        cols_srt = sorted([c for r,c in people])

        n_ppl = len(people)

        # [0, 1, 2, 3, 4]
        # [0, 1, 2, 3, 4, 5]
        r_med = rows_srt[n_ppl // 2 ]
        c_med = cols_srt[n_ppl // 2 ]

        # Compute the total manhattan distance for every person
        total_dist = 0
        for r,c in people:
            total_dist += abs(r_med - r) + abs(c_med - c)

        return total_dist

