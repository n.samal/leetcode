"""
Inputs
    S (str): main string
    T (str): subsequence pattern
Outputs:
    str: minimum contiguous string containing the subsequence pattern

Goals
    - find the smallest substring in S where we could
    make the subsequence T
    - if multiple substrings are tied for the minimum, return
    the substring positioned furthest to the left
    - if the pattern is not possible return an empty string

Examples
    Input:
    S = "abcdebdde", T = "bde"
    Output: "bcde"
    Explanation:
    "bcde" is the answer because it occurs before "bdde" which has the same length.
    "deb" is not a smaller window because the elements of T in the window must occur in order. 

Ideas

    - this looks like an extension to 76_minimum_window_substring

    - track the indices where each character occurs
        ie: {'a': [0], 'b': [0, 5], ..}

    - we need to use all the characters in T
        + crawl down the character occurence 'path'

        from the first location of b, find the first location
        where 'd' occurs after our current location

        after a valid d is found, find the position of the first
        valid e

    - we know the substring can only occur once we've seen all the characters

        ie: 'zzzzzzzzzzzzzzbde'

        this however doesn't tell us about the order

        ie: 'edb'

        we saw all the characters but in an order that's not helpful

    - creating a sliding window style approach with pointers

Strategies


References:
    https://docs.python.org/3/library/bisect.html
    https://leetcode.com/problems/minimum-window-subsequence/discuss/512645/Easy-To-Understand-%3A-Sliding-window-2-pointer-Find-then-Improve
    https://leetcode.com/problems/minimum-window-subsequence/discuss/109362/Java-Super-Easy-DP-Solution-(O(mn))
"""

from collections import defaultdict
import bisect


class Solution:

    def reduceWindow(self, S: str, T: str, s_ix: int, t_ix: int) -> int:
        """Reduce the window size

        - use pointers to track the character to match
        - reduce the window size
        - apply the same logic but backwards

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """
        # Bring pointers back within string
        s_ix -= 1
        t_ix -= 1

        while t_ix >= 0:
            # print(f' s:{S[s_ix]}  t:{T[t_ix]}')
            if S[s_ix] == T[t_ix]:
                s_ix -= 1
                t_ix -= 1
            else:
                s_ix -= 1

        # Bring back within bounds
        return s_ix + 1

    def minWindow(self, S: str, T: str) -> str:
        """Two pointers

        - compare characters and bump pointers until we match the entire subsequence
        - once the whole subsequence is met, squeeze the window while valid
        - start the search after our previous best location

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        # Pointers in each string
        s_ix: int = 0
        t_ix: int = 0
        start_ix: int = -1

        # Best answers so far, and their location
        min_len = float('inf')
        min_ind = None

        while s_ix < len(S):

            # print(f's:{S[s_ix]}  t:{T[t_ix]}')

            if S[s_ix] == T[t_ix]:
                s_ix += 1
                t_ix += 1

                # Subsequence match complete
                if t_ix == len(T):
                    start_ix = self.reduceWindow(S, T, s_ix, t_ix)
                    cur_len = s_ix - start_ix

                    if cur_len < min_len:
                        min_len = cur_len
                        min_ind = (start_ix, s_ix)

                    # Start search at next position
                    s_ix = start_ix + 1
                    t_ix = 0

            else:
                s_ix += 1

        if min_ind:
            return S[min_ind[0]:min_ind[1]]
        else:
            return ''


class SolutionTree:

    def minWindow(self, S: str, T: str) -> str:
        """

        - track where each relevant T character occurs in S
        - start crawling valid paths

        Args:
            S (str): string
            T (str): subsequence to match
        Returns:

        """
        # Best answers so far, and their location
        self.min_len = float('inf')
        self.min_ind = None  # indices for minimum substring
        self.T = T

        # Only save the locations of relevant characters
        # ie: we only care about where b,d,e are located
        self.char_loc = defaultdict(list)
        chars_t = set(T)

        for ix, char in enumerate(S):
            if char in chars_t:
                self.char_loc[char].append(ix)      

        # Crawl potential paths
        self.recurse()

        if self.min_ind:
            return S[self.min_ind[0]:self.min_ind[1]]
        else:
            return ''

    def recurse(self, t: int = 0, start_ix: int = None, s: int = -1):
        """Crawl potential paths

        - stop crawling if our path is longer than our current min
        - only consider character indices forward of the current position

        Args:
            t (int): character index in T string
            start_ix (int): starting location of potential substring
            s (int): character index in S string
        Notes:
            too slow
        """

        # We've matched the whole subsequence
        # now check if it's a better guess
        if t == len(self.T):
            if s + 1 - start_ix < self.min_len:
                self.min_len = s + 1 - start_ix
                self.min_ind = (start_ix, s + 1)

                # substr = self.S[start_ix:s + 1]
                # print(f' S: {substr}')
            return

        # Our length is already too long
        if start_ix and s + 1 - start_ix > self.min_len:
            return

        # Find locations of the next character in T
        # only location at values bigger than the current index
        options = self.char_loc[self.T[t]]
        option_loc = bisect.bisect(options, s)

        if option_loc >= 0:

            # Try each location of the character
            for ix in options[option_loc:]:

                # Save our starting index
                if t == 0:
                    self.recurse(t + 1, ix, ix)

                # Only use locations where the character match location is further
                # than our current position
                elif ix > s:
                    self.recurse(t + 1, start_ix, ix)


if __name__ == '__main__':

    # Example 1
    # S = "abcdebdde"

    # Failed case
    S = "cnhczmccqouqadqtmjjzl"
    T = "mm"

    # Out of order
    # S = "eddabbbdebdde"
    # T = "bde"

    obj = Solution()
    print(obj.minWindow(S, T))
