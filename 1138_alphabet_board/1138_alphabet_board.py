"""
Inputs
    target (str): target string
Output
    str: commands to type of the target string
Notes

    - board is defined as 

        abcde
        fghij
        klmno
        pqrst
        uvwxy
        z

    - notice that z is in a row by itself

    - commands must be provided to interface
        + 'U' Up
        + 'D' Down
        + 'L' Left
        + 'R' Right
        + '!' Enter current character

    - we start at the 0 0 position in the board
"""

class Solution:
    def alphabetBoardPath(self, target: str) -> str:
        """

        - create a grid to map the characters to a row and column position
        - for each character in the grid compute the delta in location
        required for movement
            + add those commands to our command string

        - for the 'z' case we do some prework
            + when moving to z, move the left most column first
            + when moving from z, move up first

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        grid = [['a', 'b', 'c', 'd', 'e'], ['f', 'g', 'h', 'i', 'j'],
                ['k', 'l', 'm', 'n', 'o'], ['p', 'q', 'r', 's', 't'],
                 ['u', 'v', 'w', 'x', 'y'], ['z', None, None, None, None]]

        n_rows: int = len(grid)
        n_cols: int = len(grid[0])
        dct = {}

        # Store location in the grid
        for r in range(n_rows):
            for c in range(n_cols):

                dct[grid[r][c]] = (r, c)

        # Starting location
        r = 0
        c = 0

        instructions = []

        prev_char = None

        for char in target:

            # Special case for z
            # Move to the left most column first
            if char == 'z':
                dc = 0 - c

                if dc > 0:
                    instructions.append(dc * 'R')
                elif dc < 0:
                    instructions.append(abs(dc) * 'L')

                c = 0

            # Get out of the block
            if prev_char == 'z' and char != 'z':
                instructions.append('U')
                r -= 1

            r_nxt, c_nxt = dct[char]

            dr = r_nxt - r
            dc = c_nxt - c

            if dr > 0:
                instructions.append(dr * 'D')
            elif dr < 0:
                instructions.append(abs(dr) * 'U')

            if dc > 0:
                instructions.append(dc * 'R')
            elif dc < 0:
                instructions.append(abs(dc) * 'L')

            instructions.append('!')

            # Save next location
            r = r_nxt
            c = c_nxt
            prev_char = char

        return ''.join(instructions)
