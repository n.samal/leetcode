from typing import List


def binary_search_og(arr: list, target: int, low: int, high: int, closest: bool=False):
    """Find the indice of the target value in the array

    Time Complexity:
        O(log(n))
    Space Complexity
        O(1)

    Args:
        arr (List[int]): sorted array of values
        target (int): value to search for
        low (int): lower index of search range (inclusive)
        high (int): upper index of search range (inclusive)
        closest (bool): find the closest index

    Returns:
      int: indice of the target value
    """

    while low <= high:

        mid = (low + high) // 2

        print('low:', low, 'high:', high, 'mid:', mid)

        # Found our value
        if arr[mid] == target:
            return mid

        # Value too small
        # bump start point up
        elif arr[mid] < target:
            low = mid + 1

        # Value too large
        # bump mid point down
        else:
            high = mid - 1

    if closest:
        return low
    else:
        return -1


def binary_search(arr: list, target: int):
    """Find the indice of the target value in the array

    Time Complexity:
        O(log(n))
    Space Complexity
        O(1)

    Args:
        arr (List[int]): sorted array of values
        target (int): value to search for
    Returns:
      int: indice of the target value
    """
    n = len(arr)

    # Use binary method to find closest value
    return binary_search_og(arr, target, 0, n - 1, closest=True)


if __name__ == '__main__':

    # Test case 1
    # arr_in = [2, 2]
    # arr_in = [5, 7, 7, 8, 8, 10]
    # arr_in = [6, 6, 6, 6, 6, 6]
    arr_in = [6, 6, 6, 6, 6, 6]

    n = len(arr_in)

    # Binary search
    # print(binary_search_og([1, 3, 5, 6], 5, 0, n - 1, closest=True))
    print(binary_search_og([1, 3, 5, 6], 2, 0, n - 1, closest=True))
