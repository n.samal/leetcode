from typing import List


def binary_search(arr: list, target: int, low: int, high: int, closest: bool=False):
    """Find the indice of the target value in the array

    Time Complexity:
        O(log(n))
    Space Complexity
        O(1)

    Args:
        arr (List[int]): sorted array of values
        target (int): value to search for
        low (int): lower index of search range (inclusive)
        high (int): upper index of search range (inclusive)
        closest (bool): find the closest index

    Returns:
      int: indice of the target value
    """

    while low <= high:

        mid = (low + high) // 2

        print('low:', low, 'high:', high, 'mid:', mid)

        # Found our value
        if arr[mid] == target:
            return mid

        # Value too small
        # bump start point up
        elif arr[mid] < target:
            low = mid + 1

        # Value too large
        # bump mid point down
        else:
            high = mid - 1

    if closest:
        return low
    else:
        return -1


def two_sum(arr: list, target: int):
    """Find the indices that sum to the target value

    For every value find the pair that would contribute to a sum

    Time Complexity:
        O(n*log(n))
    Space Complexity
        O(1)

    Args:
        arr (list): array of sorder integers
        target (int): target summation
    Returns:
     List[int, int] = first and last indices with target value

    Notes:
        hits time limit on larger arrays
    """

    n: int = len(arr)

    for ix in range(n):

        # Calculate search target
        # sum = arr[ix] + x
        # x = sum - arr[ix]

        # Find target value
        ix_target = binary_search(arr, target - arr[ix], low=0, high=n - 1)

        # Found a valid pair
        if ix_target != -1 and ix_target != ix:
            return sorted([ix, ix_target])

    return [-1, -1]


def two_pointers(arr: list, target: int):
    """Travel along array using two pointers

    Time Complexity:
        O(n)
    Space Complexity
        O(1)

    Args:
        arr (list): array of sorder integers
        target (int): target summation
    Returns:
     List[int, int] = first and last indices with target value
    """

    n: int = len(arr)

    # Pointer initialization
    ix: int = 0
    jx: int = n - 1

    while ix < jx:

        # Current summation
        sum_i = arr[ix] + arr[jx]

        if sum_i == target:
            return sorted([ix + 1, jx + 1])

        # Too small
        # bump lower pointer
        elif sum_i < target:
            ix += 1
        else:
            jx -= 1

    return [-1, -1]


if __name__ == '__main__':

    # Test case 1
    # arr_in = [2, 7, 11, 15]
    # arr_in = [2, 7, 11, 15]
    arr_in = [2, 7, 11, 15, 16, 17, 18]

    print(two_pointers(arr_in, 9))
    # print(two_pointers(arr_in, 16))
