
def naive(arr: list, target: int) -> list:
    """Find the quadruplets whose sum is equal to the target

    Time Complexity
        O(n^4)
    Space Complexity
        O(1)

    Args:
        arr (list): array of integers in unknown order
        target (int): target value of summation
    Returns:
        list: quadruplets that meet the sum
    References:
        https://leetcode.com/problems/4sum/
    """

    # Init vars
    n: int = len(arr)
    combos: list = []

    for ix in range(0, n - 3):
        for jx in range(ix + 1, n - 2):
            for kx in range(jx + 1, n - 1):
                for lx in range(kx + 1, n):

                    # print(ix, jx, kx, lx)

                    # Compute current sum
                    sum_i = arr[ix] + arr[jx] + arr[kx] + arr[lx]

                    # Sum meets our target
                    if sum_i == target:
                        combos.append([arr[ix], arr[jx], arr[kx], arr[lx]])

    return combos


def pointers(arr: list, target: int) -> list:
    """Find the quadruplets whose sum is equal to the target

    Time Complexity
        O(n^3)
    Space Complexity
        O(1)

    Args:
        arr (list): array of integers in unknown order
        target (int): target value of summation
    Returns:
        list: quadruplets that meet the sum
    References:
        https://leetcode.com/problems/4sum/
    Todo:
        save time by not exploring quadruplets where the target is too large
    """

    n: int = len(arr)
    # combos: list = []
    combos = set()

    # Sort the array
    arr = sorted(arr)

    for ix in range(0, n - 3):
        for jx in range(ix + 1, n - 2):

            # Init pointers
            left: int = jx + 1
            right: int = n - 1

            while left < right:

                # Compute current sum
                sum_i = arr[ix] + arr[jx] + arr[left] + arr[right]

                # We found the sum
                if sum_i == target:

                    vals = [arr[ix], arr[jx], arr[left], arr[right]]
                    vals = tuple(sorted(vals))

                    # If a unique quadruplet
                    if vals not in combos:

                        # Save it
                        combos.add(vals)

                    left += 1

                # Too large
                elif sum_i > target:
                    right -= 1

                # Too small
                else:
                    left += 1

    # Convert back to a list
    combos = [list(item) for item in combos]

    return combos


if __name__ == '__main__':

    # arr_in = [1, 0, -1, 0, -2, 2]
    arr_in = [1,-2,-5,-4,-3,3,3,5]

    print(naive(arr_in, -11))
    print(pointers(arr_in, -11))
