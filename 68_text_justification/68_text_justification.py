"""
Inputs

Outputs
    words (List(str)): list of string
    maxWidth (int): each line has exactly this many characters

Goals

    - each line
        + has exactly the specified character limit
        + fully justified (left and right justified)

            * pack extra spaces in between words

    - last line
        + left justified
        + no extra spaces between words

Cases

- Space left: No previous word
    + initialize a new row with the word
- Space left: previous word
    + add the word to our list
    + make sure we have space for the word + a single space
- No space left:
    + must start a new row
    + add the extra space to the current row, then creat a new one

Strategy

    - Manual

        Fully Justify
        + initialize characters left = max_width
        + try adding the next word

            + if we can add the word without going beyond our limit, add it and reduce character count
            + if there's a previous word in the line we must have a space inbetween
            + if we will go beyond our limit, determine the number of remaining characters left to use

                * spread the remaining characters between the words used, iterate from left to right

        Justify Row
        - create separate function to distribute spaces amongest the words
        - no extra spaces: use a single space between words
        - extra spaces, single word: add all spaces after
        - extra spaces, multi word:
            + extra spaces per space = extra_space // n_words
            + left_overs = extra_space % n_words
            + we sprinkle the left over spaces from left to right

Example

    words = ["This", "is", "an", "example", "of", "text", "justification."]

    limit = 16

    This is an
    example of text
    justification.

    What must be
    acknowledgement
    shall be

"""

from typing import List


class Solution:

    def justify_row(self, row: List[str], chars_left: int) -> str:
        """Distribute spaces amongest characters left

        - if left over spaces after even distribution,
        then sprinkle them from beginning to end

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        Args:
            row: words remaining
            chars_left: characters left to use
        Returns:
            string with spaces distributed
        """

        # Number of spaces
        n_words: int = len(row)
        n_spaces: int = n_words - 1

        # Singular word
        # ie: ['this']
        if n_spaces == 0:
            s = row[0] + ' ' * chars_left

        # Multiword
        # ie: ['this', 'that', 'what']
        else:
            extra_spaces: int = chars_left // n_spaces  # for all spaces
            spaces_left: int = chars_left % n_spaces  # left overs

            s = ""

            for ix, word in enumerate(row):

                # Add just the word
                if ix == 0:
                    s += word

                # Add original space + extra
                else:

                    spaces = (1 + extra_spaces)

                    if spaces_left:
                        spaces += 1
                        spaces_left -= 1

                    s += " " * spaces + word

        return s

    def fullJustify(self, words: List[str], maxWidth: int) -> List[str]:
        """Fully justify the characters in the paragraph

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        TODO:
            - can improve space complexity by only tracking indices
            required for row justification left_ix, right_ix

        Args:
            words: words in the paragraph
            maxWidth: characters required per row
        Returns:
            sentences distributed per row
        """

        # Init
        chars_left: int = maxWidth
        rows = []
        row_cur = []

        for word in words:

            n: int = len(word)

            # No space left in row
            if chars_left == 0:

                # Clean the row and add it
                s = self.justify_row(row_cur, chars_left)
                rows.append(s)

                # Start a new row
                row_cur = []
                chars_left = maxWidth

            # Brand new line, just add the word
            if chars_left == maxWidth:
                row_cur.append(word)
                chars_left -= n

            # We have previous words and space for the word
            elif chars_left - (n + 1) >= 0:

                # Add the word add a space
                row_cur.append(word)
                chars_left -= n + 1

            # We have previous words and not enough space
            else:

                # Clean the row and add it
                s = self.justify_row(row_cur, chars_left)
                rows.append(s)

                # Start a new row
                row_cur = [word]
                chars_left = maxWidth - n

        # If we have a remaining row
        if row_cur:

            # Create a string
            s = " ".join(row_cur)

            # Add remaining characters
            s += " " * chars_left

            # Add to rows
            rows.append(s)

        return rows


if __name__ == "__main__":

    # Example 1
    # words = [
    #     "Science",
    #     "is",
    #     "what",
    #     "we",
    #     "understand",
    #     "well",
    #     "enough",
    #     "to",
    #     "explain",
    #     "to",
    #     "a",
    #     "computer.",
    #     "Art",
    #     "is",
    #     "everything",
    #     "else",
    #     "we",
    #     "do",
    # ]

    # maxWidth = 20

    words = ["What","must","be","acknowledgment","shall","be"]
    maxWidth = 16

    obj = Solution()
    print(obj.fullJustify(words, maxWidth))

