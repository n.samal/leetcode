"""
Inputs:
    ratings (List[int]): ratings for each person
Outputs:
    int: number of teams that can be formed

Notes
    - we must choose 3 soldiers in order i < j < k
    - teams can be created in two orders
        + increasing rating[i] < rating[k] < rating[k]
        + decreasing rating[i] > rating[k] > rating[k]

    - persons can be part of multiple teams

Examples

    Example 1:

        Input: rating = [2,5,3,4,1]
        Output: 3
        Explanation: We can form three teams given the conditions. (2,3,4), (5,4,1), (5,3,1). 

    Example 2:

        Input: rating = [2,1,3]
        Output: 0
        Explanation: We can't form any team given the conditions.

    Example 3:

        Input: rating = [1,2,3,4]
        Output: 4

Ideas

    - brute force solution: O(n^3) too slow

    - count number of smaller values to left and right 
        + use brute force  O(n^2)
        + use merge sort O(n*log(n))

    - use those players to combine and create teams

        lesser rating to left + current player + greater ratings to right (increasing)
        greater rating to left + current player + lesser ratings to right (decreasing)

    - do we need to worry about double counts?

        [1, 5, 8, 9, 11]

         [1],    5, [8, 9, 11]

         [1, 5], 8, [9, 11]

         doesn't appear so since prior lefts are only consider once

"""

class Solution:
    def numTeams(self, arr: List[int]) -> int:
        """Find all teams that are monotonically increasing or decreasing

        Time Complexity
            O(n^2)
        Space Complexity
            O(1)
        """
        count: int = 0
        n: int = len(arr)

        # For each value count the number of smaller values to the left
        # count number of larger values to the right

        # Combine current person with ppl to left and right
        for i in range(1, n - 1):

            # Get counts of smaller and larger elements
            smaller_l: int = 0
            smaller_r: int = 0
            larger_l: int = 0
            larger_r: int = 0

            # Elements to left
            for j in range(0, i):

                if arr[j] > arr[i]:
                    larger_l += 1
                elif arr[j] < arr[i]:
                    smaller_l += 1

            # Elements to the right
            for j in range(i + 1, n):

                if arr[j] > arr[i]:
                    larger_r += 1
                elif arr[j] < arr[i]:
                    smaller_r += 1

            # Create teams with current player and players to the
            # left and right
            count_inc = (smaller_l * larger_r)
            count_dec = (larger_l * smaller_r)
            count_here =  count_inc + count_dec

            # print(f'count_inc: {count_inc}  count_dec: {count_dec}  count_here: {count_here}')

            count += count_here

        return count

    def brute_force(self, arr: List[int]) -> int:
        """Brute force"""
        # find all teams
        # teams must be monotonically increasing or decreasing

        count: int = 0

        n: int = len(arr)

        for i in range(n - 2):
            for j in range(i + 1, n - 1):
                for k in range(j + 1, n):

                    if arr[i] < arr[j] < arr[k] or arr[i] > arr[j] > arr[k]:
                        count += 1

        return count
