"""
Inputs
    s (str): parentheses
Outputs
    int: minimum number of insertions for balance

Notes
    - Any left parenthesis '(' must have a corresponding two consecutive right parenthesis '))'
    - Left parenthesis '(' must go before the corresponding two consecutive right parenthesis '))'.
    - For example, "())", "())(())))" and "(())())))" are balanced, ")()", "()))" and "(()))" are not balanced.

Examples 
    
    Example 1:

        Input: s = "(()))"
        Output: 1
        Explanation: The second '(' has two matching '))', but the first '(' has only ')' matching. We need to to add one more ')' at the end of the string to be "(())))" which is balanced.

    Example 2:

        Input: s = "())"
        Output: 0
        Explanation: The string is already balanced.

    Example 3:

        Input: s = "))())("
        Output: 3
        Explanation: Add '(' to match the first '))', Add '))' to match the last '('.
    
    Example 4:

        Input: s = "(((((("
        Output: 12
        Explanation: Add 12 ')' to balance the string.
    
    Example 5:

        Input: s = ")))))))"
        Output: 5
        Explanation: Add 4 '(' at the beginning of the string and one ')' at the end. The string becomes "(((())))))))".

Ideas
    - stack to balance out brackets
    - balance ratio is two to 1
        + open bracket is 2
        + close bracket is -1

        ( ))

    - iterate once to remove all balanced brackets
        + then iterate again to determine brackets
        need for balance

    - for every ( we need 2 brackets
        + to the right ))
    - for every two )) we need 1 bracket
        + ( ))
    - for every one ) we need 2 brackets
        + ( ))
        + one to left and one to right

"""


class Solution:

    def stack(self, s: str) -> int:
        """

        - use stack to balance brackets
        - track the number of unbalanced brackets
        then compute insertions required to balance them

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        Notes
            - this does not account for the bracket locations and
            therefore will not work
        """

        stack = []

        # Count the number of each brackets
        opens: int = 0
        closeds: int = 0

        for char in s:

            # Balance found '())'
            if char == ')' and len(stack) >= 2 and stack[-2] == '(' and stack[-1] == ')':
                stack.pop()
                stack.pop()

                opens -= 1
                closeds -= 1

            else:
                stack.append(char)

                if char == '(':
                    opens += 1
                else:
                    closeds += 1

        ''' Check insertions necessary '''

        count: int = 0

        # (), just add a single bracket )
        while opens and closeds:
            opens -= 1
            closeds -= 1
            count += 1

        # 2 closed for every open
        count += opens * 2

        div, mod = divmod(closeds, 2)

        # 1 open for every 2 closed
        count += div

        # 1 open, 1 closed for each single )
        count += mod * 2

        return count

    def counter(self, s: str) -> int:
        """

        - track the insertions necessary as we go
        - if we see ))
            + try to close it
        - if we see )
            + we can either add ) then close it
            + or add ( & ) to close it

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        s = s.replace('))', '}')

        opens: int = 0
        count: int = 0

        for char in s:

            if char == '(':
                opens += 1
            else:

                # Close our )) with an open ( if possible
                # or add a ( bracket to balance it
                if char == '}':
                    if opens:
                        opens -= 1
                    else:
                        count += 1

                # We need another ) for balance
                # ) -> ))
                else:
                    count += 1

                    if opens:
                        opens -= 1

                    # Add another bracket to close this
                    # )) -> ( ))
                    else:
                        count += 1

        # Close any remaining open brackets
        # two ) for each (
        return count + 2 * opens


if __name__ == "__main__":

    # Example 1
    # s = "(()))"

    # Example 2
    # s = "())"

    # Example 3
    # s = "))())("

    # Example 4
    # s = "(((((("

    # Example 5
    # s = ")))))))"

    # Expecting 4
    s = "(()))(()))()())))"

    obj = Solution()
    # print(obj.stack(s))
    print(obj.counter(s))
