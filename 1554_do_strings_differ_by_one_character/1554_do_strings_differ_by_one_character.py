"""
Inputs
    words (List[str]): list of same length words
Outputs
    bool: does there exist 2 strings that differ by one character at the same
    index
Examples

    Example 1
        Input: dict = ["abcd","acbd", "aacd"]
        Output: true
        Output: Strings "abcd" and "aacd" differ only by one character in the index 1.
    Example 2
        Input: dict = ["ab","cd","yz"]
        Output: false
    Example 3
        Input: dict = ["abcd","cccc","abyd","abab"]
        Output: true
Ideas
    - generate all variations of the current word

        abcd => *bcd, a*cd, ab*d, abc*

        + check if any of those variations exist elsewhere
"""

class Solution:
    def differByOne(self, words: List[str]) -> bool:
        """Creation variations and check

        Generate variations of each string
        - check if those substring variations
        exists with other words

        - maintain a seperate set to ensure our current variation
        is not for the same word

        Time Complexity
            O()
        Space Complexity
            O(n*m)
        """

        s_global = set([])

        for word in words:

            s_cur = set([])
            n: int = len(word)

            # print(f'word: {word}')

            for ix in range(n):

                substr = word[:ix] + '*' + word[ix + 1:]

                # print(f' sub: {substr}')

                if substr in s_global:
                    return True
                else:
                    s_cur.add(substr)

            # Add words
            s_global.update(s_cur)

            # print(f'global: {s_global}')

        return False
