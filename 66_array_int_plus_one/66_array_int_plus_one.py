"""

Example

    Input: [1,2,3]
    Output: [1,2,3]

Cases

    Simple Case
        - only modify the last value
        - [1, 2, 3] => [1, 2, 4]

    Carry Over
        - values carry over into next digit spot
        - [8, 9, 9] => [9, 0, 0]

    Carry over with Extra Digit
        - [9, 9, 9] => [1, 0, 0, 0]

    Lots of Zeros
        - [2, 0, 0, 0] => [2, 0, 0, 1]

Strategy

    - create an empty array with one extra digit ie: [0, 0, 0, 0, 0]
    - copy digits to the empty array

        - index  :[0, 1, 2] => [0, 1, 2, 3]
        - values :[9, 9, 9] => [0, 0, 0, 0]

    - start from the right and add 1
    - carry over remaining digits to the next position and so forth

        [8, 9] => [9, 0]

    - continue to carry over digits until the end of the array
    - remove a leading zero if one exists
    - return the value


"""

from typing import List


class Solution:
    def plusOne(self, digits: List[int]) -> List[int]:

        # Create an empty array
        n = len(digits)
        arr = [0 for _ in range(n + 1)]

        carry_over = 1

        # Iterate through the array backwards
        for ix in range(n - 1, -1, -1):

            # print(f"digit: {digits[ix]}")

            # Take the current digit and add 1
            digit_new = digits[ix] + carry_over

            # Determine if any carry over
            if digit_new > 9:

                # Save new value to the new location
                arr[ix + 1] = 0
                carry_over = 1

            else:

                # Save new value to the new location
                arr[ix + 1] = digit_new

                # Overwrite carry over
                carry_over = 0

        # Save carry over value
        arr[0] = carry_over

        # Determine where unique digits start
        # [0, 0, 0, 0, 1]
        for ix in range(n + 1):

            # Stop iterating
            if arr[ix] != 0:
                break

        # Save only the unique digits
        return arr[ix:]

    def plusOne2(self, digits: List[int]) -> List[int]:
        """Cleaned up method of only saving unique digits"""

        # Create an empty array
        n = len(digits)
        arr = [0 for _ in range(n + 1)]

        carry_over = 1

        # Iterate through the array backwards
        for ix in range(n - 1, -1, -1):

            # Take the current digit and add 1
            digit_new = digits[ix] + carry_over

            # Determine if any carry over
            if digit_new > 9:

                # Save new value to the new location
                arr[ix + 1] = 0
                carry_over = 1

            else:

                # Save new value to the new location
                arr[ix + 1] = digit_new

                # Overwrite carry over
                carry_over = 0

        # Save carry over value
        arr[0] = carry_over

        # No extra digit exists
        if carry_over == 0:
            return arr[1:]

        # Save all the digits
        else:
            return arr

    def plusOne3(self, digits: List[int]) -> List[int]:
        """Modify the array in place and only continue to iterate if a carry over exists

        If now carry over exist, then the rest of the array is the same as before

        Notes:
            wasn't faster than before
        """

        # Create an empty array
        n = len(digits)
        ix = n - 1
        carry_over = 1

        # While we have some carry over
        while carry_over != 0 and ix >= 0:

            # Take the current digit and add 1
            digit_new = digits[ix] + carry_over

            # Determine if any carry over
            if digit_new > 9:

                # Save new value to the new location
                digits[ix] = 0
                carry_over = 1

            else:

                # Save new value to the new location
                digits[ix] = digit_new

                # Overwrite carry over
                carry_over = 0

            # Increment count
            ix -= 1

        # No extra digits added
        if carry_over == 0:
            return digits

        # Add the carry over digit
        else:

            arr = [1]
            arr.extend(digits)

            return arr


if __name__ == "__main__":

    s = Solution()

    # arr_in = [3]
    # arr_in = [1, 2, 3]
    arr_in = [8, 9, 9]
    # arr_in = [9, 9, 9]
    # arr_in = [2, 0, 0, 0]

    # print(s.plusOne(arr_in))
    print(s.plusOne3(arr_in))
