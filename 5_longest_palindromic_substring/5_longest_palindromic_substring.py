"""

Inputs
    s (str): input string
Outputs:
    str: longest palinfromic substring

Goals
    Given a string s, find the longest palindromic substring in s

Ideas
    
    - crawl from center point and check values
        + may need special checks since palindromes
        can be of even or odd length
        
        perhaps try both

Strategies

    Center & Crawl
        - at each index crawl left and right
        - add characters while equal
            
            + we must do handling

        Time Complexity
            O(n^2)
        Space Complexity
            O(1)

    Dynamic Programming??


"""

from typing import Tuple

class Solution:
    def longestPalindrome(self, s: str) -> str:
        """At each character crawl our string for possible
        palindromes
        
        - try crawling from each substring
            + odd start   ie: 'a'
            + even start  ie: 'aa'
        
        Time Complexity
            O(n^2)

            - technically doing 2n^2 since we do 2n crawls at each of n characters

        Space Complexity
            O(1)
        """

        n: int = len(s)
        
        # Null case
        if n == 0:
            return ""
            
        max_len: int = 0
        max_ix = None

        for ix in range(n):
            
            # Crawl current character only (odd palindrome)
            crawl_ix = self.crawl_from(s, ix, ix)
            
            if crawl_ix:
                cur_len = (crawl_ix[1] - crawl_ix[0]) + 1
                
                if cur_len > max_len:
                    max_len = cur_len
                    max_ix = crawl_ix
            
            # Crawl current character and adjacent (even palindrome)
            crawl_ix = self.crawl_from(s, ix, ix + 1)
            
            if crawl_ix:
                cur_len = (crawl_ix[1] - crawl_ix[0]) + 1
                
                if cur_len > max_len:
                    max_len = cur_len
                    max_ix = crawl_ix
            
        
        # Grab the substring of longest length
        return s[max_ix[0]:max_ix[1] + 1]
    
    
    def crawl_from(self, s: str, left_ix: int, right_ix: int) -> Tuple[int, int]:
        """Crawl from the current substring, and add characters to both
        sides
        
        - only continue adding while 
        
        Args:

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """
    
        n: int = len(s)
        max_len: int = 0
        max_ix = None
            
        while left_ix >= 0 and right_ix <= n-1 and s[left_ix] == s[right_ix]:
            
            cur_len = (right_ix - left_ix) + 1
            
            if cur_len > max_len:
                max_len = cur_len
                max_ix = [left_ix, right_ix]
            
            left_ix -= 1
            right_ix += 1
            
        return max_ix
