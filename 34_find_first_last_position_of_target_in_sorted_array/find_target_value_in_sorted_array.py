from typing import List


def naive(arr: list, target: int):
    """Find the first and last indices of the target value

    Time Complexity:
        O(n)
    Space Complexity
        O(1)

    Returns:
     List[int, int] = first and last indices with target value
    """

    n: int = len(arr)

    # Init
    ix_first: int = -1
    ix_last: int = -1

    for ix in range(n):

        if arr[ix] == target:
            ix_first = ix

            # While still see the same value
            while ix < n and arr[ix] == target:

                ix_last = ix
                ix += 1

            return [ix_first, ix_last]

    return [ix_first, ix_last]


def linear_and_binary_search(arr: list, target: int):
    """Find the first and last indices of the target value

    Time Complexity:
        O(n)
    Space Complexity
        O(1)

    Returns:
     List[int, int] = first and last indices with target value
    """

    n: int = len(arr)

    # Init
    ix_first: int = -1
    ix_last: int = -1

    low: int = 0
    high: int = n

    while low < high:

        mid = (low + high) // 2

        # print(low, high, mid)

        # Found our value
        if arr[mid] == target:

            # Start at mid point
            ix = mid

            # Explore lower range
            # while still see the same value
            while ix >= 0 and arr[ix] == target:
                ix_first = ix
                ix -= 1

            # Start at mid point
            ix = mid

            # Explore upper range
            # while still see the same value
            while ix < n and arr[ix] == target:
                ix_last = ix
                ix += 1

            return [ix_first, ix_last]

        # Value too small
        # bump start point up
        elif arr[mid] < target:
            low = mid + 1

        # Value too large
        # bump mid point down
        else:
            high = mid - 1

    return [ix_first, ix_last]


def binary_search(arr: list, target: int, low: int, high: int, closest: bool=False):
    """Find the indice of the target value in the array

    Time Complexity:
        O(log(n))
    Space Complexity
        O(1)

    Args:
        arr (List[int]): sorted array of values
        target (int): value to search for
        low (int): lower index of search range (inclusive)
        high (int): upper index of search range (inclusive)
        closest (bool): find the closest index

    Returns:
      int: indice of the target value
    """

    while low <= high:

        mid = (low + high) // 2

        print('low:', low, 'high:', high, 'mid:', mid)

        # Found our value
        if arr[mid] == target:
            return mid

        # Value too small
        # bump start point up
        elif arr[mid] < target:
            low = mid + 1

        # Value too large
        # bump mid point down
        else:
            high = mid - 1

    if closest:
        return low
    else:
        return -1


def binary_search_iteration(arr: list, target: int):
    """Find the first and last indices of the target value

    Use binary search to find
        search for the target value
        search for the first location of target value
        search for the last location of target value

    Time Complexity:
        O(log(n))
    Space Complexity
        O(1)

    Returns:
     List[int, int] = first and last indices with target value
    """

    n: int = len(arr)

    # Find target value
    ix_target = binary_search(arr, target, low=0, high=n - 1)

    # Target not found
    if ix_target == -1:
        return [-1, -1]

    else:

        # Init
        ix_first = ix_target
        ix_last = ix_target
        ix_found = ix_target

        while ix_found >= 0:

            # Recurse on lower range
            ix_found = binary_search(arr, target, low=0, high=ix_first - 1)

            # Found a lower index with target
            if ix_found != -1 and ix_found < ix_first:
                ix_first = ix_found

        ix_found = ix_target

        while ix_found >= 0:

            # Recurse on upper range
            ix_found = binary_search(arr, target, low=ix_last + 1, high=n - 1)

            # Found a lower index with target
            if ix_found != -1 and ix_found > ix_last:
                ix_last = ix_found

    return [ix_first, ix_last]


''' Try 2 '''

class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:
        """Use binary search to find first occurence
        
        - continue binary search to find smallest occurence
        - continue binary search to find largest occurence
        """
        
        n: int = len(nums)
        
        if n == 0:
            return [-1, -1]
        
        # Find the location of the value
        # Note: we use inclusive bounds!!!!
        ix = self.find_any_occurence(nums, target, 0, n - 1)
        
        # Couldn't find any occurence
        if ix == -1:
            return [-1, -1]
        else:
            ans = [ix, ix]
        
        # Find first occurence
        first_ix = self.find_first_occurence(nums, target, 0, ix)
        
        # Find last occurence
        last_ix = self.find_last_occurence(nums, target, ix + 1, n - 1)
        
        # Update if we found anything
        if first_ix > -1:
            ans[0] = first_ix
        
        if last_ix > -1:
            ans[1] = last_ix
        
        return ans
        
        
    def find_any_occurence(self, arr: List[int], target: int, low: int, high: int):   
        """Find if any occurence of target in array"""
        while low <= high:
            
            mid = (low + high) // 2
            
            if arr[mid] == target:
                return mid
            
            # Too small
            elif arr[mid] < target:
                low = mid + 1
            
            # Too large
            else:
                high = mid - 1
        
        return -1
    
    def find_first_occurence(self, arr: List[int], target: int, low: int, high: int):   
        """Find if first occurence of target in array"""
        
        ans: int = -1
        
        while low <= high:
            
            mid = (low + high) // 2
            
            # Found it, but lets' search lower
            if arr[mid] == target:
                ans = mid
                high = mid - 1
            
            # Too small
            elif arr[mid] < target:
                low = mid + 1
            
            # Too large
            else:
                high = mid - 1
        
        return ans
    
    def find_last_occurence(self, arr: List[int], target: int, low: int, high: int):    
        """Find if last occurence of target in array"""
        
        ans: int = -1
        
        while low <= high:
            
            mid = (low + high) // 2
            
            # Found it, but let's search higher
            if arr[mid] == target:
                ans = mid
                low = mid + 1
            
            # Too small
            elif arr[mid] < target:
                low = mid + 1
            
            # Too large
            else:
                high = mid - 1
        
        return ans


if __name__ == '__main__':

    # Test case 1
    # arr_in = [2, 2]
    # arr_in = [5, 7, 7, 8, 8, 10]
    # arr_in = [6, 6, 6, 6, 6, 6]
    arr_in = [6, 6, 6, 6, 6, 6]

    n = len(arr_in)

    # Find values in array
    # print(binary_search(arr_in, 5, low=0, high=n - 1))
    # print(binary_search(arr_in, 7, low=0, high=n - 1))
    # print(binary_search(arr_in, 8, low=0, high=n - 1))
    # print(binary_search(arr_in, 10, low=0, high=n - 1))

    # Find non existent value
    # print(binary_search(arr_in, 9, low=0, high=n - 1))
    # print(binary_search(arr_in, 9, low=0, high=n - 1, closest=True))

    # Find target in array with repeating values
    # print(binary_search(arr_in, 6, low=0, high=n - 1))
    # print(binary_search(arr_in, 7, low=0, high=n - 1))

    # Find target in array with 2 items
    # print(binary_search_iteration(arr_in, 7))
    # print(binary_search_iteration(arr_in, 2))

    print(binary_search_iteration(arr_in, 7))
    print(binary_search_iteration(arr_in, 6))

    # Reduce bounds until we find first value
    # print(naive(arr_in, 7))
    # print(linear_and_binary_search(arr_in, 7))
    # print(binary_search_tri(arr_in, 7))

    # print(naive(arr_in, 8))
    # print(linear_and_binary_search(arr_in, 8))
    # print(binary_search_tri(arr_in, 8))

    # print(naive(arr_in, 10))
    # print(linear_and_binary_search(arr_in, 10))
    # print(binary_search_tri(arr_in, 10))

    # print(naive(arr_in, 6))
    # print(linear_and_binary_search(arr_in, 6))
    # print(binary_search_tri(arr_in, 6))
