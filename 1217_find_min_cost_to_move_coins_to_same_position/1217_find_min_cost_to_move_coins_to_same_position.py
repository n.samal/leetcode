"""
Inputs
    position (List[int]): initial location of each chip
Outputs:
    min cost to move chips to the same location

Notes
    - we can make changes to the chip position 2 ways
        + cost = 0: position[i] + 2 or position[i] - 2
        + cost = 1: position[i] + 1 or position[i] - 1

Ideas

    - maybe dynamic programming problem?

        + each step we have a potential of two choices

    - determine the initial break down of chips

        + then move smaller piles to largest piles 
        (too general since movement is cost dependent)

    - even shifts appear to be near 0 cost

        + we can just assume all coins at even numbered locations
        eventually end up at the same location at no cost

        positions = [0, 2, 4, 6, 8] => all shift to 0
        positions = [1, 3, 5, 7, 9] => all shift to 1

        similarly all odd numered coins end up at the same location

    - once we have all those coins in the same location

        + we must shift all the even coins to the odd location
        or shift all the odd coins to the even location

        both are the same cost (cost = 1 * n_coins)
"""

class Solution:
    def minCostToMoveChips(self, position: List[int]) -> int:

        evens: int = 0
        odds: int = 0

        # Count how many are at even positions or odd positions
        for v in position:

            if v % 2 == 0:
                evens += 1
            else:
                odds += 1

        # Calculate the cost to move all the even coins to an odd location
        # or all odd coins to the even location
        return min(evens, odds)
