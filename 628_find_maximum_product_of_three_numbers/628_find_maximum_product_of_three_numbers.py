"""
Inputs
    arr (List[int]): values in unknown order
Outputs
    int: maximum product of three values

Examples

    Example 1:

        Input: nums = [1,2,3]
        Output: 6
        
    Example 2:

        Input: nums = [1,2,3,4]
        Output: 24
        
    Example 3:

        Input: nums = [-1,-2,-3]
        Output: -6

Ideas

    - we can compute the maximum multiple ways

        + product of three largest positive numbers
        + product of two most negative numbers with the most
        positive number

    - sorted the entire array, then grab the min and max values
    from there

    - track the min and max numbers as we do a linear scan

        + we slide the values down and update the numbers as we go

        val  -> max1
        max1 -> max2
        max2 -> max3

        and vice versa for each case

    - similar to minimum and maximum amplitude possible
"""


class Solution:
    def maximumProduct(self, arr: List[int]) -> int:
        """Linear scan with min & max updating

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        # Grab the three largest numbers
        # Grab the three smallest numbers
        max1, max2, max3 = sorted(arr[:3], reverse=True)
        min1, min2, min3 = sorted(arr[:3], reverse=False)

        max_product = max1 * max2 * max3

        # Iterate along the remaining numbers and update
        # our min and max values
        for v in arr[3:]:

            # Update max values
            if v > max1:
                max3 = max2
                max2 = max1
                max1 = v

            elif v > max2:
                max3 = max2
                max2 = v

            elif v > max3:
                max3 = v

            # Update minimum values
            if v < min1:
                min3 = min2
                min2 = min1
                min1 = v

            elif v < min2:
                min3 = min2
                min2 = v

            elif v < min3:
                min3 = v

            # Largest product can be 
            # max1 * max2 * max3
            # min1 * min2 * max1
            max_product = max1 * max2 * max3
            max_product = max(max_product, min1 * min2 * max1)

        return max_product
