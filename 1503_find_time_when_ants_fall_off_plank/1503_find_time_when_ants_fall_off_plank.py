"""
Inputs
	n (int): plack length
	left (List[int]): location of ants moving left
	right (List[int]): location of ants moving right
Outputs
	int: moment when the last ant falls off the plank

Notes
    - ants most 1 unit each time step
    - when ants moving left reach the end of the plank, they're off
    - when ants moving right reach the end of the plank, they're off
    - when ants two ants moving in opposite directions
    bump into each other they switch directions

Examples

	Example 1:

		Input: n = 4, left = [4,3], right = [0,1]
		Output: 4
		Explanation: In the image above:
		-The ant at index 0 is named A and going to the right.
		-The ant at index 1 is named B and going to the right.
		-The ant at index 3 is named C and going to the left.
		-The ant at index 4 is named D and going to the left.
		Note that the last moment when an ant was on the plank is t = 4 second, after that it falls imediately out of the plank. (i.e. We can say that at t = 4.0000000001, there is no ants on the plank).

	Example 2:

		Input: n = 7, left = [], right = [0,1,2,3,4,5,6,7]
		Output: 7
		Explanation: All ants are going to the right, the ant at index 0 needs 7 seconds to fall.

	Example 3:

		Input: n = 7, left = [0,1,2,3,4,5,6,7], right = []
		Output: 7
		Explanation: All ants are going to the left, the ant at index 7 needs 7 seconds to fall.

	Example 4:

		Input: n = 9, left = [5], right = [4]
		Output: 5
		Explanation: At t = 1 second, both ants will be at the same intial position but with different direction.

	Example 5:

		Input: n = 6, left = [6], right = [0]
		Output: 6

	Hand Calc

		time = 0
		i: 0 1 2 3 4 5 6 
		L: 
		R: 

Ideas
    - when ants bump into each other ie: at same index, they swap directions

		Before swap
        L    x
        R:   x

		After swap
        L    x
        R:   x

        since they swap and are at the same position, it's
        equivalent to nothing happening

    - find max of all left ants reach the right and all right ants reach the left
    - the bound for the left ants, will be when the right most ant reaches the beginning
    - the bound for the right ants, will be when the left most ant reaches the end

"""

class Solution:
    def getLastMoment(self, n: int, left: List[int], right: List[int]) -> int:
    	"""

		- ant falls off the plank the second after it reaches the last position
		on the plank

		Time Complexity
			O(n*ln(n))
		Space Complexity
			O(n*ln(n))
    	"""

        # Sort positions
        left = sorted(left)
        right = sorted(right)

        # Grab position of right most ant moving left
        # location - 0
        if left:
            left_bound = left[-1]
        else:
            left_bound = 0

        # Grab position of left most ant moving right
        # n - location
        if right:
            right_bound = n - right[0]
        else:
            right_bound = 0

        # We're bounded by the max time of either
        return max(right_bound, left_bound)
