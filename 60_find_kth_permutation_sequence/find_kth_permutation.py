"""
Inputs

    n (int): upper range of numbers in set ie: [1, 2, 3, ... n]
    k (int): permutation index to return

Outputs
    int: grab the kth permutation

Cases

    n = 1, k = 1
    n = 2, k = 1, 2
    n = 3, k = 1, 2, 3 ... !3 (General Case)

Strategy

    Brute Force

        - compute all permutations
        -  spit on the kth permutation

    Banded Permutation

        - determine the range of permutation values for number in the set

        ie: set = [1, 2, 3, 4], k = 9 (0 based index of 8)

        there are a total of !4  permutations = 4*3*2*1 = 24

        We would first calculate all permutations beginning with 1, then 2, then 3, then 4

        ie: 1, perms of [2, 3, 4]
            2, perms of [1, 3, 4]
            3, perms of [1, 2, 4]
            4, perms of [1, 2, 3]

        for a given number we know the remaining count of values

             rem = n - 1
        ie:  rem = 4 - 1 = 3

        for the number of values remaning we know the number of possible permutations
        https://www.mathsisfun.com/combinatorics/combinations-permutations.html

            number of permutations = !n / !(n - r)
        ie: number of permuations = !3 / !(3 - 3) = !3 = 6

        If we know the number of permutations for each band we can figure out which band to dig into

        1: 1 - 6
        2: 7 - 12
        3: 13 - 18
        4: 19 - 24

        We know we should dig into the second band since 9 is within 7 - 12

        In a similar fashion we can continue this logic recursively on the band we just figured out

        2, 1, perms of [3, 4]
        2, 3, perms of [1, 4]
        2, 4, perms of [1, 3]

        Each band here has 2 values or !2 factorial permuations  so

        2, 1, perms of [3, 4]: 7 to 8
        2, 3, perms of [1, 4]: 9 to 10
        2, 4, perms of [1, 3]: 11 to 12

        We select the second band since 9 is there

        2, 3, 1 perms of [4]: 9 
        2, 3, 4 perms of [1]: 10

        There's only a single value in each band so the result is [2, 3, 1, 4]
"""

import math


class Solution:

    def getPermutation(self, n: int, k: int) -> str:
        """First pass and hand calc methodology

        Time Complexity
            O(n^2) due to removal of value in array
        Space Complexity:
            O(n)
        """

        # Return base case
        if n == 1:
            return '1'

        # Update k to use 0 based indexing
        # ie: 9 -> 8th index
        k = k - 1

        # Create range of values
        arr = [ix for ix in range(1, n + 1)]

        prefix = ""
        ix = 0  # index in array
        n_rem = n  # number of remaining values

        while n_rem > 0:

            # Compute total number of permutations in each band
            band_size = math.factorial(n_rem) / n_rem

            # Check if k is with our current band range
            # 1: 0 - 5 Not here
            # 2: 6 - 11
            band_low = ix * band_size
            band_high = (ix + 1) * band_size

            if band_low <= k < band_high:

                # Remove value from our numbers
                # Shift index by 1 for python
                value = arr.pop(ix)

                # Save the current starting value in the remaining numbers
                prefix += str(value)

                # Update the number of remaining values
                n_rem -= 1

                # Start at beginning of sorted numbers left
                ix = 0

                # Update the k value to search for
                # 2: 6 - 11 we want number 8
                # we'll now want the 8 - 6, the 2nd number in the range
                k = k - band_low

            # Move to the next value and therefore band
            # 1: 0 - 5 Not here
            # 2: 6 - 11
            else:
                ix += 1

        return prefix

    def getPermutation2(self, n: int, k: int) -> str:
        """First pass and hand calc methodology

        - save known factorials to save computation time (saves alot!)
        
        Time Complexity
            O(n^2) due to removal of value in array (77% percentile)
        Space Complexity:
            O(n)
        """

        # Return base case
        if n == 1:
            return '1'

        # Factorials computed
        factorials = [1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880]

        # Update k to use 0 based indexing
        # ie: 9 -> 8th index
        k = k - 1

        # Create range of values
        arr = [ix for ix in range(1, n + 1)]

        prefix = ""
        ix = 0  # index in array
        n_rem = n  # number of remaining values

        while n_rem > 0:

            # Compute total number of permutations in each band
            band_size = factorials[n_rem] / n_rem

            # Check if k is with our current band range
            # 1: 0 - 5 Not here
            # 2: 6 - 11
            band_low = ix * band_size
            band_high = (ix + 1) * band_size

            if k < band_high:

                # Remove value from our numbers
                # Shift index by 1 for python
                value = arr.pop(ix)

                # Save the current starting value in the remaining numbers
                prefix += str(value)

                # Update the number of remaining values
                n_rem -= 1

                # Start at beginning of sorted numbers left
                ix = 0

                # Update the k value to search for
                # 2: 6 - 11 we want number 8
                # we'll now want the 8 - 6, the 2nd number in the range
                k = k - band_low

            # Move to the next value and therefore band
            # 1: 0 - 5 Not here
            # 2: 6 - 11
            else:
                ix += 1

        return prefix

    def getPermutation3(self, n: int, k: int) -> str:
        """First pass and hand calc methodology

        - save known factorials to save computation time (saves alot!)
        - compute band location using formula

                ix = permutations left // k
            ie: ix = 23 / 8 = 2.875

        Time Complexity
            O(n^2) due to removal of value in array
        Space Complexity:
            O(n)
        Notes:
            not significantly faster
        """

        # Return base case
        if n == 1:
            return '1'

        # Factorials computed
        factorials = [1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880]

        # Update k to use 0 based indexing
        # ie: 9 -> 8th index
        k = k - 1

        # Create range of values
        arr = [ix for ix in range(1, n + 1)]

        prefix = ""
        ix = 0  # index in array
        n_rem = n  # number of remaining values

        while n_rem > 0:

            # Compute band index
            ix = (k * n_rem) // factorials[n_rem]

            # Calculate the size of the permutation band
            band_size = factorials[n_rem] // n_rem
            band_low = ix * band_size

            # Remove value from our numbers
            # Shift index by 1 for python
            value = arr.pop(ix)

            # Save the current starting value in the remaining numbers
            prefix += str(value)

            # Update the permutation number, k
            # 2: 6 - 11 we want number 8
            # we'll now want the 8 - 6, the 2nd number in the range
            k = k - band_low

            # Update the number of remaining values
            n_rem -= 1

        return prefix


if __name__ == "__main__":

    s = Solution()

    n = 4
    k = 9

    print(s.getPermutation(n, k))
    print(s.getPermutation3(n, k))
