"""
Inputs

Outputs
    add(int)
        add value to the end of the list
    getProduct(k) => int
        get the product of the last k values

Notes
    - 0 <= num <= 100
    - 1 <= k   <= 40000
    - all ks will be valid

Examples

    Input
        ["ProductOfNumbers","add","add","add","add","add","getProduct","getProduct","getProduct","add","getProduct"]
    [[],[3],[0],[2],[5],[4],[2],[3],[4],[8],[2]]

    Output
    [null,null,null,null,null,null,20,40,0,null,32]

    Explanation
    ProductOfNumbers productOfNumbers = new ProductOfNumbers();
    productOfNumbers.add(3);        // [3]
    productOfNumbers.add(0);        // [3,0]
    productOfNumbers.add(2);        // [3,0,2]
    productOfNumbers.add(5);        // [3,0,2,5]
    productOfNumbers.add(4);        // [3,0,2,5,4]
    productOfNumbers.getProduct(2); // return 20. The product of the last 2 numbers is 5 * 4 = 20
    productOfNumbers.getProduct(3); // return 40. The product of the last 3 numbers is 2 * 5 * 4 = 40
    productOfNumbers.getProduct(4); // return 0. The product of the last 4 numbers is 0 * 2 * 5 * 4 = 0
    productOfNumbers.add(8);        // [3,0,2,5,4,8]
    productOfNumbers.getProduct(2); // return 32. The product of the last 2 numbers is 4 * 8 = 32 

Ideas

    - maintain a rolling product array
        + when we get a new value, multiply all previous values by this number
        + add our our new number to the end

        add: Time Complexity
            O(n)

        + grab this product when needed using arr[-k]

    - try a rolling product in the opposite direction
        + instead of going from the rear, start from the front

        add: Time Complexity
            O(1)

        + caveats
            * how to handle a zero? 

			replace with a 1, so the following values will not be impacted

			arr  = [1, 2, 4, 0, 3]
            prod = [1, 2, 8, 1, 3]

            remove all previous values since they would be zero any how

            [1, 2, 4, 0]
             0  0 <=  0

        + calculate the desired product using other products

			product = product at tail / product adjacent to desired location

        	* how to handle the first case => need a dummy value for a prior

    - since all integers are positive, our product should be monotonically increasing 
    except where 0 occurs

        + we can back calculate our number using the products
        as well

        values = [1, 2, 3, 4, 0, 5]
        prod   = [1, 2, 6, 24,1, 5]
        goal   = [         <= 0, 5]
        back c = [1, 2/1,6/2, 24/6, 1/24, 0/1]

        if we encounter non integer back calculated value
        it will be due to a product reset from a zero

"""

class ProductOfNumbers1:

    def __init__(self):

        self.arr = []

    def add(self, num: int) -> None:
        """Multiply product array by new value"""        
        for ix in range(len(self.arr)):
            self.arr[ix] = self.arr[ix] * num

        self.arr.append(num)

    def getProduct(self, k: int) -> int:
        """Grab the kth value from the back
        from the product array"""
        n: int = len(self.arr)

        return self.arr[n - k]

class ProductOfNumbers:

    def __init__(self):

        self.arr = [1]

    def add(self, num: int) -> None:
        """Multiply product array by new value

        Time Complexity
            O(1)
        Space Complexity
            O(n)
        """        

        # Reset the chain, all values to the left would be zero
        # any how
        if num == 0:
            self.arr = [1]
        else:
            self.arr.append(self.arr[-1] * num)

    def getProduct(self, k: int) -> int:
        """Grab the kth value from the back
        from the product array

        Time Complexity
            O(1)
        Space Complexity
            O(1)
        """

        n: int = len(self.arr)

        # If beyond the chain then a chain reset exists
        # ie: a value = 0
        if k >= n:
            return 0
        else:
            return self.arr[-1] // self.arr[n - k - 1]

# Your ProductOfNumbers object will be instantiated and called as such:
# obj = ProductOfNumbers()
# obj.add(num)
# param_2 = obj.getProduct(k)
