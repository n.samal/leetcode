"""
Inputs
    List[int]: asteroid state prior currently
Outputs
    List[int]: asteroid state after collision

Goals
    - find out state of asteroids after all collisions
    - if two asteroids meet, the smaller one explodes
    - if both are the same size they both explode
    - two asteroids in the same direction won't meet

Notes
    - positive values means moving right
    - negative values means moving left
    
Ideas
    - use a stack that is monotonically increasing
        + if monotonically increasing values are moving in opposite directions
        
        ie: asteroids = [-2, -1, 1, 2]
        
    - more so concered with negatives and not just decreasing
        + if the value to the right is negative and last value is positive
        we must cascade
        
        ie: asteroids = [10, 2, -5]
        
        -5 collides with 2 => -5
        -5 collides with 10 => 10

Cases

    Null Case (No Asteroids)
    Single Asteroids (No Collision)
    All Positive (No Collision)
    All Negative (No Collision)
    All Negative then All Positive (No Collision)
    All Positive then All Negative (Collision)
    
    Equal Collision
        - both disappear
    Negative is greater
        - negative stays
    Positive is greater
        - positive stays

Strategy
    Stack
        - iterate through values and maintain a stack
        if the last value is positive and new value is negative then we must
        check for collisions
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
"""


class Solution:
    def asteroidCollision(self, asteroids: List[int]) -> List[int]:
        
        n: int = len(asteroids)
            
        # No asteroids or nothing to collide with
        if n == 0:
            return []
        elif n == 1:
            return asteroids
        
        
        stack = [asteroids[0]]
        
        for ix in range(1, n):
            
            # Add the value
            stack.append(asteroids[ix])
        
            # print(f'stack: {stack}')
        
            # If asteroids are moving toward each other
            # current value is negative (moving left)
            # previous value is positive (moving right)
            while len(stack) >= 2 and stack[-1] < 0 and stack[-2] > 0:
                
                # Equal collision (remove both)
                if abs(stack[-2]) == abs(stack[-1]):
                    _ = stack.pop()
                    _ = stack.pop()
                
                # Negative is greater magnitude
                # Remove the smaller positive asteroid and replace with negative
                elif abs(stack[-1]) > abs(stack[-2]):
                    neg = stack.pop()
                    stack[-1] = neg
                
                # Positive is greater magnitude remove negative
                else:
                    _ = stack.pop()
                
                # print(f'stack: {stack}')
        
        return stack
