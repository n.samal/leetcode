"""
Inputs
    text (str)
Outputs
    str: lexicographically smallest subsequence of text
    containing all distinct characters

Examples

    Example 1:

        Input: "cdadabcc"
        Output: "adbc"

    Example 2:

        Input: "abcd"
        Output: "abcd"

    Example 3:

        Input: "ecbacba"
        Output: "eacb"

    Example 4:

        Input: "leetcode"
        Output: "letcod"

Ideas
    - gather the indices of each unique character
        + if only one location, must use this location
        + crawl the char tree

    - add each character greedily
        + if the character already exists in our string
        see if our lexiography would improve by kicking it out

        old: ecba   char: c
        kicking out old k, brings b forward, which is smaller
        new: ebac

        old: ebac   char: b
        kicking out b, brings a forward
        new: eacb

    - iterate ahead to time to determine the last occurence of each character
        + we know if another character exists later, so we can kick out a character
        and for the "smaller" char now

Manual

    Example 1

        cdadbcc

          a
            b
        c    cc
         d d

    Example 3
        ecbacba

           a  a
          b  b
         c  c
        e

    Example 4
        leetcode

            c
         ee    e
              d
        l
             o
           t

"""


class Solution:
    def smallestSubsequence(self, text: str) -> str:
        """Greedy 'monotonic' stack

        - track the last occurence of each char
        - maintain a monotonically increasing stack if possible
            + if there's no other option later to use, we must add the value
            + otherwise kick out larger values
        - if we've used the value already, no need to use it again

        Time Complexity
            O(n)
        Space Complexity
            O(1)  at most one of each character of alphabet
        """

        stack = []
        cur = set([])
        n: int = len(text)

        last_seen = {text[ix]: ix for ix in range(n)}

        # Only consider new characters
        # Kick out larger values, if we have another option for later
        for ix, char in enumerate(text):

            if char not in cur:

                while stack and char < stack[-1] and last_seen[stack[-1]] > ix:
                    cur.remove(stack.pop())

                stack.append(char)
                cur.add(char)

        return ''.join(stack)


if __name__ == '__main__':

    # Example 1
    text = "cdadabcc"

    obj = Solution()
    print(obj.smallestSubsequence(text))
