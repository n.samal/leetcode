"""
Inputs
    arr (List[int]): integers in unknown order
Outputs
    bool: is there a 132 pattern

Notes
    - find a 132 pattern
        + i < j < k
        + nums[i] < nums[k] < nums[j]

Examples

    Example 1

        Input: nums = [1,2,3,4]
        Output: false
        Explanation: There is no 132 pattern in the sequence.

    Example 2

        Input: nums = [3,1,4,2]
        Output: true
        Explanation: There is a 132 pattern in the sequence: [1, 4, 2].

    Example 3

        Input: nums = [-1,3,2,0]
        Output: true
        Explanation: There are three 132 patterns in the sequence: [-1, 3, 2], [-1, 3, 0] and [-1, 2, 0].

Ideas

    - if not enough elements, return false

    - use a monotonically increasing stack
        + when we find a decreasing value at the fron there is
        a potential for this pattern

        + this works for some cases but not all (85% pass rate)

    - if we use the smallest value we've seen to the left
    this helps us find potential values for j and k

        + track current mins to the left

        + then maintain a montonically decreasing stack (from l to r)
        + or maintain a montonically decreasing stack (from r to l)

    - tracking only min and max will not work
        + order is not considered

        arr = [5, 4, 3, 2, 1]

        max = 5 across the board

        arr = [1, 2, 3, 4, 5]

References
    - https://leetcode.com/problems/132-pattern/discuss/94071/Single-pass-C%2B%2B-O(n)-space-and-time-solution-(8-lines)-with-detailed-explanation. 
    - https://leetcode.com/problems/132-pattern/discuss/206575/previous-greater-element-(-stack-O(n)-no-reverse)
    - https://leetcode.com/problems/132-pattern/discuss/700414/Python-Detailed-explanation.-Stack-solution
"""

from collections import deque
from typing import List


class Solution:

    ''' Loops '''

    def triple_loop(self, arr: List[int]) -> bool:
        """Try all combinations

        Time Complexity
            O(n^3)
        Space Complexity
            O(1)
        """
        n: int = len(arr)

        if n < 3:
            return False

        for i1 in range(n - 2):
            for i3 in range(i1 + 1, n - 1):
                for i2 in range(i3 + 1, n):

                    if arr[i1] < arr[i2] < arr[i3]:
                        return True

        return False

    def double_loop(self, arr: List[int]) -> bool:
        """Try all combinations with min tracking

        - track the minimum to our left, we'll use
        this as our '1' i the 132 pattern

        - the check all the other 32 pairs

        Time Complexity
            O(n^2)
        Space Complexity
            O(1)
        """
        n: int = len(arr)

        if n < 3:
            return False

        min_1: int = float('inf')

        for i3 in range(n - 1):

            min_1 = min(min_1, arr[i3])

            for i2 in range(i3 + 1, n):

                    if min_1 < arr[i2] < arr[i3]:
                        return True

        return False

    ''' Stack '''

    def monotonic_stack_lr(self, arr: List[int]) -> bool:
        """Monotonically increasing stack

        - maintain a montonically increasing stack
        - if we find a new value meeting our 132 criteria
        then output true

        Notes
            - works for many cases but not all cases
                + example case: arr = [3, 5, 0, 3, 4]
                + when we see 0, we remove all previous values
                which are necessary to find the correct solution
                of (3, 5, 4)
        """

        n: int = len(arr)

        if n < 3:
            return False

        queue = deque([])

        for val in arr:

            if not queue:
                queue.append(val)

            else:

                # We've got enough elements
                if len(queue) >= 2 and val > queue[-2] and val < queue[-1]:
                    return True

                # Remove invalid valid elements up front
                while queue and val < queue[-1]:
                    queue.pop()

                # Add new value
                queue.append(val)

        return False

    def monotonic_stack_rl(self, arr: List[int]) -> bool:
        """Monotonically decreasing queue (r to l)

        - track the min value at each index
            + use this as our 1 candidate of 132

        - maintain a montonically decreasing queue
            to create the 32 candidates

            + ensure queue values are larger than 1
            candidate

        Notes
            + example case: arr = [3, 5, 0, 3, 4]

            + using a monotonically increasing queue from right
            to the left only finds consecutive pairs
                ie: [3 | 5, 0] which isn't valid

                it also doen't may not meet the min at the new index

            + the decreasing queue will find the next greater
            element for all values in the stack!

            it also will allow us to elements that don't meet
            the current min requirement

               cur    queue
                5 > [0, 3, 4]

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        n: int = len(arr)

        if n < 3:
            return False

        # Compute the minimum value at each index
        mins = [arr[ix] for ix in range(n)]

        for ix in range(1, n):
            mins[ix] = min(mins[ix - 1], arr[ix])

        # From the right, maintain a monotonically
        # decreasing queue where values are greater than the min
        queue = deque([])

        for ix in range(n - 1, -1, -1):

            # Add value to queue if it has potential
            # - remove values smaller than current min
            # - check if a valid 3 candidate is found
            if arr[ix] > mins[ix]:

                while queue and queue[0] <= mins[ix]:
                    queue.popleft()

                if queue and arr[ix] > queue[0]:
                    return True

                queue.appendleft(arr[ix])

        return False


    def find132pattern(self, arr: List[int]) -> bool:
        """Monotonically increasing queue

        - find the smallest 1 possible
        - maintain an increasing queue for 32
            + remove values smaller than our min

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """
        
        # we want the smallest 1 possible
        # largest 3 possible
        
        n: int = len(arr)
        
        # For each index track the minimum value available to the left
        mins = [float('inf') for _ in range(n)]
        
        min_val = arr[0]
        for ix in range(1, n):
            mins[ix] = min_val
            
            min_val = min(min_val, arr[ix])
        
        # Iterate from right to left and find a 2 greater than our min        
        queue = deque([])
        for ix in range(n-1, -1, -1):
            
            # print(f'v: {arr[ix]}')
            
            if arr[ix] > mins[ix]:
            
                # Remove values smaller than our current min
                while queue and queue[-1] <= mins[ix]:
                    queue.pop()
            
                # print(f' queue: {queue}')

                # 2 increasing values found
                if queue and arr[ix] > queue[0]:
                    return True
            
                # Empty queue, add the current value
                queue.appendleft(arr[ix])
                
        return False
            

if __name__ == '__main__':

    # Example 1
    # arr = [1, 2, 3, 4]

    # Example 2
    # arr = [3, 1, 4, 2]

    # Example 3
    # arr = [-1, 3, 2, 0]

    # False
    # arr = [1, 0, 1, -4, -3]

    # True (3, 5, 4)
    arr = [3, 5, 0, 3, 4]

    obj = Solution()
    print(obj.triple_loop(arr))
    print(obj.double_loop(arr))
    # print(obj.monotonic_stack_lr(arr))
    print(obj.monotonic_stack_rl(arr))
