"""
Inputs:
    pushed (List[int]): integer sequence that will be pushed
    pushed (List[int]): integer sequence that will be popped
Outputs:
    bool: if and only if this could have been the result of a sequence of push and pop operations on an initially empty stack.

Notes
    - values are distinct

Examples

    Example 1
        Input: pushed = [1,2,3,4,5], popped = [4,5,3,2,1]
        Output: true
        
        Explanation: We might do the following sequence:
        
        push(1), push(2), push(3), push(4), pop() -> 4,
        push(5), pop() -> 5, pop() -> 3, pop() -> 2, pop() -> 1
    
    Example 2
    
        Input: pushed = [1,2,3,4,5], popped = [4,3,5,1,2]
        Output: false
        Explanation: 1 cannot be popped before 2.

Ideas
    
    - we can only pop an existing value
        + start index at popped[0]
        we must have pushed this value for it be present
        
        iterate through pushed until we find popped[0]
    
    - track values we've "seen" to know if it's present in our stack
        + we also need to know that it's accesible. It may be present but be at the bottom
        of the stack. Use an actual stack to verify this


Cases

    Null Null => True
    Null Something
    Something Null
"""


class Solution:
    def validateStackSequences(self, pushed: List[int], popped: List[int]) -> bool:
        """Validate stack sequence
        
        - create an empty stack
        - add values to our stack until we find the next 'pop' value
        - pop the value when found and move the next number
        - if we can't push or pop then we can't do anything
        - if we finish all operations then this sequence is possible
        
        Time Complexity
            O(n + m)
        Space Complexity
            O(n)
        
        """
        push_ix: int = 0
        pop_ix: int = 0
        n: int = len(pushed)
        m: int = len(popped)
        
        stack = []
        
        while push_ix < n or pop_ix < m:
            
            # We have the correct pop value
            if pop_ix < m and stack and stack[-1] == popped[pop_ix]:
                stack.pop()
                pop_ix += 1
            
            # Add our latest stack value
            elif push_ix < n:
                stack.append(pushed[push_ix])
                push_ix += 1
            
            else:
                return False
            
        return True
