"""

Ideas
    - update digits as we get them like in a string or linked list
        + num = num * 10 + int(char)
        + add it to the stack after

    - track multipliers on values
        + '-' -1.0
        + assume anything else is 1.0

    - add values to our stack
        + when we encounter a delimiter
            * +
            * -
            * )
            * end of string

        + start removing items from our stack and add them together
            * )
            * end of string

        once complete put this new value on the stack

Cases

    Null

    Single digit: 8, 9, -10
    Multi digit 89 + 90, -89 -90
    Multi digit with negative 89 - 90
    Negative or positive prior to brackets
        6 - (9 - 10)
        6 + (9 + 10)
"""


class Solution:

    def calculate_v0(self, s: str) -> int:
        """Create a stack of values

        - track values as we see them
        - once we see a delimiter, add the previous value to our stack
        and reset the sign and value for the future value
        - once we encounter a parenthesis ) process that first
        - some special processing for handing '-' prior to parenthesis

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        """

        # Null case
        if not s:
            return 0

        stack = []
        delim = set(['+', '-', ')'])

        # Multiplier
        num: int = 0
        sign: int = 1

        for char in s:

            # print(f'char: {char}  stack: {stack}')

            # Add to current number ie: 1 -> 10 -> 102
            if char.isdigit():
                num = num * 10 + int(char)

            # Add the prior number, and reset for future values
            elif char in delim:
                stack.append(num * sign)

                # Reset value
                num = 0
                sign = 1

                if char == '-':
                    sign *= -1

                elif char == ')':

                    sum_: int = 0
                    while stack and stack[-1] != '(':
                        sum_ += stack.pop()

                    # Remove the parentheses, and add the sum back
                    stack.pop()

                    # Account for negative prior to bracket
                    if stack and stack[-1] == '-':
                        stack.pop()  # remove '-'
                        stack.append(-sum_)
                    else:
                        stack.append(sum_)

            elif char == '(':

                # We've got a negative preceding brackets ie: -(x - y)
                if sign == -1:
                    stack.append('-')

                    # Reset value
                    num = 0
                    sign = 1

                stack.append(char)

        # Add remaining number
        stack.append(sign * num)

        # print(f'stack: {stack}')

        # We reached the end of our string, process the remaining items
        # Should only be numbers remaining
        total: int = 0

        while stack:
            total += stack.pop()

        return total

    def calculate_v1(self, s: str) -> int:
        """Create a stack of values

        - track values as we see them
        - once we see a delimiter, add the previous value to our stack
        and reset the sign and value for the future value
            + when encountering a ( save the sign for later
        - once we encounter a parenthesis ) process values within the bracket first
            + after processing the entire () 
            we apply the sign we saved previously

            ie: -(9 + 8) = -(17) = -17
                +(9 + 8) = +(17) = +17

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        # Null case
        if not s:
            return 0

        stack = []

        num: int = 0  # magnitude of current value
        sign: int = 1  # sign of current value

        for char in s:

            # print(f'char: {char}  stack: {stack}')

            # Add to current number ie: 1 -> 10 -> 102
            if char.isdigit():
                num = num * 10 + int(char)

            # Add the prior number, and reset for future values
            elif char == '-':
                stack.append(sign * num)
                num = 0
                sign = -1

            elif char == '+':
                stack.append(sign * num)
                num = 0
                sign = 1

            # Save the sign and value for processing after parenthesis
            elif char == '(':
                stack.append(sign)
                stack.append(char)

                num = 0
                sign = 1

            elif char == ')':

                sum_: int = sign * num

                while stack and stack[-1] != '(':
                    sum_ += stack.pop()

                # Remove the parentheses and get the sign before the (
                # then add the sum back
                stack.pop()
                sign = stack.pop()
                stack.append(sign * sum_)

                # Reset values
                num = 0
                sign = 1

        # Add remaining number
        stack.append(sign * num)

        # print(f'stack: {stack}')

        # We reached the end of our string, process the remaining items
        # Should only be numbers remaining
        total: int = 0

        while stack:
            total += stack.pop()

        return total


if __name__ == '__main__':

    # s_in = "2-(5-6)"
    s_in = "(5-(1+(5)))"

    obj = Solution()
    # print(obj.calculate_v0(s_in))
    print(obj.calculate_v1(s_in))
