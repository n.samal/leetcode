"""
Inputs:
    List[int]: array of temperatures for each day
Outputs:
    List[int]: number of days until a warmer temperature
Goals:
    - find number of days until a warmer temperature
    - if no future day put 0

Ideas

    - find the distance to the next greater value on the right
        + next greater value implies monotonically decreasing stack
        + likely want to store index instead of value to determine distance
        + For any left over indices pop and say no warmer

Strategies
    
    Stack
        - use next greater value strategy
        - use a monotonically decreasing stack
        - track indices of each value rather than value to calculate
        distances
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)

Examples

    T = [73, 74, 75, 71, 69, 72, 76, 73]
    
    stack = [(73, 0)]
    
    74 is greater, remove smaller values
    
    stack = [(74, 1)]
    
    75 is greater, remove smaller values
    
    stack = [(75, 2)]
    
    71 is smaller, add it
    
    stack = [(75, 2), (71, 3)]
    
    69 is smaller, add it
    
    stack = [(75, 2), (71, 3), (69, 4)]

"""


class Solution:
    def dailyTemperatures(self, arr: List[int]) -> List[int]:
        """Maintain monotonically decreasing stack
        
        - this allows us to find the next greater value
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        n = len(arr)
        
        # No greater day
        if n == 0:
            return 0
        elif n == 1:
            return [0]
        
        stack = [0]
        next_greater = [0 for _ in range(n)]
        
        for ix in range(1, n):
            
            # print(f'temperature: {arr[ix]}')
            
            # Remove days with temperatures larger than current day
            while stack and arr[ix] > arr[stack[-1]]:
                
                jx = stack.pop()
                
                # Compute distance to greater day
                next_greater[jx] = ix - jx
            
            # Add the new value
            stack.append(ix)
        
        return next_greater
