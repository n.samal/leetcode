"""
Inputs
    s (str): string
    k (int) adjacent values for a cluster
Output
    str: remove adjacent letters of size k

Ideas

    - add each new character at a time
    - maintain a stack
        + remove values if the last k values are equal

"""


class Solution:

    def intuition(self, s: str, k: int) -> str:
        """Intuition

        - add a character
        - check for equality of the last k
            + remove it if equal

        Time Complexity
            O(n*k)
        Space Complexity
            O(n)

        Notes
            - can we save the character counts to save time?
        """

        stack = []

        for char in s:

            stack.append(char)

            while stack and len(stack) >= k:

                if all(stack[-1] == stack[-ix] for ix in range(2, k + 1)):
                    for _ in range(k):
                        stack.pop()
                else:
                    break

        return ''.join(stack)

    def stack(self, s: str, k: int) -> str:
        """Track counts of each character to save time

        - save counts for adjacent characters

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        stack = []

        for char in s:

            # Characters match, update counts
            # remove values if necessary
            if stack and stack[-1][0] == char:
                stack[-1][1] += 1

                if stack[-1][1] == k:
                    stack.pop()

            else:
                stack.append([char, 1])

        # Recombine characters
        ans = ''
        for char, count in stack:
            ans += char * count

        return ans


if __name__ == '__main__':

    # Example
    s = "deeedbbcccbdaa"
    k = 3

    obj = Solution()
    print(obj.intuition(s, k))
    print(obj.stack(s, k))
