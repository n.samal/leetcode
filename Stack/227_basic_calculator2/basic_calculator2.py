"""
Inputs
    s (str): mathematical expression
Outputs
   int: evaluated expression

Notes
    - we must maintain the proper order of operations
        + division and multiplication take precedence
    - integer division is used
    - all expressions are valid
    - no negative numbers are used
    - spaces may exist in the expression

Examples

    Example 1:

        Input: s = "3+2*2"
        Output: 7

    Example 2:

        Input: s = " 3/2 "
        Output: 1

    Example 3:

        Input: s = " 3+5 / 2 "
        Output: 5

Ideas
    - iterate from left to right and maintain a stack
        + compile numbers as we get there
        + if we encounter a * or / then we immediately
        evaluate
    
    - we must also evaluate after going through the last character

        ie: '2 + 3'

Cases

    Addition

        '50 + 4'

    Subtraction

        '50 - 4'

    Multiplication

        '50 * 4'

    Division

        '50 / 4'

    Spaces

        '  2 + 4 - 50 *  7  '

    Zeros

        '0 * 0 + 0 - 0'
"""

import re


class Solution:

    ''' Close but no cigar '''

    def get_number(self, s: str, ix: int):
        """Extract number from a string"""

        num: int = 0

        while ix < len(s) and s[ix].isdigit():
            num = num * 10 + int(s[ix])
            ix += 1

        return num, ix

    def stack_v0(self, s: str) -> int:
        """Use a stack to maintain numbers

        - track the sign of the upcoming numerical values
        - for operators, combine the current number
        with the next number

        Notes
            - fails on 0 valued cases
            - fails when we having cascading symbols
                2*3*4

                num = 0 after 3*
                which is pushed to the stack next
        """

        # Remove empty spaces
        s = s.replace(' ', '')

        stack = []

        num: int = 0
        sign: int = 1

        n: int = len(s)
        ix: int = 0

        while ix < n:

            char = s[ix]

            # Numerical value
            if char.isdigit():
                num = num * 10 + int(char)
                ix += 1

            # Symbolic value
            else:

                stack.append(num * sign)

                num = 0
                sign = 1

                if char == '-':
                    sign = -1
                    ix += 1

                elif char == '+':
                    sign = 1
                    ix += 1

                # Multiplication or division
                else:

                    num: int = 0
                    ix += 1

                    # Grab the next number
                    while ix < n and s[ix].isdigit():
                        num = num * 10 + int(s[ix])
                        ix += 1

                    # Combine the previous number with this
                    if char == '/':
                        stack[-1] = int(stack[-1] / num)
                    else:
                        stack[-1] = stack[-1] * num

                    num: int = 0
                    sign: int = 1

        # Add any straglers
        stack.append(sign * num)

        return sum(stack)

    ''' Working '''

    def stack_v1(self, s: str) -> int:
        """Stack

        - use a stack to maintain numbers
        - assume a '+' symbol intially and push our values
        to the stack once we hit a new operator

            3 + 4 |+

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        Notes
            - instead of evaluating the / * immediately
            we wait until the next operation symbol to
            evluate and reset
            - can improve time by maintaining a result and last value
            instead of stack
        """
        stack = []
        operators = set(['+', '-', '/', '*'])

        num_cur: int = 0
        operator_prev: str = '+'

        for ix, char in enumerate(s):

            # Numerical value
            if char.isdigit():
                num_cur = num_cur * 10 + int(char)

            # New Operator or last character
            # let's process the previous operator
            if char in operators or ix == len(s) - 1:

                if operator_prev == '+':
                    stack.append(num_cur)

                elif operator_prev == '-':
                    stack.append(-num_cur)

                elif operator_prev == '/':
                    stack[-1] = int(stack[-1] / num_cur)

                elif operator_prev == '*':
                    stack[-1] = stack[-1] * num_cur

                # Save the new operator and reset our number
                num_cur = 0
                operator_prev = char

        return sum(stack)

    def preprocess_stack(self, s: str) -> int:
        """

        - preprocess the string
            + first remove all white space
            + then break string into numbers and operators

        - process the operators and numbers together
            + every number except the first will have an associated
            operator

            2 + 3 + 4
            2 + 3 / 4
            2 * 3 / 4
            2 / 3 / 4

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        # Remove spaces
        s = s.replace(' ', '')

        # Split string into operators and numbers
        ops = []
        nums = []

        num: int = 0

        for char in s:

            if char.isdigit():
                num = num * 10 + int(char)

            # Add prior number and operator if applicable
            else:
                ops.append(char)
                nums.append(num)
                num = 0

        # Remaining number
        if num:
            nums.append(num)

        # No operators
        # ie: 25467
        if not ops:
            return num

        # Process them into a stack
        stack = [nums[0]]

        for num, op in zip(nums[1:], ops):

            if op == '+':
                stack.append(num)
            elif op == '-':
                stack.append(-num)
            elif op == '*':
                stack[-1] = stack[-1] * num
            else:
                stack[-1] = int(stack[-1] / num)

        return sum(stack)


if __name__ == '__main__':

    # Example 1
    # s = "3+2*2"

    # Example 2
    # s = " 3/2 "

    # Example 3
    # s = " 3+5 / 2 "

    # Example 4
    # s = "2*3*4"

    # Zeros
    # s = "0*0*0"

    # Failed (13)
    s = "14-3/2"

    obj = Solution()
    print(obj.stack_v0(s))
    print(obj.stack_v1(s))
    print(obj.preprocess_stack(s))
