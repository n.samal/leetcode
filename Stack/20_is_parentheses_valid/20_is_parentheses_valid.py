""""


Notes
    - we can use a set to check if parenthesis are lefts or rights
    - if we ever have more rights than lefts we know it's invalid

Strategy
    Stack   

        - use an array, stack, queue
        - iterate through the string and add values to our stack
        - if the compliment to our parenthesis is the last value in our stack, remove it
        - if not add the value to our stack

        - check if the stack is empty upon exit to determine if all parenthesis were valid

"""

from collections import deque


        

class Solution:


    def isValid(self, string: str) -> bool:

        """Determine the brackets provided are balanced

        Args:
            arr (str): string bracket provided of parenthesis
        Returns:
            str: 'YES' or 'NO' value representing whether braces are balanced or not
        """

        print('string:', string)

        # Count of curly brackets
        n_curl_l: int = 0
        n_curl_r: int = 0

        # Count Square brackets
        n_square_l: int = 0
        n_square_r: int = 0

        # Count of parenthesis
        n_paren_l: int = 0
        n_paren_r: int = 0

        rights = set([']', '}', ')'])
        lefts = set(['[', '{', '('])

        # Init queue
        queue = []

        # Go through all characters
        for ix, char in enumerate(string):

            # Increment the value we've seen
            if char == '{':
                n_curl_l += 1
            elif char == '}':
                n_curl_r += 1
            elif char == '[':
                n_square_l += 1
            elif char == ']':
                n_square_r += 1
            elif char == '(':
                n_paren_l += 1
            elif char == ')':
                n_paren_r += 1

            # If the number of right brackets is ever larger
            if n_paren_r > n_paren_l or n_square_r > n_square_l or n_curl_r > n_curl_l:
                return False

            ''' Check for balancing '''

            # Add left brackets to queue
            if char in lefts:
                queue.append(char)

            # We've got a bracket that could close
            elif char in rights:

                # Look for balancing bracket
                if char == ')' and queue[-1] == '(':

                    # Remove the last character in queue
                    queue.pop()

                # We found balancing brackets
                elif char == ']' and queue[-1] == '[':
                    queue.pop()

                elif char == '}' and queue[-1] == '{':
                    queue.pop()
                else:
                    queue.append(char)

            # print(queue)

        # Balanced
        if len(queue) == 0:
            return True
        else:
            return False

    
    def isValid2(self, s: str) -> bool:
        """Deque to pop from left

        Notes:
            - much faster than previous implementation
        """
        # Empty array
        arr = deque([])

        # Store for easy access
        lefts = set(['(', '[', '{'])
        rights = set([')', ']', '}'])

        # Track number of each type
        n_lefts: int = 0
        n_rights: int = 0

        # Iterate through the string
        for char in s:

            # We have something to compare against
            if len(arr) > 0 and char in rights:

                last_char = arr[-1]

                # Remove the last value and ignore the current match
                # ie: (), [], {}
                if last_char == '[' and char == ']':
                    arr.pop()
                    n_lefts -= 1
                elif last_char == '(' and char == ')':
                    arr.pop()
                    n_lefts -= 1
                elif last_char == '{' and char == '}':
                    arr.pop()
                    n_lefts -= 1
                else:
                    arr.append(char)
                    n_rights += 1

            # We have no items or a left parenthesis
            else:

                if char in lefts:
                    n_lefts += 1
                elif char in rights:
                    n_rights += 1

                # Add the current character
                arr.append(char)

            # This must be invalid
            if n_rights > n_lefts:
                return False

        # Empty array
        if not arr:
            return True
        else:
            return False

if __name__ == '__main__':

    pass