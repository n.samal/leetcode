
def find_longest_valid(s: str) -> int:
    """Find the length of the longest valid parentheses

    Args:
        s (str): parentheses
    Returns:
        int: length of longest valid parentheses
    """

    count: int = 0  # current parentheses count
    max_count: int = 0  # longest parentheses count
    count_l : int = 0  # number of current left paranthesis
    count_r : int = 0  # number of current right paranthesis
    queue = []

    for char in s:

        print(' queue: ', queue, 'char:', char)

        # Found a matching pair
        if queue and queue[-1] == '(' and char == ')':
            count += 2

            # Remove matching pairs
            queue.pop()

            # Decrement parentheses count
            count_l -= 1
            count_r -= 1

            # Update max count
            max_count = max(count, max_count)

        else:

            # Add character to our queue
            queue.append(char)

            # Update character count
            if char == '(':
                count_l += 1
            elif char == ')':
                count_r += 1

        # If invalid, rest the current count
        if count_r > count_l:
            count = 0
        
    # If we have remaining characters discount our count

    return max_count


if __name__ == "__main__":

    # Basic: ()
    print(find_longest_valid('()'))

    # Longest = 2
    # print(find_longest_valid('(()'))

    # Longest = 4
    # print(find_longest_valid(')()())'))

    # What about disjoint parenthesis sets
    # print(find_longest_valid('()()())))(((())))'))

