"""
Input
    str: encoded string
Output
    str: decoded string

Goal
    - decode the string
    - 3[a] becomes aaa
    - 3[a2[c]] becomes accaccacc

Notes
    - only digits are used for patterns
    - no extra spaces etc..

    - digits may be multiple numbers
    - patterns can have subpatterns

Cases


"""


class Solution:

    ''' Recursive: Try 1'''

    def decodeString(self, s: str) -> str:

        self.n: int = len(s)
        ix: int = 0
        s_new: str = ''

        while ix < self.n:

            # Character is a letter, just add it
            if s[ix].isalpha():
                s_new += s[ix]
                ix += 1

            # We've found a digit
            else:
                ix, s_sub = self.recurse(s, ix)

                s_new += s_sub

        return s_new

    def recurse(self, s: str, ix: int):
        """
        """

        # Find the end of digit
        jx = ix
        while s[jx] != '[':
            jx += 1

        # We're at a digit, grab it
        k = int(s[ix:jx])

        # Move the digit past the '['
        ix = jx + 1

        s_new = ''

        while ix < self.n:

            # Normal character add it
            if s[ix].isalpha():
                s_new += s[ix]
                ix += 1
            # Pattern stops
            elif s[ix] == ']':
                ix += 1
                break

            # Digit, decode subpattern
            else:
                ix, s_sub = self.recurse(s, ix)

                s_new += s_sub

        # Apply multiplier
        s_new = k * s_new

        return ix, s_new

    ''' Recursion: Cleaner '''

    def decodeString2(self, s: str) -> str:

        self.n: int = len(s)
        self.ix: int = 0

        stack = self.recurse2(s)

        # Join strings at the end
        s_new = ''.join(stack)

        return s_new

    def recurse2(self, s: str):
        """

        - step into recursion anytime we start a new pattern '['
        - after we exit the pattern ']'
            multiply the pattern result by out multipler
            and add it to the main string

            make sure to reset the pattern and multiplier here

        Time Complexity
            O(n)
        Space Complexity
            O(n)

            recursion height may be large if many subpatterns

        """
        s_new = []

        multiplier: int = 0

        while self.ix < self.n:

            char = s[self.ix]

            # Alphabet
            if char.isalpha():
                s_new.append(char)
                self.ix += 1

            # Add numbers as we get them
            # 0 = 0
            # 10 = 1*10 + 0 = 10
            # 101 = 10*10 + 1 = 101
            elif char.isdigit():
                multiplier = multiplier * 10 + int(char)
                self.ix += 1

            # Start of pattern
            elif char == '[':
                self.ix += 1
                pattern = multiplier * self.recurse2(s)
                s_new.extend(pattern)

                # Just added sub pattern, reset multiplier
                multiplier = 0

            # End of pattern ']'
            else:
                self.ix += 1
                break

        return s_new

    ''' Stack: Incomplete '''

    def stack(self, s: str) -> str:
        """

        Notes
            - stacks can be more efficient than strings for lots
            of appending. A new string may be created on each append

        # TODO: incomplete, but getting there

        """
        stack = []

        cur_pattern = []
        multiplier: int = 0

        for char in s:

            # Add numbers as we get them
            # 0 = 0
            # 10 = 1*10 + 0 = 10
            # 101 = 10*10 + 1 = 101
            if char.isdigit():
                multiplier = 10 * multiplier + int(char)

            # Starting a pattern
            elif char == '[':

                # Save the previous pattern and multipler if any
                stack.append(cur_pattern)
                stack.append(multiplier)

                # Start new pattern
                cur_pattern = []
                multiplier = 0

            # Closing a pattern
            elif char == ']':

                # Grab the multipler for the current pattern
                multiplier = stack.pop()
                prev_pattern = stack.pop()
                stack.extend(prev_pattern + multiplier * cur_pattern)

                # Reset mulitplier and pattern
                cur_pattern = []
                multiplier = 0

            # A normal character
            else:

                cur_pattern.append(char)

        # Join strings at the end
        s_new = ''.join(stack)

        return s_new


if __name__ == '__main__':

    # s_in = "3[a]2[bc]"
    s_in = "3[a2[c]]"

    obj = Solution()
    print(obj.stack(s_in))
    # print(obj.decodeString2(s_in))
