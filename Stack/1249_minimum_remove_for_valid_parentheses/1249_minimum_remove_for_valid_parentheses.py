"""

Inputs
    str: input string
Outputs
    str: string without bad parentheses

Ideas
    - seems similar to balanced parenthesis
        + use a stack to track balance
        + use a +1, -1 to determine balance
    
    - we need to know which parenthesis we need to remove
        + we need to know the location in the original index
        track the character and the index
Strategies
    
    Stack   
        
        - use a stack to track parentheses balance
            
            + track the character and index
        
        - after going through the string we will know
        if the string is balanced
            
            + if not balanced only the unbalanced parenthesis remain
        
        - go through the string again to recreate the string and ignore
        bad brackets
    
        Time Complexity
            O(n)
        Space Complexity
            O(n)
    

"""

class Solution:
    def minRemoveToMakeValid(self, s: str) -> str:

        # Maintain a stack of parenthesis and their location
        # in the original string
        
        stack = []
        
        for ix, char in enumerate(s):
            
            if char.isalpha():
                continue

            # print(f'char: {char}')
            # print(f'  stack: {stack}')
            
            # Save character and location
            if char == '(':
                stack.append((char, ix))
            
            # Right bracket ')' with matching close
            # Remove the matching bracket
            elif stack and stack[-1][0] == '(':
                stack.pop()

            # Right bracket ')' without matching close
            else:
                stack.append((char, ix))

        # print(f'stack: {stack}')
                
        # If the stack is empty at the end of this
        # all parenthesis are balanced
        if not stack:
            return s
        
        # Compile the bad indices
        skips = {ix for char, ix in stack} 
        
        # print(f'skips: {skips}')

        # Reset stack
        stack = []
        
        # Otherwise let's rebuild the string but without
        # the unbalanced parentheses
        for ix, char in enumerate(s):
            
            if ix in skips:
                continue
            else:
                stack.append(char)
        
        # Create string from stack
        return ''.join(stack)
