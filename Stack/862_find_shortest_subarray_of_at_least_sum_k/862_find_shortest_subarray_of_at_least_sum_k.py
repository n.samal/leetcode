"""
Inputs
    arr (List): unordered integers
Outputs
    int: length of shortest subarray with sum at least K
Notes
    - subarray must be non empty
    - contiguous
    - if non subarray of at least K, return -1
Ideas

    - use cumsum
    - from each cumsum search backwards
        + target summations

        cum_sum[j] - cum_sum[i] >= K

    - since values are not necessarily all positive
    we may not have a monotonically increasing cumsum
        + this may cause sliding window and binary
        search strategies to not work as usual

    - sliding window may not also work
        + while loop will fail because of changing cumsum

        it's increasing, then decreasing which breaks our check

        + to check all valid we'd need to check all n
        for each n ie: O(n^2)

    - guess and check
        + use binary search and sliding window checker
        + check to see if a valid window is possible
        + this also doesn't work

            * some smaller windows may not work (not enough values)
            * some larger windows may not work as well (inclusion of non desired values)
                arr = [84, -37, 32, 40, 95]
                K = 167

                size 2 subwindows result in
                [47, -5, 72, 135]

                size 4 subwindows result in
                [119, 130]

    - transform the problem to find our monotonically increasing array
        + we want to find some location where

            cum_sum[high] - cum_sum[low] >= K

        this can be rewritten as

            cum_sum[high] - K >= cum_sum[low]

        we want to find the first index where the cumsum is lower
        or equal to cum_sum[current] - K

        we could say that we want smaller values behind us

        cum_sum = [1, 2, 3, 4, 5]

        this is the same as monotonically increasing stack!

        we can track the indices in our stack to maintain a monotonically
        increasin subarray. Use those indices to track the best window

References:
    https://medium.com/@gregsh9.5/monotonic-queue-notes-980a019d5793
"""
from typing import List
from collections import deque


class Solution:

    """ Failed Methods """

    def original(self, arr: List[int], K: int) -> int:
        """


        """

        n: int = len(arr)
        ans: int = float('inf')

        # Compute cummulative sum (exclusive - inclusive)
        cum_sum = [0 for _ in range(n + 1)]
        for ix in range(1, n + 1):
            cum_sum[ix] = cum_sum[ix - 1] + arr[ix - 1]

        head_ix: int = 0
        tail_ix: int = 0

        while head_ix < n:

            if cum_sum[head_ix] - cum_sum[tail_ix] < K:
                head_ix += 1

            # Shrink window
            while tail_ix < head_ix and cum_sum[head_ix] - cum_sum[tail_ix] >= K:

                sum_ = cum_sum[head_ix] - cum_sum[tail_ix]
                print(f'sum: {sum_}')

                ans = min(ans, head_ix - tail_ix)
                tail_ix += 1

        # No answer found
        if ans < float('inf'):
            return ans
        else:
            return -1

    def binary_search(self, arr: List[int], K: int) -> int:
        """Guess and check

        - compute cumsum
        - try a subarray size, and see if it works

        Time Complexity
            O(n*ln(n))
        Space Complexity
            O(n)
        Notes
            - same strategy to largest duplicate substring 
        """
        n: int = len(arr)
        ans: int = float('inf')

        # Compute cummulative sum (exclusive - inclusive)
        cum_sum = [0 for _ in range(n + 1)]
        for ix in range(1, n + 1):
            cum_sum[ix] = cum_sum[ix - 1] + arr[ix - 1]

        # Binary search on window size
        # reduce size when we finding a working window
        low = 0
        high = n

        while low <= high:

            mid = low + ((high - low) // 2)
            check = self.check(cum_sum, mid, K)
            print(f'low: {low}   high: {high}   mid: {mid}  check: {check}')

            if check:
                high = mid - 1
                ans = min(ans, mid)
            else:
                low = mid + 1

        # No answer found
        if ans < float('inf'):
            return ans
        else:
            return -1

    def check(self, cum_sum: List[int], window_size: int, target_sum: int) -> bool:
        """Slide across the array and see if the target sum can be found

        Args:
            cum_sum: cummulative sum
            window_size: size of subarray window
            target_sum: minimum sum target

        Time Complexity
            O(n*)
        Space Complexity
            O(1)
        """

        n: int = len(cum_sum)

        for ix in range(window_size, n):

            sum_ = cum_sum[ix] - cum_sum[ix - window_size]

            if sum_ >= target_sum:
                return True

        return False

    """ Working """

    def two_loops(self, arr: List[int], K: int) -> int:
        """Double loop

        - compute cumsum
        - slide backwards for each index to find
        first subarray that meets our summation goals

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        """
        n: int = len(arr)
        ans: int = float('inf')

        # Compute cummulative sum (exclusive - inclusive)
        cum_sum = [0 for _ in range(n + 1)]
        for ix in range(1, n + 1):
            cum_sum[ix] = cum_sum[ix - 1] + arr[ix - 1]

            # Slide backwards to find first subarray
            for jx in range(ix - 1, -1, -1):

                if cum_sum[ix] - cum_sum[jx] >= K:
                    ans = min(ans, ix - jx)

        # No answer found
        if ans < float('inf'):
            return ans
        else:
            return -1

    def monotonic_q(self, arr: List[int], K: int) -> int:
        """Monotonically increasing stack

        - compute cummulative sum
        - use a monotonically stack
            + track where the cumsum is increasing
            + when the summation meets our target goal,
            squeeze the subarray down

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        n: int = len(arr)
        ans: int = float('inf')

        # Compute cummulative sum
        cum_sum = [0 for _ in range(n + 1)]
        for ix in range(1, n + 1):
            cum_sum[ix] = cum_sum[ix - 1] + arr[ix - 1]

        # Maintain a monotonic stack
        # only track indices
        queue = deque([])

        for ix, val in enumerate(cum_sum):

            if not queue:
                queue.append(ix)

            else:

                # Remove values that break our increasing sequence
                while queue and val <= cum_sum[queue[-1]]:
                    queue.pop()

                # Try shrinking our window
                # if we meet our target sum requirement
                while queue and val - cum_sum[queue[0]] >= K:
                    ans = min(ans, ix - queue[0])
                    queue.popleft()

                # Add current value
                queue.append(ix)

        # No answer found
        if ans < float('inf'):
            return ans
        else:
            return -1


if __name__ == '__main__':

    # Changing landscape (increasing & decreasing)
    arr = [84, -37, 32, 40, 95]
    K = 167

    # Failed case
    # arr = [2, -1, 2, 5, 7, 20301]
    # K = 3

    # Failed case
    # arr = [27, 20, 79, 87, -36, 78, 76, 72, 50, -26]
    # K = 453

    obj = Solution()
    # print(obj.original(arr, K))
    # print(obj.binary_search(arr, K))

    print(obj.two_loops(arr, K))
    print(obj.monotonic_q(arr, K))
