"""
Inputs
    s (string): absolute UNIX path
Outputs
    str: simplified (canonical path)

Notes
    - convert UNIX path to canonical
        + only 1 slash between names
        + no trailing /
        + the path must be the shortest path represenatable

    - '.' means current directory
    - '..' means up one level

Example

    Example 1:

        Input: "/home/"
        Output: "/home"
        Explanation: Note that there is no trailing slash after the last directory name.

    Example 2:

        Input: "/../"
        Output: "/"
        Explanation: Going one level up from the root directory is a no-op, as the root level is the highest level you can go.

    Example 3:

        Input: "/home//foo/"
        Output: "/home/foo"
        Explanation: In the canonical path, multiple consecutive slashes are replaced by a single one.

    Example 4:

        Input: "/a/./b/../../c/"
        Output: "/c"

    Example 5:

        Input: "/a/../../b/../c//.//"
        Output: "/c"

    Example 6:

        Input: "/a//b////c/d//././/.."
        Output: "/a/b/c"

Ideas

    - replace all '//' with '/'
    - split the string by '/'

"""

class Solution:
    def simplifyPath(self, path: str) -> str:
        """

        - split by '/'
        - save pieces into a stack if relevant
        - move upwards if a parent directory and available

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        n: int = len(path)
        ix: int = 0

        stack = []

        path = path.split("/")

        # print(path)

        for item in path:

            if item in ['.', ''] :
                pass

            elif item == '..':
                if stack: 
                    stack.pop()

            else:
                stack.append(item)

        # print(stack)

        ans = '/' + '/'.join(stack)
        return ans
