"""
Inputs
    s (str): balanced parentheses string
Outputs
    int: score of parentheses
Notes
    - () is score of 1
    - AB is score of A + B, where A & B are balanced parentheses
    - (A) is score 2 *A, where A is balanced parentheses

Examples

    Example 1:

        Input: "()"
        Output: 1
    Example 2:

        Input: "(())"
        Output: 2
    Example 3:

        Input: "()()"
        Output: 2

    Example 4:

        Input: "(()(()))"
        Output: 6

Ideas

    - maintain a count of
        + current score
        + global score

    - maintain a stack, and close items while possible
        + cascade prior scores as we close nesting parentheses 

    - recurse through the string
        + if the pattern is just () score = 1
        + otherwise we need to dive deeper to extract the pattern

        many simples

            ()()()

        simple with complex

            ()(())
            (())()

        complex
            (((())(()))))

"""


class Solution:

    def stack(self, S: str) -> int:
        """

        - use a stack to maintain the score for
        prior patterns, and the current pattern

        - when we start a new pattern, start the current score from zero

        - when closing the pattern

            + if we closed a complex pattern, score = 2 * prior
            + if we closed a simple pattern , score = 1
            + we add onto the parent pattern

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        stack = [0]     # score of overall pattern

        for char in S:

            # Start of a new pattern, reset score
            if char == '(':
                stack.append(0)

            # Close of current pattern, add score to parent pattern
            elif char == ')':

                prior = stack.pop()

                if not prior:
                    stack[-1] += 1
                else:
                    stack[-1] += prior * 2

        return stack[0]

    def recursive(self, s: str) -> int:
        """

        - if we see a () add 1 to our score
        - if we see something else, then our
        pattern is more complicated, dig deeper
            + once that sub pattern emerges, then multiply
            that score by 2
            + break our curent pattern if it's only a ')'

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        n: int = len(s)
        self.ix: int = 0

        def recurse() -> int:

            score = 0

            while self.ix < n:

                if s[self.ix] == '(' and s[self.ix + 1] == ')':
                    score += 1
                    self.ix += 2

                elif s[self.ix] == ')':
                    self.ix += 1
                    break

                else:
                    self.ix += 1
                    score += recurse() * 2

            return score

        score = recurse()

        return score


if __name__ == '__main__':

    # Example 2 (2)
    # s = "(())"

    # Example 3 (2)
    # s = "()()"

    # Test (3)
    # s = "()(())"

    # Test (4)
    # s = "(()())"

    # Example 4 (6)
    # s = "(()(()))"

    # Test (3)
    # s = "(())()"

    obj = Solution()
    # print(obj.stack(s))
    print(obj.recursive(s))
