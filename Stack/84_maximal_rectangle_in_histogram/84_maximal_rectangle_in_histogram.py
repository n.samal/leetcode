from typing import List

"""

Inputs
    List(int): non negative integers representing the height of each bar
Outputs
    int: area of the largest rectangle in the histogram 
Goal
    - find the area of the largest rectangle in the histogram

Cases

    Null
        arr = []
    Single Value
        arr = [6]
    Constant Value
        arr = [1, 1, 1, 1, 1]
    Increasing
        arr = [1, 1, 2, 2, 3, 4, 6]
    Decreasing
        arr = [10, 9, 7, 5, 4, 3, 1]
    Mixed
        arr = [2, 1, 5, 6, 2, 3]

Strategy

    Manual
        - at each bar
        - move to the left until we find a smaller bar
        - move to the right until we find a smaller bar
        - compute the area for using the left and right bounds

        - be careful with the bounds for the area calculation

            + if no movement to either side we still need to account for
            the current bar. ie: ((right_ix - left_ix) + 1) * current_height

    Ideas

        - what data structure tracks the next smallest values to the right and left?
            + a monotonically increasing stack!
            + add values to our stack if increasing or equal to the previous stack value
            + remove values from our stack smaller than the current value

            + this guarentees nothing smaller to the right
            ie: [1, 2, 3, 4, 5]
            + as we remove values from the stack we'll know where the next
            smaller value is 
                ie: the previous value is our left bound
                    the value kicking us out is the right bound

            + if the stack is empty, that means there are no smaller values
            to the left

    Notes
        - when referencing the smallest prior value be careful with the indices
            + **are the indices inclusive or exclusive or one of each?**

        - if using inclusive lower bounds, we can use -1 as a reference
        to account for the correct width

        Inclusive Indices

            - indices both mean we include the column

            ie:
                arr = [0, 1, 2, 3, 4]

                inclusive right_ix = 4
                inclusive left_ix = 0

                means we want values from index 4 to 0

                if computing the width for this array with inclusive indices
                we must add 1

                count = 5
                correct width = 4 - (-1) = 5

        Inclusive upper, Exclusive lower

            - upper index means we include,
            - the lower index indices we don't include this

            ie:
                arr = [0, 1, 2, 3, 4]

                inclusive right_ix = 4
                exclusive left_ix = 0

                means we want values from index 4 to 1

                width = right_ix - left_ix
                      = 4 - 0

                this is correct

    Monotonically Increasing Stack

        - initialize an empty stack
        - iterate through the array from left to right

        - add the (index, value) to the array if nothing,
        or previous stack value is smaller or equal

        ie: stack = [1, 1, 2]
            value = 3
            stack = [1, 1, 2, 3]

        - for smaller values

        ie:
            arr = [2, 5, 4, 5, ...]

            stack = [2, 5]
            value = 4
            bar of 5 can't be extended to the right, remove 5 and calculate area

            stack = [2]
            value = 4
            value 4 larger or equal to 2, can be added
            stack = [2, 4]

            stack = [2, 4]
            value = 5
            stack = [2, 4, 5]

            * When calculating the area for '4' extend to the right while values are increasing
            Also remember to extend to the left until a smaller value is found,
            not just the index!

            We first removed the 5 adjacent to 2 ie: [2, 5, 4, 5]
            the '4' can extend left until it hits the '2'

            Reference: https://www.geeksforgeeks.org/largest-rectangle-under-histogram/

        - the decreasing case helps us understand the indices for a regular
          unload

            ie:
            arr =[10, 8, 8, 7, 5, 3]
            ix = [0 , 1, 2, 3, 4, 5]

            index: 0
                stack = []
                value = 10

            index: 1
                stack = [(10, 0)]
                value = 8

                must unload stack, until no larger values exist, stack => []
                height = 10
                no prior smaller value exist, use -1
                shift the right_index by 1 since we're past the value
                width = (current index - 1) - (previous smaller)
                      = (1 - 1) - (-1) = 1

            index: 2
                stack = [(8, 1)]
                value = 8

            index: 3
                stack = [(8, 1), (8, 2)]
                value = 7

                must unload stack until no larger values exist, stack => [(8,1)]
                height = 8
                width = (current index - 1) - (previous smaller)
                      = (3 - 1) - 1
                      = 2 - 1
                      = 1

                must unload stack until no larger values exist, stack => []
                height = 8
                there's no previous smallest index, use 0
                width = (current index - 1) - (previous smaller)
                      = (3 - 1) - None
                      = 2 - (-1)
                      = 3

        - the increasing case with help us understand the stack unload at
          the end

            ie:
            arr = [4, 5, 6, 7, 7, 8]
            ix =  [0, 1, 2, 3, 4, 5]

            let's calculate with
            inclusive upper, exclusive lower

            height = 8
            width = current index - previous smaller
            width = 5 - 4 = 1

            height = 7
            width = current index - previous smaller
            width = 5 - 3 = 2

            height = 7
            width = current index - previous smaller
            width = 5 - 2 = 3

            height = 6
            width = current index - previous smaller
            width = 5 - 1 = 4

            height = 5
            width = current index - previous smaller
            width = 5 - 0 = 5

            height = 5
            width = current index - previous smaller
            width = 5 - None? =

            the 0th index has no prior
            so when calculating the width we must add 1
            width = current index - (-1)
            width = 5 + 1 = 6
References:
    https://leetcode.com/problems/shortest-subarray-with-sum-at-least-k/discuss/204290/Monotonic-Queue-Summary
"""


class Solution:
    def stack_indices_output(self, arr: List[int]) -> int:
        """Use a monotonically increasing stack with output

        - add larger values to our stack
        - remove values smaller then the current value

        - as we remove values calculate the area

        Args:
            arr: heights of each bar
        Returns:
            int: maximum rectangle area possible

        """
        max_area: int = 0
        n: int = len(arr)

        # No values
        if not arr:
            return max_area

        # Single value
        elif n == 1:
            return arr[0]

        stack = []

        for ix in range(n):

            stack_vals = [arr[ix] for ix in stack]

            print(f'value: {arr[ix]:<15} stack: {stack_vals}')

            if stack:

                # Remove larger values in our stack
                # ie: stack_values = [1, 4, 5], value = 3
                while stack and arr[stack[-1]] > arr[ix]:

                    ix_val = stack.pop()

                    stack_vals = [arr[ix] for ix in stack]

                    # Grab the location we saw a value smaller than this
                    # note: our current index is one past it's limit, we must
                    # adjust that by one
                    if stack:
                        ix_left = stack[-1]
                        width = (ix - 1) - ix_left
                    else:
                        ix_left = 0
                        width = ix - ix_left

                    # Compute area: width * height
                    area = width * arr[ix_val]
                    max_area = max(area, max_area)

                    print(f'   removing value: {arr[ix_val]}')
                    print(f'   stack: {stack_vals}')
                    print(f'   area: {area}\n')

            # Add to the stack
            stack.append(ix)

        stack_vals = [arr[ix] for ix in stack]
        print(f'stack: {stack_vals}')

        # Remove remaining values from the stack
        # stack_values = [1, 1, 2, 2, 4, 5]
        # right bound is ix = (n - 1)
        # left bound is index of value
        while stack:

            ix_val = stack.pop()

            if stack:
                ix_left = stack[-1]
                width = ix - ix_left
            else:
                ix_left = -1
                width = ix - ix_left

            # Compute area: width * height
            area = width * arr[ix_val]
            max_area = max(area, max_area)

        return max_area

    def stack_indices(self, arr: List[int]) -> int:
        """Use a monotonically increasing stack without output

        - add larger values to our stack
        - remove values smaller then the current value

        - as we remove values calculate the area

        Args:
            arr: heights of each bar
        Returns:
            int: maximum rectangle area possible

        """
        max_area: int = 0
        n: int = len(arr)

        # No values
        if not arr:
            return max_area

        # Single value
        elif n == 1:
            return arr[0]

        stack = []

        for ix in range(n):

            if stack:

                # Remove larger values in our stack
                # ie: stack_values = [1, 4, 5], value = 3
                while stack and arr[stack[-1]] > arr[ix]:

                    ix_val = stack.pop()

                    # Grab the location we saw a value smaller than this
                    # note: our current index is one past it's limit
                    # we must adjust that by one ie: ix - 1
                    if stack:
                        width = (ix - 1) - stack[-1]
                    else:
                        width = ix

                    # Compute area: width * height
                    area = width * arr[ix_val]
                    max_area = max(area, max_area)

            # Add to the stack
            stack.append(ix)

        # Remove remaining values from the stack
        # stack_values = [1, 1, 2, 2, 4, 5]
        # right bound is ix = (n - 1)
        # left bound is index of value
        while stack:

            ix_val = stack.pop()

            if stack:
                ix_left = stack[-1]
                width = ix - ix_left
            else:
                ix_left = -1
                width = ix - ix_left

            # Compute area: width * height
            area = width * arr[ix_val]
            max_area = max(area, max_area)

        return max_area

    def brute_force(self, arr: List[int]) -> int:
        """

        - at each position
        - slide to the left until we hit a smaller bar
        - slide to the right until we hit a smaller bar
        - compute the area

        Time Complexity
            O(n^2)
        Space Complexity
            O(1)

        Args:
            arr: heights of each value in the histogram
        Returns:
            maximal rectangle
        Notes:
            too slow
        """

        max_area: int = 0
        n: int = len(arr)

        if not arr:
            return max_area

        for ix in range(n):

            left_ix: int = ix
            right_ix: int = ix

            # Slide to the left
            while left_ix > 0 and arr[left_ix - 1] >= arr[ix]:
                left_ix -= 1

            # Slide to the right
            while right_ix < (n - 1) and arr[right_ix + 1] >= arr[ix]:
                right_ix += 1

            # Compute the area with current height
            # ie: ((ix-ix) + 1) * height
            area = ((right_ix - left_ix) + 1) * arr[ix]
            max_area = max(area, max_area)

        return max_area


if __name__ == "__main__":

    # Geeks case
    arr = [6, 2, 5, 4, 5, 1, 6]
    # arr = [2, 1, 5, 6, 2, 3]

    # Null & single case
    # arr = []
    # arr = [6]

    # Two value
    # arr = [0, 9]

    # Increasing
    # arr = [1, 2, 3, 4, 5, 6]
    # arr = [1, 2, 2, 4, 4, 6]

    # Constant
    # arr = [1, 1, 1, 2, 2]

    arr = [4, 2]

    obj = Solution()
    print('brute_force :', obj.brute_force(arr))
    print('stack_output:', obj.stack_indices_output(arr))
    print('stack_clean :', obj.stack_indices(arr))
