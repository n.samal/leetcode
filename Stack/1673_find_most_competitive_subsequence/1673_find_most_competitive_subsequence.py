"""
Inputs
    arr (List[int]): numbers in unknown order
    k (int): desired subsequence length
Outputs
    List[int]: the most competitve sequence aka the smallest subsequence

Notes
    - competive subsequences are those that are smaller
    when comparing value by value
        + ie: [1, 3, 4] is better than [1, 3, 5]

    - in essence we value the smallest subsequence value 
    of length k

Examples

    Example 1:

        Input: nums = [3,5,2,6], k = 2
        Output: [2,6]
        Explanation: Among the set of every possible subsequence: {[3,5], [3,2], [3,6], [5,2], [5,6], [2,6]}, [2,6] is the most competitive.

    Example 2:

        Input: nums = [2,4,3,3,5,4,9,6], k = 4
        Output: [2,3,3,4]

Ideas

    - first find the smallest value from 0 to n - k
        + use this value as our initial value
        + build all potential k length arrays from this point
        + sorted the values, and grab the smallest

        + works but is slow

    - monotonically increasing stack?
        + we want to maintain a motonically increasing stack
        while we still can create a k length array

        + this ensures our first value is always the smallest, 
        second value is the second smallest

        + we need to place limits on the dequeing of values

"""

from typing import List


class Solution:

    ''' Backtracking '''

    def mostCompetitive_backtrack(self, arr: List[int], k: int) -> List[int]:
        """

        - find the smallest starting value
        - backtrack through paths starting with the smallest value
        - sort paths found of size k
        - grab the 1st path

        Time Complexity
            O(!n)?
        Space Complexity
            O(k) recursion height
        """

        min_v = float('inf')
        n: int = len(arr)

        # Find the smallest value
        for ix in range(n - k):
            min_v = min(min_v, arr[ix])

        # Find where these values occur
        indices = []

        for ix in range(n - k):

            if arr[ix] == min_v:
                indices.append(ix)

        # Back track from here
        self.ans = []
        self.visited = [False for _ in range(n)]
        self.backtrack(0, arr, [], k)

        return sorted(self.ans)[0]

    def backtrack(self, ix: int, arr: List[int], path: List[int], k: int):
        """

        - iterate through possible combinations

        Args:
            ix (int): starting index
            arr (List[int]): numbers
            path (List[int]): curent path
            k (int): path limit
        """

        if len(path) == k:
            self.ans.append(path)
        else:

            for jx in range(ix, len(arr)):
                if self.visited[jx] is False:
                    self.visited[jx] = True
                    self.backtrack(jx + 1, arr, path + [arr[jx]], k)
                    self.visited[jx] = False

    ''' Stack '''

    def stack_based(self, arr: List[int], k: int) -> List[int]:
        """Try to maintain a monotonic stack

        - remove larger values only if there's more
        numbers left to create a path of size k

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        n: int = len(arr)
        stack = []

        # Add the smallest values to our stack
        for ix in range(n):

            values_left = n - ix

            while stack and arr[ix] < stack[-1] and len(stack) + values_left > k:
                stack.pop()

            stack.append(arr[ix])

        # Grab the first k values
        return stack[:k]


if __name__ == '__main__':

    # Example 1
    arr_in = [3, 5, 2, 6]
    k_in = 2

    # Example 2
    arr_in = [2, 4, 3, 3, 5, 4, 9, 6]
    k_in = 4

    obj = Solution()
    print(obj.stack_based(arr_in, k_in))