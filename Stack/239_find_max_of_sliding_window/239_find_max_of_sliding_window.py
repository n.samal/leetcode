"""
Inputs
    arr (List[int]): numbers
    k (int): size of window
Outputs:
    List[int]: find the max value in each sliding window

Example

    Window position                Max
    ---------------               -----
    [1  3  -1] -3  5  3  6  7       3
     1 [3  -1  -3] 5  3  6  7       3
     1  3 [-1  -3  5] 3  6  7       5
     1  3  -1 [-3  5  3] 6  7       5
     1  3  -1  -3 [5  3  6] 7       6
     1  3  -1  -3  5 [3  6  7]      7

Ideas

    - maintain a queue of values
        + add new values
        + remove the older values

    - use a heap to find the max value quickly?

    - if we add a new value and it's bigger than our previous max
        + we can remove all the previous numbers

        org: [1, 2, 3]   new_val = 4
        new: [2, 3, 4]

        4 will be the max until we find a larger value, or it gets
        kicked out of the sliding window

        new: [2, 3, 4]
        new: [4]

        when we get another larger number, remove smaller numbers
        then add new value

        new: [4]   new_val = 5
        new: [5]

        if we get a smaller number, add it
        new: [5]            new_val = 4
        new: [5, 4]         new_val = 3
        new: [5, 4, 3]

        the largest value is at the zero position, until it gets kicked out
        then the next value is available.

    - track indices instead of values??
        + easier to track unique values

Hand Calc

    nums = [1, 3, 1, 2, 0, 5]   k = 3

    Original k
    new_val = 1
    queue_old = []
    queue_old = [1]

    new_val = 3
    queue_old = [1]
    queue_new = [3]

    new_val = 1
    queue_old = [3]
    queue_new = [3, 1]

    General
    new_val = 2
    queue_old = [3, 1]
    queue_new = [3, 2]

    new_val = 0
    queue_old = [3, 2]
    queue_new = [2, 0]

References:
    https://docs.python.org/3/library/heapq.html
"""


from collections import deque
from typing import List


class Solution:

    def naive(self, arr: List[int], k: int) -> List[int]:
        """

        Time Complexity
            O(n*k)
        Space Complexity
            O(k)
        Notes
            - how can we use the information of our old max
            for the new calculation?
        """

        # Start with first k
        queue = deque(arr[:k])
        maxes = [max(queue)]

        # Add new value, remove old
        for val in arr[k:]:

            queue.popleft()
            queue.append(val)

            max_i = max(queue)
            maxes.append(max_i)
            print(f'max: {max_i}')

        return maxes

    def max_deque(self, arr: List[int], k: int) -> List[int]:
        """Queue of indices

        - maintain a queue of indices
            + maintain a monotonically decreasing stack

        - for new values
            + remove all older values smaller than this since
            they won't contribute to the max
            + this places the current max at position 0 in our queue
            + smaller values may become our max eventually

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        n: int = len(arr)
        queue = deque([])
        maxes = []

        def update_queue(ix: int):

            # Remove values outside of our window
            # [2, 3, 4] => [3, 4, 5]
            if queue and queue[0] <= ix - k:
                queue.popleft()

            # Remove values smaller than our newest addition
            while queue and arr[queue[-1]] < arr[ix]:
                queue.pop()

        # Grab first k, and keep the max
        for ix in range(k):
            update_queue(ix)
            queue.append(ix)

        # Save the max value
        maxes.append(arr[queue[0]])

        # Go over remaining values
        for ix in range(k, n):
            update_queue(ix)
            queue.append(ix)

            maxes.append(arr[queue[0]])

        return maxes


if __name__ == '__main__':

    # Example 1
    # nums = [1, 3, -1, -3, 5, 3, 6, 7]
    # k = 3

    nums = [1, 3, 1, 2, 0, 5]
    k = 3

    obj = Solution()
    print(obj.naive(nums, k))
    print(obj.max_deque(nums, k))
