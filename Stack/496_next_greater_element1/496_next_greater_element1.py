"""

Inputs
    arr1 (List[int]): array of unknown order (no duplicates)
    arr2 (List[int]): array of unknown order (no duplicates)
Outputs
    arr: first greater number to the right for each index
Goal:
    for each value in arr1 
        - find the first greater number to the right in arr2
        - if no greater value => -1

Ideas
    - use a stack for tracking the next greater element.
        + will likely need to use a monotonically decreasing stack

Cases
    
    Null, Null

    Null, Array

    Array Null

    Increasing

    Decreasing

    General


Strategies

    Brute Force

        - for each value in arr1, find where the value exists in arr2
        - then search for the next greatest value

        Time Complexity
        O(n*m)

        Space Complexity
        O(n)

    Stack

        - use a monotonically decreasing stack to iterate through arr2

        ie: [5, 6, 7, 8, 9]

        stack: []
        index: 0
        value: 5

        stack: [5]
        index: 1
        value: 6

            remove smaller values, and track what the next larger value
            which is our current number ie: 6

        - since each target number is unique we can use a hash to store
        the next greater value for each target

        if we encounter a great value it will trigger a pop of the previous stack values


"""

from typing import List


class Solution:

    def brute_force(self, arr1: List[int], arr2: List[int]) -> List[int]:
        """Brute for calculation

        - for every arr1 value search for it in arr2
        - once we find the value, search for the next larger value

        Args:
            arr1: target values in unknown order
            arr2: array of reference values in unknown order
        Returns:
            next greater values
        """
        n: int = len(arr1)
        m: int = len(arr2)

        ans = [-1 for ix in range(n)]

        # Main array
        for ix in range(n):

            search = False

            for jx in range(m):

                # Found the next greater element
                if search and arr2[jx] > arr1[ix]:
                    ans[ix] = arr2[jx]
                    break

                # Found the element in arr2
                if arr2[jx] == arr1[ix]:
                    search = True
        return ans

    def stack(self, arr1: List[int], arr2: List[int]) -> List[int]:
        """Use monotonically decreasing stack to track when
        larger values occur

        Time Complexity
            O(n + m)

            creating stack = O(m)
            creating ans = O(n)

        Space Complexity
            O(n + m)

            storing stack = O(m)
            storing ans = O(n)

        Returns:
            next greater value for each value in arr1
        """

        d = {}
        stack = []

        # Go through secondary array
        for val in arr2:

            # Found a greater value in the stack
            while stack and val > stack[-1]:

                # Remove smaller value and save it's next greater value
                prev = stack.pop()
                d[prev] = val

            # Save our new number
            stack.append(val)

        # Go through targets
        # stored value if there, -1 if not there
        ans = [d.get(target, -1) for target in arr1]

        return ans


if __name__ == "__main__":

    # Example 1
    # arr1 = [4, 1, 2]
    # arr2 = [1, 3, 4, 2]

    # Example 2
    arr1 = [2, 4]
    arr2 = [1, 2, 3, 4]

    obj = Solution()
    print(obj.brute_force(arr1, arr2))
    print(obj.stack(arr1, arr2))
