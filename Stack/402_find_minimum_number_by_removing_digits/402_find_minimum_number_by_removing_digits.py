"""
Inputs
    num (str): number represented as a string
    k (int): number of digits to remove
Outputs
    str: smallest number possible
Notes
    - the number does not contain any leading zeros

Examples

    Example 1:

        Input: num = "1432219", k = 3
        Output: "1219"
        Explanation: Remove the three digits 4, 3, and 2 to form the new number 1219 which is the smallest.

    Example 2:

        Input: num = "10200", k = 1
        Output: "200"
        Explanation: Remove the leading 1 and the number is 200. Note that the output must not contain leading zeroes.

    Example 3:

        Input: num = "10", k = 2
        Output: "0"
        Explanation: Remove all the digits from the number and it is left with nothing which is 0.

Cases

    more k than the number
        - remove all the digits

"""

class Solution:

    def brute_force(self, num: str, k: int) -> str:
        """Try removing every digit

        - try removing each digit and track
        the smallest value possible
        - continue the removal k times

        Time Complexity
            O(n*k)
        Space Complexity
            O(1)
        """

        # Remove k digit to max to make the
        # number as small as possible

        # Try remove a digit in 3 rounds to minmize the number

        if k > len(num):
            return '0'

        for _ in range(k):

            # Number with value removed
            min_val = '99999999999999999'

            for ix in range(len(num)):

                val = num[:ix] + num[ix + 1:]

                min_val = min(min_val, val)

            # This is our new value
            num = min_val

        # Strip leading zeros
        num = num.lstrip("0")

        if not num:
            return '0'
        else:
            return num

    def stack(self, string: str, k: int) -> str:
        """Maintain monontically increasing stack

        - the smallest number is montonically increasing
            [1, 2, 3, 4, 5]

        - remove larger values while we have more digits
        to remove
        - strip leading zeros

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        if k >= len(string):
            return '0'

        stack = []

        for char in string:

            if not stack:
                stack.append(char)

            elif char >= stack[-1]:
                stack.append(char)

            else:

                while k and stack and char < stack[-1]:
                    stack.pop()
                    k -= 1
                stack.append(char)

        # Remove extra values
        # ie: '11234567', 2
        while k:
            stack.pop()
            k -= 1

        # Remove leading zeros
        stack = ''.join(map(str, stack))
        stack = stack.lstrip('0')

        if not stack:
            return '0'
        else:
            return stack
