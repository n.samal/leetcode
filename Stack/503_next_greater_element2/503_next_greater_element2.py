"""

Inputs
    nums (List[int]): values in unknown order
Outputs
    List[int]: the next greater element to the right

Goals
    - find the right next greater element to the right

        + the array is circular, so search the beginning of the array
        for greater elements as well if nothing is found on the right
        + output -1 if no greater element exists

Notes
    - using a flag to determine whether to search circularly
        + ans[ix] == -1 can be incorrect, when the resulting greater
        value is -1

Ideas
    - use of a stack to find the next greater element to the right
        + monontically decreasing stack will tell us the next
        largest value or a number of elements

        + this portion is similar to Next Greater Element v1

    - how to account for circular array?

Strategies

    Brute Force

        - for each value, search the right for a greater value
            + if we find it, then save it
            + if we reach the end without a solution, search
            the left portion of the array ie: circular portion

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)

    Stack

        - use a montonically decreasing stack to great the next greatest
        value to the right

        - if we reach the end of the array and there are remaining values in
        the stack then we need to search circularly for an answer

        - we can use the same type of montonically decreasing stack to
        track values for a next pass
            + no need to save previous values, just search for the next
            greater element

        - any remaining values should have no greater element

Example

    arr = [3, 8, 4, 1, 2]
    ix  = [0, 1, 2, 3, 4]
    ans = [8, -1, 8, 2, 3]

    ix: 0
    stack = []
    value = 3

    ix: 1
    stack = [3]
    value = 8

        8 > 3: pop off 3

    ix : 2
    stack = [8]
    value = 4

    ix : 3
    stack = [8, 4]
    value = 1

    ix : 4
    stack = [8, 4, 1]
    value = 2

        2 > 1: pop off 1

    Second Pass
    ix : 0
    stack = [8, 4]
    value = 8
        8 > 4: pop off 8

    rest of array has no greater, which does nothing

    Last Pass
    stack = [8]

    saving remaining as having -1 as greater element



"""

from typing import List


class Solution:
    def brute_force(self, arr: List[int]) -> List[int]:
        """Brute force solution

        - look for greater values to the right
        - if nothing found, start searching from the beginning

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        """

        n: int = len(arr)
        ans = [-1 for ix in range(n)]

        for ix in range(n):

            found: bool = False

            # Iterate through right
            for jx in range(ix + 1, n):

                # Found a greater element
                if arr[jx] > arr[ix]:
                    ans[ix] = arr[jx]
                    found = True
                    break

            # Iterate through left subarray if nothing found yet
            if not found:

                for jx in range(ix):
                    if arr[jx] > arr[ix]:
                        ans[ix] = arr[jx]
                        break
        return ans

    def stack_output(self, arr: List[int]) -> List[int]:
        """Montonically decreasing stack with output for debugging

        - first pass with stack finds next greater to the right
        - second pass with a stack will find greater values starting
        from beginning
        - last pass shows finds values with no greater element

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        n: int = len(arr)
        ans = [None for ix in range(n)]
        stack = []
        stack_vals = []

        ''' First pass '''

        print('\nFirst Pass')
        for ix in range(n):

            print(f'value: {arr[ix]}    stack: {stack_vals}')

            # Current value is greater than stack
            while stack and arr[ix] > arr[stack[-1]]:

                prev_ix = stack.pop()
                prev_val = stack_vals.pop()

                # Save the current value as the next greatest
                # for the previous index
                ans[prev_ix] = arr[ix]

                print(f'    removed: {prev_val}')
                print(f'    stack  : {stack_vals}')

            # Add index to the stack
            stack.append(ix)
            stack_vals.append(arr[ix])

        ''' Second Pass '''

        print('\nSecond Pass')
        for ix in range(n):

            print(f'value: {arr[ix]}    stack: {stack_vals}')

            # Current value is greater than stack
            while stack and arr[ix] > arr[stack[-1]]:

                prev_ix = stack.pop()
                prev_val = stack_vals.pop()

                # Save the current value as the next greatest
                # for the previous index
                ans[prev_ix] = arr[ix]

                print(f'    removed: {prev_val}')
                print(f'    stack  : {stack_vals}')

        print(f'stack: {stack_vals}')

        # Remaining values have no greater value
        while stack:

            ix = stack.pop()
            ans[ix] = -1

        return ans

    def stack(self, arr: List[int]) -> List[int]:
        """Montonically decreasing stack with output for debugging

        - first pass with stack finds next greater to the right
        - second pass with a stack will find greater values starting
        from beginning
        - last pass shows finds values with no greater element

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        n: int = len(arr)
        ans = [None for ix in range(n)]
        stack = []

        ''' First pass '''

        for ix in range(n):

            # Current value is greater than stack
            while stack and arr[ix] > arr[stack[-1]]:

                prev_ix = stack.pop()

                # Save the current value as the next greatest
                # for the previous index
                ans[prev_ix] = arr[ix]

            # Add index to the stack
            stack.append(ix)

        ''' Second Pass '''

        for ix in range(n):

            # Current value is greater than stack
            while stack and arr[ix] > arr[stack[-1]]:

                prev_ix = stack.pop()

                # Save the current value as the next greatest
                # for the previous index
                ans[prev_ix] = arr[ix]

        # Remaining values have no greater value
        while stack:

            ix = stack.pop()
            ans[ix] = -1

        return ans


if __name__ == "__main__":

    # Example 1
    # arr = [1, 2, 1]
    # arr = [1, 2, 0, 5, 3]
    arr = [3, 8, 4, 1, 2]

    # Test case
    # arr = [1, 8, -1, -100, -1, 222, 1111111, -111111]

    obj = Solution()
    # print(obj.brute_force(arr))
    # print(obj.stack_output(arr))
    print(obj.stack(arr))
