"""
Inputs
    points (List[List[int]]): x y locations of points
Outputs
    int: minimum area of a rectangle
Notes
    - if no rectangle possible, output 0

Examples

    Example 1

        Input: [[1,1],[1,3],[3,1],[3,3],[2,2]]
        Output: 4

    Example 2

        Input: [[1,1],[1,3],[3,1],[3,3],[4,1],[4,3]]
        Output: 2

Ideas
    - distance doesn't tell us if points are parallel or not

    - keep track of points at common x and y locations
        const_x: {1: index 0, index 1}
        const_y: {1: index 0, index 2}

        + referenceing may be easier to just store the y value instead of the index

    - sorting points may help us maintain order of points
        + we want to grab the points closest in x to us first
        + also the points closest in y
        + start small go bigger

    - area calc
        + dx * dy

    - find two points at same x, and different y
            x1, y1
            x2, y2
            x1 = x2
        + then find another two points at different x and same y
        + Point 3 different x, but same y as Point 1
            x3, y3

            y1 = y3

        + Point 4 different x, but same y as Point 2

            x4, y4

            x3 = x4
            y2 = y4

    - checking for points will be easier with set or hash
        O(1) checks.
        + we need the (x,y) values to verify this

    - search for another x location with the same y could be expensive
        + find a point at the same y as y1, 
        + then search that list of points for the corresponding x location, but 
        not the one we're currently at
        + then if that works, find see if there exists another point

        to complete the rectangle

        same x as Point 3 and same y as Point 2

    - may be much easier to organize points as pairs at constant x
    or constant y

        x = 1.0: {(y1, y2), (y1, y3), (y1, y4)}
        x = 2.0: {(y1, y2),           (y1, y4)}

        then search for point pairs at the same heights
"""

from collections import defaultdict

class Solution:

    def double_loop(self, points: List[List[int]]) -> int:
        """

        - save points into a set for easy referencing
        - iterate over all pairs of points
            + search for complimenting points


            point1 = (x1, y1)
            point2 = (x2, y2)

            point3 = (x1, y2)  same y as point 2
            point4 = (x2, y1)  same y as point 1

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        """

        ans = float('inf')

        n: int = len(points)
        s = set([(x, y) for x, y in points])


        for ix in range(n):
            for jx in range(ix + 1, n):

                x1, y1 = points[ix]
                x2, y2 = points[jx]

                # Search for complementary points
                # only save areas greater than 0
                if (x2, y1) in s and (x1, y2) in s:

                    area = abs(x2 - x1) * abs(y2 - y1)

                    if area:
                        ans = min(ans, area)

        if ans == float('inf'):
            return 0
        else:
            return ans
    
    def double_loop2(self, points: List[List[int]]) -> int:
        """

        - aggreate points by common x or y coordinate
        - iterate over point in constant x

        - search for complimenting points at the same ys
        but a differet x

            + save the area of rectangles created

        - used sorting to maintain expected ordering of point
        coordinates

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)

        """

        ans = float('inf')

        ''' Aggregate points by common x '''
        const_x = defaultdict(list)

        for x, y in points:
            const_x[x].append(y)

        ''' Iterate over constant x pairs'''

        prev_x = {}  # store x location for pairs of points

        for x in sorted(const_x):
            n: int = len(const_x[x])

            # Sort by y value
            const_x[x].sort()

            for ix in range(n):
                for jx in range(ix + 1, n):

                    y1 = const_x[x][ix]  # (x1, y1) smaller y
                    y2 = const_x[x][jx]  # (x1, y2) larger y

                    dy = y2 - y1

                    # Find points at the same y locations but at a different x
                    if (y1, y2) in prev_x:
                        dx = x - prev_x[(y1, y2)]
                        ans = min(ans, dx * dy)

                    # Save the x location for every pair of y points
                    prev_x[(y1, y2)] = x

        if ans != float('inf'):
            return ans
        else:
            return 0
