"""
Inputs
    value (int): 32 bit integer
Outputs
    int: next greater value with the same digits as input

Cases

    All the same digits

    Multiple of the same digit

    Different Digits

Strategies

    Combinations

        - this seems similar to the next greatest time

        - find all digits in the integer

        digits_str = str(value)
        digits = [int(char) for char in digits]

        - sort the digits from smallest to largest
        - create all possible combinations with these digits

            + break the loop if the combination is greater than
            the input value

        Time Complexity
            O(!n) number of combinations possible with same digits
        Space Complexity
            O(1) if a generator
            O(same as above) if not a generator

    Next Greater Value
        - similar to finding next greater permutation
        - create an array of values representing each digit
        - if the array is sorted in decreasing order (right to left)
            or increasing order from left to right

            ie: [5, 4, 3, 2, 1]

            there is no larger value

        - starting from the right find the first location where values
        are not increasing

            ie: [5, 4, 1, 3, 2]

            here we see a decreasing from 3 -> 1

        - if we put the next largest value here we can increase the number at this position
            + we cannot use any number, but just the ones on the right side
            + if we use any number this will modify the left side as well 

            ie: 
            orig: [5, 4, 1, 3, 2]
            new : [5, 4, 2, 3, 1]

            this current order is not yet ideal

        - with the remaining values to the right, order them in decreasing
         order (smallest value possible)

            ie:
            orig: [5, 4, 1, 3, 2]
            new : [5, 4, 2, 3, 1]
            new : [5, 4, 2, 1, 3]
        

References
    - https://stackoverflow.com/questions/13905936/converting-integer-to-digit-list

"""

import itertools


class Solution:

    def permutations(self, value: int) -> int:
        """Create all permutations with the given digits

        Notes
            - permuation is when order matters
            - combination is when order does not matter
        """

        # Grab digits at each place
        digits_str = str(value)
        digits = [int(char) for char in digits_str]
        digits = sorted(list(digits))

        # Go through all permutations (order of digits matters)
        for comb_i in itertools.permutations(digits):

            # Convert tuple of digits into a value
            value_str = ''
            for digit in comb_i:
                value_str += str(digit)

            value_new = int(value_str)

            if value_new > value:
                return value_new

        # Ensure the value is a 32 bit integer
        if value_new > 2**31:
            return -1
        else:
            return value_new

    def next_greater(self, value: int) -> int:
        """Find where the value decreases

        - find the position where values decrease
        - find the value just larger than the value to the right
        - swap the current position with that next greater value
        - sort the right subarray to be decreasing

        Notes
            - permuation is when order matters
            - combination is when order does not matter
        """

        # Grab digits at each place
        digits_str = str(value)
        digits = [int(char) for char in digits_str]

        n: int = len(digits)

        # Single digit, no greater value
        if n == 1:
            return -1

        # Iterate from right to left to find where values decrease
        found: bool = False
        for ix in range(n-2, -1, -1):

            # Decreasing in value
            if digits[ix] < digits[ix + 1]:
                found = True
                break

        # If no such point exists
        if not found:
            return -1

        # In the right subarray find the value just greater than this index
        min_delta: int = float('inf')
        ix_nxt: int = None

        for jx in range(ix + 1, n):

            delta = digits[jx] - digits[ix]

            if delta > 0 and delta < min_delta:
                min_delta = delta
                ix_nxt = jx

        # Swap values
        digits[ix], digits[ix_nxt] = digits[ix_nxt], digits[ix]

        # Sort all values to the right
        srtd = sorted(digits[ix + 1:], reverse=False)

        # Replace values in digits
        digits[ix + 1:] = srtd

        # Convert back into a number
        s = ''
        for digit in digits:
            s += str(digit)

        s = int(s)

        # Ensure the value is a 32 bit integer
        if s > 2**31:
            return -1
        else:
            return s

    def next_greater2(self, value: int) -> int:
        """Find where the value decreases

        - find the position where values decrease
        - find the value just larger than the value to the right
        - swap the current position with that next greater value
        - sort the right subarray to be decreasing

        Notes
            - while loops instead
        """

        # Grab digits at each place
        digits_str = str(value)
        digits = [int(char) for char in digits_str]

        n: int = len(digits)

        # Single digit, no greater value
        if n == 1:
            return -1

        # Iterate from right to left to find where values decrease
        ix = n - 2
        while ix >= 0 and digits[ix] >= digits[ix + 1]:
            ix -= 1

        # If no such point exists
        if ix == -1:
            return -1

        # In the right subarray find the value just greater than this value
        jx = n - 1
        while digits[jx] <= digits[ix]:
            jx -= 1

        # Swap values
        digits[ix], digits[jx] = digits[jx], digits[ix]

        # Reverse order of values in right subarray
        digits[ix + 1:] = digits[ix + 1:][::-1]

        # Convert back into a number
        s = ''
        for digit in digits:
            s += str(digit)

        s = int(s)

        # Ensure the value is a 32 bit integer
        if s > 2**31:
            return -1
        else:
            return s

    def next_greater3(self, value: int) -> int:
        """Find where the value decreases

        - find the position where values decrease
        - find the value just larger than the value to the right
        - swap the current position with that next greater value
        - sort the right subarray to be decreasing

        Notes
            - while loops instead
            - try sticking with strings
                + must use a list, since strings don't support item assignment
            - showed no significant improvement in speed or memory
        """

        # Grab digits at each place
        digits = list(str(value))
        n: int = len(digits)

        # Single digit, no greater value
        if n == 1:
            return -1

        # Iterate from right to left to find where values decrease
        ix = n - 2
        while ix >= 0 and digits[ix] >= digits[ix + 1]:
            ix -= 1

        # If no such point exists
        if ix == -1:
            return -1

        # In the right subarray find the value just greater than this value
        jx = n - 1
        while digits[jx] <= digits[ix]:
            jx -= 1

        # Swap values
        digits[ix], digits[jx] = digits[jx], digits[ix]

        # Reverse order of values in right subarray
        digits[ix + 1:] = digits[ix + 1:][::-1]

        # Convert back into a number
        s = int(''.join(digits))

        # Ensure the value is a 32 bit integer
        if s > 2**31:
            return -1
        else:
            return s


if __name__ == "__main__":

    # value = 123
    # value = 54123
    value = 230241
    obj = Solution()
    print(obj.permutations(value))
    # print(obj.next_greater(value))
    # print(obj.next_greater2(value))
    print(obj.next_greater3(value))
