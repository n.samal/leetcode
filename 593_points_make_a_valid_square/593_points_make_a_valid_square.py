"""
Inputs:
    p1 (List[int]): xy coordinate for point
    p2 (List[int]): xy coordinate for point
    p3 (List[int]): xy coordinate for point
    p4 (List[int]): xy coordinate for point
Outputs:
    bool: can a square be created
Goal:
    Determine if a square can be created from the four points

Ideas

    - Square parallel to coordinate system
        + 2 points should have the same x coordinate
        + 2 points should have the same y coordinate
            * use a set to track the coordinates with constant y
            * use a set to track the coordinates with constant x
        
        + does the logic depend on the quadrant??
        + right most point will be (max x, max y)
        + left most point will be (min x, min y)

    - the square can be rotated!!

    - distance between points should be the sameish
        + compute distances between every point
        + same distance for sides
        + longer distance for diagnol (sqrt(a^2 + a^2))
        + we should only see two magnitudes of distance
        where the we know the relationship of that magnitude
        
        d_max = sqrt(2a^2) = sqrt(2)* a^2
        
    - sort the points by (x, y)
        + then apply strategy

    - Average all the points, this will be our center.
        + Compute the distance from the center to each point, this distance should all be equal and greater than 0
        + this has a false positive on rectangles.
            * our center point is equidistant to all points, but the side distances are not equal
            * points are along the same radii but not at the correct angles

References
    - https://www.mathsisfun.com/geometry/rhombus.html

Cases

    General (Non Zero)

    Square
        - points along constant radii and 90 degree angles
    Rectangle
        - points along constant radii and NOT 90 degree angles
    Rhombus
        - equal length sides but not necessarily 90 degree angles

Example
    
    Example 1
        Input: 
            p1 = [0,0]
            p2 = [1,1]
            p3 = [1,0]
            p4 = [0,1] 
        Output

Strategies

    Brute Force
        
        - compute the distance from 1 point to 3 others
            + 2 distances will be the sides 
            + 1 distance will be the diagnol
            
    Set
        - compute distance between pairs of points
        - track these distances in a set
        - if the size of the set is greater than 2 false
        - if size is 
        
        Time Complexity
            O(1)
        
        Space Complexity
            O(1)
"""


class Solution:
    
    def distance(self, point1: List[int], point2: List[int]) -> float:
        """Compute distance between two points

        Args:
            point1 (List[int]): xy coordinate
            point2 (List[int]): xy coordinate
        Returns:
            float: distance
        """
        
        return ((point1[0] - point2[0])**2 + (point1[1] - point2[1])**2)**0.5
        
    
    def points_equal(self, p1: List[int], p2: List[int]) -> bool:
        """Check if the points are equal"""
        return p1[0] == p2[0] and p1[1] == p2[1]
    

    def distance_comparison(self, p1: List[int], p2: List[int], p3: List[int], p4: List[int]) -> bool:
        """

        - compute the distance between all points
            + redundancy occurs
        - make sure all those distance are the same
            + ensure the distances follow pythagorean

        Time Complexity
            O(1)
        Space Complexity
            O(1)
        """

        # Compute distance to all other points
        d1 = sorted([self.distance(p1, p) for p in [p2, p3, p4]])
        d2 = sorted([self.distance(p2, p) for p in [p1, p3, p4]])
        d3 = sorted([self.distance(p3, p) for p in [p1, p2, p4]])
        d4 = sorted([self.distance(p4, p) for p in [p1, p2, p3]])
        
        # Check that edges are the same length
        if d1[0] != d1[1]:
            return False

        # Check that the diagnol follows pythagorean
        square = d1[0]**2 + d1[1]**2
        diag = d1[2] ** 2

        # Diagnol should be non-zero
        if square == 0:
            return False
        
        # Diagnol must follow pythag
        if abs(square - diag) >= 1e-4:
            return False
        
        if all([d1 == d for d in [d2, d3, d4]]):
            return True
        else:
            return False

    def set_based(self, p1: List[int], p2: List[int], p3: List[int], p4: List[int]) -> bool:
        """Set Method
        
        - Compute the distances between all point pairs
        - track these distances
        - check that we have the correct amount of unique distances
            + 1 distance for sides
            + 1 distance for diagnol
        
        - check that we don't have duplicate points
            + this would create the same distances but not be
            able to create a square
        
        Returns:
            bool: to be or not to be a square
        """
        
        # Track the counts for each distance
        s = set([])
        
        # Check that we don't have duplicate points
        if self.points_equal(p1, p2) or self.points_equal(p1, p3) or self.points_equal(p1, p4) or self.points_equal(p2, p3) or self.points_equal(p2, p4) or self.points_equal(p3, p4):
            return False
        
        # Compute distance between points
        # No need to do 2 & 1, since it's the same distance as 1 & 2
        
        # Point 1
        s.add(self.distance(p1, p2))
        s.add(self.distance(p1, p3))
        s.add(self.distance(p1, p4))
        
        # Point 2
        s.add(self.distance(p2, p3))
        s.add(self.distance(p2, p4))
        
        # Point 3
        s.add(self.distance(p3, p4))
        
        # Check that we have two different distances
        # We should have 2 distances: 1 side, 1 diagnol
        return len(s) == 2
        
