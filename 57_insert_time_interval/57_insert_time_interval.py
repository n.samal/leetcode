"""

Cases

    Only Insertion

    - in front of intervals
    - in between
    - at the end

    Insertion with Merge
    - merge with existing time slot
    - then check adjacent slots for merging

Merge Cases

    - no overlap
    - all overlap
    - overlap


Strategy

- compare new intervals with current time slot
    (new_start, new_stop) vs (start, stop)

- determine location of insertion and check for merge here

    + in front

        new_start < current start

    + in between

        current_start <= new_start <= current stop

    + at the end

        new_start > last_stop

- continue instertion and check merge against previous time slots



"""

from typing import List


class Solution:

    def insert(self, intervals: List[List[int]], newInterval: List[int]) -> List[List[int]]:
        """Insert the new time interval and then merge

        Args:
            intervals: sorted time intervals
            newInterval: new time interval
        Returns:
            List[List[int]]: updated intervals
        """

        # No previous time intervals
        if not intervals:
            return [newInterval]

        # Stored newly merged time intervals
        intervals_new = []

        # Rename for ease
        new_start, new_stop = newInterval
        inserted: bool = False

        ''' Find the insertion point of the new time interval '''

        # Go through all time intervals
        for ix, (cur_start, cur_stop) in enumerate(intervals):

            # New starts before this time slot
            # We'll merge in the next loop if necessary
            if new_start < cur_start:
                intervals_new.append([new_start, new_stop])
                inserted = True
                break

            # Check for overlap with new slot
            elif (cur_start <= new_start <= cur_stop):

                # Save the lower and upper bounds for the overlap
                intervals_new.append([min(cur_start, new_start), max(cur_stop, new_stop)])
                inserted = True
                break

            # Insert the current value
            else:
                intervals_new.append([cur_start, cur_stop])

        # We haven't inserted the new value yet and there isn't an overlap
        if not inserted:
            intervals_new.append([new_start, new_stop])
            return intervals_new

        ''' Start merging the remaining values '''

        for ix, (cur_start, cur_stop) in enumerate(intervals[ix:]):

            # Compare against last time slot
            last_start, last_stop = intervals_new[-1]

            # Check for overlap

            # No overlap between start and stop
            if cur_start > last_stop:
                intervals_new.append([cur_start, cur_stop])

            # Some overlap exists
            # Grab the minimum and maximum of these bounds
            else:
                intervals_new[-1] = [min(cur_start, last_start), max(cur_stop, last_stop)]

        return intervals_new

    def insert2(self, intervals: List[List[int]], newInterval: List[int]) -> List[List[int]]:
        """Only add a time slot once it doesn't need to be merged any more

        Args:
            intervals: sorted time intervals
            newInterval: new time interval
        Returns:
            List[List[int]]: updated intervals
        Notes:
        """

        # Stored newly merged time intervals
        arr = []

        # Rename for ease
        new_start, new_stop = newInterval

        # Go through all time intervals
        for ix, (cur_start, cur_stop) in enumerate(intervals):

            # No overlap (before meetin)
            # New one ends before this current time slot
            if new_stop < cur_start:

                # Add the new value
                # make the current value our new thing to add
                arr.append([new_start, new_stop])
                new_start, new_stop = cur_start, cur_stop

            # No overlap (after meeting)
            # New time starts after old ends
            elif new_start > cur_stop:
                arr.append([cur_start, cur_stop])

            # Check for overlap with new slot
            # Save the lower and upper bounds for the overlap
            else:
                new_start = min(cur_start, new_start)
                new_stop = max(cur_stop, new_stop)

        # Add the remaining time slot
        # it will either be our last merged slot or the last interval
        arr.append([new_start, new_stop])

        return arr


if __name__ == '__main__':

    # Case 0 (Insertion)
    # intervals = [[1, 3], [6, 9]]
    # newInterval = [4, 5]

    # Insertion at end of time intervals
    # intervals = [[1, 5]]
    # newInterval = [6, 8]

    # Insertion at beginning of time intervals
    # intervals = [[6, 8]]
    # newInterval = [1, 5]

    # intervals = [[1, 5]]
    # newInterval = [0, 3]

    # Case 1 (Merge into single interval)
    # intervals = [[1, 3], [6, 9]]
    # newInterval = [2, 5]

    # Case 2 (Merge into multiple intervals)
    # intervals = [[1, 2], [3, 5], [6, 7], [8, 10], [12, 16]]
    # newInterval = [4, 8]

    s = Solution()
    # print(s.insert(intervals, newInterval))
    print(s.insert2(intervals, newInterval))
