"""
Inputs
    word1 (str): start word
    word2 (str): target word
Outputs
    bool: are strings are close?
Notes

    - Two strings are considered close if you can attain one from the other using the following operations
    - Operation 1: swap any two existing characters
        + For example, abcde -> aecdb
    - Operation 2: Transform every occurrence of one existing character into another existing character, and do the same with the other character.
        + For example, aacabb -> bbcbaa (all a's turn into b's, and all b's turn into a's)

    - operations can be used as many times as needed

Examples

    Example 1:

        Input: word1 = "abc", word2 = "bca"
        Output: true
        Explanation: You can attain word2 from word1 in 2 operations.
        Apply Operation 1: "abc" -> "acb"
        Apply Operation 1: "acb" -> "bca"

    Example 2:

        Input: word1 = "a", word2 = "aa"
        Output: false
        Explanation: It is impossible to attain word2 from word1, or vice versa, in any number of operations.

    Example 3:

        Input: word1 = "cabbba", word2 = "abbccc"
        Output: true
        Explanation: You can attain word2 from word1 in 3 operations.
        Apply Operation 1: "cabbba" -> "caabbb"
        Apply Operation 2: "caabbb" -> "baaccc"
        Apply Operation 2: "baaccc" -> "abbccc"

    Example 4:

        Input: word1 = "cabbba", word2 = "aabbss"
        Output: false
        Explanation: It is impossible to attain word2 from word1, or vice versa, in any amount of operations.

Ideas

    - we can swap adjacent character locations as many times as we want.
        + in essence order of the characters doesn't matter

    - we can swap all of each character to another character
        + in essence we don't care about how many 'a' occur in the string
        just that some character had x occurences

    - gather the character counts, then see if the count of each character
    matches the other string, and would be swappable

    - since we can only swap between the same characters we need to check that the same keys exists

"""


class Solution:

    def hash_based(self, word1: str, word2: str) -> bool:
        """

        - gather character counts for each word
        - see if both words have the same characters
            + we can't introduce new characters

        - see if the same distribution of counts occur 
            + we only the numerical value

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        # Get character counts for word1
        dct1 = {}

        for char in word1:

            if char not in dct1:
                dct1[char] = 0

            dct1[char] += 1

        # Get character counts for word2
        dct2 = {}

        for char in word2:

            if char not in dct2:
                dct2[char] = 0

            dct2[char] += 1

        # Define a key based upon character counts
        key1 = sorted(dct1.values())
        key2 = sorted(dct2.values())

        return key1 == key2 and sorted(dct1.keys()) == sorted(dct2.keys())

    def array_based(self, word1: str, word2: str) -> bool:
        """

        - gather character counts for each word
        - see if both words have the same characters
            + we can't introduce new characters

        - see if the same distribution of counts occur 
            + we only the numerical value

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        # Get character counts for word1
        arr1 = [0 for _ in range(26)]

        for char in word1:
            arr1[ord(char) - ord('a')] += 1

        # Get character counts for word2
        arr2 = [0 for _ in range(26)]

        for char in word2:
            arr2[ord(char) - ord('a')] += 1

            # Verify character exists in other word
            if arr1[ord(char) - ord('a')] <= 0:
                return False

        return sorted(arr1) == sorted(arr2)


if __name__ == '__main__':

    # Example 1
    # word1_in = 'abc'
    # word2_in = 'bca'

    # Example 2
    # word1_in = 'a'
    # word2_in = 'aa'

    # Example 3
    # word1_in = 'cabbba'
    # word2_in = 'abbccc'

    # Example 4
    word1_in = 'cabbba'
    word2_in = 'aabbss'

    # Example 5
    word1_in = 'uau'
    word2_in = 'ssx'

    obj = Solution()
    print(obj.hash_based(word1_in, word2_in))
    print(obj.array_based(word1_in, word2_in))
