
"""

Inputs
    S (str): original string of alphanumerics and dashes
    K (int): groupings for the license plate
Outputs
    str: cleaned up string

Goal:
    - reformat the strings such that each group contains exactly K characters
    - the first group which could be shorter than K, but still must contain at least one character
    - furthermore, there must be a dash inserted between two groups and all lowercase letters should be converted to uppercase.

Strategy

    - iterate through string
        + save characters
        + convert all characters to upper case

    - determine the number of characters for group 1

        n_left = len(new) / k

    - fill the first group, and maintain a pointer in the string
    - fill the remaining groups and maintain a dash

"""

from typing import List


class Solution:
    
    def licenseKeyFormatting(self, S: str, K: int) -> str:
        
        # Clean string
        s_clean = ''
        s_new = ''

        # Iterate through the string
        for ix in range(len(S)):

            # Ignore dashes
            if S[ix] == '-':
                pass

            # Convert to upper case and add
            else:
                s_clean += S[ix].upper()

        # Get length of clean string
        n = len(s_clean)

        # Determine number of characters in the first group
        g1 = n % K

        # Add characters to the first group
        ix = 0
        while g1 > 0:

            s_new += s_clean[ix]

            # Increment pointer and amount left in g1
            ix += 1
            g1 -= 1

        # Iterate in groups
        for jx in range(ix, n, K):

            # Include k characters, starting from j
            if jx != 0:
                s_new += '-' + s_clean[jx: jx + K]

            # No previous characters
            else:
                s_new += s_clean[jx: jx + K]

        # Remove excess dashes
        # s_new = s_new.strip('-')

        return s_new


if __name__ == '__main__':


    # Case 1
    S = "5F3Z-2e-9-w"
    K = 4

    # Case 2
    # S = "2-5g-3-J"
    # K = 2

    obj = Solution()
    print(obj.licenseKeyFormatting(S, K))
