"""

Strategy

    - track pointer in string
    - track pointer in pattern

    - if we reach the end indices
        True
    - if mismatch:
        False
    - if match: 
        move both pointers
    - if '?'
        move both pointers
    - if '*'
        assume a null character match ie: a* = 'a'
        assume a single character match  ie: a* = 'ab'
        assume a multi character match ie: a* = 'aaaaaa'

        move both pointers


https://leetcode.com/problems/wildcard-matching/discuss/17810/Linear-runtime-and-constant-space-solution





"""


class Solution:
    def isMatch(self, string: str, pattern: str) -> bool:
        """Does the pattern match this string

        Args:
            s (str): string to match
            p (str): pattern
        Returns:

        """

        # Strings
        self.string = string
        self.pattern = pattern

        # Lengths
        self.n = len(string)
        self.m = len(pattern)

        # Search for a matching pattern
        return self.search(0, 0)

    def search(self, s_ix: int, p_ix: int):
        """Search for the pattern

        Args:
            s_ix (int): string index
            p_ix (int): pointer index
        Returns:
            bool: match was found
        """

        while s_ix < self.n and p_ix < self.m:

            # We've got a match
            if self.string[s_ix] == self.pattern[p_ix]:
                s_ix += 1
                p_ix += 1

            # Single character match
            elif self.pattern[p_ix] == "?":
                p_ix += 1
                s_ix += 1

            # Wild card
            elif self.pattern[p_ix] == "*":

                # TODO: fix this

                # Try route with matching one character
                route1 = self.search(s_ix + 1, p_ix + 1)

                # Try route with continuing wild card
                route2 = self.search(s_ix + 1, p_ix)

                return any([route1, route2])

            else:
                return False

        # If the last char is '*' we can match anything
        # while s_ix < self.n and p_ix < self.m:

        # We've reached the end of the pattern and string
        return s_ix == self.n and p_ix == self.m


if __name__ == '__main__':

    s = Solution()

    # Basic mismatch
    # print(s.isMatch("aa", "a"))

    # Single character
    # print(s.isMatch("cb", "c?"))
    # print(s.isMatch("cb", "?a"))

    # Wildcard

    # End with wildcard
    # print(s.isMatch("aa", "*"))

    # Start with wildcard
    # print(s.isMatch("aa", "a*"))
    # print(s.isMatch("adceb", "*a*b"))
