"""

Inputs
    quality (list[int]): quality of each worker
    wage (List[int]): minimum wage for each worker
    k (int): number of workers to select
Outputs
    float: the least amount of money needed to form a paid group satisfying the above conditions
Goal:
    - we want to hire K workers from the group
    - every worker in the paid group should be paid in the ratio of their quality compared to other workers in the paid group.
    - every worker in the paid group must be paid at least their minimum wage expectation.

Cases


Example

    Input:
        quality = [10,20,5]
        wage = [70,50,30]
        K = 2
    Output:
        105.00000
    Explanation:
        We pay 70 to 0-th worker and 35 to 2-th worker.

    Example 2
        Input:
            quality = [3,1,10,10,1]
            wage = [4,8,2,2,7]
            K = 3
        Output:
            30.66667
        Explanation:
            We pay 4 to 0-th worker, 13.33333 to 2-th and 3-th workers separately

Strategy

    Ideas

        - our goal is to find workers of similar quality ratios and small wages
        - determine the wage to quality ratio to combine these factors
        - organize workers according to their ratio, and select values most similar to one another

        - do a sliding window of k workers, and compute their necessary pay?

    Cost Calculation

        Manual

            - compute the summation of all quality in all k workers
            - wage_i = quality_i * (wage_cptn / quality_cptn)
            - sum = sum(wage_i for wage_i in wages)
            - the summation of all will be minimized when

                + we minimize the ratio for the reference worker
                + we minimize the quality of the workers!
                + we must pay workers at least their minimum wage

    Min Heap

        - use a min heap to store k workers
        - iterate through the array and only maintain 3 values in the heap

            - compute the required sum for all the workers
            - the wages are normalized by the worker with the minimum ratio

    Top Down
        - apply recursive function to try all combinations and track the max, could be expensive
"""

import heapq as hq
from typing import List


class Solution:

    def mincostToHireWorkers(self, quality: List[int], wage: List[int], K: int) -> float:
        """Use a heap to sort the workers"""

        # n: int = len(quality)
        min_cost = float('inf')

        # Compute wage to quality
        ratios = [(w / q, q) for w, q in zip(wage, quality)]

        # Organize according to minimum wage ratio
        ratios = sorted(ratios)
        # ix_srt = sorted(range(n), key=lambda i: arr[i])

        heap = []

        # Go through all sorted ratios, smallest ratios first
        for r, q in ratios:

            # Tuple of ratio and index
            # we want to remove the largest quality, so we flip the values
            current = (-q, r)

            # Add values to our heap
            hq.heappush(heap, current)

            # We've got too many values
            if len(heap) > K:

                # Remove the highest quality workers
                _ = hq.heappop(heap)

            if len(heap) == 3:

                # Grab the worker with the smallest ratio
                min_ratio = r

                # Use the ratio to compute the current cost
                # wage_i = wage_cptn * (quality_i / quality_cptn) 
                # wage_i = (wage_cptn / quality_cptn) * quality_i
                cost = sum([-q_i * min_ratio for q_i, r_i in heap])

                # Update our best cost
                min_cost = min(min_cost, cost)

        return min_cost


if __name__ == "__main__":

    # Example 1
    # quality = [10,20,5]
    # wage = [70,50,30]
    # K = 2

    # Example 2
    quality = [3,1,10,10,1]
    wage = [4,8,2,2,7]
    K = 3

    obj = Solution()
    print(obj.mincostToHireWorkers(quality, wage, K))

