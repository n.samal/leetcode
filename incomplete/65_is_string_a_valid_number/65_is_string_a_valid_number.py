"""

Inputs
    s (str): input string

    values can contain any number/sequence of characters
Outputs
    bool: is the string a valid decimal

Examples

    "0" => true
    " 0.1 " => true
    "abc" => false
    "1 a" => false
    "2e10" => true
    " -90e3   " => true
    " 1e" => false
    "e3" => false
    " 6e-1" => true
    " 99e2.5 " => false
    "53.5e93" => true
    " --6 " => false
    "-+3" => false
    "95a54e53" => false

Strategy

    some characters can only be used once
        ie: + - . e

    if char not valid => False ie: 'a'
    if char == ' ' (whitespace) ignore it
    if char == 'e', must have valid numbers before and after or a sign
    if char == '.' must have valid numbers before and after
    if char == '+' or char == '-' must have a number after

References
    https://leetcode.com/problems/valid-number/discuss/173977/Python-with-simple-explanation

"""


class Solution:

    def isNumber(self, s: str) -> bool:

        ix: int = 0
        n: int = len(s)
        valid: bool = True

        # Decimal, exponent, sign can only be used once
        used_dec = False
        used_exp = False
        used_sgn = False

        # We need at least one number to be valid
        used_number = False

        signs = set(['+', '-'])
        numbers = set(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'])
        number_sign = numbers.union(signs)

        # All valid symbols
        valid_char = signs.union(numbers).union(set(['e', '.', ' '])) 

        while valid and ix < n:

            char = s[ix]

            # Invalid character
            if char not in valid_char:
                valid = False

            # Valid character: number
            elif char in numbers:
                used_number = True

            # Valid character: white space
            elif char == ' ':
                pass

            # Signage, must be followed by a number
            elif char in signs and used_sgn is False:

                # Number is out of range, or not a number
                if ix + 1 >= n or s[ix + 1] not in numbers:
                    valid = False
                else:
                    used_sgn = True

            # Decimal can only be used once
            # ie: ' 3.5 '
            # Decimal can't occur after 'e' only before
            # ie: '6.5e3' not '6e3.5'
            elif char == '.' and used_dec is False and used_exp is False:

                # Must be followed by a number
                if ix + 1 >= n or s[ix + 1] not in numbers:
                    valid = False
                else:
                    used_dec = True

            # Exponent
            elif char == 'e' and used_exp is False:

                # Must be preceeded by a number and followed by a sign or number
                # ie: 2e4, 2e-4, 2e+4
                if ix - 1 < 0 or ix + 1 >= n or s[ix - 1] not in numbers or s[ix + 1] not in number_sign:
                    valid = False
                else:
                    used_exp = True
            else:
                valid = False

            ix += 1

        # Did we reach the end of the string wihout an issue?
        if valid and ix == n and used_number:
            return True
        else:
            return False

    def isNumber2(self, s: str) -> bool:
        """

        - remove excess trailing and leading white spaces with strip
        - use strings functions to simplify str.isdigit()
        """

        # Remove leading and trailing white space
        s = s.strip()

        ix: int = 0
        n: int = len(s)
        valid: bool = True

        # Decimal, exponent, sign can only be used once
        used_dec = False
        used_exp = False
        used_sgn = False

        # We need at least one number to be valid
        used_number = False

        signs = set(['+', '-'])
        number_sign = numbers.union(signs)

        # All valid symbols
        valid_char = signs.union(numbers).union(set(['e', '.', ' '])) 

        while valid and ix < n:

            char = s[ix]

            # Valid character: number
            if char.isdigit():
                used_number = True

            # Signage, must be followed by a number
            elif char in signs and used_sgn is False:

                # Number is out of range, or not a number
                if ix + 1 >= n or s[ix + 1].isdigit() is False:
                    return False
                else:
                    used_sgn = True

            # Decimal can only be used once
            # ie: ' 3.5 '
            # Decimal can't occur after 'e' only before
            # ie: '6.5e3' not '6e3.5'
            elif char == '.' and used_dec is False and used_exp is False:

                # Must be followed by a number
                if ix + 1 >= n or s[ix + 1].isdigit() is False:
                    return False
                else:
                    used_dec = True

            # Exponent
            elif char == 'e' and used_exp is False:

                # Must be preceeded by a number and followed by a sign or number
                # ie: 2e4, 2e-4, 2e+4
                if ix - 1 < 0 or ix + 1 >= n or s[ix - 1].isdigit() is False or s[ix + 1] not in number_sign:
                    return False
                else:
                    used_exp = True
            else:
                return False

            ix += 1

        # Did we reach the end of the string wihout an issue?
        if valid and ix == n and used_number:
            return True
        else:
            return False


if __name__ == "__main__":

    s = Solution()

    # case = "0"
    # case = "0.1"
    # case = "abc"
    # case = "1 a"
    # case = "2e10"
    # case = "-90e3"
    # case = "1e"
    # case = "e3"
    # case = "6e-1"

    # Tricky case (decimal can only occur before 'e')
    # case = "99e2.5"

    # case = "53.5e93"
    # case = "--6"
    # case = "-+3"
    # case = "95a54e53"

    # Valid number
    case = "1 "

    # All spaces is invalid
    case = "  "

    # Decimal without preceding value, stil valid
    case = ".5"

    print(s.isNumber(case))
