""" Iterative """


def divide(dividend: int, divisor: int) -> int:
    """Calculate the quotient without using division, modulus or multiplication

    Time Complexity
        O(n)
    Space Complexity
        O(1)

    Arg:
        dividend (int): value to divide
        divisor (int): value to divide by
    Returns:
        int: quotient after division
    References:
        https://leetcode.com/problems/divide-two-integers/
    Issue:
        doesn't account for numerical limits specified in problem statement
    """

    # Nothing to divide
    if dividend == 0:
        return 0

    # Singular value
    if divisor == 1:
        return dividend
    elif divisor == -1:
        return -dividend

    # Number of divisions
    ix: int = 0

    # Both positives
    if dividend > 0 and divisor > 0:

        # Subtract values while positive
        while dividend >= 0:

            dividend -= divisor
            ix += 1

        return ix - 1

    #  Negative values
    elif dividend < 0 and divisor < 0:

        while dividend <= 0:

            dividend -= divisor
            ix += 1

        return ix - 1

    # One of the value is negative
    else:

        # Convert both to positives
        dividend = abs(dividend)
        divisor = abs(divisor)

        # Subtract values while positive
        while dividend >= 0:

            # print(ix, dividend)
            dividend -= divisor
            ix += 1

        return -(ix - 1)


def divide2(dividend: int, divisor: int) -> int:
    """Calculate the quotient without using division, modulus or multiplication

    Time Complexity
        O(n)
    Space Complexity
        O(1)

    Arg:
        dividend (int): value to divide
        divisor (int): value to divide by
    Returns:
        int: quotient after division
    References:
        https://leetcode.com/problems/divide-two-integers/
    Issues:
        Time Limit exceeded
    """

    # Initialize value
    val = None

    # Nothing to divide
    if dividend == 0:
        return 0

    # Singular value
    if divisor == 1:
        val = dividend
    elif divisor == -1:
        val = -dividend

    if val is None:

        # Determine if positive
        positive = dividend > 0 and divisor > 0

        # Convert both to positives
        dividend = abs(dividend)
        divisor = abs(divisor)

        # Number of divisions
        ix: int = 0

        # Subtract values while positive
        while dividend >= 0:

            dividend -= divisor
            ix += 1

        if positive:
            val = ix - 1
        else:
            val = -(ix - 1)

    # Account for limits specified
    return min(max(-2147483648, val), 2147483647)


""" Binary Search """


def divide_binary_search(dividend: int, divisor: int) -> int:
    """Use a binary search with a guess and check

    Args:
        dividend (int):
        divsor (int):
    Returns:
        int: number of times dividend can be divided by divisor
    """

    # Dividing by one results in itself
    if divisor == 1:
        return dividend
    elif divisor == -1:
        return -dividend

    # Dividing zero is always zero
    elif dividend == 0:
        return 0

    # Both positives
    if dividend > 0 and divisor > 0:

        val = quotient_by_binary_search(dividend, divisor)

    # Both negatives
    elif dividend < 0 and divisor < 0:

        val = quotient_by_binary_search(abs(dividend), abs(divisor))

    # One positive, one negative
    else:

        val = -quotient_by_binary_search(abs(dividend), abs(divisor))

    # Account for limits specified
    # largest number between lower bound and value
    # smallest number between upper bound and value
    return min(max(-2147483648, val), 2147483647)


def quotient_by_binary_search(
    dividend: int, divisor: int, low: int = 0, high: int = 31
):
    """Compute binary search to find the quotient

    Args:
        dividend (int): number to be divided
        divsor (int): number to divide by
        low (int): lower exponent limit
        high (int): upper exponent limit
    Returns:
        int: number of divisions aka quotient
    """

    # Save largest divided that works
    best = -1

    # Stop iterating when at settling point
    while low <= high:

        mid = (low + high) // 2

        value = divisor * mid
        print(f'low: {low} high: {high} mid: {mid} val: {value} best: {best}')

        # We're too small
        if divisor * mid < dividend:

            # Save the best quotient that works
            best = max(mid, best)
            low = mid + 1

        # We're too large
        elif divisor * mid > dividend:
            high = mid - 1

        # We've got it
        else:
            return mid

    # Return closest
    return best


if __name__ == "__main__":

    # TODO: binary search method is incorrect, currently doing exponent not multiple

    print(divide_binary_search(10, 3))
    print(divide_binary_search(-10, -3))
    print(divide_binary_search(10, -3))
    # print(divide(-10, 3))
    # print(divide_binary_search(1, 1))
    # print(divide_binary_search(0, 1))

    # print(divide(5, 2))
    # print(divide_binary_search(5, 2))

    
