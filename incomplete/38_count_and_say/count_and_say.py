class Solution:

    def countAndSay(self, n: int) -> str:
    	"""Say groups of digits together

		Arg:
			n (int): nth term of the sequence
		Returns:
			str: sequence of digits as vocalized
    	"""

    	# Conver the number into a string
    	s = str(n)

    	# No value
    	if n == 0:
    		return ''

    	# Iterate through the string
    	


    	return s
        

if __name__ == "__main__":

	# Test case
	print(countAndSay(1))
	# print(countAndSay(11))
	# print(countAndSay(21))
	# print(countAndSay(1211))
	# print(countAndSay(111221))