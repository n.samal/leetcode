"""
Inputs
    nums1 (List[int]): sorted array
    nums2 (List[int]): sorted array
Outputs:
    float: median of combined array
Goal:
    - find the median of the combined array
    - find in O(log(m+n)) time

Ideas
    - iterate and compare
    - we probably need to use binary search...

        + we need to leverage the sorted nature of the arrays
        to improve our time complexity

Cases
    
    Null Array
        - one array is empty

    General
        - both arrays have an overlapping range of values

    Small Array 1
        - values in array1 are much smaller than array 2

    Large Array 1
        - values in array1 are much larger than array 2

    n = even
        [0, 1, 2, 3, 4, 5]
        
        n = 6
        we want 2, 3
        median1 = n // 2
        median2 = median1 + 1

    n = odd

        [0, 1, 2, 3, 4]

        n = 5
        grab the middle value ie: 2
        median = n  // 2

Example: Smaller Array 1

    arr1 = [1, 2, 4, 5, 6, 7]
    arr2 = [9, 12, 16, 20]

    n = 6
    m = 4
    nm = 10

    combined array
        indices: [0, 1, 2, 3, 4, 5, 6,  7,  8,  9]
        values:  [1, 2, 4, 5, 6, 7, 9, 12, 16, 20]
    middle values
        nm // 2 = 5
        nm // 2 -1 = 4

        => 6, 7

        - both lie in array 1
        - we need to converge down to 2 values

    compute mid point in each array
        mid1 = n // 2
             = 6 // 2 = 3
        mid2 = m // 2
             = 4 // 2 = 2

        mid1 = 5
        mid2 = 16
    
    check values left
        values_left1 = (high_ix1 - low_ix2) + 1
        values_left2 = (high_ix1 - low_ix2) + 1
        values_left = values_left1 + values_left2

    mid2 > mid1



Example: General case

    arr1 = [5, 7, 9, 11, 15]
    arr2 = [8, 10, 12, 16, 20]

    n = 5
    m = 5
    nm = 10

    combined array
        indices: [0, 1, 2, 3, 4,  5,  6,  7,  8,  9]
        values:  [5, 7, 8, 9, 10, 11, 12, 15, 16, 20]
    middle values
        nm // 2 = 5
        nm // 2 -1 = 4

        => 10, 11

    - we know from n if we need a single value or two values
    - maybe we can use binary search to converge to those

    Since values are sorted, we know the lower and upper bounds
        low = min(arr1[0], arr2[0])
        high = max(arr1[-1], arr2[-1])

    arr1
        low_ix = 0
        high_ix = 4

    arr2
        low_ix = 0
        high_ix = 4
    
    values_left1 = (high_ix1 - low_ix2) + 1
    values_left2 = (high_ix1 - low_ix2) + 1
    values_left = values_left1 + values_left2

    - if we start in the middle of both arrays

        mid = n // 2
              5 // 2 = 2
        mid1 = 9 
        mid2 = 12

        compare the values

Strategies

    Iterate
        - create an empty list
        - compare top of the lists against one another
        - fill the combined list
        - find the median
    Iterate 2
        - we don't need the entire array for median calculation
        - just the middle value, or middle 2 values

    Binary Search
        - use binary search to find the middle two or middle 1 value
        - 





"""
from typing import List



class Solution:

    def iterate1(self, nums1: List[int], nums2: List[int]) -> float:
        """Consolidate into sorted array then compute median

        - iterate through arrays comparing values
        - put the smallest value into a seperate array
        - compute the median after complete

        Args:
            nums1: sorted array
            nums2: sorted array
        Returns:

        Time Complexity
            O(n+m)
        Space Complexity
            O(n+m)
        """

        n: int = len(nums1)
        m: int = len(nums2)

        ix: int = 0
        jx: int = 0
        kx: int = 0

        # Empty array
        nm = n + m
        arr = [None for _ in range(nm)]

        # Iterate through values
        while kx < nm:

            # If nothing left in array 2 or smaller value in array 1
            if jx >= m or (ix < n and nums1[ix] <= nums2[jx]):
                arr[kx] = nums1[ix]
                ix += 1

            # Use array 2
            else:
                arr[kx] = nums2[jx]
                jx += 1

            # Increment k
            kx += 1

        mid = nm // 2

        # Calculate if even
        if nm % 2 == 0:
            return (arr[mid] + arr[mid - 1]) / 2.0

        # Odd number of elements
        else:
            return arr[mid]

    def iterate2(self, nums1: List[int], nums2: List[int]) -> float:
        """Consolidate into sorted array then compute median

        - only use a partial arrays, since we only need up to the middle value

        Args:
            nums1: sorted array
            nums2: sorted array
        Returns:

        Time Complexity
            O(n+m)
        Space Complexity
            O(n+m)
        """

        n: int = len(nums1)
        m: int = len(nums2)

        ix: int = 0
        jx: int = 0
        kx: int = 0

        # Empty array
        nm = n + m
        mid = nm // 2
        arr = [None for _ in range(mid + 1)]

        # Iterate through values
        while kx <= mid:

            # If nothing left in array 2 or smaller value in array 1
            if jx >= m or (ix < n and nums1[ix] <= nums2[jx]):
                arr[kx] = nums1[ix]
                ix += 1

            # Use array 2
            else:
                arr[kx] = nums2[jx]
                jx += 1

            # Increment k
            kx += 1

        mid = nm // 2

        # Calculate if even
        if nm % 2 == 0:
            return (arr[mid] + arr[mid - 1]) / 2.0

        # Odd number of elements
        else:
            return arr[mid]


if __name__ == "__main__":

    # Arrays
    # nums1 = [1, 3]
    # nums2 = [2]

    nums1 = [1, 3]
    nums2 = [2, 4]

    obj = Solution()
    # print(obj.iterate1(nums1, nums2))
    print(obj.iterate2(nums1, nums2))
