"""
Inputs
	maxtrix (ListL[List[str]]): 

Output
	int: area of maximal rectangle

Goals
	- find the maximal retangle of only 1s

Ideas
	- seems similar to maximal square
		+ brute force calculation is quite expensive
		+ dynamic programming approach can find maximal square length much faster

	- also similar to max rectangle under histogram
		+ convert this program into that, by calculating the height of each column
		at a certain position
		+ then use the same stack strategy to grab the left and right bounds

Strategy
	
	Dynamic Programming

		- initialize vars

			+ n_rows: number of rows
			+ n_cols: number of cols
			+ max_rect_array: array of maximal rectangle area/length ending at each position
			+ max_rect_len: overall maximal rectangle area

		- iterate through cases with no priors
			+ first row
			+ first col
			+ max rectangle ending here is
				* if value ='0': 0
				* if value ='1': prior + 1

				first row: look at columns to the left
				first column: look at rows above

		- iterate through the rest of the array
			+ row 1 to row n
			+ col 1 to col n

			+ compute maximal rectangle
				
				* hand calculations show, we can calculate maximal rectangle as product of row exntesion array and column extension array
	Heights + Monotonic Stack

		- compute the heights at each row and column index
			+ we can leverage the heights calculated previously
			+ if current = 1
				height = height[col - 1] + 1
			+ if current = 0
				height = 0

		- using the known heights iterate over the current column using
		a monotonic stack to find the bounds of our rectangle (just like 84_max rectangle in histogram)
			- using these indices stored from stack to calculate the width
			- using the height computed previously
			- compute the area with both of these
			- track the max area found so far

	
		Time Complexity
			O(n*m)

			n rows
			m columns

		Space Complexity
			O(m)  for the current row store the heights of each column 

		References:
			https://leetcode.com/problems/maximal-rectangle/discuss/29065/AC-Python-DP-solutioin-120ms-based-on-largest-rectangle-in-histogram
"""

