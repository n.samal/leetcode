"""
Inputs
    timeSeries (List[int]): time of attacks
    duration (int): duration of attack time
Outputs
    int: total time a pokemon is attacked
Notes
    - an attack starts when you attack
    - the attack time doesn't overlap
        ie: if they're already poisoned until time 3
        we cannot attack/poison them until after that ends

Examples

    Example 1
        Input: [1,4], 2
        Output: 4
        Explanation: At time point 1, Teemo starts attacking Ashe and makes Ashe be poisoned immediately. 
        This poisoned status will last 2 seconds until the end of time point 2. 
        And at time point 4, Teemo attacks Ashe again, and causes Ashe to be in poisoned status for another 2 seconds. 
        So you finally need to output 4.

    Example 2:

        Input: [1,2], 2
        Output: 3
        Explanation: At time point 1, Teemo starts attacking Ashe and makes Ashe be poisoned. 
        This poisoned status will last 2 seconds until the end of time point 2. 
        However, at the beginning of time point 2, Teemo attacks Ashe again who is already in poisoned status. 
        Since the poisoned status won't add up together, though the second poisoning attack will still work at time point 2, it will stop at the end of time point 3. 
        So you finally need to output 3.

Hand Calc

    Example 1

        time_start = 1
        time_end = 1 + 2 = 3

        duration = 3 - 1 = 2

        time_end_prev = 3

        time_start = 4
        time_end = 4 + 2 = 6
        duration = 2 + (6 - 4) = 4

    Example 2

        time_end_prev = None
        time_start = 1
        time_end = 1 + 2 = 3

        duration = 3 - 1 = 2

        time_end_prev = 3
        time_start = 2
        time_end = 2 + 2 = 4

        the attack time starts before the last one ends
        we only contribute the time starting after the last one ends

        1 -> 2 -> 3
             2 -> 3 -> 4

        duration = 2 + 1 = 3

"""

class Solution:
    def findPoisonedDuration(self, timeSeries: List[int], duration: int) -> int:
        """

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        if not timeSeries:
            return 0

        if not duration:
            return 0

        total: int = 0
        time_end_prev = 0

        for time_start in timeSeries:

            time_end = time_start + duration

            # Only add non-overlapping time
            if time_start < time_end_prev:
                total += time_end - time_end_prev
            else:
                total += duration

            # Save when the last attack ended
            time_end_prev = time_end

        return total
