"""
Inputs
    arr (List[int]): numbers in unknown order
    t (int): max absolute deviation in magnitude
    k (int): max absolute deviation in index
Outputs
    bool: can a valid pair be found
Notes
    - find a pair meeting these conditions
        + indice difference must be less than or equal to k
        + value difference must be less than or equal to k
    - indices in pairs must be distinct

Ideas
    - multiple values of the same magnitude exist

    - brute force is expensive
        + we can limit comparison within t apart

    - sorting will help us determine find value pairs close to one another
        + like a sorted sliding window
        + sort k + 1 values at a time, then check?

        could be expensive

    - we want to keep values that are within t in magnitude
        + we could use a dictionary to verify if values in that similar magnitude exist
            * expensive, to check all those keys exists

        + how can we store them into bins?
            * modulus => no, it will be too similar for most values
            * integer division => should be different enough

                using exactly t, we get some key collisions above
                and below but not everywhere we want

                we don't need to store all values of the same key,
                we know that each value will be in the same range of +- t

                Value	t	Division	Int Div
                40	    3	 13.33	      13
                41	    3	 13.67	      13   |
                42	    3	 14.00	      14   |
                43	    3	 14.33	      14   |
                44	    3	 14.67	      14   *
                45	    3	 15.00	      15   |
                46	    3	 15.33	      15   |
                47	    3	 15.67	      15   |
                48	    3	 16.00	      16
                49	    3	 16.33	      16

                we have to check some keys above and below the calculated value

                ie: if value = 44, t = 3, where key = 14
                we need to check key 13 and 15

                if t = 0 we have an integer divison problem, maybe use the value itself

        + for similar values of the same key maintain the most recent index

            ie: key = 40, ix = 0
                key = 40, ix = 10

                we only need recent values to be within k of the current index

References
    - https://leetcode.com/problems/contains-duplicate-iii/discuss/61676/Python-solution-with-detailed-explanation
    - https://leetcode.com/problems/contains-duplicate-iii/discuss/61731/O(n)-Python-using-buckets-with-explanation-10-lines.
"""
from typing import List


class Solution:
    def brute_force(self, arr: List[int], k: int, t: int) -> bool:
        """Brute force

        - compare each value against the next k values

        Time Complexity
            O(n*k)
        Space Complexity
            O(1)
        """

        n: int = len(arr)

        for ix in range(n):
            for jx in range(ix + 1, ix + k + 1):
                if arr[jx] - arr[ix] <= t:
                    return True

        return False

    def sort_k(self, arr: List[int], k: int, t: int) -> bool:
        """Sort k values to find the minimum differences first

        Time Complexity
            O(n*k)
        Space Complexity
            O(k)
        Notes
            - this is same complexity as brute force solution
            but may find answers faster
        """

        n: int = len(arr)

        for ix in range(n - k):

            # Sort the next k values
            vals = sorted(arr[ix: ix + k + 1])

            # Slide across the values and compare adjacent values
            for jx in range(1, k + 1):
                if vals[jx] - vals[jx - 1] <= t:
                    return True

        return False

    def buckets(self, arr: List[int], k: int, t: int) -> bool:
        """Use buckets to save indices

        - label buckets using the value itself or it's number
        divided by t
        - for every value search the current bucket and adjacent buckets
        for values similar in magnitude, and location

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        Notes
            - remove old buckets that we no longer need
                + may run into issues when t=0
        """

        n: int = len(arr)
        dct = {}

        for ix in range(n):

            if t == 0:
                key_cur = arr[ix]
            else:
                key_cur = arr[ix] // t

            key_plus = key_cur + 1
            key_mins = key_cur - 1

            for key in [key_cur, key_plus, key_mins]:
                if key in dct:
                    if abs(arr[ix] - arr[dct[key]]) <= t and abs(ix - dct[key]) <= k:
                        return True

            # Add value
            dct[key_cur] = ix

        return False


if __name__ == '__main__':

    # Example 1
    # nums = [1, 2, 3, 1]
    # k = 3
    # t = 0

    # Example 2
    # nums = [1, 0, 1, 1]
    # k = 1
    # t = 2

    # Example 3
    # nums = [1, 5, 9, 1, 5, 9]
    # k = 2
    # t = 3

    obj = Solution()
    # print(obj.brute_force(nums, k, t))
    # print(obj.sort_k(nums, k, t))
    print(obj.buckets(nums, k, t))
