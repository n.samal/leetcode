"""

Base Cases

- all positives (want all values)
- all negatives (want smallest value only or none)
- null value (want all values)
- one value (want all value if positive, none if negative)
- mixed (general case)

========================
Approach 0: Brute Force
========================

Space Complexity: O(1)
Time Complexity: O(n)

- for each starting and ending array position compute the summation

for tail_ix in range(n - 1):
    for head_ix in range(tail_ix + 1, n):

========================
Approach 1: Caterpillar
========================

- use the caterpillar (heads and tails approach)
- keep track of
    + maximm array sum so far, and indices
    + current arr sum
    + indices of head and tail

- start the head and tail at index 0
- crawl the head forward

    + add the value to the current same
    + while we are positive in value continue to move forward
    + if the value is negative, then switch the tail movement

- move the tail foward towards the head until we have no more indices to explore

Notes

    - if we consider [] a subarray, then starting indices must change
    - if we consider [] a subarray, then indices must change

References:
    https://en.wikipedia.org/wiki/Maximum_subarray_problem

===================
Dynamic Programming
===================

- at each index track the maximum sum ending here
- at the next index we can choose to
    + continue the chain: prev_sum + val
    + start a new chain: val

- the new max is the maximum of those two values
- do this for every index and track the maximum value seen


"""

from typing import List


class Solution:

    def maxSubArray1(self, arr: List[int]) -> int:
        """Assumes an empty subarray is valid"""

        cur_sum: int = 0
        n: int = len(arr)

        head_ix: int = 0
        tail_ix: int = 0

        max_sum: int = 0

        # print(arr)

        if n == 0:
            return max_sum

        # Valid indices exist
        while head_ix < n and tail_ix < n:

            # print(arr[tail_ix:head_ix + 1])

            # If move head to explore and greater than 0
            if head_ix == tail_ix or (head_ix < n and cur_sum + arr[head_ix] > 0):

                # Add new value to the sum
                # Move head forward
                cur_sum += arr[head_ix]
                head_ix += 1

            else:

                # Remove last tail value
                # Move tail forward
                cur_sum -= arr[tail_ix]
                tail_ix += 1

            # Track our max subarray
            if cur_sum > max_sum:
                max_sum = cur_sum
                # max_ix = (tail_ix, head_ix)

        return max_sum

    def maxSubArray(self, arr: List[int]) -> int:
        """Empty subarray does not count

        Time Complexity
            O(n)
        Space Complexity
            O(1)

        """

        # TODO: incorrect

        n: int = len(arr)

        if n == 0:
            return 0
        if n == 1:
            return arr[0]

        tail_ix: int = 0
        head_ix: int = 0

        cur_sum: int = arr[head_ix]
        max_sum: int = cur_sum

        print(arr)

        # Valid indices exist
        while head_ix < n and tail_ix < n:

            arr_sub = arr[tail_ix:head_ix + 1]
            print(f" arr: {arr_sub} sum: {cur_sum}")

            # Null subarray or
            # If move head room to explore and greater than 0
            if (head_ix == tail_ix) or (head_ix < n and cur_sum + arr[head_ix] > 0):

                # Add new value to the sum
                # Move head forward
                cur_sum += arr[head_ix]
                head_ix += 1

            # Negative sum
            else:

                # Remove last tail value
                # Move tail forward
                cur_sum -= arr[tail_ix]
                tail_ix += 1

            # Track our max subarray
            max_sum = max(max_sum, cur_sum)

        return max_sum

    def kandanes(self, arr: List[int]):

        # Set to keep only unique values
        n = len(arr)        # length of the input array (int)

        current_sum = 0    # current summation if you stop at this point (int)
        # max_sum_ix = None  # indices of the max summation (tuple)
        max_sum_val = -float('inf')  # value of the max summation (int)
        start_ix = 0  # lower bound index [int]

        # Iterate through
        for stop_ix in range(n):

            # Add current value
            current_sum += arr[stop_ix]

            print(f' arr: {arr[start_ix:stop_ix + 1]}  sum: {current_sum}')
            # print(f'stop_ix: {stop_ix}  value: {arr[stop_ix]}')

            # Found a new max
            if current_sum > max_sum_val:

                print('  found new max')
                max_sum_val = current_sum
                # max_sum_ix = (start_ix, stop_ix)

            # Encountered a negative sum
            if current_sum < 0:

                print('  got negative summation')

                # Reset current summation
                current_sum = 0

                # Try starting the sum with only the next value
                start_ix += 1

        return max_sum_val

    def kandanes_mod(self, arr: List[int]):
        """Derived version of kandanes algorithm

        At each index consider
            1) starting with just the current value
            2) continue the previous best subarray summation

        At each prior index we've already considered the best subarray,
        so our previous best summation is a good place to start

        It's best to start fresh when the current summation is going negative
        ie:
            -100 + 200
            it's still better to just use 200 instead of (-100 + 200)

            -5, -4, -3

            more negatives don't benefit us, start fresh

            100, -5, 50

            here it's better to continue adding values, since we have
            a positive current sum ie: 100 + -5, 95
        """

        n = len(arr)

        if n == 0:
            return 0

        current_sum = 0    # current summation if you stop at this point (int)
        max_sum_val = -float('inf')  # value of the max summation (int)

        for val in arr:

            # Add value to current summation
            current_sum += val

            # Track the current best
            max_sum_val = max(max_sum_val, current_sum)

            # Current summation is going negative
            # it's better to start fresh
            if current_sum < 0:
                current_sum = 0

        return max_sum_val


if __name__ == "__main__":

    # Mixed
    # arr = [-2, 1, -3, 4, -1, 2, 1, -5, 4]

    # Null case
    # arr = []

    # Singular Case
    # arr = [1]
    # arr = [-1]

    # All positives
    # arr = [1, 2, 3, 4, 5]

    # All negatives
    # arr = [-2, -1]
    # arr = [-1, -2, -3, -4, -5]

    s = Solution()
    # print(s.maxSubArray(arr))
    print(s.kandanes_mod(arr))

