"""
Inputs
    text (str): string
Outputs
    int: longest string of repeated characters
Notes
    - find the longest string of repeated characters
    - we are allowed to swap two characters in the string

Examples 

    Example 1:

        Input: text = "ababa"
        Output: 3
        Explanation: We can swap the first 'b' with the last 'a', or the last 'b' with the first 'a'. Then, the longest repeated character substring is "aaa", which its length is 3.

    Example 2:

        Input: text = "aaabaaa"
        Output: 6
        Explanation: Swap 'b' with the last 'a' (or the first 'a'), and we get longest repeated character substring "aaaaaa", which its length is 6.

    Example 3:

        Input: text = "aaabbaaa"
        Output: 4

    Example 4:

        Input: text = "aaaaa"
        Output: 5
        Explanation: No need to swap, longest repeated character substring is "aaaaa", length is 5.

    Example 5:

        Input: text = "abcdef"
        Output: 1

Ideas

    - find the character counts for adjacent characters
    - if the groupings are 1 character apart we can join them

    - if no nearby groupings, we can extend the current group by 1
    by borrowing from another group

"""
from collections import defaultdict, Counter


class Solution:

    def maxRepOpt1(self, text: str) -> int:
        """Combine adjacent character groups

        - get the character counts for each group
        - see if we can connect the current group
        to the previous group

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        counts = Counter(text)
        dct = defaultdict(list)
        max_len: int = 0

        # Count the number of consecutive characters
        # and the starting location
        n: int = len(text)
        prev = None

        for ix, char in enumerate(text):

            if char == prev:
                dct[char][-1][1] += 1                
            else:
                dct[char].append([ix, 1])

            max_len = max(max_len, dct[char][-1][1])
            prev = char

        # For every key determine what the best swap could be by combining adjacent windows
        for key in dct:
            n: int = len(dct[key])

            # print(f'key: {key}')

            for ix in range(1, n):

                last_start, last_count = dct[key][ix - 1]
                cur_start, cur_count = dct[key][ix]

                distance = cur_start - (last_start + last_count)

                # print(f' distance: {distance}  counts: {counts[key]}')

                # We're close enough to bridge the gap
                # and have other characters to do it
                if distance == 1:

                    count_total = last_count + cur_count

                    # Got an extra character to use
                    if counts[key] > count_total:
                        max_len = max(max_len, count_total + 1)
                    else:
                        max_len = max(max_len, count_total)

                # Borrow a letter to extend the current chain
                else:

                    if counts[key] > cur_count:
                        max_len = max(max_len, cur_count + 1)

                    if counts[key] > last_count:
                        max_len = max(max_len, last_count + 1)

        return max_len

    def cleaner(self, text: str) -> int:
        """Same approach as above but cleaner

        - get the character counts for each group
        - see if we can connect the current group
        to the previous group

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        arr = []
        max_len: int = 0

        counts = Counter(text)

        # Count the number of consecutive characters
        # ie: [['a', 1], ['a', 3]]
        n: int = len(text)
        prev = None

        for ix, char in enumerate(text):

            if char == prev:
                arr[-1][1] += 1
            else:
                arr.append([char, 1])

            # Borrow a character from another grouping
            if counts[char] > arr[-1][1]:
                max_len = max(max_len, arr[-1][1] + 1)

            max_len = max(max_len, arr[-1][1])
            prev = char

        # Compare adjacent character groups
        n: int = len(arr)

        for ix in range(1, n - 1):

            # Two characters groups that are one character apart
            if arr[ix][1] == 1 and arr[ix - 1][0] == arr[ix + 1][0]:

                counts_here = arr[ix - 1][1] + arr[ix + 1][1]

                if counts[arr[ix - 1][0]] > counts_here:
                    grouping = counts_here + 1
                else:
                    grouping = counts_here

                max_len = max(max_len, grouping)

        return max_len


if __name__ == '__main__':

    # Example 1
    # text = "ababa"

    # Example 2
    text = "aaabaaa"

    # Example 3
    # text = "aaabbaaa"

    # Example 4
    # text = "aaaaa"

    # Example 5
    # text = "abcdef"

    obj = Solution()
    print(obj.maxRepOpt1(text))
    print(obj.cleaner(text))
