"""
Inputs
    nums( List[int]): collection of distinct integers
Outputs
    List[List[str]]: all possible permutations

Ideas
    - use top down approach to build lists using
        + track what we've used
        + track how many numbers left to build
        complete list
"""

from typing import List


class Solution:
    def permute(self, arr: List[int]) -> List[List[int]]:
        """Generate all permuations

        Time Complexity
            O(~!n)

            n, n - 1, n - 2
        Space Complexity
            O(!n)  store all answers
        """

        self.ans = []
        self.arr = arr
        visited = [False for _ in arr]
        n: int = len(arr)

        self.top_down([], visited, n)

        return self.ans

    def top_down(self, cur: List[int], visited: List[int], left: int):    
        """Crawl away

        - add number to current path
        - mark number as visited then make it avaialbe again 
        after all subpaths with that value have been explored

        Args:
            cur: current list of values
            visited: visited values
            left: numbers left
        """

        if not left:
            self.ans.append(cur)

        else:

            for ix, v in enumerate(self.arr):

                if visited[ix]:
                    continue
                else:

                    visited[ix] = True

                    self.top_down(cur + [v], visited, left - 1)

                    visited[ix] = False


if __name__ == '__main__':

    # Example 1
    arr = [1, 2, 3]

    obj = Solution()
    print(obj.permute(arr))
