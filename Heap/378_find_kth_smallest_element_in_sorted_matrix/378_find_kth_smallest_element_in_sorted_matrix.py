"""
Inputs
    arr (List[List[int]]): sorted matrix
    k (int): kth smallest
Outputs
    int: grab kth smallest element
Example
    matrix = [
   [ 1,  5,  9],
   [10, 11, 13],
   [12, 13, 15]
            ],
    k = 8,

    return 13.

Ideas
    - values are sorted, we need to leverage that
    - values are not fully sorted!
        + notice that 12 occurs in row 2, and 13 is in row 1

    - cheater way
        + flatten elements
        + sort them
        + grab k -1

    - leverage the sorting
        + place pointers at the beginning of each list
        + grab the smallest value from those N
        + bump the pointer for the list, where we grabbed a value

        + seems similar to sorting k sorted lists

    - how to handle situation when a row finishes,
    which next value should we grab?
        + the comparison is already between the smallest values

        we will grab the smallest value each time, since our frontier
        is the smallest possible values

"""

import heapq

class Solution:
    def kthSmallest(self, arr: List[List[int]], k: int) -> int:
        """Sort k sorted lists using a min heap

        - use a heap of size (n_rows) to easily
        find the minimum value
        - decrement k instead of hold a count

        Time Complexity
            O(ln(n_rows)*k)
        Space Complexity
            O(n_rows)  n_rows in the heap

        References:
            https://docs.python.org/3/library/heapq.html
        """

        n_rows: int = len(arr)
        n_cols: int = len(arr[0])

        # Initialize heap (val, row_index, col_index)
        heap = [(arr[row_ix][0], row_ix, 0) for row_ix in range(n_rows)]
        heapq.heapify(heap)

        while k:

            value, row_ix, col_ix = heapq.heappop(heap)

            k -= 1

            # Check if more elements in the current row
            if col_ix + 1 < n_cols:
                heapq.heappush(heap, (arr[row_ix][col_ix + 1], row_ix, col_ix + 1)) 

        return value

if __name__ == '__main__':

    arr = [
       [ 1,  5,  9],
       [10, 11, 13],
       [12, 13, 15]
    ],
    k = 8

    obj = Solution()
    print(obj.kthSmallest(arr, k))