import heapq

class Solution:
    def count_then_sort(self, nums: List[int], k: int) -> List[int]:
        """Gather counts and sort by frequency

        - gather counts
        - sort the keys by their frequency

        Time Complexity
            O(n*log(n))
        Space Complexity
            O(n)
        """
        # Gather count of each number
        counts = {}
        for val in nums:
            counts[val] = counts.get(val, 0) + 1

        # Sort by frequency
        elements = sorted(counts.keys(), key=lambda k: counts[k], reverse=True)

        return elements[:k]

    def heap_largest(self, nums: List[int], k: int) -> List[int]:
        """Gather counts and use a heap to gather top k

        - gather counts
        - sort the keys by their frequency

        Time Complexity
            O(n*log(k))
        Space Complexity
            O(n)

        References:
            https://docs.python.org/3/library/heapq.html
        """
        # Gather count of each number
        counts = {}
        for val in nums:
            counts[val] = counts.get(val, 0) + 1

        # Add values to a heap
        # use key to gather count information
        return heapq.nlargest(k, counts.keys(), key=counts.get)
