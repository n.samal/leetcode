
"""

Inputs
    arr (List[ints]): unsorted array of values
    K (int): kth largest value to output

Outputs
    int: kth largest values

Notes
    - array can have duplicates

Strategy

    Heap
        - store k largest values into a heap
        - grab the smallest value

        Time O(n *log(k))
        Space O(k)

    Sort then grab

        - sorting O(n*log(n))
        - grab O(1)
    Quick Select

        - use the basis of quick sort to fnd the kth largest element
        - select a random pivot point
            
            + swap that point with the right most point
            + we leave it ot the side for now

        - iterate through all elements from 
            + track the number of values smaller than the pivot point
            + move smaller elements to the left side

                * swap with their original position to the next left_count

            + move the pivot point to one position right of the left_count

        - recursively apply this same logic
            + use reduced bounds each time to search for the kth value
            + we'll eventually search smaller and smaller bounds, until we have 1 element to search
"""

from typing import List
import heapq as hq


class Solution:

    def findKthLargest(self, arr: List[int], k: int) -> int:
        """Heap based approach

        Time Complexity
            O(n*log(k))
        Space Complexity
            O(k) for heap
        Notes:
            python doesn't limit the size of the heap, so we maintain all elements
        References:
            https://en.wikipedia.org/wiki/Heapsort
            https://www.geeksforgeeks.org/heap-sort/
        """

        k_largest = hq.nlargest(k, arr)

        return k_largest[-1]

    def findKthLargest2(self, arr: List[int], k: int) -> int:
        """Sorting approach

        Time Complexity
            O(n*log(n))
        Space Complexity
            O(1) for heap
        """

        # Sort in place
        arr.sort()

        # Grab kth value from back
        return arr[-k]


if __name__ == '__main__':

    # Example 2
    arr_in = [3, 2, 3, 1, 2, 4, 5, 5, 6]
    K = 4

    obj = Solution()
    print(obj.findKthLargest(arr_in, K))
    print(obj.findKthLargest2(arr_in, K))
