
"""

Inputs
    points (List[List[ints]]): array of point coordinates
    K (int): number of points to keep

Outputs
    List[List[int]]: k closest points to the origin

Notes
    - use euclidean distance
    - You may return the answer in any order.

Strategy
    
    - for each point determine the distance to the origin
    - keep a min heap of k values
    - push the current point with the distance to the heap

    - once finished iterating

        + only keep the points and not the distances
"""

from typing import List
import heapq as hq
import math


class Solution:
    def kClosest(self, points: List[List[int]], K: int) -> List[List[int]]:
        """Heap based approach

        Time Complexity
            O(n*log(n))
        Space Complexity
            O(n) for heap
        Notes:
            python doesn't limit the size of the heap, so we maintain all elements
        References:
            https://en.wikipedia.org/wiki/Heapsort
            https://www.geeksforgeeks.org/heap-sort/
        """
        # Create a heap
        h = []
        hq.heapify(h)

        # Iterate through all points
        for ix, point in enumerate(points):

            # Compute the distance to the origin
            dist = math.sqrt(point[0]**2 + point[1]**2)

            # Add point distance and index to the heap
            hq.heappush(h, (dist, ix))

        # Grab k closest poins based upon distance
        h = hq.nsmallest(K, h)

        # Grab the points
        closest = [points[ix] for dist, ix in h]

        return closest

    def kClosest_sorted(self, points: List[List[int]], K: int) -> List[List[int]]:
        """Compute distances then sort"""

        # Sort by distances
        # Time:  O(n*log(n))
        # Space: O(n)
        points_srt = sorted(points, key=lambda p: math.sqrt(p[0]**2 + p[1]**2))

        # Grab the first K points
        return points_srt[:K]

    def kClosest_track_sorted(self, points: List[List[int]], K: int) -> List[List[int]]:
        """Track the k top values

        Time Complexity
            O(n*log(K)) or O(n*K^2)
        Space Complexity
            O(K)

        Notes
            - this is too slow, requires sorting of k values at each of the n values
        """

        arr = []

        # Iterate through all points
        for ix, point in enumerate(points):

            # Compute the distance to the origin
            dist = math.sqrt(point[0]**2 + point[1]**2)

            # We're not yet at k values
            if len(arr) < K:
                arr.append((dist, point))
                arr = sorted(arr)

            # Check if the current point is closer
            # then our previous kth point
            else:

                if dist < arr[-1][0]:

                    # Replace the kth point
                    arr[-1] = (dist, point)

                    # Then sort again
                    arr = sorted(arr)

        # Grab only the points
        return [pt for dist, pt in arr]

    def kClosest_quick_sort(self, points: List[List[int]], K: int) -> List[List[int]]:
        """Use a quick sort pivot methodology to find the closest K items

        Time Complexity
            O(n*log(K)) or O(n*K^2)
        Space Complexity
            O(K)

        Notes
            - TODO: incomplete
        """

        arr = []

        # Iterate through all points
        for ix, point in enumerate(points):

            # Compute the distance to the origin
            dist = math.sqrt(point[0]**2 + point[1]**2)

            # We're not yet at k values
            if len(arr) < K:
                arr.append((dist, point))
                arr = sorted(arr)

            # Check if the current point is closer
            # then our previous kth point
            else:

                if dist < arr[-1][0]:

                    # Replace the kth point
                    arr[-1] = (dist, point)

                    # Then sort again
                    arr = sorted(arr)

        # Grab only the points
        return [pt for dist, pt in arr]


if __name__ == '__main__':

    # Example 1
    # points = [[1, 3], [-2, 2]]
    # K = 1

    # Example 2
    points = [[3, 3], [5, -1], [-2, 4]]
    K = 2

    obj = Solution()
    print(obj.kClosest(points, K))
    print(obj.kClosest_sorted(points, K))
