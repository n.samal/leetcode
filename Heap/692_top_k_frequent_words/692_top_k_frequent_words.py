from collections import Counter
import heapq as hq

class Solution:
    def sorting(self, words: List[str], k: int) -> List[str]:
        """Aggregate then sort
                
        Time Complexity
            O(n*lg(n))

                - O(n) for aggregating counts
                - O(n lg(n)) for sorting
        
        Space Complexity
            O(n)
        """
        
        # Use a counter to aggregate word counts
        counts = Counter(words)
        
        # Grab all the unique words
        words_uq = counts.keys()
        
        # Sort the list by frequency then alphabetically
        # we'll swap the frequency magnitude ie: 4 => -4 so it comes first
        # Then the next is the word itself
        words_srt = sorted(words_uq, key = lambda w: (-counts[w], w))
        
        # Only grab the first k items
        return words_srt[:k]

    def heap_based(self, words: List[str], k: int) -> List[str]:
        """Use heap for sorting
        
        
        Time Complexity
            - n words put into a heap of size
            - heap insertion: 
                O(n*log n) if heap is not limited
                O(n*log k) if heap was limited
            - heap pop
                O(k*log n) if heap is not limited
                O(k*log k) if heap was limited
            
        Space Complexity
            O(n)
            
            - count requires us to go through the whole list
        
        Notes
            - we can't limit the heap size in python
        """
        # Use a counter to aggregate word counts
        counts = Counter(words)
        
        # Grab all the unique words
        # words_uq = counts.keys()
        
        # Create a heap of the frequency and the word
        heap = [(-counts[word], word) for word in counts.keys()]
        hq.heapify(heap)
        
        # Heapify the heap and only grab the top k items
        # Only grab the word
        words_srt = [hq.heappop(heap)[1] for _ in range(k)]
        
        return words_srt
        
        