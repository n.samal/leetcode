"""
Inputs
    schedule (List[Intervals]): working times for each employee
Outputs
    List[intervals]: common intervals of free time for all employees
    in sorted order

Notes
    - 
    - don't include intervals of zero length ie: [5, 5]

Examples

    Example 1

        Input: schedule = [[[1,2],[5,6]],[[1,3]],[[4,10]]]
        Output: [[3,4]]
        Explanation: There are a total of three employees, and all common
        free time intervals would be [-inf, 1], [3, 4], [10, inf].
        We discard any intervals that contain inf as they aren't finite.    

    Example 2
        Input: schedule = [[[1,3],[6,7]],[[2,4]],[[2,5],[9,12]]]
        Output: [[5,6],[7,9]]

Manual

    times 1 2 3 4 5 6 7 8 9 10
    emp1  x       x 
    emp2  x x 
    emp3        x x x x x x

    we can use the upper bound as exclusive ie: 1 to 2, stops at 2

    times 1 2 3 4 5 6 7 8 9 10 11 12
    emp1  1 1       1 
    emp2    1 1 
    emp3    1 1 1         1  1  1   
Ideas

    - create an empty schedule
        + go over each employees schedule and mark off
        times that don't work
        + do this for all employees
        + then go over the schedule again to find openings

    - remove open ended times before 1, and after max time

    - sort all intervals by start time
        + grab values and 'merge' time slots
        + then find gaps between previous meeting and next meeting
"""

import heapq


class Interval:

    def __init__(self, start: int = None, end: int = None):
        self.start = start
        self.end = end

    def __repr__(self):
        return f'Interval({self.start},{self.end})'


class Solution:

    def heap_and_find(self, schedule: '[[Interval]]') -> '[Interval]':
        """Heap & find gaps

        - use heap to find minimum meeting start times
        - compare the last meeting end time to our start time
            + if there's a gap, then add a meeting

        Time Complexity
            O(n*ln(n))
        Space Complexity
            O(n)
        """

        heap = []

        # Add all intervals to a heap
        for emp in schedule:
            for time in emp:
                heapq.heappush(heap, (time.start, time.end))

        # Create open times
        times = []
        last_stop = None

        while heap:

            cur_start, cur_stop = heapq.heappop(heap)

            if not last_stop:
                last_stop = cur_stop

            # Meeting overlaps
            # use the longest meeting as our next limiter
            # ie: (0, 25) vs (1, 3)
            elif cur_start <= last_stop:
                last_stop = max(cur_stop, last_stop)

            else:
                times.append(Interval(last_stop, cur_start))
                last_stop = cur_stop

        return times


def get_intervals(schedule):

    schedule_new = []
    for emp_sched in schedule:
        emp = []

        for slot in emp_sched:
            emp.append(Interval(slot[0], slot[1]))

        schedule_new.append(emp)

    return schedule_new


if __name__ == '__main__':

    # Example 1
    # schedule = [[[1, 2], [5, 6]], [[1, 3]], [[4, 10]]]

    # Example 2
    schedule = [[[1, 3], [6, 7]], [[2, 4]], [[2, 5], [9, 12]]]

    schedule = get_intervals(schedule)

    obj = Solution()
    print(obj.heap_and_find(schedule))
