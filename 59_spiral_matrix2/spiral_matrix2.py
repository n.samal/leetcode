from typing import List


class Solution:

    def generateMatrix(self, n: int) -> List[List[int]]:
        """Create a square matrix of numbers that spiral

        Args:
            n (int): number of integers
        Returns:
            List[List[int]]: matrix of integers
        """

        # Single element
        if n == 1:
            return [[1]]

        # Number of elements
        n_elem = n**2

        # Create empty matrix
        arr = [[None for _ in range(n)] for _ in range(n)]

        # Set limits for row
        row_lim_low = 0
        row_lim_up = n - 1

        # Set limits for column
        col_lim_low = 0
        col_lim_up = n - 1

        # Initial positions
        row_ix = 0
        col_ix = 0

        # Initial direction
        row_step = 0
        col_step = 1

        # Array
        ix = 0

        # While elements left
        while ix < n_elem:

            # Save value
            arr[row_ix][col_ix] = ix + 1

            # Increment index
            ix += 1

            # Hit our right limit
            if col_ix + col_step > col_lim_up:

                # Reduce limits
                row_lim_low += 1

                # Start moving down
                col_step = 0
                row_step = 1

            # Hit our left limit
            elif col_ix + col_step < col_lim_low:

                # Reduce limit
                row_lim_up -= 1

                # Start moving down
                col_step = 0
                row_step = -1

            # Hit lower limit
            elif row_ix + row_step > row_lim_up:

                # Reduce limits
                col_lim_up -= 1

                # Start moving left
                row_step = 0
                col_step = -1

            # Hit upper limit
            elif row_ix + row_step < row_lim_low:

                # Reduce limits
                col_lim_low += 1

                # Start moving right
                col_step = 1
                row_step = 0

            # Movement
            row_ix += row_step
            col_ix += col_step

        return arr


    def loops(self, n: int) -> List[List[int]]:
        """

        - traverse each of the four directions
        - after movement in a direction, close it off
            + right (close top most row)
            + down (close right most col)
            + left (close bottom most row)
            + up (close leftmost col)

        Time Complexity
            O(n^2)
        Space Complexity
            O(n^2)
        """
    
        # Empty array
        arr = [[0 for _ in range(n)] for _ in range(n)]
        
        # Starting position and value
        r: int = 0
        c: int = 0
        v: int = 1
            
        # Bounds
        c_low: int = 0
        c_high: int = n
            
        r_low: int = 0
        r_high: int = n
            
        while v <= n**2:
            
            # Right
            for c in range(c_low, c_high):
                arr[r][c] = v
                v += 1
            
            r_low += 1
            
            # Down
            for r in range(r_low, r_high):
                arr[r][c] = v
                v += 1
            
            c_high -= 1
            
            # Left
            for c in range(c_high - 1, c_low - 1, -1):
                arr[r][c] = v
                v += 1
                
            r_high -= 1
                
            # Up
            for r in range(r_high - 1, r_low - 1, -1):
                arr[r][c] = v
                v += 1
                
            c_low += 1
        
        # print(arr)
        
        return arr


if __name__ == "__main__":

    # Single row
    # arr = [1, 2, 3, 4, 5]

    s = Solution()
    # print(s.generateMatrix(1))
    # print(s.generateMatrix(2))
    print(s.generateMatrix(3))
