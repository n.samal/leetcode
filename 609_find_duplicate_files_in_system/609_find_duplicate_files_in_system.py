"""
Inputs
    paths (List[str]): directory listing
Outputs
    List[List[str]]: 

Goals
    - find all the files with the same content
    - put those filepaths into groups along with their direcory
Examples
    Input:
    ["root/a 1.txt(abcd) 2.txt(efgh)", "root/c 3.txt(abcd)", "root/c/d 4.txt(efgh)", "root 4.txt(efgh)"]
    Output:  
    [["root/a/2.txt","root/c/d/4.txt","root/4.txt"],["root/a/1.txt","root/c/3.txt"]]

Ideas

    - iterate over each path 
        + gather the directory  ie: root/a 1.txt(abcd) => root/a
        + gather the files      ie: 1.txt(abcd) => 1.txt
        + gather their content  ie: 1.txt(abcd) => abcd

    - store files hashed by their content
        + add filepaths to a list
        + can use a defaultdict to save time

"""

from typing import List
from collections import defaultdict


class Solution:
    def findDuplicate(self, paths: List[str]) -> List[List[str]]:

        # key = content, value = filepaths
        dct = defaultdict(list)

        for path in paths:

            directory, files, contents = self.parse_dir(path)

            for ix in range(len(files)):
                dct[contents[ix]].append(directory + '/' + files[ix])

        # Only gather lists with at least 2 files
        duplicates = [group for group in dct.values() if len(group) >= 2]

        return duplicates

    def parse_dir(self, s: str):
        """Parse a string into 3 pieces

        - gather the directory
        - the files and their content
        """

        files = []
        contents = []
        directory = None

        indicators = set([' ', '(', ')'])

        for ix, char in enumerate(s):

            if char not in indicators:
                continue

            # File started and or directory ended
            if char == ' ':

                f_start = ix + 1

                if not directory:
                    directory = s[:ix]

            # Content start, file ending
            if char == '(':
                files.append(s[f_start:ix])
                c_start = ix + 1

            # Content ending
            elif char == ')':
                contents.append(s[c_start:ix])

        return directory, files, contents


if __name__ == '__main__':

    # Example 1
    paths = ["root/a 1.txt(abcd) 2.txt(efgh)", "root/c 3.txt(abcd)", "root/c/d 4.txt(efgh)", "root 4.txt(efgh)"]

    obj = Solution()
    print(obj.findDuplicate(paths))
