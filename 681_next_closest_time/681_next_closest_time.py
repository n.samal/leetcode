"""

Inputs
    time (str): time stamp

Outputs
    str: next greatest time stamp

Goal:
    - using only the digits provided find the next greatest time

Cases

    max_time = '23:59'
    min_time = '00:00'

Strategy
    - Manual

        + go through the string and see what digits are available
        + for the minutes places try to find a greater valid minutes

            * if yes => then use this greater minute with current hour
            * ie: 12:10 => 12:11

            * if not => then we must move to the hours place

                - use the smallest minute with the next greatest hour

                ie: 12:59 => 15:11
                    next greatest minute = 11
                    next greatest hour = 15

                - if no greater hour, then use the next smallest hour

                ie: 23 => 22

    - Brute Force

        + use the known digits, generate all possible times

            * remove times that are invalid ie: only hours between 00 and 23, minutes between 00 and 59


"""

import itertools


class Solution:

    def nextClosestTime(self, time: str) -> str:
        """Generate all minute and hour combinations

        from the combinations track the following
            minimum minute
            next greater minute

            minimum hour
            next great hour

        Use our manual logic to determine the next possible time

        Time Complexity
            O(1)
        Space Complexity
            O(1)
        """

        # Grab digits
        # 23:59
        digits = set([])

        # Add valid digits to our set
        for char in time:
            if char != ':':
                digits.add(int(char))

        # Sort digits so we start with smallest digits
        digits = sorted(digits)

        # Generate current hour and minute
        cur_hour = 10*int(time[0]) + int(time[1])
        cur_min = 10*int(time[3]) + int(time[4])

        # Calculate current time as number of minutes
        # current = (60 * cur_hour) + cur_min

        ''' Find next greater minute '''

        # Minute information
        min_min = float('inf')
        min_gtr = None

        print('Minutes')

        # Generate all minutes possible with digits available
        # https://docs.python.org/3.7/library/itertools.html#itertools.product
        for p1, p2 in itertools.product(digits, repeat=2):

            # '59' => 59
            time_i = 10*p1 + p2

            print(f' {time_i}')

            # Valid time
            if time_i < 60:

                min_min = min(min_min, time_i)

                # Save first time bigger than our current time
                if time_i > cur_min:
                    min_gtr = time_i
                    break

        # We found a greater minute
        if min_gtr:

            # Add the current hour to the greater minute
            return str(cur_hour).zfill(2) + ':' + str(min_gtr).zfill(2)

        ''' Find next greater hour '''

        # Minute information
        hour_min = float('inf')
        hour_gtr = None

        print('Hours')

        # Generate all minutes possible with digits available
        # https://docs.python.org/3.7/library/itertools.html#itertools.product
        for p1, p2 in itertools.product(digits, repeat=2):

            # '59' => 59
            time_i = 10*p1 + p2

            print(f' {time_i}')

            # Valid time
            if time_i < 24:

                hour_min = min(hour_min, time_i)

                # Save first time bigger than our current time
                if time_i > cur_hour:
                    hour_gtr = time_i
                    break

        # We found a greater hour
        if hour_gtr:

            # Add the greater hour to the minimum minute
            return str(hour_gtr).zfill(2) + ':' + str(min_min).zfill(2)

        # No greater hour or no greater minute
        # Use the minimum hour and minimum minute
        else:
            return str(hour_min).zfill(2) + ':' + str(min_min).zfill(2)

    def nextClosestTime2(self, time: str) -> str:
        """Generate all combinations

            - grab digits directly instead of in a loop

        Notes
            - actually a good bit slower
        """

        # Sort digits so we start with smallest digits
        digits = sorted([int(time[0]), int(time[1]), int(time[3]), int(time[4])])

        # Generate current hour and minute
        cur_hour = 10*int(time[0]) + int(time[1])
        cur_min = 10*int(time[3]) + int(time[4])

        # Calculate current time as number of minutes
        # current = (60 * cur_hour) + cur_min

        ''' Find next greater minute '''

        # Minute information
        min_min = float('inf')
        min_gtr = None

        print('Minutes')

        # Generate all minutes possible with digits available
        # https://docs.python.org/3.7/library/itertools.html#itertools.product
        for p1, p2 in itertools.product(digits, repeat=2):

            # '59' => 59
            time_i = 10*p1 + p2

            print(f' {time_i}')

            # Valid time
            if time_i < 60:

                min_min = min(min_min, time_i)

                # Save first time bigger than our current time
                if time_i > cur_min:
                    min_gtr = time_i
                    break

        # We found a greater minute
        if min_gtr:

            # Add the current hour to the greater minute
            return str(cur_hour).zfill(2) + ':' + str(min_gtr).zfill(2)

        ''' Find next greater hour '''

        # Minute information
        hour_min = float('inf')
        hour_gtr = None

        print('Hours')

        # Generate all minutes possible with digits available
        # https://docs.python.org/3.7/library/itertools.html#itertools.product
        for p1, p2 in itertools.product(digits, repeat=2):

            # '59' => 59
            time_i = 10*p1 + p2

            print(f' {time_i}')

            # Valid time
            if time_i < 24:

                hour_min = min(hour_min, time_i)

                # Save first time bigger than our current time
                if time_i > cur_hour:
                    hour_gtr = time_i
                    break

        # We found a greater hour
        if hour_gtr:

            # Add the greater hour to the minimum minute
            return str(hour_gtr).zfill(2) + ':' + str(min_min).zfill(2)

        # No greater hour or no greater minute
        # Use the minimum hour and minimum minute
        else:
            return str(hour_min).zfill(2) + ':' + str(min_min).zfill(2)

    def nextClosestTime3(self, time: str) -> str:
        """Use digits to generate all possible numbers

        - use the number of minutes for a simpler comparison

        Time Complexity
            O(1)
        Space Complexity
            O(1)
        Notes:
            speed is reduced, however has better McCabe complexity
        """

        # Grab digits
        # 23:59
        digits = set([])

        # Add valid digits to our set
        for char in time:
            if char != ':':
                digits.add(int(char))

        # Sort digits so we start with smallest digits
        digits = sorted(digits)

        # Generate current hour and minute
        cur_hour = 10*int(time[0]) + int(time[1])
        cur_min = 10*int(time[3]) + int(time[4])

        # Calculate current time as number of minutes
        cur_time = (60 * cur_hour) + cur_min

        # Minimums
        min_hr = float('inf')
        min_min = float('inf')

        # Generate all minutes possible with digits available
        # https://docs.python.org/3.7/library/itertools.html#itertools.product
        for h1, h2, m1, m2 in itertools.product(digits, repeat=4):

            # '59' => 59
            min_i = 10*m1 + m2
            hour_i = 10*h1 + h2

            # Check for valid times
            if hour_i < 24 and min_i < 60:

                # Compute number of minutes
                time_i = (60 * hour_i) + min_i

                print(f' {time_i}')

                # Track minimum hours and minutes
                min_min = min(min_min, min_i)
                min_hr = min(min_hr, hour_i)

                # We found a greater time
                if time_i > cur_time:
                    return str(hour_i).zfill(2) + ':' + str(min_i).zfill(2)

        # Use the minimum time
        return str(min_hr).zfill(2) + ':' + str(min_min).zfill(2)


if __name__ == '__main__':

    # Examples
    # Minute at max position
    time_in = '01:59'

    # Hour at max position
    time_in = '23:45'

    # Hour & Minute at max
    # time_in = '23:59'

    # General case
    # time_in = '10:23'
    # time_in = '19:34'
    # time_in = '18:42'
    time_in = '22:37'

    obj = Solution()
    # print(obj.nextClosestTime(time_in))
    print(obj.nextClosestTime3(time_in))
