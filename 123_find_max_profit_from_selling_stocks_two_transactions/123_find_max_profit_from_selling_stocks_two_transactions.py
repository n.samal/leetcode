"""
Inputs
    k (int): number of trades
    prices (List [int]): stock prices
Outputs
    int: maximum profit possible

Notes
    - you can complete at most two transactions
    - must sell our stock before we can trade again
    - we cannot engage in multiple transactions at once

Examples

Tricky Example

    prices = [1,2,4,2,5,7,2,4,9,0]

    if we break the prices into monotonically increasing
    ranges as our stack would do

    [1,2,4,
           2,5,7,
                  2,4,9,
                         0]

    profit = 4 - 1 = 3
    profit = 7 - 2 = 5
    profit = 9 - 2 = 7

    max_profit from only 2 transactions = 5 + 7 = 12

    However the answer is 13.
    If we wait to sell later we can get a larger profit

    [1,2,4,2,5,7,
                 2,4,9, 
                       0]

    profit = 7 - 1 = 6
    profit = 9 - 2 = 7

    max_profit = 6 + 7 = 13

Ideas

    Monotonically Increasing Stack

        - maintain a monotonically increasing stack
        - the max profit for some days is the max - min

        - track all the potential profits we can make
        - save the sum of the top 2 profits

        => doesn't work for all cases. fails, when we should
        wait through a market dip for a bigger pay day

    Splitting

        - we want to split our array into one or two pieces
            + try all split points

        - for each piece, we want to calculate the max profit
            + profit is not, max - min
            + profit is max_so_far - min_so_far

        - comments

            + does no translate to k transactions well
            + expensive
"""

from typing import List


class Solution:

    def split_array(self, prices: List[int]) -> int:
        """

        - try splitting at every point
        - compute the max profit from each section

        Time Complexity
            O(n^2)
        Space Complexity
            O(1)

        Notes
            - doesn't translate to more than 2 transaction well
            - is expensive
        """

        n: int = len(prices)
        max_profit: int = 0

        # Try Splitting the array at every index
        for ix in range(n):

            # Find max profit from left
            max_left: int = 0
            min_so_far: int = prices[0]

            for jx in range(1, ix):

                max_left = max(max_left, prices[jx] - min_so_far)

            # Find max profit from right
            max_right: int = 0
            min_so_far: int = prices[ix]

            for jx in range(ix + 1, n):

                max_right = max(max_right, prices[jx] - min_so_far)
                min_so_far = min(min_so_far, prices[jx])

            max_profit = max(max_profit, max_left + max_right)

        return max_profit

    def two_pass(self, prices: List[int]) -> int:
        """

        - iterate once to find the max profit starting from day 0
        and selling at day i (left to right)
        - iterate again to find the max profit starting at day i
        and selling later (right to left)

        - find the best combination of those two

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        Notes
            - doesn't translate to more than 2 transaction well
        """

        n: int = len(prices)
        max_profit: int = 0

        ''' Compute max profit from day 0 to day i '''
        max_left = [0 for _ in range(n)]
        min_so_far: int = prices[0]

        for ix in range(1, n):

            # Compute profit if buying on cheapest day and selling now
            max_left[ix] = max(max_left[ix - 1], prices[ix] - min_so_far)
            min_so_far = min(min_so_far, prices[ix])

        ''' Compute max profit from day i to day n '''

        max_right = [0 for _ in range(n)]
        max_so_far: int = prices[n - 1]

        for ix in range(n - 2, -1, -1):

            # Compute profit if buying now and selling at max price to the right
            max_right[ix] = max(max_right[ix + 1], max_so_far - prices[ix])
            max_so_far = max(max_so_far, prices[ix])

            # Best combo
            # selling on or before day ix, and buying on some day ix or after
            max_profit = max(max_profit, max_left[ix] + max_right[ix])

        return max_profit

    ''' Not Working but close'''

    def monotonic_stack(self, prices: List[int]) -> int:
        """

        - maintain a montonically increasing stack
        - compute the max profit for every range of
        increasing values
        - pick the top two profits

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        Notes
            - works for 90% of cases but fails in cases, when it's
            better to wait for a bigger pay out day
                + case = [1,2,4,2,5,7,2,4,9,0]
        """

        ''' Find the ranges of increasing prices '''

        profits = []
        stack = []

        for price in prices:

            if not stack:
                stack.append(price)

            # Increasing price
            elif stack and price >= stack[-1]:
                stack.append(price)

            # Price decrease
            else:

                # Compute the profit and save it
                max_val = stack[-1]
                min_val = stack[0]

                profits.append(max_val - min_val)

                # Reset stack with smallest price
                stack = [price]

        if stack:
            profits.append(stack[-1] - stack[0])

        ''' Find the two maximum profits '''

        if not profits:
            return 0

        profit1: int = 0
        profit2: int = 0

        # print(profits)

        # Find the two largest profits possible
        # cascade the max values down
        for profit in profits:

            if profit > profit1:
                profit2 = profit1
                profit1 = profit

            elif profit > profit2:
                profit2 = profit

        return profit1 + profit2


if __name__ == '__main__':

    # Example 1 (6)
    # prices = [3, 3, 5, 0, 0, 3, 1, 4]

    # Example 2 (Montonically increasing)
    # prices = [1, 2, 3, 4, 5]

    # Example 3 (Montonically decreasing)
    # prices = [7, 6, 4, 3, 1]

    # Tricky example (13)
    prices = [1, 2, 4, 2, 5, 7, 2, 4, 9, 0]

    obj = Solution()
    # print(obj.monotonic_stack(prices))
    print(obj.split_array(prices))
    print(obj.two_pass(prices))
