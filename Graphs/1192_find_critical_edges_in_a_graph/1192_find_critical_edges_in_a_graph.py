"""
Inputs
    n (int): number of servers in the network
    connections (List[int]): connections between servers
Outputs
    List[int]: critical connections

Notes
    - servers are numbered from 0 to n - 1
    - connection[i] represents a connection between a & b
    
    - any server can reach any other server directly or 
    indirectly through the network

    - a critical connection is one that if removed will
    prevent a server from reaching another serrver

Ideas

    - track which nodes are reachable for every node in the graph
        
        - see which nodes are only connected to one other node
        (this works for many cases but not all)

          x-----x 
         /      |
        x----x--x
        |
        x----x--x
        |       |
        x----x--x

    - brute force
        + remove every connection and see if the number of islands is increased
        + too expensive

Key Ideas

    - a graph is similar to a tree, however cycles can exist

    - if a node has

        + multiple paths of entry: it's a graph and there exists cycle in that set of nodes
        + singular path  of entry: it's a tree

    - try traversing the graph like we do with a tree using Depth First Search (DFS)

        + if during that search we encounter ancestor, then we know there a link back
        and therefore a cycle exists

        + if no backlink exists, then this infers a bridge

    - use Tarjans algorithm to track the lowest depth/link number for each node

        + low link is the smallest node that is accessible from the current node

References
    https://www.youtube.com/watch?v=aZXi1unBdJA
    https://www.youtube.com/watch?v=jFZsDDB0-vo
    https://leetcode.com/problems/critical-connections-in-a-network/discuss/550837/Step-by-step-explanation-for-dummies
    https://leetcode.com/problems/critical-connections-in-a-network/discuss/601695/Cleanest-and-Easiest-Understand-Python-Solution-99-Time-100-Mem
"""

from typing import List
from collections import defaultdict


class Solution:
    def criticalConnections(self, n: int, connections: List[List[int]]) -> List[List[int]]:
        """Tarjans algorithm

        - treat the graph like a directed graph and travel using DFS
            + group together nodes based upon the min depth node they touch

        - when adjacent nodes increase in depth, then a bride exists

        Time Complexity
            O(v + e)
        Space Complexity
            O(v)
        """

        # Collect all neighboring nodes
        self.graph = defaultdict(list)

        for start, stop in connections:
            self.graph[start].append(stop)
            self.graph[stop].append(start)

        # Perform depth first search and track
        # visited, and min reachable depth
        self.visited = [False for _ in range(n)]

        # Minimum depth node that can be reached through
        # a depth first traversal (where we assume a directed edge)
        self.min_depth = [float('inf') for _ in range(n)]

        self.ans = []
        self.dfs(0, None, 0)

        return self.ans

    def dfs(self, cur_node: int, prev_node: int, depth: int) -> int:

        self.visited[cur_node] = True
        self.min_depth[cur_node] = depth

        for neighbor in self.graph[cur_node]:

            # Don't go directly back to our previous node
            if neighbor == prev_node:
                continue

            # Find min depth accessible via neighbors
            if not self.visited[neighbor]:
                depth_neigh = self.dfs(neighbor, cur_node, depth + 1)
            else:
                depth_neigh = self.min_depth[neighbor]

            # Check if node doesn't reach backwards to ancestor
            # if it goes backwards the depth will be the same or smaller
            # if depth_neigh > self.min_depth[cur_node]:
            if depth_neigh > depth:
                self.ans.append([cur_node, neighbor])

            # Update the minimum depth from all neighbors
            self.min_depth[cur_node] = min(self.min_depth[cur_node], depth_neigh)

        return self.min_depth[cur_node]


if __name__ == '__main__':

    # Example 1 ([[1,3]])
    n = 4
    connections = [[0,1],[1,2],[2,0],[1,3]]

    # Example 2 ([[1,3]])
    n = 6
    connections = [[0,1],[1,2],[2,0],[1,3],[3,4],[4,5],[5,3]]

    # Example 3 ([])
    # n = 10
    # connections = [[1,0],[2,0],[3,0],[4,1],[5,3],[6,1],[7,2],[8,1],[9,6],[9,3],[3,2],[4,2],[7,4],[6,2],[8,3],[4,0],[8,6],[6,5],[6,3],[7,5],[8,0],[8,5],[5,4],[2,1],[9,5],[9,7],[9,4],[4,3]]

    obj = Solution()
    print(obj.criticalConnections(n, connections))
