"""
Inputs
    numCourses (int): number of courses to take
    prerequisites (List[int]): prereq for each course
Outputs
    List[int]: order of classes to take

Notes
    - prequisites are provided as [a, b]
        + we must take course b before taking course a

    - there are many valid answers, return any of them
    - return an ampty array for cases that don't work

Examples

    Example 1:

        Input: numCourses = 2, prerequisites = [[1,0]]
        Output: [0,1]
        Explanation: There are a total of 2 courses to take. To take course 1 you should have finished course 0. So the correct course order is [0,1].

    Example 2:

        Input: numCourses = 4, prerequisites = [[1,0],[2,0],[3,1],[3,2]]
        Output: [0,2,1,3]
        Explanation: There are a total of 4 courses to take. To take course 3 you should have finished both courses 1 and 2. Both courses 1 and 2 should be taken after you finished course 0.
        So one correct course order is [0,1,2,3]. Another correct ordering is [0,2,1,3].

    Example 3:

        Input: numCourses = 1, prerequisites = []
        Output: [0]

Hand Calc

    Example 2

        numCourses = 4,
        prerequisites = [[1,0],[2,0],[3,1],[3,2]]

        Course Requirements
         0: None
         1: 0
         2: 0
         3: 1, 2

        Edges
         - gather incoming, outgoing for each node

        0:
          in: [None] 
          out: [1, 2]
        1:
          in:  [0]
          out: [3]
        2:
          in: [0]
          out: [3]
        3:
          in: [1, 2]
          out: None

        - take all the with no requirements first
            ie: 0
        - take all the courses that only require 0
            ie: 1, 2

        - take the courses that only require what we've taken
            ie: 3 requires 1 & 2
Ideas
    - perhaps BFS/DFS and/or topological sorting
    - review all the edges going into and out of each node

        + if no incoming edges must be a starting point
        + add that node to our topological order
        + travel to adjacent nodes and them to our queue
            * reduce those incoming edges by 1

"""

from typing import List
from collections import defaultdict


class Solution:

    def findOrder(self, numCourses: int, prerequisites: List[List[int]]) -> List[int]:
        """

        Time Complexity
            O(v + e) each node and edge is only visited once
        Space Complexity
            O(v + e)
        """

        edges_in = [0 for _ in range(numCourses)]

        ''' Track outgoing edges for each course '''

        edges_out = defaultdict(list)

        for course, pre in prerequisites:
            edges_out[pre].append(course)
            edges_in[course] += 1

        ''' Find the starting nodes '''
        queue = []

        for node in range(numCourses):
            if edges_in[node] == 0:
                queue.append(node)

        ''' Iterate over nodes '''
        topo_sort = []
        visited = [False for _ in range(numCourses)]

        while queue:

            node = queue.pop()

            # Mark node and add to ordering
            visited[node] = True
            topo_sort.append(node)

            # Reduce edges in for adjacencies and check for new starting points
            for adj in edges_out[node]:

                edges_in[adj] -= 1

                if visited[adj] is False and edges_in[adj] == 0:
                    queue.append(adj)

        # Were we able to find a valid ordering?
        if len(topo_sort) == numCourses:
            return topo_sort
        else:
            return []


if __name__ == '__main__':

    # Example 1
    numCourses = 4
    prerequisites = [[1, 0], [2, 0], [3, 1], [3, 2]]

    obj = Solution()
    print(obj.findOrder(numCourses, prerequisites))
