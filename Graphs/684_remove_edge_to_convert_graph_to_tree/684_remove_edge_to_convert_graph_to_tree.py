"""
Inputs
    edge (List[List[int]]): undirected edge between nodes
Notes
    - edges are undirected
    - edges u, v are always provided in where u < v

    - a tree is an undirected graph with no cycles
    - find the edge that could be removed from the graph
    and result in a tree of N nodes
        + multiple values can exist
        + return the answer that is the last edge in the provided
        2D array
Ideas

    - Track the number of neighbors for each node
        + if a node has 3 numbers we need to remove an edge here
        + we want to remove the edge that is connected to another node 
        that is connected to the rest of the graph/ not a child

    - How to detect cycle when node does not have 3 neighbors?
        + use depth first search to detect a cycle/path??
        + we need to try and utilize a general approach

Key Ideas

    - Build the graph 1 edge at a time. Before we add a new edge, we check
    to see if there exists a path between the two nodes
"""

from collections import defaultdict

class Solution:

    def dfs(self, current: int, target: int) -> bool:
        """Crawl the path and look for a path between the two nodes

        - node does can be direct/indirect
        """

        if current == target:
            return True

        if current not in self.visited:
            self.visited.add(current)

            for neig in self.graph[current]:
                if self.dfs(neig, target):
                    return True
            self.visited.remove(current)

        return False

    def findRedundantConnection(self, edges: List[List[int]]) -> List[int]:
        """Add edges after they've been checked

        - only add a node once we've verified that a path does not exist
        between the nodes in our current edge

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        """

        self.graph = defaultdict(set)

        for u, v in edges:

            self.visited = set([])

            if u in self.graph and v in self.graph and self.dfs(u, v):
                return [u, v]
            else:
                self.graph[u].add(v)
                self.graph[v].add(u)
