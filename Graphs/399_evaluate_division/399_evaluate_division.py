"""
Inputs
    equations (List[List[str]]): top and bottom of each fraction
    values (List[float]): values for each equation
Outputs
    List[float]: results for each query
Notes

    - equations are provided as 
        equation[i] = [A_i, B_i]

        along with values[i]

        A_i / B_i = values[i]

        where A_i and B_i represents a single variable

    - queries are provided in a similar fashion

        queries[j] = [C_i, D_i]

        where we want to determine what

        C_i / D_i = j

    - Find the answer to all queries
    - if we cannot determine the query return -1.0

    - we can assume no contradictions and no divisions
    by zero

Examples

    Example 1:

        Input: equations = [["a","b"],["b","c"]], values = [2.0,3.0], queries = [["a","c"],["b","a"],["a","e"],["a","a"],["x","x"]]
        Output: [6.00000,0.50000,-1.00000,1.00000,-1.00000]
        Explanation: 
        Given: a / b = 2.0, b / c = 3.0
        queries are: a / c = ?, b / a = ?, a / e = ?, a / a = ?, x / x = ?
        return: [6.0, 0.5, -1.0, 1.0, -1.0 ]

    Example 2:

        Input: equations = [["a","b"],["b","c"],["bc","cd"]], values = [1.5,2.5,5.0], queries = [["a","c"],["c","b"],["bc","cd"],["cd","bc"]]
        Output: [3.75000,0.40000,5.00000,0.20000]

    Example 3:

        Input: equations = [["a","b"]], values = [0.5], queries = [["a","b"],["b","a"],["a","c"],["x","y"]]
        Output: [0.50000,2.00000,-1.00000,-1.00000]

Cases

    One

        a/a = 1

    Available
        query: a/b
        given: a/b

        use the given value

    Reciprocal
        query: a/b
        given: b/a 

        store the reciproal, or search for the reciprocal if the main
        value cannot be found

    Not Available

        query: a/e
        given: a/b

        we have no knowledge of e, and cannot calculate a value

        query: x/x = -1

        *Although x/x should be 1, be output -1 since we don't have the information for
        x available*

    Calculated

        - use the known information in combination to determine the query result

        query = a / c

        combination:
            a   x   y
            - * - * -
            x   y   c

        compute the product of each value

        on the first step search for a / c

        if a / c not available, search for other things we can use
        like a / x

        a   x
        - * -
        x   c

        use the x to see if we can find c from there
        if x / c not available, search for other things we can use

        exhaustive search our neighbors

Ideas

    - maintain a hash of the equations and their results for easy access

        + also maintain the reciprocal at the same time, this will allow
        for easy retrieval

        ie:  a/b = blah      so b/a = 1 / blah

        + if we store this in a tuple it doesn't allow for easy access of what
        variables are available

        ie: 'a/b' or (a, b)

        to check for the existence of a or b will be expensive

        + store these nested instead

        ie: graph['a']['b'] = value[i]

        we can use the hash keys to determine what variables are available in
        general, and those specific to 'a'

References:
    https://leetcode.com/problems/evaluate-division/discuss/88275/Python-fast-BFS-solution-with-detailed-explantion
"""

from collections import defaultdict
from typing import List


class Solution:
    def calcEquation(self, equations: List[List[str]], values: List[float], queries: List[List[str]]) -> List[float]:
        """

        Time Complexity
            O(q * n)

            all input equations could be visited for each query

        Space Complexity
            O(n) where n = number of numerators/denominators

            size of visited hash
            size of graph
            size of backtracking stack
        """

        n: int = len(queries)
        results = [-1.0 for _ in range(n)]

        # Store the equations as a hash
        dct = defaultdict(defaultdict)

        for (num, den), value in zip(equations, values):

            dct[num][den] = value
            dct[den][num] = 1.0 / value

        # Grab the result for each query if available
        for ix, query in enumerate(queries):

            visited = set([])
            results[ix] = self.backtrack(dct, query[0], query[1], 1.0, visited)

        return results

    def backtrack(self, dct: defaultdict, num: str, den: str, prior: float, visited: set ) -> float:
        """Use backtracking to compute the fractional value of a target query

        - check the graph for the query symbols
        - traverse neighboring equations in an effort to combine them
        - mark with variables have been explored
            + close to prevent loops
            + open them up after fully exploring that path

        Arg:
            dct (defaultdict): graph of numerator/denominator and the fractional value
            num (str): numerator
            den (str): target denominator
            prior (float): product of fractions so far
            visited (set): numerators previously explored
        Return:
            float: result of query if available
        """

        # Check for non existence
        if num not in dct or den not in dct:
            return -1.0

        # Same numerator & denominator
        elif num == den:
            return 1.0

        # Existing fraction
        elif num in dct and den in dct[num]:
            return dct[num][den] * prior

        # Try to compute our query with the available fractions
        # check all available denominators
        # then mark each path as explored
        else:

            for deno_neig in dct[num]:

                if deno_neig not in visited:

                    visited.add(deno_neig)
                    result = self.backtrack(dct, deno_neig, den, prior * dct[num][deno_neig], visited)
                    visited.remove(deno_neig)

                    if result != -1.0:
                        return result

        # We couldn't figure it out
        return -1.0


if __name__ == '__main__':

    # Example 1
    # equations = [["a","b"],["b","c"]]
    # values = [2.0,3.0]
    # queries = [["a","c"],["b","a"],["a","e"],["a","a"],["x","x"]]

    # Example 2
    # equations = [["a","b"],["b","c"],["bc","cd"]]
    # values = [1.5,2.5,5.0]
    # queries = [["a","c"],["c","b"],["bc","cd"],["cd","bc"]]

    # Example 3
    equations = [["a","b"]]
    values = [0.5]
    queries = [["a","b"],["b","a"],["a","c"],["x","y"]]

    obj = Solution()
    print(obj.calcEquation(equations, values, queries))
