"""
Inputs
    tickets (List[List[str]]): starting and ending airport for each ticket
Outputs
    List[str]: itinerary for a valid flight

Notes
    - all tickets use airports with 3 letter codes
    - there exists a valid itinerary
        + each ticket must be used only once
        + all tickets must be used
        + we must start from JFK

Example

    Example 1:

        Input: [["MUC", "LHR"], ["JFK", "MUC"], ["SFO", "SJC"], ["LHR", "SFO"]]
        Output: ["JFK", "MUC", "LHR", "SFO", "SJC"]

    Example 2:

        Input: [["JFK","SFO"],["JFK","ATL"],["SFO","ATL"],["ATL","JFK"],["ATL","SFO"]]
        Output: ["JFK","ATL","JFK","SFO","ATL","SFO"]
        Explanation: Another possible reconstruction is ["JFK","SFO","ATL","JFK","ATL","SFO"].
                    But it is larger in lexical order.

Ideas

    - topological sorting?
        + doesn't seem to work since we have many potentials

    - backtracking
        + create a graph from the tickets
        + sort the valid cities so we try the lexographically smallest
        one first
        + backtrack away, while we mask off used locations
"""

from collections import defaultdict
from typing import List


class Solution:
    def findItinerary(self, tickets: List[List[str]]) -> List[str]:
        """

        Time Complexity
            O(e^d)

            - every edge can be crawled multiple times, depending on the
            number of neighbors for each node

        Space Complexity
            O(v + e)

            - stack height can recurse to all edges
            - we store a bool array for masking
        """

        # Create a graph
        self.graph = defaultdict(list)

        for ix, (start, stop) in enumerate(tickets):
            self.graph[start].append((stop, ix))

        # Sort cities in alphabetical order
        # we want the lexigraphically smallest answer
        for start in self.graph:
            self.graph[start] = sorted(self.graph[start], key=lambda t: t[0])

        # Mark which tickets are currently in use
        self.ans = []
        self.tickets_used = [False for _ in range(len(tickets))]

        self.backtrack(len(tickets), [])

        return self.ans[0]

    def backtrack(self, tickets_left: int, path: List[str]):
        """Backtrack through routes

        Args:
            tickets_left (int): number of tickets left
            path (List[str]): cities in our route
        """

        # Already found our answer
        if self.ans:
            return

        if tickets_left == 0:
            self.ans.append(path)

        elif path:

            # Crawl to adjacent cities from last city
            start = path[-1]

            for stop, ix in self.graph[start]:

                if self.tickets_used[ix] is False:
                    self.tickets_used[ix] = True
                    self.backtrack(tickets_left - 1, path + [stop])
                    self.tickets_used[ix] = False

        # No previous path, but we must start from 'JFK'
        else:

            start = 'JFK'
            for stop, ix in self.graph[start]:

                if self.tickets_used[ix] is False:
                    self.tickets_used[ix] = True
                    self.backtrack(tickets_left - 1, [start, stop])
                    self.tickets_used[ix] = False


if __name__ == '__main__':

    # tickets = [["MUC", "LHR"], ["JFK", "MUC"], ["SFO", "SJC"], ["LHR", "SFO"]]
    tickets = [["JFK","SFO"],["JFK","ATL"],["SFO","ATL"],["ATL","JFK"],["ATL","SFO"]]

    obj = Solution()
    obj.findItinerary(tickets)