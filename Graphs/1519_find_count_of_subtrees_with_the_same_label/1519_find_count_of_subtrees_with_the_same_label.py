"""
Inputs
    n (int): number of nodes
    edges (List[List[int]]): edges between nodes
    labels (str): labels for each node
Outputs
    List[int]: count of descendants with the same label for each node

Notes
    - edges are undirected
    - there are no cycles
    - n nodes are provided
    - node i is labeled with labels[i]
    - node 0 is the root node

Ideas
    - track edges for each node
        + u -> v
        + v -> u

    - we need values for the subtree and not for the entire tree
        + we need to manipulate passed data and not use counts
        directly

        ie: counts of 'a' in subtree, not counts of 'a' global

    - crawl the subtree for each node, and track
    the counts of each label
        + depth first search, we want to crawl subtrees first
        + track visited nodes
        + aggregate data from subtrees
"""

from collections import defaultdict
from typing import List


class Solution:
    def countSubTrees(self, n: int, edges: List[List[int]], labels: str) -> List[int]:

        self.counts = [0 for _ in range(n)]  # counts of nodes with the same label
        self.seen = set([])                  # visited nodes
        self.labels = labels                 # node labels

        # Gather edges for each node
        self.graph = defaultdict(list)

        for u, v in edges:
            self.graph[u].append(v)
            self.graph[v].append(u)

        # Crawl from the root node
        self.dfs(0)

        return self.counts

    def dfs(self, parent: int):
        """Explore all edges

        - explore all edges for the current node
            + only travel where we haven't been before
        - track the label counts starting at the current node
            + then aggregate child counts

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        # Counts for this subtree
        label = self.labels[parent]
        counts = defaultdict(int)
        counts[label] = 1

        self.seen.add(parent)

        for child in self.graph[parent]:
            if child not in self.seen:
                counts_sub = self.dfs(child)

                # Add subtree counts to parent count
                for key in counts_sub:
                    counts[key] += counts_sub[key]

        # Save counts seen for this node and below
        self.counts[parent] = counts[label]

        return counts


if __name__ == '__main__':

    # Example 1
    # n = 7
    # edges = [[0, 1], [0, 2], [1, 4], [1, 5], [2, 3], [2, 6]]
    # labels = "abaedcd"

    # Example 2
    n = 4
    edges = [[0,1],[1,2],[0,3]]
    labels = "bbbb"

    # Example 3
    # n = 5
    # edges = [[0,1],[0,2],[1,3],[0,4]]
    # labels = "aabab"

    # Example 4
    # n = 6
    # edges = [[0,1],[0,2],[1,3],[3,4],[4,5]]
    # labels = "cbabaa"

    # Example 5
    # n = 7
    # edges = [[0,1],[1,2],[2,3],[3,4],[4,5],[5,6]]
    # labels = "aaabaaa"

    obj = Solution()
    print(obj.countSubTrees(n, edges, labels))
