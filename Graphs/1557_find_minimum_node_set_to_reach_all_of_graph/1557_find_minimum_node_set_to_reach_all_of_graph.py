"""
Inputs
    n (int):
    edges (List[int]): 
Outputs
    List[int]: what is the minimum set of nodes we need to visit all nodes in the graph

Notes
    - graphic is acyclic and directed
    - nodes are numbered from 0 to n - 1
    - edges indicate the start node and end node
    - a unique solution is guaranteed to exist

Hand Calc 1

    What's the best way to combine sets that cover all nodes [0, 1, 2, 3, 4, 5]

    Each node currently takes us

        0 => [1, 2, 5]
        1 => []
        2 => [5]
        3 => [4, 2, 5]
        4 => [2, 5]
        5 => []

    we probably want to ignore nodes that only go to 1 location
    ie: 1, 5, we can ignore those paths

    it may also benefit use to consider each node as visiting itself
    for our checking method

    0 => [0, 1, 2, 5]
    1 => [1]
    2 => [2, 5]
    3 => [3, 4, 2, 5]
    4 => [4, 2, 5]
    5 => [5]

    if 0 can go to 1, 2, 5 then we don't need those nodes

        - ignoring 1, 2, 5. we only
        have 3 & 4 as options left

        - 3 is the only node that can visit 3

Hand Calc 2

    n = 5, edges = [[0,1],[2,1],[3,1],[1,4],[2,4]]

        0 => [1, 4]
        1 => [4]
        2 => [1, 4]
        3 => [1, 4]


    0, 2, 3 can each visit 2 other nodes. They each cover almost the same nodes

    we need 2 to visit 2
    we need 3 to visit 3

    we could also write our graph backwards. 
    ie: which nodes can be visited by what other nodes

    0: [0]
    1: [0, 1, 2, 3]
    2: [2]
    3: [3]
    4: [0, 1, 2, 3, 4]

    the single length nodes we know must be visited 
    ie: 2, 3

    if we visit 2 and 3, then we will be able to
    visit 1 & 4

    ix      = [0, 1, 2, 3, 4]
    visited = [F, T, T, T, T]

    only zero is left, so we visit 0

Ideas

    - graph the graph in both directions
        + what nodes can we visit from node i? ie: 0 can reach many places
        + what nodes can visit node i?         ie: only node 0 can reach node 0

    - start with the nodes that can only visit them selves
        + crawl to their edges, and mark off all those nodes

        0  can visit 1 & 4

        + go to node that can be visited by the next smallest amount

"""

from collections import defaultdict

class Solution:
    def original(self, n: int, edges: List[List[int]]) -> List[int]:
        """

        - create a graph
            + track which nodes can visit whom
            + also track which nodes can be visited by other nodes
        
        - find the nodes that cannot by visited by other nodes aka
        the starting points

        Notes
            - too slow
            - the crawling unnecessary
        """

        # What is the smallest set of vertices can we use to visit all nodes
        nodes_all = set([ix for ix in range(n)])

        # Create graph
        graph = defaultdict(list)

        for start, stop in edges:
            graph[start].append(stop)

        ''' Crawl graph '''

        reaches = defaultdict(list)    # which nodes can we reach
        reached_by = defaultdict(list) # who can reach this node

        # Travel from each vertice and understand the 
        for node in range(n):

            queue = [node_next for node_next in graph[node]]

            while queue:

                node_next = queue.pop()

                # Save links
                reaches[node].append(node_next)
                reached_by[node_next].append(node)

                # Move to other edges
                if node_next in graph:
                    for node_next2 in graph[node_next]:
                        queue.append(node_next2)

        ''' Track which nodes can reach each key '''

        # Sort by nodes which can be visited the least
        # (largest to smallest)

        ans = []

        for node in range(n):
            if len(reached_by[node]) == 0:
                ans.append(node)

        return ans

    def find_starts(self, n: int, edges: List[List[int]]) -> List[int]:
        """

        - we don't need to crawl the tree to find the starting points
            + the starting points are never referenced as a 'stop'
        - create a set of points for all nodes
            + remove the nodes that we can vist from another node

        Time Complexity
            O(n + e)
        Space Complexity
            O(n)
        """

        nodes = set([ix for ix in range(n)])
        
        # Remove the node if it can be visited
        for start, stop in edges:
            nodes.discard(stop)

        return list(nodes)
