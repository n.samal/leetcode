"""
Inputs
    node (Node): head node of graph
Outputs
    Node: head node of new graph

Notes
    - create a deep copy of the graph
        + nodes must be new nodes and not refernce the old node

    - nodes are connected and undirected
    - every value is unique
    - no repeated edges or loops
    - all nodes can be visited from the root node

Ideas
    - maintain a hash of references
        + we can convert the old node to the new node

    - if we've never been to node create a new node with the same value

        + go through all the adjacancies and save their references

    - if we've been to a node just return the mirrored version

"""

# Definition for a Node.
class Node:
    def __init__(self, val = 0, neighbors = None):
        self.val = val
        self.neighbors = neighbors if neighbors is not None else []

class Solution:
    def cloneGraph(self, node: 'Node') -> 'Node':

        # Crawl the tree and create a new node if it hasn't been created
        # save a conversion table referecning the old node to the new node
        # every time we see the old node being reference, just reference the new node instead

        self.mirror = {}

        return self.recurse(node)

    def recurse(self, node: 'Node') -> 'Node':
        """Crawl and copy

        - if we've been to a node before we've copied it
            + just return the mirrored node

        - if the first visit then create a copy

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        if node:

            # Create a copy if it doesn't exist
            if node in self.mirror:
                node_mir = self.mirror[node]
            else:
                node_mir = Node(node.val)
                self.mirror[node] = node_mir

                # Travel to adjacencies
                for neighbor in node.neighbors:
                    node_mir.neighbors.append(self.recurse(neighbor))

            return node_mir
