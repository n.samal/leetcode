"""
Inputs
    robot (obj): interface to robot API
Outputs 
    mis: depending on API method called

Notes

    - our robot can be accessed via 4 API calls
        + move
        + turnLeft
        + turnRight
        + clean

    - the room can be modeled as a grid
    - we don't know the position of the robot
    - when the sensor detects and obstacle it will
    not move to that blocked position

    aka stays at the current cell

    - the initial robot positon is always accessible
    - the initial heading is facing north
    - all accessible cells are marked as 1

Examples

    Input:
        room = [
          [1,1,1,1,1,0,1,1],
          [1,1,1,1,1,0,1,1],
          [1,0,1,1,1,1,1,1],
          [0,0,0,1,0,0,0,0],
          [1,1,1,1,1,1,1,1]
        ],
        row = 1,
        col = 3

    Explanation:
    All grids in the room are marked by either 0 or 1.
    0 means the cell is blocked, while 1 means the cell is accessible.
    The robot initially starts at the position of row=1, col=3.
    From the top left corner, its position is one row below and three columns right.

Ideas

    - we should clean at every position we're capable of reaching
    
    - track the heading of the robot to determine the direction of movement
    - track the positions explored relative to our start location
        + use the four possible connection directions as potential locations to explore
        + track which positions were found to be open/closed, already cleaned
        + avoid visiting nodes we've been to before

    - we should NOT exclusively visit nodes we haven't visited. we may need to travel
    across visited nodes to get to our next desired location
    
    - continue searching while potential unvisited nodes left to explore

    - we could clean the current location then we search adjacent positions to see if they're open
        + to check we must rotate then use the move command
        + we could check all 4 adjacencies for potential by rotating however this would not clean them

              1
            1 x 1
              1

        we would later need to explore those positions, however upon moving we could find more unvisited
        nodes closer to us... dilemmas
    
    - we could prioritize nodes near our current location
        
        + using an A* like distance metric could be expensive since we'd need to find the closest position
        to the robot each time. The robot is always moving so the distance to all potential nodes would need to be recalculated

        + instead use a depth-first search type of approach, just greedily grabbed/navigate to the unvisited 
        nodes closest to our current position

            * choosing to navigate to the closest univisted position near us can be a more complicated methods
            
            * instead choose to backtrack to where we we just came from (simpler logic)

"""

# Robot control interface
class Robot:
   def move(self):
       """
       Returns true if the cell in front is open and robot moves into the cell.
       Returns false if the cell in front is blocked and robot stays in the current cell.
       :rtype bool
       """

   def turnLeft(self):
       """
       Robot will stay in the same cell after calling turnLeft/turnRight.
       Each turn will be 90 degrees.
       :rtype void
       """

   def turnRight(self):
       """
       Robot will stay in the same cell after calling turnLeft/turnRight.
       Each turn will be 90 degrees.
       :rtype void
       """

   def clean(self):
       """
       Clean the current cell.
       :rtype void
       """

from typing import List


class Solution:
    def cleanRoom(self, robot):
        """
        :type robot: Robot
        :rtype: None
        """

        self.robot = robot  # robot interface
        # self.pos = [0,0]    # (row, col) of robot
        # self.heading = 0    # 0 = north, 1 = west, 2 = south, 3 = east (CCW+)
        self.dirs = ((+1, 0), (0, -1), (-1, 0), (0, +1)) # relative position deltas
        self.visited = set([])

        self.dfs(0, 0, 0)

    def dfs(self, row: int, col: int, heading: int):
        """Depth first search

        - crawl to the node right in front of us
            + new position is relative to our current positon
            and our current heading

        - after each dive, we reset our heading and position
            + this is extra steps, but simplfies the logic
            + logic for tracking current heading is removed

        Args:
            row (int): row of current robot position
            col (int): column of current robot position
            heading (int): heading of robot

        Time Complexity
            O(n)
        Space Complexity
            O(n)  recursion 
        """

        # Clean current position and mark as visited
        self.robot.clean()
        self.visited.add((row, col))

        # Check all adjacent positions/directions
        for dir_ix in range(4):

            heading_new = (heading + dir_ix) % 4
            r_new = row + self.dirs[heading_new][0]
            c_new = col + self.dirs[heading_new][1]

            # Explore unvisited nodes in front of us first
            if (r_new, c_new) not in self.visited:
                moved = self.robot.move()

                if moved:
                    self.dfs(r_new, c_new, heading_new)
                    self.reset()
            
            # Update heading with direction index
            # heading must match desired position for the move() check to work
            self.robot.turnLeft()


    def reset(self):
        """Reset our robot heading and position"""

        # Turn 180 and go back to prior position
        self.robot.turnLeft()
        self.robot.turnLeft()

        self.robot.move()

        # Reset heading (extra steps but much simpler logic)
        self.robot.turnLeft()
        self.robot.turnLeft()
