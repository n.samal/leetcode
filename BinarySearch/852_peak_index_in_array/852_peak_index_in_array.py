class Solution:
    def peakIndexInMountainArray(self, arr: List[int]) -> int:
        """Linear Scan

        - find when the array starts decreasing, this is the index

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        n: int = len(arr)

        for ix in range(1, n):

            # Decreasing begins
            if arr[ix] < arr[ix - 1]:
                return ix - 1


    def binary_search(self, arr: List[int]) -> int:
        """Use binary search to speed up

        Time Complexity
            O(log(n))
        Space Complexity
            O(1)
        """
        n: int = len(arr)
        low: int = 0
        high: int = n
        
        while low < high:

            # Compute mid point
            mid = (low + high) // 2

            # Check if midpoint is peak
            if arr[mid] > arr[mid + 1] and arr[mid] > arr[mid - 1]:
                return mid

            # Values are increasing still, peak is to the right
            elif arr[mid] > arr[mid - 1]:

                low = mid + 1

            # Peak is to the left
            else:

                high = mid

        return low

    def binary_search2(self, arr: List[int]) -> int:
        """Use binary search to speed up

        - modify if statements for speed up

        Time Complexity
            O(log(n))
        Space Complexity
            O(1)
        """
        n: int = len(arr)
        low: int = 0
        high: int = n
        
        while low < high:

            # Compute mid point
            mid = (low + high) // 2

            # Values are increasing still
            if arr[mid] > arr[mid - 1]:

                # Right side decreasing
                if arr[mid] > arr[mid + 1]:
                    return mid
                
                # Peak is to the right
                else:
                    low = mid + 1

            # Peak is to the left
            else:
                high = mid

        return low