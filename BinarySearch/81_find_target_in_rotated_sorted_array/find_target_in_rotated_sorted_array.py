"""
Inputs
    arr (List[int]): array sorted in ascending order is rotated at some pivot unknown
    target (int): target value
Outputs
    bool: does target exist in the array
Notes

    - pivot position is unknown
    - duplicates can exist in the array

Examples

    Example 1:

        Input: nums = [2,5,6,0,0,1,2], target = 0
        Output: true

    Example 2:

        Input: nums = [2,5,6,0,0,1,2], target = 3
        Output: false

Ideas

    - search for the pivot index, then search within bounds

        arr = [low    pivot   high]

        pivot can be within bounds: low <= pivot <= high

    - pivot can be found with linear search

        value[i] > arr[i + 1]

        [3, 4, *6*, 0]

        pivot is the right

    - apply binary search to find the pivot point

Cases

    Array

        Pivot at left most position

            [*0*, 0, 0, 1, 2, 3, 4, 5]

        Pivot at right most position

            [1, 2, 3, 4, 5, *1*]

        Pivot at middle position

            [4, 5, *0*, 1, 2, 3, 4]

Key Points

    - finding the pivot point via binary search is disrupted
    here because of duplicate values
    - use binary search logic along with the known target range
    to exclude the correct bounds
"""

from typing import List

''' Find Pivot Index '''


def get_pivot_index_linear(arr: list):
    """Linear search for the pivot index

    Time Complexity
        O(n)
    Space Complexity
        O(1)
    """

    n: int = len(arr)

    if n == 0:
        raise ValueError('No Pivot exist')

    if n == 1:
        return 0

    for ix in range(n - 1):

        if arr[ix] > arr[ix + 1]:
            return ix + 1

    return 0


def get_pivot_index_binary_search2(arr: List[int]):
    """

    Cases

        pivot on left
        pivot on right
        pivot in middle

    Notes
        - works for many cases but not all
        - this runs into issues to the occurence
        of duplicate values!!

        see below
    """
    n: int = len(arr)

    low: int = 0
    high: int = n - 1

    # Already sorted
    # ie: [0, 0, 0, 1, 2, 3, 4]
    if arr[low] < arr[high]:
        return low

    while low <= high:

        mid = low + ((high - low) // 2)

        print(f'low: {low}  high: {high}   mid: {mid}')

        # Smaller value to our right
        if mid + 1 < n and arr[mid + 1] < arr[mid]:
            return mid + 1

        # Larger value to left
        elif mid - 1 >= 0 and arr[mid - 1] > arr[mid]:
            return mid

        # Values to the left are in order
        # move right
        # ie: [0, 1, 2, *3*, 4, 0, 0]
        elif arr[low] < arr[mid]:
            low = mid + 1

        # Values to the right are in order
        # move left
        # ie: [7, 1, 2, *3*, 4, 5, 6]
        else:
            high = mid - 1

    print(f'low: {low}  high: {high}   mid: {mid}')

    return mid


''' Binary Search '''


def binary_search_og(arr: list, target: int, low: int, high: int, closest: bool = False):
    """Find the indice of the target value in the array

    Time Complexity:
        O(log(n))
    Space Complexity
        O(1)

    Args:
        arr (List[int]): sorted array of values
        target (int): value to search for
        low (int): lower index of search range (inclusive)
        high (int): upper index of search range (inclusive)
        closest (bool): find the closest index

    Returns:
      int: indice of the target value
    """

    while low <= high:

        mid = (low + high) // 2

        print('low:', low, 'high:', high, 'mid:', mid)

        # Found our value
        if arr[mid] == target:
            return mid

        # Value too small
        # bump start point up
        elif arr[mid] < target:
            low = mid + 1

        # Value too large
        # bump mid point down
        else:
            high = mid - 1

    if closest:
        return low
    else:
        return -1


''' Target Search '''


def find_target_linear(arr: list, target: int) -> bool:
    """Find the target in a rotated and sorted array

    - use a linear search to find the pivot
    - then use binary search to find existence

    Args:
        arr (list): array of values that are sorted and rotated
        target (int): integer value to search for
    Return:
        bool: if target is in array

    Time Complexity
        O(n)
    Space Complexity
        O(1)
    """

    # Length of array
    n: int = len(arr)

    if n == 0:
        return False

    elif n == 1:

        if arr[0] == target:
            return True
        else:
            return False

    # Find the pivot index
    # pivot = get_pivot_index_binary_search1(arr)
    pivot = get_pivot_index_linear(arr)
    # print('pivot:', pivot)

    # Use binary search on each half
    if arr[pivot] <= target <= arr[n - 1]:
        value = binary_search_og(arr, target, pivot, n - 1)
    else:
        value = binary_search_og(arr, target, 0, pivot - 1)

    # Output bool
    if value >= 0:
        return True
    else:
        return False


def find_target_combined(arr: list, target: int) -> bool:
    """Search for the target while using binary search bounds

    - use the bounds to determine if a range is in order

        [low   mid   high]

        + low <= mid : in order
        + mid <= high: in order

    - if one side is in order
        + see if the target is in range, then search for it
        + or exclude that whole range, since target can't be there

    Args:
        arr (list): array of values that are sorted and rotated
        target (int): integer value to search for
    Return:
        bool: if target is in array

    Time Complexity
        O(n)
    Space Complexity
        O(1)
    """

    # Length of array
    n: int = len(arr)

    if n == 0:
        return False

    elif n == 1:

        if arr[0] == target:
            return True
        else:
            return False

    low: int = 0
    high: int = n - 1

    while low <= high:

        mid = low + ((high - low) // 2)

        if arr[mid] == target:
            return True

        # Left side is in order
        # ie: [1, 2, 3, *4*, 5]
        elif arr[low] <= arr[mid]:

            if arr[low] < target < arr[mid]:
                high = mid - 1
            else:
                low = mid + 1

        # Right side is in order
        else:

            if arr[mid] < target <= arr[high]:
                low = mid + 1
            else:
                high = mid - 1

    return False


if __name__ == '__main__':

    # arr_in = [2, 5, 6, 0, 0, 1, 2]
    # arr_in = [1]
    # arr_in = [1, 1]
    # arr_in = [3, 1]
    arr_in = [1, 3, 1, 1, 1]

    print(get_pivot_index_linear(arr_in))
    print(get_pivot_index_binary_search2(arr_in))

    # print(find_target(arr_in, 0))
    # print(find_target(arr_in, 1))
    # print(find_target(arr_in, 3))
    # print(find_target(arr_in, 8))
