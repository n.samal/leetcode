"""
Notes

    - mountain array is one that is increasing, then decreasing
    - mountain array is at least of length 3

    - our goal is to find the minimum index where our target value occurs
    - if there is no such index, then return -1 

    You can't access the mountain array directly.  You may only access the array using a MountainArray interface:

        MountainArray.get(k) returns the element of the array at index k (0-indexed).
        MountainArray.length() returns the length of the array.

Examples

    Example 1:

        Input: array = [1,2,3,4,5,3,1], target = 3
        Output: 2
        Explanation: 3 exists in the array, at index=2 and index=5. Return the minimum index, which is 2.

    Example 2:

        Input: array = [0,1,2,4,2,1], target = 3
        Output: -1
        Explanation: 3 does not exist in the array, so we return -1.

Ideas

    - seems similar to 'find target in rotated array'
    - use binary search to find the peak then search both halves
    - can we search simulatenously 

Key Points

    - finding the peak index can be tricky
        + the peak occurs when values to both sides are less

        + comparing low vs mid doesn't seem to work well
        instead we compare mid vs mid + 1

        ie: [3, 5, 3, 2, 1]

        here low == mid

"""

# """
# This is MountainArray's API interface.
# You should not implement it, or speculate about its implementation
# """
#class MountainArray:
#    def get(self, index: int) -> int:
#    def length(self) -> int:

class Solution:
    def findInMountainArray(self, target: int, mountain_arr: 'MountainArray') -> int:
        """

        - find the peak index then binary search on the two halves

        Time Complexity
            O(log(n))
        Space Complexity
            O(1)
        """

        n: int = mountain_arr.length()
        self.ans: int = float('inf')

        # Find the peak index
        # One section is monotonically increasing
        # second section is monotonically decreasing
        p = self.get_peak_index(mountain_arr, 0, n - 1)

        # print(f'peak: {p}')

        # Find the smallest occurence of the target
        self.binary_search(target, mountain_arr, 0, p)
        self.binary_search_rev(target, mountain_arr, p, n - 1)

        if self.ans == float('inf'):
            return -1
        else:
            return self.ans

    def get_peak_index(self, mountain_arr, low: int, high: int):

        while low <= high:

            mid = low + ((high - low) // 2)

            # We're increasing, go right
            if mountain_arr.get(mid) < mountain_arr.get(mid + 1):
                low = mid + 1

            # We're decreasing, go left
            else:
                high = mid - 1

        return low

    def binary_search(self, target: int, mountain_arr, low: int, high: int):
        """Increasing array binary search """

        while low <= high:

            mid = low + ((high - low) // 2)

            mid_v = mountain_arr.get(mid)

            # Found the element, save it and move left
            if mid_v == target:
                self.ans = min(mid, self.ans)
                high = mid - 1

            elif target < mid_v:
                high = mid - 1
            else:
                low = mid + 1

    def binary_search_rev(self, target: int, mountain_arr, low: int, high: int):
        """Decreasing array binary search """

        while low <= high:

            mid = low + ((high - low) // 2)

            mid_v = mountain_arr.get(mid)

            # Found the element, save it and move left
            if mid_v == target:
                self.ans = min(mid, self.ans)
                high = mid - 1

            elif target < mid_v:
                low = mid + 1
            else:
                high = mid - 1
