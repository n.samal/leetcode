def square_int(value: int) -> int:
    """Compute the square root of the value but only return the integer

    Use binary search to search for the value

    Space Complexity
        O(1)
    Time Complexity
        O(log(n))

    Args:
        value (int): value to root
    Returns:
        int: integer portion of the values root
    """

    if value in [0, 1]:
        return value

    # Define bounds
    low: int = 1
    high: int = value

    while low < high:

        # Calculate mid point
        mid = (low + high) // 2

        print('low:', low, 'high:', high, 'mid:', mid)

        # Squared The term
        sq = mid**2

        # We hit our value dead on!
        # or mid point isn't moving (integer math is to the floor
        # so the relevant value is low)
        if sq == value or low == mid:
            return mid

        # Midpoint too small
        elif sq < value:
            low = mid

        # Midpoint too large
        else:
            high = mid

    return low


if __name__ == "__main__":

    print(square_int(100))
    print(square_int(64))
    print(square_int(25))
    print(square_int(9))
    print(square_int(8))
    print(square_int(7))
    print(square_int(4))


