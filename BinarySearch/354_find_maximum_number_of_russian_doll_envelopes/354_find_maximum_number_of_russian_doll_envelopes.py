"""
Inputs
    envelopes (List[List[int]]): envelope sizes
Outputs
    int: max number of envelopes you can russian doll
Notes
    - rotation is not allowed
    - an envelope can only fit when both the width and height
    is smaller than the encompassing envelopes height and width

Ideas
    - seems like a variation on longest increasing subsequence
    - if we sort the array it may help
    - like longest increasing subsequence uses piles
        + track the piles of various lengths
        + for each pile save the smallest envelope
        used to create such a pile

    - piles exactly as before may not work
        + we have two dimensions to ensure

Example Case

    values are not sorted yet

    envs = [[2,100], [3,200], [4,300], [5,500], [5,400], [5,250], [6,370], [6,360], [7,380]]

    values sorted by 1st dimension, then second

    envs = [[2,100], [3,200], [4,300], [5,250], [5,400], [5,500], [6,360], [6,370], [7,380]]

    start our sequence with the smallest values

    [2, 100]

    add onto it value by value if possible

    [2,100], [3,200], [4,300]

    if we add the next largest values [5, 400] we're unable to build any longer chain ontop because
    of the large height

    initial chain = [2,100], [3,200], [4,300], [5,400]

    however if we had skipped [4, 300], we could build a chain like

    improved chain = [2,100], [3,200], [5,250], [6,360], [7,380]

    we can sort ascending in the first dimension, then descending in the second dimension
    we get

    [[2,100], [3,200], [4,300], [5,500], [5,400], [5,250], [6,370], [6, 360], [7,380]]

    extracting the dimensions separately
    width  : [2,    3,   4,   5,   5,   5,   6,   6,    7]
    heights: [100, 200, 300, 500, 400, 250, 370, 360, 380]

    since the widths are in increasing order, we know every envelope to the right has equal or larger width
    for the heights if use Longest Common Subsequence we can find the correct russian doll sequence

    we rearrange the second dimension in decreasing order to avoid stacking envelopes of the same width
    but a larger height on top of each other

    ie:
    heights_asc_desc = [100, 200, 300, 500, 400, 250, 370, 360, 380]
    longest subsequence = [100, 200, 250, 360, 380]

    if we did the normal sort with ascending, ascending you get

    heights_asc_desc = [100, 200, 300, 250, 400, 500, 360, 370, 380]
    longest subsequence = [100, 200, 250, 360, 370, 380]

    note that heights of 360 and 380 are of the same width, 6. this is not
    possible with our problem definition. Rearranging the second dimension
    descending makes this a nonissue
"""

from typing import List


class Solution:

    def two_loops(self, envelopes: List[List[int]]) -> int:
        """Double loop

        - sort sequences
        - track the longest chain possible using each envelop
        - compare each sequence against previous best sequenece
            + add onto that chain if possible

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        """

        if not envelopes:
            return 0

        max_global: int = 1
        n: int = len(envelopes)
        envelopes.sort()

        # Longest chain possible using each envelope
        sequences = [1 for _ in range(n)]

        def is_smaller(env1: List[int], env2: List[int]) -> bool:
            """Compare envelopes"""
            return env1[0] < env2[0] and env1[1] < env2[1]

        for ix in range(n):
            for jx in range(0, ix):

                # We're bigger than our previous envelope, add to the chain
                if is_smaller(envelopes[jx], envelopes[ix]):
                    sequences[ix] = max(sequences[ix], sequences[jx] + 1)

                    max_global = max(max_global, sequences[ix])

        return max_global

    def bisect_tails(self, envelopes: List[List[int]]) -> int:
        """Longest increasing subsequence

        - sort sequences
        - using binary search to find the longest sequence 
        on the heights
            + widths are already in the correct order

        Time Complexity
            O(n*ln(n))
        Space Complexity
            O(n)
        """

        if not envelopes:
            return 0

        n: int = len(envelopes)
        ans: int = 1

        # Put widths in ascending, heights in ascending
        envelopes.sort(key=lambda x: (x[0], -x[1]))

        # Update smallest envelope to chain of length 1
        sequences = [float('inf') for _ in range(n)]
        sequences[0] = envelopes[0][1]

        for ix, env in enumerate(envelopes):

            low = 0
            high = ix

            while low <= high:

                mid = low + ((high - low) // 2)

                # We're bigger than our guess, let's try larger
                if env[1] > sequences[mid]:
                    low = mid + 1
                else:
                    high = mid - 1

            # Save the chain, if it's smaller than our previous limiting envelope
            # lengths were zero based indices, so we add 1 to the count
            if env[1] < sequences[low]:
                sequences[low] = env[1]
                ans = max(low + 1, ans)

        return ans

    def bisect_tails2(self, envelopes: List[List[int]]) -> int:
        """Longest increasing subsequence

        - organize by width then by 
        - build sequence list as we go

        Time Complexity
            O(n*ln(n))
        Space Complexity
            O(n)
        """

        if not envelopes:
            return 0

        # Put widths in ascending, heights in ascending
        # envelopes.sort(key=lambda x: (x[0], x[1]))
        envelopes.sort(key=lambda x: (x[0], -x[1]))

        # Update smallest envelope to chain of length 1
        sequences = [envelopes[0][1]]

        for width, height in envelopes:

            low = 0
            high = len(sequences) - 1

            while low <= high:

                mid = low + ((high - low) // 2)

                # We're bigger than our guess, let's try larger
                if height > sequences[mid]:
                    low = mid + 1
                else:
                    high = mid - 1

            # Save the chain, if it's smaller than our previous limiting envelope height
            # Add the length if it's a chain of new length
            if low < len(sequences):
                sequences[low] = height
            else:
                sequences.append(height)

        return len(sequences)


if __name__ == '__main__':

    # envelopes = [[5, 4], [6, 4], [6, 7], [2, 3]]
    envelopes = [[2, 100], [3, 200], [4, 300], [5, 500], [5, 400], [5, 250], [6, 370], [6, 360], [7, 380]]

    obj = Solution()
    # print(obj.two_loops(envelopes))
    # print(obj.bisect_tails(envelopes))
    print(obj.bisect_tails2(envelopes))
