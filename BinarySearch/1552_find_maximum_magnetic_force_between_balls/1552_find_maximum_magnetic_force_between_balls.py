"""
Inputs
    positions (List[int]): available basket locations
    m (int): number of balls to distribute
Outputs:
    int: minimum force
Notes
    - need to distribute m balls amongest n possible positions
    - the position locateds are defined as position[i]
    - the force between balls is a function of distance
        force = abs(x - y)

    - we want to distribute the balls so that the minimum force
    is maximized (Maximize the min)
Examples
     Example 1
        Input: position = [1,2,3,4,7], m = 3
        Output: 3
        Explanation: Distributing the 3 balls into baskets 1, 4 and 7 will make the magnetic force between ball pairs [3, 3, 6]. The minimum magnetic force is 3. We cannot achieve a larger minimum magnetic force than 3.

     Example 2
        Input: position = [5,4,3,2,1,1000000000], m = 2
        Output: 999999999
        Explanation: We can use baskets 1 and 1000000000.

Ideas
    - brute force will be too expensive
    - sorting the positions should help us with order
    - we can bound the problem
        + max possible distance = max - min
        + min distance = 1
    - try binary search
        + specify a force requirement
        + distribute a ball once we've met the requirement
"""

class Solution:
    def maxDistance(self, position: List[int], m: int) -> int:
        """Binary search with checker

        - sort values so we get the positions in order
        - guess a minimum force we can meet
            + use a checker to verify the force can be met
            while distributing at least m balls

        Time Complexity
            O(n*ln(n))

            - sorting
            - binary search of max difference
        Space Complexity
            O(1)
        """

        position = sorted(position)

        # Set bounds
        low = 1
        high = position[-1] - position[0]

        def checker(force: int):
            """Try to distribute m balls at a given distance 
            
            - once we meet the minimum distance, distribute the ball
            - at the end check that we distributed all m balls
            """

            count: int = 1
            last_pos: int = position[0]    

            for pos in position[1:]:

                if pos - last_pos >= force:
                    last_pos = pos
                    count += 1

            return count >= m

        while low <= high:

            mid = low + ((high - low) // 2)

            check = checker(mid)

            # print(f'low: {low}   high: {high}   mid: {mid}   check: {check}')

            # Try for a larger magnetic force
            if check:
                low = mid + 1

            else:
                high = mid - 1

        return low - 1
