"""
Inputs

Outputs
    int: find the left most column with at least a 1
Notes
    - the matrix is binary (only 0 and 1)
    - we can't access the grid directly
        + we can get grid dimensions
        + we can check specific rows,column positions

    - each row is sorted ie: [0,0,0,1,1,1]

    - if we can't find a 1, then return -1

Ideas

    - linear search is too expensive
    - since rows are sorted we can apply
    binary search on each row
        + we don't need to perform normal
        binary search on each row

        we only need to search for a better
        goal

        ie: we found a 1 at column 5
        we don't need to search columns 5 through n

        we target our binary search between 0 and 5

        aka 0 to best

    - check the last column in each row, make sure
        it actually has some 1s in it

    - are the rows sorted with respect to another???
        + doesn't seem like it

"""

# """
# This is BinaryMatrix's API interface.
# You should not implement it, or speculate about its implementation
# """
#class BinaryMatrix(object):
#    def get(self, row: int, col: int) -> int:
#    def dimensions(self) -> list[]:

class Solution:

    def leftMostColumnWithOne(self, bm: 'BinaryMatrix') -> int:
        """Binary search on rows

        - apply binary search on every row
        - only search between 0 and our current
        best

        Time Complexity
            O(r*ln(c))
        Space Complexity
            O(1)
        """

        n_rows, n_cols = bm.dimensions()
        best = float('inf')

        for row in range(n_rows):

            # Check if there's any 1s
            if bm.get(row, n_cols - 1) != 1:
                continue
            else:

                low = 0
                high = min(best, n_cols - 1)

                while low <= high:

                    mid = (low + high) // 2

                    # Found a 1, try lower
                    if bm.get(row, mid) == 1:
                        best = mid
                        high = mid - 1
                    else:
                        low = mid + 1

        if best != float('inf'):
            return best
        else:
            return -1

    def linear_search(self, bm: 'BinaryMatrix') -> int:
        """Down & left

        - search by only moving down and left
            + move left while we have 1s
            + if no 1s move to the next row

        Time Complexity
            O(r + c)
        Space Complexity
            O(1)
        """

        n_rows, n_cols = bm.dimensions()
        best = float('inf')

        for row in range(n_rows):

            col = min(best, n_cols - 1)

            while col >= 0 and bm.get(row, col) == 1:
                best = col
                col -= 1

        if best != float('inf'):
            return best
        else:
            return -1
