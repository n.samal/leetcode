"""
Inputs
    arr (List[int]):
    threshold (int):  

Outputs
    int: find smallest divisor less then or equal to threshold
Notes
    - choose integer divisor and divide all values in the array by it
        + sum the result of the division

    - division of each value is equal to nearest greater integer

    ie: 7/3 => 3
       10/2 => 5

Examples

    Input: nums = [1,2,5,9], threshold = 6
    Output: 5
    Explanation: We can get a sum to 17 (1+2+5+9) if the divisor is 1. 
    If the divisor is 4 we can get a sum to 7 (1+1+2+3) and if the divisor is 5 the sum will be 5 (1+1+1+2). 

Ideas
    - the problem is bounded
        + binary search seems like the way to go

    - divisors
        + smallest value possible = 1
        + larger value 

    - can we speed up the checking process
        + currently need to iterate all n to check summation
"""

from math import ceil

class Solution:
    def smallestDivisor(self, arr: List[int], threshold: int) -> int:
        """Use binary search to find the smallest divisor
        that under our threshold target

        Time Complexity
            O(ln(max)*n)
        Space Complexity
            O(1)
        """

        low = 1
        high = max(arr)

        while low <= high:

            mid = (low + high) // 2
            check = self.check(arr, mid, threshold)

            # print(f'low: {low}  high: {high}  mid: {mid}  check: {check}')

            # Reduce divisor if it works
            if check:
                high = mid - 1
            else:
                low = mid + 1

        return high + 1

    def check(self, arr: List[int], divisor: int, threshold: int) -> bool:
        """Check if we are within our threshold limit
        by summing the rounded divisions

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        cur_sum = 0

        for val in arr:

            cur_sum += ceil(val / divisor)

            if cur_sum > threshold:
                return False

        return True
