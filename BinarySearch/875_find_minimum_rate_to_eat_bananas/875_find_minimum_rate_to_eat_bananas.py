"""
Inputs
    piles (List[int]): bananas in each pile
    H (int): hours guards are away
Outputs
    int: minimum integer so that Koko can eat all the bananas in H hours
Goal
    - find the minimum integer K that Koko can eat all the bananas 
Notes
    - K is the banana eating speed
    - each hour Koko eats some pilo of bananas, and eats K of them
        + if pile is less than K, she eats all of the bananas in 
        the pile but not any more during the hour
Examples

    Example 1
    Input: piles = [3,6,7,11], H = 8
    Output: 4

    Hour 1: eat @ index 0
    before: [0, 6, 7, 11]
    after: [0, 6, 7, 11]

    Hour 2: eat @ index 1
    before:[0, 6, 7, 11]
    after:[0, 2, 7, 11]

    Hour 3: eat @ index 1
    before:[0, 6, 7, 11]
    after:[0, 0, 7, 11]

Ideas
    - best case scenario is that Koko can eat
    all the piles in one hour each
        + this means she can eat the biggest pile in one hour
        ie: K_max = max(piles)

    - in the worse case scenario, Koko can only eat the 1 banana each hour
        + this means many hours to eat a single pile

    - there doesn't seem to be a benefit to eating bananas in any specific order

    - this is a bounded problem and we can apply binary search
        + low = 1, high = max(piles)
        + see if we can meet the hour requirement to consume bananas
"""

from typing import List
import math

class Solution:
    def minEatingSpeed(self, piles: List[int], H: int) -> int:
        """Guess & check methodology

        - try to eat bananas at an assumed rate
            + if it works, try to go slower
            + if it doesn't work, then eat faster

        Args:
            piles (List[int]): bananas in each pile
            H (int): hours until guards are back
        Returns:
            int: minimum rate we can each bananas

        Time Complexity
            O(ln(max) * n)
        Space Complexity
            O(1)
        """

        low: int = 1
        high: int = max(piles)
        ans: int = 1

        while low <= high:

            mid = low + (high - low) // 2

            check = self.check(piles, H, mid)

            # print(f'low: {low}  high: {high}  mid: {mid}  check: {check}')

            # Try to eat slower
            if check:
                ans = mid
                high = mid - 1

            # We need more time
            else:
                low = mid + 1

        return ans

    def check(self, piles: List[int], H:int, K: int) -> bool:
        """Check if we can eat all the bananas quick enough

        - round up the number of hours needed to eat each pile
            ie: K = 4, bananas = 2, 1 hour is still required to eat them
        - if we can't eat fast enough then fail

        Args:
            piles (List[int]): bananas in each pile
            H (int): hours until guards are back
            K (int): bananas eaten per hour
        Returns:
            bool: can all the bananas be eaten in time?

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        n_hours: int = 0

        for val in piles:

            n_hours += math.ceil(val / K)             

            if n_hours > H:
                return False

        return True
