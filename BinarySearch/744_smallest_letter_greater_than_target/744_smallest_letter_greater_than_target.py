"""

Inputs
    target (str): target string
    letters (List[str]): array of letters in sorted order
Outputs:
    str: smallest letter greater than target

Notes
    - python allows for string comparison, we can use like numbers

Cases
    - duplicates in letters
    - target not in letters

Strategies

    Linear Scan
        - search for the target in the array
        - once we find a letter greater than ours, output it

        - if we haven't found anything, wraparound occurs.
            + just grab the first element from the array
    Binary Search

        - use binary search to find the target/closest value
        - if we find the target
            + grab the next largest value (either with linear scan or binary search)
            + since searching for the next largest value,

                bump the indices when we find the target!

                low = mid + 1

        - if no target found, grab the first element in the array
"""

from typing import List


class Solution:
    def linear(self, letters: List[str], target: str) -> str:
        """Do a linear scan through the array for the next
        greatest letter

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        for letter in letters:
            if letter > target:
                return letter

        # No greater letter found, use the wraparound answer
        return letters[0]

    def binary_search(self, letters: List[str], target: str) -> str:
        """Use binary search to find the target arround
        greatest letter

        Time Complexity
            O(log(n))
        Space Complexity
            O(1)
        """

        n: int = len(letters)

        low: int = 0
        high: int = n - 1

        while low < high:

            mid = (low + high) // 2

            # Target is smaller, move left
            if target < letters[mid]:
                high = mid

            # Target is equal or larger, move right
            # keep moving right even if we match the target
            else:
                low = mid + 1

        # No greater letter found, use the wraparound answer
        # use this logic since we can converge to low = n - 1
        if low < n and letters[low] > target:
            return letters[low]
        else:
            return letters[0]

    def binary_search2(self, letters: List[str], target: str) -> str:
        """Use binary search to find the target arround
        greatest letter

        - modify bounds so we can converge outside normal bounds
            + typically we choose n - 1 to converge within bounds
            + we'll choose n

        Time Complexity
            O(log(n))
        Space Complexity
            O(1)
        """

        n: int = len(letters)

        low: int = 0
        high: int = n

        while low < high:

            mid = (low + high) // 2

            # Target is smaller, move left
            if target < letters[mid]:
                high = mid

            # Target is equal or larger, move right
            # keep moving right even if we match the target
            else:
                low = mid + 1

        # No greater letter found, use the wraparound answer
        # use this logic since we can converge to low = n - 1
        if low > n - 1:
            return letters[0]
        else:
            return letters[low]


if __name__ == "__main__":

    # Case 3
    letters = ["c", "f", "j"]
    # target = "d"
    # target = "f"
    target = "j"

    obj = Solution()
    print(obj.binary_search(letters, target))
