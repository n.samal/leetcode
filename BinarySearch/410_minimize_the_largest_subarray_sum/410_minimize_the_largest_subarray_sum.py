"""
Inputs
    arr (List[int]): array of values
    m (int): number of subarrays
Outputs
    int: what is the minimize subarray sum possible?
Goals
    - find the smallest subarray sum possible if splitting
    the array into m pieces
Notes
    - values are non-negative integers
    - subarrays must be nonempty and continuous

Examples

    Input:
    nums = [7,2,5,10,8]
    m = 2

    Output:
    18

    Explanation:
    There are four ways to split nums into two subarrays.
    The best way is to split it into [7,2,5] and [10,8],
    where the largest sum among the two subarrays is only 18.

Ideas

    - seems like boat problem
        + determine the bounds
        + then use binary search and checker

    - bounds are defined by largest summation of a subarray, and smallest
    summation of a subarray

        + max summation = entire array
        + min summation = smallest value in array

        we know all integers are non-negative, so just use 1 for now

    - with a check, try to build smaller subarrays
        + if the value causes our subarray sum to go over the limit
        we must split
        + if the value is over our limit we also fail
        + if the value can be added to our summation, then add it
        + make sure we don't go over the max number of splits

Cases

    One Subarray
        - our smallest subarray summation = summation of all elements
    Subarray = Length - 1
        - we split the array into every piece
        - largest sum = max

    General
        - splits inbetween 1 and length

    More splits than values
        - may not be possible with problem statement
"""

from typing import List


class Solution:

    def splitArray(self, arr: List[int], m: int) -> int:
        """Guess and check

        - guess what our maximum subarray sum is
        - see if works and try again

        Time Complexity
            O(ln(sum(arr) * n))

            - we're cutting the bounds 0 to sum(arr) in half
            - each time we potentially need to sum all values

        Space Complexity
            O(1)
        """

        low: int = 0
        high: int = sum(arr)
        ans: int = 0

        # Only one subarray
        if m == 1:
            return high

        while low <= high:

            mid = low + ((high - low) // 2)

            # Check if our guess works
            # ie: 1 cut = 2 subarrays
            check = self.check_guess(arr, mid, m - 1)

            print(f' low: {low}  high: {high}  mid: {mid}  check: {check}')

            # Try going smaller if the guess works, otherwise go bigger
            if check:
                high = mid - 1
                ans = mid
            else:
                low = mid + 1

        return ans

    def check_guess(self, arr: List[int], max_sum: int, max_splits: int):
        """Check we can achieve the smallest sum

        - add values to our current subarray until we hit the summation limit
        - if a single value is greater then our limit, we're also in trouble
        - split the subarray if our limit is hit

        Args:
            arr (List[int]):
            max_sum (int): maximum subarray summation
            max_splits (int): maximum number of splits

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        cur_sum: int = 0  # summation of current subarray
        count: int = 0    # number of splits

        for val in arr:

            if cur_sum + val <= max_sum:
                cur_sum += val

            elif val > max_sum:
                return False

            else:
                cur_sum = val
                count += 1

                if count > max_splits:
                    return False

        return True


if __name__ == '__main__':

    # Test case 1
    nums = [7, 2, 5, 10, 8]
    m = 2

    # Single subarray
    nums = [1, 2147483647]
    m = 1

    # Subarray count = length
    nums = [1, 2147483647]
    m = 2

    nums = [2, 3, 1, 2, 4, 3]
    m = 5

    obj = Solution()
    print(obj.splitArray(nums, m))
