"""
Inputs
    s (str): string
Outputs
    str: longest duplicate substring
Notes
    - if no duplicate substring then return ""
    - substring must occur 2 or more times
    - occurences may overlap    

Examples

    Example 1:

        Input: "banana"
        Output: "ana"

    Example 2:

        Input: "abcd"
        Output: ""

Ideas

    - subarray iteration
        + iterate over all possible subarrays
        + track the counts of each substring occurence
        + track the longest string

    - use a counter to track the position of duplicate characters
        + if no duplicate characters, then no chance of reoccurence
        + use those positions and check over them in pairs

            a: [1, 3, 5]

            then track the increasing substring 

Strategies

    Binary Search with Rabin Karps Algorithm

        - we know any duplicate substring will be between the length 0 and n
        - we make a guess and see if any such substring occurs

            + if yes, then we search for larger possible substrings
            + if no, we search for smaller substrings

        - since we don't want to store the entire substr in each hashmap we can
        use a custom hashing function
            + if we've seen the hash before then it's a duplicate substring
            + this issue may be a limitation by the grader/platform

        References:
            https://leetcode.com/problems/longest-duplicate-substring/discuss/327643/Step-by-step-to-understand-the-binary-search-solution
            https://leetcode.com/problems/longest-duplicate-substring/discuss/290961/Java-w-comments-Binary-Search-Length-%2B-Rolling-Sequence-Hashing

"""

from collections import defaultdict
from itertools import combinations


class Solution:
    def hash_(self, S: str) -> str:
        """Subarray iteration and counter

        - iterate over all substrings
        - track counts of each substring

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        Notes
            - too slow
        """

        counts = {}
        ans = ''

        n: int = len(S)

        for ix in range(n):
            for jx in range(ix + 1, n + 1):

                substr = S[ix:jx]

                print(f's: {substr}')

                counts[substr] = counts.get(substr, 0) + 1

                if counts[substr] >= 2 and len(substr) > len(ans):
                    ans = substr

            print('')

        return ans

    def sliding_window(self, S: str) -> str:
        """Subarray iteration and counter

        - track occurence of each character
        - for every pair of indices for the same character
            + sliding the substring right while they matchs

        ie: a: [0, 5, 8]
            sliding at 0 and 5 simultaneously
            sliding at 0 and 8 simultaneously

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        Notes
            - too slow
        """

        ans = ''

        n: int = len(S)

        ''' Track locations for each character '''

        locs = defaultdict(list)
        for ix, char in enumerate(S):
            locs[char].append(ix)

        ''' Slide substring windows while they match '''
        for char in locs:

            if len(locs[char]) < 2:
                continue

            for ix, jx in combinations(locs[char], 2):

                start = ix

                while ix < n and jx < n and S[ix] == S[jx]:

                    substr = S[start:ix + 1]

                    if len(substr) > len(ans):
                        ans = substr

                    ix += 1
                    jx += 1

        return ans

    def binary_search(self, S: str) -> str:
        """Subarray iteration and counter

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        Notes
            - memory limit exceeded
                + due to substring stored in set of check method
                + use of standard hash function did not work
        """

        ans = ''

        n: int = len(S)

        low = 0
        high = n

        while low <= high:

            mid = low + ((high - low) // 2)

            check, ix = self.check(S, mid)

            if check:
                low = mid + 1

                if mid > len(ans):
                    ans = S[ix:ix + mid]

            else:
                high = mid - 1

        return ans

    def binary_search2(self, S: str) -> str:
        """Subarray iteration and counter

        - removing substring gathering as much as possible

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        Notes
            - passed but slow
        """

        ans_len = 0
        ans_ix = 0

        n: int = len(S)

        low = 0
        high = n

        while low <= high:

            mid = low + ((high - low) // 2)

            check, ix = self.check(S, mid)

            if check:
                low = mid + 1

                if mid > ans_len:
                    ans_len = mid
                    ans_ix = ix

            else:
                high = mid - 1

        return S[ans_ix:ans_ix + ans_len]

    def check(self, S: str, guess: int):
        """Check if a substring of length G occurs more than once

        Args:
            S (str): string to check
            guess (int): size of sliding window for substring
        Returns:
            Tuple[bool, int]: if duplicate substring found
                                starting location of substring
        """
        seen = set([])
        n: int = len(S)

        for ix in range(0, n + 1 - guess):
            substr = S[ix:ix + guess]

            if substr in seen:
                return True, ix
            else:
                seen.add(substr)

        return False, 0

    def check2(self, S: str, guess: int):
        """Check if a substring of length G occurs more than once

        - use hash function to substring

        Args:
            S (str): string to check
            guess (int): size of sliding window for substring
        Returns:
            Tuple[bool, int]: if duplicate substring found
                                starting location of substring
        Notes:
            - create a custom hash function to improve time
        """
        seen = set([])
        n: int = len(S)

        for ix in range(0, n + 1 - guess):
            substr = hash(S[ix:ix + guess])

            if substr in seen:
                return True, ix
            else:
                seen.add(substr)

        return False, 0


if __name__ == '__main__':

    # Example 1
    S = "banana"

    obj = Solution()
    # print(obj.longestDupSubstring(S))
    # print(obj.sliding_window(S))
    print(obj.binary_search(S))
