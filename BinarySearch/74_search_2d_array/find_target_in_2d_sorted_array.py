# Given a 2D array of integers
# when integers increase in the +row direction
# integers increase in +col direction
# can we determine if the target value exists in the array?


def naive(arr: list, target: int):
    """Find if the target value is in the given array

    Time Complexity
        O(n^2)

    Args:
        arr (List[List[Int]]): 2D array of integers in increasing order
        target (int): value to search for
    Returns:
        bool: is the number in the matrix
    """

    # Get dimensions
    n_rows: int = len(arr)
    n_cols: int = len(arr[0])

    for row_ix in range(n_rows):
        for n_cols in range(n_cols):

            if arr[row_ix][col_ix] == target:
                return True

    return False

# other approaches
# method 1: binary approach on rows (n * log(m))
# method 2: binary approach on rows and columns (log(n) * log(m))


def optimal(arr: list, target: int):
    """Find if the target value is in the given array

    Time Complexity
        O(n + m)

    Args:
        arr (List[List[Int]]): 2D array of integers in increasing order
        target (int): value to search for
    Returns:
        bool: is the number in the matrix
    """

    # Get dimensions
    n_rows: int = len(arr)

    if n_rows == 0:
        return False

    n_cols: int = len(arr[0])

    # Starting indices
    row_ix: int = 0
    col_ix: int = n_cols - 1

    # While valid rows and columns
    while 0 <= row_ix < n_rows and 0 <= col_ix < n_cols:

        # Found our value
        if arr[row_ix][col_ix] == target:
            return True

        # Current value is too large
        # All values to the right, and below this way will be too large
        elif arr[row_ix][col_ix] > target:

            # Shift column to left by one
            col_ix -= 1

        # Value is too small
        # All values to the left in this row are also too small
        # (we're at the largest value in the row)
        else:
            row_ix += 1

    return False

def searchMatrix(self, grid: List[List[int]], target: int) -> bool:
    """

    Time Complexity
        O(n + m)
    Space Complexity
        O(1)
    """
    
    if not grid:
        return False
    
    n_rows: int = len(grid)
    n_cols: int = len(grid[0])
        
    # Start at the largest row, and squeeze down
    # if the value is within our row then search
    # otherwise go lower
    r = n_rows - 1
    c = 0
    
    while r >= 0 and c < n_cols:
        
        if grid[r][c] == target:
            return True

        # Value is smaller then smallest on row
        elif target < grid[r][0]:
            r -= 1
        
        else:
            c += 1
        
    return False

if __name__ == '__main__':

    print(optimal([], 1))