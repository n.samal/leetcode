
def get_pivot_index(arr: list):
    """Find the index of the pivot element in a sorted and rotated array

    Pivot point can occur at 3 locations
        left: then array is sorted
        middle: pivot exists
        right: pivot at end

    if a pivot exists, all values next to it are greater

    Args:
        arr (list): sorted and rotated array of numbers
    Returns:
        int: location of pivot element in the array
    Notes
        - this logic only works when no duplicates exist
    """

    n = len(arr)

    # Single element
    if n == 1:
        return 0

    # No elements
    elif n == 0:
        return -1

    # Two elements
    elif n == 2:

        if arr[0] <= arr[1]:
            return 0
        else:
            return 1

    # Initialize pointers
    left = 0
    right = n - 1

    # Array is already sorted
    if arr[left] < arr[right]:
        return left

    ''' Binary search '''
    while left <= right:

        mid = (left + right) // 2

        print(f"left: {left} right: {right} mid: {mid}")

        # Pivot point at way right
        if mid == n - 1 and arr[mid] < arr[mid - 1]:
            return mid

        # Mid point is greater than right
        # inflection point is right
        # [3, 4, *5*, 0, 1, 2]
        elif arr[mid] > arr[mid + 1]:
            return mid + 1

        # At the pivot point
        # value is less than neighbors
        elif arr[mid] < arr[mid - 1] and arr[mid] < arr[mid + 1]:
            return mid

        # Pivot on left
        # [8, 0, 1, 2, *3*, 4, 5, 6, 7]
        elif arr[mid] <= arr[left]:
            right = mid - 1

        # Pivot point must be on right
        # left half is increasing
        # [2, 3, 4, 5, *6*, 7, 8, 0, 1]
        else:
            left = mid + 1

    return left


if __name__ == '__main__':

    # arr_in = [3, 4, 5, 1, 2]

    # arr_in = [7, 0, 1, 2, 3, 5, 6]

    # Pivot on left
    # arr_in = [8, 0, 1, 2, 3, 4, 5, 6, 7]

    # Pivot on right
    # arr_in = [4, 5, 6, 7, 0, 1, 2]
    # arr_in = [2, 3, 4, 5, 6, 7, 8, 0, 1]
    # arr_in = [2, 3, 4, 5, 6, 7, 8, 0]

    # Rotated array with duplicates
    # arr_in = [2, 2, 2, 0, 1]

    # Base cases
    # arr_in = []           # null
    # arr_in = [0]        # single element
    # arr_in = [5, 1]     # double element sorted and rotated
    # arr_in = [1, 5]     # double element sorted
    # arr_in = [1, 1]     # double element sorted
    # arr_in = [1, 3, 5]  # sorted without duplicates
    # arr_in = [1, 1, 1]  # sorted with duplicates
    # arr_in = [5, 1, 3]  # sorted and rotated

    # Method fails on sorted, rotated and duplicates
    arr_in = [3, 3, 3, 1]    # sorted, rotated, duplicates

    print(get_pivot_index(arr_in))

