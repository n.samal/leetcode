"""
Inputs
    weights (List[int]): weight for each package
    D (int): number of days to ship packages
Outputs
    int: smallest boat capacity to ship in the day requirement
Goal:
    - find the smallest boat capacity we can use to ship these packages
    - packages must ship in the order specified
    - we cannot load more than the ships capacity

Examples

    Example 1
        Input: weights = [1,2,3,4,5,6,7,8,9,10], D = 5
        Output: 15
        Explanation:
        A ship capacity of 15 is the minimum to ship all the packages in 5 days like this:
        1st day: 1, 2, 3, 4, 5
        2nd day: 6, 7
        3rd day: 8
        4th day: 9
        5th day: 10

Ideas
    - binary search with a checker
    - bound the problem
        - if our boat is massive we can go in one day => sum(arr)
        - if boat is small, we can only carry the heaviest package
"""

from typing import List
import bisect


''' Binary Search '''


class Solution:

    def shipWithinDays(self, weights: List[int], D: int) -> int:
        """Guess and check

        Time Complexity
            O(ln(S) * n)

            S = sum of weights
            n = counting packages for check

        Space Complexity
            O(1)
        """

        # Cases
        # - the boat only carries one package a day (the heaviest)
        # - all packages go in 1 day
        low: int = max(weights)
        high: int = sum(weights)
        ans: int = 0

        while low <= high:

            mid = low + ((high - low) // 2)

            check = self.boat_check(weights, mid, D)

            print(f'low: {low}  high: {high}  mid: {mid}  check: {check}')

            if check:
                ans = mid
                high = mid - 1
            else:
                low = mid + 1

        return ans

    def boat_check(self, arr: List[int], weight_limit: int, day_limit: int) -> bool:
        """Can our boat ship all the packages in the days given

        - if any package if more than our boat capacity it's a no go
        - if the package fits, then add it
        - the package is over the capacity, we must go on another trip

        Args:
            arr (List[int]): weight of each package
            weight_limit (int): most weight the boat can hold
            day_limit (int):
        Returns:
            bool:
        """

        n_days: int = 1
        cur_sum: int = 0

        for val in arr:

            if cur_sum + val <= weight_limit:
                cur_sum += val

            # The package is more than our boat can handle
            elif val > weight_limit:
                return False

            # Need another boat for the next package
            else:
                cur_sum = val
                n_days += 1

                if n_days > day_limit:
                    return False
        return True


''' Binary Search: with Cummulative Summation '''


class Solution2:

    def shipWithinDays(self, weights: List[int], D: int) -> int:
        """Guess and check

        Time Complexity
            O(ln(S) * n)

            S = sum of weights
            n = counting

        Time Complexity
            O(lg(S) * lg(n))

            lg(s) binary search on capacity
            lg(n) binary search for each check

        Space Complexity
            O(1)
        Notes
            - not significantly faster
        """

        n: int = len(weights)
        cumsum = [0 for ix in range(n + 1)]

        for ix in range(1, n + 1):
            cumsum[ix] = cumsum[ix - 1] + weights[ix - 1]

        # Cases
        # - the boat only carries one package a day (the heaviest)
        # - all packages go in 1 day
        low: int = max(weights)
        high: int = sum(weights)
        ans: int = 0

        while low <= high:

            mid = low + ((high - low) // 2)

            check = self.boat_check(cumsum, mid, D)

            print(f'low: {low}  high: {high}  mid: {mid}  check: {check}')

            if check:
                ans = mid
                high = mid - 1
            else:
                low = mid + 1

        return ans

    def boat_check(self, cum_sum: List[int], weight_limit: int, day_limit: int) -> bool:
        """Can our boat ship all the packages in the days given

        - if any package if more than our boat capacity it's a no go
        - if the package fits, then add it
        - the package is over the capacity, we must go on another trip

        Args:
            cum_sum (int): cummulative summation
            weight_limit (int): most weight the boat can hold
            day_limit (int):
        Returns:
            bool:

        Time Complexity
            O(lg(n))
        Space Complexity
            O(1)
        """

        n: int = len(cum_sum)
        n_days: int = 1

        ix: int = 0
        search = 0 + weight_limit

        while ix < n:

            # Find packages we can take under the weight limit
            ix = bisect.bisect(cum_sum, search)

            if ix < n:
                n_days += 1
                search = cum_sum[ix - 1] + weight_limit

                if n_days > day_limit:
                    return False

        return True


if __name__ == "__main__":

    # Base case
    weights = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    D = 5

    # obj = Solution()
    # obj.shipWithinDays(weights, D)

    obj = Solution2()
    print(obj.shipWithinDays(weights, D))
