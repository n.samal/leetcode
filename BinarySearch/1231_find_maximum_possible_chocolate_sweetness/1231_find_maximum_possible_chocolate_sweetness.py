"""
Inputs
    sweetness (List[int]): sweetness for each piece of the chocolate bar
    K (int): number of cuts
Outputs
    int: find the maximum total sweetness you can eat
Goals:
    - The candy bar can be cut into continous pieces using k cuts
    - We always eat the candy bar chunk that has the minimum sweetness
    - what is the max sweetness we can eat?

Example 

    Example 1

        Input: sweetness = [1,2,3,4,5,6,7,8,9], K = 5
        Output: 6
        Explanation: You can divide the chocolate to [1,2,3], [4,5], [6], [7], [8], [9]

    Example 2:

        Input: sweetness = [5,6,7,8,9,1,2,3,4], K = 8
        Output: 1
        Explanation: There is only one way to cut the bar into 9 pieces.

    Example 3:

        Input: sweetness = [1,2,2,1,2,2,1,2,2], K = 2
        Output: 5
        Explanation: You can divide the chocolate to [1,2,2], [1,2,2], [1,2,2]

Ideas

    - we can bound the problem
        - best case: we eat the whole bar => sum(arr)
        - worst case: we eat the least sweet piece => min(arr)

    - try to use binary search with a checker
        - previous problems we minimized the maximum
        - here we maximize the minimum

    - if we eat all the bar we have enough sweetness but not enough pieces
    - if we split into too many pieces we may not meet the sweetness

    - similar problems
        + Problem 410
        + Problem 1231

Cases

    k = 0: No slices, eat the whole bar
        max sweetness = sum(arr)
    
    k = 1 to ~n: general case

    k = n - 1: candy bar is split at every possible partition
        max sweetness for us = min(arr)


Hand Calc

    sweetness = [1,2,3,4,5,6,7,8,9], K = 5

    guess what min_sweetness level we can get
    start eating pieces

    sweetness_target_guess = 4

    cur_piece = 1
    cur_piece = 1,2
    cur_piece = 1,2,3

    once've we've met our minimum sweetness target break off that piece
    and start a new one
    n_pieces = 1

    cur_piece = 4
    break it off
    n_pieces = 2

    cur_piece = 5
    break it off
    n_pieces = 3

    cur_piece = 6
    break it off
    n_pieces = 4

    if we've met our n_pieces goal and the sweetness goal, let's
    up our sweetness requirement

Notes
    - could use cumsum and binary search to improve time on checking method
        + tricky part is finding the summation just greater than our sweetness target
References:
    https://leetcode.com/problems/divide-chocolate/discuss/572159/DP-solution-in-Python-(TLE)
"""

from typing import List
# import bisect


''' Linear Check '''


class Solution:
    def maximizeSweetness(self, sweetness: List[int], K: int) -> int:
        """Guess and check methodology

        - see if we can meet our minimum sweets level and the n_pieces
            + if we can, go for more sweetness
            + if we can't, then go for less sweetness

        Time Complexity
            O(n * ln(S))

            split the bounds in half
            each time we run through the whole sweetness array

        Space Complexity
            O(1)
        """
        low: int = min(sweetness)
        high: int = sum(sweetness)
        ans: int = 1

        # No cuts
        if K == 0:
            return high

        while low <= high:

            mid = low + (high - low) // 2

            # Number of pieces = slices + 1
            # ie: 0 slice = 1 piece
            check = self.check_min_sweet(sweetness, K + 1, mid)

            print(f"low: {low}  high: {high}  mid: {mid}  check: {check}")

            # Try to get more sweetness
            if check:
                ans = mid
                low = mid + 1
            else:
                high = mid - 1

        print(f"low: {low}  high: {high}  mid: {mid}")

        return ans

    def check_min_sweet(self, arr: List[int], k: int, target: int) -> bool:
        """Can we cut the candy bar to get our assumed min sweetness?

        Checks
            - can we cut the candy bar into at least k pieces?
            - is the sweetness of each piece greater than our target?

        - if we eat all the bar we have enough sweetness but not enough pieces
        - if we split into too many pieces we may not meet the sweetness

        Args:
            arr (List[int]): sweetness of candy bar pieces
            k (int): desired number of pieces
            target (int): minimum sweetness target
        Returns:
            bool:

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        cur_sum: int = 0  # current subarray sweetness
        count: int = 0  # number of pieces

        for val in arr:

            # Eat the piece
            cur_sum += val

            # We've met our goal
            # break off that piece and start a new one
            # (we only count pieces once they're large enough)
            if cur_sum >= target:
                count += 1
                cur_sum = 0

        return count >= k



''' Log Check '''


if __name__ == "__main__":

    # Example 1
    # sweetness = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    # K = 5

    # Example 3
    sweetness = [1, 2, 2, 1, 2, 2, 1, 2, 2]
    K = 2

    obj = Solution()
    print(obj.maximizeSweetness(sweetness, K))
