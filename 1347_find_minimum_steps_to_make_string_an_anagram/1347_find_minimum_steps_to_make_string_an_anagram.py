"""
Inputs
    s (str): goal string
    t (str): modifiable string
Outputs
    int: minimum steps needed to create an anagram

Notes
    - we want to modify t to become an anagram of s
    - an anagram is a word that contains the same characters
    as another word, but may not be in the same order
    - the size of both strings will be equal 

Examples

    Example 1:

        Input: s = "bab", t = "aba"
        Output: 1
        Explanation: Replace the first 'a' in t with b, t = "bba" which is anagram of s.

    Example 2:

        Input: s = "leetcode", t = "practice"
        Output: 5
        Explanation: Replace 'p', 'r', 'a', 'i' and 'c' from t with proper characters to make t anagram of s.

    Example 3:

        Input: s = "anagram", t = "mangaar"
        Output: 0
        Explanation: "anagram" and "mangaar" are anagrams.

    Example 4:

        Input: s = "xxyyzz", t = "xxyyzz"
        Output: 0

    Example 5:

        Input: s = "friend", t = "family"
        Output: 4

Ideas

    - use a hashmap or array counter to track
    the frequency of each letter

    - track the differences between the two
        + ie: what characters we have vs what we need

Key Points
    - we don't need to track the positive and negative delta, just one
    or the other

    ie:
        s = 'bab'
        t = 'aba

        comparing counts

        needs = 'a': 1
        needs = 'b': 2

        has   = 'a': 2
        has   = 'b': 1

        delta = 'a': -1
        delta = 'b': +1

        we don't need two changes, or zero changes. If we swap the 'a' to 'b' we
        achieve our goal in 1 step

        we can consider these pairs (combine each required addition with a removal)
"""

import string


class Solution:
    def review(self, s: str, t: str) -> int:
        """

        - track counts of each the alphabet
        in each string
        - for every character removal or addition we add a count
            + we should only track one or the other

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        # Frequency counts
        arr_need = [0 for _ in range(26)]
        arr_has = [0 for _ in range(26)]

        for ix in range(len(s)):
            arr_need[ord(s[ix]) - ord('a')] += 1
            arr_has[ord(t[ix]) - ord('a')] += 1

        # Check where we need characters
        count_raw: int = 0
        count_pos: int = 0
        count_neg: int = 0

        for char in string.ascii_lowercase:

            ix = ord(char) - ord('a')
            delta = arr_need[ix] - arr_has[ix]

            if delta:
                count_raw += delta
                print(f'char: {char}   need: {arr_need[ix]}   has: {arr_has[ix]}   delta: {delta:2}   count: {count_raw:2}')

            if delta > 0:
                count_pos += delta

            if delta > 0:
                count_neg += delta

        print(f'count_raw: {count_raw}   count_pos: {count_pos}   count_neg: {count_neg}')

        return count_pos

    def counter(self, s: str, t: str) -> int:
        """Character counter

        - track counts of each the alphabet in each string
        - for every character removal or addition we add a count
            + we should only track one or the other

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        # Frequency counts
        arr_need = [0 for _ in range(26)]
        arr_has = [0 for _ in range(26)]

        for ix in range(len(s)):
            arr_need[ord(s[ix]) - ord('a')] += 1
            arr_has[ord(t[ix]) - ord('a')] += 1

        # Check where we need characters
        count: int = 0

        for ix in range(26):
            delta = arr_need[ix] - arr_has[ix]

            if delta > 0:
                count += delta

        return count


if __name__ == '__main__':

    # Example 1
    # s = 'bab'
    # t = 'aba'

    # Example 2
    s = 'leetcode'
    t = 'practice'

    # Example 5
    # s = 'friend'
    # t = 'family'

    obj = Solution()
    print(obj.review(s, t))
