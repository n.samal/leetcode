"""
Inputs
    int: value
Outputs
    int: compliment of base 10 integer

Examples

    Example 1:

        Input: 5
        Output: 2
        Explanation: 5 is "101" in binary, with complement "010" in binary, which is 2 in base-10.

    Example 2:

        Input: 7
        Output: 0
        Explanation: 7 is "111" in binary, with complement "000" in binary, which is 0 in base-10.

    Example 3:

        Input: 10
        Output: 5
        Explanation: 10 is "1010" in binary, with complement "0101" in binary, which is 5 in base-10.

"""

import math

class Solution:
    def bitwiseComplement(self, N: int) -> int:
        """Flip the bits in the number

        - we can't use ~ since it flips the signed bit as well
        - compute xor with every bit 
            1 ^ bit => new bit
            1 ^ 1   => 0
            1 ^ 0   => 1

        - create a bit array of all 1s, then compute xor

        Time Complexity
            O(1)
        Space Complexity
            O(1)
        """

        if N == 0:
            return 1 

        # Find the number of bits
        # ie: 5 => log2(5) = 2.32   and 2^2 = 4 + 1
        # ie: 10 => log2(10) = 3.32  and 2^3 = 8
        n_bits: int = math.floor(math.log2(N)) + 1

        # Find number with same number of bits
        # but all 1s
        # ie: 7 =>  111
        # ie: 8 => 1000
        bit_1s = 2 ** n_bits - 1

        return bit_1s ^ N

    def bin_flip(self, N: int) -> int:
        """Flip the bits in the number

        - gather the string representation
        - flip bits from the string rep

        Time Complexity
            O(1)     
        Space Complexity
            O(1)
        """

        # Only get the unsigned portion
        # 0b101 => 101
        bit_str = bin(N)[2:]

        n: int = len(bit_str)
        value: int = 0

        # Add value for each bit
        for ix in range(n):
            value +=  (not int(bit_str[n - 1 - ix])) * 2 **ix

        return value
