"""
Input
    x (int):
    y (int):
Output
    int: hamming distances
Notes
    - the hamming distance is the distance between two integers
    where corresponding bits are different

Example

    Input: x = 1, y = 4
    Output: 2

    Explanation:
    1   (0 0 0 1)
    4   (0 1 0 0)
           ↑   ↑

    The above arrows point to positions where the corresponding bits are different.

Ideas
    - use bin to and then compare the bits at each location
        + while loop starting from the right
        + when one values ends early, just assume 0s for the remaining values

            ie 0b1 -> 0b000001

    - use xor operator to do comparison
        + then count the number of ones that occur

"""

class Solution:
    def bit_shift(self, x: int, y: int) -> int:

        # References:
        # https://wiki.python.org/moin/BitwiseOperators

        # 1 when values are different
        xor = x ^ y

        dist = 0

        while xor:

            # Compare rightmost bit against 1
            if xor & 1:
                dist += 1

            # Shift rightmost bit away
            xor = xor >> 1

        return dist

    def hammingDistance(self, x: int, y: int) -> int:

        # References:
        # https://wiki.python.org/moin/BitwiseOperators

        # 1 when values are different
        xor = bin(x ^ y)

        return xor.count('1')


    ''' Different problem but similar enough '''

    def hammingWeight(self, n: int) -> int:
        """

        Time Complexity
            O(1)
        Space Complexity
            O(1)
        """        

        # Create a bit mask of just 1
        # 00000000000000000000000000000001
        mask: int = 1
        count: int = 0
        
        # Check each of the 32 bit locations for a 1
        # If a match exist, increment
        for i in range(32):

            if mask & n != 0:
                count += 1
        
            # Shift the bit mask by 1
            mask = mask << 1
        
        return count
        