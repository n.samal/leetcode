from typing import List

"""
Strategies:

- sort all numbers O(n^2), then crawl values to determine possible combos
- top down approach, search all numbers
    - route 1: try including the current number
    - route 2: try excluding the current number

"""


class Solution(object):

    def combinationSum2(self, candidates: List[int], target: int) -> int:
        """Find all unique combinations that sum to target

        Space Constraints: O(1)
        Time Constraints: O(n)

        Args:
            nums (List[int]): integers in unknown order
        Returns:
            int: first missing posiive
        Notes:
            Current method is too slow
        """

        # Sort candidates
        candidates = sorted(candidates)

        self.candidates = candidates   # original candidates
        self.n = len(self.candidates)  # number of candidates

        # self.solutions = set([])  # all valid combos
        self.solutions = []  # all valid combos
        self.target = target  # target summation

        # Crawl through every candidate
        # self.top_down(0, candidates, [])
        self.top_down2(candidates, target, [], 0)

        # Output the candidates
        return self.solutions

    def top_down2(self, arr: List[int], target: int, path: List[int], start_ix: int = 0):
        """Crawl down remaining candidates

        Sorted arra
        arr = [1, 2, 2, 3, 4, 5, 5]

        - keep adding depth to the path while valid
            []
            [1]
            [1, 2]
            [1, 2, 2]
            [1, 2, 2, 3]
        - prune paths that go beyond the target (backtracking)
            [1, 2, 2 | 3] > target

            no need to try paths with larger values
            [1, 2, 2 | 4]
            [1, 2, 2 | 5]

        - don't use paths with duplicates starting values

            [current path | start value]
            [1 | 2]
            [1 | 2] => duplicate path
            [1 | 3]
            [1 | 4]
            [1 | 5]
            [1 | 5] => duplicate path

            imagine the worst case scenario of
            arr = [1, 1, 1, 1, 1, 1]

            many repeating paths that we can skip

        Args:
            arr (List[int]): numerical candidates
            target (int): target summation
            path (List[int]): candidates used
            start_ix (int): starting index
        """

        print('used:', path, 'target:', target)

        # We found a valid combinaion
        # need this here for base case: target = 0, path = []
        if target == 0:

            # Found a candidate
            print("Candidate found")
            print(" ", path)

            # Save the combination
            self.solutions.append(path)

        else:

            # Continue path with remaining candidates
            for ix in range(start_ix, self.n):

                # print('used:', path, 'target:', target, 'val:', arr[ix])

                # We've started from this value before
                if ix > start_ix and arr[ix] == arr[ix - 1]:

                    print(' -> skipping duplicate seed')
                    continue

                # Overshooting target, skip all larger values
                elif target - arr[ix] < 0:

                    print(' -> too large, skipping remaining values')

                    return

                # Try including this value in our path
                # - update the sum
                # - add it to the path
                # - check the next value in the candidate list
                self.top_down2(arr, target - arr[ix], path + [arr[ix]], ix + 1)

    def top_down(
        self,
        current_sum: int,
        candidates_left: List[int],
        candidates_used: List[int]
    ):
        """Try all possible paths with backtracking

        Args:
            current_sum (int): current summation of using candidates
            candidates_left (List[int]): candidates left
            candidates_used (List[int]): candidates we've used
        Returns:

        Notes:
            Current method is too slow
        """

        print('used:', candidates_used, 'left:', candidates_left)

        # We found a valid combinaion
        if current_sum == self.target:

            # Save the combination
            self.solutions.add(tuple(candidates_used))

            # Found a candidate
            print("Candidate found")
            print(" ", candidates_used)

        else:

            # Go through remaining candidates
            for ix, value in enumerate(candidates_left):

                # No need to try remaining values#
                # remaining candidates will be greater than target
                if current_sum + value > self.target:
                    break

                else:

                    # Try using the current value
                    self.top_down(
                        current_sum + value,
                        candidates_left[ix + 1:],
                        candidates_used + [value],
                    )

                    # Try skipping value
                    self.top_down(
                        current_sum, candidates_left[ix + 1:], candidates_used
                    )


if __name__ == "__main__":

    s = Solution()

    # Test case 1
    # print(s.combinationSum2([10, 1, 2, 7, 6, 1, 5], 8))

    # Test case 2
    # print(s.combinationSum2([2, 5, 2, 1, 2], 5))

    # My test case
    # print(s.combinationSum2([1, 2, 3, 4, 5, 6, 7], 8))
    print(s.combinationSum2([1, 1, 3, 3, 5, 6, 7], 8))
