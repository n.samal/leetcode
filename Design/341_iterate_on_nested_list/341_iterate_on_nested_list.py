"""

Goal
    - Given a nested list of integers, implement an iterator to flatten it.

Notes
    - Each element is either an integer, or a list -- whose elements may also be integers or other lists.

Ideas

    - cheater way
        + create a flatten list then iterate

    - maintain a list of the relevant indices
        [1, 1, 1, ]
        if the len of the current sublist
             is 1, it's a single item
             greater than 1, it's a sublist
        + this could get complicated
        + crawl through the current sublist with pointers

    - crawl some how using BFS or DFS
        + need to maintan the correct order. review some examples for this
    
Strategies
    
    Flatten then Iterate
        - use recursion to create a new list
        - use an index and iterate through the new list on depend

        Time Complexity
            
            Creation: O(N + L)
                iterate through all items, and the size of lists during the recursion
            next: O(1)
            hasNext: O(1)

        Space Complexity
            O(n)
                iterate through all items, and the size of our recursion stack
    
    Queue Based

        - use a stack/queue to store our 'nodes'
        - add values in to our stack / queue

        nestedlist = [1, 2, 3, 4, [5, 6, [7, 8]], 9]

        Going Forward

        value = 1, just an int add it
        value = 2, just an int add it
        value = 3, just an int add it
        value = 4, just an int add it
        
        stack = [[5, 6, [7, 8]], 9]
        value = [5, 6, [7, 8]], it's a list iterate in reverse and add

            we want 5, 6 before 9. Iterate sublist in reverse, and append left
        
        stack = [5, 6, [7, 8], 9]
        value = 5, just an int add it
        value = 6, just an int add it
        
        stack = [[7, 8], 9] Iterate sublist in reverse, and append left
        stack = [7, 8, 9]
        
        value = 7, just an int add it
        value = 8, just an int add it
        value = 9, just an int add it

        Time Complexity
            O(n + L)

            have to go through each item, then each list
            - each list is used multiple times, first for reversal, then again in the queue
        
        Space Complexity
            O(n + L)
"""

# """
# This is the interface that allows for creating nested lists.
# You should not implement it, or speculate about its implementation

# """
class NestedInteger:
   def isInteger(self) -> bool:
       """
       @return True if this NestedInteger holds a single integer, rather than a nested list.
       """
       pass

   def getInteger(self) -> int:
       """
       @return the single integer that this NestedInteger holds, if it holds a single integer
       Return None if this NestedInteger holds a nested list
       """
       pass

   def getList(self) -> [NestedInteger]:
       """
       @return the nested list that this NestedInteger holds, if it holds a nested list
       Return None if this NestedInteger holds a single integer
       """
       pass


''' Flatten then Iterate '''

class NestedIterator:
    def __init__(self, nestedList: [NestedInteger]):
    
        self.arr = self.flatten_recurse(nestedList)    
        self.n = len(self.arr)
        self.ix = 0

    def flatten_recurse(self, nestedList):
        """Recursively crawl the sublist and add it to a new list"""

        arr = []

        def recurse(items):
            """Crawl the item further if it's not an integer"""
            
            for item in items:
            
                # Found an integer
                if item.isInteger():
                    arr.append(item.getInteger())
                
                # It's got more items
                else:
                    recurse(item.getList())
                
        recurse(nestedList)

        return arr


    def next(self) -> int:
        """Grab the next item in the list"""
        
        val = self.arr[self.ix]

        # Move to the next position for next time
        self.ix += 1

        return val

    def hasNext(self) -> bool:
        """Do we have more items"""
        return self.ix < self.n
   
''' Iterate on the Fly '''      

from collections import deque

class NestedIterator:

    def __init__(self, nestedList: [NestedInteger]):
        
        # Initialize the queue    
        self.queue = deque(nestedList)

    def next(self) -> int:
        """Iterate over our queue breath first search style (FIFO)

        Notes
            - we could also use extend left
        """

        while self.queue:

            # FIFO
            value = self.queue.popleft()

            # We've got an int
            if value.isInteger():
                return value.getInteger()
            
            # It's a list of sorts
            else:

                for item in value.getList()[::-1]:
                    self.queue.appendleft(item)

    def hasNext(self) -> bool:
        """Do we have more items in our queue"""
        return len(self.queue) > 0



# Your NestedIterator object will be instantiated and called as such:
# i, v = NestedIterator(nestedList), []
# while i.hasNext(): v.append(i.next())