"""
Inputs
    arr (List[str]): numbers, operators in unknown order
Output:
    Node: number of evaluated expression

Notes
    - postfix is provided as: value1, value2, operator
    - we need to write the evaluate expression to provide a 'Node' value

    - your TreeBuilder object will be instantiated and called as such:
        obj = TreeBuilder();
        expTree = obj.buildTree(postfix);
        ans = expTree.evaluate();

Ideas
    - we only want to evaluate the node when we have a valid postfix expression

    - maintain a stack and add new values
        + if a number, just add the number
        + if a operator, we should have numbers prior to this. we can
        evaluate the left, right value with this operator

    - creating a binary tree node will allow us to evaluate more easily

"""

def evaluate_postfix(self, postfix: List[str]) -> int:
    """Maintain a stack, and evaluate values 

    Time Complexity
        O(n)
    Space Complexity
        O(n)
    Notes
        - this is the base concept for evaluation, not the full
        implementation
    """

    # Postfix order is
    #    v1, v2, operator
    # We can asses adjacent values in that order like a falling stack
    stack = []
    operators = set('+-*/')

    for char in postfix:

        stack.append((char, None))
        print(stack)

        # Prior two values are numbers, current is operand
        while len(stack) >= 3 and stack[-1] in operators and stack[-2].isdigit() and stack[-3].isdigit():

            char = stack.pop()
            v2 = int(stack.pop())
            v1 = int(stack.pop())

            if char == '+':
                res = v1 + v2
            elif char == '-':
                res = v1 - v2
            elif char == '*':
                res = v1 * v2
            else:
                res = v1 // v2

            stack.append(str(res))

    print(stack)

    return int(stack[-1])

import abc 
from abc import ABC, abstractmethod 
"""
This is the interface for the expression tree Node.
You should not remove it, and you can define some classes to implement it.
"""

''' Object Oriented Approach '''

class Node(ABC):

    @abstractmethod
    # define your fields here
    def evaluate(self) -> int:
        """Evaluate the current node"""
        pass        

class Number(Node):
    """Numerical value"""
    def __init__(self, value):
        self.value = value

    def evaluate(self):
        return self.value

class BinaryNode(Node):
    """Generic binary tree node"""
    def __init__(self, left, right):
        self.left = left
        self.right = right

    def evaluate(self):
        """Overwrite this method"""
        pass

class PlusNode(BinaryNode):
    """Add children"""

    def evaluate(self):
        return self.left.evaluate() + self.right.evaluate()

class MinusNode(BinaryNode):
    """Subtract children"""

    def evaluate(self):
        return self.left.evaluate() - self.right.evaluate()

class MultiplyNode(BinaryNode):
    """Product of children"""

    def evaluate(self):
        return self.left.evaluate() * self.right.evaluate()

class DivisionNode(BinaryNode):
    """Division children"""

    def evaluate(self):
        return self.left.evaluate() // self.right.evaluate()

def simplify(char: str, left: 'Node', right: 'Node') -> 'Node':
    """Combine two numbers via postfix operator

    - create node which allows use of evaluate method later
    - node also defines the left and right children as we instantiate
    """

    if char == '+':
        node = PlusNode(left, right)
    elif char == '-':
        node = MinusNode(left, right)
    elif char == '*':
        node = MultiplyNode(left, right)
    else:
        node = DivisionNode(left, right)

    return node

class TreeBuilder(object):
    def buildTree(self, postfix: List[str]) -> 'Node':
        """Driver code to create node for expression tree

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        # We can asses adjacent values in that order like a falling stack
        # Build the tree from the bottom up, using the stack approach
        #   Postfix order = v1, v2, operator
        stack = []

        for char in postfix:
            if char.isdigit():
                stack.append(Number(int(char)))
            else:
                r = stack.pop()
                l = stack.pop()

                # Evaluate expression to a Node
                stack.append(simplify(char, l, r))

        return stack[0]

''' Stack Approach '''

class BinaryNodeValue(Node):
    """Generic binary tree node with a value"""
    def __init__(self, value, left = None, right = None):
        self.value = value
        self.left = left
        self.right = right

    def evaluate(self):
        """Evaluate recursively until we have a number"""

        # No kids, use the current value
        if self.left is None and self.right is None:
            return int(self.value)

        elif self.value == '+':
            return self.left.evaluate() + self.right.evaluate()

        elif self.value == '-':
            return self.left.evaluate() - self.right.evaluate()

        elif self.value == '*':
            return self.left.evaluate() * self.right.evaluate()

        else:
            return self.left.evaluate() // self.right.evaluate()

class TreeBuilder(object):
    def buildTree(self, postfix: List[str]) -> 'Node':
        """Create nodes for expression tree

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        # We can asses adjacent values in that order like a falling stack
        # Build the tree from the bottom up, using the stack approach
        #   Postfix order = v1, v2, operator
        stack = []

        for char in postfix:

            if char.isdigit():
                stack.append(BinaryNodeValue(int(char)))

            else:

                # Create node with priors as kids
                node = BinaryNodeValue(char)
                node.right = stack.pop()
                node.left = stack.pop()

                # Save for future expressions
                stack.append(node)

        return stack[0]
