"""
Goals
    
    Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.

    push(x) -- Push element x onto stack.
    pop() -- Removes the element on top of the stack.
    top() -- Get the top element.
    getMin() -- Retrieve the minimum element in the stack.


Ideas
     - use two stacks
        + one for mins
        + one for values
     - use a single stack with a tuple
        + (value, current_min)
"""


class MinStack:

    def __init__(self):
        """
        Create two empty stacks: one for values, one for current minimums
        """
        
        # One stack for values one for mins
        self.stack_val = []
        self.stack_min = []
        

    def push(self, x: int) -> None:
        
        # Grab the previous min
        prev = self.getMin()
        
        # Add value to the stack
        self.stack_val.append(x)
        
        # Add the relevant minimum
        if prev is None:
            self.stack_min.append(x)
        else:
            min_v = min(x, prev)
            self.stack_min.append(min_v)
            
    def pop(self) -> None:
        """Take the top value off the stack"""
        v = self.stack_val.pop()
        m = self.stack_min.pop()
        
        return v

    def top(self) -> int:
        """Grab the top of the stack if there"""
        
        if self.stack_val:
            return self.stack_val[-1]
        else:
            return -1

    def getMin(self) -> int:
        """Grab the current minimum"""
        
        if self.stack_min:
            return self.stack_min[-1]
        else:
            return None
        


# Your MinStack object will be instantiated and called as such:
# obj = MinStack()
# obj.push(x)
# obj.pop()
# param_3 = obj.top()
# param_4 = obj.getMin()