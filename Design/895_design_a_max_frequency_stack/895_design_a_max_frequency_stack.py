"""
Notes

    - maintain a stack that tracks values that most frequently occur
    - push(): add our value to the stack

    - pop() should remove the maximum frequency value
        + if there are ties for the maximum frequency, remove
        the value that is closest to the end of the stack

Ideas

    - maintain a normal stack, and dictionary for counts of each value
        + when we add a value we check to see if we have a new best
        + we we pop values, pop off values until we hit our max frequency value

        we must maintain this values and add them back. However, as we add them back
        we must update our max frequency.

        Note. here the max frequency could occur to the left of our position, so having
        a history of max frequency values will prevent us from searching O(n) for every pop

        save the max_freq_count, max_freq_value or both as we go

    - Optimal approach
        + maintain a counter for frequency of each value
        + also maintain a stack of values for each frequency

        frequency[3] = [4, 5, 6]

        all these values occur three times, and were added in that order

        + tracking the keys of the dictionary/count we know the max frequency
        and can remove the most recent high in O(1) time

"""

''' Stack of Value, and Max Frequency '''

class FreqStack:

    def __init__(self):

        self.dct = {}
        self.stack = []  # save the value and the current best

        self.max_freq_value = None
        self.max_freq_count = float('-inf')

    def push(self, x: int) -> None:
        """Add the value and update our max count"""

        # print(f'push: {x}')

        self.dct[x] = self.dct.get(x, 0) + 1
        self.update_count(x)
        self.stack.append((x, self.max_freq_count, self.max_freq_value))

    def update_count(self, x):
        """Did we find a new max frequency"""

        # print(f'update: {x}')

        if self.dct[x] >= self.max_freq_count:
            self.max_freq_count = self.dct[x]
            self.max_freq_value = x

    def pop(self) -> int:
        """Remove the first occurence of the max frequency value

        Time Complexity
            O(n)
        """

        # print(f'pop')
        # print(f' stack: {self.stack}')
        # print(f' most_frequ: {self.max_freq_value}')

        stack_pop = []

        # Remove values until we hit the frequent value
        while self.stack[-1][0] != self.max_freq_value:

            cur_value = self.stack.pop()[0]
            stack_pop.append(cur_value)
            self.dct[cur_value] -=1

        # Remove frequent value
        v, _, __ = self.stack.pop()
        self.dct[v] -= 1

        # We have a previous value, use that as our initial max count
        if self.stack:
            self.max_freq_count = self.stack[-1][1]
            self.max_freq_value = self.stack[-1][2]
        else:
            self.max_freq_count = float('-inf')

        # Add values back and update the new best
        while stack_pop:

            cur_value = stack_pop.pop()
            self.dct[cur_value] +=1
            self.update_count(cur_value)
            self.stack.append((cur_value, self.max_freq_count, self.max_freq_value))

        return v


''' Stack for each Frequency '''

from collections import defaultdict

class FreqStack:

    def __init__(self):

        self.dct = {}  # counter
        self.groups = defaultdict(list)
        self.max_freq_count = float('-inf')

    def push(self, x: int) -> None:
        """Add the value and update our max count"""

        # Update counter
        self.dct[x] = self.dct.get(x, 0) + 1

        # Update our best
        if self.dct[x] >= self.max_freq_count:
            self.max_freq_count = self.dct[x]

        # Save value the respective frequency stack
        self.groups[self.dct[x]].append(x)

    def pop(self) -> int:
        """Remove the first occurence of the max frequency value

        Time Complexity
            O(1)
        """

        # Grab last value at the max frequency and update our counter
        v = self.groups[self.max_freq_count].pop()
        self.dct[v] -= 1

        # Update our max frequency count if no more values left
        if len(self.groups[self.max_freq_count]) == 0:
            self.max_freq_count -= 1

        return v
