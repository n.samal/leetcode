"""

Ideas
    
    - initialize data structure to None
    - players can be represented as +1 and -1
        + positive 3 means player one wins
        + negative 3 means player two wins
        
    - after a player plays a value
        + check all potential wins
        + we need a minimum number of plays required before
        a win is possible
    
    Double Array
        - matrix representation initialized to None

        Time Complexity
            O(n)
        Space Complexity
            O(n*n)

    Single Array
        - single array
        - check the array 

    Summation
        - maintain a summation reprsenting each check
            + row
            + col
            + diag 1
            + diag 2
        - if the summations matches any of these,then report the win

        Time Complexity
            O(1)
        Space Complexity
            O(n)
    

"""

        
PLAYER_1_INT = 1
PLAYER_2_INT = -1

NO_WIN = 0
PLAYER_1_WIN = 1
PLAYER_2_WIN = 2

class TicTacToe:

    def __init__(self, n: int):
        """
        Initialize your data structure here.
        """
        
        # n by n data structure
        self.n = n
        self.arr = [[0 for col_ix in range(n)] for row_ix in range(n)]

    def move(self, row: int, col: int, player: int) -> int:
        """
        Player {player} makes a move at ({row}, {col}).
        @param row The row of the board.
        @param col The column of the board.
        @param player The player, can be either 1 or 2.
        @return The current winning condition, can be either:
                0: No one wins.
                1: Player 1 wins.
                2: Player 2 wins.
        """
        
        # Grab player integer
        if player == 1:
            player_val =  PLAYER_1_INT
        else:
            player_val =  PLAYER_2_INT
        
        # Mark the position in the array
        self.arr[row][col] = player_val
        
#         print('Board')
#         for row_ix in range(self.n):
#             print(self.arr[row_ix])
        
        
        # Played on a diagnol
        if row == col or row == self.n - 1 - col:
            diag_check = self.check_diagnol()
        
            if diag_check in [PLAYER_1_WIN, PLAYER_2_WIN]:
                return diag_check
        
        # Check the modified column
        column_check = self.check_column(col)
        
        if column_check in [PLAYER_1_WIN, PLAYER_2_WIN]:
            return column_check
        
        # Check row
        row_check = self.check_row(row)
        
        if row_check in [PLAYER_1_WIN, PLAYER_2_WIN]:
            return row_check
        else:
            return NO_WIN
    
    ''' Checks '''
    
    def check_column(self, column) -> int:
        """Check the column summation
        
        - positive n means all +1
        - negative n means all -1
        - neither means no win
        """

        sum_ = 0
        
        for row_ix in range(self.n):
            sum_ += self.arr[row_ix][column]
        
        if sum_ == self.n:
            return PLAYER_1_WIN
        elif sum_ == -self.n:
            return PLAYER_2_WIN
        else:
            return NO_WIN
        
    def check_row(self, row) -> int:
        """Check the row summation
        
        - positive n means all +1
        - negative n means all -1
        - neither means no win
        
        """
        sum_ = sum(self.arr[row])
        
        if sum_ == self.n:
            return PLAYER_1_WIN
        elif sum_ == -self.n:
            return PLAYER_2_WIN
        else:
            return NO_WIN
    
    def check_diagnol(self) -> int:
        """Check the row summation
        
        - positive n means all +1
        - negative n means all -1
        - neither means no win
        
        """
        sum_diag1 = 0
        sum_diag2 = 0
        
        # Left to right diagnol
        for ix in range(self.n):
            sum_diag1 += self.arr[ix][ix]
        
        # Right to left diagnol
        for ix in range(self.n):
            sum_diag2 += self.arr[ix][self.n - 1 - ix]
        
        # print(f'sum_diag1: {sum_diag1}')
        # print(f'sum_diag2: {sum_diag2}')
        
        # Check summation
        if sum_diag1 == self.n or sum_diag2 == self.n:
            return PLAYER_1_WIN
        elif sum_diag1 == -self.n or sum_diag2 == -self.n:
            return PLAYER_2_WIN
        else:
            return NO_WIN
        
# Your TicTacToe object will be instantiated and called as such:
# obj = TicTacToe(n)
# param_1 = obj.move(row,col,player)