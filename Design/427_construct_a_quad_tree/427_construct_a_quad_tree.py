"""
Inputs
    grid (List[List[int]]): binary grid
Outputs
    Node: quad tree representation of grid
Notes

    - break the grid into quads
        + where each quadrant is all the same value
        + if quad is not all the same, then we break that piece
        into smaller pieces, that fit that criteria (hiearchical)

        + a leaf indicates, this is the lowest dimensionality
        if not, there are more layers to dive into

    - attributes

        + isLeaf = does the value have layers below?
        + val = value for the layer
            * if similar use that value
            * if non-simlar use a value of 1

Ideas

    - apply recursive approach based upon dimensions
        + if the current layer is all similar, no need to dig digger
        + if layer is non-simlar we must break that into pieces

            * eventually the pieces become singular (1 by 1)
            which will become a node

    - worst case occurs if all values are different
        + every value is a node for the quad tree

        Time Complexity
            O(n^2)?
        Space Complexity
            O(n^2)?

    - best case, all values are similar. one leaf

"""

from typing import List

# Definition for a QuadTree node.
class Node:
    def __init__(self, val, isLeaf, topLeft, topRight, bottomLeft, bottomRight):
        self.val = val
        self.isLeaf = isLeaf
        self.topLeft = topLeft
        self.topRight = topRight
        self.bottomLeft = bottomLeft
        self.bottomRight = bottomRight

    def __repr__(self):
        return f'[{self.isLeaf}, {self.val}]'

    def __str__(self):
        return f'[{self.isLeaf}, {self.val}]'

class Solution:
    def construct(self, grid: List[List[int]]) -> 'Node':

        n_rows: int = len(grid)
        n_cols: int = len(grid[0])

        node = self.create_quad(grid, 0, n_rows, 0, n_cols)

        return node

    def create_quad(self, grid: List[List[int]], r_low: int, r_high: int, c_low: int, c_high: int):

        # Check if all the same
        same = all([grid[r][c] == grid[r_low][c_low] for r in range(r_low, r_high) for c in range(c_low, c_high)])

        # All values align
        if same:
            node = Node(grid[r_low][c_low], 1, None, None, None, None)
            return node

        else:

            dr = (r_high - r_low) // 2
            dc = (c_high - c_low) // 2

            r_mid = r_low + dr
            c_mid = c_low + dc

            # Split grid based upon the provided bounds
            node_top_left = self.create_quad(grid, r_low, r_mid, c_low, c_mid)
            node_top_right = self.create_quad(grid, r_low, r_mid, c_mid, c_high)

            node_bot_left = self.create_quad(grid, r_mid, r_high, c_low, c_mid)
            node_bot_right = self.create_quad(grid, r_mid, r_high, c_mid, c_high)

            node = Node(1, 0, node_top_left, node_top_right, node_bot_left, node_bot_right)
            return node

if __name__ == '__main__':

    # Example 1
    # grid = [[0,1],[1,0]]

    # Example 2
    # grid = [[1,1,1,1,0,0,0,0],[1,1,1,1,0,0,0,0],[1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1],[1,1,1,1,0,0,0,0],[1,1,1,1,0,0,0,0],[1,1,1,1,0,0,0,0],[1,1,1,1,0,0,0,0]]

    # Example 3
    # grid = [[1,1],[1,1]]

    # Example 4
    # grid = [[0]]

    # Example 5
    grid = [[1,1,0,0],[1,1,0,0],[0,0,1,1],[0,0,1,1]]

    obj = Solution()
    print(obj.construct(grid))
