"""
Inputs
    persons (List[int]): vote for candidate id
    times (List[int]): time of vote
Outputs
    int: candidate id

Ideas

    - maintain count of votes for each person
    - track the person with the most votes
        + update this with each new vote

    - main a historical record of leaders at each timestamp
        + use binary search to find the next smallest time

"""
from collections import defaultdict
import bisect
from typing import List


class TopVotedCandidate:

    def __init__(self, persons: List[int], times: List[int]):
        """

        - track the current leader
        - maintain history of leaders at each time step

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        Notes
            - could improve time by only adding leaders when the leader
            actually changes
            - try using a list of tuples rather than dictionary
        """

        # Track overall counts
        counts = defaultdict(int)
        leader = (-1, -1)  # Candidate, Vote Count

        # Sort votes by time
        # votes = [(p, t) for t, p in sorted(zip(times, persons), key=lambda t: t[0])]

        self.leaders = {}

        for person, time in zip(persons, times):

            counts[person] += 1

            # Update current leader and save historical record
            # break ties with most recent votes
            if counts[person] >= leader[1]:
                leader = (person, counts[person])
                self.leaders[time] = leader[0]

    def q(self, t: int) -> int:
        """Find top voted candidate

        Time Complexity
            O(log(n))
        Space Complexity
            O(n)
        """

        # Get leader at exact time
        if t in self.leaders:
            return self.leaders[t]

        # Grab the first time smaller than query
        times = list(self.leaders.keys())

        ix = bisect.bisect(times, t)

        return self.leaders[times[ix - 1]]


if __name__ == "__main__":

    # Example 1
    persons = [0, 1, 1, 0, 0, 1, 0]    
    times = [0, 5, 10, 15, 20, 25, 30]

    # Your TopVotedCandidate object will be instantiated and called as such:
    obj = TopVotedCandidate(persons, times)

    for t in [3, 12, 25, 15, 24, 8]:
        leader = obj.q(t)

        print(f'time: {t}   leader: {leader}')
