"""
Notes
    - set: will store the key, value and timestamp
    - get: will grab the value for the timestamp
        + if the timestamp is not avaialable grab
        the closest value
        + if no value exists then provide null string
Examples

    Example 1
        Input: 
            inputs = ["TimeMap","set","get","get","set","get","get"], inputs = [[],["foo","bar",1],["foo",1],["foo",3],["foo","bar2",4],["foo",4],["foo",5]]
        Output: 
            [null,null,"bar","bar",null,"bar2","bar2"]
        Explanation:   
            TimeMap kv;   
            kv.set("foo", "bar", 1); // store the key "foo" and value "bar" along with timestamp = 1   
            kv.get("foo", 1);  // output "bar"   
            kv.get("foo", 3); // output "bar" since there is no value corresponding to foo at timestamp 3 and timestamp 2, then the only value is at timestamp 1 ie "bar"   
            kv.set("foo", "bar2", 4);   
            kv.get("foo", 4); // output "bar2"   
            kv.get("foo", 5); //output "bar2"  

    Example 2
        Input: 
            inputs = ["TimeMap","set","set","get","get","get","get","get"], inputs = [[],["love","high",10],["love","low",20],["love",5],["love",10],["love",15],["love",20],["love",25]]
        Output: 
            [null,null,null,"","high","high","low","low"]

Ideas
    - we need multiple values for the same key

    - hierarchy
        + key
        + timestamp
        + value

    - use dictionary of dictionaries

        + store timestamp: value under each key
        + keys will be timestamps

        [1, 3, 6, 10]

        timestamp = 0, nothing smaller
        timestamp = 2
            fits between 1 and 3
            grab value at 1
        timestamp = 5
            fits between 3 and 6
            grabs value at 3
        timestamp = 12
            fits after 10
            grab value at 10

"""

# from collections import defaultdict
import bisect

""" Dictionary of Dictionaries """

class TimeMap:

    def __init__(self):
        self.dct = {}

    def set(self, key: str, value: str, timestamp: int) -> None:
        """Save value into dictionary of dictionaries"""        
        if key in self.dct:
            self.dct[key][timestamp] = value
        else:
            self.dct[key] = {timestamp: value}

    def get(self, key: str, timestamp: int) -> str:
        """Grab the closest key

        - use binary search to find the previous time
        Notes:
            - too slow (perhaps from gathering keys)
            - works for 99% of cases
        """

        if key not in self.dct:
            return ''

        if timestamp in self.dct[key]:
            return self.dct[key][timestamp]

        # Grab all timestamps
        # then find where timestamp will sit amongst previous times
        times = list(self.dct[key].keys())
        ix = bisect.bisect(times, timestamp)

        # print(f'times: {times}')
        # print(f' timestamp: {timestamp} ix: {ix}')

        # Grab the previous time if exists
        if ix == 0:
            return ''
        else:
            return self.dct[key][times[ix - 1]]

""" Dictionary of Tuples """

class TimeMap2:

    def __init__(self):
        self.dct = {}

    def set(self, key: str, value: str, timestamp: int) -> None:
        """Save value into dictionary of lists"""        
        if key in self.dct:
            self.dct[key].append((timestamp, value))
        else:
            self.dct[key] = [(timestamp, value)]

        print(f'set()   key: {key}   timestamp: {timestamp}   value: {value}   ')


    def get(self, key: str, timestamp: int) -> str:
        """Grab the closest key

        - use binary search to find the insertion position
            in the timestamps
            + grab the previous time

        References:
            https://www.w3schools.com/python/ref_func_chr.asp
            https://en.wikipedia.org/wiki/List_of_Unicode_characters
        """
        
        print(f'get()   key: {key}   timestamp: {timestamp}')

        items = self.dct.get(key, None)

        if not items:
            return ''

        # Find the position to sort the timestamp
        low = 0
        high = len(items) - 1

        while low <= high:

            mid = low + ((high - low) // 2)
            mid_time = items[mid][0]

            if mid_time == timestamp:
                return items[mid][1]

            elif timestamp > mid_time:
                low = mid + 1

            else:
                high = mid - 1

        ix = low

        print(f'  items: {items}')
        print(f'  ix: {ix}')

        # Grab previous time if it exists
        if ix == 0:
            return ''
        else:
            return items[ix - 1][1]

# Your TimeMap object will be instantiated and called as such:
# obj = TimeMap()
# obj.set(key,value,timestamp)
# param_2 = obj.get(key,timestamp)