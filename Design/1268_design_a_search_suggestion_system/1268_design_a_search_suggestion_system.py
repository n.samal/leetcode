"""
Inputs
    products (List[str]):
    searchWord (str): search query
Outputs
    List[List[str]]: search suggestions as we type
Notes
    - as we type the search query bring up the suggested
    product names

        + ie: searchWord = 'mouse'
        m
        mo
        mou
        ...
        mouse

    - suggestions should have a common prefix
    - we will display at most 3 suggestions
    - if there are more than 3 common prefixes
    just display the lexicographically minimums

Examples

    Example 1:

        Input: products = ["mobile","mouse","moneypot","monitor","mousepad"], searchWord = "mouse"
        Output: [
        ["mobile","moneypot","monitor"],
        ["mobile","moneypot","monitor"],
        ["mouse","mousepad"],
        ["mouse","mousepad"],
        ["mouse","mousepad"]
        ]
        Explanation: products sorted lexicographically = ["mobile","moneypot","monitor","mouse","mousepad"]
        After typing m and mo all products match and we show user ["mobile","moneypot","monitor"]
        After typing mou, mous and mouse the system suggests ["mouse","mousepad"]

    Example 2:

        Input: products = ["havana"], searchWord = "havana"
        Output: [["havana"],["havana"],["havana"],["havana"],["havana"],["havana"]]

    Example 3:

        Input: products = ["bags","baggage","banner","box","cloths"], searchWord = "bags"
        Output: [["baggage","bags","banner"],["baggage","bags","banner"],["baggage","bags"],["bags"]]

    Example 4:

        Input: products = ["havana"], searchWord = "tatiana"
        Output: [[],[],[],[],[],[],[]]

Ideas

    - use a prefix tree/trie
        + cascading dictionaries for each letter
        + we need to modify this, since we need the word at every node instead
        of just at the end
"""

from typing import List


class Solution:

    ''' Prefix Tree aka Trie: Words at each node '''

    def suggestions_by_trie1(self, products: List[str], searchWord: str) -> List[List[str]]:
        """

        - add each product to the prefix tree
        - for each substring in our search query
            + gather the top 3 suggestions

        Time Complexity
            O(n)  n = total number of characters in products
                  dependent on trie generation
        Space Complexity
            O(n)  depends on the longest word

        Notes
            - instead of storing the word at every node, we could instead
            only store them at the leaf like usual, then traverse
            the trie again to the find words
            - we traverse the trie 3 times and get the 3 most relevant results
        """

        self.dct = {}
        products = sorted(products)

        # Add a prefix tree for each word
        for word in products:
            self.get_prefix_tree(word)

        # Get the 3 suggestions for each substring
        n: int = len(searchWord)
        ans = []

        for ix in range(1, n + 1):

            substr = searchWord[:ix]
            suggestion = self.get_word(substr)

            # print(f'search: {substr}')
            # print(f' suggestion: {suggestion}')

            ans.append(suggestion)

        return ans

    def get_prefix_tree(self, word: str):
        """Add this word to the prefix tree

        m
         * = [mobile, moneypot, monitor]
         o
          * = [mobile, moneypot, monitor]
          b
           * = [mobile]
           .. etc
          n
           * = [moneypot, monitor]

        Time Complexity
            O(n)
        Space Complexity
            O(n*n)

        Notes
            - we store the word at each node level along the entire
            word substring range
        """

        dct = self.dct

        # Put the current character into the tree
        for char in word:

            if char not in dct:
                dct[char] = {}

            # Move to the lower level dictionary
            dct = dct[char]

            # Add word to node
            if '*' not in dct:
                dct['*'] = []

            dct['*'].append(word)

    def get_word(self, search: str) -> List[str]:
        """Dive into the prefix tree and get suggestions

        - crawl to the deepest level relevant to the search
        - then grab the top 3 suggestions

        Args:
            search (str): search substring
        Returns:
            List[str]: top 3 suggestions
        """

        dct = self.dct

        for char in search:

            if char in dct:
                dct = dct[char]
            else:
                return []

        return dct['*'][:3]

    ''' Prefix Tree aka Trie: Words at leaf only'''

    def suggestions_by_trie2(self, products: List[str], searchWord: str) -> List[List[str]]:
        """

        - add each product to the prefix tree
        - for each substring in our search query
            + gather the top 3 suggestions

        Time Complexity
            O(c)  c = total number of characters in products
                  dependent on trie generation
        Space Complexity
            O(n*26)  depends on trie nodes, which is dependent on words

        Notes
            - close but not complete
        """

        self.dct = {}
        products = sorted(products)

        # Add a prefix tree for each word
        for word in products:
            self.get_prefix_tree_standard(word)

        # Get the 3 suggestions for each substring
        n: int = len(searchWord)
        ans = []

        for ix in range(1, n + 1):

            substr = searchWord[:ix]
            suggestion = self.get_word_from_leaf(substr)

            print(f'search: {substr}')
            print(f' suggestion: {suggestion}')

            ans.append(suggestion)

        return ans

    def get_prefix_tree_standard(self, word: str):
        """Add this word to the prefix tree

        m
         o
          b
          n

        Notes
            - we store the word only at the leaf level of each word
        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        dct = self.dct

        # Put the current character into the tree
        # then move to the lower level dictionary
        for char in word:

            if char not in dct:
                dct[char] = {}
            dct = dct[char]

        # Add word to node
        if '*' not in dct:
            dct['*'] = []

        dct['*'].append(word)

    def get_word_from_leaf(self, search: str) -> List[str]:
        """Dive into the prefix tree and get suggestions

        - crawl to the deepest level matching the search prefix
        - then grab the top 3 suggestions

        Args:
            search (str): search substring
        Returns:
            List[str]: top 3 suggestions
        """

        # Find the closest prefix and node in our trie
        prefix = []
        dct = self.dct

        for char in search:
            if char in dct:
                dct = dct[char]
                prefix.append(char)
            else:
                break

        # Search for complete words from this node
        # and down
        self.suggestions = []

        if prefix:
            self.dfs(dct)

        return self.suggestions[:3]

    def dfs(self, trie: dict):
        """Find the next suggestion in our prefix tree

        - add all complete words in our prefix tree
        in alphabetical order
        - stop when we've gathred more than 3 suggestions

        Args:
            trie (dict): prefix tree
        """

        if len(self.suggestions) >= 3:
            return

        if '*' in trie:
            self.suggestions.extend(trie['*'])

        for key in trie:
            if key != '*':
                self.dfs(trie[key])

    ''' Binary Search '''

    def suggestions_by_binarysearch(self, products: List[str], searchWord: str) -> List[List[str]]:
        """Binary search

        - use binary search to determine the closest string greater than
         or equal to our query
        - then grab the next 3 products with matching prefixes

        Time Complexity
            O(n * log(n))

                - n = length of products
                - sorting of products list
                - searched each substring in the product list
        Space Complexity
            O(n)
        """
        products = sorted(products)
        n: int = len(products)

        ans = []
        m: int = len(searchWord)

        # For every substring
        # - get first suggestion >= query with binary search
        # - find the next 3 words with matching prefixes
        for ix in range(1, m + 1):
            substr = searchWord[:ix]
            n_sub: int = len(substr)

            jx = self.binary_search(products, substr)

            suggestions = []

            # Grab 3 word that have the same prefix
            for i in range(jx, min(jx + 3, n)):

                if products[i][:n_sub] != substr:
                    break

                suggestions.append(products[i])

            # Add suggestions
            ans.append(suggestions)

        return ans

    def binary_search(self, products, search) -> int:
        """Search for 1st product >= than the search query

        Args:
            products (List[str]): products in alphabetical order
            search (str): search query prefix

        Time Complexity
            O(log(n))  n = number of products
        Space Complexity
            O(1)

        Notes
            - https://stackoverflow.com/questions/4806911/string-comparison-technique-used-by-python
        """

        n: int = len(products)

        low: int = 0
        high: int = n - 1

        while low < high:

            mid = low + ((high - low) // 2)

            if products[mid] >= search:
                high = mid
            else:
                low = mid + 1

        return low


if __name__ == '__main__':

    # Example 1
    # products = ["mobile", "mouse", "moneypot", "monitor", "mousepad"]
    # searchWord = "mouse"
    # ans = [ ["mobile","moneypot","monitor"], ["mobile","moneypot","monitor"], ["mouse","mousepad"], ["mouse","mousepad"], ["mouse","mousepad"]]

    # Example 2
    # products = ["havana"]
    # searchWord = "havana"
    # ans = [["havana"],["havana"],["havana"],["havana"],["havana"],["havana"]]

    # Example 3
    # products = ["bags","baggage","banner","box","cloths"]
    # searchWord = "bags"
    # ans = [["baggage","bags","banner"],["baggage","bags","banner"],["baggage","bags"],["bags"]]

    # Example 4
    # products = ["havana"]
    # searchWord = "tatiana"
    # ans = [[],[],[],[],[],[],[]]

    obj = Solution()
    response = obj.suggestions_by_trie1(products, searchWord)
    response = obj.suggestions_by_trie2(products, searchWord)
    response = obj.suggestions_by_binarysearch(products, searchWord)

    print(ans == response)
