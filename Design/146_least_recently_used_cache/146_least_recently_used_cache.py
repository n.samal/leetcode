"""

Goals
    - Design and implement a data structure for Least Recently Used (LRU) cache. 
    - It should support the following operations: get and put.

    get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
    put(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.

    The cache is initialized with a positive capacity.

Cases
    
    Table is Empty
        
        get:
            + return -1
        set:
            + introduce the value 
            + modify the table capacity value
        
    Table is Partially Full
        
        get:
            + check for the value and retrieve it
            + reinsert this value back into the queue
        
        set:
            + introduce the value 
            + modify the table capacity value
        
    
    Table is Full
        get: 
            + check the table for the value and retrieve it
            + since we just checked it, make this 'used'
            + remove the old value, and reinsert this into the queue
        put:
            + remove the oldest value
            + insert the new value


Ideas
    - use a hash table and a queue
        + insert new items into the queue
        + pop out the oldest items

    - use an ordered dictionary
        + this is a mix between a hash table and a queue


"""

from collections import OrderedDict

class LRUCache:

    def __init__(self, capacity: int):
        """
        Args:
            capacity (int): number of items in cache
        """
        self.capacity = capacity
        self.d = OrderedDict()
        
    def get(self, key: int) -> int:
        """Grab the value from the cache if it exists"""
        
        # Make it recently used, then grab the value
        if key in self.d:
            self.d.move_to_end(key, last=True)
            return self.d[key]
        else:
            return -1 

    def put(self, key: int, value: int) -> None:
        """Insert this value into the cache"""

        # print(self.d)
        
        # If the key is already present or we have space
        # Just update the hash
        if key in self.d or len(self.d) < self.capacity:
            self.d[key] = value
            self.d.move_to_end(key, last=True)
            
        # Table is full, remove oldest value then introduce new
        else:
            self.d.popitem(last=False)
            self.d[key] = value
            self.d.move_to_end(key, last=True)
            

# Your LRUCache object will be instantiated and called as such:
# obj = LRUCache(capacity)
# param_1 = obj.get(key)
# obj.put(key,value)