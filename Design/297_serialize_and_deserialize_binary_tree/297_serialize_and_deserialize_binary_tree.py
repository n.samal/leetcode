from collections import deque


class TreeNode(object):

    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

    def __repr__(self):
        return f'({self.val})'

    def __str__(self):
        return self.val


''' NLR: PreOrder Traversal '''


class Codec:

    def serialize(self, root):
        """Crawl tree with preorder traversal (NLR)

        Args:
            root: TreeNode
        Returns:
            str: encoding

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        if not root:
            return ''
        
        arr = []
        queue = deque([root])

        while queue:

            node = queue.popleft()

            if not node:
                arr.append('null')
            else:
                arr.append(str(node.val))
                
                queue.append(node.left)
                queue.append(node.right)

        # Add last bracket and convert to string
        s = ','.join(arr)

        return s

    def deserialize(self, data: str):
        """Decodes your encoded data to tree.

        - iterate through the string similar to a linked list or general
        string to gather numbers
        - add values upon seeing a break point in string
        - includes null values

        Args:
            data: str
        Returns:
            TreeNode
        """
        
        # Null cases: '', []
        if len(data) <= 2:
            return None
        
        # Split and convert to integers
        arr = data.split(',')
        arr = [int(v) if v != 'null' else 'null' for v in arr]
        
        # Create the node, and connect to parent node
        for ix in range(len(arr)):

            arr[ix] = TreeNode(arr[ix])

            # Left = 2p + 1
            if ix % 2 != 0:

                p_ix = (ix - 1) // 2

                if p_ix >= 0:
                    arr[p_ix].left = arr[ix]

            # Right = 2p + 2
            else:
                p_ix = (ix - 2) // 2

                if p_ix >= 0:
                    arr[p_ix].right = arr[ix]

        return arr[0]        

    def deserialize2(self, data: str):
        """Decodes your encoded data to tree.


        - values provided are PreOrder (NLR)
        
        - split string and through it into a data queue
        - throw the first node into a parents queue
        
        - for each parent in the queue grab 
          data two values at a time (left, right)
            + connect the values to the parent
            + if those nodes have kids throw them into the queue
            for later processing
        
        Args:
            data: str
        Returns:
            TreeNode

        Notes
            - improve time complexity
        """
        
        # Null cases: '', []
        if len(data) <= 2:
            return None
        
        # Split and convert to integers
        arr = deque(data.split(','))

        val = arr.popleft()
        root = TreeNode(int(val))
        queue = deque([root])

        # Create the node, and connect to parent node
        while queue:

            parent = queue.popleft()

            if parent:

                # Get the kid values (NLR)
                left = arr.popleft()
                right = arr.popleft()

                if left != 'null':
                    parent.left = TreeNode(int(left))

                if right != 'null':
                    parent.right = TreeNode(int(right))

                # Add kids to the queue
                queue.append(parent.left)
                queue.append(parent.right)

        return root



if __name__ == '__main__':

    # Basic
    # s_in = "[1,2,3,null,null,4,5]"

    # Negative number
    # s_in = "[-1,0,1]"

    # Null case
    # s_in = "[]"

    # Failure
    s_in = '[5,2,3,null,null,2,4,3,1]'

    # Your Codec object will be instantiated and called as such:
    codec = Codec()
    # root = codec.deserialize(s_in)
    # codec.deserialize(codec.serialize(s_in))
    codec.serialize(codec.deserialize(s_in))
