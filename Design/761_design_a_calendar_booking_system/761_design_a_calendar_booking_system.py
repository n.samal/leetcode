"""
Notes

    - double bookings are allowed
    - triple bookings are not allowed

        + A triple booking happens when three events have some non-empty intersection 
        (ie., there is some time that is common to all 3 events.)

    - For each call to the method MyCalendar.book, 
        + return true if the event can be added to the calendar successfully without causing a triple booking. 
        + Otherwise, return false and do not add the event to the calendar.

Ideas

    - use a influx/outflow method similar to number of cars neededd
    for a trip

        + positive influx at start
        + negative influx at end

        + iterate over all times in order
        + then ensure our booking influx is never over 2

    - double loop

        + track all calendar events
        + track double booked time slots

        + if we overlap with a double book then fail
        + if we overlap with a normal calendar event
        add the event then add the overlap time to 
        double booked time slots

Cases

    No Overlap

    a    b
             c     d

    Full Overlap
    a           b
       c     d

    Partial Overlap

    a      b
        c     d

        Start Time
        End TIme

Key Ideas:
    - overlap time period = max(starts), min(stops)
    - overlap does not occur when max(starts) > min(ends)

References:
    https://leetcode.com/problems/my-calendar-ii/discuss/109522/Simplified-winner's-solution
    https://leetcode.com/problems/my-calendar-ii/discuss/323479/Simple-C++-Solution-using-built-in-map-(Same-as-253.-Meeting-Rooms-II)
"""

import bisect
from collections import defaultdict

''' Flux Based: Dictionary '''

class MyCalendarTwo:

    def __init__(self):

        self.dct = defaultdict(int)

    def book(self, start: int, end: int) -> bool:
        """

        Time Complexity
            O(n*log(n))  sorting of the time slots
        Space Complexity
            O(n)
        """

        # Add the booking
        self.dct[start] += 1
        self.dct[end] -= 1

        bookings: int = 0

        for time in sorted(self.dct.keys()):

            bookings += self.dct[time]

            # Over booked, remove the booking
            if bookings > 2:
                self.dct[start] -= 1
                self.dct[end] += 1
                return False

        return True

''' Flux Based: Dictionary with insertion sort for keys'''

class MyCalendarTwo:

    def __init__(self):

        self.dct = defaultdict(int)
        self.keys = []

    def book(self, start: int, end: int) -> bool:
        """

        Time Complexity
            O(n*log(n))  sorting of the time slots
        Space Complexity
            O(n)

        Notes
            - not significantly faster
        """

        if start not in self.dct:
            bisect.insort(self.keys, start)
        if end not in self.dct:
            bisect.insort(self.keys, end)

        # Add the booking
        self.dct[start] += 1
        self.dct[end] -= 1

        bookings: int = 0

        for time in self.keys:

            bookings += self.dct[time]

            # Over booked, remove the booking
            if bookings > 2:
                self.dct[start] -= 1
                self.dct[end] += 1

                return False

        return True

''' Flux Based: Bisection Method '''

class MyCalendarTwo:

    def __init__(self):

        self.arr = []

    def book(self, start: int, end: int) -> bool:
        """Same as previouus method but with array

        Time Complexity
            O(n)  insertion sort, and popping
        Space Complexity
            O(n)
        """

        # Add the booking
        key_in  = (start, 1)
        key_out = (end, -1)

        bisect.insort(self.arr, key_in)
        bisect.insort(self.arr, key_out)

        bookings: int = 0

        for time, flux in self.arr:

            bookings += flux

            # Over booked, remove the booking
            # find the insertion point to the left of item (original index)
            if bookings > 2:
                self.arr.pop(bisect.bisect_left(self.arr, key_in))
                self.arr.pop(bisect.bisect_left(self.arr, key_out))
                return False

        return True

''' Double Loop '''

class MyCalendarTwo:

    def __init__(self):
        
        self.calendar = []
        self.overlap = []

    def is_overlap(self, start1: int, end1: int, start2: int, end2: int) -> bool:
        """When no overlap, max(starts) > min(ends)"""
        return max(start1, start2) < min(end1, end2)

    def book(self, start: int, end: int) -> bool:
        """
        
        - maintain a list of normal bookings
        and double booking overlaps

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        # Search for overlap with double booking
        # Time 1
        # 1     4
        #     2      7
        for s, e in self.overlap:
            if self.is_overlap(start, end, s, e):
                return False
        
        # Check our normal calendar
        for s, e in self.calendar:
            if self.is_overlap(start, end, s, e):
                overlap = (max(start, s), min(end, e))
                self.overlap.append(overlap)
        
        # Add to normal calendar
        self.calendar.append((start, end))
        return True



# Your MyCalendarTwo object will be instantiated and called as such:
# obj = MyCalendarTwo()
# param_1 = obj.book(start,end)