"""

Goals
    - design a hit counter for the hits received in the past 5 minutes

Notes
    - You may assume that the earliest timestamp starts at 1.
    - the timestamp parameter (in seconds granularity)
    - It is possible that several hits arrive roughly at the same time.
    - Assume that calls are being made to the system in chronological order (ie, the timestamp is monotonically increasing). 
    

Ideas
    - use dictionary with timestamps

Strategies

    Dictionary 

        - use a dictionary to maintain the bucket for the current 5 minutes
            + timestamp: hits at this time
            + increment the hit counter as we get new values

        - if we're checking the hit counter at a specific time
            + iterate through all seconds prior to our time range
            + remove those times from the hit count, and delete them from our queue
            
            + every time we get a timestamp query, remove values smaller than
            our current time

        - Removal
            - removing all times prior to valid window can be time consuming
            - lets just check all keys see if they're valid
    
    Array with Modulus
        - maintain an array of 300 values that track the counts
            + at any time we only need to track the past 300 seconds

        - use the modulus of the timestamp to determine which index to save our
        time and count in
            + the modulus allows use to create a rotating index between 0 and 299 for any value
            + at time 1 we save to slot 0
            + when at time 301 when only care about times 2 to 301, we can overwrite the
            information at slot 0 again

            + when at 30 seconds we save to slot 29: time - 1 % 300
            + when at 330 seconds we save to slot 29 again: time - 1 % 300
                + we only need values from past time 30 to 330

        Hit: O(1)
        GetHit: O(300)
"""


class HitCounter:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        
        self.hits = 0
        self.dict = {}

    def hit(self, timestamp: int) -> None:
        """Record a hit.
        Args:
            timestamp (int): current timestamp (in seconds granularity).
        """
        
        # Increment overall hit count
        self.hits += 1
        
        # print(f'setHit:  time: {timestamp}  hits: {self.hits}')
        
        # Increment counter if in 
        if timestamp in self.dict:
            self.dict[timestamp] += 1
        else:
            self.dict[timestamp] = 1

    ''' Dictionary Method '''

    def getHits_v0(self, timestamp: int) -> int:
        """
        Return the number of hits in the past 5 minutes.
        Args:
            timestamp (int): current timestamp (in seconds granularity).
        
        Notes
            - remove timestamps prior to our valid bucket
            - track the last time removed, we'll use this as our
            starting point for the next iteration

            - too slow =/
        """
        
        # print(f'getHits:  time: {timestamp}  hits: {self.hits}')
        
        # Valid window starts here and ends at time stamp
        valid_window_start = timestamp - 300
        
        # Time stamp more than 5 minutes
        if timestamp > 5 * 60:
        
            # print(f'  valid_time_start: {valid_window_start}  valid_time_end: {timestamp}')
            # print(f'  remov_time_start: {self.removal_window_start}  remov_time_end: {valid_window_start}')
        
            # Remove all timestamps prior to this 5 minute bucket
            for time in range(self.removal_window_start, valid_window_start + 1):

                hits_i = self.dict.pop(time, 0)
                self.hits -= hits_i

                # Remove from our bucket count
                if time in self.dict:
                    del self.dict[time]

            # Update the next to start removing at
            self.removal_window_start = valid_window_start
                
        return self.hits

    def getHits(self, timestamp: int) -> int:
        """Return the number of hits in the past 5 minutes.

        Args:
            timestamp (int): current timestamp (in seconds granularity).
        
        Notes
            - remove timestamps prior to our valid bucket
            - only check the keys in our bucket not every time
        """
        
        # print(f'getHits:  time: {timestamp}  hits: {self.hits}')
        # print(f'  valid_time_start: {valid_window_start}  valid_time_end: {timestamp}')
        
        # Valid window starts here and ends at time stamp
        valid_window_start = timestamp - 300
    
        # Find all keys that need to be removed
        removal_keys = []
    
        for key in self.dict:
            
            if key <= valid_window_start:
                removal_keys.append(key)
        
        # Remove all timestamps prior to the valid window
        for key in removal_keys:
            hits_i = self.dict.pop(key, 0)
            self.hits -= hits_i
                
            # print(f' removing: {key}  hits_i: {hits_i}  hits: {self.hits}')
                    
        return self.hits

class HitCounter2:

    def __init__(self):
        
        # Save slots for 300 seconds
        self.arr = [[0,0] for _ in range(300)]

    def hit(self, timestamp: int) -> None:
        """Record a hit.
        Args:
            timestamp (int): current timestamp (in seconds granularity).
        """

        # Find the relevant index for the current time
        #  time 1 = slot 0
        #  time 300 = slot 299
        #  time 301 = 301 - 1 % 300 = slot 0 
        #  time 302 = 302 - 1 % 300 = slot 1
        #  overwrite the old values if it's not relevant anymore
        ix = (timestamp - 1) % 300

        # Determine the previous time was relevant
        time, count = self.arr[ix]

        # Update the previous count if it's the same time
        if time == timestamp:
            self.arr[ix][1] += 1

        else:
            self.arr[ix] = [timestamp, 1]

    ''' Array Method '''

    def getHits(self, timestamp: int) -> int:
        """Return the number of hits in the past 5 minutes.

        Args:
            timestamp (int): current timestamp (in seconds granularity).
        
        Notes
            - remove timestamps prior to our valid bucket
        """
        
        # print(f'getHits:  time: {timestamp}  hits: {self.hits}')
        # print(f'  valid_time_start: {valid_window_start}  valid_time_end: {timestamp}')

        hits: int = 0
        
        # Valid window starts here and ends at time stamp
        valid_window_start = timestamp - 300
    
        # Only count valid times
        for time, count in self.arr:
            
            if time > valid_window_start:
                hits += count
        
        return hits




# Your HitCounter object will be instantiated and called as such:
# obj = HitCounter()
# obj.hit(timestamp)
# param_2 = obj.getHits(timestamp)