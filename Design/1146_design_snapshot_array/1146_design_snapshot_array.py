"""
Goal

    Implement a SnapshotArray that supports the following interface:

    SnapshotArray(int length) initializes an array-like data structure with the given length.  Initially, each element equals 0.
    void set(index, val) sets the element at the given index to be equal to val.
    int snap() takes a snapshot of the array and returns the snap_id: the total number of times we called snap() minus 1.
    int get(index, snap_id) returns the value at the given index, at the time we took the snapshot with the given snap_id

Ideas
    - use an array of arrays to store values
        + this will be sparse, since we don't change all values
        all the time, only pieces are changed via set

    - at each index only store values that have changed

        + if searching for a particular snap id find
        the exact index, or the first index that is smaller

        + search for closest index
            * using basic iteration
            * using binary search

        + if no index available, just return 0

    - values are only saved whenever 'snap' is called

Notes
    - array of arrays works but is too slow
        Space Complexity: O(snaps * length)

References:
    https://docs.python.org/3/library/bisect.html
"""
from collections import OrderedDict


''' Sparse Array '''


class SnapshotArray1:
    """Array of arrays: is sparse"""

    def __init__(self, length: int):
        """Init"""   

        self.arr = [0 for _ in range(length)]
        self.snap_ix = -1 # current snap index
        self.snaps = []   # all values at each snap

    def set(self, index: int, val: int) -> None:
        """Set the array value"""
        self.arr[index] = val

    def snap(self) -> int:
        """Take a snapshot of the current array"""
        self.snaps.append(self.arr[:])
        self.snap_ix += 1

        return self.snap_ix

    def get(self, index: int, snap_id: int) -> int:
        return self.snaps[snap_id][index]


class SnapshotArray2:
    """Dictionary of values at each snap_id, same principle as before"""
    def __init__(self, length: int):
        """Initialize data structures"""

        self.snap_ix = -1  # current snap index
        self.snaps = {}    # dictionary for each index, then snap id
        self.tmp = {ix: 0 for ix in range(length)}      # temporary values

    def set(self, index: int, val: int) -> None:
        """Set value for temporary dict"""
        self.tmp[index] = val

        # print(f'set: ({index}, {val})')

    def snap(self) -> int:
        """Take a snapshot of the current array"""

        self.snap_ix += 1
        self.snaps[self.snap_ix] = self.tmp.copy()

        # print(f'snap: {self.snap_ix}')
        return self.snap_ix

    def get(self, index: int, snap_id: int) -> int:
        """Find the closest snap_id"""

        # print(f'get: ({index})')
        return self.snaps[snap_id][index]


''' Iterative Search '''


class SnapshotArray3:
    """Only save new values when snap is taken"""

    def __init__(self, length: int):
        """Initialize data structures"""

        self.snap_ix = -1  # current snap index
        self.snaps = {}    # dictionary for each index, then snap id
        self.tmp = {}      # temporary values

    def set(self, index: int, val: int) -> None:
        """Set value for temporary dict"""
        self.tmp[index] = val

        print(f'set: ({index}, {val})')

    def snap(self) -> int:
        """Take a snapshot of the current array"""

        self.snap_ix += 1

        print(f'snap: {self.snap_ix}')

        # Save the values to snap dict, and clear temp
        # only iterate over newly updated values
        for index in self.tmp:

            if index not in self.snaps:
                self.snaps[index] = OrderedDict()
            self.snaps[index][self.snap_ix] = self.tmp[index] 

        self.tmp = {}

        return self.snap_ix

    def get(self, index: int, snap_id: int) -> int:
        """Find the closest snap_id"""

        print(f'get: index={index}  snap_id={snap_id}')

        # No value stored at this index yet
        if index not in self.snaps:
            return 0

        # Grab exact value
        if snap_id in self.snaps[index]:
            return self.snaps[index][snap_id]

        # Find the last working snap_id
        while snap_id >= 0 and snap_id not in self.snaps[index]:
            snap_id -= 1

        if snap_id in self.snaps[index]:
            return self.snaps[index][snap_id]
        else:
            return 0


''' Binary Search '''


class SnapshotArray:
    """Only save new values when snap is taken"""

    def __init__(self, length: int):
        """Initialize data structures"""

        self.snap_ix = -1  # current snap index
        self.snaps = {}    # dictionary for each index, then snap id
        self.tmp = {}      # temporary values

    def set(self, index: int, val: int) -> None:
        """Set value for temporary dict"""
        self.tmp[index] = val

        print(f'set: ({index}, {val})')

    def snap(self) -> int:
        """Take a snapshot of the current array"""

        self.snap_ix += 1

        print(f'snap: {self.snap_ix}')

        # Save the values to snap dict, and clear temp
        # only iterate over newly updated values
        for index in self.tmp:

            if index not in self.snaps:
                self.snaps[index] = OrderedDict()
            self.snaps[index][self.snap_ix] = self.tmp[index] 

        self.tmp = {}

        return self.snap_ix

    def get(self, index: int, snap_id: int) -> int:
        """Find the closest snap_id"""

        print(f'get: index={index}  snap_id={snap_id}')

        # No value stored at this index yet
        if index not in self.snaps:
            return 0

        # Grab exact value if it exists
        if snap_id in self.snaps[index]:
            return self.snaps[index][snap_id]

        # Find the closest snap_id, smaller than value
        n: int = len(self.snaps[index])
        snap_ids = list(self.snaps[index].keys())

        print(f' snap_ids: {snap_ids}')

        low = 0
        high = n - 1
        ans = None

        while low <= high:

            mid = (low + high) // 2

            # Too large, go lower
            if snap_ids[mid] > snap_id:
                high = mid - 1

            # Smaller, save and move higher
            else:
                ans = mid
                low = mid + 1

        # No valid answer found
        if ans is None:
            return 0
        else:
            return self.snaps[index][snap_ids[ans]]


# Your SnapshotArray object will be instantiated and called as such:
obj = SnapshotArray(length)
obj.set(index, val)
param_2 = obj.snap()
param_3 = obj.get(index,snap_id)
