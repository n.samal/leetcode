"""
Notes

    Design your implementation of the circular queue. 
    The circular queue is a linear data structure in which the operations are performed based on FIFO (First In First Out) principle

    Your implementation should support following operations:

        MyCircularQueue(k): Constructor, set the size of the queue to be k.
        Front: Get the front item from the queue. If the queue is empty, return -1.
        Rear: Get the last item from the queue. If the queue is empty, return -1.
        enQueue(value): Insert an element into the circular queue. Return true if the operation is successful.
        deQueue(): Delete an element from the circular queue. Return true if the operation is successful.
        isEmpty(): Checks whether the circular queue is empty or not.
        isFull(): Checks whether the circular queue is full or not.

    - do not use the built in queue library

Examples

    Example:

        MyCircularQueue circularQueue = new MyCircularQueue(3); // set the size to be 3
        circularQueue.enQueue(1);  // return true
        circularQueue.enQueue(2);  // return true
        circularQueue.enQueue(3);  // return true
        circularQueue.enQueue(4);  // return false, the queue is full
        circularQueue.Rear();  // return 3
        circularQueue.isFull();  // return true
        circularQueue.deQueue();  // return true
        circularQueue.enQueue(4);  // return true
        circularQueue.Rear();  // return 4

Ideas
    - use an array or a linked list
    - add nodes and pop off/disconnect the head/tail accordingly

"""

class Node:
    """Linked List Node"""
    def __init__(self, value, nextNode=None):
        self.value = value
        self.next = nextNode

class MyCircularQueue:

    def __init__(self, k: int):
        """Initialize your data structure here. Set the size of the queue to be k."""
        self.capacity = k
        self.count = 0

        self.head = None
        self.tail = None

    def enQueue(self, value: int) -> bool:
        """Insert an element into the circular queue. Return true if the operation is successful."""

        if self.count == self.capacity:
            return False

        # No nodes, create a head & tail
        if self.count == 0:
            self.head = Node(value)
            self.tail = self.head

        # Add to previous tail
        else:
            node_new = Node(value)
            self.tail.next = node_new
            self.tail = node_new

        self.count += 1
        return True

    def deQueue(self) -> bool:
        """Delete an element from the circular queue. Return true if the operation is successful."""
        if self.count == 0:
            return False
        self.head = self.head.next
        self.count -= 1
        return True

    def Front(self) -> int:
        """Get the front item from the queue."""

        # If we had a node, return the head
        if self.count == 0:
            return -1
        else:
            return self.head.value

    def Rear(self) -> int:
        """Get the last item from the queue."""

        # If we had a node, return the tail
        if self.count == 0:
            return -1
        else:
            return self.tail.value

    def isEmpty(self) -> bool:
        """Checks whether the circular queue is empty or not."""
        return self.count == 0

    def isFull(self) -> bool:
        """Checks whether the circular queue is full or not."""
        return self.count == self.capacity