"""
Notes
    - initially provide sentences with the amount of times
    they've occured
    - as a user types the input we get one character at a time
        ie: 'i a#'
        
    - as they type we want to provide suggestions for each substring
        ie: 'i'
            'i '
            'i a'
            'i a#'
    - for each query we want the top 3 suggestions with the searched
    prefix
        + suggestions should be in order of occurences then alphabetically

    - when we've hit a #, this query has ended
    and we want to save the query to our history
"""

class AutocompleteSystem:

    def __init__(self, sentences: List[str], times: List[int]):
        
        self.trie = {}
        self.prev = ''
        n: int = len(sentences)
        
        for ix in range(n):
            
            self.add_to_prefix_trie(times[ix], sentences[ix])
            # print(sentences[ix])
            # print(self.trie)

    def add_to_prefix_trie(self, times: int, sentence: str):
        """Add the current word and count to the trie"""
        
        dct = self.trie

        for char in sentence:

            # print(f' char: {char}')

            if char not in dct:
                dct[char] = {}

            dct = dct[char]

        # Store the word and times
        if '*' not in dct:
            dct['*'] = {}

        dct['*'][sentence] = times

    def add_query_to_prefix_trie(self, sentence: str):
        """Add an occurence to the trie"""
        
        dct = self.trie

        for char in sentence:

            # print(f' char: {char}')

            if char not in dct:
                dct[char] = {}

            dct = dct[char]

        # Store the word and times
        if '*' not in dct:
            dct['*'] = {}

        # Add new item if necessary
        if sentence in dct['*']:
            dct['*'][sentence] += 1
        else:
            dct['*'][sentence] = 1

        
    def recurse(self, dct: dict):
        """Gather all words from this prefix trie position"""        
        for key in dct:
            
            if key == '*':
                for word in dct['*']:
                    self.words.append((dct['*'][word], word))
            else:
                self.recurse(dct[key])
    
        
    def input(self, char: str) -> List[str]:
        
        # Add new character to search
        self.prev += char
        
        dct = self.trie
        broke = False
        
        # Travel down the trie as long as we can
        for char in self.prev:
            if char not in dct:
                broke = True
                break
            else:
                dct = dct[char]
        
        # Find all words relevant to this prefix
        # Sort the words by hotness, then alphabetical
        if not broke:
            self.words = []
            self.recurse(dct)
            words = sorted(self.words, key=lambda w: (-w[0], w[1]))
            suggestion = [word for k, word in words[:3]]
        else:
            suggestion = []
    
        # Add to the trie, then clear the string for a new search
        if self.prev[-1] == '#':
            self.add_query_to_prefix_trie(self.prev[:-1])
            self.prev = ''
        
        # print(f"'{self.prev}'")
        # print(suggestion)
        
        return suggestion


# Your AutocompleteSystem object will be instantiated and called as such:
# obj = AutocompleteSystem(sentences, times)
# param_1 = obj.input(c)