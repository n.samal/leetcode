"""
Notes
    mplement the BrowserHistory class:

    - BrowserHistory(string homepage) Initializes the object with the homepage of the browser.
    - void visit(string url) Visits url from the current page. It clears up all the forward history.
    - string back(int steps) Move steps back in history. If you can only return x steps in the history and steps > x, you will return only x steps. Return the current url after moving back in history at most steps.
    - string forward(int steps) Move steps forward in history. If you can only forward x steps in the history and steps > x, you will forward only x steps. Return the current url after forwarding in history at most steps.

Ideas
    - maintain an array of our history and a pointer of
    where we are in the history

    - if visiting a new page, remove all previous forward history
    - if moving backwards or forwards
        + grab the value if possible
        + if beyond the bounds grab the bounding value
"""

class BrowserHistory:

    def __init__(self, homepage: str):
        """Maintain history"""

        self.arr = [homepage]
        self.ix: int = 0

    def visit(self, url: str) -> None:

        # Clear all forward history
        self.arr = self.arr[:self.ix + 1]

        # Add the new url
        self.arr.append(url)
        self.ix += 1

    def back(self, steps: int) -> str:
        """Go back in history"""

        if self.ix - steps < 0:
            self.ix = 0
        else:
            self.ix -= steps

        return self.arr[self.ix]

    def forward(self, steps: int) -> str:
        """Go forwards in history"""

        if self.ix + steps > len(self.arr) -1:
            self.ix = len(self.arr) - 1
        else:
            self.ix += steps

        return self.arr[self.ix]

# Your BrowserHistory object will be instantiated and called as such:
# obj = BrowserHistory(homepage)
# obj.visit(url)
# param_2 = obj.back(steps)
# param_3 = obj.forward(steps)