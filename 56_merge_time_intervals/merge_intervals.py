from typing import List


class Solution:

    def merge(self, intervals: List[List[int]]) -> List[List[int]]:

        # print('merge')

        if not intervals:
            return intervals

        # Sort intervals by start time
        intervals = sorted(intervals, key=lambda l: l[0])

        # Merged intervals
        merged = [intervals[0]]

        # Iterate through intervals
        for start, stop in intervals[1:]:

            # Rename for ease
            last_start, last_stop = merged[-1]

            # Compare current interval with merged interval
            if last_start <= start <= last_stop:

                # Update merged slot
                merged[-1][0] = last_start # we've already got the minimum start time
                # merged[-1][0] = min(last_start, start)
                merged[-1][1] = max(last_stop, stop)

            # No overlap
            else:
                merged.append([start, stop])

        return merged


if __name__ == "__main__":

    s = Solution()
    print(s.merge([[1, 3], [2, 6], [8, 10], [15, 18]]))
