
class Solution:

    @staticmethod
    def convert(s: str) -> int:
        """Convert the string to an integer"""

        summation: int = 0

        # Iterate through characters
        for ix, char in enumerate(s[::-1]):

            # Convert current character
            digit = int(char)

            summation += (digit * 10**ix)

        return summation

    def multiply(self, num1: str, num2: str) -> str:
        """Multiply the values with direct multiplication

        Strategy
        - Convert to numbers then multiply

        Notes
        - Do NOT directly convert inputs to integers
        - numbers only contain digits from 0 to 9
        - no leading zeros

        Args:
            num1 (str): value 1 
            num2 (str): value 2
        Returns:
            str: product of input values
        """

        # Convert values first
        num1 = self.convert(num1)
        num2 = self.convert(num2)

        return str(num1 * num2)

    def multiply_convert2(self, num1: str, num2: str) -> str:
        """Multiply the values indirectly

        Strategy
        - Convert a single value indirectly
        - multiply that single value by the broken down number

        Args:
            num1 (str): value 1
            num2 (str): value 2
        Returns:
            str: product of input values
        """

        # Convert a single number directly
        # ie: '123' => 123
        num1 = self.convert(num1)

        # Initliaze the summation
        summation: int = 0

        # Iterate through second number backwards
        for ix, char in enumerate(num2[::-1]):

            # Grab value representation
            # ie: '522' => 500 + 20 + 2
            value = int(char) * 10**ix

            # Multiply subchunk
            # ie: 123 * 20
            summation += num1 * value

        return str(summation)

    def multiply_place_by_place(self, num1: str, num2: str) -> str:
        """Compute the product of each number digit by digit

        Args:
            num1 (str): value 1
            num2 (str): value 2
        Returns:
            str: product of input values
        Notes:
            appears to have much slower runtime
        """
        summation: int = 0

        # Iterate through numbers backwards
        for ix, digit1 in enumerate(num1[::-1]):

            # Generate value for current digit
            value1 = int(digit1) * 10**ix

            # Iterate through second number backwards
            for jx, digit2 in enumerate(num2[::-1]):

                value2 = int(digit2) * 10**jx

                # Compute product and add to summation
                summation += value1 * value2

        return str(summation)

    def multiply_str_place_by_place(self, num1: str, num2: str) -> str:
        """Apply standard by hand multiplication method

        Args:
            num1 (str): value 1
            num2 (str): value 2
        Returns:
            str: product of input values
        TODO:
            finish this
        """

        # Initialize product string
        # ie: '00000'
        s: str = '0' * len(num1) * len(num2)

        # Iterate through string 1 backwards
        for ix, ch1 in num1[::-1]:

            # Initialize carry amount
            carry: int = 0

            # Iterate through string 2 backwards
            for jx, ch2 in num2[::-1]:

                # Multiply current char 1 by char 2
                # Leverage string arithmetic ie: x - '0'
                product = (ch1 - '0') * (ch2 - '0')

                # Calculate position for calculation
                # carry over position is next location
                pos1 = ix + jx
                pos2 = pos1 + 1

                # Add computation to current value
                tmp = (s[pos1] - '0') + product + carry

                # Apply carry over to next position

    def multiply_int_place_by_place(self, num1: str, num2: str) -> str:
        """Apply standard by hand multiplication method with ints

        Multiply each digit

        Time Complexity
            O(n * m)
        Space Complexiy
            O(n + m)

        Args:
            num1 (str): value 1
            num2 (str): value 2
        Returns:
            str: product of input values

        Notes:
            Account for edge cases
            - "0" * number
            - leading zeros
            - combined "0" and leading zeros

        """

        n = len(num1)
        m = len(num2)

        # Initialize integers
        # numbers will be less than size of digit summation
        # ie: 9999 * 99 => xxxx + xx => 6
        arr = [0] * (n + m)

        # Iterate through string 1 backwards
        for ix in range(n - 1, -1, -1):

            # Iterate through string 2 backwards
            for jx in range(m - 1, -1, -1):

                # Add product to current position
                # we want to account for carry over!
                # ie: 10 + 5 => 10, keep 1 and 0
                sum = int(num1[ix]) * int(num2[jx]) + arr[ix + jx + 1]

                # Overwrite trailing digit to next location
                # ie: 15 => 5
                arr[ix + jx + 1] = sum % 10

                # Keep main digit
                # ie: 15 => 1
                arr[ix + jx] += sum // 10

        # Skip leading zeros
        ix = 0
        while ix < (n + m) and arr[ix] == 0:
            ix += 1

        # Combine values into a string
        s = ''
        for i in range(ix, n + m):
            s += str(arr[i])

        # If empty return '0'
        if s:
            return s
        else:
            return '0'


if __name__ == "__main__":

    # Convert values
    # print(Solution.convert('0'))
    # print(Solution.convert('10'))
    # print(Solution.convert('100'))
    # print(Solution.convert('1000'))
    # print(Solution.convert('2000'))
    # print(Solution.convert('2020'))

    # Integer array
    s = Solution()
    # print(s.multiply_int_place_by_place('123', '45'))
    # print(123 * 45)

    # print(s.multiply_int_place_by_place('1230', '45'))
    # print(1230 * 45)

    print(s.multiply_int_place_by_place('9133', '0'))
    print(9133 * 0)


# TODO: reverse strings initially for easier indexing