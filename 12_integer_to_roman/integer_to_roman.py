def integer_to_roman(remainder: int) -> str:
    """Convert the integer into it's roman numeral representation

    Args:
        remainder (int): integer to convert
    Returns:
        s (string): string of roman characters
    """

    # String to integer conversion
    conversion = {1000: 'M',
                  900: 'CM',
                  500: 'D',
                  400: 'CD',
                  100: 'C',
                  90: 'XC',
                  50: 'L',
                  40: 'XL',
                  10: 'X',
                  9: 'IX',
                  5: 'V',
                  4: 'IV',
                  1: 'I',
                  }

    # Roman numeral string
    s: str = ''

    # Negative values not accounted for
    if remainder < 0:
        raise ValueError('Unable to convert negative values')

    # Find the largest value smaller than the remainder

    while remainder > 0:

        # Go through all keys from largest to smallest
        for key in conversion.keys():

            # We found the largest value less than our remainder
            if key <= remainder:
                break

        # Add character
        s += conversion[key]

        # Update remainder
        remainder -= key

    return s


if __name__ == "__main__":

    # print(integer_to_roman(3))
    # print(integer_to_roman(4))
    # print(integer_to_roman(9))
    # print(integer_to_roman(58))
    print(integer_to_roman(1994))
