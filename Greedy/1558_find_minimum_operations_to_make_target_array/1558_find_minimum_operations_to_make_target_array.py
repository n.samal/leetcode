"""
Inputs
    goal (List[int]): goal state
Outputs
    int: minimum number of operations to the reach the goal state
Notes
    - we start with an array of all zeros
    - at each step we can do either of two steps
        + add 1 to any index
        + multiply every index by 2

Ideas
    - top down
        + crawl both routes while valid
        + works, but is exensive in space and time

    - for a single number
        + we can use log_2 to determine
        how many times we can double the value

            8 => 3

            count = 1: 0 to 1
            count = 2: 1 to 2
            count = 3: 2 to 4
            count = 4: 4 to 6
            count = 5: 6 to 8

        + for odd numbers we can leverage
          even + 1

            9 = 8 + 1

        + the log_2 doesn't tell us the exact answer

            for some numbers the closest number gathered from log_2
            is still quite far away. 

            num = 100,000
            log_2(100,00) = 16.609

            however 2^16 = 65536, which is only ~65% of the way
                    2^17 = 131072

            instead of just using the log_2, we can just divide by two as long as possible
            once we hit a dead end, we can shift the value by one in order to divide by 2

            100000 -> 50000 -> 25000 -> 12500 -> 6250 -> 3125 -> 1562.5
            
            3125 -> 3124
                    3124 -> 1562 -> 781
                                    781 -> 780
                                           780 -> etc..

Hand Calc 1

    goal = [2, 4, 8, 16]

    for reference, these numbers can be divide by 2 th
    log_2 = [1, 2, 3, 4]
    log_2 = [1, 2, 3, 4]

    change the smallest number last, focus on
    largest numbers

    0   cur =  [0, 0, 0, 0]
    1   cur =  [0, 0, 0, 1]
    2   cur =  [0, 0, 0, 2]
    3   cur =  [0, 0, 1, 2]
    4   cur =  [0, 0, 2, 4]
    5   cur =  [0, 1, 2, 4]
    6   cur =  [0, 2, 4, 8]
    7   cur =  [1, 2, 4, 8]
    8   cur =  [2, 4, 8, 16]

Hand Calc 2
    goal  = [4, 2, 5]
    log_2 = [2, 1, 2.]

    focus on larger numbers first
    goal    =  [4, 2, 5]
    0   cur =  [0, 0, 0]
    1   cur =  [0, 0, 1]
    2   cur =  [1, 0, 1]
    3   cur =  [2, 0, 2]
    4   cur =  [2, 1, 2]
    5   cur =  [4, 2, 4]
    6   cur =  [4, 2, 5]

    - seems like focus on larger numbers first
    - add one to get a base value (0 to 1)
        + then multiply them to largest
        even value possible
        - then add one if we need to get to an odd
            16 to 17

    - we can do some multiplications simultaneously
        ie: [4, 2, 5] the 4 and 5

    - the count of ones is indiviual to each index
    - the doubles we can group together if done intelligently

References:
    https://leetcode.com/problems/minimum-numbers-of-function-calls-to-make-target-array/discuss/805971/Python3-Simple-intuition-without-bitwise-operations
    https://leetcode.com/problems/minimum-numbers-of-function-calls-to-make-target-array/discuss/805672/Simple-Explanation
"""

from typing import List

class Solution:

    def top_down(self, goal: List[int]) -> int:
        """"T

        Time Complexity
            O(?)
        Space Complexity
            O(?)

        Notes
            - too slow
            - seems overly complicated
        """

        # We can do two operations
        # operation 0: add 1 to a specific index
        # operation 1: multiply all values by 2
        self.min_ans = float('inf')

        arr_cur = [0 for _ in goal]
        n: int = len(goal)

        memo = set([])

        def top_down(arr_cur, count: int = 0):

            print(f'arr_cur: {arr_cur} count: {count}')

            state = (tuple(arr_cur), count)

            if state in memo:
                return

            if count > self.min_ans:
                return

            # Mark current state as visited
            memo.add(state)

            ''' Route 2x '''
            if count > 0:
                arr_new = arr_cur[:]
                possible: bool = True

                for ix in range(n):

                    if arr_cur[ix] * 2 <= goal[ix]:
                        arr_new[ix] = arr_cur[ix] * 2
                    else:
                        possible = False
                        break

                if possible:
                    top_down(arr_new, count + 1)

            ''' Route Plus 1 '''

            working: int = 0
            for ix in range(n):

                if arr_cur[ix] + 1 <= goal[ix]:
                    arr_new = arr_cur[:]
                    arr_new[ix] += 1

                    top_down(arr_new, count + 1)

                elif arr_cur[ix] == goal[ix]:
                    working += 1

            # All are matching
            if working == n:
                self.min_ans = min(self.min_ans, count)
                return True

        top_down(arr_cur)

        return self.min_ans

    def greedy(self, goal: List[int]) -> int:
        """

        - if we can group together our multiplications by two
        together, we're limited by the max number of twos required
        - for individual values we still need to shift numbers by 1
        to achieve the closest even, and get to zero

        Time Complexity
            O(n*c)

            c = log_2(max(a))
        Space Complexity
            O(1)
        """
        ones: int = 0  # shifts by 1
        twos: int = 0  # multiply by 2

        for val in goal:

            twos_here: int = 0

            while val:

                if val % 2 == 0:
                    twos_here += 1
                    val = val // 2
                else:
                    ones += 1
                    val -= 1

            twos = max(twos, twos_here)

        return ones + twos

if __name__ == '__main__':

    # arr = [1, 5]
    # arr = [1, 5]
    # arr = [4, 2, 5]
    arr = [3, 2, 2, 4]
    # arr = [2, 4, 6, 8]
    # arr = [1000000000]

    obj = Solution()
    print(obj.top_down(arr))
    print(obj.greedy(arr))
