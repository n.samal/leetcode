"""
Inputs
    str
Outputs
    List[str]: max number of non-empty, non overlapping strings
Notes
    - find the maximum number of substrings that don't overlap
    - if we include a character in our substring we must include
    all of its instances
    - if there's a tie, we want to gather the list of substrings
    with the smallest number of characters

Examples

    Example 1:

        Input: s = "adefaddaccc"
        Output: ["e","f","ccc"]
        Explanation: The following are all the possible substrings that meet the conditions:
        [
          "adefaddaccc"
          "adefadda",
          "ef",
          "e",
          "f",
          "ccc",
        ]
        If we choose the first string, we cannot choose anything else and we'd get only 1. If we choose "adefadda", we are left with "ccc" which is the only one that doesn't overlap, thus obtaining 2 substrings. Notice also, that it's not optimal to choose "ef" since it can be split into two. Therefore, the optimal way is to choose ["e","f","ccc"] which gives us 3 substrings. No other solution of the same number of substrings exist.

Example 2:

        Input: s = "abbaccd"
        Output: ["d","bb","cc"]
        Explanation: Notice that while the set of substrings ["d","abba","cc"] also has length 3, it's considered incorrect since it has larger total length.

Ideas
    - gather start and stop for each character
    - prioritize small strings

    - seems like finding non-overlapping meetings
    - try recursive approach
        + maintain bounds
        + maintain count of substrings
        + maintain count of chars in substr list

References:
    https://leetcode.com/problems/maximum-number-of-non-overlapping-substrings/discuss/744420/C%2B%2BJavaPython-Interval-Scheduling-Maximization-(ISMP)
    https://en.wikipedia.org/wiki/Interval_scheduling
"""

from typing import List


class Solution:

    ''' Original: brainstorming '''

    def maxNumOfSubstrings(self, s: str) -> List[str]:
        """

        - grab bounds for each character
        - use DFS to search for meetings outside
          our current bounds
        - track the longest substring list
        """

        self.max_sub = []
        self.min_char = float('inf')

        self.s = s
        self.n = len(s)

        # Gather first and last occurence
        # of characters (inclusive, exclusive)
        self.bounds = {}

        for ix, char in enumerate(s):

            if char in self.bounds:
                self.bounds[char][1] = ix + 1
            else:
                self.bounds[char] = [ix, ix + 1]

        self.seen = {char: False for char in self.bounds}
        self.dfs(0)

        return self.max_sub

    def dfs(self, ix: int, subs: List[str] = [], bounds: List[int] = [float('inf'), float('-inf')]):
        """

        Args:
            ix (int): starting index in string
            subs (List[str]): substrings
            bounds (List[int]): starting index, ending index bounds
        """

        # Track best
        if subs and len(subs) > len(self.max_sub):

            print(subs)
            m = sum(map(len, subs))
            self.min_char = m
            self.max_sub = subs

        for jx in range(ix, self.n):

            char = self.s[jx]

            if self.seen[char] is False:

                bounds_cur = self.bounds[char]

                # Must start after our current limit
                if bounds_cur[0] >= bounds[1]:

                    # Update bounds
                    bounds_new = bounds[:]
                    bounds_new[0] = min(bounds_new[0], bounds_cur[0])
                    bounds_new[1] = max(bounds_new[1], bounds_cur[1])
                    sub_new = self.s[bounds_cur[0]:bounds_cur[1]]

                    # Start crawl beyond our current bounds
                    self.seen[char] = True
                    self.dfs(bounds_cur[1], subs + [sub_new], bounds_new)
                    self.seen[char] = False

    ''' Greedy '''

    def greedy(self, s: str) -> List[str]:
        """Greedily add meetings

        - grab bounds for each character
        - sort 'meetings' by earliest end time
        - add meetings that start after our current meeting
            + update aggregate meeting bounds

        Time Complexity
            O(n^2)  slow due to character bound loop
        Space Complexity
            O(n)

        Notes
            - works but is slow due to character bound loop
        """

        max_sub = []

        # Gather first and last occurence
        # of characters (inclusive, exclusive)
        bounds = {}

        for ix, char in enumerate(s):
            if char in bounds:
                bounds[char][1] = ix + 1
            else:
                bounds[char] = [ix, ix + 1]

        # Update the bounds to include inbetween characters
        # ie: 'abab' with our 'a' we must take a 'b'
        # this is tricky, since the bounds bounce around
        for key in s:
            start, stop = bounds[key]

            for ix in range(start + 1, stop):

                start_other, stop_other = bounds[s[ix]]

                bounds[key][0] = min(bounds[key][0], start_other)
                bounds[key][1] = max(bounds[key][1], stop_other)

        # Grab meetings that end first
        keys = sorted(bounds.keys(), key=lambda k: (bounds[k][1], bounds[k][0]))

        # Bounds of aggregated meeting
        start_agg = float('inf')
        stop_agg = float('-inf')

        # Greedily add meetings if they don't conflict
        for key in keys:

            start, stop = bounds[key]

            if start >= stop_agg:

                max_sub.append(s[start:stop])
                start_agg = min(start_agg, start)
                stop_agg = max(stop_agg, stop)

        return max_sub

    def greedy2(self, s: str) -> List[str]:
        """Greedily add meetings

        - grab bounds for each character
        - sort 'meetings' by earliest end time
        - add meetings that start after our current meeting
            + update aggregate meeting bounds
            + keep updating until our bounds stop moving

        Time Complexity
            O(26 * 26)  slow due to character bound loop
        Space Complexity
            O(n)

        Notes
            - Passes 95% of test cases
        """

        max_sub = []

        # Gather first and last occurence
        # of characters (inclusive, exclusive)
        bounds = {}

        for ix, char in enumerate(s):
            if char in bounds:
                bounds[char][1] = ix + 1
            else:
                bounds[char] = [ix, ix + 1]

        # Update the bounds to include inbetween characters
        # ie: 'abab' with our 'a' we must take a 'b'
        # this is tricky, since the bounds bounce around,
        # we iterate until bounds stop moving
        for key in s:
            start, stop = bounds[key]

            for key2 in set(s[start:stop]):

                bounds[key] = (min(bounds[key][0], bounds[key2][0]), 
                                max(bounds[key][1], bounds[key2][1]))

        # Grab meetings that end first
        keys = sorted(bounds.keys(), key=lambda k: (bounds[k][1], bounds[k][0]))

        # Bounds of aggregated meeting
        start_agg = float('inf')
        stop_agg = float('-inf')

        # Greedily add meetings if they don't conflict
        for key in keys:

            start, stop = bounds[key]

            if start >= stop_agg:

                max_sub.append(s[start:stop])
                start_agg = min(start_agg, start)
                stop_agg = max(stop_agg, stop)

        return max_sub

    def greedy3(self, s: str) -> List[str]:
        """Greedily add meetings

        - grab bounds for each character
        - sort 'meetings' by earliest end time
        - add meetings that start after our current meeting
            + update aggregate meeting bounds
            + keep updating until our bounds stop moving

        Time Complexity
            O(26 * 26)  slow due to character bound loop
        Space Complexity
            O(n)

        Notes
            - Passes 95% of test cases
        References:
            https://leetcode.com/problems/maximum-number-of-non-overlapping-substrings/discuss/744726/Python-Easy-to-Read-solution-with-explanation
        """

        max_sub = []

        # Gather first and last occurence
        # of characters (inclusive, exclusive)
        bounds = {}

        for ix, char in enumerate(s):
            if char in bounds:
                bounds[char][1] = ix + 1
            else:
                bounds[char] = [ix, ix + 1]

        # Update the bounds to include inbetween characters
        # ie: 'abab' with our 'a' we must take a 'b'
        # this is tricky, since the bounds bounce around,
        # we iterate until bounds stop moving
        for key in s:
            start, stop = bounds[key]

            while True:

                bounds_l, bounds_r = bounds[key]

                for key2 in set(s[start:stop]):

                    bounds[key] = (min(bounds[key][0], bounds[key2][0]), 
                                   max(bounds[key][1], bounds[key2][1]))

                if (bounds_l, bounds_r) == bounds[key]:
                    break

        # Grab meetings that end first
        keys = sorted(bounds.keys(), key=lambda k: (bounds[k][1], bounds[k][0]))

        # Bounds of aggregated meeting
        start_agg = float('inf')
        stop_agg = float('-inf')

        # Greedily add meetings if they don't conflict
        for key in keys:

            start, stop = bounds[key]

            if start >= stop_agg:

                max_sub.append(s[start:stop])
                start_agg = min(start_agg, start)
                stop_agg = max(stop_agg, stop)

        return max_sub


if __name__ == '__main__':

    # Example 1
    # s = "adefaddaccc"

    # Example 2
    # s = "abbaccd"

    # Example 2
    # s = "abab"

    # More complicated
    s = "cabcccbaa"
    # s = "cbadabdb"

    obj = Solution()
    # print(obj.maxNumOfSubstrings(s))
    print(obj.greedy(s))
    print(obj.greedy2(s))
