"""
Inputs
    a (str): string 1   
    b (str): string 2

Outputs
    bool: is palindrome possible

Notes

    - splits must be completed at the same index for both strings

        + a = a_prefix + a_suffix
        + b = b_prefix + b_suffix

    - we search for palindromes that are created by

        + candidate1 = a_prefix + b_suffix
        + candidate2 = b_prefix + a_suffix

Examples

    Example 1:

        Input: a = "x", b = "y"
        Output: true
        Explaination: If either a or b are palindromes the answer is true since you can split in the following way:
        aprefix = "", asuffix = "x"
        bprefix = "", bsuffix = "y"
        Then, aprefix + bsuffix = "" + "y" = "y", which is a palindrome.

    Example 2:

        Input: a = "abdef", b = "fecab"
        Output: true

    Example 3:

        Input: a = "ulacfd", b = "jizalu"
        Output: true
        Explaination: Split them at index 3:
        aprefix = "ula", asuffix = "cfd"
        bprefix = "jiz", bsuffix = "alu"
        Then, aprefix + bsuffix = "ula" + "alu" = "ulaalu", which is a palindrome.

    Example 4:

        Input: a = "xbdef", b = "xecab"
        Output: false

Hand Calc

    Example 2

        a = "abdef"
        b = "fecab"

        split_ix = 0

            a = '' + 'abdef'
            b = '' + 'fecab'

            cand1 = '' + 'fecab' = fecab
            cand2 = '' + 'abdef' = abdef

        split_ix = 1

            a = 'a' + 'bdef'
            b = 'f' + 'ecab'

            cand1 = 'a' + 'ecab' = aecab
            cand2 = 'f' + 'bdef' = fbdef

        split_ix = 2

            a = 'ab' + 'def'
            b = 'fe' + 'cab'

            cand1 = 'ab' + 'cab' = abcab
            cand2 = 'fe' + 'def' = fedef

            cand2 is a palindrome

    Example 3

        a = "ulacfd"
        b = "jizalu"

        Split at every index

        split_ix = 0

            a = '' + 'ulacfd'
            b = '' + 'jizalu'

            cand1 = '' + 'jizalu' = jizalu
            cand2 = '' + 'ulacfd' = ulacfd

        split_ix = 1

            a = 'u' + 'lacfd'
            b = 'j' + 'izalu'

            cand1 = 'u' + 'izalu' = uizalu
            cand2 = 'j' + 'lacfd' = lacfd

        split_ix = 2

            a = 'ul' + 'acfd'
            b = 'ji' + 'zalu'

            cand1 = 'ul' + 'zalu' = ulzalu
            cand2 = 'ji' + 'acfd' = jiacfd

        split_ix = 3

            a = 'ula' + 'cfd'
            b = 'jiz' + 'alu'

            cand1 = 'ula' + 'alu' = ulaalu
            cand2 = 'jiz' + 'cfd' = jizcfd

            cand1 works.
            a_prefix == reversed(b_suffix)

Cases

    Null + Other

    Small + Large

        'ab' + 'xyxba'

    Same + Same

        even: 'ab' + 'ba'
        odd : 'abc' + 'cba'

        just check if substr1 = reverse(substr2)

Ideas

    - if combining a nullstring with a palindrome, we get a palindrome

        + ie: cand1 = '' + 'x' = 'x'
        + ie: cand2 = '' + 'y' = 'y'
        + ie: cand2 = '' + 'yzy' = 'yzy'

    - palindromes can be of even or odd length

        + odd: 'azxza'
        + even: 'azza'

    - assuming a candidate is a palindrome. we combine
    the front of one string, with the back of another

        a = "abdef"
        b = "fecab"

        splicing front of A with back of B
        the characters don't match so it's not possible

        'a' != 'b'

        splice the fron of B with the back of A

        'f' vs 'f'
        'fe' vs 'fe'

        once we hit a differing chracter, check if the remaining
        piece is a palindrome

        ''
        'a'
        'abba'

"""


class Solution:

    ''' Utility '''

    def is_palindrome1(self, string: str) -> bool:
        return string == string[::-1]

    def is_palindrome2(self, string: str) -> bool:

        n: int = len(string)

        low: int = 0
        high: int = n - 1

        while low <= high and string[low] == string[high]:

            low += 1
            high -= 1

        return low > high

    ''' Brute Force '''

    def checkPalindromeFormation(self, a: str, b: str) -> bool:
        """

        - try every split
        - then check if candidate values are palindromes

        Time Complexity
            O(n*n)
        Space Complexity
            O(n)
        """

        n: int = len(a)

        for ix in range(n):

            # Try first candidate
            cand1 = a[:ix] + b[ix:]

            if self.is_palindrome2(cand1):
                return True

            # Try second candidate
            cand2 = b[:ix] + a[ix:]

            if self.is_palindrome2(cand2):
                return True

        return False

    ''' Greedy '''

    def greedy(self, a: str, b: str) -> bool:
        """

        - see if front of a and back of b works
        - if not, try the front of b and back of a

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        if self.check(a, b):
            return True
        else:
            return self.check(b, a)

    def check(self, a: str, b: str) -> bool:
        """

        Time Complexity
            O(n)
        Space Complexity
            O(1)

        Notes
            - use pointers instead of slicing to save some space
            complexity
        """

        n: int = len(a)

        low: int = 0
        high: int = n - 1

        # Check the front of A against the back of B
        while low < high and a[low] == b[high]:
            low += 1
            high -= 1

        # Check if either remaining middle characters is a palindrome
        return self.is_palindrome2(a[low:high + 1]) or self.is_palindrome2(b[low:high + 1])


if __name__ == '__main__':

    # Example 1 (true)
    a = "x"
    b = "y"

    # Example 2 (true)
    a = "abdef"
    b = "fecab"

    # Example 3 (true)
    a = "ulacfd"
    b = "jizalu"

    # Trickier case
    a = "pvhmupgqeltozftlmfjjde"
    b = "yjgpzbezspnnpszebzmhvp"

    # Trickier case
    a = "aejbaalflrmkswrydwdkdwdyrwskmrlfqizjezd"
    b = "uvebspqckawkhbrtlqwblfwzfptanhiglaabjea"

    obj = Solution()
    # print(obj.checkPalindromeFormation(a, b))
    print(obj.greedy(a, b))
