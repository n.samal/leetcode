"""
Inputs:
    prices (List[int]): prices on day i
Outputs:
    int: maximum profit
Goal:
    - find the maximum profit by buying once and selling once

Ideas


Strategies
    Iterate twice
        - find the minimum price
        - find the maximum price to the right of each value
        - compute the profit using: max_
        
        - crawl from the right to track the max value
        - then compute the 

    One Pass
        - find the largest peak after the smallest valley
    
        - maintain two running values
            - current_best_max
            - current_best_min
        
        - compute the delta between the two as our best
            - assume we bought on the smallest day and sold on the highest day


"""


class Solution:
    def iterate_twice(self, prices: List[int]) -> int:
        """Iterate twice
        
        - iterate once to determine max prices to the right of each day
        - iterate again to determine the potential profit for each day
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        
        n: int = len(prices)
        
        # Find max value on the right of each index
        max_right = [price for price in prices]
        
        for ix in range(n - 2, -1, -1):
            max_right[ix] = max(prices[ix], max_right[ix + 1])
        
        # print(f'max_right: {max_right}')
        
        # Find delta at each index
        max_delta = 0
        
        for ix in range(n):
            max_delta = max(max_delta, max_right[ix] - prices[ix])
            
        # If we can't make a profit, then don't sell
        if max_delta < 0:
            return 0
        else:
            return max_delta
    
    def greedy(self, prices: List[int]) -> int:
        """Greedy calculation
        
        - on each day calculate the potential profit
            using the previous computed min
        - only considered the minimum from previous days
        
        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """
        
        n: int = len(prices)
            
        if n == 0:
            return n
            
        max_profit: int = 0
        min_price: int = prices[0]
            
        for price in prices[1:]:
            
            # Track the max profit found
            profit = price - min_price
            max_profit = max(max_profit, profit)
            
            # Track the minimum price found
            min_price = min(min_price, price)
        
        # If we can't make a profit, then don't sell
        if max_profit < 0:
            return 0
        else:
            return max_profit
