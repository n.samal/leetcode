"""
Inputs
    grid (List[List[int]]): binary grid
Output
    int: minimum swaps necessary to make the grid valid
Notes
    - grid is n x n
    - swaps can be made between adjacent rows
    - grid vaid when all cells above the main diagnol
    are zero
    - output -1 if not possible

Examples

    Example 1
        Input: grid = [[0,0,1],
                       [1,1,0],
                       [1,0,0]]
        Output: 3

    Example 2

        Input: grid = [[0,1,1,0],
                       [0,1,1,0],
                       [0,1,1,0],
                       [0,1,1,0]]
        Output: -1
        Explanation: All rows are similar, swaps have no effect on the grid.

    Example 3

        Input: grid = [[1,0,0],
                       [1,1,0],
                       [1,1,1]]
        Output: 0


Ideas
    - since we need all the cells above the diagnol
    to be zero, we want many zeros

            1, 0, 0, 0, 0
            1, 1, 0, 0, 0
            1, 1, 1, 0, 0
            1, 1, 1, 1, 0
            1, 1, 1, 1, 1

        + we want to organize by number of trailing zeros
        + most trailing zeros at the top

    - to be possible we need a certain number of zeros in each column
        + last column has most zeros (n - 1)
        + 2nd to last has 1 less (n - 2)
        + etc ...

    - use bubble sort style of sortig to swap adjacent rows
        + start at the back and swap to get the desired
        row in place (n - 1 to 0)
        + then focus on remaining rows ( n - 1 to 1)

        + how to track which row is of interest?
            * we don't need the rows, but just the number
            of trailing zeros

Manual Example

    current state => counts = [0, 1, 5, 3, 2, 4]
    goal state    => counts = [5, 4, 3, 2, 1, 0]

    start at 0 and swap adjacent items until we get to the
    end. Where end is defined as n - 1 - ix

    0 < 1: swap
    old_counts = [0, 1, 5, 3, 2, 4]
    new_counts = [1, 0, 5, 3, 2, 4]

    0 < 5: swap
    old_counts = [1, 0, 5, 3, 2, 4]
    new_counts = [1, 5, 0, 3, 2, 4]

Manual example 2

    - sort does not necessarily give us the minimum swaps
    - we just need each row to meet the minimum requirement for zeros
    but not to be sorted

    for a grid where n = 6

        min_required = [5, 4, 3, 2, 1, 0]

    in our example, the number of trailing zero counts
    current =  [5, 2, 5, 3, 2, 5]
    meets   =  [T, F, T, T, T, T]

    let's swap the one at index 1, since it fails
    required = [5, 4, 3, 2, 1, 0]
    old      = [5, 2, 5, 3, 2, 5]
    new      = [5, 5, 2, 3, 2, 5]
    meets   =  [T, T, F, T, T, T]

    swap again
    required = [5, 4, 3, 2, 1, 0]
    old      = [5, 5, 2, 3, 2, 5]
    new      = [5, 5, 3, 2, 2, 5]
    meets   =  [T, T, T, T, T, T]

    - likely want to start with the most stringent
    requirement and then go to relaxed requirements
    ie: 5, 4, 3, 2, 1, 0

    - if we meet the requirement for the column move
    to the next.
    - if we don't meet, then search right, for
    the first value that meets the requirement.
        + swap all adjacent values to bring that
        to the current column
        + count the swaps required

"""
from typing import List


class Solution:
    def bubble_sort(self, grid: List[List[int]]) -> int:
        """Bubble sort

        - count the number of trailing zeros
        - use bubble sort to bring the smallest
        number of zeros to the bottom of the grid
        - check that it meets the requirement after
        we've done all our swaps for position n - 1 - i

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)

        References:
            https://www.geeksforgeeks.org/bubble-sort/
        Notes
            - passes 80% of cases
            - sorting is not necessarily the minimum!
        """

        n: int = len(grid)

        ''' Count number of trailing zeros '''
        counts = [0 for _ in range(n)]

        for r in range(n):

            c = n - 1
            while c >= 0 and grid[r][c] == 0:
                counts[r] += 1
                c -= 1

        ''' Bubble Sort '''
        # Start at the front and bring all the smaller counts
        # of trailing zeros to the bottom of the grid
        # back end will stay organized

        min_swaps: int = 0

        for r1 in range(n):
            for r2 in range(n - r1 - 1):
                if counts[r2] < counts[r2 + 1]:
                    counts[r2], counts[r2 + 1] = counts[r2 + 1], counts[r2]
                    min_swaps += 1

            # Check that the desired count is met
            # ie: we need at least n - 1 zeros on the first row
            if counts[n - 1 - r1] < r1:
                return -1

        return min_swaps

    def greedy_bubble(self, grid: List[List[int]]) -> int:
        """Greedy bubble sort

        - count the number of trailing zeros in each row
        - iterate over each column
        - if we meet the zero requirement move on
        - if not, find the first working column
            + swap values to bring that into place

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        """

        n: int = len(grid)

        ''' Count number of trailing zeros '''
        counts = [0 for _ in range(n)]

        for r in range(n):

            c = n - 1
            while c >= 0 and grid[r][c] == 0:
                counts[r] += 1
                c -= 1

        ''' Bubbleish Sort '''
        # bring the first value that meets our requirement
        # to the current column

        min_swaps: int = 0

        for r1 in range(n):

            min_needed = n - 1 - r1

            # Find the first column which meets our requirement
            ix = r1
            while ix < n and counts[ix] < min_needed:
                ix += 1

            # Unable to find a column meeting our requirement
            if ix == n:
                return -1

            # Swap values to bring to owkring number
            # to the current column
            while ix != r1:
                counts[ix], counts[ix - 1] = counts[ix - 1], counts[ix]
                min_swaps += 1
                ix -= 1

        return min_swaps


if __name__ == '__main__':

    # Example 1
    # grid = [[0,0,1],
    #        [1,1,0],
    #        [1,0,0]]

    # Dummy
    # grid = [
    # [1, 0, 0, 0, 0],
    # [1, 1, 1, 1, 1],
    # [1, 1, 1, 1, 0],
    # [1, 1, 1, 0, 0],
    # [1, 1, 0, 0, 0]]

    # Impossible case
    grid = [[0,1,1,0],
            [0,1,1,0],
            [0,1,1,0],
            [0,1,1,0]]

    # Failed case
    # Sort is not necessarily the min
    # we need each row to meet the minimum requirement
    # min_required = [5, 4, 3, 2, 1, 0]

    # grid = [[1, 0, 0, 0, 0, 0],
    #         [0, 1, 0, 1, 0, 0],
    #         [1, 0, 0, 0, 0, 0],
    #         [1, 1, 1, 0, 0, 0],
    #         [1, 1, 0, 1, 0, 0],
    #         [1, 0, 0, 0, 0, 0]]

    obj = Solution()
    # print(obj.minSwaps(grid))
    print(obj.greedy_bubble(grid))
