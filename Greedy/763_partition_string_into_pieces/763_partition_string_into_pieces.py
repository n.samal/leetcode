"""
Inputs
    S (str): string of characters
Outputs
    List[int]: size of substrings for each partition
Goal
    - partition this string into as many parts as possible so that each letter appears in at most one part

Example

    Example 1

        Input: S = "ababcbacadefegdehijhklij"
        Output: [9,7,8]
        Explanation:
        The partition is "ababcbaca", "defegde", "hijhklij".
        This is a partition so that each letter appears in at most one part.
        A partition like "ababcbacadefegde", "hijhklij" is incorrect, because it splits S into less parts.

Ideas
    - track first time and last time we've seen each character
        + compare bounds of each character group
        + merge common groups, and create new partitions
        for non intersecting groups

    - seems very similar to merged meeting events
            + in that problem, sorting of the event times helped tremendously

Strategies

    Tracking Bounds (First & Last)

        - track the first time and last time we've seen each character
        - iterate through the string again
            + compare the bounds of current character against our last partition
            + if no overlap, then create a new partition
            + if some overlap then expand the bounds of the partition

        Time Complexity
            O(n)

            1st iteration for creating index tracking
            2nd iteration for partition comparison
            3rd iteration for computing lengths

        Space Complexity
            O(n)

    Double Iteration

        - use the same logic as before but only tracking the current partition values
            + partition start
            + partition stop

        - if the current character needs more length, extend the partition

        Time Complexity
            O(n)

        Space Complexity
            O(1)
"""

from typing import List


class Solution:
    def partitionLabels(self, S: str) -> List[int]:
        """Track first and last occurence of each character

        - track bounds for each character
        - merge 'bounds' for each character like merging meetings
        - compute length of each meeting

        Time Complexity
            O(n) three iterations through string
        Space Complexity
            O(n)
        """

        n: int = len(S)

        # Track occurence of each character
        # update first occurence once
        # update last occurence many times
        seen = {}

        for ix, char in enumerate(S):

            if char not in seen:
                seen[char] = [ix, ix]
            else:
                seen[char][1] = ix

        # Init partition with first character
        parts = [seen[S[0]]]

        for ix in range(1, n):

            char = S[ix]
            end_ix = seen[char][1]

            # print(f'ix: {ix}   char: {char}   parts: {parts}')

            # We're past the last partition
            # create a new partition with the current character
            if ix > parts[-1][1]:
                parts.append(seen[char][:])

            # Meeting end is beyond partition bounds
            # update partition bound requirement
            elif end_ix > parts[-1][1]:
                parts[-1][1] = end_ix

        # Compute length for each partition
        # must account characters at index 0
        # ie: 2 - 0 = length 3
        lengths = [p[1] - p[0] + 1 for p in parts]

        return lengths

    def greedy(self, S: str) -> List[int]:
        """Track last occurence only

        - merge 'bounds' for each character like merging meetings
        - only track the bounds of the last meeting

        Time Complexity
            O(n) three iterations through string
        Space Complexity
            O(n)
        Notes:
            not significantly faster, or better on memory
        """

        # Track last occurence of each character
        seen = {char: ix for ix, char in enumerate(S)}

        # Starting and ending position of parition
        part_strt = 0
        part_stop = 0

        lengths = []

        for ix, char in enumerate(S):

            # If current character demands a longer parition, extend it
            part_stop = max(seen[char], part_stop)

            # We made it to the end of current partion
            if ix == part_stop:

                # Save the length of current partion
                lengths.append(part_stop - part_strt + 1)

                # Start a new partition
                part_strt = ix + 1
                part_stop = ix + 1

        return lengths
