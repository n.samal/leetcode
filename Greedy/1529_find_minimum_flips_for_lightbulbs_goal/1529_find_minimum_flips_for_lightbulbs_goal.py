"""
Inputs
    target (str): goal state of bulbs
Outputs
    int: minimum number of flips to achieve target
Notes
    - bulb states
        + 0: off
        + 1: on
    - initially all bulbs are off
    - when you switch on any bulb, i 
    it flips all bulbs to the right in the opposite direction

Examples

    
    Example 1:

        Input: target = "10111"
        Output: 3
        Explanation: Initial configuration "00000".
        flip from the third bulb:  "00000" -> "00111"
        flip from the first bulb:  "00111" -> "11000"
        flip from the second bulb:  "11000" -> "10111"
        We need at least 3 flip operations to form target.
    
    Example 2:

        Input: target = "101"
        Output: 3
        Explanation: "000" -> "111" -> "100" -> "101".
    
    Example 3:

        Input: target = "00000"
        Output: 0
    
    Example 4:

    Input: target = "001011101"
    Output: 5

Ideas
    - greedily flip all bulbs
        + get all bulbs to the left of us to be at the goal state
        + just focus on the right bulbs

    - if bulbs are all zeros do nothing

        + more generally if all bulbs are at the desired current state
        don't do anything, just keep moving to the right

        + if the current bulb is the opposite of what we want, then
        flip the remainig bulbs

        + we don't have to the flip the bulb itself, but only the state
        we're expecting

        initially expecting 0s
        then expecting 1s
        etc..
"""


class Solution:
    def minFlips(self, target: str) -> int:
        """
        
        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        n: int = len(target)

        state = False  # state
        count: int = 0
        ix: int = 0

        while ix < n:

            if int(target[ix]) == state:
                ix += 1

            else:
                count += 1
                state = not state

        return count


if __name__ == '__main__':

    # Examples 
    # test = "10111"
    # test = "101"
    # test = "00000"
    target = "001011101"

    obj = Solution()
    print(obj.minFlips(target))
