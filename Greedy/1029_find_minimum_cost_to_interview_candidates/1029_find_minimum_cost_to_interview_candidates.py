"""
Inputs
    costs (List[List[int]]): costs of flying candidates
Outputs
    int: minimum total cost
Notes
    - we must send 2n candidates to either city A or city B
    - we can only send n candidates to each city
    - we are given the costs for each candidate
        [cost for city A, cost for city B]
    - find the best way to send the candidates to reduce cost

Examples

    Example 1

        Input: costs = [[10,20],[30,200],[400,50],[30,20]]
        Output: 110
        Explanation: 
        The first person goes to city A for a cost of 10.
        The second person goes to city A for a cost of 30.
        The third person goes to city B for a cost of 50.
        The fourth person goes to city B for a cost of 20.

        The total minimum cost is 10 + 30 + 50 + 20 = 110 to have half the people interviewing in each city.

    Example 2

        Input: costs = [[259,770],[448,54],[926,667],[184,139],[840,118],[577,469]]
        Output: 1859

    Example 3

        Input: costs = [[515,563],[451,713],[537,709],[343,819],[855,779],[457,60],[650,359],[631,42]]
        Output: 3086

Ideas

    - brute force
        + top down 

    - sort cities by cost
        + which metric to use?

    - the cheapest A may also be paired with the cheapest B
        + this isn't necessarily the best overall
             
             A B
            10 10
            20 10
            40 5000
            40 10000

            here our B costs would be much larger if we only
            choose the cheapest A

        + we should choose the As that saves us the most money relative
        to the other city

            A      B    Delta (A - B)
            259	  770	-511
            448	  54	394
            926	  667	259
            184	  139	45
            840	  118	722
            577	  469	108

        find the cities that save us the most money for A relative to B, pick
        the first n, then the rest are B

            A      B    Delta (A - B)
            259   770   -511
            184   139   45
            577   469   108
            926   667   259
            448   54    394
            840   118   722
"""

class Solution:
    def twoCitySchedCost(self, costs: List[List[int]]) -> int:
        """

        - calculate the relative cost difference between traveling
        to either city (A - B)
        - sort values to find cheapest relative costs for a city

        Time Complexity
            O(n*log(n))
        Space Complexity
            O(n)
        """

        total: int = 0
        n: int = len(costs)
        mid: int = n // 2

        # Find the cheapest relative cost for A - B
        costs = sorted(costs, key=lambda c: c[0] - c[1])

        # First half goes to A
        for cityA, _ in costs[:mid]:
            total += cityA

        # Last half goes to B
        for _, cityB in costs[mid:]:
            total += cityB

        return total
