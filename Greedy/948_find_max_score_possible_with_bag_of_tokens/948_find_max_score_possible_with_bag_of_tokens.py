"""
Inputs
    P (int): initial power
    tokens (List[int]): value of tokens
Outputs
    int: maximum score possible

Notes
    - we start with
        + an initial Power P
        + an initial score of 0

    - we can play a token 2 ways

        + if score >= 1, you can play any token
        and add that token value to the Power total
        but also need to remove 1 from the score

        + if power P >= token[i]
        then you can play that token 

        you will gain +1 score, but lose that Power
        according to the token value

    - we want to maximize the score

Ideas

    - sorting the tokens will help make our choices easier

    - if we have no score we must choose some coin P <= Power

        + choosing the smallest coin available should reduce
        our Power hit and increase our score which we could use later

        + while we still have Power we can maximize our points
        by using small tokens

    - if we have a score, choosing the largest token may allow
    us to gain more score in the future

    - if dynamic programming is used, define the states with

        + Power level
        + Score
        + tokens used & left (possible bitmask)

    - see if we can solve greedily

"""

class Solution:
    def bagOfTokensScore(self, tokens: List[int], power: int) -> int:
        """Greedy

        - eat the smallest tokens we can while we can power
            + this gives us points
        - if no available tokens to use, then try trading
        our score for the largest available token
        - track the max score at each step

        Time Complexity
            O(n*log(n))
        Space Complexity
            O(n)
        """

        tokens = sorted(tokens)

        cur_score: int = 0
        max_score: int = 0

        low: int = 0
        high: int = len(tokens) - 1

        while low <= high:

            # We have power, let's eat the smallest tokens
            # to gain score
            if power >= tokens[low]:

                power -= tokens[low]
                low += 1
                cur_score += 1

                max_score = max(max_score, cur_score)

            # We've got points but not enough power for a token
            # let's trade for the largest token available
            elif cur_score > 0:
                power += tokens[high]
                cur_score -= 1
                high -= 1

            else:
                break

        return max_score
