"""
Inputs
    points (List[List[int]]): start and ending x location of each balloon

Outputs
    int: min number of arrows needed to pop all balloons    

Notes
    - each ballon takes up some horizontal distance

        x_start to x_stop (inclusive, inclusive)

    - we want to pop all ballons efficiently

Examples

    Example 1:

        Input: points = [[10,16],[2,8],[1,6],[7,12]]
        Output: 2
        Explanation: One way is to shoot one arrow for example at x = 6 (bursting the balloons [2,8] and [1,6]) and another arrow at x = 11 (bursting the other two balloons).

    Example 2:

        Input: points = [[1,2],[3,4],[5,6],[7,8]]
        Output: 4

    Example 3:

        Input: points = [[1,2],[2,3],[3,4],[4,5]]
        Output: 2

Ideas

    - if two ballons overlap, we only maintain the smaller portion of the overlap

        1 2 3 4 5
        x x x x
            x x x

        overlap is 3 to 4

    - bound the problem
        + in the worst case we use n arrows
        + in the best case we use 1 arrow

        + how to check that n arrows work?
        still requires determining overlap and optimal order

    - count the number of ballons at each location ie: 2 ballons at x = 2
        + requires additional computation to determine where to pop ..

    - sort and merge adjacent intervals
        + treat like time intervals and only maintain overlapping regions

"""

class Solution:
    def findMinArrowShots(self, points: List[List[int]]) -> int:
        """Greedy solution

        - sort intervals 
        - merge adjacent intervals if overlap exists
            + if overlap then update bounds to limiting positions
            + if no overlap, start a new interval

        Time Complexity
            O(n*log(n))
        Space Complexity
            O(n)
        """

        if not points:
            return 0

        count: int = 1

        # Sort the intervals by lowest start time but largest range
        points = sorted(points, key = lambda l: (l[0], -l[1]))

        last_point = points[0]

        # print(points)

        for point in points[1:]:

            # print(point)

            # Does overlap exist?
            # if so, update the overlap range
            if point[0] <= last_point[1] :
                last_point[1] = min(last_point[1], point[1])

            else:
                count += 1
                last_point = point

        return count
