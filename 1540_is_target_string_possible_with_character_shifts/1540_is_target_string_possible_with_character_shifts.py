"""
Inputs
    s (str): starting string
    t (str): target string
Outputs
    bool: is a conversion possible
Notes
    - can we convert s to t in no more than k moves
    - on the ith move
        + we can shift any character or do nothing
        + the character cannot be chosen before
        + we can apply the shift at that index i times

    - shifting a character means replacing it by the next character
        + a -> b -> c ..
        + wraparound occurs so z -> a 
        + abcdefghijklmnopqrstuvwxyz

Example 1

    s = "input", t = "ouput", k = 9
    Output: true
    Explanation: In the 6th move, we shift 'i' 6 times to get 'o'. And in the 7th move we shift 'n' to get 'u'.

    i => 0 jklmno  6 moves
    n => u opqrstu 7 moves
    p => p         0 moves
    u => u         0 moves
    t => t         0 moves

Example 2 

    Input: s = "abc", t = "bcd", k = 10
    Output: false
    Explanation: 
    We need to shift each character in s one time to convert it into t. 
    We can shift 'a' to 'b' during the 1st move. 
    However, there is no way to shift the other characters in the remaining moves to obtain t from s.

    a => b         1 move
    b => c         1 move
    c => d         1 move

    left over moves 10 - 3 = 7

Example 3

    Input: s = "aab", t = "bbb", k = 27

    a => b         1 move
    a => b         1 move or 27 moves
    b => b         0 moves

    left_overs = 27 - 2 = 25 moves left

    we can just swap the letters back and forth 25 times
    a => b => a

Ideas
    - try 

"""


class Solution:

    def canConvertString(self, s: str, t: str, k: int) -> bool:
        """

        - track the number of moves for each character
        - if that move is taken already, then we must wraparound
        the alphabet
        - check if this move count will work under our limitations

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        Notes
            - works but is slow, on larger strings
                + slow down on the while loop

        References:
            https://www.w3schools.com/python/python_sets.asp
        """

        n_src: int = len(s)
        n_tgt: int = len(t)

        if n_src != n_tgt:
            return False

        # Determine moves necessary at each index
        # if moves i is taken, then we gotta go all the way around
        # the alphabet, i + 26
        moves = set([])

        for ix in range(n_src):

            moves_needed = (ord(t[ix]) - ord(s[ix])) % 26

            # Ignore characters that match
            if moves_needed == 0:
                continue

            # Add the wraparound if the move is
            # taken already
            while moves_needed in moves:
                moves_needed += 26

            # Add the move if valid
            if moves_needed > k:
                return False
            else:
                moves.add(moves_needed)

        return True

    def convert_string2(self, s: str, t: str, k: int) -> bool:
        """Same principle with shift counting

        - track the number of characters that already use
        the same shift
            + if taken then we must wrap around by 26

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        n_src: int = len(s)
        n_tgt: int = len(t)

        if n_src != n_tgt:
            return False

        # Determine moves necessary at each index
        # save the number of chars requiring that shift
        moves = [0 for _ in range(26)]

        for ix in range(n_src):

            moves_needed = (ord(t[ix]) - ord(s[ix])) % 26

            # Ignore characters that match
            if moves_needed == 0:
                continue

            # See if the moves is in a valid range
            # 1st shift = 3
            # 2nd shift = 3 + 26
            # 3rd shift = 3 + 26 + 26
            if moves_needed + 26 * moves[moves_needed] > k:
                return False

            # Track how many characters need this exact shift
            moves[moves_needed] += 1

        return True


if __name__ == '__main__':

    # Example 1
    s = "input"
    t = "ouput"
    k = 9

    # Example 2
    # s = "abc"
    # t = "bcd"
    # k = 10

    # Example 3
    # s = "aab"
    # t = "bbb"
    # k = 27

    # Wrap necessary
    # s = "iqssxdlb"
    # t = "dyuqrwyr"
    # k = 40

    # Edge case
    # s = "mygdwuntwkoc"
    # t = "btydmdiatnhx"
    # k = 48

    obj = Solution()
    print(obj.canConvertString(s, t, k))
