"""
Inputs
    S (str): any characters
Outputs
    str: any possible result, if not have an empty string
Goals
    - can the string be rearranged such that no adjacent characters
    are the same

Ideas

    - grab the character counts
    - try to build out a new character
        + remove the largest counts first by padding
        them
        + then concentrate on even distribution of characters

        ie: abbbcc

            bcbcba
"""

from collections import defaultdict
import heapq


class Solution:

    def original(self, S: str) -> str:
        """

        - grab all character counts
        - build a new string
            + use the most frequent letters first
            + alternate chars

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        Notes
            - use a heap to improve times
        """

        ''' Get character counts '''

        dct = defaultdict(int)

        for char in S:
            dct[char] += 1

        # Sort keys by the largest counts
        kv = sorted(dct.items(), key=lambda t: t[1], reverse=True)

        last_char = None
        found = True
        s_new = ''

        # Grab the first character that's not the last character
        while found:

            found = False

            for char, cnt in kv:

                if char != last_char and char in dct:
                    dct[char] -= 1

                    if dct[char] == 0:
                        del dct[char]

                    s_new += char
                    last_char = char
                    found = True
                    break

        if not dct:
            return s_new
        else:
            return ''

    def heap_based(self, S: str) -> str:
        """

        - grab all character counts
        - build a heap using most frequent letters
        - grab characters two at a time and build the string

        Time Complexity
            O(n * ln(n))
        Space Complexity
            O(n)
        Notes
            - use a heap to improve times
        """

        ''' Get character counts '''

        dct = defaultdict(int)

        for char in S:
            dct[char] += 1

        # Sort keys by the largest counts
        heap = []
        for k, v in dct.items():
            heapq.heappush(heap, (-v, k))

        last_char = None
        s_new = ''

        # Grab the first character that's not the last character
        while len(heap) >= 2:

            cnt1, ch1 = heapq.heappop(heap)
            cnt2, ch2 = heapq.heappop(heap)

            # Place most frequent, then second most frequent
            if ch1 != last_char:

                s_new += ch1
                s_new += ch2

                last_char = ch2

            # Place 2nd most frequent, then first most frequent
            else:

                s_new += ch2
                s_new += ch1

                last_char = ch1

            # Increment counts for max heap
            if abs(cnt1) > 1:
                heapq.heappush(heap, (cnt1 + 1, ch1))
            if abs(cnt2) > 1:
                heapq.heappush(heap, (cnt2 + 1, ch2))

        # All characters used
        if not heap:
            return s_new

        # One remaining character with 1 count left
        elif len(heap) == 1 and abs(heap[0][0]) == 1 and heap[0][1] != last_char:
            return s_new + heap[0][1]

        else:
            return ''


if __name__ == '__main__':

    # Example 1
    # S = "aaabbc"
    # S = "aab"
    S = "aaab"

    obj = Solution()
    # print(obj.original(S))
    print(obj.heap_based(S))
