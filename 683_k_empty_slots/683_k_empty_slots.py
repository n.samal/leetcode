"""
Inputs

    
Outputs


Goal
    
    - You have N bulbs in a row numbered from 1 to N. 
    - Initially, all the bulbs are turned off. 
    - We turn on exactly one bulb everyday until all bulbs are on after N days.

    - You are given an array bulbs of length N where bulbs[i] = x means that on the (i+1)th day, we will turn on the bulb at position x where i is 0-indexed and x is 1-indexed.

    - Given an integer K, find out the minimum day number such that there exists two turned on bulbs that have exactly K bulbs between them that are all turned off.

    - If there isn't such day, return -1.

Examples

    Example 1
        Input:
            bulbs: [1,3,2]
            K: 1
        Output:
            2
        Explanation:
            On the first day: bulbs[0] = 1, first bulb is turned on: [1,0,0]
            On the second day: bulbs[1] = 3, third bulb is turned on: [1,0,1]
            On the third day: bulbs[2] = 2, second bulb is turned on: [1,1,1]
            We return 2 because on the second day, there were two on bulbs with one off bulb between them.

    Example 2
        Input:
            bulbs: [1,2,3]
            K: 1
        Output:
            -1

Strategy

    Manual
        - this seems similar to optimal seat positions between taken seats
        - keep track of indices where bulbs are turned on

            maintain this in a sorted order

            ie: 
                ix: []
                ix: [1]
                ix: [1, 3]
                ix: [1, 2, 3]

        - for each bulb, compute the distance between the bulb and the bound to the left
            ie:
                ix: []
                ix: 1 - left, right - 1
                ix: 1 - left, 3 - 1, right - 3
                ix: 1 - left, 2 - 1, 3 - 2, right - 3

        - at each step check to see if the desired amount of space is found between bulbs

            dist = bulb_i - left_bulb

            if dist_i == K
                return day_ix

    Notes
        - what's the best way to maintain a sorted list with new incoming values

            + heap sort, merge sort?

    Convert to similar problem

        We get a queue of when each bulb is turned on
            bulbs: [6,5,8,9,7,1,10,2,3,4]

        if we start writing this out we get something like the following
            index: [1,2,3,4,5,6,7,8,9,10]
            day 1: [0,0,0,0,0,1,0,0,0, 0]
            day 2: [0,0,0,0,1,1,0,0,0, 0]
            day 3: [0,0,0,0,1,1,0,1,0, 0]
            day 4: [0,0,0,0,1,1,0,1,1, 0]
            day 5: [0,0,0,0,1,1,1,1,1, 0]
            day 6: [1,0,0,0,1,1,1,1,1, 0]
            day 7: [0,0,0,0,1,1,0,0,0, 0]
            day 8: [0,0,0,0,1,1,0,0,0, 0]
            day 9: [0,0,0,0,1,1,0,0,0, 0]
            day10: [0,0,0,0,1,1,0,0,0, 0]

        if convert this to bloom / toggle dates

            bulb index: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            bulbs     : [6, 5, 8, 9, 7, 1, 10, 2, 3, 4]
            days      : [6, 8, 9, 10, 2, 1, 5, 3, 4, 7]

    Sliding Window

        - Create the list which represents the days that each bulb gets turned on

        days : [6, 8, 9, 10, 2, 1, 5, 3, 4, 7]

        - create a sliding window at position i, with length k + 2

            + we need k empty values, a left and a right bulb on

        index: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        days : [|6, 8, 9, 10|, 2, 1, 5, 3, 4, 7]

        - index 0: starting here is invalid
            + index 3 is not on until day 6, the days before
            this is turned on block our route

        days : [6| 8, 9, 10, 2| 1, 5, 3, 4, 7]
        - index 1: starting here is valid

            on day 8 left is on
            right side is on since day 2
            middle two values are off, since they're larger than both 2 and 8

        - At each position, we need several checks
            + middle values are larger than the left and right
            + a quick check, is find out which of the left & right is bigger
            check that the middle value is larger than that max

        Time Complexity
            O(n*k)

            left indices searched (n-k) times
            inbetweens k searched each time
        Space Complexity
            O(n)

        Notes
            - if we know the max values for each sliding window ahead of time
            we can reference that instead of crawling the inbetween values

    References:
        https://leetcode.com/problems/k-empty-slots/discuss/338640/Python-sliding-window-O(n)-with-explanation
        https://leetcode.com/problems/k-empty-slots/discuss/107948/Iterate-over-time-vs.-iterate-over-position
"""

from typing import List


class Solution:

    def kEmptySlots(self, bulbs: List[int], K: int) -> int:
        """Sliding window approach

        Args:
            bulbs: order in fix bulbs are turned on
            K: desired empty bulbs between turned on bulbs
        Time Complexity
            O(n*k)

            left indices searched (n-k) times
            inbetweens k searched each time
        Space Complexity
            O(n)
        """

        n: int = len(bulbs)
        ans: int = float('inf')

        # Days that bulbs are turned on
        days = [None for _ in range(n)]

        # Order of bulbs toggles
        for ix, toggle_i in enumerate(bulbs):

            # Track the toggle day for each bulb
            days[toggle_i - 1] = ix + 1

        print(f'bulbs: {bulbs}')
        print(f'days : {days}')

        # Create a sliding window with k values in between bounds
        for left_ix in range(n - K - 1):

            # Define the end of our sliding window
            # ie: [0, 1, 2, 3, 4, ]
            right_ix = left_ix + K + 1

            # Find the max of our bounds
            # ie: [8]
            max_bound = max(days[left_ix], days[right_ix])

            works: bool = True

            # We must make sure the values inbetween are open
            # ie: they have not bloomed/turned off
            for jx in range(left_ix + 1, right_ix):
                if days[jx] <= max_bound:
                    works = False
                    break

            # We have a valid solution, save it
            if works:
                ans = min(ans, max_bound)

        # Return our answer if we found something
        if ans == float('inf'):
            return -1
        else:
            return ans

    def kEmptySlots2(self, bulbs: List[int], K: int) -> int:
        """Sliding window approach with bound bump

            - if we encounter a smaller value in our window
            just restart our window at that position

        Args:
            bulbs: order in fix bulbs are turned on
            K: desired empty bulbs between turned on bulbs
        Time Complexity
            O(n*k)

            left indices searched (n-k) times
            inbetweens k searched each time
        Space Complexity
            O(n)
        """

        n: int = len(bulbs)
        ans: int = float('inf')

        # Days that bulbs are turned on
        days = [None for _ in range(n)]

        # Order of bulbs toggles
        for ix, toggle_i in enumerate(bulbs):

            # Track the toggle day for each bulb
            days[toggle_i - 1] = ix + 1

        print(f'bulbs: {bulbs}')
        print(f'days : {days}')

        # Create a sliding window with k values in between bounds
        left_ix: int = 0
        right_ix: int = left_ix + k + 1

        while right_ix < n:

            # Find the max of our window, assume it's good
            # ie: [8]
            max_bound = max(days[left_ix], days[right_ix])
            works: bool = True

            # We must make sure the values inbetween are open
            # ie: they have not bloomed/turned off
            for jx in range(left_ix + 1, right_ix):
                if days[jx] <= max_bound:
                    works = False
                    break

            # We have a valid solution, save it
            if works:
                ans = min(ans, max_bound)
                left_ix += 1
            else:
                left_ix = jx

            # Update our right index
            right_ix = left_ix + k + 1

        # Return our answer if we found something
        if ans == float('inf'):
            return -1
        else:
            return ans


if __name__ == '__main__':

    bulbs_in = [6, 5, 8, 9, 7, 1, 10, 2, 3, 4]
    k = 2

    obj = Solution()
    # print(obj.kEmptySlots(bulbs_in, k))
    print(obj.kEmptySlots2(bulbs_in, k))
