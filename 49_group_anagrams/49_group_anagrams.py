from typing import List


class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        """Group anagrams together

        Compile a hash of character counts for each string

        Args:
            strs (List[str]): lower case strings
        Returns:
            List[List[str]]: groupings of similar anagrams
        """

        # Initialize empty hash
        db = {}

        # Iterate through all strings
        for st in strs:

            count = {}

            # Iterate through characters
            for char in st:

                if char not in count:
                    count[char] = 1
                else:
                    count[char] += 1

            # Generate tuple of dictionary in sorted order
            key = [(k, v) for k, v in sorted(count.items())]
            key = tuple(key)

            # Check if this count exists
            if key in db:

                # Add the current string to the list
                db[key].append(st)

            # Start a new list
            else:
                db[key] = [st]

        # Initialize empty array
        arr = [None] * len(db.keys())

        # Convert database into list
        for ix, key in enumerate(db.keys()):
            arr[ix] = db[key]
        return arr

    def groupAnagrams_list(self, strs: List[str]) -> List[List[str]]:
        """Group together similar anagrams

        - Keep character counts using an a sparse array
            key = [0, 0, 0, 1, 0, ...]
        - Store each key in a dictionary
        - Retrive the dictionary view as the result

        Args:
            strs (List[str]): lower case strings
        Returns:
            List[List[str]]: groupings of similar anagrams
        """

        # Initialize empty hash
        db = {}

        # Iterate through all strings
        for st in strs:

            # Counter of each character in alphabet
            count = [0] * 26

            # Iterate through characters
            for char in st:

                # Increment character count
                count[ord(char) - ord('a')] += 1

            # Generate tuple of dictionary in sorted order
            key = tuple(count)

            # Check if this count exists
            if key in db:

                # Add the current string to the list
                db[key].append(st)

            # Start a new list
            else:
                db[key] = [st]

        return db.values()

    def groupAnagrams_sorted(self, strs: List[str]) -> List[List[str]]:
        """Group together similar anagrams

        - Use sorted characters as keys
            key = ('e', 'a', 't')
        - Store each key in a dictionary
        - Retrive the dictionary view as the result

        Args:
            strs (List[str]): lower case strings
        Returns:
            List[List[str]]: groupings of similar anagrams
        """

        # Initialize empty hash
        db = {}

        # Iterate through all strings
        for st in strs:

            # Sort keys
            key = tuple(sorted(st))

            # Check if this count exists
            if key in db:

                # Add the current string to the list
                db[key].append(st)

            # Start a new list
            else:
                db[key] = [st]

        return db.values()


if __name__ == "__main__":

    s = Solution()

    # print(s.groupAnagrams(["eat", "tea", "tan", "ate", "nat", "bat"]))
    print(s.groupAnagrams_list(["eat", "tea", "tan", "ate", "nat", "bat"]))
