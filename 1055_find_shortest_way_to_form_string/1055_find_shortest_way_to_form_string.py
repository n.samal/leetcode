"""
Inputs
    source (str):
    target (str):
Outputs
    int: minimum number of subsequences
Goals
    - can the target string be created by concatenating together
    subsequences of the source string
    - a subsequence can be created my excluding some of none of
    the characters
    - what is the minimum number of concatenations possible
    - if the subsequence cannot be created output -1

Examples

    Example 1
        Input: source = "abc", target = "abcbc"
        Output: 2
        Explanation: The target "abcbc" can be formed by "abc" and "bc", which are subsequences of source "abc".

    Example 2
        Input: source = "abc", target = "acdbc"
        Output: -1
        Explanation: The target string cannot be constructed from the subsequences of source string due to the character "d" in target string.

    Example 3
        Input: source = "xyz", target = "xzyxz"
        Output: 3
        Explanation: The target string can be constructed as follows "xz" + "y" + "xz".

Ideas

    - create a counter of characters
        + track the index of each character

    - we cannot add new characters
        + if the target has other characters then output -1

        ie: source = "abc", target = "acdbc"

        no 'd' available

    - treat each character in the string as a node
        + edges are only 1 directional towards other characters past
        our current position

    - greedy approach seems possible
        + search for the character using the current index position
        + use the list of indices to see what's viable
        + use binary search to speed up the search

    - array of next character positions
        + generate an array of indices for each character

        ie: a = [1,1,3,3,3, 0,0,0]

        at position 0, we known the next 'a' exists at 0
        at position 1, we know the next position is at 1 etc..
        if no viable position, leave as 0

References
    - https://leetcode.com/problems/shortest-way-to-form-string/discuss/330938/Accept-is-not-enough-to-get-a-hire.-Interviewee-4-follow-up

"""

from collections import defaultdict
import bisect


class Solution:

    def shortestWay(self, source: str, target: str) -> int:
        """Indice Hash/List and binary search

        - store the indices for each character in the source
        - greedily try to build the target string
            + track the current pointer in source
            + start from scratch if no viable position found

        Time Complexity
            O(t*ln(s))

            for t characters in the target
            compute binary search on the source indice list

        Space Complexity
            O(n)
            storing indices for the entire source
        """

        ''' Gather character indices '''

        source_d = defaultdict(list)

        for ix, char in enumerate(source):
            source_d[char].append(ix)

        ''' Try generating string'''

        counts: int = 1  # count of strings
        ix: int = -1  # current index

        for char in target:

            print(f'target_char: {char}  ix: {ix}')

            # Character not available for use
            if char not in source_d:
                return -1

            else:

                # Find the first point the char is available after the current index
                jx = bisect.bisect(source_d[char], ix)

                # No viable character found before index
                # start a new string at the first possible character
                if jx == len(source_d[char]):
                    ix = source_d[char][0]
                    counts += 1

                # Found a character after current position
                else:
                    jx = source_d[char][jx]
                    ix = jx

        return counts

    def two_pointers(self, source: str, target: str) -> int:
        """Use by hand logic

        - from our current position in s search for the target
        character
            + if we find it, move to the next target char
            + if we can't find it try starting from the beginning of
            the source string

        - on our second search of the source
            + if we find it, then increase the string count
            + if we can't find it, then the character doesn't exist in our
            source

        Time Complexity
            O(t*s)

            t: for every target character
            s: we may need to go through the full source

        Space Complexity
            O(1)
        """
        s_len: int = len(source)
        t_len: int = len(target)

        s: int = 0  # pointer in source string
        t: int = 0  # pointer in target string
        count: int = 1  # number of sequences

        print(f'source: {source[s]}   target: {target[t]}')

        while t < t_len:

            # Check for target character from current 's' position
            while s < s_len and source[s] != target[t]:
                print(f'source: {source[s]}   target: {target[t]}')
                s += 1

            # If no match found
            # check for target character from the start of source
            if s == s_len:

                print(' Resetting s')

                s = 0
                while s < s_len and source[s] != target[t]:
                    print(f'  source: {source[s]}   target: {target[t]}')
                    s += 1

                # Couldn't find the character from the beginning
                if s == s_len:
                    return -1
                else:
                    count += 1

            # Match found
            else:
                t += 1
                s += 1

        return count

    def next_index(self, source: str, target: str) -> int:
        """Use an array of locations where te

        - to improve look up time of the next possible index
        use an array
            + previous binary search O(ln(n))
            + new O(1)

        Time Complexity
            O(t)

            O(1) time for each of the t character in the target

        Space Complexity
            O(s*s)

            arrays of source length for each of the s characters in source
        Notes
            - time savings not apparent in submission
        """

        count: int = 1
        s_len: int = len(source)

        ''' Generate next index array '''

        next_char = {}  # locations of where the next character occurs

        # Create a next index array for every character
        for char in source:
            next_char[char] = [-1 for _ in range(s_len)]

        # Fill next index array
        # crawl from the known point back to the last unique value
        for ix, char in enumerate(source):
            jx = ix
            while jx >= 0 and next_char[char][jx] == -1:
                next_char[char][jx] = ix
                jx -= 1

        ''' Try to generate the target string '''
        ix = 0

        for char in target:

            # Character doesn't exist
            if char not in next_char:
                return -1

            # Must start a new substring
            # start pointer at first occurence of character
            if ix == s_len or next_char[char][ix] == -1:
                count += 1
                ix = next_char[char][0] + 1

            # Find the next valid index
            # then bump the pointer
            else:
                ix = next_char[char][ix] + 1

        return count


if __name__ == '__main__':

    # Example 1
    # source = "abc"
    # target = "abcbc"

    # Example 3
    # source = "xyz"
    # target = "xzyxz"

    # Test case
    # source = "aaaaa"
    # target = "aaaaaaaaaaaaa"

    # Test case
    source = "adbsc"
    target = "addddddddddddsbc"

    obj = Solution()
    # print(obj.shortestWay(source, target))
    # print(obj.two_pointers(source, target))
    print(obj.next_index(source, target))
