"""
Inputs
    A (List[int]): ordered list of disjoint meetings
    B (List[int]): ordered list of disjoint meetings   

Outputs
    List[List[int]]: 

Notes
    - times are closed intervals (inclusive, inclusive)

        + [a, b] means a <= b

    - The intersection of two closed intervals is a set of real numbers that is either empty,
     or can be represented as a closed interval.  

     For example, the intersection of [1, 3] and [2, 4] is [2, 3].)

Examples

    Example 1

        Input: A = [[0,2],[5,10],[13,23],[24,25]], 
               B = [[1,5],[8,12],[15,24],[25,26]]
        Output: [[1,2],[5,5],[8,10],[15,23],[24,24],[25,25]]

Notes

    - leverage the sorted arrays

    - apply a two pointers like approach
        + search for an overlap between adjacent meetings
        + bump the earlier meeting

"""

class Solution:
    def intervalIntersection(self, A: List[List[int]], B: List[List[int]]) -> List[List[int]]:
        """Find the meeting overlap times for each meeting

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        ans = []

        # Find the overlapping regions
        # overlap = min of (start, stop)
        # overlap = max of (stop, start)

        # Combine the lists together but identify A vs B
        # or iterate between the two

        n_A: int = len(A)
        n_B: int = len(B)

        a: int = 0
        b: int = 0

        while a < n_A and b < n_B:

            low = max(A[a][0], B[b][0])
            high = min(A[a][1], B[b][1])

            if low <= high:
                ans.append([low, high])

            # Bump the one that ends sooner
            if A[a][1] <= B[b][1]:
                a += 1
            else:
                b += 1

            # print(ans)

        return ans