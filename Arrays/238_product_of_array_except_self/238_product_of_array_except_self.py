"""
Inputs
    arr (List[int]): values in unknown order
Outputs
    List[int]: array of values where product
Goal:
    - at each position find the product of all values except it self
    - try not to use division

Example

    Example 1
        Input:  [1,2,3,4]
        Output: [24,12,8,6]
    Example 2    

Strategies
    Total Product
        - compute the total product
        - at each location divide the total product by the current number

        Time Complexity
            O(n)
        Space Complexity
            O(n)

    Cummulative Product
        - compute the cummulative product from left to right
        - compute the cummulative product from right to left

        - at each position, multiply the product of all values to right
        by the product of all values to the left

        Time Complexity
            O(n)
        Space Complexity
            O(n)

"""

from typing import List


class Solution:

    def total_product(self, arr: List[int]) -> List[int]:
        """Total product

        Notes:
            may fail when value is 0.0
        """
        # Compute total product
        total_prod: int = 1

        for val in arr:
            total_prod *= val

        # Divide each location by total product
        ans = [total_prod // val for val in arr]

        return ans

    def cummulative_product(self, arr: List[int]) -> List[int]:
        """Cummulative product

        Notes
            - can consolidate loops forward loops
        """

        n: int = len(arr)
        prod_lr = [1 for _ in range(n)]
        prod_rl = [1 for _ in range(n)]
        ans = [0 for _ in range(n)]

        # Products left to right
        prod_lr[0] = arr[0]

        for ix in range(1, n):
            prod_lr[ix] = prod_lr[ix - 1] * arr[ix]

        # Products right to left
        prod_rl[-1] = arr[-1]

        for ix in range(n - 2, -1, -1):
            prod_rl[ix] = prod_rl[ix + 1] * arr[ix]

        # Divide each location by totl aproduct
        for ix in range(n):
            ans[ix] = 1

            # Grab product to the left if exists
            if ix >= 1:
                ans[ix] *= prod_lr[ix - 1]

            if ix < (n - 1):
                ans[ix] *= prod_rl[ix + 1]

        return ans

    def cummulative_product2(self, arr: List[int]) -> List[int]:
        """Cummulative product

        - consolidating forward loops

        Notes
            - can consolidate loops forward loops
        """

        n: int = len(arr)
        prod_lr = [1 for _ in range(n)]
        prod_rl = [1 for _ in range(n)]
        ans = [0 for _ in range(n)]

        # Products right to left
        prod_rl[-1] = arr[-1]

        for ix in range(n - 2, -1, -1):
            prod_rl[ix] = prod_rl[ix + 1] * arr[ix]

        # Products left to right
        prod_lr[0] = arr[0]

        # Left to right
        for ix in range(n):

            ans[ix] = 1

            # Grab product to the left if exists
            if ix >= 1:
                prod_lr[ix] = prod_lr[ix - 1] * arr[ix]
                ans[ix] *= prod_lr[ix - 1]

            if ix < (n - 1):
                ans[ix] *= prod_rl[ix + 1]

        return ans

    def cummulative_product3(self, arr: List[int]) -> List[int]:
        """Cummulative product

        - consolidating forward loops
        - only track current value for prod_lr

        Notes
            - can consolidate loops forward loops
        """

        n: int = len(arr)
        prod_rl = [1 for _ in range(n)]
        ans = [0 for _ in range(n)]

        # Products right to left
        prod_rl[-1] = arr[-1]

        for ix in range(n - 2, -1, -1):
            prod_rl[ix] = prod_rl[ix + 1] * arr[ix]

        # Products left to right
        prod_lr = arr[0]

        # Left to right
        for ix in range(n):

            ans[ix] = 1

            # Grab product to the left if exists
            if ix >= 1:
                ans[ix] *= prod_lr

                # Update for next time
                prod_lr = prod_lr * arr[ix]

            if ix < (n - 1):
                ans[ix] *= prod_rl[ix + 1]

        return ans

    def cummulative_product4(self, arr: List[int]) -> List[int]:
        """Cummulative product

        - consolidating forward loops
        - only track current value for prod_lr
        - remove prod_rl array

        Notes
            - can consolidate loops forward loops
        """

        n: int = len(arr)
        ans = [1 for _ in range(n)]

        # Products left to right
        prod_lr = arr[0]

        for ix in range(1, n):

            ans[ix] *= prod_lr

            # Update for next time
            prod_lr = prod_lr * arr[ix]

        # From right to left
        prod_rl = arr[-1]

        for ix in range(n-2, -1, -1):

            ans[ix] *= prod_rl

            # Update for next time
            prod_rl = prod_rl * arr[ix]

        return ans


if __name__ == "__main__":

    # Input
    arr = [1, 2, 3, 4]

    obj = Solution()
    print(obj.cummulative_product(arr))
    print(obj.cummulative_product2(arr))
    print(obj.cummulative_product4(arr))
