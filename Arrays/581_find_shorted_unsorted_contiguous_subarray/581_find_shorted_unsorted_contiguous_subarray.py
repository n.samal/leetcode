"""
Inputs
    arr (List[int]): values in unknown order
Outputs
    int: length of shortest array 

Notes
    - find one continuous subarray if sorted would result
    in the whole array being sorted

Examples

    Input: [2, 6, 4, 8, 10, 9, 15]
    Output: 5
    Explanation: You need to sort [6, 4, 8, 10, 9] in ascending order to make the whole array sorted in ascending order.

Ideas
    - find where the unsorted values occur


"""

from typing import List


class Solution(object):

    def sort_and_index(self, nums):
        """Sort the array then find indices

        - sort the array
        - find the location of the first unsorted position
        - from the back find the first unsorted position
        - the length can be computed with those indices

        Time Complexity
            O(n*log(n))
        Space Complexity
            O(n)
        """

        arr = sorted(nums)
        n = len(nums)

        # Find where the first location where sorting is necessary
        for ix_start in range(n):
            if nums[ix_start] != arr[ix_start]:
                break

        # Input is already sorted
        if ix_start == n - 1:
            return 0

        # Find the last location where sorted is necessary
        for ix_stop in range(n - 1, -1, -1):
            if nums[ix_stop] != arr[ix_stop]:
                break

        return (ix_stop + 1) - ix_start

    def stack(self, arr: List[int]):
        """Maintain monotonic stacks to determine unsorted indices

        - iterate across the array to determine the first unsorted index
            + if we see a negative trend, then it's not sorted
            + then workbackwards to see where to place this number
        - iterate across the array backwards to find the last unsorted index
        - compute the length

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        n = len(arr)
        ix_start = float('inf')
        ix_stop = float('-inf')

        stack = [0]

        # Find the first unsorted index
        # maintain a monotonically increasing stack
        for ix in range(1, n):

            # Find the insert position of the unsorted value
            while stack and arr[ix] < arr[stack[-1]]:
                ix_start = min(ix_start, stack.pop())

            stack.append(ix)

        # Input is already sorted
        if len(stack) == n:
            return 0

        # Find the last unsorted index
        # maintain a monotonically decreasing stack
        stack = [n - 1]

        for ix in range(n - 2, -1, -1):

            # Find the insert position of the unsorted value
            while stack and arr[ix] > arr[stack[-1]]:
                ix_stop = max(ix_stop, stack.pop())

            stack.append(ix)

        return (ix_stop + 1) - ix_start


if __name__ == '__main__':

    # Example 1
    # arr = [2, 6, 4, 8, 10, 9, 15]
    arr = [1, 3, 5, 4, 2]

    obj = Solution()
    print(obj.stack(arr))
