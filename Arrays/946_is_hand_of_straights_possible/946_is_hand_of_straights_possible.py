"""
Inputs:
    hand (List[int]): integers representing card values, in unknown order
    W (int): number of groups
Outputs:
    bool: can cards be organized into W consecutive groups
Goals
    - arrange the cards into W groups of consecutive numbers
    - determine whether or not this can be done

Ideas
    - if the number of cards can't be split equally into
    W groups, then return False
        + use divmod

    - we want to track how many cards we have of each value
        + use a counter

    - sorting the cards should help with creating groups

        + we want to grab the smallest card, then grab the consecutive values
        + if the next value does exist, grab it, then reduce the count
        + if the next value does not exist, then return False

    - finding the next smallest value

        + this is O(n) if iterating of hash keys
        + perhaps we can use a sorted deque of unique values

            * only remove values after their count goes to zero
            * grab the next smallest value with popleft()
            * ==> This might not work if the middle value is getting removed
                [1, 2, 3, 4] and we have to remove 2

        + we could use a heap to easily grab the next smallest value

            + check if the value is valid by referencing the counts
"""

from typing import List
import heapq


class Solution:

    """ Minimum: iteration """

    def isNStraightHand(self, hand: List[int], W: int) -> bool:
        """
        Args:
            hand (List[int]):
            W (int): size of each groups

        Time Complexity
            O(W * n_groups)

            for each group we go through W cards to find the minimum

        Space Complexity:
            O(n)  size of counter

        """
        n: int = len(hand)
        n_groups, mod = divmod(n, W)

        # Unable to split the groups equally
        if mod != 0:
            return False

        # Get the card counts
        counts = {}

        for val in hand:
            counts[val] = counts.get(val, 0) + 1

        # Start creating groups
        for ix in range(n_groups):

            # Grab the smallest value to start each group
            val = min(counts.keys())

            for jx in range(W):

                # Find the next consecutive card
                # ie: 3, 4, 5
                val_new = val + jx

                # Remove the card if it exists
                if val_new not in counts:
                    return False
                else:
                    self.reduce_count(counts, val_new)

        return True

    def reduce_count(self, counts: dict, val: int):
        """Remove one card from our card count

        Args:
            counts (dict): counts of each value
            val (int): value of current card to remove
        """

        # Reduce the count
        counts[val] -= 1

        # Remove the key if no more cards exist
        if counts[val] <= 0:
            del counts[val]

    """ Minimum via heap """

    def is_straight_heap(self, hand: List[int], W: int) -> bool:
        """Use a heap to grab the smallest value

        - use a heap of all values to find the minimum in log(n) time

        Args:
            hand (List[int]):
            W (int): size of each groups

        Time Complexity
            O(n*log(n))

            removing n values from our heap of size n

        Space Complexity:
            O(n)  size of counter

        Notes:
            much faster
            - could potentially just create a heap of only uniques
        """
        n: int = len(hand)
        n_groups, mod = divmod(n, W)

        # Unable to split the groups equally
        if mod != 0:
            return False

        # Track card counts
        counts = {}
        for val in hand:
            counts[val] = counts.get(val, 0) + 1

        # Create heap with card values
        heap = hand[:]
        heapq.heapify(heap)

        # Start creating groups
        for ix in range(n_groups):

            # Grab the smallest value available
            start = heap[0]
            while counts[start] <= 0:
                start = heapq.heappop(heap)

            # Create the current group
            for jx in range(W):

                # Find the next consecutive card
                # ie: 3, 4, 5
                val_new = start + jx

                # Check the count and reduce it
                count = counts.get(val_new, 0)

                if count <= 0:
                    return False
                else:
                    counts[val_new] -= 1

        return True


if __name__ == '__main__':

    cards = [1, 2, 3, 4, 5, 6]
    groups = 2

    # cards = [5, 1]
    # groups = 1

    obj = Solution()
    # print(obj.isNStraightHand(cards, groups))
    print(obj.is_straight_heap(cards, groups))
