"""
Inputs
    poured (int): amount of liquid poured
    query_row (int): row of the cup
    query_glass (int): column of the cup

Outputs
    int: liquid amount at the given row

Notes
    - glasses are stacked in a pyramid shape

    - row is 0 indexed
    - cup (column) is also 0 indexed

    - cups can only hold 1 unit of liquid
    - overflow is equally distributed to the left and right cup below

    - overflow from the last row goes to the floor

Ideas

    - generate the champage set as an array

        + similar to pascals triangle

    - pour the given amount into the top cup, and cascade the information down

        + we need to go to 1 past the row of interest
        since we need the query row in the normal state and not the overflow state
        
        + if there's no overflow, then the rows below don't have any liquid

Hand Calc

    row 1:      [0]
    row 2:    [0, 1]
    row 3:   [0, 1, 2]
    row 4:  [0, 1, 2, 3]
    row 5: [0, 1, 2, 3, 4]

    row_i has i elements

    each value pours to itself, and the index to the right on the next row

    row 1: 0 pours to 0, 1
    row 2: 0 pours to 0, 1
           1 pours to 1, 2
    row 3: 0 pours to 0, 1
           1 pours to 1, 2
           2 pours to 2, 3

"""

class Solution:
    def champagneTower(self, poured: int, query_row: int, query_glass: int) -> float:
        """Cascade information to levels below

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        # Row 0 cup 0 (1 element)
        row_prev = [poured]

        for ix in range(1, query_row + 2):

            row_cur = [0 for _ in range(ix + 1)]

            # Cascade the information down from previous row
            for jx in range(ix):

                overflow = row_prev[jx] - 1

                if overflow > 0:
                    row_prev[jx] = 1
                    row_cur[jx] += overflow / 2.0
                    row_cur[jx + 1] += overflow / 2.0

            print(f'row_prev:      {row_prev}')
            print(f'row_cur : {ix} {row_cur}')

            # Our current row becomes the next row
            row_prev_prev = row_prev
            row_prev = row_cur

        # Use the settled state of our query row
        return row_prev_prev[query_glass]
