"""
Inputs:
    grid (List[List[str]]): bomberman grid
Outputs:
    int: maximum score using one bomb
Notes
    - every cell in the grid can have one of 3 states
        + 'W': wall
        + 'E': enemy
        + '0': empty
    - bombs can only be placed in empty cells
    - bombs will kill all enemys in the same row/column
    until it hits a wall

Examples

    Input: [["0","E","0","0"],["E","0","W","E"],["0","E","0","0"]]
    Output: 3 
    Explanation: For the given grid,

    0 E 0 0 
    E 0 W E 
    0 E 0 0

    Placing a bomb at (1,1) kills 3 enemies.

Ideas

    - intuition
        + at every open cell expand up and down
        + only expand while we have cells left to explore
        and we haven't hit a wall

    - count the number of enemys between every boundary

        + try count on each column and each row
        + example

        row      = [0, E, E, W, 0, E, W, 0, 0, E]

        our goal is to count the number between like
        score    = [2, 2, 2, 0, 1, 1, 0, 1, 1, 1]

        we try breaking this into pieces by counting in
        both directions

            * count from left to right
            * count from right to left

        score_lr = [0, 1, 2, 0, 0, 1, 0, 0, 0, 1]
        score_rl = [2, 2, 1, 0, 1, 1, 0, 1, 1, 1]
        score is the max of those two
        score    = [2, 2, 2, 0, 1, 1, 0, 1, 1, 1]

    - how to combine this with column numbers?

        0 E 0 0
        E 0 W E
        0 E 0 0

        - only track scores at 0 cells, and aggregate instead of using max
        - if we modify the same grid, we can aggregate the score easily

        row wise score

        1 0 1 1
        0 1 0 0
        1 0 1 1

        column wise score

        1 0 0 1
        0 2 0 0
        1 0 0 1

        grab row wise numbers first

        1 0 1 1
        0 1 0 0
        1 0 1 1

        then perform column wise score, down first

        1 1 1 1
        1 1 0 1
        2 1 1 1
"""

from typing import List


class Solution:

    def intuition(self, grid: List[List[str]]) -> int:
        """Intuition

        - from each open position crawl in every direction

        Time Complexity
            O(r*c (r + c))
        Space Complexity
            O(1)

        Notes
            - works but has repeated iterations over each row/column
        """

        if not grid:
            return 0

        n_rows: int = len(grid)
        n_cols: int = len(grid[0])

        max_score: int = 0

        for r in range(n_rows):
            for c in range(n_cols):

                if grid[r][c] == '0':

                    count = 0

                    # Crawl up while valid
                    ix = r
                    while ix >= 0 and grid[ix][c] != 'W':
                        if grid[ix][c] == 'E':
                            count += 1
                        ix -= 1

                    # Crawl down while valid
                    ix = r
                    while ix < n_rows and grid[ix][c] != 'W':
                        if grid[ix][c] == 'E':
                            count += 1
                        ix += 1

                    # Crawl east while valid
                    ix = c
                    while ix < n_cols and grid[r][ix] != 'W':
                        if grid[r][ix] == 'E':
                            count += 1
                        ix += 1

                    # Crawl west while valid
                    ix = c
                    while ix >= 0 and grid[r][ix] != 'W':
                        if grid[r][ix] == 'E':
                            count += 1
                        ix -= 1

                    max_score = max(max_score, count)

        return max_score

    ''' Improved '''

    def intuition2(self, grid: List[List[str]]) -> int:
        """Intuition improved

        - preprocess grid to store scores for each position
            + slide up, the slide down to spread enemy scores between
            walls
            + slide left and right

        - then grab the best score from each open position
            + we track this simulatneously to save time

        Time Complexity
            O(r*c)
        Space Complexity
            O(r*c)
        """

        if not grid:
            return 0

        n_rows: int = len(grid)
        n_cols: int = len(grid[0])

        max_score: int = 0
        scores = [[0 for c in range(n_cols)] for r in range(n_rows)]

        ''' Slide scores left and right '''
        for r in range(n_rows):

            # Left to right
            score = 0

            for c in range(n_cols):

                if grid[r][c] == '0':
                    scores[r][c] += score

                elif grid[r][c] == 'E':
                    score += 1

                else:
                    score = 0

            # Right to left
            score = 0
            for c in range(n_cols - 1, -1, -1):

                if grid[r][c] == '0':
                    scores[r][c] += score

                elif grid[r][c] == 'E':
                    score += 1

                else:
                    score = 0

        ''' Slide scores up and down '''
        for c in range(n_cols):

            # Going down
            score = 0

            for r in range(n_rows):

                if grid[r][c] == '0':
                    scores[r][c] += score

                elif grid[r][c] == 'E':
                    score += 1

                else:
                    score = 0

            # Going up
            score = 0
            for r in range(n_rows - 1, -1, -1):

                if grid[r][c] == '0':
                    scores[r][c] += score
                    max_score = max(max_score, scores[r][c])

                elif grid[r][c] == 'E':
                    score += 1

                else:
                    score = 0

        return max_score


if __name__ == '__main__':

    # Example 1
    grid = [["0","E","0","0"],["E","0","W","E"],["0","E","0","0"]]

    obj = Solution()
    # print(obj.intuition(grid))
    print(obj.intuition2(grid))
