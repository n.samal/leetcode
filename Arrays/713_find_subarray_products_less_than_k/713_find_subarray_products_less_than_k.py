"""
Inputs
    arr (List[int]): 
    k (int): product limit
Outputs
    int: number of subarrays with products less than k
Notes
    - find the number of contiguous subarrays with product less than k
    - numbers are all positive integers

Examples
    Input: nums = [10, 5, 2, 6], k = 100
    Output: 8
    Explanation: The 8 subarrays that have product less than 100 are: [10], [5], [2], [6], [10, 5], [5, 2], [2, 6], [5, 2, 6].
    Note that [10, 5, 2] is not included as the product of 100 is not strictly less than k.    

Ideas

    - values are all positive and integers so the products
    should only be increasing

    - brute force ish
        + at current value slide right, and compute product
        + once values are over the limit break the loop

        + O(n^2)

Hand Calc

    indx = [0 , 1, 2, 3]
    nums = [10, 5, 2, 6]

    Sliding from 0 to right
        prod = [10, 50, 100, 600]

    Sliding from 1 to right
        prod = [NA, 5, 10, 60]
        this is the previous products divided by 10

    Sliding from 2 to right
        prod = [NA, NA, 2, 12]
        this is the previous products divided by 5

    Sliding from 3 to right
        prod = [NA, NA, NA, 6]
        this is the previous products divided by 2

Hand Calc: Sliding Window Method

    idx  = [0, 1, 2, 3, 4, 5]
    arr  = [1, 5, 2, 4, 3, 6]
    prod = [1, 5, 10, 40, 120, 720]

    the prod array contains the maximum product
    possible for a subarray ending at an index (0 to i)

    to consider s smaller product, we just need to divide out the
    left most value

    prod (0 to n) = 720
    prod (1 to n) = 720  (remove 1)
    prod (2 to n) = 144  (remove 5)

    - at each position we want to consider all subarrays ending
    here

        + ie: starting at 0, ending at 0

        + ie: starting at 0, ending at 1
        + ie: starting at 1, ending at 1

        + ie: starting at 0, ending at 2
        + ie: starting at 1, ending at 2
        + ie: starting at 1, ending at 2

        + we know if the largest subarray ending at an index
        is valid, then all smaller subarrays are also valid
        since they must be a smaller product

    - once we've hit the target over gone above we need to reduce our product

        + we divide by the values on the left
        + bump the left bound from 0 to 1, and recompute total product

    right = 0
        prod = 1        left = 0

    right = 1
        prod = 5        left = 0

    right = 2
        prod = 10       left = 0

    right = 3
        prod = 40       left = 0

        product is too big
        prod = 40 / 1 = 40

        left = 0 + 1 = 1

        prod = 40 / 5 = 8
        left = 1 + 1 = 2

    etc..
"""

from typing import List


class Solution:

    def brute_force(self, arr: List[int], k: int) -> int:
        """Intuition

        - for every values multiply all to the right
        - continue multipling while under the limit

        Time Complexity
            O(n^2)
        Space Complexity
            O(1)
        """

        count: int = 0
        n: int = len(arr)

        for ix in range(n):

            prod = arr[ix]

            if prod < k:
                count += 1

            for jx in range(ix + 1, n):

                prod *= arr[jx]

                if prod < k:
                    count += 1
                else:
                    break

        return count

    def caterpillar(self, arr: List[int], k: int) -> int:
        """Sliding window via two pointers

        - slide right to consider every subarray
        ending at a position (right)
        - squeeze our subarray down to bring the product
        within our target
            + remove the left most value until we get under the limit

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        count: int = 0
        n: int = len(arr)

        left: int = 0
        prod: int = 1

        for right in range(n):

            prod = prod * arr[right]

            # Remove left value to get within target
            while prod >= k and left < right:
                prod = prod // arr[left]
                left += 1

            # Add all working subarrays ending at right
            if prod < k:
                count += ((right + 1) - left)

        return count


if __name__ == '__main__':

    # Example 1
    # arr = [10, 5, 2, 6]
    # k = 100

    # Mine
    arr = [1, 5, 2, 4, 3, 6]
    k = 30

    # Mine
    arr = [1, 2, 3]
    k = 0

    obj = Solution()
    print(obj.brute_force(arr, k))
    print(obj.caterpillar(arr, k))
