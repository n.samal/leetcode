"""
Inputs
    points (List[int]): point values for each card
    k (int): number of cards to take
Outputs
    int: maximum point value possible
Goals
    - determine the max point value possible by grabbing cards from each side
    - at each step you can take a card from the left or card from the right
    - the score is the sum of card points
Examples

    Example 1
        Input: cardPoints = [1,2,3,4,5,6,1], k = 3
        Output: 12
        Explanation: After the first step, your score will always be 1. However, choosing the rightmost card first will maximize your total score. The optimal strategy is to take the three cards on the right, giving a final score of 1 + 6 + 5 = 12.

    Example 5

        Input: cardPoints = [1,79,80,1,1,1,200,1], k = 3
        Output: 202

Cases

    - take all cards from left
    - take all cards from right
    - take some from each side

        k = 5
            => 0, 5 or 5, 0
            => 1, 4 or 4, 1
            => 2, 3 or 3, 2

Ideas
    - in a general scenario we grab cards from both sides
    - if we take one n cards from one side, then the rest
    must be taken from the opposite

        * cards_left + cards_right = k

    - go through all iterations of k

        + all from left
        + all but 1 from left
        + all but 2 from left
        + ...
        + all from right

    - compute the array summation from the subarrays
        + use a prefix summation to save computation time

    - seems dynamic programming ish but it may not be necessary
    since the exact summations can be computed

Strategies

    Sliding Window

        - compute cummulative summation for efficiency
        - try computing all subwindows

            left: k - 0, right = 0
            left: k - 1, right = 1
            left: k - 2, right = 2

        - bounds will be defined by 

            left: 0 to (k - ix)
            right: (n - ix), to n

"""

class Solution:
    def maxScore(self, cards: List[int], k: int) -> int:
        """Try all combinations of sliding windows

        - use cumsum to save summation time

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        result = 0

        ''' Compute cummulative summation '''
        # sum = exclusive upper - inclusive lower
        n: int = len(cards)
        cumsum = [0 for _ in range(n + 1)]

        for ix in range(1, n + 1):
            cumsum[ix] = cumsum[ix - 1] + cards[ix - 1]

        # print(f'cum_sum: {cumsum}')

        ''' Try all possible windows '''
        for i in range(k + 1):

            # Take i cards from left, and remaining from right
            left_sum = cumsum[k - i] - cumsum[0]
            right_sum = cumsum[n] - cumsum[n - i]

            # print(f'left: {k - i}  right: {i}')
            # print(f' left: {left_sum}  right: {right_sum}')

            current = left_sum + right_sum

            result = max(result, current)

        return result
