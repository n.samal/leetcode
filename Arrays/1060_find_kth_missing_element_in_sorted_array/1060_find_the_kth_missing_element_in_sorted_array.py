"""
Inputs
    nums (List[int]): sorted array of unique values
    k (int): kth number
Outputs
    int: find the kth missing number from the left most value
    
Example

    Example 1
        Input: A = [4,7,9,10], K = 1
        Output: 5
        Explanation: 
        The first missing number is 5.
    
    Example 2
        
        Input: A = [4,7,9,10], K = 3
        Output: 8
        Explanation: 
        The missing numbers are [5,6,8,...], hence the third missing number is 8.
    
    Example 3

        Input: A = [1,2,4], K = 3
        Output: 6
        Explanation: 
        The missing numbers are [3,5,6,7,...], hence the third missing number is 6.

Cases
    Null => First missing is 1 or 0
    Single => First missing is 1 + last value
    Double =>
        No Gap: (4,5) => Delta of 1
        Single Gap: (4, 6) => Delta of 2
        Multi Gap: (4, 7) => Delta > 2
    
    Within Array
        - kth value lies within the array ranges
        
        A = [4, 7, 9, 10], K = 3
        Answer = 8
        
        4 => 5, 6
        7 => 8

    Beyond Array
        - kth value is past last array value
    
        A = [1, 2, 4], K = 3
        Answer = 6
        
        1, 2 => 3
        4 => 5, 6 

Ideas

    - scan the numbers from left to right
    - compare adjacent numbers
        + compute the number of missing values
        + track the number of missing values we've seen so far
    
    - Track the number of missing values can be done two ways
        + count of missing values in seperate int
        + or decrement the given K value (more efficient, but modifying the value)

"""


class Solution:
    def missingElement(self, arr: List[int], k: int) -> int:
        """
        
        - compare adjacent values and compute the number of missing values
        - decrement our k value until it's within range
            - if within our range, compute the missing number
            - if outside our range continue through the array
        - if we get outside the array just bump the number by the remaining counts

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        
        for ix in range(1, len(arr)):
            
            # Missing values between numbers
            delta = arr[ix] - arr[ix - 1] - 1
            
            # print(f'delta: {delta}  k: {k}')
            
            # Value lies within our range, shift from left value of range
            if k <= delta:
                return arr[ix - 1] + k
            
            # Not within the range, decrement our count
            else:
                k -= delta

        # Null case
        # Made it to the end without finding any value
        if arr:
            return arr[ix] + k
        else:
            return k
            
