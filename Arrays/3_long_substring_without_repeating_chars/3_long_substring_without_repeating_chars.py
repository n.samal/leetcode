"""

Inputs
    arr (List[int]): numbers
    target (int): target summation
Outputs
    List[int]: indices of the pair

Cases

    'abcabcdefg'
    'wwwdef'


Strategy

    - Caterpillar! aka sliding window

        + crawl forward while we have new characters to explore
        + if we run into an old character, reduce the size of our window

            * chars.remove(string[tail_ix])
            * tail_ix += 1
        + if we have a new character

            * add it to our set
                chars.add(string[head_ix])
            * update our max 
                max = max(current, max)
            * Move the head forward
                head_ix += 1

        Time Complexity
            O(2n) each character is visited twice
        Space Complexity
            O(n) all unique characters

    - Caterpillar with Memory

        + use the same sliding window approach
        + but we track the index of the last time we saw each character
        + if we run into a new character we've seen before
            bump our indices using the index stored in our memory

        ie: 'abcdefgc'

            tail_chr = 'a'
            head_chr = 'c'

            instead of bumping the tail from 'a' -> 'b'
            we use the memory to bump from 'a' to 'd'

            moving one past the last time we saw the 'c' character
            **Note: we should only consider this if the last time we saw 'c'
            is in our current sub window.

            We're not updating the hash memory each time we slide!!
"""


class Solution:

    def lengthOfLongestSubstring(self, s: str) -> int:
        """Sliding window approach
        
        Time Complexity
            O(2n) each character is visited twice
        Space Complexity
            O(n) all unique characters
        """
        # Init
        n = len(s)
        max_len = 0

        # Initialize pointers
        tail_ix = 0
        head_ix = 0

        # Initial set
        chars = set([])

        # We have characters left to explore
        while head_ix < n:

            # New character
            if s[head_ix] not in chars:

                # Add the character
                chars.add(s[head_ix])

                # Track the max length
                max_len = max(max_len, len(chars))

                # Move the head forward
                head_ix += 1

            # Found a duplicate character
            else:

                # Remove the old characters
                chars.remove(s[tail_ix])

                # Move up one
                tail_ix += 1

        return max_len

    def lengthOfLongestSubstring2(self, s: str) -> int:
        """Sliding window approach

        - remove len() and use indices for the calculation instead

        Time Complexity
            O(2n) each character is visited twice
        Space Complexity
            O(n) all unique characters

        Notes
            - much improved speed over len() version

        """
        # Init
        n = len(s)
        max_len = 0

        # Initialize pointers
        tail_ix = 0
        head_ix = 0

        # Initial set
        chars = set([])

        # We have characters left to explore
        while head_ix < n:

            # New character
            if s[head_ix] not in chars:

                # Add the character
                chars.add(s[head_ix])

                # Track the max length
                max_len = max(max_len, (head_ix - tail_ix) + 1)

                # Move the head forward
                head_ix += 1

            # Found a duplicate character
            else:

                # Remove the old characters
                chars.remove(s[tail_ix])

                # Move up one
                tail_ix += 1

        return max_len

    def lengthOfLongestSubstring3(self, s: str) -> int:
        """Sliding window approach with memory

        - remember the last time we saw each character
        - move indices forward by that amount
            if we're still considering them in our window

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        Notes
            - improved speed over second version
        """
        # Init
        n = len(s)
        max_len = 0

        # Initialize pointers
        tail_ix = 0
        head_ix = 0

        # Initial set
        chars = {}

        # We have characters left to explore
        while head_ix < n:

            # New character
            if s[head_ix] not in chars:

                # Add the character index tracking
                chars[s[head_ix]] = head_ix

            # Found a duplicate character
            else:

                # Determine when we saw this character last
                ix_last = chars[s[head_ix]]

                # We're still considering these indices in our window
                if ix_last >= tail_ix:

                    # Move up one past the last time we saw this character
                    tail_ix = ix_last + 1

                # Update our character hash, with the last time we saw this
                # aka now
                chars[s[head_ix]] = head_ix

            # Track the max length
            max_len = max(max_len, (head_ix - tail_ix) + 1)

            # Move the head forward
            head_ix += 1

        return max_len

    def track_pointers(self, s: str) -> int:
        """

        - track the left pointer (exclusive)
        - compute the maximum window from current length
        - if we encounter an old repeated character we bump our
        pointer to that position
            + must make sure movements are monotonic ie: 'abba'
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        dct = {}
        max_len: int = 0
        l: int = -1

        for ix, char in enumerate(s):

            if char not in dct:
                dct[char] = ix
            else:
                l = max(l, dct[char])
                dct[char] = ix

            max_len = max(max_len, ix - l)

        return max_len
        



if __name__ == "__main__":

    s = Solution()

    # Sorted values
    s_in = "abcabcbb"
    # s_in = "bbbbb"
    # s_in = "pwwkew"

    # s_in = "abcdecfg"
    # s_in = "abba"
    # s_in = "tmmzuxt"

    # print(s.lengthOfLongestSubstring(s_in))
    print(s.lengthOfLongestSubstring3(s_in))
