"""
Inputs
    arr (List[List[int]]): binary grid
Outputs
    int: longest line of consecutive ones
Notes
    The line could be horizontal, vertical, diagonal or anti-diagonal.

Example

    Input:
        [[0,1,1,0],
         [0,1,1,0],
         [0,0,0,1]]
    Output: 3
"""

class Solution:
    def longestLine(self, arr: List[List[int]]) -> int:
        """

        - keep matrices of ones in a row for each direction
            + left to right along row
            + left to right diagnol
            + right to left diagnol
            + up to down along column

        Time Complexity
            O(n*m)
        Space Complexity
            O(n*m)
        Notes
            - we could combine the matrices into one 3D array instead
            of multiple 2D arrays
            d1 = row
            d2 = column
            d3 = matrice type ie: rows, cols, diag lr, diag rl
        """

        if not arr:
            return 0

        n_rows: int = len(arr)
        n_cols: int = len(arr[0])

        rows = [[0 for _ in range(n_cols)] for _ in range(n_rows)]
        cols = [[0 for _ in range(n_cols)] for _ in range(n_rows)]
        diag_lr = [[0 for _ in range(n_cols)] for _ in range(n_rows)]
        diag_rl = [[0 for _ in range(n_cols)] for _ in range(n_rows)]

        max_count: int = 0

        # Process rows
        for r in range(n_rows):
            for c in range(n_cols):

                if arr[r][c] == 1:

                    if c - 1 >= 0:
                        rows[r][c] = 1 + rows[r][c - 1]
                    else:
                        rows[r][c] = 1

                    if c - 1 >= 0 and r - 1 >= 0:
                        diag_lr[r][c] = 1 + diag_lr[r - 1][c - 1]
                    else:
                        diag_lr[r][c] = 1

                    if c + 1 < n_cols and r - 1 >= 0:
                        diag_rl[r][c] = 1 + diag_rl[r - 1][c + 1]
                    else:
                        diag_rl[r][c] = 1

                    max_count = max(max_count, rows[r][c], diag_lr[r][c], diag_rl[r][c])

        # Process columns
        for c in range(n_cols):
            for r in range(n_rows):

                if arr[r][c] == 1:

                    if r - 1 >= 0:
                        cols[r][c] = 1 + cols[r - 1][c]
                    else:
                        cols[r][c] = 1

                    max_count = max(max_count, cols[r][c])

        return max_count
