"""
Inputs
	N (int): number of seats in a row
Outputs
	
Notes

	- if no one is sitting yet, we should sit at location 0
	- there are N seats in a single row

	- when someone enters the room we want to maximize the distance to the nearest
	person

		+

	- we also remove individuals from the room use the 'leave' method

Ideas
	- compare adjacent seats taken, then sit them in the middle
	- we also need to look at seats at the 0 position and n - 1 position
		+ these distances are different, no division by 2 necessary
"""

import bisect

class ExamRoom:

    def __init__(self, N: int):
        
        self.arr = []
        self.n = N

    def seat(self) -> int:
    	"""

    	- track the the indices of seats taken
    	- compare adjacent seats and calculate the mid point
    	- also review seats at the first and last position

    	Time Complexity
    		O(n)
    	Space Complexity
    		O(1)
    	"""
        
        # No one sitting, take the first seat
        if len(self.arr) == 0:
            self.arr = [0]
            return 0
        else:
            
            prev = self.arr[0]
            max_dist = -1
            
            # Compute distance to first seat if not taken
            if self.arr[0] != 0:
                dist = self.arr[0] - 0
                
                if dist > max_dist:
                    max_dist = dist
                    new_seat = 0
            
            # Go through all taken seats
            # compute mid point distance
            for seat in self.arr[1:]:
                
                dist = (seat - prev) // 2

                if dist > max_dist:
                    max_dist = dist
                    new_seat = prev + dist
                
                prev = seat
            
            # Last position is open
            if self.arr[-1] != self.n - 1:

                dist = (self.n - 1 - prev)
                if dist > max_dist:
                    max_dist = dist
                    new_seat = self.n - 1
            

            # Save the new position
            bisect.insort(self.arr, new_seat)
            
            # print(self.arr)
            
            return new_seat
            
    def leave(self, p: int) -> None:
    	"""Remove one seat

		Time Complexity
			O(n)
		Space Complexity
			O(1)
    	"""
        
        # Find the taken seat and remove it
        ix = bisect.bisect_left(self.arr, p)
        self.arr.pop(ix)
        
        # print(f'leave: {p}')
        # print(self.arr)
