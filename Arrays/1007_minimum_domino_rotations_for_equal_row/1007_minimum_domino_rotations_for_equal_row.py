"""
Inputs:
    A (List[int]): top row of values on each domino
    B (List[int]): bottom row of values on each domino

Goals
    - how many swaps does it take to get all the same value in one of the rows?

Notes
    - Dominos only numbered from 1 to 6

Ideas
    - Get the count of each value.
        + if we have more than 4 values total it's possible
        + be careful that we don't double count the same dominoe

        2 | 2

Examples

    Example 1
        A = [2,1,2,4,2,2]
        B = [5,2,6,2,3,2]

        1: A = 1, B = 0, same = 0
        2: A = 4, B = 3, same = 1
        3: A = 0, B = 1, same = 1
        4: A = 1, B = 0, same = 0
        5: A = 0, B = 1, same = 0
        6: A = 0, B = 1, same = 0

        2 is the only possbility

        we have A = 4, B = 3, one double count

        ignoring the double count

        we can swap  4 - 1 dominoes from the top row 
        or 3 - 1 dominos from the bottom row

    Example 2
        A = [3,5,1,2,3]
        B = [3,6,3,3,4]

        Here we have 5 threes. However we can't make a full row
        The first domino has two threes. We can't swap that to a position
        where we want a three

        1: one
        2: two
        3: 5 - 1 double counted = 4
        4: 1
"""

from typing import List


class Solution:
    def count_and_check_v0(self, A: List[int], B: List[int]) -> int:
        """

        - save counts of each dominoe in an array
            + one for top row
            + one for bottom row
            + another for duplicates

        - if we're able to get 6 in a row, then determine
        which swap would be better for us
            + either swapping from A or B
            + remember not to count values twice ie: duplicates

        Time Complexity
            O(n)
        Space Complexity
            O(1)

        """
        n: int = len(A)

        countsA = [0 for _ in range(6)]  # top row
        countsB = [0 for _ in range(6)]  # bottom row
        countsS = [0 for _ in range(6)]  # double counted

        # Add the counts
        for ix in range(n):

            # Increment the tile count
            countsA[A[ix] - 1] += 1
            countsB[B[ix] - 1] += 1

            # Same value on both sides of domino
            if A[ix] == B[ix]:
                countsS[A[ix] - 1] += 1

            # If we've already n in a row, we're done
            if countsA[A[ix] - 1] == n or countsB[B[ix] - 1] == n:
                return 0

        # print(f'Top Row   : {countsA}')
        # print(f'Bot Row   : {countsB}')
        # print(f'Duplicates: {countsS}')

        min_swaps: int = float("inf")

        # Check which numbers may work
        for ix in range(6):

            # Possibility found
            # figure out if it's better to move dominoes from A or B
            if countsA[ix] + countsB[ix] - countsS[ix] >= n:
                min_val = min(countsA[ix] - countsS[ix], countsB[ix] - countsS[ix])
                min_swaps = min(min_swaps, min_val)

        # No valid one found
        if min_swaps == float("inf"):
            return -1
        else:
            return min_val

    def count_and_check_v1(self, A: List[int], B: List[int]) -> int:
        """Same method as before but with easier variable names

        - cleaner

        Time Complexity
            O(n)
        Space Complexity
            O(1)

        """
        n: int = len(A)

        countsA = [0 for _ in range(6)]  # top row
        countsB = [0 for _ in range(6)]  # bottom row
        countsS = [0 for _ in range(6)]  # double counted

        # Add the counts
        for ix in range(n):

            val_top = A[ix]
            val_bot = B[ix]

            # Increment the tile count
            countsA[val_top - 1] += 1
            countsB[val_bot - 1] += 1

            # Same value on both sides of domino
            if val_top == val_bot:
                countsS[val_top - 1] += 1

        # print(f'Top Row   : {countsA}')
        # print(f'Bot Row   : {countsB}')
        # print(f'Duplicates: {countsS}')

        # Check which numbers may work
        for ix in range(6):

            # Possibility found
            # figure out if it's better to move dominoes from A or B
            if countsA[ix] + countsB[ix] - countsS[ix] >= n:
                return min(countsA[ix], countsB[ix]) - countsS[ix]

        # No possibilty found
        return -1

    def count_and_check_v2(self, A: List[int], B: List[int]) -> int:
        """Same method as before but with easier variable names

        - cleaner
        - check counts on the fly
        - use an array numbered from 0 to 6 to make indexing easier to read

        Time Complexity
            O(n)
        Space Complexity
            O(1)

        """
        n: int = len(A)

        countsA = [0 for _ in range(7)]  # top row
        countsB = [0 for _ in range(7)]  # bottom row
        countsS = [0 for _ in range(7)]  # double counted

        # Add the counts
        for ix in range(n):

            val_top = A[ix]
            val_bot = B[ix]

            # Increment the tile count
            countsA[val_top] += 1
            countsB[val_bot] += 1

            # Same value on both sides of domino
            if val_top == val_bot:
                countsS[val_top] += 1

            # Possibility found
            # figure out if it's better to move dominoes from A or B
            if countsA[val_top] + countsB[val_top] - countsS[val_top] >= n:
                return min(countsA[val_top], countsB[val_top]) - countsS[val_top]
            elif countsA[val_bot] + countsB[val_bot] - countsS[val_bot] >= n:
                return min(countsA[val_bot], countsB[val_bot]) - countsS[val_bot]

        # No possibilty found
        return -1
