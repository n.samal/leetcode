from typing import List
import math

"""

Requirements:

    Space: O(1)
    Time: O(n)

Strategies:

- sorting will not work since Tthis would require O(n^2)
    
    + sort then compare adjacent values and determine if a gap exists

- if extra space existed, we could use a Set to verify the existence
  of each value in the range

- from the examples the smallest possible positive is +1

Strategy 1

    - Iterate through the array and determine current min and max
    - if the minimum value is greater than 1, then the missing positive => 1
    - if the maximum value is less than 1, then the missing positive => 1

        [3, 4, 1, 6, 6]

        min: 1
        max: 6
        n = 5

        we could have missing values in the range
        ie: [1, 6, 1, 6, 1]

        must do more work

    - if all the values were valid, we'd have numbers ranging from 1 to n
        [1, 2, 3, ...., n-2, n-1, n]

    - let's write over invalid values with an invalid positive number

        - we need a positive number due to the method we use for
        marking valid numbers

        before: [3, 4, 1, 6, 6]
        after : [3, 4, 1, n+1, n+1]

    - if a number is valid, then mark the respective index
        - we need to mark the indice and preserve information

        ie: [4, 3, 2, 1]

        if we overwrite indices, then we can run into issues with
        later indices
            ix = 0: [4, 3, 2, 1]
            ix = 1: [4, 2, 1, V]
            ix = 2: [4, 2, V, V]

    - mark values with their negative

        Only consider values within 1 to n

        index = abs(val) - 1

        before: [3, 4, 1, 6, 6]

        index = 3 - 1 = 2

        after : [3, 4, -1, 6, 6]

        index = 4 - 1 = 3

        after : [3, 4, -1, -6, 6]

        index = 1 - 1 = 0

        after : [-3, 4, -1, -6, 6]

        value = abs(-6) => invalid index range
        value = abs(-6) => invalid index range

        arr : [-3, 4, -1, -6, 6]

    - Find the first value not marked as valid aka is positive
        arr : [-3, *4*, -1, -6, 6]
"""


class Solution(object):
    def firstMissingPositive(self, arr: List[int]) -> int:
        """Find the first missing positive number

        Space Constraints: O(1)
        Time Constraints: O(n)

        Args:
            arr (List[int]): integers in unknown order
        Returns:
            int: first missing posiive
        """

        # Initialize values
        start = math.inf
        stop = -math.inf
        n = len(arr)

        ''' Determine array bounds '''

        # Iterate through range and determine min and max values
        # that are positive
        for ix in range(n):

            start = min(start, arr[ix])
            stop = max(stop, arr[ix])

            # Check if valid
            if arr[ix] < 1 or arr[ix] > n:

                # Overwrite invalid numbers
                arr[ix] = n + 1

        ''' Base Cases '''
        # If our start value is greater than 1 we know we can use 1
        if start > 1:
            return 1

        # We've got all negatives
        elif stop < 1:
            return 1

        # No values
        elif n == 0:
            return 1

        ''' Mark Valid Position Indices '''

        # Let's iterate through the remaing valid numbers
        for ix in range(n):

            # Treat all values as valid
            val = abs(arr[ix])

            # Number is valid
            if 1 <= val <= n:

                # Shift value to represent an index
                # 1 => 0
                jx = val - 1

                # Mark the indice as a valid number
                # this marks the index without overwriting information
                arr[jx] = abs(arr[jx]) * -1

        ''' Find invalid value '''

        # Iterate through array and find the first number that isn't valid
        # we marked valid values as negative
        for ix in range(n):

            # Find positive values
            if arr[ix] > 0:
                return ix + 1

        # If all values were valid
        # [1, 2, 3, ..., n-1, n]
        return n + 1


if __name__ == "__main__":

    s = Solution()

    # No values
    # print(s.firstMissingPositive([]))

    # All positives
    # print(s.firstMissingPositive([7, 8, 9, 11, 12]))

    # All negatives
    # print(s.firstMissingPositive([-7, -8, -9, -11, -12]))

    # All valid numbers
    # print(s.firstMissingPositive([1, 2, 3, 4]))
    # print(s.firstMissingPositive([4, 1, 2, 3]))

    # Valid numbers, one missing
    # print(s.firstMissingPositive([1, 2, 0, 4]))

    # Valid numbers, multiple missing
    # print(s.firstMissingPositive([200, 2, 2000, 1]))
    # print(s.firstMissingPositive([3, 4, -1, 1]))
