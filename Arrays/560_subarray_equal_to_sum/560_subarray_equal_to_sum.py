"""

Inputs:
    arr (List[int]): values 
    k: desired sum
Outputs:
    int: number of subarrays each to desired sum
Goals:
    find the count of subarrays each to the desired sum

Ideas
    - use a sliding window?
        + may not work because the array is not sorted
    - cummulative sum
        + will also us to calcualte the subarray sum more
        efficiently
Strategies
    Cumumulative Sum Iteration

        + compute the cummulative sum for the entire array
        + iterate through all possible subarray indices
            (i, j)
        + compute their difference and see if it matches k

    Cumumulative Sum Hash
        + compute the cummulative sum for the entire array
        + store the cumsum at each index
        + search for the complimenting cumsum

        k = cumsum_i - cumsum_j

        + use a hash to identity if we've found the compliment

"""

from itertools import accumulate
from typing import List


class Solution:
    def iterative(self, arr: List[int], k: int) -> int:
        """Iterate through cummulative sum and check

        Time Complexity
            O(n^2)

        Space Complexity
            O(n)

        Notes
            - too slow
        """
        n: int = len(arr)
        count: int = 0

        # Compute cummulative sum starting with 0
        cumsum = [0 for val in range(n)]
        cumsum[0] = arr[0]

        for ix in range(1, n):
            cumsum[ix] = cumsum[ix - 1] + arr[ix]

        # Try all pair combinations
        for ix in range(n):

            # Cummulative sum from start works
            if cumsum[ix] == k:
                count += 1

            for jx in range(ix + 1, n):

                # Compute subarray sum
                sum_i = cumsum[jx] - cumsum[ix]

                if sum_i == k:
                    count += 1

        return count

    def iterative2(self, arr: List[int], k: int) -> int:
        """Iterate through cummulative sum and check

        Time Complexity
            O(n^2)

        Space Complexity
            O(n)

        Notes
            - too slow
        """
        n: int = len(arr)
        count: int = 0

        # Compute cummulative sum starting with 0
        cumsum = [0 for val in range(n + 1)]

        for ix in range(1, n + 1):
            cumsum[ix] = cumsum[ix - 1] + arr[ix - 1]

        # Try all pair combinations
        for ix in range(n + 1):
            for jx in range(ix + 1, n + 1):

                # print(f"ix: {ix} jx: {jx}")

                # Compute subarray sum
                sum_i = cumsum[jx] - cumsum[ix]

                if sum_i == k:
                    count += 1

        return count

    def iterative_no_space(self, arr: List[int], k: int) -> int:
        """Iterate through cummulative sum starting at each position

        Time Complexity
            O(n^2)
        Space Complexity
            O(1)

        """
        n: int = len(arr)
        count: int = 0

        # Start a window at each position
        for ix in range(n):

            sum_i: int = 0

            for jx in range(ix, n):

                sum_i += arr[jx]

                if sum_i == k:
                    count += 1

        return count

    def hashtable(self, arr: List[int], k: int) -> int:
        """Use hashtable to store previous cummulative sums

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        n: int = len(arr)
        count: int = 0

        # Compute cummulative sum starting with 0
        # One instance of zero ie: null
        cumsum = 0
        d = {0: 1}

        for ix in range(n):

            # Add current value to cumsum
            cumsum += arr[ix]

            # Compute compliment
            comp = cumsum - k

            # Add number of pairs that meet this sum
            if comp in d:
                count += d[comp]

            # Save value in hash
            if cumsum not in d:
                d[cumsum] = 1
            else:
                d[cumsum] += 1

        return count

    def hashtable2(self, arr: List[int], k: int) -> int:
        """Use hashtable to store previous cummulative sums

        - use dictionary default methods

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        n: int = len(arr)
        count: int = 0

        # Compute cummulative sum starting with 0
        # One instance of zero ie: null
        cumsum = 0
        d = {0: 1}

        for ix in range(n):

            # Add current value to cumsum
            cumsum += arr[ix]

            # Compute compliment
            comp = cumsum - k

            # Add number of pairs that meet this sum
            count += d.get(comp, 0)

            # Save value in hash
            if cumsum in d:
                d[cumsum] += 1
            else:
                d[cumsum] = 1

        return count


if __name__ == "__main__":

    # arr = [1, 1, 1]
    arr = [1, -1, 2, 4, 7, 9, 2, -4, 2]
    k = 2

    obj = Solution()
    print(obj.iterative(arr, k))
    print(obj.iterative2(arr, k))
    print(obj.hashtable(arr, k))
    print(obj.hashtable2(arr, k))
