"""
Inputs
    intervals (List[List[int]]): sorted time intervals that are disjoint
    toBeRemoved(List[int]): time interval to remove
Outputs
    List[List[int]]: sorted time intervals with removed time

Notes

    - each interval intervals[i] = [a, b] represents the 
    set of real numbers x such that a <= x < b.
    - remove the intersections between any interval in intervals and the interval toBeRemoved.

Examples

    Example 1:

        Input: intervals = [[0,2],[3,4],[5,7]], toBeRemoved = [1,6]
        Output: [[0,1],[6,7]]

    Example 2:

        Input: intervals = [[0,5]], toBeRemoved = [2,3]
        Output: [[0,2],[3,5]]

    Example 3:

        Input: intervals = [[-5,-4],[-3,-2],[1,2],[3,5],[8,9]], toBeRemoved = [-1,4]
        Output: [[-5,-4],[-3,-2],[4,5],[8,9]]

Cases

    Partial Overlap: Beginning

        + start time is truncated

    Partial Overlap: End
        
        + ending time is truncated

    Partial Overlap: Middle

        + the interval must be split in half

    Full Overlap

        + 

    No Interference

"""

class Solution:
    def removeInterval(self, intervals: List[List[int]], toBeRemoved: List[int]) -> List[List[int]]:
        """Check for interference with the removal interval

        Cases
        1) whole interval to be removed
        2) we split the interval in half
        3) we partially split the interval
        4) no interference

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        
        intervals_new = []
        
        # Iterate across the intervals to see if any intersection occurs
        for start, stop in intervals:
            
            # Remove the whole interval
            if toBeRemoved[0] <= start <= toBeRemoved[1] and toBeRemoved[0] <= stop <= toBeRemoved[1]:
                continue
            
            # Save booleans for ease
            start_within = start < toBeRemoved[0] < stop
            stop_within = start < toBeRemoved[1] < stop
            
            # Split the current interval in half
            if start_within and stop_within:
                intervals_new.append([start, toBeRemoved[0]])
                intervals_new.append([toBeRemoved[1], stop])
            
            elif start_within and not stop_within:
                intervals_new.append([start, toBeRemoved[0]])
                
            elif stop_within and not start_within:
                intervals_new.append([toBeRemoved[1], stop])
            
            # No interference
            else:
                intervals_new.append([start, stop])
            
        return intervals_new
        