"""
Inputs
    arr (List[int]): integers in unknown order
    k (int): size of the group
Outputs
    bool: can the array be split into sets of k consecutive numbers

Examples

    Example 1

        Input: nums = [1,2,3,3,4,4,5,6], k = 4
        Output: true
        Explanation: Array can be divided into [1,2,3,4] and [3,4,5,6].

    Example 2

        Input: nums = [3,2,1,2,3,4,3,4,5,9,10,11], k = 3
        Output: true
        Explanation: Array can be divided into [1,2,3] , [2,3,4] , [3,4,5] and [9,10,11].

    Example 3
        Input: nums = [3,3,2,2,1,1], k = 3
        Output: true

    Example 4

        Input: nums = [1,2,3,4], k = 3
        Output: false
        Explanation: Each array should be divided in subarrays of size 3.

Ideas
    - track the counts of each value
    - if we can't split the array equally we have no chance

    - grab the smallest value and try build up the array accordingly
        + if we have available counts then move to the next number
        + if no number available then we fail

Notes
    - seems almost identical to #649, except that we need to sort
    - also identical to #846, hand of straights

"""

from collections import defaultdict
from typing import List


class Solution:
    def isPossibleDivide(self, arr: List[int], k: int) -> bool:
        """

        - check if the groups can be split equally
        - track the frequency of each character
        - try building up each group
            + if a value is unavailable fail
            + if value is available keep building up the group

        Args:

        Returns:

        Time Complexity
            O(n*log(n))

            if sorting all unique values

        Space Complexity
            O(n)
        """

        # We can always split into groups of 1
        if arr and k == 1:
            return True

        # Check if we can evenly split the values
        n: int = len(arr)
        div, mod = divmod(n, k)

        if mod != 0:
            return False

        ''' Count the value frequency'''

        counts = defaultdict(int)

        for val in arr:
            counts[val] += 1

        # Grab all uniques values, from largest to smallest
        uniques = sorted(counts.keys(), reverse=True)

        for g_ix in range(div):

            # Grab the smallest available number
            start = uniques[-1]
            while counts[start] <= 0:
                _ = uniques.pop()
                start = uniques[-1]

            # print(f'start: {start}  counts: {counts[start]}')

            # Try building up the set
            for ix in range(k):

                # print(f' next: {start + ix}  counts: {counts[start + ix]}')

                if counts[start + ix] > 0:
                    counts[start + ix] -= 1
                else:
                    return False

        return True


if __name__ == '__main__':

    arr_in = [12, 12, 2, 11, 22, 20, 11, 13, 3, 21, 1, 13]
    groupsize = 3

    obj = Solution()
    print(obj.isPossibleDivide(arr_in, groupsize))
