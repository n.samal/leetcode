"""
Inputs
    arr (List[int]): values in unknown order
Outputs
    int: length of longest mountain subarray

Notes
    - a mountain subarray has a increasing section
    then a descreasing section
        + increasing section must be purely increasing
        + decreasing section must be purely decreasing
        + the shorest mountain possible is of length 3

Examples

    Example 1:

        Input: arr = [2,1,4,7,3,2,5]
        Output: 5
        Explanation: The largest mountain is [1,4,7,3,2] which has length 5.

    Example 2:

        Input: arr = [2,2,2]
        Output: 0
        Explanation: There is no mountain.

Ideas
    - create a stack and track values in our mountaing
        + maintain montonically increasing stack for ascension
        + maintain montonically decreasing stack for descension

        + we don't really need the values, but just the lengths

    - track the count of increasing and decreasing values instead

"""

class Solution:
    def longestMountain(self, arr: List[int]) -> int:
        """

        - track the number of increasing values in a row
        - track the number of decreasing values in a row
        - when we have a mountain of at least size 3, save
        our best result

        - we also need to bump indices for flat surfaces
            ie: 2, 2, 2, 2

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        n: int = len(arr)

        # if increasing we have potential for a mountain
        # when decreasing we have 

        max_len: int = 0
        n_inc: int = 1
        n_dec: int = 0

        ix: int = 1

        while ix < n:

            while ix < n and arr[ix] > arr[ix - 1]:
                n_inc += 1
                ix += 1

            while ix < n and arr[ix] < arr[ix - 1]:
                n_dec += 1
                ix += 1

            if n_inc >= 2 and n_dec >= 1:
                max_len = max(max_len, n_inc + n_dec)

            # Constant value
            if ix < n and arr[ix] == arr[ix - 1]:
                ix += 1

            # Reset counts
            n_inc = 1
            n_dec = 0

        return max_len