"""
Inputs

Outputs

Ideas
    - at each node in the matrix 
        + search for 3 or more adjacent candies
        + flood fill / check horizontally
        + flood fill / check vertically

        + replace those values with zeros,
        but only once all patterns have been found.

        some of the vertical nodes are also involved
        in horizontal patterns

            2 2 2
            2
            2

        + may be easier to search along a constant column
        , then we don't repeat the search along something 
        we'd already reviewed

        then go over constant row in a similar fashion

    - mark all flagged cells with zeros, or potentially
    with a negative number since these are not valid
    candy ranges

    - iterate over columns and slide elements up vertically for all 
        + rotate array 
        + pop() possible over columns?

        maintain a copy of the column values

        pop all non positive elements, then append 0s to the end
        while the length is less than normal

        then overwrite values with our new column

        + better idea is use pointers, and modify the overwrite position

    - apply same functionality once again to determine if any new patterns
    exist

"""

from typing import List


class Solution:
    def candyCrush(self, arr: List[List[int]]) -> List[List[int]]:
        """

        - search for patterns in the array
            + mark them with their negative
            + track if any patterns were found
        - remove marked values from the grid
            + add zeros depending on how many candies were crushed
            in a column

        - iterate again if changes were made

        Time Complexity
            O(n*m)

        Space Complexity
            O(1)
        """

        n_rows: int = len(arr)
        n_cols: int = len(arr[0])
        found: bool = False

        # Search for horizontal patterns of 3
        for row_ix in range(n_rows):
            for col_ix in range(n_cols - 2):

                val = arr[row_ix][col_ix]

                if abs(val) > 0 and abs(arr[row_ix][col_ix + 1]) == abs(val) and abs(arr[row_ix][col_ix + 2]) == abs(val):
                    arr[row_ix][col_ix] = -abs(val)
                    arr[row_ix][col_ix + 1] = -abs(val)
                    arr[row_ix][col_ix + 2] = -abs(val)
                    found = True

        # Search for vertical patterns of 3
        for col_ix in range(n_cols):
            for row_ix in range(n_rows - 2):

                val = arr[row_ix][col_ix]

                if abs(val) > 0 and abs(arr[row_ix + 1][col_ix]) == abs(val) and abs(arr[row_ix + 2][col_ix]) == abs(val):
                    arr[row_ix][col_ix] = -abs(val)
                    arr[row_ix + 1][col_ix] = -abs(val)
                    arr[row_ix + 2][col_ix] = -abs(val)
                    found = True

        # Crush the candies if patterns were found
        # iterate across constant columns, and slide down positive values
        if found:

            for col_ix in range(n_cols):

                jx = n_rows - 1  # overwrite index

                for row_ix in range(n_rows - 1, -1, -1):

                    if arr[row_ix][col_ix] > 0:
                        arr[jx][col_ix] = arr[row_ix][col_ix]
                        jx -= 1

                count = jx + 1

                # Write zeros to top end
                for row_ix in range(n_rows):
                    if count:
                        arr[row_ix][col_ix] = 0
                        count -= 1
                    else:
                        break

            return self.candyCrush(arr)

        else:
            return arr


if __name__ == '__main__':

    # Example 1
    board = [[110,5,112,113,114],[210,211,5,213,214],[310,311,3,313,314],[410,411,412,5,414],[5,1,512,3,3],[610,4,1,613,614],[710,1,2,713,714],[810,1,2,1,1],[1,1,2,2,2],[4,1,4,4,1014]]

    obj = Solution()
    print(obj.candyCrush(board))
