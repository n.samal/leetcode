"""
Inputs
    arr (List[int]): values in sorted order
Outputs
    bool: 
Goals
    Given an array nums sorted in ascending order, return true if and only if you can split it into 1 or more subsequences such that each subsequence consists of consecutive integers and has length at least 3.

Examples

    Example 1
        Input: [1,2,3,3,4,5]
        Output: True
        Explanation:
        You can split them into two consecutive subsequences : 
        1, 2, 3
        3, 4, 5

    Example 2

        Input: [1,2,3,3,4,4,5,5]
        Output: True
        Explanation:
        You can split them into two consecutive subsequences : 
        1, 2, 3, 4, 5
        3, 4, 5

    Example 3

        Input: [1,2,3,4,4,5]
        Output: False

Ideas

    - create counter of each value
    - create chains of 3 when possible
    - if smaller chains occur
        + try to connect to other chains

        ie: [3, 4, 5] => [6, 7]
            [3, 4, 5] => [6]

    - numbers with multiples appear to be locations of starting new chains

        ie: [1,2,3,3,4,4,5,5]

            new_chain @ 3 = 3
            new_chain @ 4 = 4
            new_chain @ 5 = 5

    - create a table of chains
        + one table of length 3 or greater
            * key parameter = ending value
            * we know value is already greater than 3 so we don't need to track it
        + one table of smaller chains
            * key parameter = starting value
            * this is not necessary since smaller values can be appended onto existing
            chains in increments

            ie:
            [1, 2, 3]
            [1, 2, 3] => 4
            [1, 2, 3] => 4 => 5

Cases

    Chain of 3
        [1, 2, 3]
        [1, 2, 3] [7, 8, 9]

    Existing Chain + 1
        [1, 2, 3] => 4
        [1, 2, 3] [7, 8, 9] => 10

    Existing Chain + 2
        [1, 2, 3] => 4 => 5

        4 added on first iteration
        5 added on next iteration

References
    - https://docs.python.org/3.7/library/collections.html#collections.defaultdict
"""

from collections import defaultdict 

class Solution:
    def isPossible(self, arr: List[int]) -> bool:
        """Greedily create sequences

        - add values to an existing sequence if possible
        - otherwise create a new 3 chain sequence
        - if either is not possible, then fail

        Args:
            arr (List[int]): integers in sorted order
        Returns:
            bool: can consecutive subsequences be created 
        Time Complexity
            O(n)
        Space Complexity
            O(n)

        Notes
            - works for 98% of test cases
        """

        # Count occurences of each value
        counts = defaultdict(int)
        for val in arr:
            counts[val] += 1

        # Track the number of chains ending at each value
        # ie: [3, 4, 5] => chains[5] = 1
        chains_complete = defaultdict(int)

        for val in arr:

            # print(f'value: {val}')

            if counts[val] == 0:
                continue

            # Try adding this onto an existing sequence
            elif chains_complete[val - 1] > 0:

                # Remove previous chain
                chains_complete[val - 1] -= 1

                # New chain ends at current value
                chains_complete[val] = chains_complete.get(val, 0) + 1

                # Reduce counts
                counts[val] -= 1

            # We've got two numbers to add onto
            # create a chain of 3
            elif counts[val + 1] > 0 and counts[val + 2] > 0:

                # print(' Chain of 3')

                # Reduce all counts
                counts[val] -= 1
                counts[val + 1] -= 1
                counts[val + 2] -= 1

                # Save the last value
                chains_complete[val + 2] = counts.get(val + 2, 0) + 1

            # Unable to connect to any value
            else:
                return False

        return True
