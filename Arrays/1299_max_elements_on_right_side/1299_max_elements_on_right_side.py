"""

Inputs
    arr (List[int]): values in unknown order
Outputs:
    List[int]: greatest value to the right of each position

Goal:
    - find the greatest value to the right of each position
    - for the latest index there is no greater, so use -1

Case
    Single Value
    MultiValue (General)

Strategy
    
    Manual

        - at each positon track the max for the subarray to the right
        - if we iterate through the array backwards we can track the max 
        greedily

"""


class Solution:
    def replaceElements(self, arr: List[int]) -> List[int]:
        """Backwards iterations with max tracking

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        n: int = len(arr)
        ans = [-1 for ix in range(n)]
        
        # Single element
        if n == 1:
            return ans
        
        max_val = arr[-1]
        
        # Iterate through the array backwards
        for ix in range(n-2, -1, -1):
            
            # Save the max value from previous subarray
            ans[ix] = max_val
            
            # Track the max value for the current subarray
            max_val = max(max_val, arr[ix])
                        
        return ans