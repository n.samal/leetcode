"""
Strategy

    Iterate & Check

        - iterate through all elements in the array
        - for the first element check it against the lower bound
        - for the last element check it against the upper bound
        - for any general element check it against it's neighbor

            - if there's a jump larger than 1, we're missing a range

            ie: arr[ix] - 

        Time Complexity
            O(n)
        Space Complexity
            O(n)
"""

from typing import List


class Solution:
    
    def findMissingRanges(self, arr: List[int], lower: int, upper: int) -> List[str]:

        # Range of missing values
        missing = []
        n = len(arr)

        # For null set
        # ie: []
        if n == 0:
            if lower == upper:
                return [str(lower)]
            else:
                return [f'{lower}->{upper}']

        for ix, val in enumerate(arr):

            # Check against lower bound
            if ix == 0 and val != lower:

                # Compute distance from lower
                # ie: [3, 4, 5], lower = 0
                delta = val - lower

                # Only missing a single value
                # ie: [1, 4, 5], lower = 0
                # ie: [2, 4, 5], lower = 1
                # ie: [3, 4, 5], lower = 2
                if delta == 1:
                    missing_i = f"{lower}"

                # Missing a range
                # ie: [3, 4, 5], lower = 2
                else:
                    missing_i = f"{lower}->{val - 1}"

                # Add the missing value
                missing.append(missing_i)

            # Compare neighbors
            # look for large jumps
            if ix > 0 and arr[ix] - arr[ix - 1] > 1:

                # How big is the gap
                delta = arr[ix] - arr[ix - 1]

                # Missing single value
                # [4, 5, 7]
                if delta == 2:
                    # '6'
                    missing_i = f"{arr[ix] - 1}"

                # Missing a range
                # [4, 5, 9]
                else:

                    # Missing values, 6 to 8
                    missing_i = f"{arr[ix - 1] + 1}->{arr[ix] - 1}"

                # Add the missing value
                missing.append(missing_i)

            # Check against upper bound
            if ix == n - 1 and val != upper:

                # Compute distance from lower
                delta = upper - arr[ix]

                # Only missing a single value
                # ie: [3, 4, 5], upper = 6
                if delta == 1:
                    missing_i = f"{upper}"

                # Missing a range
                # ie: [1, 4, 5], upper = 6
                # ie: [2, 4, 5], upper = 7
                # ie: [2, 4, 5], upper = 8
                else:
                    missing_i = f"{val + 1}->{upper}"

                # Add the missing value
                missing.append(missing_i)

        return missing
    
    def findMissingRanges2(self, arr: List[int], lower: int, upper: int) -> List[str]:
        """Moved checks outside of for loop

        Time Complexity
            O(n)

        Notes
            - significant speed improvement
        """
        # Range of missing values
        missing = []
        n = len(arr)

        # For null set
        # ie: []
        if n == 0:
            if lower == upper:
                return [str(lower)]
            else:
                return [f'{lower}->{upper}']

        # Check against lower bound
        if arr[0] != lower:

            # Compute distance from lower
            # ie: [3, 4, 5], lower = 0
            delta = arr[0] - lower

            # Only missing a single value
            # ie: [1, 4, 5], lower = 0
            # ie: [2, 4, 5], lower = 1
            # ie: [3, 4, 5], lower = 2
            if delta == 1:
                missing_i = f"{lower}"

            # Missing a range
            # ie: [3, 4, 5], lower = 2
            else:
                missing_i = f"{lower}->{arr[0] - 1}"

            # Add the missing value
            missing.append(missing_i)

        ''' General Case '''

        for ix in range(1, n):

            # Compare neighbors
            # look for large jumps
            if arr[ix] - arr[ix - 1] > 1:

                # How big is the gap
                delta = arr[ix] - arr[ix - 1]

                # Missing single value
                # [4, 5, 7]
                if delta == 2:
                    # '6'
                    missing_i = f"{arr[ix] - 1}"

                # Missing a range
                # [4, 5, 9]
                else:

                    # Missing values, 6 to 8
                    missing_i = f"{arr[ix - 1] + 1}->{arr[ix] - 1}"

                # Add the missing value
                missing.append(missing_i)

        ''' Upper Bound '''

        # Check against upper bound
        if arr[n - 1] != upper:

            # Compute distance from lower
            delta = upper - arr[n - 1]

            # Only missing a single value
            # ie: [3, 4, 5], upper = 6
            if delta == 1:
                missing_i = f"{upper}"

            # Missing a range
            # ie: [1, 4, 5], upper = 6
            # ie: [2, 4, 5], upper = 7
            # ie: [2, 4, 5], upper = 8
            else:
                missing_i = f"{arr[n - 1] + 1}->{upper}"

            # Add the missing value
            missing.append(missing_i)

        return missing


if __name__ == "__main__":

    # Example 1
    # arr_in = [0, 1, 3, 50, 75]

    # Lower bound there, missing rest
    # arr_in = [0]

    # Upper bound there, missing rest
    # arr_in = [99]

    # Middle
    arr_in = [45]

    lower_in = 0
    upper_in = 99

    obj = Solution()
    print(obj.findMissingRanges(arr_in, lower_in, upper_in))
