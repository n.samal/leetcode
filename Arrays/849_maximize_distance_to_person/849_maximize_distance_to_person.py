"""
Inputs
    
Outputs
    
Examples


Cases
    
    1 seat to left
        [1, 0, 0, 0, 0]
    1 seat to right
        [0, 0, 0, 0, 1]
    1 seat in middle
        [0, 0, 1, 0, 0]
    many seats
        [1, 0, 1, 0, 0]
    no seats taken
        [0, 0, 0, 0, 0]

Strategy
    
    - Hand Calc

        - we sit in the middle between two people
            
            dist_i = (ix - jx) // 2

            ix   [0, 1, 2, 3, 4, 5]
            even [0, 1, 0, 0, 1, 1]
            even [0, 1, x, 0, 1, 1]
            
            4 - 1 = 3 seats
            3 seats // 2 = 1 seat apart

            ix  [0, 1, 2, 3, 4, 5]
            odd [0, 1, 0, 0, 0, 1]
            odd [0, 1, 0, x, 0, 1]
            
            sit in seat 3
            5 - 1 = 4 seats
            4 seats / 2 = 2 seats apart

        - if open to the right, then (right_bound - left most seat)

            ix  [0, 1, 2, 3, 4, 5]
            arr [0, 0, 0, 0, 0, 1]

            dist_i = seat - left_bound
            5 - 0 = 5

        - if open to the left, then (seat - left_bound)

            ix  [0, 1, 2, 3, 4, 5]
            arr [1, 0, 0, 0, 0, 0]

            dist_i = right_bound - seat
            5 - 0 = 5

    - Iterate and track seats

        - go through the array and track seats that are taken

            taken = [1, 4, 5]

        - use those bounds to determine the distance between each seat
            dist_i = (taken[ix] - taken[ix - 1]) // 2
        - use the first index to determine distance to left bound
            dist_i = taken[0] - 0

        - use the last index to determine distance to right bound

            dist_i = (n - 1) - taken[n - 1]

        Time Complexity
            O(n)
        Space Complexity
            O(n)

    - Greedy
        - in the apporach above we only compare to seats to our left
        - track the last seat we saw, and calculate the distance to it
        - for the last seat, compare it to the right bound

"""
from typing import List


class Solution:

    def maxDistToClosest(self, seats: List[int]) -> int:
        """Track the person to the left

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """
        max_dist: int = 0
        last_seat = None

        for ix, val in enumerate(seats):

            # Seat is taken
            if val == 1:

                # No seat to the left yet
                # ie: 2 - 0 = 2
                if last_seat is None:
                    dist_i = ix

                # Compare to last taken seat
                else:
                    dist_i = (ix - last_seat) // 2

                # Update our distance
                max_dist = max(max_dist, dist_i)

                # Update our last seat
                last_seat = ix

        # Compute distance from right bound to last seat
        if last_seat is not None:
            dist_i = (ix - last_seat)

            # Update our distance
            max_dist = max(max_dist, dist_i)
            return max_dist

        # No seats taken
        else:
            return len(seats)


if __name__ == '__main__':

    # Example
    seats_in = [1, 0, 0, 0, 1, 0, 1]

    # Right open
    seats_in = [1, 0, 0, 0]

    obj = Solution()
    print(obj.maxDistToClosest(seats_in))
