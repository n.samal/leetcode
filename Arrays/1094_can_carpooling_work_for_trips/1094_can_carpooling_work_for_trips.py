"""
Inputs
    trips (List[List[int]]): trip requirements
    capacity (int): vehicle capacity 
Outputs
    bool: can we pick up and drop off all passengers
Notes
    - car is driving east and only east
    - we're given a list of trips
        number of passengers, start location, end location
    - we drop off the passengers at the end location

    - car has a given capacity

Examples

    Example 1:

        Input: trips = [[2,1,5],[3,3,7]], capacity = 4
        Output: false

    Example 2:

        Input: trips = [[2,1,5],[3,3,7]], capacity = 5
        Output: true

    Example 3:

        Input: trips = [[2,1,5],[3,5,7]], capacity = 3
        Output: true

    Example 4:

        Input: trips = [[3,2,7],[3,7,9],[8,3,9]], capacity = 11
        Output: true
"""

class Solution:

    def save_location(self, trips: List[List[int]], capacity: int) -> bool:
        """

        - track the number of people at each location
        - for each trip add the number of people from
        the starting position to right before the ending location

        Time Complexity
            O(n*r)

            n = number of people
            r = range of trip

        Space Complexity
            O(1)
        """

        max_dist = float('-inf')

        for persons, start, stop in trips:
            max_dist = max(max_dist, stop)

        # Store the amount of people at each location
        people = [0 for _ in range(max_dist + 1)]

        # For each trip, add the people requirement to each time slot
        for persons, start, stop in trips:

            for ix in range(start, stop):
                people[ix] += persons

                if people[ix] > capacity:
                    return False

        return True


    def flux_sort(self, trips: List[List[int]], capacity: int) -> bool:
        """

        - track the infux/outflux of passengers at each location
        - sort the fluxes into the system by location and direction
        - then aggregate the fluxes to determine the number 
        of current passengers

        Time Complexity
            O(n*log(n))
        Space Complexity
            O(n)
        """

        deltas = []
        
        # Aggregate influx and outflux of passengers
        for n_pass, start, stop in trips:
            
            deltas.append((start, n_pass))
            deltas.append((stop, -n_pass))
        
        # Organize the deltas by location and flux
        # we want negative fluxes first
        deltas = sorted(deltas)
        
        # print(deltas)
        
        # Combine capacities
        cur_capacity: int = 0
        
        for loc, delta in deltas:
            
            cur_capacity += delta
            
            if cur_capacity > capacity:
                return False
        
        return True



    def carPooling(self, trips: List[List[int]], capacity: int) -> bool:
        """

        - only track the change in person requirement at the critical locations
        instead of the requirement at each location
            + we'll slide the requirements from left to right.
            Essentially a cummulative sum

            change = [0, 0, 0, 1, 0, 0, 0, 4]
            car    = [0, 0, 0, 1, 1, 1, 1, 5]

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        max_dist = float('-inf')

        for persons, start, stop in trips:
            max_dist = max(max_dist, stop)

        # Store the amount of people at each location
        people = [0 for _ in range(max_dist + 1)]

        # For each trip, add the change in people
        for persons, start, stop in trips:

            people[start] += persons
            people[stop] -= persons

        # Aggregate the person needs for each time slot
        count: int = 0
        for ix in range(max_dist + 1):

            count += people[ix]

            if count > capacity:
                return False

        return True
