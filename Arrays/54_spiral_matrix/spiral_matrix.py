from typing import List


class Solution:

    def spiralOrder(self, matrix: List[List[int]]) -> List[int]:

        # Grab bounds
        n_rows = len(matrix)

        if n_rows == 0:
            return []

        n_cols = len(matrix[0])
        n_elem = n_cols * n_rows

        # Set limits for row
        row_lim_low = 0
        row_lim_up = n_rows - 1

        # Set limits for column
        col_lim_low = 0
        col_lim_up = n_cols - 1

        # Initial positions
        row_ix = 0
        col_ix = 0

        # Initial direction
        row_step = 0
        col_step = 1

        # Array
        ix = 0
        arr = []

        # While elements left
        while ix < n_elem:

            # Store value
            arr.append(matrix[row_ix][col_ix])
            # print(arr)

            # Increment index
            ix += 1

            # Hit our right limit
            if col_ix + col_step > col_lim_up:

                # Reduce limits
                row_lim_low += 1

                # Start moving down
                col_step = 0
                row_step = 1

            # Hit our left limit
            elif col_ix + col_step < col_lim_low:

                # Reduce limit
                row_lim_up -= 1

                # Start moving down
                col_step = 0
                row_step = -1

            # Hit lower limit
            elif row_ix + row_step > row_lim_up:

                # Reduce limits
                col_lim_up -= 1

                # Start moving left
                row_step = 0
                col_step = -1

            # Hit upper limit
            elif row_ix + row_step < row_lim_low:

                # Reduce limits
                col_lim_low += 1

                # Start moving right
                col_step = 1
                row_step = 0

            # Movement
            row_ix += row_step
            col_ix += col_step

        return arr



if __name__ == "__main__":

    # arr = [
    #         [1, 2, 3],
    #         [4, 5, 6],
    #         [7, 8, 9]
    #         ]

    arr = [
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 10, 11, 12]
        ]

    # Empty array
    arr = []

    # Single row
    # arr = [1, 2, 3, 4, 5]

    s = Solution()
    print(s.spiralOrder(arr))
