
/**
 * @param {number[][]} matrix
 * @return {number[]}
 */
var spiralOrder = function(matrix) {

    // Grab bounds
    let n_rows = matrix.length;

    if (n_rows == 0){
        return [];
    }
    
    let n_cols = matrix[0].length;
    let n_elem = n_cols * n_rows;

    // Set limits
    let row_lim_low = 0;
    let row_lim_up = n_rows - 1;

    // Set limits for column
    let col_lim_low = 0;
    let col_lim_up = n_cols - 1;

    // Initial positions
    let row_ix = 0;
    let col_ix = 0;

    // Initial direction
    let row_step = 0;
    let col_step = 1;

    // Initial count and array
    let ix = 0;
    let arr = [];

    // While elements left
    while (ix < n_elem){

        // Store value
        arr.push(matrix[row_ix][col_ix]);
        console.log(arr);

        // Increment index
        ix++;

        // Hit our right limit
        if (col_ix + col_step > col_lim_up){
            
            // Reduce limits
            row_lim_low += 1;

            // Start moving down
            col_step = 0;
            row_step = 1;
        }
        // Hit our left limit
        else if (col_ix + col_step < col_lim_low){

            // Reduce limit
            row_lim_up -= 1;

            // Start moving down
            col_step = 0;
            row_step = -1;

        }
        // Hit lower limit
        else if (row_ix + row_step > row_lim_up){

            // Reduce limits
            col_lim_up -= 1;

            // Start moving left
            row_step = 0;
            col_step = -1;
        }
        // Hit upper limit
        else if (row_ix + row_step < row_lim_low){

            // Reduce limits
            col_lim_low += 1;

            // Start moving right
            col_step = 1;
            row_step = 0;
        }

        // Movement
        row_ix += row_step;
        col_ix += col_step;
    }

    return arr;

}
