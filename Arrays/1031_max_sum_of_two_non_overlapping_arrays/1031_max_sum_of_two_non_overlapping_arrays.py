"""
Inputs
    arr (List[int]): non negative integers in unknown order
    L (int): length of contiguous subarray 1
    M (int): length of contiguous subarray 2
Outputs
    int: maximum sum of elements

Goals
    - find the maximum sum of elements in two NON overlapping
    contiguous subarray

Ideas

    - values are non negative, so perhaps we can use that fact

    - use a sliding window to determine the sum at the given position
        + compute sliding window for both window sizes
        + track the summation value and indice range

    - use a cummulative sum, so we can recreate subarray summations easily

    - determine the combination that works best without any overlap
        + for every window 1 position, check all window 2 positions
        while only computing the ones without overlap
        + O(n^2)

    - we know the windows don't overlap
        + Window 1 must be before Window 2
        + or Window 1 must be after Window 2

    - current window maximum occurs when

        + W1 first: Current Window 1 + max Window 2 to the right
        + W2 first: Current Window 2 + max Window 1 to the right

        alternatively we can also say

        + W1 first: max Window 1 to left + Current Window 2
        + W2 first: max Window 2 to left + Current Window 1

    - the max Window can be tracked as we iterate in one direction

        + max Window 2 to right:

            + compute window 2 at each index
            + track max from right to left

        + alternative approach is max Window 1 to left

            + compute window 1 at each index
            + track max from left to right
        
Strategy

    Brute Force

        - iterate once through the array to compute the cum sum
        - compute best array of each size, and track their bounds
        - find best combination without intersecting bounds

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)

    Simulanteous Range Summation

        - iterate once through the array to compute the cum sum
        - iterate again through the array assuming Window 1 is before Window 2
            + compute the current window 1
            + compute the current window 2
            + track the best window 1 seen so far
            + for each current window 2, add the best window 1 seen so far

        - iterate again through the array assuming Window 2 is before Window 1
            + compute the current window 1
            + compute the current window 2
            + track the best window 2 seen so far
            + for each current window 1, add the best window 2 seen so far

        Time Complexity
            O(n)
        Space Complexity
            O(n)

References:
    - https://leetcode.com/problems/maximum-sum-of-two-non-overlapping-subarrays/discuss/278723/Analysis-Maximum-Sum-of-Two-Non-Overlapping-Subarrays
    - https://leetcode.com/problems/maximum-sum-of-two-non-overlapping-subarrays/discuss/300029/Python-breaking-down-Lee215's-solution

"""

from typing import List


class Solution:

    def maxSumTwoNoOverlap(self, arr: List[int], w1: int, w2: int) -> int:
        """

        - compute the cummulative summation
        - assume w1 occurs first
            - compute the current window 1
            - track the best window 1 to the left
            - compute the current window 2
            - track the best summation = best_win1 + current_win2

        - do the same assuming w2 occurs first

        Args:
            arr (List[int]): integers in unknown order
            w1 (int): length of window 1
            w2 (int): length of window 2
        Returns:
            int: maximum summation of two non-overlapping windows
        """

        n: int = len(arr)
        win1_max: int = 0
        win2_max: int = 0
        ans: int = 0

        # Compute cummulative summation
        cumsum = [0 for ix in range(n + 1)]
        for ix in range(1, n + 1):
            cumsum[ix] = arr[ix - 1] + cumsum[ix - 1]

        # Assume Window 1 before Window 2
        # Iterate over starting positions for Window 2
        for ix in range(w1, n - w2 + 1):

            # Use cumsum for range summations
            # sum = upper exclusive - lower inclusive
            win1 = cumsum[ix] - cumsum[ix - w1]
            win2 = cumsum[ix + w2] - cumsum[ix]

            # print(f'win1: {win1}  win2: {win2}')

            # Track best so far
            win1_max = max(win1, win1_max)
            ans = max(win2 + win1_max, ans)

        # Assume Window 2 before Window 1
        # Iterate over starting positions for Window 1
        for ix in range(w2, n - w1 + 1):

            # Use cumsum for range summations
            win2 = cumsum[ix] - cumsum[ix - w2]
            win1 = cumsum[ix + w1] - cumsum[ix]

            # Track best so far
            win2_max = max(win2, win2_max)
            ans = max(win1 + win2_max, ans)

        return ans


if __name__ == '__main__':

    # Example 1
    # arr_in = [0,6,5,2,2,5,1,9,4]
    # w1_in = 1
    # w2_in = 2

    # Example 2
    arr_in = [3, 8, 1, 3, 2, 1, 8, 9, 0]
    w1_in = 3
    w2_in = 2

    # Example 3
    # arr_in = [2,1,5,6,0,9,5,0,3,8]
    # w1_in = 4
    # w2_in = 3

    obj = Solution()
    print(obj.maxSumTwoNoOverlap(arr_in, w1_in, w2_in))