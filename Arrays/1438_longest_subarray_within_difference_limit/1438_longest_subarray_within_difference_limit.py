"""
Inputs
    nums (List[int]): integers in unknown order
    limit (int): absolute difference limit
Outputs:
    int: longest non-empty subarray with differences less than limit
Goals:
    - return the size of the longest non-empty subarray such that the absolute difference between any two elements of this subarray is less than or equal to limit.

Examples

    Example 1
        Input: nums = [8,2,4,7], limit = 4
        Output: 2 
        Explanation: All subarrays are: 
        [8] with maximum absolute diff |8-8| = 0 <= 4.
        [8,2] with maximum absolute diff |8-2| = 6 > 4. 
        [8,2,4] with maximum absolute diff |8-2| = 6 > 4.
        [8,2,4,7] with maximum absolute diff |8-2| = 6 > 4.
        [2] with maximum absolute diff |2-2| = 0 <= 4.
        [2,4] with maximum absolute diff |2-4| = 2 <= 4.
        [2,4,7] with maximum absolute diff |2-7| = 5 > 4.
        [4] with maximum absolute diff |4-4| = 0 <= 4.
        [4,7] with maximum absolute diff |4-7| = 3 <= 4.
        [7] with maximum absolute diff |7-7| = 0 <= 4. 
        Therefore, the size of the longest subarray is 2.

    Example 2
        Input: nums = [10,1,2,4,7,2], limit = 5
        Output: 4 
        Explanation: The subarray [2,4,7,2] is the longest since the maximum absolute diff is |2-7| = 5 <= 5.
    Example 3

        Input: nums = [4,2,2,2,4,4,2,2], limit = 0
        Output: 3

Ideas

    - the max difference is the largest - smallest value
        + we can just track the max and min values
        and check than each value lies within the limit

        max_delta = high - low

        if max_delta < limit, everything else is in bounds

    - Alternatively we can track the next value is within our bounds using
        ie: bound_high = min + limit
            bound_low =  max - limit

        the upper bound gets smaller with smaller values
        the lower bound gets larger with larger values

    - use a sliding window to expand the array at the current position

        + expand the window while we meet the criteria
        + move to next starting position if we can't stay within the limit

    - track min and max values for current sliding window

        + just iterate => O(n^2)
        + use heaps    => O(n*log(n))
        + use deques   => O(n)

Example by Hand

    Input: nums = [8,2,4,7], limit = 4

    Current = [8]
    bounds_high = 8 + 4 = 12
    bounds_low = 8 - 4 = 4
    next value must be within those
    next = 2
        move to next starting position

    Current = [2]
    bounds_high = 2 + 4 = 6
    bounds_low = 2 - 4 = -2
    next = 4
        this is within bounds

    Current = [2, 4]
    current_min = 2
        bounds_high = 2 + 4 = 6
        bounds_low = 2 - 4 = -2
    current_max = 4
        bounds_high = 4 + 4 = 8
        bounds_low = 4 - 4 = 0

    we're limited by the smallest high_bound
    and the largest low bound
    bounds_high = min(bounds_high, current + limit)
    bounds_low = max(bounds_low, current - limit)

    bounds_high = 6
    bounds_low = 0
    next = 7
        this is outside our bounds
        stop trying to extend here

    Current = [4]

References:
    https://leetcode.com/problems/longest-continuous-subarray-with-absolute-diff-less-than-or-equal-to-limit/discuss/609708/Python-Clean-Monotonic-Queue-solution-with-detail-explanation-O(N)
    https://leetcode.com/problems/longest-continuous-subarray-with-absolute-diff-less-than-or-equal-to-limit/discuss/609771/JavaC
    https://leetcode.com/problems/longest-continuous-subarray-with-absolute-diff-less-than-or-equal-to-limit/discuss/610281/Java-Sliding-Window-and-Heaps!!!

"""

from typing import List
from collections import deque


class Solution:

    def longestSubarray(self, arr: List[int], limit: int) -> int:
        """Two loops

        - at each starting point define the bounds
        - move the tail index while values are within our bounds
        - update the bounds at each index

        Args:
            arr (List[int]): integers of unknown order
            limit: maximum absolute difference allowed
        Returns:
            int:

        Time Complexity
            O(n^2)
                create every subarray and track the best bounds
        Space Complexity
            O(1)

        Notes
            - too slow
            - use a data structure to track the current min and max values
        """

        best: int = 0
        n: int = len(arr)

        # Small numbers
        if n <= 1:
            return n

        for head_ix in range(n):

            limit_low = arr[head_ix] - limit
            limit_high = arr[head_ix] + limit

            print(f'current: {arr[head_ix]}  limits: {limit_low},{limit_high}')

            for tail_ix in range(head_ix + 1, n):

                print(f' current: {arr[tail_ix]}  limits: {limit_low},{limit_high}')

                # Check that values are within our limits
                if arr[tail_ix] > limit_high or arr[tail_ix] < limit_low:
                    break

                # Update our longest length
                best = max(tail_ix - head_ix + 1, best)

                # Update our limits
                limit_high = min(arr[tail_ix] + limit, limit_high)
                limit_low = max(arr[tail_ix] - limit, limit_low)

        return best

    def longest_via_dq(self, arr: List[int], limit: int) -> int:
        """Deque to track sliding window max and min

        - same logic as before but with deques for tracking min and max
            + indices are tracked
            + min is monotonically increasing
            + max is monotonically decreasing

        Args:
            arr (List[int]): integers of unknown order
            limit: maximum absolute difference allowed
        Returns:
            int:

        Time Complexity
            O(n)
                track min and max with sliding windows
        Space Complexity
            O(1)
        """

        best: int = 0
        tail_ix: int = 0
        n: int = len(arr)

        maxs = deque([])
        mins = deque([])

        # Small numbers
        if n <= 1:
            return n

        for ix in range(n):

            ''' Maintain dequeues '''
            # min = monotonic increasing values
            # remove smaller values
            while mins and arr[ix] <= arr[mins[-1]]:
                mins.pop()

            # max = monotonic decreasing values
            # remove larger values
            while maxs and arr[ix] >= arr[maxs[-1]]:
                maxs.pop()

            # Add current indices once we meet criteria
            mins.append(ix)
            maxs.append(ix)

            # We've over the limit, shrink our window
            # delta = maxs[0] - mins[0]
            if arr[maxs[0]] - arr[mins[0]] > limit:

                tail_ix += 1

                # Remove indices outside our window
                if mins[0] < tail_ix:
                    mins.popleft()

                if maxs[0] < tail_ix:
                    maxs.popleft()

            best = max(best, ix - tail_ix + 1)

        return best


if __name__ == "__main__":

    # Example 1
    # nums = [8, 2, 4, 7]
    # limit = 4

    # Failed
    nums = [10, 1, 2, 4, 7, 2]
    limit = 5

    obj = Solution()
    print(obj.longestSubarray(nums, limit))
    print(obj.longest_via_dq(nums, limit))
