def binary_search_og(arr: list, target: int, low: int, high: int, closest: bool=False):
    """Find the indice of the target value in the array

    Time Complexity:
        O(log(n))
    Space Complexity
        O(1)

    Args:
        arr (List[int]): sorted array of values
        target (int): value to search for
        low (int): lower index of search range (inclusive)
        high (int): upper index of search range (inclusive)
        closest (bool): find the closest index

    Returns:
      int: indice of the target value
    """

    # print('arr:', arr)
    # print(' target:', target)

    while low < high:

        mid = (low + high) // 2

        # print('low:', low, 'high:', high, 'mid:', mid)

        # Found our value
        if arr[mid] == target:
            return mid

        # Value too small
        # bump start point up
        elif arr[mid] < target:
            low = mid + 1

        # Value too large
        # bump mid point down
        else:
            high = mid - 1

    # print(' low:', low, 'low_v:', arr[low])

    if closest:
        return low
    else:
        return -1


def find_k_closest(arr: list, k: int, target: int):
    """Find the indice of the target value in the array

    Time Complexity:
        O(log(n))
    Space Complexity
        O(1)

    Args:
        arr (List[int]): sorted array of values
        target (int): value to search for
        k (int): number of closest values to grab
    Returns:
      list: values of closest integers
    """

    # Init
    k_closest = [None] * k
    k_ix = 0
    n = len(arr)

    # Use binary method to find closest value
    ix_closest = binary_search_og(arr, target, 0, n - 1, closest=True)

    # print('arr:', arr)
    # print(' target:', target)
    # print(' ix_cls:', ix_closest)
    # print(' ix_val:', arr[ix_closest])

    # Initialize pointers to each side
    ix_left = ix_closest - 1
    ix_right = ix_closest + 1

    # Save closest value
    k_closest[k_ix] = arr[ix_closest]
    k_ix += 1

    # We need closer values
    while k_ix < k:

        # No more valid values to search
        if ix_left < 0 and ix_right > n - 1:
            break

        # Must go right
        elif ix_left < 0:

            k_closest[k_ix] = arr[ix_right]
            ix_right += 1

        # Must go left
        elif ix_right > n - 1:

            k_closest[k_ix] = arr[ix_left]
            ix_left -= 1

        # Valid left and right
        else:

            left_d = arr[ix_left] - target
            right_d = arr[ix_right] - target

            # Closer left
            # We also prefer smaller values, which should also be left
            if abs(left_d) <= abs(right_d):
                k_closest[k_ix] = arr[ix_left]
                ix_left -= 1
            else:
                k_closest[k_ix] = arr[ix_right]
                ix_right += 1

        # Increment index
        k_ix += 1

    # Put in increasing order
    return sorted(k_closest)


if __name__ == '__main__':

    # Test case 1
    # print(find_k_closest([1, 2, 3, 4, 5], 3, k=4, ))
    # print(find_k_closest([1, 2, 3, 4, 5], -1, k=4, ))
    print(find_k_closest([0, 0, 1, 2, 3, 3, 4, 7, 7, 8], k=3, target=5))
