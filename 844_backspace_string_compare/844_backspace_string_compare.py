"""
Inputs

Outputs

Example

    Input: S = "ab#c", T = "ad#c"
    Output: true
    Explanation: Both S and T become "ac".

    Input: S = "ab##", T = "c#d#"
    Output: true
    Explanation: Both S and T become "".

    Input: S = "a##c", T = "#a#c"
    Output: true
    Explanation: Both S and T become "c".

Strategy

    Generate new Strings & Compare

        - Iterate through the first string, and generate a clean string
        - Iterate through the second string, and generate a clean string
        - compare the two strings character by character

            - if lengths are different then False

        Time: O(n + m)
        Space: O(n + m)

    Clean the String

        - start from front
            - Add new characters as we see them
            - if we see a backspace remove a character
            - if no characters left to remove, return blank

    Constant Space
        - start with pointers in the back
        - compare the strings character by character
        - put a priority on the backspace chars
"""


class Solution:
    def backspaceCompare(self, S: str, T: str) -> bool:
        """Clean then compare"""
        # Clean strings and check
        s_clean = self.clean_str_stack(S)
        t_clean = self.clean_str_stack(T)

        return s_clean == t_clean

    def clean_str(self, s: str):
        """Remove backspaces from character"""

        # New string
        s_new: str = ''

        for char in s:

            # Remove last character, when there's stuff to remove
            if char == '#':
                if s_new:
                    s_new = s_new[:-1]
            else:
                s_new += char

        return s_new

    def clean_str_stack(self, s: str):
        """Use a stack to hold characters"""

        stack = []

        for char in s:

            if char == '#':
                stack.pop()
            else:
                stack.append(char)

        return stack

    def clean_str_iterate(self, string: str):
        """

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        Notes
            - use a queue/stack instead to save on string building
            - can turn this into a generator, so we see valid characters
        """
        string_new = ''

        ix: int = len(string) - 1
        n_stars: int = 0

        while ix >= 0:

            # Grab consecutive stars
            if string[ix] == '#':
                n_stars += 1
                ix -= 1

            # Remove alphabetical characters
            elif n_stars > 0 and string[ix] != '#':
                n_stars -= 1
                ix -= 1

            # Alphabetical character
            else:
                string_new = string[ix] + string_new
                ix -= 1
        return string_new

    ''' Constant Space: Original '''

    def point_and_clean(self, s: str, ix: int) -> int:
        """Iterate through the string backwards

        - count the number of backspaces
        - use the backspaces on non backspace characters
            - if we hit a new set of backspaces stop
        - return the index we stop at

        Args:
            s (str): string with some backspaces
            ix (str): index in the string
        Returns:
            int: new index in the string
        """

        # Number of back spaces
        back: int = 0

        # We've got a valid index
        while ix >= 0:

            print(f'{s[:ix+1]}|{s[ix+1:]}')

            # Track number of back spaces
            # ie: #fg###
            if s[ix] == '#':

                back += 1
                ix -= 1

            # Using backspaces on actual characters
            # ie:  fg|##
            # ie: #fg|###
            elif back > 0 and s[ix] != '#':

                back -= 1
                ix -= 1

            # We've run out of backspaces and got a normal character
            # or we've hit a new set of backspaces
            # ie: #|fg###
            # ie: #a|fg##
            else:
                break

        return ix

    def constant_space1(self, S: str, T: str) -> bool:
        """Solve the problem with constant space"""

        # Iterate through strings backwards and check character by character
        n: int = len(S)
        m: int = len(T)

        # Pointers, starting at back
        ix: int = n - 1
        jx: int = m - 1

        # Characters to explore
        while ix >= 0 or jx >= 0:

            # Grab new index for comparison
            ix = self.point_and_clean(S, ix)
            jx = self.point_and_clean(T, jx)

            # Both have valid characters
            if ix >= 0 and jx >= 0:

                print(f's: {S[ix]}  t: {T[jx]}')

                # Matching characters
                if S[ix] == T[jx]:
                    ix -= 1
                    jx -= 1

                # Non matching
                else:
                    return False

            # One is a character and one is negative
            if (ix >= 0 and S[ix] != '#' and jx < 0) or (jx >= 0 and T[jx] != '#' and ix < 0):
                return False

        # Both are negative indexes, no more characters to compare
        return True

    ''' Constant Space: Cleaner '''

    def constant_space2(self, S: str, T: str) -> bool:
        """Constant space from scratch (little cleaner)

        - iterate on each string until we find a valid character
        which stays

        Time Complexity
            O(n+m)
        Space Complexity
            O(1)
        """

        s: int = len(S) - 1
        t: int = len(T) - 1

        while s >= 0 or t >= 0:

            # Find the next character
            s = self.get_next_char(S, s)
            t = self.get_next_char(T, t)

            # Both valid
            if s >= 0 and t >= 0:

                if S[s] == T[t]:
                    s -= 1
                    t -= 1
                else:
                    return False

            # One valid, one invalid
            elif (s >= 0 and t < 0) or (t >= 0 and s < 0):
                return False

        # Made it to the end of both strings
        return True

    def get_next_char(self, string: str, ix: int):
        """Find the next valid character that
        doesn't get deleted

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        n_stars: int = 0

        while ix >= 0:

            # Grab consecutive stars
            if string[ix] == '#':
                n_stars += 1
                ix -= 1

            # Remove alphabetical characters
            # until we hit a new backspace pattern
            elif n_stars > 0 and string[ix] != '#':
                n_stars -= 1
                ix -= 1

            # Alphabetical character
            else:
                break

        return ix

    ''' Constant Space: generator '''

    def constant_space_generator(self, S: str, T: str) -> bool:
        """Use a generator to serve characters"""

        # TODO: in progress

        # Iterate through strings backwards and check character by character
        n: int = len(S)
        m: int = len(T)

        # Pointers
        ix: int = n - 1
        jx: int = m - 1

        while ix >= 0 or jx >= 0:

            # Grab characters and new index
            s, ix = self.character_gen(S, ix)
            t, jx = self.character_gen(T, jx)

            # Compare characters
            if s != t:
                return False

        return True


if __name__ == "__main__":

    # Example 1
    # S = "ab#c"
    # T = "ad#c"

    # Example 2
    # S = "ab##"
    # T = "c#d#"

    # Example 3
    # S = "a##c"
    # T = "#a#c"

    # Example 4
    # for multiple ## only remove actual chars
    # S = "y#fo##f"
    # T = "y#f#o###f"

    S = "nzp#o#g"
    T = "b#nzp#o#g"

    # True
    # S = "bxj##tw"
    # T = "bxo#j##tw"

    obj = Solution()
    # print(obj.backspaceCompare(S, T))
    # print(obj.constant_space(S, T))

    print('S: ')
    print(obj.clean_str_stack(S))
    print(obj.clean_str_stack(S))
    print('T: ')
    print(obj.clean_str_stack(S))
    print(obj.clean_str_stack(S))
