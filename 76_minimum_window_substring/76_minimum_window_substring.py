"""

Inputs
    s (str): main string
    t (str): pattern string to match
Outputs
    str: substring of minimum length

Goal
    - find the minimum substring that matches all the characters in
    the pattern string

Strategy
    Sliding Window
        - create a character counter of pattern string
        - start a window at the first character
        - increase the window size character by character

            + update the character count

        - once we meet the pattern character count

            + reduce the window size on the tail end

        - stop running when we have no more characters to explore

        Time Complexity
            O(t + 2n*t)

            we go through each character in t once for the counter
            we go through each character in s twice

                + once for adding to the counter
                + once for removal
                + at each incident we can go through all the keys in t

        Space Complexity
            O(t + n)

            we are storing all main string characters
            we are also storing all pattern string characters

    Sliding Window with Counting Match

        - in the previous method we use an auxillary function to see
        if the characters counts are met. This can be time consuming

            + at each check, we go through t possible keys
            + we don't need to check all characters each time, but only
            the characters that have changed

        - instead maintain a count of how many characters we've matched from
        our pattern.

            ie: count_t = {'a': 1, 'b': 2, 'c': 4}

            + we need to match the counts for those 3 characters
            + if match all those 3, then we've match the pattern
            + update the match count when we see a character of interest
            otherwise just ignore it

        Time Complexity
            O(t + 2n)

            we go through each character in s twice
            we go through each character in t once

        Space Complexity
            O(t + n)

            we are storing all main string characters
            we are also storing all pattern string characters

"""


class Solution:
    def minWindow(self, s: str, t: str) -> str:
        """Determine the minimum substring that contains the pattern

        Args:
            s (str): main string
            t (str): pattern string to match
        Return:
            str: minimum substring with the pattern
        """

        # Create counters for the strings
        cnt_s = {}
        cnt_t = {}

        for char in t:

            if char not in cnt_t:
                cnt_t[char] = 1
            else:
                cnt_t[char] += 1

        # Length of main string
        n: int = len(s)
        head_ix: int = 0
        tail_ix: int = 0
        min_str: str = ''
        min_val = float('inf')

        # Iterate throught the main string
        while head_ix < n:

            print(f'Adding character: {s[tail_ix:head_ix + 1]}')

            char = s[head_ix]

            # Add the new character to the counter
            if char not in cnt_s:
                cnt_s[char] = 1
            else:
                cnt_s[char] += 1

            # We have the correct characters
            while self.is_count_valid(cnt_s, cnt_t) is True:

                # Calculate the subwindow length
                str_len = (head_ix - tail_ix) + 1

                # Update our minimum string
                if str_len < min_val:
                    min_val = str_len
                    min_str = s[tail_ix:head_ix + 1]

                # Remove the last tail character
                char = s[tail_ix]
                cnt_s[char] -= 1

                if cnt_s[char] <= 0:
                    del cnt_s[char]

                # Move the tail index up
                tail_ix += 1

                print(f'Removing character: {s[tail_ix:head_ix + 1]}')

            # Increment the head
            head_ix += 1

        return min_str

    def minWindow2(self, s: str, t: str) -> str:
        """Determine the minimum substring that contains the pattern

        - use python dict get methods for cleaner code

        Args:
            s (str): main string
            t (str): pattern string to match
        Return:
            str: minimum substring with the pattern
        Notes:
            - no significant change in speed
        """

        # Create counters for the strings
        cnt_s = {}
        cnt_t = {}

        for char in t:

            # If there add one, otherwise set to 0
            cnt_t[char] = cnt_t.get(char, 0) + 1

        # Length of main string
        n: int = len(s)
        head_ix: int = 0
        tail_ix: int = 0
        min_str: str = ''
        min_val = float('inf')

        # Iterate throught the main string
        while head_ix < n:

            print(f'Adding character: {s[tail_ix:head_ix + 1]}')

            # Add the new character to the counter
            char = s[head_ix]
            cnt_s[char] = cnt_s.get(char, 0) + 1

            # We have the correct characters
            while self.is_count_valid(cnt_s, cnt_t) is True:

                # Calculate the subwindow length
                str_len = (head_ix - tail_ix) + 1

                # Update our minimum string
                if str_len < min_val:
                    min_val = str_len
                    min_str = s[tail_ix:head_ix + 1]

                # Remove the last tail character
                char = s[tail_ix]
                cnt_s[char] -= 1

                if cnt_s[char] <= 0:
                    del cnt_s[char]

                # Move the tail index up
                tail_ix += 1

                print(f'Removing character: {s[tail_ix:head_ix + 1]}')

            # Increment the head
            head_ix += 1

        return min_str

    def is_count_valid(self, cnt_s, cnt_p) -> bool:
        """Is there enough characters in the string for the pattern

        Args:
            cnt_s (str): character counts in main string
            cnt_p (str): character counts in pattern
        Returns:
            bool: is there enough characters to match the pattern
        """

        # Iterate through pattern keys
        for key in cnt_p:

            # Check count against substring

            # Key not present
            if key not in cnt_s:
                return False

            # Not enough of this character
            elif cnt_s[key] < cnt_p[key]:
                return False

        return True

    def minWindow3(self, s: str, t: str) -> str:
        """Determine the minimum substring that contains the pattern

        - use python dict get methods for cleaner code
        - use match counter

        Args:
            s (str): main string
            t (str): pattern string to match
        Return:
            str: minimum substring with the pattern
        Notes:
            -
        """

        # Create counters for the strings
        cnt_s = {}
        cnt_t = {}

        for char in t:

            # If there add one, otherwise set to 0
            cnt_t[char] = cnt_t.get(char, 0) + 1

        # Minimum matches need
        match_needed: int = len(cnt_t)
        matches: int = 0

        # Length of main string
        n: int = len(s)

        # Pointers
        head_ix: int = 0
        tail_ix: int = 0

        # Minimum string
        min_str: str = ''
        min_val = float('inf')

        # Iterate throught the main string
        while head_ix < n:

            print(f'Adding character: {s[tail_ix:head_ix + 1]}')

            # Add the new character to the counter
            char = s[head_ix]
            cnt_s[char] = cnt_s.get(char, 0) + 1

            # We have relevant character, and it meets our pattern count
            # we only update the counter on exact matches!!!
            # ie: 'ABB' if we already match on 'B's and add another 'B'
            # we don't consider that an additional match
            if char in cnt_t and cnt_s[char] == cnt_t[char]:
                matches += 1

            # We have the correct matches for our keys
            while tail_ix <= head_ix and matches == match_needed:

                # Calculate the subwindow length
                str_len = (head_ix - tail_ix) + 1

                # Update our minimum string
                if str_len < min_val:
                    min_val = str_len
                    min_str = s[tail_ix:head_ix + 1]

                # Remove the last tail character
                char = s[tail_ix]
                cnt_s[char] -= 1

                # Check if we meet the match requirement still
                # if we don't update our match count
                if char in cnt_t and cnt_s[char] < cnt_t[char]:
                    matches -= 1

                # Move the tail index up
                tail_ix += 1

                print(f'Removing character: {s[tail_ix:head_ix + 1]}')

            # Increment the head
            head_ix += 1

        return min_str


if __name__ == "__main__":

    # Example window
    S = "ADOBECODEBANC"
    T = "ABC"

    obj = Solution()
    # print(obj.minWindow(S, T))
    # print(obj.minWindow2(S, T))
    print(obj.minWindow3(S, T))
