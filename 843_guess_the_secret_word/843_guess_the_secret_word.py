"""
Inputs
    wordList (List[str]): words in unknown order
    master (obj): API interface
Outputs
    None
Goals
    - find the secret word string in 10 guesses or less

Notes
    - after each guess we are told the correctness of our solution -> int
        + the integer represents the number of exact matches for position & value
    - we can only use strings from the word list as guesses  

Ideas

    - sort the word list?

        + this should hopefully align common characters

    - create a database of words that share characters at the same index

        + ie: {0: {'a': ['aaaaa', 'aabbb'], 'b': ['baaaa', 'bbbbb']}
        + use the shared characters to determine which word align with another word

    - we should track the results of each guess

        + larger return values imply better guesses
        + or we can build upon our best guesses by only continuing with 'good' words

    - it may be valuable to track which characters are correct & which are incorrect

        + with 10 guesses, it's unlikely we can determine which characters
        are good and which are bad

    - if we have no matches, then we can remove strings

        + if n_matches == 0, then remove all words that share
        characters at the same index

        this is only beneficial if there are words to remove
        if the word is very unique ie: zwzzwz, this doesn't help much

        try to pick words that are similar to other words so we can remove
        more words at a time

    - utilize a similarity index

        + count the number of common characters
            * this could be expensive, if comparing every word to every other word
            O(n^2)
        * perhaps an edit distance could be used, but also could be complex
        * use the match function as a similarity index. Summation of matches
            to other words

Cases

    matches = 0

        + no characters matched
        + we can remove words with the same letters at the same location

    matches = 1 to 5

        + use words with similar characters ie: similar match count
        + reduce our word list to use similar words

            ie: guess_prev = 'GUESS'

    matches = 6

        + we're done!

Strategies

References:
    https://docs.python.org/3/library/collections.html
    https://docs.python.org/3.7/library/stdtypes.html#set
    https://leetcode.com/problems/guess-the-word/discuss/425068/Python-Beats-99
"""

# """
# This is Master's API interface.
# You should not implement it, or speculate about its implementation
# """
# class Master:
#     def guess(self, word: str) -> int:

from collections import defaultdict
from typing import List


class Solution:

    def findSecretWord(self, wordlist: List[str], master: 'Master') -> None:
        """Find the secret word using information from the word list

        - reorder the word list, so that more 'similar'/'common' words come first

        - guess the most similar word first
        - if we guessed correct, stop
        - if guess is totally wrong, remove words that have
          the same characters at the same index
        - if the guess is partially right, we know some characters are good but
            not which characters are good

            + compare the guess string to our word bank
            + only keep words with the same match count as our guess string (just as many good characters)
            + this ensures we will have just as good of a guess
              for the next round

        Args:
            wordList (List[str]): list of strings
            master (obj): interface API

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        # Generate utility data structs
        char_dict = self.get_char_index_dict(wordlist)

        # Find words that are most similar to other words
        wordlist.sort(key=lambda s: self.get_similarity_count(s, char_dict), reverse=True)

        for ix in range(10):

            guess_str = wordlist[0]
            n_matches = master.guess(guess_str)

            # Nothing matched, remove invalid words
            if n_matches == 0:

                exclusions = set([])

                for ix, char in enumerate(guess_str):
                    for word in char_dict[ix][char]:
                        exclusions.add(word)

                wordlist = [w for w in wordlist if w not in exclusions]

            # We found the secret!
            elif n_matches == 6:
                return

            # Find words with similar character match counts
            # ie: similar words
            else:
                wordlist = [w for w in wordlist if self.get_matches(w, guess_str) == n_matches]

    ''' Utility Functions '''

    def get_matches(self, word1: str, word2: str) -> int:
        """Count the number of common characters between each word"""

        count: int = 0

        for ch1, ch2 in zip(word1, word2):
            if ch1 == ch2:
                count += 1

        return count

    def get_char_index_dict(self, wordList: List[str]) -> dict:
        """Create a dictionary of sets

        - track the words that use a specific character
        at a specific index
        - sets allow for easy checks later

        Example:

            {0:
                {'a': ['aaa', 'abb', 'acc']}
                {'b': ['bbb', 'baa', 'bcc']}
             1:
                {'a': [...]},
                {'b': [...]},
             }
        """

        n: int = len(wordList[0])

        # Create empty dictionary at each index
        db = {ix: defaultdict(set) for ix in range(n)}

        for word in wordList:
            for ix, char in enumerate(word):
                db[ix][char].add(word)

        return db

    def get_similarity_count(self, word: str, char_dict: dict) -> int:
        """Find the the number of 'similar' words

        - at each index, count the number of words that also
        have the same character placed there
        - sum the count of words to give a crude idea of 'uniqueness'
        - the count tells us the inverse of uniqueness

            + ie: how many similar words exists
            + the word with the lowest number is most unique
        """

        count: int = 0

        for ix, char in enumerate(word):
            count += len(char_dict[ix][char])

        return count


if __name__ == '__main__':

    # Test case 1
    wordlist = ["acckzz", "ccbazz", "eiowzz", "abcczz"] 

    obj = Solution()
    obj.findSecretWord(wordlist, None)

    # char_dict = obj.get_char_index_dict(wordlist)

    # for word in wordlist:
    #     print(f'word: {word}  uniqueness_inv: {obj.get_similarity_count(word, char_dict)}')
