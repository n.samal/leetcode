
def naive(arr: list, target: int) -> int:
    """Find the triplet whose sum is closest to the target value

    Time Complexity
        O(n^3)
    Space Complexity
        O(1)

    Args:
        arr (list): array of integers in unknown order
        target (int): target value of summation
    Returns:
        int: triplet sum closest to the target value
    References:
        https://leetcode.com/problems/3sum-closest/
    """

    # Init vars
    n: int = len(arr)
    min_dist = float('inf')
    min_sum: int = None

    for ix in range(0, n - 2):
        for jx in range(ix + 1, n - 1):
            for kx in range(jx + 1, n):

                # Compute current sum
                sum_i = arr[ix] + arr[jx] + arr[kx]

                # Compare against current best
                if abs(sum_i - target) < min_dist:

                    min_dist = abs(sum_i - target)
                    min_sum = sum_i

    return min_sum


def pointers(arr: list, target: int) -> int:
    """Find the triplet whose sum is closest to the target value

    Time Complexity
        O(n^2)
    Space Complexity
        O(1)

    Args:
        arr (list): array of integers in unknown order
        target (int): target value of summation
    Returns:
        int: triplet sum closest to the target value
    References:
        https://leetcode.com/problems/3sum-closest/
    """

    # Init vars
    n: int = len(arr)
    min_dist: int = float('inf')
    min_sum: int = None

    # Sort the array
    arr = sorted(arr)

    for ix in range(0, n - 2):

        # Init pointers
        left: int = ix + 1
        right: int = n - 1

        while left < right:

            # Compute current sum
            sum_i = arr[ix] + arr[left] + arr[right]

            # Compare against current best
            if abs(sum_i - target) < min_dist:

                min_dist = abs(sum_i - target)
                min_sum = sum_i

            # We found the sum, ain't getting better than this
            if sum_i == target:
                return sum_i

            # Too large
            elif sum_i > target:
                right -= 1

            # Too small
            else:
                left += 1

    return min_sum


if __name__ == '__main__':

    arr_in = [-1, 2, 1, -4]

    print(naive(arr_in, 1))
    print(pointers(arr_in, 1))
