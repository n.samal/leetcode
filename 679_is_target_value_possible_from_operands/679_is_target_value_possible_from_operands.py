"""
Inputs:
    arr (List[int]): four integers in unknown order
Outputs:
    bool: can we obtain the value 24
Goal:
    - can the value 24 be obtained through only *, /, +, -, (,) operations    
Notes
    - integers provided are only in the range 1 through 9

Ideas

    - recursively crawl all paths
        + use depth first search to find path and terminate quickly

    - start with each value
        + try every possible operation with another value
        + skip values that have already been used

    - we must try some operators twice
        + 1 / 2 is not the same as 2 / 1
        + 1 - 2 is not the same as 2 - 1
        + 1 * 2 is the same as 2 * 1

    - we can attack the problem in two pairs

        pair 1 = op(v1, v2)
        pair 2 = op(v3, v4)
        result = op(pair1, pair2)

        use all combinations to determine this




    - possibilties can cover a range of values
        - all four values are always used
        - three operands are used, however due to parenthesis the order
        or operations can be varied up


        (v1) ? (v2)    ? (v3) ? (v4)
        (v1 ? v2)      ? (v3) ? (v4)
        (v1 ? v2 ? v3) ? (v4)
        (v1 ? v2 ? v3 ? v4)

        1 then 3
            (v1) ? (v2 ? v3 ? v4)
            (v1) ? ((v2 ? v3) ? v4)
        2 pairs
            (v1 ? v2) ? (v3 ? v4)
        3 then 1 => seems to be same as 1 then 3
            (v1 ? v2 ? v3) ? v4)   
            ((v1 ? v2) ? v3) ? v4)   3 
        4 individuals
            (v1 ? v2 ? v3 ? v4)

        3 cascade: op3(op2(op1(v1, v2), v3), v4)
        2 pairs:   op1(v1, v2) op2 op3(v3, v4)

                 op2(op1(v1, v2), v3), v4)


        parenthesis a

References
    - https://docs.python.org/3/library/itertools.html
    - https://leetcode.com/problems/24-game/discuss/236842/Python-itertools-permutations-and-product
    - https://leetcode.com/problems/24-game/discuss/247025/Python-4-case-%2B-*

"""

from operator import add, sub, mul, truediv
from typing import List
import itertools


def division(x, y):
    """Overwrite division to account for zero division"""
    if y == 0:
        return float('inf')
    else:
        return truediv(x, y)


def check_target(value: float, target: float, tol: float = 1e-3):
    """Check if value is within tolerance of our target"""
    return abs(value - target) <= tol


class Solution:

    def judgePoint24(self, arr: List[int]) -> bool:
        """Apply breadth first search

        """

        operands = [add, sub, mul, division]
        target = 24

        for v1, v2, v3, v4 in itertools.permutations(arr):

            print(f'v1: {v1}  v2: {v2}  v3: {v3}  v4: {v4}')

            for op1, op2, op3 in itertools.product(operands, repeat=3):

                results = []

                # Pair & Pair
                results.append(op3(op1(v1, v2), op2(v3, v4)))

                # Pair Value Value
                results.append(op3(op2(op1(v1, v2), v3), v4))

                # Value Pair Value
                results.append(op3(op1(v1, op2(v2, v3)), v4))

                # Value Value Pair
                results.append(op3(v1, op2(v3, op1(v1, v2))))

                print(results)

                for result in results:
                    if check_target(result, target):
                        return True
        return False


if __name__ == "__main__":

    # Test
    # arr_in = [1, 2, 3, 4]

    # Example 1
    # arr_in = [4, 1, 8, 7]

    # Example 2
    # arr_in = [1, 2, 1, 2]

    # Zero
    # arr_in = [1, 2, 0, 3]

    # Failed
    # arr_in = [1, 9, 1, 2]
    # arr_in = [1, 3, 4, 6]
    arr_in = [1, 5, 9, 1]

    obj = Solution()
    print(obj.judgePoint24(arr_in))
