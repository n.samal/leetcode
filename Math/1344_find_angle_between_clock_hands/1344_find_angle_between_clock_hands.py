"""
Inputs
    hour (int): current hour
    minutes (int): current minute
Outputs
    float: smaller angle between hands (degrees)
Notes
    

Examples

    Example 1

        Input: hour = 12, minutes = 30
        Output: 165

    Example 2

        Input: hour = 3, minutes = 30
        Output: 75

    Example 3

        Input: hour = 3, minutes = 15
        Output: 7.5
Ideas

    - define vectors for the angle positions then compute
    difference between them

        + use dot product for angle
        + may take too long to define each vector

    - just use the angles defined by the clock

        + 360 degrees for circle
        + 12 positions for hour, so 360 / 12 = 30 degrees per hour
        + 60 positions for minute, so 360 / 60

    - precompute all the degree positions

        hours   = [0, 30, 60, 90, ...]
        minutes = [0, 6, 12, 18, ...]

        + use the array to gather the position of each hand
        + remember that the hour position flexes between hour
        positons

            ie: 1:15 is a fraction between 1 & 2
                1:30 is a half way between 1 & 2
"""

class Solution:
    def angleClock(self, hour: int, minutes: int) -> float:
        """

        - find angle defined by hour hand
        - find angle defined by minute hand

        - compute the difference between the two


        Time Complexity
            O(1)
        Space Complexity
            O(1)
        """

        # Define angle increment between circumferential position
        delta_hr = 360 / 12
        delta_min = 360 / 60

        # Hours: Location in degrees
        hour_locs = [0 for _ in range(12 + 1)]

        for ix in range(1, 12 + 1):
            hour_locs[ix] = hour_locs[ix - 1] + delta_hr

        # Minutes: Location in degrees
        min_locs = [0 for _ in range(60 + 1)]

        for ix in range(1, 60 + 1):
            min_locs[ix] = min_locs[ix - 1] + delta_min

        # Compute angle between the two
        angle_hour = hour_locs[hour] + ((minutes / 60) * delta_hr)
        angle_min = min_locs[minutes]

        max_angle = max(angle_hour, angle_min)
        min_angle = min(angle_hour, angle_min)

        delta = max_angle - min_angle
        delta_comp = 360 - delta

        return min(delta, delta_comp)
