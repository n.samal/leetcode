"""
Inputs
    ints: points of the rectangle
Outputs
    - find the total area of two rectilinear 2D rectangles in a plane

Notes

    (A,B) is bottom left corner of rectangle 1
    (C,D) is top right corner of rectangle 1

    (E,F) is bottom left corner of rectangle 2
    (G,H) is top right corner of rectangle 2

Examples

    Example:

        Input: A = -3, B = 0, C = 3, D = 4, E = 0, F = -1, G = 9, H = 2
        Output: 45
Cases

    - No Overlap
    - Overlap

        + Top Right in other rectangle
        + Bottom Right in other rectangle
        + Top Left in other rectangle
        + Bottom Left in other rectangle

Ideas
    
    - compute total area of both and substract overlap
    - how to determine overlap?

        + see if a corner lies within the other rectangle
        + we need to check which one of the four cases 
        then extract the overlap area

        + overlap area is defined by the min and max of
        several lines (Side edges or top/bottom edges)

            max of left most rectangle vs min of right
            or
            max of bottom most rectangle vs min of top most



"""


class Solution:
    def computeArea(self, A: int, B: int, C: int, D: int, E: int, F: int, G: int, H: int) -> int:
        """Compute overlap with min  /max

        - compute the area of both rectangles
        - calculate the overlap width and height
        using the min and max of bounding dimensions
            + if a negative width or height it's not overlapping

        Time Complexity
            O(1)
        Space Complexity
            O(1)
        """

        # total area = rect1 area + rect2 area - overlap area

        # Rectangle 1 dimensions
        width1 = C - A
        height1 = D - B
        area1 = width1 * height1

        # Rectangle 2 dimensions
        width2 = G - E
        height2 = H - F
        area2 = width2 * height2

        # Rect1 could be left, or right side
        # Intersection could occur in several ways
        # left or right corner could overlay, top or bottom corner

        # Overlap dimensions
        left_side = max(A, E)
        right_side = min(C, G)
        top_side = min(D, H)
        bottom_side = max(B, F)

        width = right_side - left_side
        height = top_side - bottom_side

        if width < 0 or height < 0:
            overlap = 0

        else:
            width = right_side - left_side
            height = top_side - bottom_side

            overlap = width * height

        return area1 + area2 - overlap