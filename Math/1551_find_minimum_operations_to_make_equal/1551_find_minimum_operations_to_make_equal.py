"""
Input:
    n (int): number of values
Outputs:
    int: minimum number of operations to make all equal
Notes
    - array is defined via function
        arr[i] = (2 * i) + 1
    - in once operation we select two indices to modify
        arr[x] -= 1
        arr[y] += 1
    - our goal to make all the elements of the array equal
    - it's guaranteed that all elements can be made equal

    - 1 <= n <= 10^4

Examples

    Example 1:

        Input: n = 3
        Output: 2
        Explanation: arr = [1, 3, 5]
        First operation choose x = 2 and y = 0, this leads arr to be [2, 3, 4]
        In the second operation choose x = 2 and y = 0 again, thus arr = [3, 3, 3].

    Example 2:

        Input: n = 6
        Output: 9

Hand Calc

    Even Count

        n = 6
        ix  = [0  1, 2, 3, 4,  5]
        arr = [1, 3, 5, 7, 9, 11]

        sum = 36
        avg = 36 / 6 = 6
        target value = 5

        right = n // 2
        left = right - 1 

        Shift 1
            5 + 1 = 6
            7 - 1 = 6

            1 round required
        Shift 2
            3 + 3 = 6
            9 - 6 = 3

            3 round required

        Shift 3
            1 + 5 = 6
            11 - 6 = 5

            5 rounds required

        9 rounds required

    Odd Count

        n = 7
        ix  = [0  1, 2, 3, 4,  5,  6]
        arr = [1, 3, 5, 7, 9, 11, 13]

        mid = n // 2

        sum = 49
        avg = 49 / 7 = 7
        target value = 7

        no need to modify the mid point
        we run from 0 to n // 2 exclusive

        Shift 1
            9 - 7 = 2
            7 - 5 = 2

        Shift 2 
            11 - 7 = 4
            7 - 3 = 4

        Shift 3
            13 - 7 = 6
            7 - 1 = 6

Ideas

    - first determine the goal value
        + likely the mid point
        + may differ between even and odd values

    - even number
        + target = average = sum / n
        + need to modify all values
    - odd number
        + target = average
        + mid point is already our target value

    - we may be able to calculate our mid point
        as the average between left & right midpoints
        for the even case

    - from the mid point move left and right
        + calculate the number of turns to equalize the value

    - appears we only need to check one side

"""
class Solution:
    def original(self, n: int) -> int:
        """

        - iterate over the array to determine the goal
        value

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        if n == 1:
            return 0

        sum_ = sum([2*i + 1 for i in range(n)])
        goal = sum_ // n

        count: int = 0

        for ix in range(n // 2):
            count += goal - ((2*ix) + 1)

        return count

    def minOperations(self, n: int) -> int:
        """

        - use midpoints to calculate the goal value

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        if n == 1:
            return 0

        if n % 2 == 0:
            mid_r = n // 2
            mid_l = mid_r - 1

            goal_l = (2*mid_l) + 1
            goal_r = (2*mid_r) + 1
            goal = (goal_l + goal_r) // 2

        else:
            mid_ix = n // 2
            goal = 2*(mid_ix) + 1

        count: int = 0

        for ix in range(n // 2):
            count += goal - ((2*ix) + 1)

        return count
