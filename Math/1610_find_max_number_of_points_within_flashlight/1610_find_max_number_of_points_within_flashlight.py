"""
Inputs
    points (List[List[int]]): other points 
    angle (int): search angle in degrees
    location (List[int]): xy location of person
Outputs
    int: max number of points in view

Notes
    - initially we are facing east and can see
    points within out field of view
    - we cannot move, but can rotate from the location
    - what's the max number of points we can see?

    - the field of view is inclusive
        +  d - angle/2, d + angle/2

Ideas

    - compute the angle for each point relative to the person

        + theta = atan(dy/dx)
        + use atan2 to make angle calcs easier

    - then for each rotation angle check if the theta lies within

        direction + angle / 2
        direction - angle / 2

    - how to handle points that are ontop of the current location

        + just check if the point is the same, or distance is within
        a threshold and then skip over this for angle counts

    - handling positive and negative angles

        + atan2 brings points within -pi to pi
        https://en.wikipedia.org/wiki/Atan2

        + the issue arises when comparing points in the same location
        but appear to be different from their angles

        0 and 360 are next to each other, but from a delta basis are 360 apart

        + pushing all negatives in the 0 to 360 range doesn't solve this
        360 - 0 = 360 still doesn't help

            * instead we'll push duplicates of the same point

            point = -45
            point_mirror = -45 + 360 = 315

            og    dupl
            -135  225
            135   495
            
            instead of comparing 135 to -135 = 270
            we do 225 - 135 = 90

            the higher point may or may not have pairs for comparison

"""


class Solution:
    def visiblePoints(self, points: List[List[int]], angle: int, location: List[int]) -> int:
        """Rotate in a circle

        - calculate angles for all points relative to the location
        - try every heading for the location, and see which points are within the window

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        Notes
            - works for many cases but not all
            - issues with angle = 0 currently
        """
        
        # Compute angles for each point relative to person
        # and number of overlapping points
        angles = []
        overlap = 0

        for x, y in points:

            dx = x - location[0]
            dy = y - location[1]
            dist = sqrt(dx**2 + dy**2)

            # Track overlapping points
            # Prevent double count
            if dist == 0.0:
                overlap += 1
                continue

            # 360 = 2pi rad
            # 180 = 1pi rad
            theta = math.atan2(dy, dx)
            theta = theta * 180 / math.pi
            
            angles.append(theta)
            angles.append(theta + 360)

            # print(f'dx: {dx}   dy: {dy}   theta: {theta}')

        # Try every rotation
        max_count = 0

        # todo: may need to go from -180 to 180 + 360
        for heading in range(0, 360):

            count = overlap

            for theta in angles:

                if heading - (angle / 2) <= theta <= heading + (angle / 2):
                    count += 1

            max_count = max(max_count, count)

        return max_count


    def sliding_window(self, points: List[List[int]], angle: int, location: List[int]) -> int:
        """Rotate in a circle

        - calculate angles for all points relative to the location
        - sort angles, then use a sliding window
            + add points while within our window

        Time Complexity
            O(n*log(n))
        Space Complexity
            O(n)
        """
        
        # Compute angles for each point relative to person
        # and number of overlapping points
        angles = []
        overlap = 0

        for x, y in points:

            dx = x - location[0]
            dy = y - location[1]
            dist = sqrt(dx**2 + dy**2)

            # Track overlapping points
            # Prevent double count
            if dist == 0.0:
                overlap += 1
                continue

            # 360 = 2pi rad
            # 180 = 1pi rad
            theta = math.atan2(dy, dx)
            theta = theta * 180 / math.pi

            # Add value along with positive twin
            angles.append(theta)
            angles.append(theta + 360)

            print(f'dx: {dx}   dy: {dy}   theta: {theta}')

        # Count points within our window
        angles = sorted(angles)
        n = len(angles)
        max_count = 0

        # Use a caterpillar approach to find valid points
        # slide right while angles are good
        # slide left to remove invalid angles
        l = 0
        r = 0

        while l < n:

            while r < n and angles[r] - angles[l] <= angle:
                r += 1

            max_count = max(max_count, r - l)

            # Bump left
            l += 1

        return max_count + overlap

