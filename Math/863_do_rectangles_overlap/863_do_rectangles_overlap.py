"""
Inputs
    rec1 (List[int]): xy coordinates for rectangle 1
    rec2 (List[int]): xy coordinates for rectangle 2
Outputs
    bool: do rectangles overlap?

Notes
    - only two points provided per rectangle
        + bottom left corner (x, y)
        + top right corner (x, y)

    - if rectangles only touch that doesn't not mean
    a overlap

    - if the area of intersection is positive 

Ideas

    - if one of the corners is within a rectangle then
    we have an overlap

       x low < x < x high
       y low < y < y high

       ______  .
       |     |
       |   . |
       |_____|

    - we have to check whether rec1 is in rec2 and vice versa

        ______  .
        |     |
        |     |
        |_____|
       .

        + here rec2 is outside of rec1 but rec1 is inside rec1

Cases

    Overlap does not occur in 4 cases
        - left
        - right
        - up
        - down

    Rectangle 2 to the left

        | 2 | | 1 |

        occurs when rect 2 x_max < rect 1 x_min

    Rectangle 2 to the right

        | 1 | | 2 |

        occurs when rect 2 x_min > rect 1 x_max

    Rectangle 2 above

        | 2 |
        | 1 |

        occurs when rect 2 y_min > rect 1 y_max

    Rectangle 2 below

        | 1 |
        | 2 |

        occurs when rect 2 y_max < rect 1 y_min
"""

from typing import List


class Solution:
    def isRectangleOverlap(self, rec1: List[int], rec2: List[int]) -> bool:
        """Check for the inverse

        - if a rectangle has no area then no overlap
        - if the rectangles are not near eachother
        then no overlap
            + left, right up or down

        Time Complexity
            O(1)
        Space Complexity
            O(1)
        """

        # Unpack for ease
        # Bottom lower power and upper point
        x_min1, y_min1, x_max1, y_max1 = rec1
        x_min2, y_min2, x_max2, y_max2 = rec2

        # Check for empty area rectangles
        if x_max1 - x_min1 == 0 or x_max2 - x_min2 == 0:
            return False
        elif y_max1 - y_min1 == 0 or y_max2 - y_min2 == 0:
            return False

        # Rectangle 2 to the left
        if x_max2 <= x_min1:
            return False

        # Rectangle 2 to the right
        elif x_min2 >= x_max1:
            return False

        # Rectangle 2 is above
        elif y_min2 >= y_max1:
            return False

        # Rectangle 2 is below
        elif y_max2 <= y_min1:
            return False

        else:
            return True

    ''' Check if point is within bounding box (NOT WORKING)'''

    def bounding_box(self, rec1: List[int], rec2: List[int]) -> bool:
        """Check if a rectangle point lands within the bounding box
        of the other rectangle

        Notes
            - cases that touch fail here
             (0,0) (1, 1)
             (1,0) (2, 1)
            
            - utilize the methodology above, it's easier
            to check for the inverse, rather than if within the 
            bounding box
        """

        # Unpack for ease
        # Bottom lower power and upper point
        x_min1, y_min1, x_max1, y_max1 = rec1
        x_min2, y_min2, x_max2, y_max2 = rec2

        # Check for empty area rectangles
        if x_max1 - x_min1 == 0 or x_max2 - x_min2 == 0:
            return False
        elif y_max1 - y_min1 == 0 or y_max2 - y_min2 == 0:
            return False

        # Does the second rectangle lie within in first rectangle at all
        # has to lie within in x and in y
        # or vice versa
        check1 = self.check_overlap(rec1, rec2)
        check2 = self.check_overlap(rec2, rec1)

        if check1 or check2:
            return True
        else:
            return False

    def check_overlap(self, rec1, rec2):

        # Bottom lower power and upper point
        x_min1, y_min1, x_max1, y_max1 = rec1
        x_min2, y_min2, x_max2, y_max2 = rec2

        # Check first corner for x overlap then y overlap
        if x_min1 <= x_min2 <= x_max1:
            if y_min1 <= y_min2 <= y_max1:
                return True

        # Second point check
        if x_min1 <= x_max2 <= x_max1:
            if y_min1 <= y_max2 <= y_max1:
                return True

        return False


if __name__ == '__main__':

    # Case
    rec1 = [0, 0, 1, 1]
    rec2 = [1, 0, 2, 1]

    # Example
    # rec1 = [-1, 0, 1, 1]
    # rec2 = [0, -1, 0, 1]

    obj = Solution()
    # print(obj.isRectangleOverlap(rec1, rec2))
    print(obj.bounding_box(rec1, rec2))
