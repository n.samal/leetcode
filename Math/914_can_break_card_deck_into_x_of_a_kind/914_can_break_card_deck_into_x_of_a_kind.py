"""
Inputs
    deck (List[int]): 
Outputs
    bool: can the deck be split into groups of X cards

Notes

    - Each group has exactly X cards.
    - All the cards in each group have the same integer.

Ideas
    - count the number of each card
    - the minimum card count is our limiting size

        + if we have 4 Jacks, then our group size is at most 4

    - in some cases we need to break into smaller groups than our limiting size

        ie: [1,1,1,1,2,2,2,2,2,2]

        we should use the greatest common divisor between all the counts

Examples

    Example 1:

        Input: deck = [1,2,3,4,4,3,2,1]
        Output: true
        Explanation: Possible partition [1,1],[2,2],[3,3],[4,4].

    Example 2:

        Input: deck = [1,1,1,2,2,2,3,3]
        Output: false´
        Explanation: No possible partition.

    Example 3:

        Input: deck = [1]
        Output: false
        Explanation: No possible partition.

    Example 4:

        Input: deck = [1,1]
        Output: true
        Explanation: Possible partition [1,1].

    Example 5:

        Input: deck = [1,1,2,2,2,2]
        Output: true
        Explanation: Possible partition [1,1],[2,2],[2,2].

"""

from collections import defaultdict
import math

class Solution:
    def hasGroupsSizeX(self, deck: List[int]) -> bool:
        """Find the greatest common divisor between all card counts

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        # Can we split the deck into equivalent counts
        n: int = len(deck)

        if n == 1:
            return False

        # Get counts for each card
        dct = defaultdict(int)
        min_cnt = float('inf')

        for card in deck:
            dct[card] += 1

        # Get the minimum count for the card
        for key in dct:
            min_cnt = min(min_cnt, dct[key])

        # Can we split the every card into groups of the limiting card
        # We can also try smaller groups if that doesn't work
        # Find the greatest common divisor for every card
        gcd = min_cnt

        for key in dct:

            gcd = math.gcd(dct[key], gcd)

        return gcd >= 2
