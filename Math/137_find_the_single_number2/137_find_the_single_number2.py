"""
Goal
    Given a non-empty array of integers, every element appears three times except for one, which appears exactly once. Find that single one.
"""

class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        """Math based
        
        - same strategy as single number 1
        
        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """
        
        # total_sum = (unique_sum - x) + (unique_sum - x) + unique
        # x is only included once since it only appears once
        # all other values appears in the unique sum 3 times
        
        total_sum = sum(nums)
        uniq_sum = sum(set(nums))
        
        
        # total_sum = 3*uniq_sum - 2x
        # x = (3*unique_sum - total_sum) / 2
        return (3 * uniq_sum - total_sum) // 2
