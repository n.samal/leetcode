"""
Inputs
    num (int): non negative integer
Outputs
    int: summation of digits

Notes
    - Repeatedly add all its digits until the result has only one digit.

Example:

    Example 1:

        Input: 38
        Output: 2 
        Explanation: The process is like: 3 + 8 = 11, 1 + 1 = 2. 
                     Since 2 has only one digit, return it.
References:
    https://en.wikipedia.org/wiki/Digital_root
"""

class Solution:
    def addDigits(self, num: int) -> int:
        """

        - continue to break down digits by dividing by 10
            
            value: 38
                div 10 = 3
                mod 10 = 8
            value: 3
                div 10 = 0
                mod 10 = 3

        - track the summation of digits, and stop once below
        our threshold

        Time Complexity
            O(log(num)?)
        Space Complexity
            O(1)

        """


        while num >= 10:

            # Break digit down
            num_new = 0

            while num:

                div, mod = divmod(num, 10)

                num_new += mod
                num = div

            # Propogate new number
            num = num_new

        return num


obj = Solution()
obj.addDigits(38)
