class Solution:
    def set_based(self, arr: List[int]) -> int:
        """Find the odd main odd
        
        - maintain a set of values
            + if we see a matching pair pop it
            + if new value add it
        
        - the remaining value is our odd man out
        
        Time Complexity
            O(n)
        Space Complexity
            O(~n)
        """
        
        s = set([])
        
        for val in arr:
            
            # Is not there
            if val not in s:
                s.add(val)
                
            # We have a pair
            else:
                s.remove(val)
        
        return s.pop()
    
    def math_based(self, arr: List[int]) -> int:
        """Use algebra to calculate the missing value
        
        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """
        
        total_sum = sum(arr)
        uniq_sum = sum(set(arr))

        # Our total sum should be equal to 
        # total_sum = x + 2(v1) + 2(v2) ... + 2(vn)
        # total_sum = x + 2(v1 + v2 + .. vn)
        # uniq_sum = (v1 + v2 + .. vn) + x
        # rewrite total sum using unique sum
        # total_sum = uniq_sum + (uniq_sum - x)
        # solve for x
        
        return 2 * uniq_sum - total_sum
