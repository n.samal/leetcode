"""
Inputs
    nums (List[int]): 
Outputs
    List[int]: 

Notes
    - find all elements that appear more than n/3 times
    - algorithm should be O(1) space, O(n) time

Ideas

    - depending on the the appearance limit there
    can only be a few of each number
        + ie: 1 number can only appear more than 50% of the time
        + ie: 2 numbers can only appears more than 33% of the time

    - if we have extra space just track the counts of all numbers

    - if limited space, then just track the count for those numbers
    that might work

    - we could use the +- approach
        + reduce counts at every step,
        those numbers that show up frequently will survive
        all others will dissipate

References:
    - https://en.wikipedia.org/wiki/Boyer–Moore_majority_vote_algorithm
    - https://algorithms.tutorialhorizon.com/majority-element-boyer-moore-majority-vote-algorithm/
"""

class Solution(object):

    def boyer_moore(self, arr):
        """Boyer Moore majority vote algorithm

        - iterate over the array
        - track counts of two numbers as potential candidates
        for the majority
        - if we see the number, increment the counts
        - if we don't see the number then decrement the counts
        - once the counts get to 0, consider some other numbers as candidates

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        n = len(arr)

        count1: int = 0
        count2: int = 0

        # Starting candidates
        cand1: int = None
        cand2: int = None

        for v in arr:

            # Prior candidates
            if v == cand1:
                count1 += 1
            elif v == cand2:
                count2 += 1

            # New candidates
            elif count1 == 0:
                cand1 = v
                count1 = 1

            elif count2 == 0:
                cand2 = v
                count2 = 1

            # Neither candidate matched, reduce the counts
            else:
                count1 -= 1
                count2 -= 1

        count1 = 0
        count2 = 0
        ans = []

        # Verify counts
        for v in arr:

            if v == cand1:
                count1 += 1

            elif v == cand2:
                count2 += 1

        if count1 > n / 3:
            ans.append(cand1)

        if count2 > n / 3:
            ans.append(cand2)

        return ans


if __name__ == '__main__':

    # Example 1
    arr = [2, 2]

    obj = Solution()
    print(obj.boyer_moore(arr))
