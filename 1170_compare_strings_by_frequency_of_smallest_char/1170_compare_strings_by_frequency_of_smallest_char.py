"""
Inputs
    queries (List[str]): array of strings
    words (List[str]): array of strings
Outputs
    List[str]: number of words such that 

Notes
    f(s) = frequency of the smallest character in a string s

Example
    Example 1
        Input: queries = ["cbd"], words = ["zaaaz"]
        Output: [1]
        Explanation: On the first query we have f("cbd") = 1, f("zaaaz") = 3 so f("cbd") < f("zaaaz").

    Example 2

        Input: queries = ["bbb","cc"], words = ["a","aa","aaa","aaaa"]
        Output: [1,2]
        Explanation: On the first query only f("bbb") < f("aaaa"). 
        On the second query both f("aaa") and f("aaaa") are both > f("cc").

"""

import bisect
from typing import List


class Solution:

    def numSmallerByFrequency(self, queries: List[str], words: List[str]) -> List[int]:
        """

        - grab min freq for each character
        - grab min freq for current query
            + compare this against each word
            and gather the counts

        Time Complexity
            O(q*w)  go through every word for each query

        Space Complexity
            O(w)  storing min char frequency of each word
        """

        # Get frequency of each word
        words_frq = [self.get_frequency_of_min_char(word) for word in words]

        # Sort it for easy comparions on count
        words_frq.sort(reverse=True)

        print(f'words_frq: {words_frq}')

        answer = [None for _ in queries]

        for ix, query in enumerate(queries):

            q_min = self.get_frequency_of_min_char(query)

            print(f'query: {q_min}')

            count = 0

            for frq in words_frq:
                print(f' word: {frq}')

                if q_min < frq:
                    count += 1
                else:
                    break

            # Determine placement in frequency array
            # ie: offset to find count larger than us
            # count = bisect.bisect(words_frq, q_min)
            # answer[ix] = count - n

            answer[ix] = count

        return answer

    def numSmallerByFrequency2(self, queries: List[str], words: List[str]) -> List[int]:
        """Use binary search to speed up the counting process

        Time Complexity
            O(q*ln(w))  for each query using binary search to find count

        Space Complexity
            O(w)  storing min char frequency of each word
        """

        n: int = len(words)

        # Get frequency of each word
        words_frq = [self.get_frequency_of_min_char(word) for word in words]

        # Sort it for easy comparions on count
        words_frq.sort()

        print(f'words_frq: {words_frq}')

        answer = [None for _ in queries]

        for ix, query in enumerate(queries):

            q_min = self.get_frequency_of_min_char(query)

            print(f'query: {q_min}')

            # Determine placement in frequency array
            # ie: offset to find count larger than us
            count = bisect.bisect(words_frq, q_min)
            answer[ix] = n - count

        return answer

    def get_frequency_of_min_char(self, string: str) -> int:
        """Find the frequency of the minimum character"""

        # Key of minimum letter
        min_s = float('inf')
        dct = {}

        for char in string:

            # Compute integer for easy minimum calc
            key = ord(char) - ord('a')
            min_s = min(key, min_s)

            dct[key] = dct.get(key, 0) + 1

        return dct[min_s]


if __name__ == '__main__':

    # Example 1
    # queries = ["cbd"]
    # words = ["zaaaz"]

    # Example 2
    queries = ["bbb","cc"]
    words = ["a","aa","aaa","aaaa"]

    obj = Solution()
    print(obj.numSmallerByFrequency(queries, words))
    print(obj.numSmallerByFrequency2(queries, words))
