"""

Inputs
    S (str): original string
    indexes (List[int]): indices of potential swaps
    sources (List[str]): patterns
    targets (List[str]): strings to replace for pattern 
Output
    str: the updated string

Goal:
    - replace patterns found in the original string with the target value

Cases

    Null
        - no indexes
    Unordered
        - indexes out of ideal order

    Unreal match
        - source string is larger than what's left in the main string
        (python handles this already, no need to implement check)

Strategy

    Manual

        - create a new empty string
        - iterate through the original string until we find a source index
            + add characters that aren't in our index list. We don't care to swap these
        - if we have a source index, check if the substring matches our string

            + if the whole source substring matches

                * add the target value
                * move to the next position in our main string, after the source substring ends
            + if no match

                * add the original source string, and move the index past that
"""

from typing import List


class Solution:
    def findReplaceString(
        self, S: str, indexes: List[int], sources: List[str], targets: List[str]
    ) -> str:

        # If no indexes to search
        if not indexes:
            return S

        s_new: str = ""  # new string
        n: int = len(S)  # number of main string characters
        n_targets: int = len(targets)  # number of targets

        # Determine the indexes that would sort our source indexes
        indexes_srt = sorted(range(n_targets), key=lambda ix: indexes[ix])

        # Rearrange indexes, sources, targets in left to right order
        indexes = [indexes[ix] for ix in indexes_srt]
        sources = [sources[ix] for ix in indexes_srt]
        targets = [targets[ix] for ix in indexes_srt]

        ix: int = 0  # main string index
        jx: int = 0  # indexes index

        # Iterate through the string
        while ix < n:

            # No more valid targets, add the rest of the string
            if jx >= n_targets:
                s_new += S[ix:]
                break

            # Not the relevant source index we're looking for
            # add the character and move on
            if ix != indexes[jx]:
                s_new += S[ix]
                ix += 1

            # Potential source string
            else:

                m = len(sources[jx])

                # Check if the string matches the source
                if S[ix : ix + m] == sources[jx]:

                    # Add the target string
                    s_new += targets[jx]

                    # Move the left pointer past our source
                    ix += m

                # No match found
                else:

                    # Add the normal character
                    s_new += S[ix]
                    ix += 1

                # Move to the next potential match
                jx += 1

        return s_new

    def findReplaceString2(
        self, S: str, indexes: List[int], sources: List[str], targets: List[str]
    ) -> str:
        """Determine that indexes that would sort our indexes, then 
        use those directly for our comparisons

        Notes
            - improved speed, but more confusing
        """

        # If no indexes to search
        if not indexes:
            return S

        s_new: str = ""  # new string
        n: int = len(S)  # number of main string characters
        n_targets: int = len(targets)  # number of targets

        # Determine the indexes that would sort our source indexes
        indexes_srt = sorted(range(n_targets), key=lambda ix: indexes[ix])

        ix: int = 0  # main string index
        jx: int = 0  # indexes index

        # Iterate through the string
        while ix < n:

            # No more valid targets, add the rest of the string
            if jx >= n_targets:
                s_new += S[ix:]
                break

            # Search index to use
            search_jx = indexes_srt[jx]  # index in original source/target

            # Not the relevant source index we're looking for
            # add the character and move on
            if ix != indexes[search_jx]:
                s_new += S[ix]
                ix += 1

            # Potential source string
            else:

                m = len(sources[search_jx])

                # Check if the string matches the source
                if S[ix : ix + m] == sources[search_jx]:

                    # Add the target string
                    s_new += targets[search_jx]

                    # Move the left pointer past our source
                    ix += m

                # No match found
                else:

                    # Add the normal character
                    s_new += S[ix]
                    ix += 1

                # Move to the next potential match
                jx += 1

        return s_new


    def find_replace_dict(self, string: str, indexes: List[int], sources: List[str], targets: List[str]) -> str:
        """
        
        - iterate through characters in the original string
        - try replacement the characters if a switch is necessary
            + if a match, then we replacment and move our pointer
            after the src ends
            + otherwise continue down the normal path
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        string_new = []
        n: int = len(string)
        
        # Compile for ease
        dct = {}
        
        for ix, src, tgt in zip(indexes, sources, targets):
            dct[ix] = (src, tgt)
        
        ix: int = 0
        
        while ix < n:
            
            # print(f'{string[ix]}')
            
            if ix in dct:

                src, tgt = dct[ix]
                
                # If replacement works, then replace source
                if string[ix:ix + len(src)] == src:
                    string_new.append(tgt)
                    ix += len(src)
                
                # Otherwise use the other character
                else:
                    string_new.append(string[ix])
                    ix += 1
            
            # Add old character
            else:
                string_new.append(string[ix])
                ix += 1
            
        return ''.join(string_new)


if __name__ == "__main__":

    # Basic case, 1 match
    # S = "abcd"
    # indexes = [0, 2]
    # sources = ["ab", "ec"]
    # targets = ["eee", "ffff"]

    # Indexes not in sorted order
    S = "vmokgggqzp"
    indexes = [3, 5, 1]
    sources = ["kg", "ggq", "mo"]
    targets = ["s", "so", "bfr"]

    obj = Solution()
    print(obj.findReplaceString(S, indexes, sources, targets))
    print(obj.findReplaceString2(S, indexes, sources, targets))
