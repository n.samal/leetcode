"""

Inputs
    arr (List[int]): numbers
    target (int): target summation
Outputs
    List[int]: indices of the pair

Notes

    - input values can have duplicates ie: [1, 1, 1, 1]
    - we cannot use the same element exact twice ie: index 0 is different than index 1
    - there is always one solution

Follow ups

    - is the array sorted? => nope


Cases

Strategy

    - Head & Tails

        + create indices at the start and end of the array
        + while indices haven't crossed, check summation against the target
        + this doesn't work since, it depends on a sorted array

    - Set

        + 

"""
from typing import List


class Solution:

    def head_and_tails(self, arr: List[int], target: int) -> List[int]:
        """Head & tails approach"""
        head = 0
        tail = len(arr) - 1

        # Sort the array
        arr = sorted(arr)

        while head < tail:

            # Check summation
            sum = arr[head] + arr[tail]

            # We found the target
            if sum == target:
                return [head, tail]

            # Values are the same
            # can't use one of the elements
            elif arr[head] == arr[tail]:
                head += 1

            # Too large
            elif sum > target:
                tail -= 1

            # Too small
            else:
                head += 1

        return None

    def twoSum(self, arr: List[int], target: int) -> List[int]:
        """Search for the compliment for the summation"""

        # Store the index for each value
        d = {}

        # Go through all the values
        for ix, val in enumerate(arr):

            # Calculate what we need to get our target
            comp = target - val

            # We found the compliment!
            # put the lower index in first
            if comp in d:
                return [d[comp], ix]

            # Store the value
            else:
                d[val] = ix

        return [None, None]


if __name__ == "__main__":

    s = Solution()

    # Sorted values
    arr_in = [2, 7, 11, 15]
    target = 9

    # Unsorted values
    arr_in = [3, 2, 4]
    target = 6

    print(s.twoSum(arr_in, target))
