from collections import deque

class MovingAverage:

    def __init__(self, size: int):
        """
        Args:
            size (int): size of the sliding window
        """
    
        self.sum = 0
        self.queue = deque([])
        self.size = size

    def next(self, val: int) -> float:
        """Grab the input and compute new average
        
        Args:
            val (int):
        Returns:
            Moving average of window
        """
        
        count = len(self.queue)

        # We're under the limit
        if count < self.size:
            self.sum += val
        
        # Window is at limit
        else:
        
            self.sum -= self.queue.popleft()
            self.sum += val

        # Store our new value
        self.queue.append(val)
        count = len(self.queue)
        # print(f'count: {count}  deque: {self.queue}')
            
        return self.sum / count
            
# Your MovingAverage object will be instantiated and called as such:
# obj = MovingAverage(size)
# param_1 = obj.next(val)
