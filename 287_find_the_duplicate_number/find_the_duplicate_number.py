from typing import List

# Other methods
# use a counting sort
#  space complexity: O(n)
#  time complexity: O(n)

# double loop
#  space complexity: O(1)
#  time complexity: O(n^2)


def find_duplicate(arr: List[int]) -> int:
    """Find the duplicate number in the array

    We're restricted on space to O(1) and time of < O(n^2)
    If we use a good sorting algorithm like merge sort
    we can get n*log(n)

    Time Complexity
        n*log(n)
    Space Complexity
        O(1)

    Args:
        arr (list): array of number in unknown order
    Returns:
        int: value of duplicate number
    """

    # Sort array in place
    arr.sort()

    # Iterate through value
    for ix in range(1, len(arr)):

        if arr[ix] == arr[ix - 1]:
            return arr[ix]

    return -1


if __name__ == '__main__':

    # arr_in = [1, 3, 4, 2, 2]
    arr_in = [3, 1, 3, 4, 2]
    print(find_duplicate(arr_in))
