"""
Inputs
    string (str): entire string
    k (int): number of repetitions desired
Outputs
    int: longest substring

Notes

    - we want all characters within the selected substring
    to meet our repetition requirement

Examples

    Example 1:

        Input: s = "aaabb", k = 3
        Output: 3
        Explanation: The longest substring is "aaa", as 'a' is repeated 3 times.

    Example 2:

        Input: s = "ababbc", k = 2
        Output: 5
        Explanation: The longest substring is "ababb", as 'a' is repeated 2 times and 'b' is repeated 3 times.

Ideas

    - guess and check
        + worst case we only find 1 pair of k letters, or none
        + best case we find all pairs of k letters

        + use a counter and a queue/pointers to push and pop
        characters from our count to determine the frequency
        of each character within our counter

        only push substring lengths when our criteria is met

    - for each character travel as far as necessary to meet our requirement

    - divide and conquer
        + initially try the whole string
        + get the character counts for the entire string
        + for each character check if we meet the repetition requirements
        + if not, then split our string at that position => left, right substrings

        + track the best substring we find overall

    - sliding window

        + hash for tracking
        + unsure of how to contract/extend 

"""

from collections import defaultdict


class Solution:
    def longestSubstring(self, s: str, k: int) -> int:
        pass

    ''' Divide & Conquer '''

    def longestSubstring_dc(self, s: str, k: int) -> int:

        max_count = self.divide_and_conquer(s, 0, len(s))
        return max_count

    def divide_and_conquer(self, s: str, start: int, stop: int) -> int:
        """Split the string recursively if our character
        counts are not met

        Args:
            s (str): the whole string
            start (int): start of substring (inclusive)
            stop (int): end of substring (exclusive)
        Returns:
            int: largest substring meeting requirements

        Time Complexity
            O(n^2)

            - if each character is invalid, each character is checked n times
            - we get the character counts for each substring

        Space Complexity
            O(n)  hash map

        Notes
            - we can improve the time slightly, by only recursing once we've
            go through all invalid characters

            ie: ababab?[]242ababa

            perform a single split
                ababab      ababa

            instead of
                ababab?[]242ababa

                ababab []242ababa
                ababab? ]242ababa
                ababab?[ 242ababa
                ababab?[] 42ababa
                ababab?[]2 2ababa
                ababab?[]24 ababa

            - our current time is near the same complexity of a brute force approach,
            where we check all our substrings
        """

        # print(f'start: {start}   stop: {stop}')

        length = stop - start

        # Avoid invalid recursions
        if length < k:
            return 0

        # Get character counts
        dct = defaultdict(int)

        for char in s[start:stop]:
            dct[char] += 1

        # Ensure counts meet requirement
        # split when we hit an invalid substring
        for ix in range(start, stop):

            char = s[ix]

            if dct[char] < k:
                return max(self.divide_and_conquer(s, 0, ix), self.divide_and_conquer(s, ix + 1, len(s)))

        # No issues found
        return stop - start

    ''' Sliding Window '''

    def sliding_window(self, s: str, k: int) -> int:

        n: int = len(s)
        max_len: int = 0

        # Get initial counter
        st = set([])

        for char in s:
            st.add(char)

        n_uniq: int = len(st)

        # See if we can meet each goal size of unique keys
        for uniq_goal in range(1, n_uniq + 1):

            l: int = 0
            r: int = 0
            dct = defaultdict(int)  # window counter
            meets_k: int = 0        # number of keys matching goal

            while r < n:

                # We need more unique characters that match our repetition goal
                if len(dct) < uniq_goal and meets_k < uniq_goal:
                    dct[s[r]] += 1

                    # We just broke the meets threshold
                    if dct[s[r]] == k:
                        meets_k += 1

                    r += 1

                # We're meeting our goal
                else:
                    pass

                # We meet the repetition requirements for all substring characters
                if meets_k >= uniq_goal and len(dct) >= uniq_goal:
                    max_len = max(max_len, r - l)

        return max_len


    ''' Binary Search: Does not Work '''

    def binary_search(self, s: str, k: int) -> int:
        """

        Notes
            - this does not work for all cases.
            - a longer substring may not necessarily be better for meeting
            our requirement since it requires more good keys

            additionally a shorter substring may not contain all number of keys
            necessary for our requirement

            - Failed case: s = "aaabbb", k = 3

            with a window size of 4, we will always fail, and never try for
            a larger window size

            aaab
            aabb
            abbb
        """

        low: int = k
        high: int = len(s)
        ans: int = 0

        while low <= high:

            mid = low + ((high - low) // 2)

            check = self.check(s, k, mid)

            print(f'low: {low}   high: {high}   mid: {mid}   check: {check}')

            if check:
                ans = mid
                low = mid + 1
            else:
                high = mid - 1

        return ans

    def check(self, s: str, k: int, guess: int) -> bool:
        """Check if our length guess can work

        - try all windows of size guess

        Time Complexity
            O(~g*n)

            - check g keys for each window

        Space Complexity
            O(g)
        """

        dct = defaultdict(int)

        # Counter for initial window
        for char in s[:guess]:
            dct[char] += 1

        works = all([dct[key] >= k for key in dct])

        if works:
            return True

        # Check remaining windows for a valid substring
        for ix in range(guess, len(s)):

            # Remove last character
            last_char = s[ix - guess]
            dct[last_char] -= 1

            if dct[last_char] == 0:
                del dct[last_char]

            # Add the new character
            dct[s[ix]] += 1

            works = all([dct[key] >= k for key in dct])
            if works:
                return True

        return False


if __name__ == '__main__':

    # Example 1
    # s = "aaabb"
    # k = 3

    # Example 2
    # s = "ababbc"
    # k = 2

    # Example failed
    s = "aaabbb"
    k = 3

    obj = Solution()
    print(obj.longestSubstring_dc(s, k))
    # print(obj.binary_search(s, k))

