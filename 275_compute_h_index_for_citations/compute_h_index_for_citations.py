from typing import List
from collections import deque


''' Guess & Check methodlogy '''

def check_h_index(arr: List[int], h_index: int) -> bool:
    """Determine if the h index is possible for the
    given citations

    A scientist has index h if h of his/her N papers have
    at least h citations each and the other N − h papers
    have no more than h citations each."

    Args:
        arr (list): array of papers and their citation count
        h_index (int): h-index we'd like to check
    Returns:
        bool: is this h-index possible
    """

    # Init
    queue = deque(arr)  # all citations
    others = []          # citations to check for other criteria

    count_meets: int = 0
    count_others: int = 0

    while queue:

        # Pop item
        book = queue.pop()

        # print('book:', book)

        if book >= h_index:

            count_meets += 1
            
            # We meet the base criteria
            if count_meets >= h_index:
                break
            # print(' pass')

        else:
            # print(' fail')
            others.append(book)

    # We don't have enough papers
    if count_meets < h_index:
        return False

    # Check remaining books that don't meet criteria
    while others:

        # Pop item
        book = others.pop()

        # print('book:', book)

        # Check that books have no more
        # than h-citations
        if book > h_index:
            # print(' fail')
            return False

    # We meet criteria
    return True


def compute_h_index(arr: List[int]) -> int:
    """Compute the h index for an array of citations

    Args:
        arr (list): array of papers and their citation count
    Returns:
        int: calculated h index
    """
    print("compute_h_index")

    # Maximum number of citations
    low: int = 1
    high: int = len(arr)

    # No books
    if high == 0:
        return 0

    # Binary search
    while low < high:

        # Calculate mid point
        mid = (low + high) // 2
        # mid = low + (high - low) // 2  # avoid overflow

        print(f" low: {low} high: {high} mid: {mid}")

        # Check if this h-index works
        check = check_h_index(arr, mid)

        # We have a working value
        if check:
            low = mid + 1   # try larger
        else:
            high = mid - 1  # try smaller

    # print(f" low: {low} high: {high} mid: {mid}")

    return high


''' Binary search '''

def search(arr: List[int]) -> int:
    """Compute the h index for an array of citations

    Args:
        arr (list): array of papers and their citation count
    Returns:
        int: calculated h index
    Notes:
        broken method
    Reference:
        https://leetcode.com/problems/h-index-ii/discuss/71063/Standard-binary-search
    """
    print("compute_h_index")

    # Maximum number of citations
    n: int = len(arr)
    low: int = 0
    high: int = n - 1

    # No books
    if high == 0:
        return 0

    # Binary search
    while low <= high:

        # Calculate mid point
        mid = (low + high) // 2

        print(f" low: {low} high: {high} mid: {mid}")

        # Number of books with a greater than or equal citation count
        # (books to the right + this one)
        n_books_greater = n - mid

        # We have enough books for this h-index
        if n_books_greater >= arr[mid]:
            low = mid + 1   # try larger

        # We don't have enough books
        else:
            high = mid - 1  # try smaller

    print(f" low: {low} high: {high} mid: {mid}")
    print(' books_to_right:', n - low)
    
    return high


if __name__ == '__main__':

    # No books
    # print(compute_h_index([]))

    # Single book
    # print(compute_h_index([0]))
    # print(compute_h_index([1]))
    # print(compute_h_index([3]))

    # Basic test case
    # print(search([0, 1, 3, 5, 6]))
    print(search([0, 10, 30, 55, 66]))
    # print(compute_h_index([5, 5, 5, 5, 6]))
    # print(search([0, 1, 3, 3, 4, 5, 9]))

    # Larger citation bank
    # print(search([0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11]))

    # Check method
    # print(check_h_index([0, 1, 3, 5, 6], 3))
    # print(check_h_index([0, 1, 3, 5, 6], 4))

    print('complete.')
