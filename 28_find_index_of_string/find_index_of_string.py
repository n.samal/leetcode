def find_index_of_str1(string: str, search: str) -> int:
    """Remove the duplicates from the sorted array

    Time Complexity
        O(n)
    Space Complexity
        O(1)

    Arg:
        arr (list): integers in unknown order
        element (int): element to remove
    Returns:
        int: index of search string
    References:
        https://leetcode.com/problems/remove-element/
    Todo:
        doesn't work for strings of length 1 or 0
    """

    # Define indices
    n: int = len(string)
    m: int = len(search)
    jx: int = 0

    # Null search string
    if m == 0:
        return 0

    # Iterate through main string
    for ix in range(n):

        # We have valid indices and matching characters
        while ix < n and jx < m and string[ix] == search[jx]:

            ix += 1
            jx += 1

        # Check if we made it to the end
        if jx == m - 1:
            return ix - m
        else:
            # Reset the search index
            jx = 0

    # Did not find the value
    return -1


def find_index_of_str(string: str, search: str) -> int:
    """Remove the duplicates from the sorted array

    Time Complexity
        O(n)
    Space Complexity
        O(1)

    Arg:
        arr (list): integers in unknown order
        element (int): element to remove
    Returns:
        int: index of search string
    References:
        https://leetcode.com/problems/remove-element/
    """

    # Define indices
    n: int = len(string)
    m: int = len(search)

    # Null strings
    if n == 0 and m == 0:
        return 0

    # Iterate through main string
    for ix in range(n):

        # We found search string
        if string[ix:ix + m] == search:
            return ix

    # Did not find the value
    return -1


if __name__ == "__main__":

    print(find_index_of_str("hello", "ll"))
    print(find_index_of_str("aaaaa", "ba"))
    print(find_index_of_str("a", "a"))
    print(find_index_of_str("", ""))
