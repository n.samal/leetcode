from typing import List


def next_greater_perm(arr: List[int]):
    """Find the next greater number by only permutating the current values

    ie: "53421"

    53 421
    53,124
    53,214

    54, 321
    54, 123

    largest possible value -> 54321 (increasing right to left)
    smallest possible vaue -> 12345 (decreasing right to left)

    - if all values increasing (right to left), then we have the
      largest value already
    - move from right to left
        - find the first value that is decreasing from the previous number
          ie: 3
        - find the value to the right of this number that is the next
          greatest number ie: 4
        - swap those numbers ie: 5|4,3|21 (this is not the final number)
        - reverse the order of all values to the right of the swapped value
        to minize the rest of the number
          ie: 54|1,2,3

    Sort in ascending order if not possible

    Args:
        arr (list): array of integers in unknown order
    Returns:
        list: array representing the next greatest value
    """

    # Get array length
    n: int = len(arr)
    ix_dec: int = -1
    ix: int = n - 1

    # Integer value is only one digit
    if n <= 1:
        return None

    # Find the indice of the first decreasing value
    while ix > 0 and arr[ix - 1] > arr[ix]:
        ix -= 1

    # Store index
    ix_dec = ix - 1

    print('ix_dec:', ix_dec)

    # No decreasing index found
    if ix_dec == -1:

        # Sort values in ascending order
        arr[:] = sorted(arr)
        return None

    # Find the next greatest value to the right of our value
    ix_greater: int = None

    # Search from right to left
    for ix in range(n - 1, -1, -1):

        # Value greater than decreasing index
        if arr[ix] > arr[ix_dec]:
            ix_greater = ix
            break

    # No greater value found
    if ix_greater is None:
        return None

    # print(' ix_greater:', ix_greater, 'value:', arr[ix_greater])

    # Swap the decreasing index with the next larger value
    arr[ix_dec], arr[ix_greater] = arr[ix_greater], arr[ix_dec]

    # print(' ', arr)

    ''' Swap remaining numbers '''
    # arr[ix_dec + 1:] = arr[ix_dec + 1:][::-1]

    # Initialize swap indices
    left = ix_dec + 1
    right = n - 1

    # Reverse all values after our swapped index
    while left < right:

        arr[left], arr[right] = arr[right], arr[left]

        # Bump indices
        left += 1
        right -= 1

    return None


if __name__ == "__main__":

    # arr_in = [1, 2, 3]
    # arr_in = [1, 1, 5]

    # arr_in = [5, 3, 4, 2, 1]
    # arr_in = [3, 2, 1]

    # arr_in = [1, 5, 1]
    # arr_in = [1, 1]
    arr_in = [5, 1, 1]

    print(arr_in)
    next_greater_perm(arr_in)
    print(arr_in)


    # arr_in = [5, 3, 4, 2, 1]




    find where value gets smaller

    2 > 1


