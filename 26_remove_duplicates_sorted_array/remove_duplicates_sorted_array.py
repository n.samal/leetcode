
def remove_duplicates(arr: list) -> int:
    """Remove the duplicates from the sorted array

    Time Complexity
        O(n)
    Space Complexity
        O(1)

    Arg:
        arr (list): integers in sorted order
    Returns:
        int: number of unique values
    References:
        https://leetcode.com/problems/remove-duplicates/
    """

    # No items in array
    if len(arr) == 0:
        return 0

    # Define indices
    ix_uq: int = 0  # index of unique value

    for ix in range(len(arr)):

        # Value doesn't equal our last unique value
        # We have a new unique value!
        if arr[ix] != arr[ix_uq]:

            # Increment unique count
            ix_uq += 1

            # Overwrite nonunique value with unique number
            arr[ix_uq] = arr[ix]

    return ix_uq + 1


if __name__ == "__main__":

    arr_in = [0, 0, 1, 1, 1, 2, 2, 3, 3, 4]
    # arr_in = [1, 1, 2]
    n = remove_duplicates(arr_in)

    print(arr_in[:n+1])
