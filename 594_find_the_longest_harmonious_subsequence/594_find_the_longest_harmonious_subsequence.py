"""
Inputs
    arr (List[int]): values in unknown order
Outputs
    int: length of the longest harmonious subsequence

Notes

    - harminous array is where the difference between
    the minimum and maximum value is one
    - subsequence is a sequence that can be derived
    by deleting some or no elements but not modifying
    the order of elements

Examples

    Example 1:

        Input: nums = [1,3,2,2,5,2,3,7]
        Output: 5
        Explanation: The longest harmonious subsequence is [3,2,2,2,3].

    Example 2:

        Input: nums = [1,2,3,4]
        Output: 2

    Example 3:

        Input: nums = [1,1,1,1]
        Output: 0

Ideas

    - subsequences can only contain two values

        + a singular value only has a range of 0
        ie: [2, 2, 2, 2, 2]

    - we only need to consider values above and below each potential
    seed ie: 2, we consider subarrays with 1 or 3

    - use a hash to track the unique values and also track
    the count and/or indices

        + we actually don't need indices since order is not
        critical here. ie: not monotonically decreasing, increasing, etc..

        arr_seq = [2, 3, 3, 3, 2, 2, 2, 3]

        just combine the counts for both keys
"""

from collections import defaultdict

class Solution:
    def findLHS(self, arr: List[int]) -> int:
        """

        - track the counts for each value
        - see if we can combine each unique value
        with the lower or higher compliment (+1, -1)

        - assume that combinations are put in order

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        """
        # Store the counts of each value
        dct = defaultdict(int)

        for v in arr:
            dct[v] += 1

        max_len: int = 0

        # Try all combinations
        for key in dct:

            # Lower combination
            low = key - 1

            if low in dct:
                count = dct[key] + dct[low] 
                max_len = max(max_len, count)

            # Upper combination
            high = key + 1

            if high in dct:
                count = dct[key] + dct[high] 
                max_len = max(max_len, count)

        return max_len
