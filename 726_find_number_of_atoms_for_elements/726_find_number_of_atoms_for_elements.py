"""
Inpts
    s (str): chemical formula
Outputs:
    s (str): expanded chemical formula in sorted order

Notes

    Formulas are in general order of

        ElementNumber
        Element name can be
            - single letter uppercase  ie: H
            - upper case then lower case  ie: Mg

        Number
            - if no number, count = 1 ie: H
            - if a number, then the element is multiplied by the number ie: H2

        Parentheses
            - elements followed by ()
            - require a recursion pattern then a multiplier for the digit

Examples

    Example 1:
        Input:
            formula = "H2O"
        Output: "H2O"
        Explanation:
        The count of elements are {'H': 2, 'O': 1}.

    Example 2:
        Input:
            formula = "Mg(OH)2"
        Output: "H2MgO2"
        Explanation:
        The count of elements are {'H': 2, 'Mg': 1, 'O': 2}.

    Example 3:
        Input:
            formula = "K4(ON(SO3)2)2"
        Output: "K4N2O14S4"
        Explanation:
        The count of elements are {'K': 4, 'N': 2, 'O': 14, 'S': 4}.

Ideas
    - use recursion to determine the sub pattern
        + multiply the values by the count after getting of the sub pattern
        + add those values to the current value count

    - can be assume single numbers?
    - do patterns only exist after elements
        + ie: H(HeQ6)2

        => appears not, some patterns exist first

            ((HHe28Be26He)9)34
"""

from collections import defaultdict


class Solution:

    def countOfAtoms(self, formula: str) -> str:
        """Recursively crawl string to aggregate subpatterns

        Time Complexity
            O(n^2)

            - iterate over each key to gather data
            - then again to combine data, this can be large for many subpatterns
        Space Complexity
            O(n)

        """

        dct, ix = self.recurse(formula, 0)

        s = ''

        # Recreate string
        for key in sorted(dct):

            if dct[key] == 1:
                s += key
            else:
                s += key + str(dct[key])

        return s

    def recurse(self, formula: str, ix: int) -> dict:

        dct = defaultdict(int)
        n: int = len(formula)

        while ix < n and formula[ix] != ')':

            ''' Handle Parentheses '''

            if formula[ix] == '(':

                dct_sub, ix = self.recurse(formula, ix + 1)

                count = 0
                while ix < n and formula[ix].isdigit():
                    count = 10 * count + int(formula[ix])
                    ix += 1

                if count == 0:
                    count = 1

                for key in dct_sub:
                    dct[key] += dct_sub[key] * count

            ''' Get element name'''

            if ix < n and formula[ix].isalpha():

                # Upper, lower ie: He
                if ix + 1 < n and formula[ix + 1].islower():
                    key = formula[ix: ix + 2]
                    ix += 2

                # Normal ie: H
                else:
                    key = formula[ix]
                    ix += 1

                ''' Get number if relevant '''
                count = 0

                while ix < n and formula[ix].isdigit():
                    count = 10 * count + int(formula[ix])
                    ix += 1

                if count == 0:
                    dct[key] += 1
                else:
                    dct[key] += count

        # Bump if coming out of a sub pattern ie: ')'
        ix += 1

        return dct, ix


if __name__ == '__main__':

    # Example 2
    # s = 'Mg(OH)2'

    # Example 3
    # s = 'K4(ON(SO3)2)2'

    # Large numbers
    # s = "(NB3)33"

    # Longer case
    s = "((HHe28Be26He)9)34"

    obj = Solution()
    print(obj.countOfAtoms(s))
