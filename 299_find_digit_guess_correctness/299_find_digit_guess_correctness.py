"""

Inputs
    secret (str): correct string
    guess (str): guess of string
Outputs
    str: bulls and cows
Notes
    assume that the secret number and your friend's guess only contain digits
    and their lengths are always equal.

Examples

    Example 1
    - Input: secret = "1807", guess = "7810"
    - Output: "1A3B"
    - Explanation: 1 bull and 3 cows. The bull is 8, the cows are 0, 1 and 7.

    Example 2
    - Input: secret = "1123", guess = "0111"
    - Output: "1A1B"
    - Explanation: The 1st 1 in friend's guess is a bull, the 2nd or 3rd 1 is a cow.

Cases
    - general case
        unique characters across string

    - secret can have multiple of the same character
        ie: 1111

Definition

    Bulls = how many digits in said guess match your secret number exactly in both digit and position
    Cows = how many digits match the secret number but locate in the wrong position

    A: indicates bulls
    B: indidcates cows

Strategy

    - compare each character against one another

        + if they match, increment the bulls criteria
        + if they don't match, check this later for a cow

            * store characters for secret and guess in a hash
            * we can't use a set because duplicate characters won't count ie: '1111'

    - go through the non-match characters and check if there's a cow

        + if there's some overlap, increment the number of cows
        + overlap = min(v1, v2)
        + ie: (0, 10) => no overlap
        + ie: (5, 10) => 5 overlapping

References:
    - https://docs.python.org/3.6/library/stdtypes.html#dict
"""


class Solution:

    def getHint(self, secret: str, guess: str) -> str:
        """Remove keys one by one"""
        # Initialize digits
        n_bulls: int = 0        
        n_cows: int = 0

        # Store non match characters
        # the character and the count
        d_secret = {}
        d_guess = {}

        # Iterate through strings
        for ch_s, ch_g in zip(secret, guess):

            # We've got a match
            if ch_s == ch_g:
                n_bulls += 1

            # Save characters that don't match
            else:

                # Update secret dictionary
                if ch_s in d_secret:
                    d_secret[ch_s] += 1
                else:
                    d_secret[ch_s] = 1

                # Update guess dictionary
                if ch_g in d_guess:
                    d_guess[ch_g] += 1
                else:
                    d_guess[ch_g] = 1

        # Check the guess characters
        while d_guess:

            # Grab the first key in guesses
            char = list(d_guess.keys())[0]

            # Found a match in the secret
            if char in d_secret:

                n_cows += 1

                # Update dictionaries
                d_secret[char] -= 1

                # No more characters
                if d_secret[char] <= 0:
                    del d_secret[char]

            # Decrement the character in the guess
            d_guess[char] -= 1

            if d_guess[char] <= 0:
                del d_guess[char]

        hint = f'{n_bulls}A{n_cows}B'

        return hint

    def getHint2(self, secret: str, guess: str) -> str:
        """Remove keys all at once instead of one by one

        Check all ones at once
        ie: secret = '1111'
            guess =  '2222'

        Check all ones, then all twos
        ie: secret = '1122'
            guess =  '2211'
        """

        # Initialize digits
        n_bulls: int = 0
        n_cows: int = 0

        # Store non match characters
        # the character and the count
        d_secret = {}
        d_guess = {}

        # Iterate through strings
        for ch_s, ch_g in zip(secret, guess):

            # We've got a match
            if ch_s == ch_g:
                n_bulls += 1

            # Save characters that don't match
            else:

                # Update secret dictionary
                if ch_s in d_secret:
                    d_secret[ch_s] += 1
                else:
                    d_secret[ch_s] = 1

                # Update guess dictionary
                if ch_g in d_guess:
                    d_guess[ch_g] += 1
                else:
                    d_guess[ch_g] = 1

        # Check the guess characters
        for char in d_guess.keys():

            # Found a match in the secret
            if char in d_secret:

                # Determine the overlap in character count
                n_cows += min(d_guess[char], d_secret[char])

        return f'{n_bulls}A{n_cows}B'

    def getHint3(self, secret: str, guess: str) -> str:
        """Create an array to track number counts instead of dict

        Check all ones at once
        ie: secret = '1111'
            guess =  '2222'

        Check all ones, then all twos
        ie: secret = '1122'
            guess =  '2211'
        """

        # Initialize digits
        n_bulls: int = 0
        n_cows: int = 0

        # Store non match characters
        # the character and the count
        # ie: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        arr_secret = [0 for ix in range(10)]
        arr_guess = [0 for ix in range(10)]

        # Iterate through strings
        for ch_s, ch_g in zip(secret, guess):

            # We've got a match
            if ch_s == ch_g:
                n_bulls += 1

            # Save characters that don't match
            else:

                # Update array counts
                arr_guess[int(ch_g)] += 1
                arr_secret[int(ch_s)] += 1

        # Check the guess characters
        for ix in range(10):

            # Determine the overlap in character count
            n_cows += min(arr_secret[ix], arr_guess[ix])

        return f'{n_bulls}A{n_cows}B'


if __name__ == "__main__":

    s = Solution()

    # Example 1
    # secret = "1807"
    # guess = "7810"

    # Example 2
    # secret = "1123"
    # guess = "0111"

    # Example
    secret = "1122"
    guess = "2211"

    # print(s.getHint2(secret, guess))
    print(s.getHint3(secret, guess))
