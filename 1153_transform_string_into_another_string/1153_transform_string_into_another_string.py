"""
Inputs
    str1 (str): source string
    str2 (str): target string
Outputs
    bool: is the mapping possible with zero or more conversion
Notes
    - in one conversion you can convert all occurences of one character to any
    other lowercase English character

Examples
    Example 1:

        Input: str1 = "aabcc", str2 = "ccdee"
        Output: true
        Explanation: Convert 'c' to 'e' then 'b' to 'd' then 'a' to 'c'. 
        Note that the order of conversions matter.

    Example 2:

        Input: str1 = "leetcode", str2 = "codeleet"
        Output: false
        Explanation: There is no way to transform str1 to str2.
Ideas
    - compare the indices
        + generate a hashmap for desired start to desired target
        + if all source chars map to a single target then it's possible

        + if a source chars map to a multiple targets then fail

            e -> [t, o, d]

    - order of operations is important
        + we want to avoid operations which will later be changed by a conversion later

        ie: 

        "aabcc" -> "ccdee"

        Desired
        a -> c
        b -> d
        c -> e

        + if we change 'a' first, this will turn into 'e's later
        + we want to use 'c' first since 'e' does not occur in our source mapping

    - if we need to avoid a certain order of operations we could always
    use a temporary variable

        str1: abcd
        str2: dbca

        mappings
            a -> d
            d -> a

        if we change one, the second change wil be impacted

        str1  : abcd
        a -> d: dbcd
        d -> a: abca  (cycle issue now)

        instead of converting directly from a ------> d
        we use a temporary conversion from  a -> t -> d

        str1  : abcd
        a -> t: tbcd
        d -> a: tbca
        t -> d: dbca
        str2  : dbca

    - what if the strings contain all the characters
        str1: abcdefghijklmnopqrstuzwxyz
        str2: zabcdefghijklmnopqrstuzwxy

        then some cycles occur

        mappings
            a -> z
            b -> a
            c -> b
            etc..

        + each time we change it would impact our string due to the cycles
        + to avoid this we could use a temporary character... however
        we may not have any to use

    - how many temporary characters do we need?

        appears only one temporary character is needed, so if one character
        is not used from the alphabet in our target
        then we can use that missing character to convert to the goal string

        + source = abcdedghijklmnopqrstuvqxyz
          target = abcdedghijklmnopqrstuvqxxx

References:
    https://docs.python.org/3.7/library/stdtypes.html#set-types-set-frozenset
"""


class Solution:
    def canConvert_og(self, str1: str, str2: str) -> bool:
        """Work in progress

        """
        ''' Generate a reverse mapping '''
        dct = {}

        for ch_src, ch_tgt in zip(str1, str2):

            # A different mapping already exists
            if ch_tgt in dct and dct[ch_tgt] != ch_src:
                return False
            else:
                dct[ch_tgt] = ch_src

        # Store keys
        target_keys = set(list(dct.keys()))
        source_keys = set(list(dct.values()))
        diff_keys = target_keys - source_keys

        while diff_keys:

            # Grab a mapping where the target character does not occur in the source
            ch_tgt = diff_keys.pop()
            ch_src = dct[ch_tgt]

            # Remove the utilized key from the source and target keys
            target_keys.remove(ch_tgt)
            source_keys.remove(ch_src)

        # No verify that remaing keys don't overlap

        return True

    def canConvert2(self, str1: str, str2: str) -> bool:
        """Check if 

        - if multiple mappings to the source character then False
        - if no key available to use a temporary character
            then return False

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        # If characters are equal
        # ie: all alphabet used, but they are equal
        if str1 == str2:
            return True

        ''' Generate a mapping '''
        dct = {}

        for ch_src, ch_tgt in zip(str1, str2):

            # A different mapping already exists
            if ch_src in dct and dct[ch_src] != ch_tgt:
                return False
            else:
                dct[ch_src] = ch_tgt

        target_keys = set(dct.values())
        return len(target_keys) < 26


if __name__ == '__main__':

    # Example 1
    str1 = "aabcc"
    str2 = "ccdee"

    # Example 2
    # str1 = "leetcode"
    # str2 = "codeleet"

    obj = Solution()
    # print(obj.canConvert(str1, str2))
    print(obj.canConvert2(str1, str2))
