"""
Inputs
    List[List[str]]: 2D grid of characters
    List[str]: 2D grid of characters
Outputs
    List[str]: words that were found
Goals
    - find which words can be found in the character grid

Ideas

    - create a prefix tree for all words
    - iterate over each location in the grid
        + crawl the prefix tree using current char
        + crawl the prefix tree for neighbors
        + mark chars we've crawled over here since
        they can't be used twice in the same word

    - mark the end of a word with non-alphabetical character
        t
        r
        e
        e
        * t
          o
          p
          *
"""

from typing import List


class Solution:


    ''' Attempt 1 '''

    def get_prefix_tree(self, words) -> dict:
        """Build a trie

        - generate a prefix tree as nested dictionaries
        - specify the end of words using '*' flag

        a
        p
        p
        l
        e i
        * c
          a
          t
          i
          o
          n
          *

        Time Complexity
            O(n)   n = number of leters for all words
        Space Complexity
            O(n)
        """

        dct = {}

        for word in words:

            dct_cur = dct
            for char in word:

                if char not in dct_cur:
                    dct_cur[char] = {}

                dct_cur = dct_cur[char]

            dct_cur['*'] = word

        return dct

    def findWords(self, board: List[List[str]], words: List[str]) -> List[str]:
        """

        Time Complexity
            O(n*m* 3^k)

            k = longest word
            if each character in our longest word has a valid prefix
            3 routes at each character

        Space Complexity
            O(w + c)  size of results and size of trie
        """

        words = set(words)
        self.n_rows: int = len(board)
        self.n_cols: int = len(board[0])

        # self.words_found = []
        self.words_found = set([])

        prefix_tree = self.get_prefix_tree(words)

        for r in range(self.n_rows):
            for c in range(self.n_cols):

                char = board[r][c]

                if char in prefix_tree:
                    self.crawl(board, r, c, prefix_tree[char])

        return list(self.words_found)

    def crawl(self, board, row: int, col: int, prefix_tree: dict):
        """Crawl the grid while we have a potential word in our prefix tree

        Notes
            - to avoid removing all the data we can
                + copy the board and modify it
                + or reapply character after we crawl all possible
                paths below
        """

        dirs = [(1, 0), (-1, 0), (0, 1), (0, -1)]  # dy, dx

        if '*' in prefix_tree:
            self.words_found.add(prefix_tree['*'])

        # Use a copy and mark as visited
        # board = [row[:] for row in board]
        char_og = board[row][col]
        board[row][col] = -1

        for dy, dx in dirs:

            r_new = row + dy
            c_new = col + dx

            if 0 <= c_new < self.n_cols and 0 <= r_new < self.n_rows and board[r_new][c_new] != -1:

                char = board[r_new][c_new]

                if char in prefix_tree:
                    self.crawl(board, r_new, c_new, prefix_tree[char])

        # Reapply original char
        board[row][col] = char_og

    ''' Attempt 2 '''

    def findWords2(self, board: List[List[str]], words: List[str]) -> List[str]:
        """

        - at each grid location try starting a search for each word
            + only start the search if the first letter matches and we
            haven't found that word yet

        Time Complexity
            O(r*c*w*4*3^W)

            each letter for each word has three major directions (not backwards)

        Space Complexity
            O(r*c)  visited hash
        """
        
        words = set(words)
        
        n_rows: int = len(board)
        n_cols: int = len(board[0])
        dirs = [(-1, 0), (1, 0), (0, 1), (0, -1)]
        
        self.words_found = set([])

        for r in range(n_rows):
            for c in range(n_cols):
                
                char = board[r][c]
                
                for word in words:
                    if word not in self.words_found and char == word[0]:
                        self.search(board, r, c, word, 1, set([(r, c)]))
        
        return list(self.words_found)
    
    def search(self, board, r: int, c: int, word: str, word_ix: int, visited):
        """Depth first search with backtracking

        - search adjacencies for the next character
        - block visited paths during deep searches then open them again
        - save the word when all characters have been found

        Time Complexity
            O(4*3^w) each character in the word has 3 potential paths, but we explore 4 directions
        Space Complexity
            O(W) visited path, and words found
        """

        if word_ix == len(word):
            self.words_found.add(word)
            return

        n_rows: int = len(board)
        n_cols: int = len(board[0])
        
        for r_new, c_new in [(-1 + r, 0 + c), (1 + r, 0 + c), (0 + r, 1 + c), (0 + r, -1 + c)]:

            key = (r_new, c_new)

            if 0 <= r_new < n_rows and 0 <= c_new < n_cols and key not in visited and board[r_new][c_new] == word[word_ix]:
                visited.add(key)
                self.search(board, r_new, c_new, word, word_ix + 1, visited)
                visited.remove(key)



if __name__ == '__main__':

    # Example 1
    board = [
    ['o', 'a', 'a', 'n'],
    ['e', 't', 'a', 'e'],
    ['i', 'h', 'k', 'r'],
    ['i', 'f', 'l', 'v']]
    words = ["oath", "pea", "eat", "rain"]
    # words = ["pea", "peter", "peas"]

    # board = [["a", "a"]]
    # words = ["a"]

    obj = Solution()
    print(obj.findWords(board, words))
