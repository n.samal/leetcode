"""
Inputs
    n (int): number of employees
    headID (int): ID of the CEO
    manager (List[int]): manager ID for each employee
    informTime (List[int]): time needed by employee to communicate to subordinates
Outputs
    int: the number of minutes needed to inform all the employees about the urgent news.
Goals
    - the CEO has no manager and therefore their manager is notated as -1
    - each communication is handled by each manager and deseminated according
    to the company hiearchy

Ideas

    - seems like a tree that we need to traverse
        + follow the manager hiearchy and add the time required for
        communication at each layer
        + output the max time summation for the final layer

    - create a child relationship for each manager
        + this should make traversal easier to follow

    - we can traverse using BFS or DFS

"""

class Solution:
    def numOfMinutes(self, n: int, headID: int, manager: List[int], informTime: List[int]) -> int:
        """Travese the management tree and find the time needed to spread information

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        # Only a single employee
        if n == 1:
            return informTime[0]

        # Find the subordinates for each manager
        children = {}

        for employee_id, manager_id in enumerate(manager):

            if manager_id not in children:
                children[manager_id] = [employee_id]
            else:
                children[manager_id].append(employee_id)

        # print(f'children: {children}')

        max_time = 0
        queue = [(headID, informTime[headID])]

        while queue:

            employee_id, time = queue.pop()

            # Track our max time
            max_time = max(time, max_time)

            # print(f'employee: {employee_id}')

            # If the employee has subordinates, then disseminate information
            if employee_id in children: 
                for child_id in children[employee_id]:

                    # print(f' child: {child_id}')
                    queue.append((child_id, time + informTime[child_id]))

        return max_time
