"""
Inputs
    maze (List[List[int]]): binary grid
    start (List[int]): starting position
    destination (List[int]): goal position

Outputs
    int: minimum distance to reach the goal

Notes
    - open positions designated as 0
    - closed positions designated as 1

    - we can move in 4 directions
        + up
        + down
        + left 
        + right
    
    - the ball continues to  slide until we hit a wall
"""

from collections import deque

class Solution:
    def shortestDistance(self, maze: List[List[int]], start: List[int], destination: List[int]) -> int:
        """Breath first search

        - from the starting position try each direction
            + slide in each direction until we hit a wall
            + track the distance traveled

        - if the cost to reach the position is smaller than before, we add it back into our queue
        - output the min cost to reach the goal


        Time Complexity
            O(r*c*max(r,c))  at each position we can slide a max of each dimension
        Space Complexity
            O(r*c)
        """
        
        
        # Are we at the goal
        if start == destination:
            return 0
        
        # Start is blocked
        if maze[start[0]][start[1]] == 1:
            return -1
        
        n_rows: int = len(maze)
        n_cols: int = len(maze[0])
            
        min_dist = [[float('inf') for _ in range(n_cols)] for _ in range(n_rows)]
        min_dist[start[0]][start[1]] = 0
        
        queue = deque([(start[0], start[1])])
        visited = set([])
        
        while queue:
            
            r, c = queue.popleft()
            
            # Try each movement
            for dx, dy in [[0, 1], [0, -1], [1, 0], [-1, 0]]:
                
                r_new = r
                c_new = c
                dist: int = 0
                
                while 0 <= r_new + dy < n_rows and 0 <= c_new + dx < n_cols and maze[r_new + dy][c_new + dx] == 0:
                    r_new += dy
                    c_new += dx
                    
                    dist += 1

                # Compute cost to reach this stop
                cost = min_dist[r][c] + dist
                
                # If we have a new best, we need to revisit the node again
                if cost < min_dist[r_new][c_new]:
                    min_dist[r_new][c_new] = cost
                
                    key_new = (r_new, c_new)
                    queue.append(key_new)
        
        # for row in min_dist:
            # print(row)
        
        if min_dist[destination[0]][destination[1]] != float('inf'):
            return min_dist[destination[0]][destination[1]]
        else:
            return -1
        