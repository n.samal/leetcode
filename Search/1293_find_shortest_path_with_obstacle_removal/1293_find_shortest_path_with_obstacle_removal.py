"""
Inputs
    grid (List[List[int]]): 2D grid
    k (int): number of obstacles we can remove
Outputs
    int: minimum number 
Notes
    - cell markings
        + 1: obstacle
        + 0: free cell

    - movements are left, right up down
    - we can eliminate some obstacles

Ideas

    - use a heap to notate the minimum cost action to explore

    - track several parameters
        + number of obstacles eliminated
        + current cost
        + current number of steps?

    - must track certain grids as visited to prevent endless loop
        + we shouldn't visit a grid location if we've been
        there before at a lower cost

    - we want explore locations at low cost, but also with maximum K left
        + due to this fact we may need to expand our state space to include K left
        + Breadth First Search can be used explore states evenly
        + python only has min heap, so we need to flip K left

    - optimize state space
        + n_steps: minimize
        + k_left: maximize
        + location: maximize (we want those closest to our goal)

            we could reframe this as minimize distance
            instead of maximize index

References:
    https://en.wiktionary.org/wiki/Manhattan_distance
    https://machinelearningmastery.com/distance-measures-for-machine-learning/
    https://leetcode.com/problems/shortest-path-in-a-grid-with-obstacles-elimination/discuss/451787/Python-O(m*n*k)-BFS-Solution-with-Explanation
"""

import heapq
from collections import deque
from typing import List


class Solution:

    ''' Normal Search: too slow '''

    def heap_search(self, grid: List[List[int]], k: int) -> int:
        """"Search via heap

        - explore the minimum cost states first
            + state = (location, k_left)
        - when obstacles occur try to remove them if we can
        - flip K left to so we get states with max K

        Time Complexity
            O(n*m*k * ln(m*n*k))

            search of heap state space increase time required
        Space Complexity
            O(n*m*k)

            our state space includes k_left, so we have potentially k different versions
            of that same location

        Notes
            - works, but slow on extreme cases
        """

        ans = float('inf')
        n_rows: int = len(grid)
        n_cols: int = len(grid[0])
        deltas = [(0, 1), (0, -1), (1, 0), (-1, 0)]  # (dy, dx)

        visited = set()

        heap = [(0, -k, 0, 0)]  # n_steps, k left, row_ix, col_ix
        heapq.heapify(heap)

        while heap:

            n_steps, k_left, row_ix, col_ix = heapq.heappop(heap)
            k_left = k_left * - 1

            if row_ix == n_rows - 1 and col_ix == n_cols - 1:
                ans = min(ans, n_steps)

            visited.add((row_ix, col_ix, k_left))

            # print(f'(y, x):  {row_ix},{col_ix}  k: {k_left}')

            # Explore neighbors
            for dy, dx in deltas:

                row_new = row_ix + dy
                col_new = col_ix + dx

                if 0 <= row_new < n_rows and 0 <= col_new < n_cols and (row_new, col_new, k_left) not in visited:

                    if grid[row_new][col_new] == 0:
                        # print(f'  open:  {row_new},{col_new}  k: {k_left}')
                        heapq.heappush(heap, (n_steps + 1, k_left * -1, row_new, col_new))

                    elif grid[row_new][col_new] == 1 and k_left > 0:
                        # print(f'  clsd:  {row_new},{col_new}  k: {k_left - 1}')
                        heapq.heappush(heap, (n_steps + 1, (k_left - 1) * - 1, row_new, col_new))

        if ans != float('inf'):
            return ans
        else:
            return -1

    def bfs_queue(self, grid: List[List[int]], k: int) -> int:
        """"Breadth first search

        - use BFS to explore the state space evenly
            + state = (location, k_left)
        - when obstacles occur try to remove them if we can

        Time Complexity
            O(n*m*k)
        Space Complexity
            O(n*m*k)

            our state space includes k_left, so we have potentially k different versions
            of that same location

        Notes
            - works, but slow on extreme cases
        """

        ans = float('inf')
        n_rows: int = len(grid)
        n_cols: int = len(grid[0])
        deltas = [(0, 1), (0, -1), (1, 0), (-1, 0)]

        visited = set()

        queue = deque([(0, 0, 0, k)])  # n_steps, row_ix, col_ix, k left

        while queue:

            n_steps, row_ix, col_ix, k_left = queue.popleft()

            if row_ix == n_rows - 1 and col_ix == n_cols - 1:
                return n_steps

            visited.add((row_ix, col_ix, k_left))

            # print(f'(y, x):  {row_ix},{col_ix}  k: {k_left}')

            # Explore neighbors
            for dy, dx in deltas:

                row_new = row_ix + dy
                col_new = col_ix + dx

                if 0 <= row_new < n_rows and 0 <= col_new < n_cols and (row_new, col_new, k_left) not in visited:

                    if grid[row_new][col_new] == 0:
                        # print(f'  open:  {row_new},{col_new}  k: {k_left}')
                        queue.append((n_steps + 1, row_new, col_new, k_left))

                    elif grid[row_new][col_new] == 1 and k_left > 0:
                        # print(f'  clsd:  {row_new},{col_new}  k: {k_left - 1}')
                        queue.append((n_steps + 1, row_new, col_new, k_left - 1))

        if ans != float('inf'):
            return ans
        else:
            return -1

    ''' A Star Search: Distance Heuristic '''

    def distance(self, row_cur: int, col_cur: int, row_goal: int, col_goal: int) -> int:
        """Compute manhattan distance to goal"""
        return (row_goal - row_cur) + (col_goal - col_cur)

    def heap_astar(self, grid: List[List[int]], k: int) -> int:
        """"Search with distance heuristic

        - explore the minimum cost states first
            + state = (n_steps, distance, k_left)
            + flip K left to so we get states with max K
        - when obstacles occur try to remove them if we can

        Time Complexity
            O(n*m*k * ln(m*n*k))

            search of heap state space increase time required
        Space Complexity
            O(n*m*k)

            our state space includes k_left, so we have potentially k different versions
            of that same location

        Notes
            - faster, but still too slow
        """

        n_rows: int = len(grid)
        n_cols: int = len(grid[0])
        deltas = [(0, 1), (1, 0), (0, -1), (-1, 0)]  # (dy, dx)

        visited = set()

        dist = self.distance(0, 0, n_rows - 1, n_cols - 1)
        heap = [(0, dist, -k, 0, 0)]  # n_steps, distance, k left, row_ix, col_ix
        heapq.heapify(heap)

        while heap:

            n_steps, dist, k_left, row_ix, col_ix = heapq.heappop(heap)
            k_left = k_left * - 1

            if dist == 0:
                return n_steps

            visited.add((row_ix, col_ix, k_left))

            # print(f'(y, x):  {row_ix},{col_ix}  k: {k_left}')

            # Explore neighbors
            for dy, dx in deltas:

                row_new = row_ix + dy
                col_new = col_ix + dx

                if 0 <= row_new < n_rows and 0 <= col_new < n_cols and (row_new, col_new, k_left) not in visited:

                    dist_new = self.distance(row_new, col_new, n_rows - 1, n_cols - 1)

                    if grid[row_new][col_new] == 0:
                        heapq.heappush(heap, (n_steps + 1, dist_new, k_left * -1, row_new, col_new))

                    elif grid[row_new][col_new] == 1 and k_left > 0:
                        heapq.heappush(heap, (n_steps + 1, dist_new, (k_left - 1) * - 1, row_new, col_new))

        return -1


if __name__ == '__main__':

    # Example 1
    # grid = [[0, 0, 0],  [1, 1, 0],  [0, 0, 0],  [0, 1, 1],  [0, 0, 0]]
    # k = 1

    # Smaller impossible grid
    grid = [
        [0, 1, 1],
        [1, 1, 1],
        [1, 0, 0]]
    k = 1

    # Large grid
    grid = [
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]]
    k = 5

    obj = Solution()
    # print(obj.heap_search(grid, k))
    # print(obj.bfs_queue(grid, k))
    print(obj.heap_astar(grid, k))
