"""
Inputs
    m (int): minimum number of digits in pattern
    n (int): maximum number of digits in pattern
Outputs:
    int: number of unique and valid patterns

Notes
    - android devices has 3 x 3 screen
    - digits are labled from 1 to 9
    - we can create lock/unlock patterns with the digits

        + dots in sequence should be distinct
        + adjacent dots are valid
        + we can perform jumps to old numbers only
        if the numbers inbetween have been visited
        in our pattern

"""

class Solution:
    def numberOfPatterns(self, m: int, n: int) -> int:
        """Backtrack from each starting position"""

        # Min and max key length
        self.keys_min = m
        self.keys_max = n

        self.count: int = 0
        self.visited = [False for _ in range(10)]

        # Requirements for skips
        # cur -> nxt: requirement
        self.reqs = {}
        self.reqs[(1, 3)] = 2
        self.reqs[(1, 7)] = 4
        self.reqs[(1, 9)] = 5
        self.reqs[(2, 8)] = 5
        self.reqs[(3, 1)] = 2
        self.reqs[(3, 7)] = 5
        self.reqs[(3, 9)] = 6
        self.reqs[(4, 6)] = 5
        self.reqs[(6, 4)] = 5
        self.reqs[(7, 1)] = 4
        self.reqs[(7, 3)] = 5
        self.reqs[(7, 9)] = 8
        self.reqs[(8, 2)] = 5
        self.reqs[(9, 1)] = 5
        self.reqs[(9, 3)] = 6
        self.reqs[(9, 7)] = 8

        for v in range(1, 10):
            self.visited[v] = True
            self.recurse(v, 1)
            self.visited[v] = False

        return self.count

    def recurse(self, cur: int, length: int):
        """Crawl remaining paths
        
        Time Complexity
            O(!max length)
        Space Complexity
            O(max length) recursion stack
        """

        if length >= self.keys_min:
            self.count += 1

        # Max pattern length reached
        if length == self.keys_max:
            return

        # Move to the next position
        # if the position has requirements
        for v in range(1, 10):

            key = (cur, v)

            if self.visited[v] is True:
                continue

            # No requirements
            elif key not in self.reqs:
                self.visited[v] = True
                self.recurse(v, length + 1)
                self.visited[v] = False

            # Requirements which have been met
            elif key in self.reqs and self.visited[self.reqs[key]] is True:
                self.visited[v] = True
                self.recurse(v, length + 1)
                self.visited[v] = False
