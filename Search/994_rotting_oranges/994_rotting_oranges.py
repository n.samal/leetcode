"""

Inputs:
    grid (List[List[int]]): states of each orange
Outputs
    int: minimum number of minutes until no cell has fresh orange
Goal
    - output minute number of minutes until no more fresh oranges
    - return -1 if it's impossible


Ideas
    - seems similar to flood fill
        + start queue with all current rotten oranges
        + need to grab adjacencies


Strategies
    Flood Fill
        + start queue with rotten values
        + at each rotten 


"""

from typing import List
from collections import deque


class Solution:

    def bfs(self, arr: List[List[int]]) -> int:
        """Breadth first search on rotten fruits

        Time Complexity
            O(m*n)

            if all fruits are rotten, then we iterate through all cells

        Space Complexity
            O(m*n)

            if all fruits are rotten, then we put all cells in the queue

        """
        FRESH: int = 1
        ROTTEN: int = 2

        n_rows: int = len(arr)        
        n_cols: int = len(arr[0])
        queue = deque([])
        n_fresh: int = 0  # nuumber of fresh fruits
        mins_needed: int = 0  # minutes needed

        # Only move in required directions
        if n_rows == 1:
            deltas = ((0, -1), (0, 1))
        elif n_cols == 1:
            deltas = ((-1, 0), (1, 0))
        else:
            deltas = ((-1, 0), (1, 0), (0, -1), (0, 1))

        # Find location and count of rotten fruits
        for row_ix in range(n_rows):
            for col_ix in range(n_cols):

                # Store minute and location of bad fruits
                if arr[row_ix][col_ix] == ROTTEN:
                    queue.append((0, row_ix, col_ix))
                elif arr[row_ix][col_ix] == FRESH:
                    n_fresh += 1

        # We have no fresh fruits to start
        if n_fresh == 0:
            return 0

        while queue:

            cur_min, cur_row, cur_col = queue.popleft()

            # Mark fresh fruit as rotten now
            if arr[cur_row][cur_col] == FRESH:
                arr[cur_row][cur_col] = ROTTEN
                n_fresh -= 1

                # Track max minute
                mins_needed = max(cur_min, mins_needed)

            # Search adjacent fruits
            for del_row, del_col in deltas:
                new_row = cur_row + del_row
                new_col = cur_col + del_col

                # Found adjacent fresh fruit
                if (0 <= new_col < n_cols) and (0 <= new_row < n_rows) and arr[new_row][new_col] == FRESH:

                    # Add to our queue for later
                    queue.append((cur_min + 1, new_row, new_col))

        # We got rid of all the bad fruits
        if n_fresh == 0:
            return mins_needed
        else:
            return -1

    def bfs2(self, arr: List[List[int]]) -> int:
        """Breadth first search on rotten fruits

        Time Complexity
            O(m*n)
        Space Complexity
            O(m*n)
        """
        EMPTY = 0
        FRESH = 1
        ROTTEN = 2

        n_fresh: int = 0
        n_rotten: int = 0
            
        n_rows: int = len(arr)
        n_cols: int = len(arr[0])
            
        minutes: int = -1
            
        queue_cur = deque([])
        queue_nxt = deque([])
        
        # Find all rotten oranges
        for r in range(n_rows):
            for c in range(n_cols):
                
                if arr[r][c] == ROTTEN:
                    queue_cur.append((r, c))
                
                elif arr[r][c] == FRESH:
                    n_fresh += 1
        
        # No fresh oranges to rot
        if n_fresh == 0:
            return 0

        # Propogate while we have fresh oranges
        while n_fresh and queue_cur:
            
            minutes += 1
            
            # print(f'minute: {minutes}')
            
            # for row in arr:
                # print(row)
            
            while queue_cur:
            
                r, c = queue_cur.popleft()
                
                if arr[r][c] == FRESH:
                    n_fresh -= 1
                    arr[r][c] = ROTTEN
            
                # Find adjacents
                for r_new, c_new in [(r + 1, c), (r - 1, c), (r, c + 1), (r, c - 1)]:
                    if 0 <= r_new < n_rows and 0 <= c_new < n_cols and arr[r_new][c_new] == FRESH:
                        queue_nxt.append((r_new, c_new))       
            
            queue_cur = queue_nxt
            queue_nxt = deque([])
        
        # Did we eliminate all fresh oranges?
        if n_fresh == 0:
            return minutes
        else:
            return -1


if __name__ == "__main__":

    # Example 1
    # arr = [[2, 1, 1], [1, 1, 0], [0, 1, 1]]

    # Example 2
    # arr = [[2, 1, 1], [0, 1, 1], [1, 0, 1]]

    # Example 3
    # arr = [[0, 2]]

    # Columns
    # arr = [[2], [1], [1], [1], [2], [1], [1]]

    # Overlapped fruit possible
    arr = [[2, 2], [1, 1], [0, 0], [2, 0]]

    obj = Solution()
    print(obj.bfs(arr))
