"""
Inputs
    n (int): number of cities
    connections (List[List[int]]): edges between different cities
Outputs
    int: find the minimum number of edges 
Notes

    - connections are one way
        + each road connections defined as a list [a, b]
        + directions is from a => b

    - the big city is indexed at 0
    - what's the minimum number of roads we need to reorient, so that
    every city can go to the big city?

Examples

    Example 1

        Input: n = 6, connections = [[0,1],[1,3],[2,3],[4,0],[4,5]]
        Output: 3
        Explanation: Change the direction of edges show in red such that each node can reach the node 0 (capital).

    Example 2

        Input: n = 5, connections = [[1,0],[1,2],[3,2],[3,4]]
        Output: 2
        Explanation: Change the direction of edges show in red such that each node can reach the node 0 (capital).

    Example 3

        Input: n = 3, connections = [[1,0],[2,0]]
        Output: 0

Ideas

    - track which nodes go to which other nodes
        + if any node goes from 0 to x, we know
        we need to flip it

    - track which nodes are connected to zero
        + if the node can't make it, we must flip it

        then add it's connections to our frontier of nodes to explore
        next

        + if the node can make it zero, we don't need to flip it

        add it's connections to our frontier as well, and check those

        + continue this process recursively or with a queue until
        we don't have anything left to check

"""

from collections import defaultdict, deque
from typing import List


class Solution:
    def minReorder(self, n: int, connections: List[List[int]]) -> int:
        """Breadth first search

        - track the inbound and outboud connections for each node
        - starting from the big city crawl to the other cities
        in an even fashion (BFS)
            + we assume our current node can make it to the big city
            so all in bound nodes can also make it too
            + all outbound nodes must be flipped

            + add all adjacent cities to our frontier, and make sure
            they can make it to a city we know works

        - track the number of flipped nodes, and where we've visited to prevent
        loops

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        # Track inbound and outgoing nodes
        self.inbound = defaultdict(list)
        self.outbound = defaultdict(list)

        for start, stop in connections:

            # start => stop
            self.outbound[start].append(stop)
            self.inbound[stop].append(start)

        ''' Crawl nodes from the capital '''

        count: int = 0
        queue = deque([0])  # node
        visited = set([])

        while queue:

            node = queue.popleft()

            visited.add(node)

            ''' Add all connected nodes to our frontier '''

            for node_in in self.inbound[node]:

                if node_in not in visited:
                    queue.append(node_in)

            # Grab number of outbound nodes, we need to flip these
            for node_out in self.outbound[node]:

                if node_out not in visited:
                    queue.append(node_out)

                    # Flip the outbound
                    count += 1

        return count


if __name__ == '__main__':

    # Example 1
    # n = 6
    # connections = [[0,1],[1,3],[2,3],[4,0],[4,5]]

    # Example 2
    # n = 5
    # connections = [[1,0],[1,2],[3,2],[3,4]]

    # Example 3
    n = 3
    connections = [[1,0],[2,0]]

    obj = Solution()
    print(obj.minReorder(n, connections))
