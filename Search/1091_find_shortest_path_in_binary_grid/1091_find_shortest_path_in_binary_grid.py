"""
Inputs
    grid (List[List[int]]): binary grid
Outputs
    int: shortest path to goal

Notes
    - starting position is (0, 0)
    - goal is (n-1, n-1)
    - we can move in 8 directions
    - we can only move to empty cells
    - find the shorest path to the goal
        + return -1 if it does not exist

Ideas
    - find the shortest path to each position

        + use BFS
        + use heap with A* search

"""

import heapq
from typing import List
from collections import deque


def manhattan_dist(row_cur: int, col_cur: int, row_goal: int, col_goal: int) -> int:
    return (row_goal - row_cur) + (col_goal - col_cur)


class Solution:
    def shortestPathBinaryMatrix(self, grid: List[List[int]]) -> int:
        """A* search

        - use distance heuristic for goal
        - use heap to grab minimum cost nodes
        - from each node travel to all valid positions

        Time Complexity
            O(n*m)
        Space Complexity
            O(n*m)

        Notes
            - too slow
        """

        n: int = len(grid)

        # Starting position is blocked
        if grid[0][0] == 1 or grid[n - 1][n - 1] == 1:
            return -1

        visited = set([])

        # dy, dx
        dirs = [(0, 1), (0, -1),   # right, left
               (1, 0), (-1, 0),    # down, up
               (-1, -1), (-1, 1),  # up left, up right 
               (1, -1), (1, 1)]    # down left, down right

        # Heurisitic
        dist = manhattan_dist(0, 0, n - 1, n - 1)

        heap = [(0, dist, 0, 0)]  # cost, distance, row, col
        heapq.heapify(heap)

        while heap:

            cost, dist, row_cur, col_cur = heapq.heappop(heap)

            if row_cur == n - 1 and col_cur == n - 1:
                return cost + 1

            # Ensure we ignore previously visited positions
            if (row_cur, col_cur) in visited:
                continue
            else:
                visited.add((row_cur, col_cur))

            for dy, dx in dirs:

                row_new = row_cur + dy
                col_new = col_cur + dx
                dist_new = manhattan_dist(row_cur, col_cur, n - 1, n - 1)

                if 0 <= row_new < n and 0 <= col_new < n and grid[row_new][col_new] == 0 and (row_new, col_new) not in visited:
                    heapq.heappush(heap, (cost + 1, dist_new, row_new, col_new))

        return -1

    def bfs(self, grid: List[List[int]]) -> int:
        """breadth first search

        - use BFS to search evenly
        - marked visited nodes in grid
        - from each node travel to all valid positions

        Time Complexity
            O(n*m)
        Space Complexity
            O(n*m)

        Notes
            - too slow
        """

        n: int = len(grid)

        # Starting position is blocked
        if grid[0][0] == 1 or grid[n - 1][n - 1] == 1:
            return -1

        # dy, dx
        dirs = [(0, 1), (0, -1),   # right, left
               (1, 0), (-1, 0),    # down, up
               (-1, -1), (-1, 1),  # up left, up right 
               (1, -1), (1, 1)]    # down left, down right

        queue = deque([(0, 0, 0)])  # cost,row, col

        while queue:

            cost, row_cur, col_cur = queue.popleft()

            if row_cur == n - 1 and col_cur == n - 1:
                return cost + 1

            # Skip if we've visited it before, otherwise mark it
            if grid[row_cur][col_cur] == 1:
                continue
            else:
                grid[row_cur][col_cur] = 1

            for dy, dx in dirs:

                row_new = row_cur + dy
                col_new = col_cur + dx

                if 0 <= row_new < n and 0 <= col_new < n and grid[row_new][col_new] == 0:
                    queue.append((cost + 1, row_new, col_new))

        return -1


if __name__ == '__main__':

    # Example 1
    # grid = [[0, 1], [1, 0]]

    # Example 2
    grid = [[0, 0, 0], [1, 1, 0], [1, 1, 0]]

    obj = Solution()
    print(obj.shortestPathBinaryMatrix(grid))
    print(obj.bfs(grid))
