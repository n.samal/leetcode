"""

Inputs
    N (int): number of digits
    K (int): absolute difference target
Outputs
    List[int]: array of number with the same consecutive difference
Notes
    - return all non-negative integers of length N
    - ensure the absolute difference between consecutive
    digits is K
    - leading zeros cannot be used
    - input limits
        1 <= N <= 9
        0 <= K <= 9

Examples

    Example 1
        Input: N = 3, K = 7
        Output: [181,292,707,818,929]
        Explanation: Note that 070 is not a valid number, because it has leading zeroes.

    Example 2
        Input: N = 2, K = 1
        Output: [10,12,21,23,32,34,43,45,54,56,65,67,76,78,87,89,98]

Ideas
    - initiliaze queue with seed values
        + avoid 0, to prevent leading zero
        + if N = 0, and K = 0 then zero would be valid

    - build upon seed values
        + use suffix math to add digits

    - we could initialize a dictionary of valid characters
    for each other character since this doesn't change

        if the previous number is 1 and the difference is 7
        we know the number is 8. no need to iterate over all
        other values

"""

from typing import List


class Solution:
    def numsSameConsecDiff(self, N: int, K: int) -> List[int]:
        """Depth first search

        - iteratively generate new numbers from a queue
        - only add digits to our value if
            + length is less than N
            + difference is equal to K
        - add special handling for N = 1

        Time Complexity
            O(9* 2^N??)

            every digit has one complmenting pair -> O(2^N)
            we explore 9 digits each loop

        Space Complexity
            O(2^N)
        """

        # Seed values (avoid leading zero)
        queue = [(1, 1), (2, 1), (3, 1), (4, 1), (5, 1), (6, 1), (7, 1), (8, 1), (9, 1)]

        # Single digit case (we want zero)
        if N == 1:
            queue.append((0, 1))

        ans = []

        while queue:

            digit, n_digits = queue.pop()

            if n_digits == N:
                ans.append(digit)

            else:

                for suffix in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]:

                    # 45 => 5
                    digit_prev = digit % 10

                    if abs(suffix - digit_prev) == K:
                        new_digit = digit * 10 + suffix
                        queue.append((new_digit, n_digits + 1))

        return ans


if __name__ == '__main__':

    # Example 1
    N = 3
    K = 7

    # Example 2
    # N = 2
    # K = 1

    # Single digit
    N = 1
    K = 0

    obj = Solution()
    print(obj.numsSameConsecDiff(N, K))

