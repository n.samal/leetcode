"""
Inputs
    N (int): number range
Outputs:
    int: find the number of number of confusing numbers

Notes
    - the number range is from 1 to N (inclusive)
    - a confusing number is one that is a different number when
    flipped by 180
    - a confusing number only contains valid numbers

    - when flipped
        Valid Numbers
          0 => 0
          1 => 1
          6 => 9
          8 => 8
          9 => 6

        Invalid Numbers
          2
          3
          4
          5
          7

Examples

    Example 1
        Input: 20
        Output: 6
        Explanation: 
        The confusing numbers are [6,9,10,16,18,19].
        6 converts to 9.
        9 converts to 6.
        10 converts to 01 which is just 1.
        16 converts to 91.
        18 converts to 81.
        19 converts to 61.

    Example 2
        Input: 100
        Output: 19
        Explanation: 
        The confusing numbers are [6,9,10,16,18,19,60,61,66,68,80,81,86,89,90,91,98,99,100].

Ideas
    - confusing numbers become different numbers when flipped
    - confusing numbers cannot contain invalid numbers

    - some numbers when flipped will still be the same
        + 11 => 11
        + 88 => 88

    - we want to target
        + numbers using 6, 9
        + but not number using 2, 3, 5, or 7
        + numbers with trailing 0s

    - create a queue and build from our seed numbers

        + add 0, 1, 6, 8, 9 before and after our seed
        + only count numbers within our number range
        and if they haven't been used before

    - we can create new number by adding values before and after
        + we may not need to do both directions

        + Suffix Adding

            seed = [0]
            adders = [0, 1, 6, 8, 9]

            new = [00, 01, 06, 08, 09]

            ** Notice we can generate other seeds just by adding
            suffices to 0

            seed = [1]
            new = [10, 11, 16, 18, 19]

            seed = [6]
            new = [60, 61, 66, 68, 69]

        + Prefix Adding can be ignored
            * suffix seems to generate all variations

References
    - similar approach
        + https://leetcode.com/problems/confusing-number-ii/discuss/605791/Python-explanations

"""


class Solution:

    ''' Check if confusing '''

    def is_confusing(self, N: int) -> bool:
        """Use modulus and integers division
        to go through digits one by one

        Time Complexity
            O(n)

            iterating over digits places

        Space Complexity
            O(1)
        """

        flips = {0: 0, 1: 1, 6: 9, 8: 8, 9: 6}

        val: int = N
        val_new: int = 0

        while val:

            # Grab the last digits
            last_digit = val % 10

            if last_digit not in flips:
                return False

            # 89 => 86 => 68
            val_new = val_new * 10 + flips[last_digit]

            # Move to the next digit
            # 1234 => 123.4 = 123
            val = val // 10

        return val_new != N

    def is_confusing2(self, N: int) -> bool:
        """

        - start generating the new number
            + flip and reverse all their numbers
        - check if the number is the same

        Time Complexity
            O(n)

            iterating over string digits

        Space Complexity
            O(n)
            storing new string
        """

        invalid = set(['2', '3', '4', '5', '7'])
        flips = {'0': '0', '1': '1', '6':'9', '8': '8', '9': '6'}

        # Convert to a string
        s = str(N)

        # Flip each character, and reverse order
        s_new = ''

        for char in s:

            if char in invalid:
                return False
            else:
                s_new = flips[char] + s_new

        return int(s_new) != N

    ''' Generate confusing numbers '''

    def confusingNumberII(self, N: int) -> int:
        """Use a queue with a checker and memory

        - start a seed with basic values
        - build variations from that seed
        - only count numbers that have been checked as confusing

        Args:
            N (int): number range [inclusive]

        Time Complexity
            O(5^k)
        Space Complexity
            O(5^k)

        Notes
            works but is slow
            - in worst case we go through N numbers
            - more typically we try 5 routes for each valid number under our limit
        """

        # queue = deque([0, 1, 6, 8, 9])
        count: int = 0
        queue = set([0])
        used = set([])

        while queue:

            # val = queue.popleft()
            val = queue.pop()

            if val not in used and val <= N:

                # Mark seed as used
                used.add(val)

                if self.is_confusing(val):
                    count += 1

                for digit in [0, 1, 6, 8, 9]:

                    # Generate variations
                    # ie: 6 => '61'
                    val_new_suffix = val * 10 + digit

                    if val_new_suffix <= N and val_new_suffix not in used and val_new_suffix not in queue:
                        queue.add(val_new_suffix)

        return count

    def confusingNumber2(self, N: int) -> int:
        """Use a queue with a checker without memory

        - start a seed with basic values
        - build variations from that seed
        - only count numbers that have been checked as confusing

        Args:
            N (int): number range [inclusive]

        Time Complexity
            O(5^k)
        Space Complexity
            O(5^k) size of the queue

        Notes
            - works but is slow
            - we can remove 0 from the seed, and just start the seed
            with [1, 6, 8, 9] while removing the check for 0 seed & 0 adder
        """

        count: int = 0
        queue = [0]

        while queue:

            val = queue.pop()

            if self.is_confusing(val):
                count += 1

            for digit in [0, 1, 6, 8, 9]:

                # Generate variations
                # ie: 6 => '61'
                val_new_suffix = val * 10 + digit

                # Don't regenerate the zero seed again
                if val == 0 and digit == 0:
                    continue

                if val_new_suffix <= N:
                    queue.append(val_new_suffix)

        return count

    def confusingNumber3(self, N: int) -> int:
        """Use a queue to track the original and rotated number

        - build the rotated number as we build number variations

            number = 910
            rotate = 016

            if adding a new digit, leverage our knowledge
            of the previous rotation

            number_new = 910 * 10 + 9 = 9109
            rotate_new = flip of 9 + 016
                       = 6 * 10^3 + 016 = 6016

        Args:
            N (int): number range [inclusive]

        Time Complexity
            O(N)
        Space Complexity
            O(N)

        Notes
            - improved speed over previous version but still slow
        """

        count: int = 0
        flips = {0: 0, 1: 1, 6: 9, 8: 8, 9: 6}
        queue = [(1, 1, 1), (6, 9, 1), (8, 8, 1), (9, 6, 1)]  # (number, rotated number, n_digits)

        while queue:

            val, rotated, n_digits = queue.pop()

            if val <= N and val != rotated:
                # print(f' value: {val}  rotated: {rotated}')
                count += 1

            for digit in [0, 1, 6, 8, 9]:

                # Generate variations
                # ie: 910 => '9109'
                val_new_suffix = val * 10 + digit

                # Rotation of 910 = 016
                # 016 => 9 flip + 016
                rot_new_prefix = flips[digit] * 10 ** n_digits + rotated

                if val_new_suffix <= N:
                    queue.append((val_new_suffix, rot_new_prefix, n_digits + 1))

        return count


if __name__ == '__main__':

    # case = 20
    case = 100

    obj = Solution()
    # print(obj.confusingNumberII(case))
    print(obj.confusingNumber2(case))
    print(obj.confusingNumber3(case))
