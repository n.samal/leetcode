"""
Inputs
    low (int): lower bound inclusive
    high (int):  upper bound inclusive
Outputs
    List[int]: sorted list of sequential digits in our range
Notes
    - sequential digits are numbers where digits
    are one more than the previous digits
    - low & high bounds are inclusive

Examples

    Example 1:

    Input: low = 100, high = 300
    Output: [123,234]
    Example 2:

    Input: low = 1000, high = 13000
    Output: [1234,2345,3456,4567,5678,6789,12345]
"""

from collections import deque

class Solution:
    def sequentialDigits(self, low: int, high: int) -> List[int]:
        """

        - use a queue to generate values
        - start from the base values and add another
        digit to the number if possible
            + 1 -> 12
            + 2 -> 23
            + 78 -> 789

            + we can't add a digit to 9 
        - use a deque to add values in their order

        Time Complexity
            O(1)

            the number of values possible is limited by the value 10
            9
            8 -> 89
            7 -> 78 -> 789
            6 -> 67 -> 678 -> 6789
            5 -> 56 -> 567 -> 5678 -> 56789
            4 -> 45 -> 456 -> 4567 -> 45678 -> 456789
            3 -> 34 -> 345 -> 3456 -> 34567 -> 345678 -> 3456789
            2 -> 23 -> 234 -> 2345 -> 23456 -> 234567 -> 2345678 -> 23456789
            1 -> 12 -> 123 -> 1234 -> 12345 -> 123456 -> 1234567 -> 12345678 -> 123456789

        Space Complexity
            O(1)
        """

        ans = []
        queue = deque([1, 2, 3, 4, 5, 6, 7, 8, 9])

        while queue:

            v = queue.popleft()

            # Value is valid
            if low <= v <= high:
                ans.append(v)

            # Get last digit
            digit = v % 10

            # Create the new value
            # 1 -> 12
            # 789 -> x
            if digit + 1 < 10:
                new_v = v * 10 + (digit + 1)
                queue.append(new_v)

        return ans
