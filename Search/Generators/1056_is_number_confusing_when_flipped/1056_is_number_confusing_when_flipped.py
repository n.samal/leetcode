"""
Inputs
    N (int): number
Outputs
    bool: is the number confusing
Notes
    - a confusing number is one that is a different number when
    flipped by 180 ie: think auction cards
    - a confusing number only contains valid numbers

    - when flipped
        Valid Numbers
          0 => 0
          1 => 1
          6 => 9
          8 => 8
          9 => 6

        Invalid Numbers
          2
          3
          4
          5
          7

Examples

    Example 1

        Input: 6
        Output: true
        Explanation: 
        We get 9 after rotating 6, 9 is a valid number and 9!=6.

    Example 2

        Input: 89
        Output: true
        Explanation: 
        We get 68 after rotating 89, 86 is a valid number and 86!=89.

    Example 3

        Input: 11
        Output: false
        Explanation: 
        We get 11 after rotating 11, 11 is a valid number but the value remains the same, thus 11 is not a confusing number.

    Example 4

        Input: 25
        Output: false
        Explanation: 
        We get an invalid number after rotating 25.

Ideas
    - confusing numbers become different numbers when flipped
    - confusing numbers cannot contain invalid numbers

    - some numbers when flipped will still be the same
        + 11 => 11
        + 88 => 88
"""
class Solution:
    def confusingNumber(self, N: int) -> bool:
        """

        - start generating the new number
            + flip and reverse all their numbers
        - check if the number is the same

        Time Complexity
            O(n)

            iterating over string digits

        Space Complexity
            O(n)

            storing new string
        """

        invalid = set(['2', '3', '4', '5', '7'])
        flips = {'0': '0', '1': '1', '6':'9', '8': '8', '9': '6'}

        # Convert to a string
        s = str(N)

        # Flip each character, and reverse order
        s_new = ''

        for char in s:

            if char in invalid:
                return False
            else:
                s_new = flips[char] + s_new

        return int(s_new) != N

    def confusingNumber2(self, N: int) -> bool:
        """Use modulus and integers division
        to go through digits one by one

        Time Complexity
            O(n)

            iterating over digits places

        Space Complexity
            O(1)
        """

        flips = {0: 0, 1: 1, 6: 9, 8: 8, 9: 6}

        val: int = N
        val_new: int = 0

        while val:

            # Grab the last digits
            last_digit = val % 10

            if last_digit not in flips:
                return False

            # 89 => 86 => 68
            val_new = val_new * 10 + flips[last_digit]

            # Move to the next digit
            # 1234 => 123.4 = 123
            val = val // 10

        return val_new != N


if __name__ == '__main__':

    case = 6

    obj = Solution()
    # print(obj.confusingNumberII(case))
    print(obj.confusingNumber2(case))
