"""
Inputs
    grid (List[List[str]]): 2D grid of characters
Outputs
    bool: does a cycle exist
Notes
    - cycle is a path of length 4 or more that starts and ends at the same cell
    - directions
        + up, down, left right
    - you cannot visit a cell that you visited in the last move
    - grid is guaranteed to be non null

Examples
    
    Example 1

        Input: grid = [
                       ["a","a","a","a"],
                       ["a","b","b","a"],
                       ["a","b","b","a"],
                       ["a","a","a","a"]
                      ]
        Output: true
        Explanation: There are two valid cycles

    Example 2

        Input: grid = [
                       ["c","c","c","a"],
                       ["c","d","c","c"],
                       ["c","c","e","c"],
                       ["f","c","c","c"]
                      ]
        Output: true
        Explanation: There is only one valid cycle
    
    Example 3
        Input: grid = [["a","b","b"],["b","z","b"],["b","b","a"]]
        Output: false

Ideas

    - perform flood fill at each grid
        + we don't want to revisit nodes we've filled before

        + basic flood fill won't work, we need to track the length of the
        path and not just the number of cells

        these are of length 4 but not a cycle

        a a a a 

        a
        a a a

    - use dfs to search right and down first
        + track the path length and compare
        the current path position against the neighbors

        1 2 3 4

        1 2
        4 3

        1 2 3 4 5
              7 6

"""

from typing import List


class Solution:
    def containsCycle(self, grid: List[List[str]]) -> bool:
        """Modified DFS

        - mark nodes we've visited with the length of the path
        - crawl to neighbors in each direction
            + if we've been somewhere before and it's >= 3 then we've
            got a cycle

        Time Complexity
            O(n*m)
        Space Complexity
            O(n*m)
        """

        n_rows: int = len(grid)
        n_cols: int = len(grid[0])

        visited = [[0 for c in range(n_cols)] for r in range(n_rows)]

        for r in range(n_rows):
            for c in range(n_cols):

                if visited[r][c] == 0:

                    queue = [(r, c, 1)]

                    while queue:

                        r_cur, c_cur, count = queue.pop()

                        visited[r_cur][c_cur] = count

                        for r_new, c_new in [(r_cur, c_cur + 1), (r_cur + 1, c_cur), (r_cur, c_cur - 1), (r_cur - 1, c_cur)]:
                            if 0 <= r_new < n_rows and 0 <= c_new < n_cols and grid[r_new][c_new] == grid[r][c]:

                                # New position
                                if visited[r_new][c_new] == 0:
                                    queue.append((r_new, c_new, count + 1))

                                # Been here before
                                elif abs(visited[r_new][c_new] - visited[r_cur][c_cur]) >= 3:
                                    return True

        return False


if __name__ == '__main__':

    # Example 1
    grid = [
           ["a", "a", "a", "a"],
           ["a", "b", "b", "a"],
           ["a", "b", "b", "a"],
           ["a", "a", "a", "a"]
          ]

    grid = [["c", "c", "c", "a"], ["c", "d", "c", "c"], ["c", "c", "e", "c"], ["f", "c", "c", "c"]]
    grid = [["a", "b", "b"], ["b", "z", "b"], ["b", "b", "a"]]

    obj = Solution()
    print(obj.containsCycle(grid))
