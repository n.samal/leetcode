"""
Inputs

Outputs
    int: minimum valid to make a grid have at least 1 valid path
Notes

    - you can modify the sign on a cell one time at cost = 1
    - some invalid signs may occur ie: pointing you outside the
    grid
    - Grid directions
        1 = go right
        2 = go left
        3 = go down
        4 = go up

Ideas

    - if the grid is already valid, then we don't have to change anything
        + min_cost = 0

    - if the grid is invalid, we want to minimize the number of cells changed

    - if the grid is of size 1, then the start = stop
        + min cost = 0

    - follow the current grid, and determine what positions are possible
        + mark these positions as a minimum cost of 0
        + we can crawl the path via DFS or BFS
        + we must maintain a visited set to avoid revisiting old locations
        + for the remaining positions we need to determine what the best way to get
        there is??

    - seems like a dynamic programming problem
        + at each location we can determine the minimum cost needed to get
        to the current position

        cur_pos = cost 

        + every position is dependent on it's neighbors..to every side

Strategies

    Depth First Search then 

        - travel the map to find where we can go with 0 cost
            + if we made it to the end, we're done
            + keep track of which places we've been to, 
            and the minimum cost at each location

        - try pivoting some of previously visited locations
            + try all pivots and track the cost required

References
    - https://docs.python.org/3.7/library/heapq.html
    - https://leetcode.com/problems/minimum-cost-to-make-at-least-one-valid-path-in-a-grid/discuss/535260/Python-heap
    - https://leetcode.com/problems/minimum-cost-to-make-at-least-one-valid-path-in-a-grid/discuss/524886/JavaC%2B%2BPython-BFS-and-DFS
    - https://leetcode.com/problems/minimum-cost-to-make-at-least-one-valid-path-in-a-grid/discuss/524828/Python-O(MN)-simple-BFS-with-explanation
"""

import heapq
from collections import deque
from typing import List


class Solution:
    def min_heap(self, grid: List[List[int]]) -> int:
        """Grab the lowest cost nodes

        - at each node evaluate all potential options
            + take the given path = zero cost
            + modify path = cost + 1
        - only travel along the lowest cost nodes first
            + if we've been somewhere before it implies
            we've been there for lower cost, we don't reevaluate

        Time Complexity
            O(m*n)
        Space Complexity
            O(m*n)

        """
        n_rows = len(grid)
        n_cols = len(grid[0])
        directions = [(0, 1), (0, -1), (1, 0), (-1, 0)]

        visited = set([])

        heap = [(0, 0, 0)]  # cost_i, row_i, col_i (we want low cost first!)
        heapq.heapify(heap)

        while heap:

            # Grab the minimum cost node
            cost, row, col = heapq.heappop(heap)

            # Made it to our goal
            if row == n_rows - 1 and col == n_cols - 1:
                return cost

            # Have we been here before?
            # if we have it was a cheaper visit, and no need to explore again
            if (row, col) in visited:
                continue
            else:
                visited.add((row, col))

            # Move in all directions
            for ix, dir_i in enumerate(directions):
                row_new = row + dir_i[0]
                col_new = col + dir_i[1]

                if row_new >= 0 and row_new < n_rows and col_new >= 0 and col_new < n_cols and (row_new, col_new) not in visited:

                    # We're already moving in the given direction
                    if grid[row][col] - 1 == ix:
                        heapq.heappush(heap, (cost, row_new, col_new))
                    else:
                        heapq.heappush(heap, (cost + 1, row_new, col_new))

    def alternating_queue(self, grid: List[List[int]]) -> int:
        """Use two queues to traverse the grid

            - travel to every node possible in the current layer
                + save zero cost positions in our current queue
                + save higher cost positions in our next queue
            - travel the next layer after the current queue is empty
                + only travel nodes if we haven't been here before more cheaply
            - save the costs required to reach each position

        Time Complexity
            O(m*n)
        Space Complexity
            O(m*n)

        """
        n_rows = len(grid)
        n_cols = len(grid[0])
        directions = [(0, 1), (0, -1), (1, 0), (-1, 0)]

        cost = 0
        queue_cur = deque([(0, 0, 0)])
        queue_nxt = deque([])
        costs = [[float('inf') for col_ix in range(n_cols)] for row_ix in range(n_rows)] 

        # Travel the grid one layer at a time
        # only travel this node if we haven't been to this node more cheaply
        while queue_cur or queue_nxt:

            if not queue_cur:
                queue_cur = queue_nxt
                queue_nxt = deque([])

            row, col, cost = queue_cur.popleft()

            if cost < costs[row][col]:
                costs[row][col] = cost
            else:
                continue

            for ix, dir_i in enumerate(directions):
                row_new = row + dir_i[0]
                col_new = col + dir_i[1]

                if 0 <= row_new < n_rows and 0 <= col_new < n_cols:

                    if ix + 1 == grid[row][col]:
                        queue_cur.append((row_new, col_new, cost))
                    else:
                        queue_nxt.append((row_new, col_new, cost + 1))

        return costs[n_rows - 1][n_cols - 1]


if __name__ == "__main__":

    # Failed case
    grid = [[1, 1, 3], [3, 2, 2], [1, 1, 4]]

    obj = Solution()
    print(obj.min_heap(grid))
    print(obj.alternating_queue(grid))

