"""
Inputs
    grid (List[List[int]]): relationships between friends
Output
    int: the total number of friend circles

Notes
    - matrice indicates a friend relationship
        M[i][j] = 1 means
         i & j are friends
    - friend circles can be made of a single person
Examples

    Example 1:

        Input: 
        [[1,1,0],
         [1,1,0],
         [0,0,1]]
        Output: 2
        Explanation:The 0th and 1st students are direct friends, so they are in a friend circle. 
        The 2nd student himself is in a friend circle. So return 2.

    Example 2:

        Input: 
        [[1,1,0],
         [1,1,1],
         [0,1,1]]
        Output: 1
        Explanation:The 0th and 1st students are direct friends, the 1st and 2nd students are direct friends, 
        so the 0th and 2nd students are indirect friends. All of them are in the same friend circle, so return 1.

Ideas
    - almost like flood fill 
        + try flood fill at each student
        + track the number of islands aka
        student circles

    - for each student crawl it's adjacencies
        + mark all connected students
        + track visited friend circles
"""

from collections import defaultdict

class Solution:
    def findCircleNum(self, grid: List[List[int]]) -> int:
        """

        - try seeding our search from each person
            + if we've been there before don't do a search
            + if we haven't then crawl all adjacencies
            and mark them

        - only increment our count when encountering a node
        we haven't visited

        Time Complexity
            O(n*n)
        Space Complexity
            O(n)
        Notes
            - we could use the given grid for adjacencies
            instead of generating our edges data structure
        """

        n: int = len(grid)

        edges = defaultdict(list)
        visited = [False for _ in range(n)]
        count: int = 0

        ''' Gather adjacencies'''
        for r in range(n):
            for c in range(n):
                if grid[r][c] == 1:
                    edges[r].append(c)

        ''' Travel Friend Circles '''

        for r in range(n):

            # New starting point
            if visited[r] is False:
                count += 1

                queue = [r]

                while queue:

                    node = queue.pop()
                    visited[node] = True

                    # Crawl to adjacent nodes
                    for adj in edges[node]:
                        if visited[adj] is False:
                            queue.append(adj)

        return count

    def findCircleNum2(self, grid: List[List[int]]) -> int:
        """

        - use the given grid for adjacencies

        Time Complexity
            O(n*n)
        Space Complexity
            O(n)
        Notes
            - not significantly faster or better on memory
        """

        n: int = len(grid)

        visited = [False for _ in range(n)]
        count: int = 0

        ''' Travel Friend Circles '''

        for r in range(n):

            # New starting point
            if visited[r] is False:
                count += 1

                queue = [r]

                while queue:

                    node = queue.pop()
                    visited[node] = True

                    # Crawl to adjacent nodes
                    for adj, relation in enumerate(grid[node]):
                        if relation and visited[adj] is False:
                            queue.append(adj)

        return count
