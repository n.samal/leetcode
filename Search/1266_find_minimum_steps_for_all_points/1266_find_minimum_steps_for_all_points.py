"""
Inputs
	points (List[List[in]]): xy points 
Outputs
	int: minimum time/steps to visit all point in order
Notes
	- we must visit the points in order provided
	- appears we can start at any point
	- we can travel in 3 directions of equal cost

		+ move vertically
		+ move horizontally
		+ move diagnolly

Examples

	Example 1:

		Input: points = [[1,1],[3,4],[-1,0]]
		Output: 7
		Explanation: One optimal path is [1,1] -> [2,2] -> [3,3] -> [3,4] -> [2,3] -> [1,2] -> [0,1] -> [-1,0]   
		Time from [1,1] to [3,4] = 3 seconds 
		Time from [3,4] to [-1,0] = 4 seconds
		Total time = 7 seconds

	Example 2:

		Input: points = [[3,2],[-2,2]]
		Output: 5

Ideas

	- use BFS or A* to find point

		+ start exactly at the first point
		+ find min steps to next point (goal)
		+ our goal becomes our next current

	- seems similar to the 975 cut all trees

	- use distance metric with A*
		+ manhattan distance: dist = abs(dx) + abs(dy)

	- distance cases
		+ in worst case if we only move horizontal and vertical
		we must use the manhattan distance

		+ if we travel in x and y simultaneously we save steps
			* although we can travel diagnol for most of the way
			we must still go x, or y for the remaining bit

"""

from typing import List

import heapq
from collections import deque

class Solution:

    ''' BFS '''
    def bfs(self, points: List[List[int]]) -> int:
    	"""Breadth first search

    	Notes
    		- too slow.
    		Likely spending too much time traveling in each direction
    	"""

        if not points:
            return 0

        count: int = 0

        # Start at the initial point
        cur_x = points[0][0]
        cur_y = points[0][1]

        for goal_x, goal_y in points[1:]:

            queue = deque([(cur_x, cur_y, 0)])
            visited = set([])

            while queue:

                x, y, steps = queue.popleft()

                if x == goal_x and y == goal_y:
                    count += steps
                    break

                visited.add((x, y))

                for x_new, y_new in [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1), (x + 1, y + 1), (x - 1, y - 1)]:

                    if (x_new, y_new) not in visited:
                        queue.append((x_new, y_new, steps + 1))

            # Our goal becomes our next start
            cur_x = goal_x
            cur_y = goal_y

        return count

    ''' A Star '''

    def distance(self, current, goal):
    	"""Euclidean distance"""
        return ((current[0] - goal[0])**2 + (current[1] - goal[1])**2)**0.5

    def a_star(self, points: List[List[int]]) -> int:
    	"""A* with heap

		Notes
			- works but is too slow
    	"""

        if not points:
            return 0

        count: int = 0

        # Start at the initial point
        cur_x = points[0][0]
        cur_y = points[0][1]

        for goal_x, goal_y in points[1:]:

            dist = self.distance((cur_x, cur_y), (goal_x, goal_y))

            heap = [(0, dist, cur_x, cur_y)]

            visited = set([])

            while heap:

                steps, dist, x, y = heapq.heappop(heap)

                if x == goal_x and y == goal_y:
                    count += steps
                    break

                visited.add((x, y))

                for x_new, y_new in [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1), (x + 1, y + 1), (x - 1, y - 1)]:

                    if (x_new, y_new) not in visited:
                        # queue.append((x_new, y_new, steps + 1))
                        dist_new = self.distance((x_new, y_new), (goal_x, goal_y))
                        heapq.heappush(heap, (steps + 1, dist_new, x_new, y_new))

            # Our goal becomes our next start
            cur_x = goal_x
            cur_y = goal_y

        return count     

    ''' Simplification '''

	def minTimeToVisitAllPoints(self, points: List[List[int]]) -> int:
		"""Chebyshev distance

		- in the worst case we travel the manhattan distance
		- we travel diagnolly most of the way

			+ need an extra dx, or dy for the remainder. The
			total distance is limited by the dx, or dy

			review the first example for details

		Time Complexity
			O(n)
		Space Complexity
			O(1)

		Notes
			- https://iq.opengenus.org/euclidean-vs-manhattan-vs-chebyshev-distance/
			- https://en.wikipedia.org/wiki/Chebyshev_distance
		"""

		if not points:
            return 0

        count: int = 0

        # Start at the initial point
        cur_x = points[0][0]
        cur_y = points[0][1]

        for goal_x, goal_y in points[1:]:

            dx = abs(cur_x - goal_x)
            dy = abs(cur_y - goal_y)

            count += max(dx, dy)

            # Our goal becomes our next start
            cur_x = goal_x
            cur_y = goal_y

        return count
