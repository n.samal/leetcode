"""
Inputs
    grid (List[List[int]]): binary matrix
Outputs
    int: the maximum area of an island in the given 2D array.

Notes

    Values
        1: land
        0: water
    - islands can only be connected in four directions (up, down, left, right)
    - we can assume the edges of the grid are surrounded by water

Ideas

    - use a flood fill at each element in the grid
        + if we've visited before skip it
        + track the number of nodes in each island

        + make sure we don't double count nodes with the
        same neighbors, since the queue may allow that

"""

from typing import List

class Solution:
    def maxAreaOfIsland(self, grid: List[List[int]]) -> int:
        """Depth First Search with node tracking

        Time Complexity
            O(n*m)
        Space Complexity
            O(n*m)
        """

        n_rows: int = len(grid)
        n_cols: int = len(grid[0])

        visited = [[False for c in range(n_cols)] for r in range(n_rows)]
        max_count: int = 0

        for r in range(n_rows):
            for c in range(n_cols):

                if not visited[r][c] and grid[r][c] == 1:

                    queue = [(r, c)]
                    count: int = 0

                    while queue:

                        r, c = queue.pop()

                        # Avoid double counting the same node                        
                        if visited[r][c] is False:
                            visited[r][c] = True
                            count += 1
                        else:
                            continue

                        for (r_new, c_new) in [(r - 1, c), (r + 1, c), (r, c - 1), (r, c + 1)]:
                            if (0 <= r_new < n_rows) and (0 <= c_new < n_cols) and visited[r_new][c_new] is False and grid[r_new][c_new] == 1:
                                queue.append((r_new, c_new))

                    max_count = max(max_count, count)

        return max_count

if __name__ == '__main__':

    # Failed Example (4)
    grid = [[1,1,0,0,0],
             [1,1,0,0,0],
             [0,0,0,1,1],
             [0,0,0,1,1]]

    obj = Solution()
    print(obj.maxAreaOfIsland(grid))