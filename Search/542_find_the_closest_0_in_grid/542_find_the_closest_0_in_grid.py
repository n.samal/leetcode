"""
Inputs
    matrix (List[List[int]]): binary grid
Outputs
    List[List[int]]: distance to closest 0
Notes
    - distance between two adjacent cells is 1
    - adjacent cells are only 
        + up
        + down
        + left
        + right

    - there is at least one zero in the grid
Ideas
    - create empty grid of distances, init to infinity

    - go through each point
        + if the matrix is 0, then dist = 0
        + otherwise grab the minimum distance
        from adjacent points then add 1

    - when we go iterate fowards we prpogate distance
    from top left to bottom right
        + what happens when all adjacencies are 1s??

    + perform BFS from each non zero point
        + seems expensive

    + perform BFS layer by layer from zero points
        + 1st adjacencies are 1
        + use those values to seed the next search
        + distance is 2
"""

class Solution(object):

    def propogate(self, matrix):
        """Propogate minimum distances

        - create an empty grid for distances
        - if the matrix is 0, distance is 0
            + otherwise grab the shorest distance + 1
            + this propogates values down and right

        - iterate from the bottom right, to top left
            + this propogates values to the top

        [1, 1, 1]
        [1, 1, 1]
        [1, 1, 0]

        Time Complexity
            O(r*c)
        Space Complexity
            O(r*c)
        """

        n_rows = len(matrix)
        n_cols = len(matrix[0])

        ans = [[float('inf') for c in range(n_cols)] for r in range(n_rows)]

        # Find minimums starting top left
        # propogate down and right
        for r in range(n_rows):
            for c in range(n_cols):

                if matrix[r][c] == 0:
                    ans[r][c] = 0
                else:

                    min_dist = float('inf')

                    # Find minimum from valid neighbors (above and left)
                    for r_new, c_new in [(r, c - 1), (r - 1, c)]:
                        if 0 <= r_new < n_rows and 0 <= c_new < n_cols:

                            min_dist = min(min_dist, ans[r_new][c_new])

                    ans[r][c] = min_dist + 1

        # Find minimums starting from bottom right
        # propogate up and left
        for r in range(n_rows - 1, -1, -1):
            for c in range(n_cols - 1, -1, -1):

                if matrix[r][c] != 0:

                    min_dist = ans[r][c]

                    # Find minimum from valid neighbors (bottom and right)
                    for r_new, c_new in [(r, c + 1), (r, c - 1), (r - 1, c), (r + 1, c)]:
                        if 0 <= r_new < n_rows and 0 <= c_new < n_cols:

                            min_dist = min(min_dist, ans[r_new][c_new])

                    ans[r][c] = min_dist + 1

        return ans

    def search(self, matrix):
        """Search in layers

        - start search at zeros and propogate outwards
        - the next layer is our new seed points
        - only continue our search while we are
        finding shorter distances

        Time Complexity
            O(r*c)
        Space Complexity
            O(r*c)
        Notes
            - can have repetition on search nodes
        """

        n_rows = len(matrix)
        n_cols = len(matrix[0])

        ans = [[float('inf') for c in range(n_cols)] for r in range(n_rows)]
        queue = []

        # Find zeros in original grid
        for r in range(n_rows):
            for c in range(n_cols):

                if matrix[r][c] == 0:
                    ans[r][c] = 0
                    queue.append((r, c))

        # Travel from each seed point
        # and try all valid neighbors
        # only save shorter distances
        while queue:

            r, c = queue.pop()

            dist_new = ans[r][c] + 1

            for r_new, c_new in [(r, c + 1), (r, c - 1), (r - 1, c), (r + 1, c)]:
                if 0 <= r_new < n_rows and 0 <= c_new < n_cols:

                    if dist_new < ans[r_new][c_new]:
                        ans[r_new][c_new] = dist_new
                        queue.append((r_new, c_new))

        return ans


if __name__ == '__main__':

    # Example 2
    grid = [[0,0,0], [0,1,0], [1,1,1]]

    obj = Solution()
    ans = obj.search(grid)
