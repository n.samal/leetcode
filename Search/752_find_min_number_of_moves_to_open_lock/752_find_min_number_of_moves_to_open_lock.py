"""
Inputs:
    deadends (List[str]): states that we cannot use
    target (str): goal state 
Outputs:
    int: minimum number of moves to target state

Notes

    - You have a lock in front of you with 4 circular wheels. 
    - Each wheel has 10 slots: '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'. 
    - The wheels can rotate freely and wrap around: 
        for example we can turn '9' to be '0', or '0' to be '9'. 
        Each move consists of turning one wheel one slot.
    - The lock initially starts at '0000', a string representing the state of the 4 wheels.
    - You are given a list of deadends dead ends, meaning if the lock displays any of these codes,
    the wheels of the lock will stop turning and you will be unable to open it.
    - return -1 if we can't reach the target state

Examples

    Example 1

        Input: deadends = ["0201","0101","0102","1212","2002"], target = "0202"
        Output: 6
        Explanation:

            A sequence of valid moves would be "0000" -> "1000" -> "1100" -> "1200" -> "1201" -> "1202" -> "0202".
            Note that a sequence like "0000" -> "0001" -> "0002" -> "0102" -> "0202" would be invalid, because the wheels of the lock become stuck after the display becomes the dead end "0102".

    Example 2

        Input: deadends = ["8888"], target = "0009"
        Output: 1
        Explanation:
            We can turn the last wheel in reverse to move from "0000" -> "0009".

    Example 3

        Input: deadends = ["8887","8889","8878","8898","8788","8988","7888","9888"], target = "8888"
        Output: -1
        Explanation:
            We can't reach the target without getting stuck.

    Example 4
        Input: deadends = ["0000"], target = "8888"
        Output: -1

Ideas

    - appears to be like a graph, with each lock combo
    as a different state
    - certain states are blocked
    - we need to track what states we've been to, in order
    to avoid infinite loops

    - BFS should allow us to search evenly, and find the state in the minimum number of steps

Strategies

    Breadth First Search

        Possible States: P ie: 1, 2, 3, ... 9
        Digits: number of digits D ie: 0000

        Time Complexity
            O(S^N * N^2)

        - we must go through all values in our queue, which may be full: O(S^N)
        - in the worst case there's no possible path
        - at each state we need to compute all the neighbors: O(N^2)

            4 digits in old string (D)
            2 deltas
            4 digits in new string (D)

            4 * 2 * 4 = 2N^2

        Space Complexity
            O(9^4) = O(S^N)

        - each of the four digits has 9 possible states = 9 * 9 * 9 * 9
        - in the worst case our queue is full of every state,
        - our queue may not be full but our visited state will eventually fill up
"""

from typing import List
from collections import deque


class Solution:

    def get_neighbors(self, state: str) -> str:
        """Grab all states adjacent to the current state

        - shift all characters in the state
            0000 => 1000, 0100, 0010, 0001
            0000 => 9000, 0900, 0090, 0009

        Args:
            state (str):
        Returns:
            str: adjacency of current lock combo state
        """

        # Go through each digit '0000'
        for ix in range(4):
            for delta in (-1, 1):

                # Shift the value and account for overflow
                # Digits can only be between 0 and 9
                char_new = (int(state[ix]) + delta) % 10

                yield state[:ix] + str(char_new) + state[ix + 1:]

    def openLock(self, deadends: List[str], target: str) -> int:
        """Breadth first search

        - start at '0000'
        - use BFS to search evenly from the current state
        - for the current node search all adjacent nodes
            - only add them if they haven't been visited

        Args:
            deadends (List[str]): states that we cannot use
            target (str): goal state
        Returns:
            int: minimum number of counts to find
        """

        visited = set(deadends)

        # State, count
        queue = deque([('0000', 0)])

        while queue:

            state, count = queue.popleft()

            # At our goal!
            if state == target:
                return count

            # We haven't been here before
            if state not in visited:

                # Mark as visited
                visited.add(state)

                # Add our neighbors to the queue
                for neighbor in self.get_neighbors(state):
                    if neighbor not in visited:
                        queue.append((neighbor, count + 1))

        # Unable to reach target
        return -1


if __name__ == '__main__':

    # Example 1
    deadends = ["0201", "0101", "0102", "1212", "2002"]
    target = "0202"

    obj = Solution()
    print(obj.openLock(deadends, target))
