"""
Inputs
    beginWord (str): beginning word
    endWord (str): ending word
    wordList (List[str]): words list
Outputs
    int: shortest transformation sequence

Goal
    - transform the beginning word to the end word in the shortest number
    of sequences
    - only one letter can change at a time
    - each transformed word must exist in the word list

Notes
    - return 0 if no possible word sequence
    - all words are of the same length
    - no duplicate words
    - all words are lower case
    - beginWord and endWord are non empty and not the same

Ideas

    - find "adjacent" words to crawl to
        + adjacent words are of edit distance 1
        + counter may not help since we need to account
        for the order of words
        + use regex ??? to find valid words for each potential
        different character

            ie: hit
                .it
                h.t
                hi.

    - try skipping characters in every word
        + create a table of words with common splits

            ie:
                table['it']: [hit]
                table['ht']: [hit, hot]
                table['hi']: [hit]

    - we can crawl two ways
        + beginning to end
        + end to beginning

"""

from typing import List
from collections import deque


class Solution:

    def get_word_pattern_dicts(self, wordList: str):
        """Create adjacency tables

        table['.it']: set(hit)
        table['h.t']: set(hit, hot)
        table['hi.']: set(hit)

        Returns:
            Dict[str, set]: word to patterns
            Dict[str, set]: pattern to words
        """

        # Generate adjacency table
        word_to_pat = {}  # key = word, value = patterns
        pat_to_word = {}  # key = patterns, value = word

        for word in wordList:

            # Try replacing each character with a dummy char
            # ie: hit => .it, h.t, hi.
            for ix in range(len(word)):
                pattern = word[:ix] + '.' + word[ix + 1:]

                # Connect patterns to words
                if pattern in pat_to_word:
                    pat_to_word[pattern].add(word)
                else:
                    pat_to_word[pattern] = set([word])

                # Connect words to patterns
                if word in word_to_pat:
                    word_to_pat[word].add(pattern)
                else:
                    word_to_pat[word] = set([pattern])

        return word_to_pat, pat_to_word

    ''' Depth First Search '''

    def ladderLength_dfs(self, beginWord: str, endWord: str, wordList: List[str]) -> int:
        """Find the shortest transformation sequence to the endWord"""

        self.endWord = endWord
        self.min_global: int = float('inf')

        # Get word and pattern dictionaries
        wordList.append(beginWord)
        self.word_to_pat, self.pat_to_word = self.get_word_pattern_dicts(wordList)

        print(f'word_to_pat: {self.word_to_pat}')
        print(f'pat_to_word: {self.pat_to_word}')

        # Recursively crawl to the end
        self.recurse(beginWord, set([]), 1)

        if self.min_global == float('inf'):
            return 0
        else:
            return self.min_global

    def recurse(self, currentWord: str, visited: set, count: int = 0):
        """Crawl the graph

        - crawl from the current word to all it's patterns
        - then crawl from it's patterns to all it's words

        Notes:
            method is too slow, try using breadth first search instead
        """

        if currentWord == self.endWord:
            self.min_global = min(count, self.min_global)
            print(f'  => word found!   count: {count}')
            return

        # Mark the current word as visited
        visited.add(currentWord)

        print(f'current: {currentWord}  count: {count}')

        # Grab all adjacencies
        for pattern in self.word_to_pat.get(currentWord, []):

            print(f' pattern: {pattern}')
            for word in self.pat_to_word.get(pattern, []):
                if word not in visited:
                    print(f'  word: {word}')

                    # Create a copy of the visited table
                    visited_new = visited.copy()

                    self.recurse(word, visited_new, count + 1)

    ''' Breadth First Search '''

    def ladderLength_bfs(self, beginWord: str, endWord: str, wordList: List[str]) -> int:
        """Find the shortest transformation sequence to the endWord"""

        self.endWord = endWord

        # Get word and pattern dictionaries
        wordList.append(beginWord)
        self.word_to_pat, self.pat_to_word = self.get_word_pattern_dicts(wordList)

        print(f'word_to_pat: {self.word_to_pat}')
        print(f'pat_to_word: {self.pat_to_word}')

        # Evenly crawl to the end
        return self.bfs(beginWord)

    def bfs(self, beginWord: str):
        """Breadth first search

        - crawl from the current word to all it's patterns
        - then crawl from it's patterns to all it's words
        - use BFS to more evenly search

        Time Complexity

        Space Complexity
            O(n*m)
        """

        queue = deque([(beginWord, 1)])
        visited = set([])

        while queue:

            cur_word, count = queue.popleft()
            print(f'current: {cur_word}  count: {count}')

            # Found our goal
            if cur_word == self.endWord:
                return count

            # Mark the current word as visited
            visited.add(cur_word)

            # Grab adjacent words
            for pattern in self.word_to_pat.get(cur_word, []):
                print(f' pattern: {pattern}')

                for word in self.pat_to_word.get(pattern, []):
                    if word not in visited:
                        queue.append((word, count + 1))

        return 0


if __name__ == '__main__':

    begin_in = "hit"
    end_in = "cog"
    arr_in = ["hot", "dot", "dog", "lot", "log", "cog"]

    obj = Solution()
    # print(obj.ladderLength(begin_in, end_in, arr_in))
    print(obj.ladderLength_bfs(begin_in, end_in, arr_in))
