"""
Inputs
    forest (List[List[int]]): 2D array of tree
Outputs
    int: minimum number of steps to cut all trees

Notes

    - forest values
        + 0: blocked
        + 1: walkable
        + > 1: tree with height = forest[r][c]

    - we have to cut trees from lowest height first
        + after cutting the values becomes 1
        + the height of each tree is unique

    - starting point is (0, 0)
        + we can walk in 4 directions (l, r, up, down)
"""
from collections import deque
from typing import List


class Solution:
    def cutOffTree(self, forest: List[List[int]]) -> int:
        """

        - find where all the trees are located
            + store them and sort them by height

        - find the minimum steps required to reach
        each tree, starting with the smallest ones
            + use BFS or A*

        Time Complexity
            O(n^2)

            n = rows * cols
            in the worst case we search the entire grid for each tree
        Space Complexity
            O(n)
        """

        n_rows: int = len(forest)
        n_cols: int = len(forest[0])

        trees = []

        # Store the height and location of each tree
        for r in range(n_rows):
            for c in range(n_cols):

                height = forest[r][c]

                if height > 1:
                    trees.append((height, r, c))

        trees = sorted(trees)

        r_cur: int = 0
        c_cur: int = 0
        total_steps: int = 0

        # Go through the shorest trees first
        for tree in trees:

            height, r_goal, c_goal = tree

            steps = self.bfs(r_cur, c_cur, r_goal, c_goal, forest)

            # We couldn't make it to the goal
            if steps < 0:
                return -1

            total_steps += steps
            r_cur, c_cur = r_goal, c_goal

        return total_steps

    def bfs(self, r_start, c_start, r_goal, c_goal, forest) -> int:
        """Breadth first search

        - search evenly to find the minimum number of steps
        to the goal state

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        Notes
            - use A* to get to goal state faster
        """

        n_rows: int = len(forest)
        n_cols: int = len(forest[0])

        # location, steps taken
        queue = deque([(r_start, c_start, 0)])
        visited = set([])

        while queue:

            r, c, n_steps = queue.popleft()

            # Found the goal, let's cut the tree
            if r == r_goal and c == c_goal:
                # forest[r][c] = 1
                return n_steps

            # Mark as visited
            visited.add((r, c))

            for r_new, c_new in [(r, c + 1), (r, c - 1), (r + 1, c), (r - 1, c)]:

                if 0 <= r_new < n_rows and 0 <= c_new < n_cols and forest[r_new][c_new] and (r_new, c_new) not in visited:
                    queue.append((r_new, c_new, n_steps + 1))

        return -1


if __name__ == '__main__':

    # Example 1 (6)
    forest = [
              [1,2,3],
              [0,0,4],
              [7,6,5]]

    # Example 2 (-1)
    forest = [
              [1,2,3],
              [0,0,0],
              [7,6,5]]

    # Example 1 (6)
    forest = [
              [2,3,4],
              [0,0,5],
              [8,7,6]]

    obj = Solution()
    print(obj.cutOffTree(forest))
