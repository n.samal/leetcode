"""
Inputs
    n (int): number of oranges
Outputs
    int: minimum number of days to eat n oranges
Notes
    - we start with n oranges
    - we have 3 choices
        + eat one orange
        + if divisible by 2, then we can eat n/2 oranges
        + if divisible by 3, then we can eat 2 * (n/3) oranges
    - we can only choose one action each day

Ideas
    - seems like a search problem
        + DFS/BFS

"""

from collections import deque


class Solution:
    def bfs(self, n: int) -> int:
        """Breadth first search

        - use BFS to search evenly
            + the first time we see 0 oranges, will
            be the first day possible
            + try our biggest jumps first
        - use hash to prevent duplicate nodes
        being visited at a later day

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        Notes
            - works but is slow
        """

        queue = deque([(n, 0)])  # number of oranges, day
        seen = set([])

        while queue:

            oranges, day = queue.popleft()

            print(f'oranges: {oranges}   day: {day}')

            # Add orange count for the minimum day
            seen.add(oranges)

            if oranges == 0:
                return day

            if oranges % 3 == 0:
                can_eat = (oranges // 3) * 2
                oranges_new = oranges - can_eat

                if oranges_new not in seen:
                    queue.append((oranges_new, day + 1))

            if oranges % 2 == 0:
                can_eat = oranges // 2
                oranges_new = oranges - can_eat

                if oranges_new not in seen:
                    queue.append((oranges_new, day + 1))

            if oranges >= 1:
                oranges_new = oranges - 1

                if oranges_new not in seen:
                    queue.append((oranges_new, day + 1))


if __name__ == '__main__':

    # Example 1
    # n = 10
    # n = 6
    # n = 1
    n = 56

    obj = Solution()
    print(obj.bfs(n))
