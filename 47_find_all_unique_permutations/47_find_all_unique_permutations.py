"""
Inputs
    arr (List[int]): numbers in unknown order, duplicates can exist
Outputs
    List[List[int]]: all possible unique permutations in any order.

Examples

    Example 1:

        Input: nums = [1,1,2]
        Output:
        [[1,1,2],
        [1,2,1],
        [2,1,1]]

    Example 2:

        Input: nums = [1,2,3]
        Output: [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]

Ideas

    - use backtracking to compute all postions
    - use a set to track only unique candidates
        + seems expensive

    - at each location only use unique candidates once

        ie: arr = [1, 1, 1, 2, 3, 4]

        at index = 0, for our path, we only need use 1 once there

        [1, x, x, x, x, x]
        [1, x, x, x, x, x] <- redundant
        [1, x, x, x, x, x] <- redundant
        [2, x, x, x, x, x]

        so we can skip straight to using 2 as our value at index 0
        if we track which candidate was used last

"""

from typing import List


class Solution:
    def permuteUnique(self, arr: List[int]) -> List[List[int]]:
        """Backtracking with marking

        Time Complexity
            O(N!)
        Space Complexity
            O(n)
        """

        self.arr = sorted(arr)
        self.ans = []
        self.n = len(arr)
        self.used = [False for _ in range(self.n)]

        self.recurse(self.n, [])

        return self.ans

    def recurse(self, remaining: int, path: List[int]):
        """Backtracking with marking

        - mark numbers if used
        - prevent the same number from being used twice
        - save path upon completion
        """

        if not remaining:
            self.ans.append(path)
            return

        prev = None

        for ix in range(self.n):

            if self.used[ix] is False:

                # Use uniques candidates once at each position
                if self.arr[ix] == prev:
                    continue

                self.used[ix] = True
                self.recurse(remaining - 1, path + [self.arr[ix]])
                self.used[ix] = False

                prev = self.arr[ix]


if __name__ == "__main__":

    nums = [1, 1, 2]

    obj = Solution()
    print(obj.permuteUnique(nums))
