from typing import List
# import bisect


class Solution:

    def threeSum(self, nums: List[int]) -> List[List[int]]:

        pass

    def naive(self, arr: List[int], target: int) -> List[List[int]]:
        """Find all the triplets whose sum equals the target value

        Time Complexity
            O(n^3)
        Space Complexity
            O(n)

        Args:
            arr (List[int]): array of integers in unknown order
            target (int): target value of summation
        Returns:
            List[List[int]]: triplet sums equal to the target value
        References:
            https://leetcode.com/problems/3sum/
        """

        # Init vars
        n: int = len(arr)
        sets = []

        for ix in range(0, n - 2):
            for jx in range(ix + 1, n - 1):
                for kx in range(jx + 1, n):

                    # Compute current sum
                    sum_i = arr[ix] + arr[jx] + arr[kx]

                    # Check if we meet our sum
                    if sum_i == target:

                        set_i = sorted([arr[ix], arr[jx], arr[kx]])

                        # Check if we've already included this
                        # TODO: this is a large piece of slowdown
                        if set_i not in sets:
                            sets.append(set_i)

        return sets

    def pointers(self, arr: List[int], target: int) -> List[List[int]]:
        """Find the triplet whose sum is equal to the target value

        Time Complexity
            O(n^2) for inner and outer loop
        Space Complexity
            O(n)

        Args:
            arr (list): array of integers in unknown order
            target (int): target value of summation
        Returns:
            int: triplet sum equal to the target value
        References:
            https://leetcode.com/problems/3sum
        """

        # Init vars
        n: int = len(arr)
        sets = []

        # Sort the array
        # O(n*log(n))
        arr = sorted(arr)

        # O(n)
        for ix in range(0, n - 2):

            # Only positives remain
            # We only sum to a positive number
            if target <= 0 and arr[ix] > 0:
                break

            # We've got duplicate starting values
            # ie: [0, 0, 1, 1, ] => skip the second 0 and second 1
            elif ix != 0 and arr[ix] == arr[ix - 1]:
                continue

            # Init pointers
            left: int = ix + 1
            right: int = n - 1

            # O(n)
            while left < right:

                # Prevent duplicates of the second value in our tuple
                # ie: [-4, -2, -2, -2, 0, 1, 2, 2, 2, 3, 3, 4, 4, 6, 6]
                # when ix_v = 4, we only need to test one of the -2
                if left != ix + 1 and arr[left] == arr[left - 1]:
                    left += 1
                    continue

                # Compute current sum
                sum_i = arr[ix] + arr[left] + arr[right]

                # We found the sum
                if sum_i == target:

                    tup = [arr[ix], arr[left], arr[right]]
                    sets.append(tup)

                    # Bump both pointers to prevent duplicates
                    left += 1
                    right -= 1

                # Too large
                elif sum_i > target:
                    right -= 1

                # Too small
                else:
                    left += 1

        return sets

    def threeSum_set(self, arr: List[int]) -> List[List[int]]:
        """Iterate throught the sorted list

        Outer loop = current value
        Inner Loop = secondary value

            Track the values we've seen here
            - if we find the complementing value, we've got a zero sum
            - the values are ordered

        Time Complexity
            O(n^2) for the double loop
        Space Complexity
            O(n^2) for tracking duplicates, answer, and also sorting method

            Set size may become O(n), while ans is also O(n)

        Notes
            - currently too small because of search in ans
                if ans_i not in ans: O(n)

            - instead prevent duplicate numbers from running
                ie: don't run arr[ix] = -1, then arr[ix + 1] = -1
                we'll find the same results
        """

        # Let's sort the array
        # O(n*log(n))
        arr = sorted(arr)

        n = len(arr)

        # Track duplicates and answers
        ans = []
        duplicates = set([])

        # Try loop
        for ix in range(0, n - 1):

            # If the current value is greater than 1, we cannot equal 0
            # ie: 2 + 3 + 4 > 0
            if arr[ix] > 0:
                break

            # Prevent duplicates
            elif ix > 0 and arr[ix] == arr[ix - 1]:
                continue

            # Save values and count
            s = set()

            for jx in range(ix + 1, n):

                # sum = arr[ix] + arr[jx] + z
                # 0 = arr[ix] + arr[jx] + z
                # z = -(arr[ix] + arr[jx])
                comp = -(arr[ix] + arr[jx])

                # We found the compliment
                if comp in s:

                    # We only need two of the 3 values for definition
                    # we know the 3rd value must equal our compliment
                    # for the target summation
                    tup = (arr[ix], comp)

                    if tup not in duplicates:

                        # Add the values in sorted order
                        ans_i = [arr[ix], comp, arr[jx]]
                        ans.append(ans_i)

                        # Add to our duplicate set
                        duplicates.add(tup)

                # Add current value to the set
                s.add(arr[jx])

        return ans


if __name__ == "__main__":

    # arr_in = [-1, 0, 1, 2, -1, -4]

    # Duplicate numbers can cause issues with duplicate entries
    # arr_in = [0, 0, 0, 0]
    # arr_in = [-2, 0, 0, 2, 2]
    arr_in = [-4, -2, -2, -2, 0, 1, 2, 2, 2, 3, 3, 4, 4, 6, 6]

    s = Solution()

    # print(s.naive(arr_in, 0))
    print(s.pointers(arr_in, 0))
    # print(s.threeSum_set(arr_in))
