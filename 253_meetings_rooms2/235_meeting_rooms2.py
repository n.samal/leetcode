"""
Inputs
    List[List[int]]: time intervals for meetings
Outputs
    int: min number of rooms required
Goal
    - find the minimum number of conference rooms required

Cases

    Meeting Conflict
        - Meeting 2 starts between Meeting 1 start to stop
    No Conflict

        - Meeting 2 starts after Meeting 1 ends.
        - it would be good to minimize the gap between meetings


Strategy

    Room Array with Comparison

        - first sort the intervals by the start time
            + this should allow for easier comparison between meeting conflicts
        - keep an array of array representing rooms
        - iterate through the time intervals and the existing rooms

            - find the meeting without a conflict and the smallest gap
            - add the meeting to that room

        - output the number of rooms needed

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)

    Room Heap

        - instead of storing rooms as an array, store them as a min heap
        - the min heap will be sorted according the end time of the meeting

            + the meeting with the smallest end time is checked first
            + if we can't put a meeting after this then we must allocate a new room
        
        - track the count of meeting conflicts

        Time Complexity
            O(n*log(n))
        Space Complexity
            O(n)
            
"""

from typing import List
import heapq as hq


class Solution:

    def minMeetingRooms(self, intervals: List[List[int]]) -> int:
        """Compare the current meeting to room schedules
        """

        # Number of rooms
        rooms = []

        # Sort the array by the start time, then end time
        intervals.sort(key=lambda x: (x[0], x[1]))

        # Add the first meeting to the room
        if intervals:
            rooms.append([intervals[0]])
        # No meetings
        else:
            return 0

        # Iterate through rest of the meetings
        for cur_start, cur_stop in intervals[1:]:

            min_gap_val: int = float("inf")
            min_gap_ix: int = None

            # Iterate through existing conference room schedules
            for room_ix in range(len(rooms)):

                # Compare against the last meeting in the room
                m2_start, m2_stop = rooms[room_ix][-1]

                # Time gap from last meeting
                delta = cur_start - m2_stop

                # Check for a conflict
                if delta >= 0 and delta < min_gap_val:

                    min_gap_val = delta
                    min_gap_ix = room_ix

            # Can't find a room without a conflict
            # make a new room
            if min_gap_ix is None:
                rooms.append([[cur_start, cur_stop]])

            # Add the time to the minimum room
            else:
                rooms[min_gap_ix].append([cur_start, cur_stop])

        return len(rooms)

    def minMeetingRooms2(self, intervals: List[List[int]]) -> int:
        """Compare the current meeting to room schedules

        - look for the first room available
            + since the meetings are sorted by start and stop
            the first available room should have the smallest stop

        """

        # Number of rooms
        rooms = []

        # Sort the array by the start time, then end time
        intervals.sort(key=lambda x: (x[0], x[1]))

        # Add the first meeting to the room
        if intervals:
            rooms.append([intervals[0]])
        # No meetings
        else:
            return 0

        # Iterate through rest of the meetings
        for cur_start, cur_stop in intervals[1:]:

            new_room_ix: int = None

            # Iterate through existing conference room schedules
            for room_ix in range(len(rooms)):

                # Compare against the last meeting in the room
                m2_start, m2_stop = rooms[room_ix][-1]

                # Time gap from last meeting
                delta = cur_start - m2_stop

                # Check for a conflict
                if delta >= 0:
                    new_room_ix = room_ix
                    break

            # Can't find a room without a conflict
            # make a new room
            if new_room_ix is None:
                rooms.append([[cur_start, cur_stop]])

            # Add the time to the minimum room
            else:
                rooms[new_room_ix].append([cur_start, cur_stop])

        return len(rooms)

    def min_meetings_heap(self, intervals: List[List[int]]) -> int:
        """Compare the current meeting to room schedules

        - look for the first room available
            + since the meetings are sorted by start and stop
            the first available room should have the smallest stop
        Notes
            - not significantly faster
        """

        # Meetings stored as a heap, (stop_time, start_time)
        heap = []
        hq.heapify(heap)

        # Number of rooms
        n_rooms: int = 1

        # Sort the array by the start time, then end time
        intervals.sort(key=lambda x: (x[0], x[1]))

        # Add the first meeting to the room, only use the end time
        if intervals:
            hq.heappush(heap, intervals[0][1])

        # No meetings
        else:
            return 0

        # Iterate through rest of the meetings
        for cur_start, cur_stop in intervals[1:]:

            # Compare against the meeting with the smallest end time
            # Time gap from last meeting
            delta = cur_start - heap[0]

            # Check for a conflict with the smallest end time
            # Add a new room if needed
            if delta < 0:
                n_rooms += 1
                hq.heappush(heap, cur_stop)

            # Update the room used, to have this meeting
            else:
                hq.heapreplace(heap, cur_stop)

        return n_rooms


if __name__ == "__main__":

    intervals_in = [[0, 30],[5, 10],[15, 20]]

    obj = Solution()
    print(obj.minMeetingRooms(intervals_in))
    # print(obj.minMeetingRooms2(intervals_in))
    print(obj.min_meetings_heap(intervals_in))
