"""
Inputs
    List[str]: words to abbreviate
Outputs
    List[str]: word abbreviations
Notes
    - generate the minimal possible abbreviation for every word
    - abbreviation order
        + first character
        + number of abbreviated characters
        + last character
    - if any conflict on abbreviation exists then uses a longer prefix
        + ie: an abbreviation can only map to 1 word
    - if the abbreviation is not shorter than the word, don't
    abberviate it
    - put the answers in the same order as original list
Example

    Input: ["like", "god", "internal", "me", "internet", "interval", "intension", "face", "intrusion"]
    Output: ["l2e","god","internal","me","i6t","interval","inte4n","f2e","intr4n"]

Ideas

    - words smaller than 3 chars can't be abbreviated

    - track the abbreviations in a hash
        + try abbreviating it if possible

    - if another abbreviation exist, we need to extend those abbreviations
    until they don't match

        + intension => i7n
        + intrusion => i7n

        try smaller

        + intension => in6n
        + intrusion => in6n

        try smaller

        + intension => int5n
        + intrusion => int5n

        try smaller

        + intension => inte4n
        + intrusion => intr4n

    - comparison between pairs is straightforward, what about n different words
    of the same abbreviation?

        + go over conflicting words, and extend the abbreviation if possible
        + try the word extension, and check for conflicts again

    - sorting the words by first n letters and last letter should put similar words near each other
        + we can determine conflicts more quickly
    - use a trie like structure to determine conflicts on words??
"""

from collections import defaultdict
from typing import List


class Solution:
    def wordsAbbreviation(self, words: List[str]) -> List[str]:

        self.convert = {}
        self.abbreviate(words)

        # Grab word conversions
        words_new = []

        for word in words:

            if len(word) <= 3:
                words_new.append(word)
            else:
                words_new.append(self.convert[word])

        return words_new

    def abbreviate(self, words: List[str], k: int = 1) -> dict:
        """Abbreviate words, and store conflicting words

        Args:
            words: words to abbreviate
            k: prefix length

        Time Complexity
            O(n*m)

            n = number of words
            m = length of word

        Space Complexity
            O(n)
        """

        working = {}
        conflicts = defaultdict(list)

        for word in words:

            n: int = len(word)

            if n <= 3:
                continue

            # Create abbreviation and look for conflicts
            else:
                key = word[:k] + str(n - (k + 1)) + word[-1]

                if len(key) >= n:
                    working[word] = word

                elif key in working:
                    conflicts[key].append(working.pop(key))
                    conflicts[key].append(word)

                elif key in conflicts:
                    conflicts[key].append(word)

                else:
                    working[key] = word

        # Save working abbreviations
        for key, word in working.items():
            self.convert[word] = key

        # Try a longer prefix
        if conflicts:
            for key in conflicts:
                self.abbreviate(conflicts[key], k + 1)


if __name__ == '__main__':

    # Example 1
    words = ["like", "god", "internal", "me", "internet", "interval", "intension", "face", "intrusion"]

    obj = Solution()
    print(obj.wordsAbbreviation(words))
