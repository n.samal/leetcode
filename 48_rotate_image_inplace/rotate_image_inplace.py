from typing import List
from copy import deepcopy
# from pprint import pprint

"""

Inputs
    arr (List[List[int]])
Outputs
    none
Goal:
    - rotate the matrix 90deg clockwise in place

Example

    Input
        [1, 2, 3]
        [4, 5, 6]
        [7, 8, 9]

    Output

        [7, 4, 1]
        [8, 5, 2]
        [9, 6, 3]

Other Transforms
    - investigate other transforms to see if we can leverage them

    Horizontal Flip aka row by row transform

        [3, 2, 1]
        [6, 5, 4]
        [9, 8, 7]

    Vertical Flip aka col by col transform

        [7, 8, 9]
        [4, 5, 6]
        [1, 2, 3]

    Transpose

        - swap each (row,col) => (col, row)

        [1, 4, 7]
        [2, 5, 8]
        [3, 6, 9]

        - https://en.wikipedia.org/wiki/Transpose
        - https://matrix.reshish.com/transpose.php

Cases

    - Transpose then horizontal flip

        - apply a transpose in place
            + keep in mind we only transpose half a matrix
            + if we run through all indices then everything gets
            flipped back

        - then iterate through each row and flip

    - Vertical flip then transpose

        - vertical flip (only do half)
            + iterate through each row (until half)
            + flip the row index

        - transpose in place (only do half the matrix)

"""

''' Transform Functions '''


def transpose_outplace(arr: List[List[int]]):
    """Tranpose the matrix out of place"""

    # Grab rows and columns
    n_rows = len(arr)
    n_cols = len(arr[0])

    # Normal matrix
    # arr_t = [[None for col_ix in range(n_cols)] for row_ix in range(n_rows)]

    # Create a new empty matrix
    arr_t = [[None for row_ix in range(n_rows)] for col_ix in range(n_cols)]

    # Iterate through the matrix
    for row_ix in range(n_rows):
        for col_ix in range(n_cols):

            # Grab the pivot point and save
            arr_t[col_ix][row_ix] = arr[row_ix][col_ix]

    return arr_t


def transpose_inplace(arr: List[List[int]]):
    """Tranpose a copy of the matrix in place

    using all indices... doesn't work!

    Notes:
        - as expected this doesn't do anything
        - if we go through the entire matrix
            + then we swap the positions back to their original position
    """

    # Grab rows and columns
    n_rows = len(arr)
    n_cols = len(arr[0])

    # Normal matrix
    arr_new = deepcopy(arr)

    # Iterate through the matrix
    for row_ix in range(n_rows):
        for col_ix in range(n_cols):

            # Swap indices
            arr_new[col_ix][row_ix], arr_new[row_ix][col_ix] = arr_new[row_ix][col_ix], arr_new[col_ix][row_ix]

    return arr_new


def transpose_inplace_tri(arr: List[List[int]]):
    """Tranpose a copy of the matrix in place

    swap the row and column positions
    only iterate through half the matrix (upper triangle)

    Notes:
        - this assume a square matrix
    """

    # Grab rows and columns
    n_rows = len(arr)
    n_cols = len(arr[0])

    # Normal matrix
    arr_new = deepcopy(arr)

    # Iterate through all rows
    for row_ix in range(n_rows):

        # Only start on the diagnol
        for col_ix in range(row_ix, n_cols):

            # Swap indices
            arr_new[col_ix][row_ix], arr_new[row_ix][col_ix] = arr_new[row_ix][col_ix], arr_new[col_ix][row_ix]

    return arr_new


def flip_horizontal(arr: List[List[int]]):
    """Flip the matrix horizontally in place"""

    # Normal matrix
    arr_new = deepcopy(arr)

    for row_ix in range(len(arr_new)):
        arr_new[row_ix].reverse()

    return arr_new


def flip_vertical(arr: List[List[int]]):
    """Flip the matrix vertically in place"""

    # Only go through half the rows
    # even case: n = 4, 0, 1, 2, 3
    #   half way 
    # odd case: n = 5, 0, 1, 2, 3, 4
    #   half way 

    # Normal matrix
    arr_new = deepcopy(arr)

    n = len(arr_new)

    # For half the rows
    for row_ix in range(0, n // 2):

        # Save the current row
        tmp = arr_new[row_ix]

        # Flip the rows
        arr_new[row_ix] = arr_new[n - row_ix - 1]
        arr_new[n - row_ix - 1] = tmp

    return arr_new


def print_matrix(arr: List[List[int]]):

    for row in arr:
        print(row)


''' Actual Functions '''


def rotate_image_inplace(arr: List[List[int]]):
    """Rotate the image by 90 degrees out of place

    - tranpose the matrix, then horizontal flip

    Time Complexity
        O(1/2 n^2 + n^2)

        - tranpose uses half the elements
        - all elements in row reversed, potentially half
    Space Complexity
        O(n x n)

    Args:
        arr: matrice of integers
    Returns:
        List[List[int]]
    """
    n_rows: int = len(arr)
    n_cols: int = len(arr[0])

    ''' Transpose matrix (in place) '''
    # Iterate through all rows
    for row_ix in range(n_rows):

        # Only start on the diagnol
        for col_ix in range(row_ix, n_cols):

            # Swap indices
            arr[col_ix][row_ix], arr[row_ix][col_ix] = arr[row_ix][col_ix], arr[col_ix][row_ix]

    # Reverse the rows
    for row_ix in range(len(arr)):
        arr[row_ix].reverse()


def rotate_image_inplace2(arr: List[List[int]]):
    """Rotate the image by 90 degrees out of place

    - vertical flip, then transpose
    - it works!

    Time Complexity
        O(1/2 n^2 + n^2)
    Space Complexity
        O(n x n)

    Args:
        arr: matrice of integers
    Returns:
        List[List[int]]
    Notes
        - slightly faster than method 1, but same theoretical time
    """
    n: int = len(arr)

    ''' Vertical Flip '''

    # For half the rows
    for row_ix in range(0, n // 2):

        # Save the current row
        # tmp = arr[row_ix]

        # Flip the rows
        # arr[row_ix] = arr[n - row_ix - 1]
        # arr[n - row_ix - 1] = tmp

        # Python style swap
        arr[row_ix], arr[n - row_ix - 1] = arr[n - row_ix - 1], arr[row_ix]

    ''' Transpose matrix (in place) '''

    # Iterate through all rows
    for row_ix in range(n):

        # Only start on the diagnol
        for col_ix in range(row_ix, n):

            # Swap indices
            arr[col_ix][row_ix], arr[row_ix][col_ix] = arr[row_ix][col_ix], arr[col_ix][row_ix]



if __name__ == "__main__":

    arr = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    # arr_in = [[1, 2, 3, 4, 5], [6, 7, 8, 9, 10], [11, 12, 13, 14, 15], [16, 17, 18, 19, 20], [21, 22, 23, 24, 25]]

    print_matrix(arr)
    print('')

    # print_matrix(transpose_outplace(arr))
    # print('')

    # print_matrix(transpose_inplace(arr))
    # print('')

    # print_matrix(transpose_inplace_tri(arr))
    # print('')

    # print_matrix(flip_horizontal(arr))
    # print_matrix(flip_vertical(arr))

    # rotate_image_inplace(arr)
    rotate_image_inplace2(arr)
    print_matrix(arr)
    print('')
