def romanToInt(s: str) -> int:
    """Convert the roman numeral to an integer value
    Account for numbers such as 'IV' = 4

    Args:
        s (string): string of roman characters
    Returns:
        int:
    """

    # String to integer conversion
    conversion = {'I': 1,
                  'IV': 4,
                  'V': 5,
                  'IX': 9,
                  'X': 10,
                  'XL': 40,
                  'L': 50,
                  'XC': 90,
                  'C': 100,
                  'CD': 400,
                  'D': 500,
                  'CM': 900,
                  'M': 1000}

    # Initialize sum
    n = len(s)
    summation: int = 0
    ix: int = 0

    # While we have characters to explore
    while ix < n:

        # Rename for ease
        char = s[ix]

        # The double character is in the dictionary
        if ix + 1 < n and s[ix:ix + 2] in conversion:

            summation += conversion[s[ix:ix + 2]]

            # Skip two chars
            ix += 2

        # Add character and increment
        else:

            summation += conversion[char]
            ix += 1

    return summation


if __name__ == "__main__":

    print(romanToInt('III'))
    print(romanToInt('IV'))
    print(romanToInt('IX'))
    print(romanToInt('LVIII'))
    print(romanToInt('MCMXCIV'))
