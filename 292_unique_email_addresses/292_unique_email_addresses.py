
"""

Goal:
    - Find all unique email addresses
    - ignore '.' in the local name
    - ignore all strings after '+' in the local name

Examples:

    ["test.email+alex@leetcode.com","test.e.mail+bob.cathy@leetcode.com","testemail+david@lee.tcode.com"]
    Explanation: "testemail@leetcode.com" and "testemail@lee.tcode.com" actually receive mails

Strategy

    Iterate through

        - iterate through the string and track characters
        - use a set to track unique emails
        - once we've hit a '@' character we know the local name starts, and domain name starts
        - once we've hit a '+' the local name starts but we don't know where the domain name starts
        - add a cleaned email address to our set
        - count the length of the set

        Time Complexity
            O(n)
        Space Complexity
            O(n)



"""

from typing import List


class Solution:

    def numUniqueEmails(self, emails: List[str]) -> int:

        # Check track of unique emails
        s = set([])

        count = 0

        # Go through each email
        for email in emails:

            # Get length of string
            n = len(email)

            # Intitialize values
            ix = 0
            email_clean = ''
            ix_at = None

            # Iterate through the string
            while ix < n:

                # The domain name is about to start
                if email[ix] == '@':
                    ix_at = ix
                    break

                # Ignore everything else
                elif email[ix] == '+':
                    break

                # Ignore the '.'
                elif email[ix] == '.':
                    pass

                # Add characters to our string
                else:
                    email_clean += email[ix]

                # Increment the string
                ix += 1

            # Find the '@' symbol if we haven't found it yet
            while ix_at is None:

                if email[ix] == '@':
                    ix_at = ix
                    break

                # Increment string
                ix += 1

            # Combine the local name with '@' and the domain
            email_clean += email[ix_at:]

            # Add the cleaned email to our list
            if email_clean not in s:
                s.add(email_clean)
                count += 1

        return count


if __name__ == '__main__':


    # arr = ["test.email+alex@leetcode.com","test.e.mail+bob.cathy@leetcode.com","testemail+david@lee.tcode.com"]
    arr = ["test.email+alex@leetcode.com", "test.email.leet+alex@code.com"]

    obj = Solution()
    print(obj.numUniqueEmails(arr))
