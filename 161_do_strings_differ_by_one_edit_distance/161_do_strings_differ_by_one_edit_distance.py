"""
Inputs
    source (string): starting string
    target (string): goal string
Outputs
    bool: are strings one edit distance apart

Notes
    A string s is said to be one distance apart from a string t if you can:
    - Insert exactly one character into s to get t.
    - Delete exactly one character from s to get t.
    - Replace exactly one character of s with a different character to get t.

Ideas
    - if strings match, then it's false
    - determine the case to look for using the string lengths

        + same length = replacement
        + shorter t   = deletion
        + longer  t   = insertion

    - find the first non matching character, then check
    that the remainder of the string is the same

    - null cases, can create issues where we search invalid bounds
        + use a while loop to prevent iteration into invalid bounds

        + use a check on length for null cases
        ie: ''  vs 'a'
            'a' vs ''

"""


class Solution:
    def isOneEditDistance(self, source: str, target: str) -> bool:
        """

        - use the string lengths to determine which route could be taken
            + replacement
            + insertion
            + deletion

        - compare the string to determine the 1st non matching character
        - compare the remainder of the string to see if a valid match
        could occur

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        Notes
            - works but breaks on some cases like
                source = 'a'
                target = 'ac'
            - null string handling could be better
        """

        S: int = len(source)
        T: int = len(target)

        # Too many differences
        if abs(S - T) > 1:
            return False

        # Strings are equal
        if source == target:
            return False

        if not source:
            return T == 1

        if not target:
            return S == 1

        # Compare characters until we hit a difference
        for ix in range(S):
            if source[ix] != target[ix]:
                break

        ''' Check for a replacement, insertion or deletion '''

        # Replacement occured, check similarity of remainder
        # abcdefg
        # abcHefg
        if S == T:
            return source[ix + 1:] == target[ix + 1:]

        # Insertion check
        # a|b
        # a|cb
        elif T > S:
            return source[ix:] == target[ix + 1:]

        # Deletion check
        # abc|defg
        # abc|efg
        else:
            return source[ix + 1:] == target[ix:]

    def isOneEditDistance2(self, source: str, target: str) -> bool:
        """

        - compare the string to determine the 1st non matching character
        - compare the remainder of the string to see if a valid match
        could occur

        - use while loops to prevent iteration over null cases
            + substrings only gathered on valid strings

        Time Complexity
            O(n)
        Space Complexity
            O(n) substring comparison
        Notes
            - out of bound substring extraction on a valid string results
            in a null  ie: abcde[5:] => ''
        """

        S: int = len(source)
        T: int = len(target)

        # Too many differences
        if abs(S - T) > 1:
            return False

        # Compare characters until we hit a difference
        s: int = 0

        while s < S and s < T:

            if source[s] != target[s]:

                # Replacment check
                if S == T:
                    return source[s + 1:] == target[s + 1:]

                # Insertion check
                elif T > S:
                    return source[s:] == target[s + 1:]

                # Deletion check
                else:
                    return source[s + 1:] == target[s:]

            s += 1

        # Ensure only 1 character difference
        return abs(S - T) == 1


if __name__ == '__main__':

    # Insertion
    # source_in = 'ab'
    # target_in = 'acb'

    # Null case
    # source_in = 'a'
    # target_in = ''

    # Insertion near bounds case
    # source_in = 'a'
    # target_in = 'ac'

    # Replacement case near bounds
    source_in = 'abcd'
    target_in = 'abce'


    obj = Solution()
    # print(obj.isOneEditDistance(source_in, target_in))
    print(obj.isOneEditDistance2(source_in, target_in))
