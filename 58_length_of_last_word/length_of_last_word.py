

class Solution:

    def lengthOfLastWord(self, s: str) -> int:
        """Find the length of the last word

        Args:
            s (str): string of unknown length
        Returns:
            int: length of last word
        """

        # Starting index
        ix = len(s) - 1
        start = None

        ''' Find the first character '''

        # Iterate through string backwards until there's a character
        while ix >= 0 and s[ix] == ' ':
            ix -= 1

        # No character found
        if ix < 0:
            return 0

        # Save location of character
        else:
            start = ix

        ''' Find the second character '''

        # Iterate through string backwards
        while ix >= 0 and s[ix] != ' ':
            ix -= 1

        return start - ix


if __name__ == "__main__":

    s = Solution()
    # print(s.lengthOfLastWord("    "))
    print(s.lengthOfLastWord("a"))
    # print(s.lengthOfLastWord("Hello World"))
    # print(s.lengthOfLastWord("  Hello World   "))
    # print(s.lengthOfLastWord("World"))
