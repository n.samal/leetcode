"""
Inputs
    s (int): target sum bound (always positive)
    arr (List[int]): positive integers
Outputs
    int: size of the minimum subarray
Notes
    - find the minimal length of a contiguous subarray where
    the sum >= s
    - if no such array exists output 0
Example

    Input: s = 7, nums = [2,3,1,2,4,3]
    Output: 2
    Explanation: the subarray [4,3] has the minimal length under the problem constraint.

Ideas
    - compute the cummulative sum to save subarray summation time

    - as we compute the cumsum, we can search backwards for the "compliment"
        + look for values >= s

        cur_sum - previous >= target_sum

        we want target_sum >= s

        find if values of the size previous exist, or smaller

        cur_sum - target_sum >= previous

    - since the summation is monotonically increasing
    we can apply binary search to find if such a value exists

Hand Calc

    nums =      [2, 3, 1, 2,  4,  3]
    cumsum = [0, 2, 5, 6, 8, 12, 15]

    at index 1:
        cumsum = 2
        target_sum > cumsum
    at index 2:
        cumsum = 5
        target_sum > cumsum
    at index 3:
        cumsum = 5
        target_sum > cumsum
    at index 4:
        cumsum = 5
        target_sum > cumsum

"""

from typing import List


class Solution:
    def two_loops(self, s: int, arr: List[int]) -> int:
        """Minimum subarray

        - compute cumsum
        - extend our subarray lower bound trying to meet our criteria
            + stop after we've found the first valid subarray

        Time Complexity
            O(n^2)

            each n we potentially explore all n

        Space Complexity
            O(n)
        Notes
            - too slow
        """

        n: int = len(arr)
        ans: int = float('inf')

        cum_sum = [0 for ix in range(n + 1)]

        for ix in range(1, n + 1):
            cum_sum[ix] = cum_sum[ix - 1] + arr[ix - 1]

            # Only consider valid situations
            if cum_sum[ix] >= s:

                # Search for the shortest subarray
                # with summation >= s
                jx = ix - 1
                cur_sum = 0

                # Extend our lower bounds
                # and stop after the first time we meet our target sum
                while jx >= 0 and cur_sum < s:
                    cur_sum = cum_sum[ix] - cum_sum[jx]
                    jx -= 1

                # Save our best answer
                ans = min(ans, ix - (jx + 1))

        if ans == float('inf'):
            return 0
        else:
            return ans

    def binary_search(self, s: int, arr: List[int]) -> int:
        """Minimum subarray with binary search

        - compute cumsum
        - use binary search to find summations that met our target

        Time Complexity
            O(n*ln(n))

            each n we use binary search to find the target sum

        Space Complexity
            O(n)
        """

        n: int = len(arr)
        ans: int = float('inf')

        cum_sum = [0 for ix in range(n + 1)]

        for ix in range(1, n + 1):
            cum_sum[ix] = cum_sum[ix - 1] + arr[ix - 1]

            # Only consider valid situations
            if cum_sum[ix] >= s:

                low = 0
                high = ix - 1

                while low <= high:

                    mid = (low + high) // 2
                    cur_sum = cum_sum[ix] - cum_sum[mid]

                    # Summation mets our goal
                    if cur_sum >= s:
                        low = mid + 1
                    else:
                        high = mid - 1

                # Save our best answer
                # bump low index to last working one
                ans = min(ans, ix - (low - 1))

        if ans == float('inf'):
            return 0
        else:
            return ans

    def two_pointers(self, s: int, arr: List[int]) -> int:
        """Minimum subarray with two pointers

        - compute cumsum
        - reduce the subarray size once we meet our target sum
            + caterpillar method!

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        Notes
            - summations are inclusive, inclusive so need to calculate
            lengths accordingly
        """

        n: int = len(arr)
        ans: int = float('inf')

        cur_sum = 0

        low = 0

        for high in range(n):

            # Increase subarray size
            cur_sum += arr[high]

            # Reduce subarray size
            while cur_sum >= s:

                ans = min(ans, high - low + 1)

                cur_sum -= arr[low]
                low += 1

        if ans == float('inf'):
            return 0
        else:
            return ans


if __name__ == '__main__':

    s = 7
    nums = [2, 3, 1, 2, 4, 3]

    obj = Solution()
    print(obj.two_loops(s, nums))
    print(obj.binary_search(s, nums))
    print(obj.two_pointers(s, nums))
