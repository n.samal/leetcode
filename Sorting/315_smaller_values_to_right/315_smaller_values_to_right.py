"""

Inputs
    nums: integers in unknown order
Outputs
    List[int]: number of integers smaller than current value

Example

    Input: [5,2,6,1]
    Output: [2,1,1,0] 
    Explanation:
    To the right of 5 there are 2 smaller elements (2 and 1).
    To the right of 2 there is only 1 smaller element (1).
    To the right of 6 there is 1 smaller element (1).
    To the right of 1 there is 0 smaller element.

Case
    Null: return Null
    Single Value: 0
    General Case

    Increasing Order
        - no values to right are smaller
        ie: [5, 6, 7, 8, 10, 20]

    Decreasing Order
        - all values to right are smaller
        ie: [20, 15, 13, 10, 9]

Ideas
    - sort the values, and track indices
    - use a stack?

Strategy

    Brute Force

        - at each index, check all values to the right
        - maintain a count of values less than current

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)

        - seems too slow

    Sorting Idea?

        indices: [0,1,2,3]
        Input:   [5,2,6,1]

        sorted:  [1,2,5,6]
        indices: [3,1,0,2]

        sorted array:
            0: 0 smaller vaulues possible
            1: 1 smaller values possible
            2: 2 smaller values possible
            3: 3 smaller values possible

            is the index for the possible smaller values, larger then our original index?

            0: No smaller possible
            1: 1 smaler possible,
                smaller values = [3]
                original index = [1]
                count = 1

        - we still need to perform checks on values.. doesn't help too much

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)

    Insertion Sort

        reverse the numbers
        arr    : [5, 2, 6, 1]
        arr_rev: [1, 6, 2, 5]

        - create empty sorted array
        - iterate from left to right in reversed array
        - for each value find the position to put into a sorted order
            + insertion location tells us how many values to the right are smaller than our current value

        - save those positions or counts into an array (counts)
        
        counts = []
    
        value = 1
        sorted = []
        insertion location: 0
        counts: [0]

        value = 6
        sorted = [1]
        insertion location: 1
        counts: [0, 1]

        value = 2
        sorted: [1, 6]
        insertion location: 1
        counts: [0, 1, 1]

        value = 5
        sorted: [1, 2, 6]
        insertion location: 2
        counts: [0, 1, 1, 2]

        sorted: [1, 2, 5, 6]

        # Reverse the counts

        Time Complexity
        - first reversal O(n)
        - for loop: O(n)
            - find sorting position: Bisection O(lg(n))
            - insertion into sorted array: O(n)

        - last reversal O(n)

        Time Complexity: O(n^2)
        Space Complexity: O(n)

        https://docs.python.org/3.7/library/bisect.html has insertion sort method O(n)

    Merge Sort
        - use the merge sort to sort values while also tracking values to the right

            ie:
            arr = [5, 2, 7, 1]

            left = [5, 2]   right = [7, 1]

            left = [5] right = [2]      left = [7]  right = [1]

        - as we start recombining the subarrays, we are comparing items to our right

            [5] [2] [7] [1]
            [2, 5] [1, 7]
            [1, 2, 5, 7]

            + when we encounter a smaller value on the right, we mark the index in our counts
            + in order to do this we should track the indices for each value

        + the recombination of values

        References:
            https://leetcode.com/problems/count-of-smaller-numbers-after-self/discuss/445769/merge-sort-CLEAR-simple-EXPLANATION-with-EXAMPLES-O(n-lg-n)
"""

import bisect
from typing import List


class Solution:
    def bruteforce(self, arr: List[int]) -> List[int]:
        """Apply brute force solution"""
        n: int = len(arr)

        if n == 0:
            return []

        arr_smaller = [0 for _ in range(n)]

        for ix in range(n):

            n_smaller: int = 0

            # Check values to the right
            for jx in range(ix + 1, n):
                if arr[jx] < arr[ix]:
                    n_smaller += 1

            # Save value
            arr_smaller[ix] = n_smaller

        return arr_smaller

    """ Insertion Sort """

    def insertion_in_reverse(self, arr: List[int]) -> List[int]:
        """Insertion sort uses to determine count of smaller values

        - Reverse the array
        - maintain a sorted list
        - determine where to place the value for sorting


        Time Complexity
            O(n^2)
        Space Complexity
            O(n)

        References:
            https://docs.python.org/3/library/bisect.html
        """

        counts = []
        sort = []

        # Iterate in reverse
        for val in arr[::-1]:

            # Find location to insert current value in sorted array
            ix = bisect.bisect(sort, val)
            counts.append(ix)

            # Use insertion sort into sorted array
            bisect.insort(sort, val)

        # Reverse counts
        return counts[::-1]

    def insertion_in_reverse2(self, arr: List[int]) -> List[int]:
        """Insertion sort uses to determine count of smaller values

        - Reverse the array
        - maintain a sorted list
        - determine where to place the value for sorting


        Time Complexity
            O(n^2)
        Space Complexity
            O(n)

        References:
            https://docs.python.org/3/library/bisect.html
        """

        # Null case
        n: int = len(arr)
        if n == 0:
            return []
        elif n == 1:
            return [0]

        # Counts at each index
        counts = [0 for _ in range(n)]

        # Values to the right
        right = [arr[-1]]

        for ix in range(n - 2, -1, -1):

            # Find position of value relative to
            # the sorted values on our right
            jx = bisect.bisect_left(right, arr[ix])

            # Save number of values to the right
            counts[ix] = jx

            # Insert the current value, into the right values
            bisect.insort(right, arr[ix])

        return counts

    """ Merge Sort """

    def merge_sort_only(self, arr: List[int]) -> List[int]:
        """Normal out of place merge sort

        Notes:
            - use this for inspiration
        """

        n: int = len(arr)
        mid = n // 2

        if n == 1:
            return arr

        # Split the array
        left = arr[:mid]
        right = arr[mid:]

        # Merge the subinterval
        left = self.merge_sort(left)
        right = self.merge_sort(right)

        # Combine the intervals
        l1 = len(left)
        l2 = len(right)

        ix: int = 0
        jx: int = 0

        arr_new = []

        while ix < l1 and jx < l2:

            # Add left if smaller
            if left[ix] <= right[jx]:
                arr_new.append(left[ix])
                ix += 1

            # Add right otherwise
            else:
                arr_new.append(right[jx])
                jx += 1

        # Add remaining values if any
        if ix < l1:
            arr_new.extend(left[ix:])

        elif jx < l2:
            arr_new.extend(right[jx:])

        return arr_new

    ''' Merge & Count: without bounds '''

    def merge_and_count(self, arr: List[int]) -> List[int]:
        """Use merge sort on indices and track smaller values

        Time Complexity
            O(n*log(n))
        Space Complexity
            O(n)
        """

        # Null case
        if not arr:
            return []

        # Create counts and indices
        n: int = len(arr)
        self.arr = arr
        self.counts = [0 for ix in range(n)]
        indices = [ix for ix in range(n)]

        self.merge_sort_count_bounds(indices, 0, n)

        return self.counts

    def merge_sort_count1(self, indices: List[int]) -> List[int]:
        """Merge sort indices of array based upon value while counting

        Args:
            arr: indices of class array variable
        Notes:
            - indices out of place
            - runs out of time
        """

        n: int = len(indices)
        mid = n // 2

        if n == 1:
            return indices

        # Merge the subintervals
        left = self.merge_sort_count(indices[:mid])
        right = self.merge_sort_count(indices[mid:])

        # Combine the intervals
        l1 = len(left)
        l2 = len(right)

        ix: int = 0
        jx: int = 0
        kx: int = 0

        # indices_new = [None for _ in range(n)]

        while ix < l1 and jx < l2:

            # Add left index if left value is smaller
            if self.arr[left[ix]] <= self.arr[right[jx]]:
                indices[kx] = left[ix]
                ix += 1

            # Add right index otherwise
            else:

                # Add the count for all greater values
                #  ie: [2, 5] vs [1, 7]
                #  1 is smaller, so we mark
                #  a smaller value for [2,5]
                for lx in left[ix:]:
                    self.counts[lx] += 1

                indices[kx] = right[jx]
                jx += 1

            # Increment global position
            kx += 1

        # Add remaining values if any
        while ix < l1:
            indices[kx] = left[ix]
            ix += 1
            kx += 1

        while jx < l2:
            indices[kx] = right[jx]
            jx += 1
            kx += 1

        return indices

    def merge_sort_count_bounds(self, indices: List[int], low: int, high: int) -> List[int]:
        """Merge sort indices of array based upon value while counting

        Args:
            arr: indices of class array variable
            low: lower bound of sort (inclusive)
            high: upper bound of sort (exclusive)
        Notes:
            - track the number of right values added

            [2, 5] [1, 6]

            1 < 2: add 1 (right count = 1)
            2 < 6: add 2 (1 smaller value to the right)
            5 < 6: add 5 (1 smaller value to the right)
            6 is left: add 6

            [1, 2, 5, 6]

            here 1 is smaller than both 2 and 5

            When we remove values from our left subarray
            we increment the counts at that time
        """

        n: int = high - low
        mid = low + (n // 2)

        print(f'low: {low}  high: {high}  mid: {mid}')

        if n <= 1:
            return

        # Merge the subintervals
        self.merge_sort_count_bounds(indices, low, mid)
        self.merge_sort_count_bounds(indices, mid, high)

        # Combine the intervals
        ix: int = low  # left subarray index
        jx: int = mid  # right subarray index
        kx: int = 0  # sorted subarray index

        right_count: int = 0  # number of items take from right

        indices_new = [None for _ in range(n)]

        while ix < mid and jx < high:

            # Add left index if left value is smaller
            if self.arr[indices[ix]] <= self.arr[indices[jx]]:

                # Increment count for smaller right values we've seen so far
                self.counts[indices[ix]] += right_count

                indices_new[kx] = indices[ix]
                ix += 1

            # Right value is smaller
            else:

                indices_new[kx] = indices[jx]
                right_count += 1
                jx += 1

            # Increment global position
            kx += 1

        # Add remaining values if any
        while ix < mid:

            self.counts[indices[ix]] += right_count
            indices_new[kx] = indices[ix]

            ix += 1
            kx += 1

        while jx < high:
            indices_new[kx] = indices[jx]
            jx += 1
            kx += 1

        # Replace old indices
        indices[low:high] = indices_new


if __name__ == "__main__":

    # Example
    arr = [5, 2, 7, 1]

    obj = Solution()
    print(obj.merge_and_count(arr))
