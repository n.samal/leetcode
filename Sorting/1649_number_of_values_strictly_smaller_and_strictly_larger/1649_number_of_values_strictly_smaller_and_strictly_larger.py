"""
Inputs
    instructions (List[int]): values to add in order
Outputs
    int: total cost of created sorted array

Notes
    - we start with an empty container
    - for each element in the instructions we must add them to
    create a sorted array
    - the cost of insertions is the minimum of
        + number of strictly smaller elements than current value in nums
        + number of strictly larger elements than current value in nums
    - 

Ideas
    - maintain a sorted array
        + use binary search with insertion sort
        + binary search left to find strictly smaller values to the left
        + binary search right to find strictly larger values to the right

        + add the current cost to the total cost

    - apply merge sort technique
        + count number of larger values to the right
        + count number of smaller values to the left

"""

import bisect
from typing import List

MOD = 10**9 + 7


class Solution:

    def create_by_insertion(self, instructions: List[int]) -> int:
        """Apply insertion sort with binary search to find cost

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        """

        arr = []  # sorted array
        cost_total: int = 0

        for v in instructions:

            # Find number of values strictily less than value
            ix_less = bisect.bisect_left(arr, v)

            # Find number of values strictily greater than value
            ix_greater = len(arr) - bisect.bisect(arr, v)

            cost = min(ix_less, ix_greater)
            cost_total += cost

            # Add the value in sorted order
            bisect.insort(arr, v)

            # print(f'cost: {cost}   arr: {arr}')

        return cost_total % MOD

    def create_by_mergesort(self, instructions: List[int]) -> int:
        """Use divide and conquer to compare values to the right

        - merge sort once for smaller values to the left
        - merge sort again for larger values to the left

        Time Complexity
            O(n*log(n))
        Space Complexity
            O(n)
        """

        n: int = len(instructions)
        self.count_smaller = [0 for _ in range(n)]
        self.count_larger = [0 for _ in range(n)]

        # Save original array, and indices to sort
        self.arr = instructions
        arr_smaller = [i for i in range(n)]
        arr_larger = [i for i in range(n)]

        # Merge sort to calculate count of less and smaller numbers
        self.merge_sort_count_smaller_to_left(arr_smaller, 0, n)
        self.merge_sort_count_greater_to_left(arr_larger, 0, n)

        # Compute cost for each value
        cost_total: int = 0

        for ix in range(n):
            cost_total += min(self.count_smaller[ix], self.count_larger[ix])

        return cost_total % MOD

    def merge_sort_count_smaller_to_left(self, indices: int, start: int, stop: int):
        """Merge sort and a count

        Args:
            indices (List[int]): indices of original array to sort
            start (int): starting index (inclusive)
            stop (int): ending index (exclusive)
        """

        if stop - start == 1:
            return

        mid = start + (stop - start) // 2

        # Sort indices through divide and conquer
        self.merge_sort_count_smaller_to_left(indices, start, mid)
        self.merge_sort_count_smaller_to_left(indices, mid, stop)

        # Define pointers (local and global)
        l: int = start
        r: int = mid
        g: int = 0

        indices_new = [None for _ in range(start, stop)]
        count: int = 0  # number of smaller values

        # Grab the smaller valued index first
        # track how many smaller left values we have
        while l < mid and r < stop:

            if self.arr[indices[l]] < self.arr[indices[r]]:
                count += 1
                indices_new[g] = indices[l]
                l += 1
            else:
                self.count_smaller[indices[r]] += count
                indices_new[g] = indices[r]
                r += 1

            g += 1

        # Remaining left array
        while l < mid:
            indices_new[g] = indices[l]
            l += 1
            g += 1

        # Remaining right array
        while r < stop:
            self.count_smaller[indices[r]] += count
            indices_new[g] = indices[r]
            r += 1
            g += 1

        # Overwrite the old indices
        indices[start:stop] = indices_new

    def merge_sort_count_greater_to_left(self, indices: int, start: int, stop: int):
        """Merge sort and a count

        Args:
            indices (List[int]): indices of original array to sort
            start (int): starting index (inclusive)
            stop (int): ending index (exclusive)
        """

        if stop - start == 1:
            return

        mid = start + (stop - start) // 2

        # Sort indices through divide and conquer
        self.merge_sort_count_greater_to_left(indices, start, mid)
        self.merge_sort_count_greater_to_left(indices, mid, stop)

        # Define pointers (local and global)
        l: int = start
        r: int = mid
        g: int = 0

        indices_new = [None for _ in range(start, stop)]
        count: int = 0  # number of smaller values

        # Grab the greater valued index first
        # track how many greater left values we have
        while l < mid and r < stop:

            if self.arr[indices[l]] > self.arr[indices[r]]:
                count += 1
                indices_new[g] = indices[l]
                l += 1
            else:
                self.count_larger[indices[r]] += count
                indices_new[g] = indices[r]
                r += 1

            g += 1

        # Remaining left array
        while l < mid:
            indices_new[g] = indices[l]
            l += 1
            g += 1

        # Remaining right array
        while r < stop:
            self.count_larger[indices[r]] += count
            indices_new[g] = indices[r]
            r += 1
            g += 1

        # Overwrite the old indices
        indices[start:stop] = indices_new


if __name__ == '__main__':

    instructions = [1, 5, 6, 2]
    instructions = [1, 2, 3, 6, 5, 4]

    obj = Solution()
    print(obj.create_by_mergesort(instructions))
