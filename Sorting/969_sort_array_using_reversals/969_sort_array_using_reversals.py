"""
Inputs
    arr (List[int]): values in unknown order
Outputs
    List[int]: list of instructions to sort
Notes
    - in pancake flip we reverse the first k values
        1 <= k <= arr.length
    
    if arr = [3,2,1,4] and we performed a pancake flip choosing k = 3, 
    we reverse the sub-array [3,2,1], so arr = [1,2,3,4] after the pancake flip at k = 3.

    - return any valid answer

Example 

    Example 1

        Input: arr = [3,2,4,1]
        Output: [4,2,4,3]
        Explanation: 
        We perform 4 pancake flips, with k values 4, 2, 4, and 3.
        Starting state: arr = [3, 2, 4, 1]
        After 1st flip (k = 4): arr = [1, 4, 2, 3]
        After 2nd flip (k = 2): arr = [4, 1, 2, 3]
        After 3rd flip (k = 4): arr = [3, 2, 1, 4]
        After 4th flip (k = 3): arr = [1, 2, 3, 4], which is sorted.
        Notice that we return an array of the chosen k values of the pancake flips.

    Example 2
    
        Input: arr = [1,2,3]
        Output: []
        Explanation: The input is already sorted, so there is no need to flip anything.
        Note that other answers, such as [3, 3], would also be accepted.

Hand Calc

    array =  [3, 2, 4, 1]
    sorted = [1, 2, 3, 4]

    goal value = 4

    bring goal to front

    reverse first 3
    arr = [3, 2, 4, 1]
    arr = [4, 2, 3, 1]

    reverse all four to bring it, 4

    arr = [1, 3, 2, 4]

    goal value = 3
    reverse 2
    arr = [3, 1, 2, 4]

    reverse to back 3
    arr = [2, 1, 3, 4]

    arr = [2, 1, 3, 4]

""" 

class Solution(object):
    def pancakeSort(self, arr):
        """Bubble like

        - use bubble like sort
        - find the largest value in the sorted array
        - bring it to the front of the unsorted array through reversal
        then reverse it back to the sorted position
        - do this for entire array

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        """

        # Find the correct order
        arr_srt = sorted(arr)
        n = len(arr_srt)

        if arr == arr_srt:
            return []

        # We want to bring the largest number to the front
        # reverse it, then it'll be placed at the end
        # continue this process, until only 1 unsorted value

        order = []

        while len(arr_srt) > 1:

            target = arr_srt.pop()

            # Find the first location of the value
            ix = arr.index(target) + 1

            # Reverse target to the front
            order.append(ix)
            arr[:ix] = arr[:ix][::-1]

            # Reverse the list and bring it to the correct position
            # at the end
            order.append(n)
            arr[:n] = arr[:n][::-1]

            # Reduce unsorted count
            n -= 1

        return order
