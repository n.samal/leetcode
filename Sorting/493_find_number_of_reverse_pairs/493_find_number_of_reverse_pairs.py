"""
Inputs
    arr (List[int]): pairs in unknown order
Outputs
    int: number of reverse pairs
Notes
    - reverse pair is defined by
        + i < j
        + nums[i] > 2 * nums[j]

Examples

    Example1:

        Input: [1,3,2,3,1]
        Output: 2

    Example2:

        Input: [2,4,3,5,1]
        Output: 3

Ideas

    - iterate over values
        + at the current value see if there's any previous numbers
        twices as large as the current number
        + works but probably too expensive in time

    - use a hash to track values??
        + may still lead to O(n^2) time complexity, since
        we have to check all prior keys

    - using merge sort
        + comparison to a value on the right is occuring
        during the process. We leverage this process
        + problem 315 (smaller values to the right) is similar

"""

from typing import List


class Solution:

    def brute_force(self, arr: List[int]) -> int:
        """Check all pairs manually

        Time Complexity
            O(n^2)
        Space Complexity
            O(1)
        """

        n: int = len(arr)
        count: int = 0

        for ix in range(n - 1):
            for jx in range(ix + 1, n):

                if arr[ix] > 2 * arr[jx]:
                    count += 1

        return count

    def merge_sort_count(self, arr: List[int]) -> int:
        """Modified merge sort

        - break the array into halfs
        - when arrays are single values we start recombining
            + during recombination we know the right values
            are on the right. here we can check for values
            being larger then 2 of the other value

        Time Complexity
            O(n * log(n))
        Space Complexity
            O(1)
        """

        self.count: int = 0
        self.merge_sort(arr)

        return self.count

    def merge_sort(self, arr: List[int]):
        """Sort values in place

        Time Complexity
            O(n * log(n))
        Space Complexity
            O(1)
        """

        n: int = len(arr)

        if n > 1:

            mid = n // 2

            # Split array in half
            left = arr[:mid]
            right = arr[mid:]

            # Recursively sort the halfs
            self.merge_sort(left)
            self.merge_sort(right)

            # Start recombining the array in place
            l: int = 0  # pointer in left
            r: int = 0  # pointer in right
            i: int = 0  # pointer in main array

            n_l: int = len(left)
            n_r: int = len(right)

            # Count the number of special pairs
            # look for larger numbers while we meet our criteria
            for l in range(n_l):

                while r < n_r and left[l] > 2 * right[r]:
                    r += 1

                # For this left we're also bigger than all previous rights
                # ie: [2, 4], [1, 1, 3]
                # when l_val = 4, and r_val = 3, we know we're bigger than 2 other values
                self.count += r

            # Reset pointers
            l: int = 0
            r: int = 0

            while l < n_l and r < n_r:

                if left[l] <= right[r]:
                    arr[i] = left[l]
                    i += 1
                    l += 1

                else:
                    arr[i] = right[r]
                    i += 1
                    r += 1

            # Add remaining left pieces
            while l < n_l:

                arr[i] = left[l]
                i += 1
                l += 1

            # Add remaining right pieces
            while r < n_r:
                arr[i] = right[r]
                i += 1
                r += 1


if __name__ == '__main__':

    # Example 1 (2)
    arr = [1, 3, 2, 3, 1]

    # Example 2 (3)
    # arr = [2, 4, 3, 5, 1]

    obj = Solution()
    print(obj.merge_sort_count(arr))
