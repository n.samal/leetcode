"""
Inputs
    s (str): original number
    k (int): max number of swaps
Outputs
    str: minimum integer

Notes
    - we must can swap adjacent digits at most k times
    - find the minimum value possible

Examples

    Example 1:

        Input: num = "4321", k = 4
        Output: "1342"
        Explanation: The steps to obtain the minimum integer from 4321 with 4 adjacent swaps are shown.

    Example 2:

        Input: num = "100", k = 1
        Output: "010"
        Explanation: It's ok for the output to have leading zeros, but the input is guaranteed not to have any leading zeros.
    
    Example 3:

        Input: num = "36789", k = 1000
        Output: "36789"
        Explanation: We can keep the number without any swaps.
    
    Example 4:

        Input: num = "22", k = 22
        Output: "22"
    
    Example 5:

        Input: num = "9438957234785635408", k = 23
        Output: "0345989723478563548"


Ideas
    - if the number of swaps is much larger then the length
        our answer is the value sorted (smallest to largest)

    - if the numbers are all the same, then output the value

    - bring the smallest digit to the left most position
        + only consider 'unsorted' values as candidates

    - apply a bubble sort like approach to bring the smallest values left
        + if no more swaps available then stop

        + maybe track the index of where sorted values start

        + how to find the next smallest value?

            * iterate all allowed characters and save

            * sort all the values then track when values are left to be used

            * maybe keep track of the indices for each value
              as we swap, update the position locations

            * when a value is sorted, remove it from our unsorted queue,
            if no more values, then remove that key

            .. this could be expensive

References
    - https://leetcode.com/problems/minimum-possible-integer-after-at-most-k-adjacent-swaps-on-digits/discuss/732932/Python-Detailed-comments-with-explanation-using-bsearch-only
"""

class Solution:

    def minInteger(self, s: str, k: int) -> str:
        """Bubble sort ish

        - find the smallest value within range
        that's unsorted, then move it to the front
        - keep swapping while k > 0

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        
        Notes

        """

        n: int = len(s)

        # We can just sort the array
        # basically a bubble sort
        if k >= n ** 2:
            return ''.join(map(str, sorted(s)))

        # All the same value, can't change it
        if len(set(s)) == 1:
            return s

        # Convert to array of values for ease
        arr = list(map(int, s))

        # Sorted position (exclusive)
        ix_srt = -1

        while k and ix_srt < n:

            # Find smallest unsorted value within reach
            cur_min = float('inf')
            cur_min_ix = ix_srt

            max_bound = min(n, ix_srt + 1 + k + 1)

            for ix in range(ix_srt + 1, max_bound):

                if arr[ix] < cur_min:
                    cur_min = arr[ix]
                    cur_min_ix = ix

                # Found smallest value possible
                if cur_min == 0:
                    break

            # Bring that value to the left most position
            ix = cur_min_ix
            while k and ix - 1 > ix_srt:
                arr[ix], arr[ix - 1] = arr[ix - 1], arr[ix]
                ix -= 1
                k -= 1

            # Bump sort index
            ix_srt += 1

        return ''.join(map(str, arr))

    def sort_then_swap(self, s: str, k: int) -> str:
        """Bubble sort ish

        - sort all values
        - find the smallest value within range
        that's unsorted, then move it to the front
        - keep swapping while k > 0

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        Notes
            - currently working some cases but not others,
            likely issue around bounds for k
        """

        n: int = len(s)

        # We can just sort the array
        if k >= n ** 2:
            return ''.join(map(str, sorted(s)))

        # All the same value, can't change it
        if len(set(s)) == 1:
            return s

        # Convert to array of values for ease
        arr = list(map(int, s))
        arr_srt = sorted(arr, reverse=True)

        # Unsorted position starts here
        ix_srt = 0

        while k and arr_srt:

            # Find smallest unsorted value within reach
            ix = ix_srt
            max_bound = min(n, ix_srt + k + 1)

            while ix < max_bound and arr[ix] != arr_srt[-1]:
                ix += 1

            # Remove the last target min
            arr_srt.pop()

            # Unable to find value within reach
            # try next smallest value
            if ix == max_bound:
                continue

            # Bring the min value to the left most position
            while k and ix - 1 >= ix_srt:
                arr[ix], arr[ix - 1] = arr[ix - 1], arr[ix]
                ix -= 1
                k -= 1

            # Bump unsort index
            ix_srt += 1

        return ''.join(map(str, arr))


if __name__ == '__main__':

    # Example 1
    # s = "4321"
    # k = 4

    # Example 5
    # s = '9438957234785635408'
    # k = 23

    # Test case
    # s = '3142'
    # k = 4

    # More k than needed ("124498948179")
    s = "294984148179"
    k = 11

    obj = Solution()
    print(obj.minInteger(s, k))
    print(obj.sort_then_swap(s, k))
