"""
Inputs
    List[int]: non negative integers in unknown order
Outputs
    str: combined values

Goals
    - arrange the values such that they form the largest number


Ideas

    - convert all numbers to strings
    - sorting may help us order the values
        + standard sorting doesn't necessarily help
            
            ie: 10|2 > 2|10

            we want 210 
            
            ie: 3,30, 34, 5, 9
            
            the largest number starts with 9 not 34
        
        + we need to compare combined values

            python allows for string comparison
            
            * compare each value against it's string compliment
            for a nother value and use this to choose the first number
            
            almost like a merge sort
            
            * continue this process for all values
            
            9|30 vs 30|9 => 930 so 9
            9|34 vs 34|9 => 934 so 9
            9|5 vs 5|9   => 95 so 9
            9|3 vs 3|9   => 9
        
            choose 9, and continue process with remaining numbers
"""

from typing import List
from functools import cmp_to_key


def compare(x: str, y: str) -> int:
    """Compare string compliments

    2 & 10

    2|10 > 10|2

    Args:
        x (str): current value
        y (str): comparing value
    Returns:
        int: magnitude used for comparison
    """

    s1 = x + y
    s2 = y + x

    if s1 >= s2:
        return 1
    elif s1 == s2:
        return 0
    else:
        return -1


class Solution:

    ''' Python: sorting with custom compare '''

    def largestNumber_v1(self, nums: List[int]) -> str:
        """Sort values by comparing values on complimenting criteria

        - use python built in function with custom comparison

        Time Complexity
            O(n lg(n)) sorting
        Space Complexity
            O(n)

        References:
            https://realpython.com/python-sort/
        """

        # Convert all values to strings
        strings = list(map(str, nums))

        # Sort the values by our complimenting comparison
        # where we want larger complimenting values first
        strings = sorted(strings, key=cmp_to_key(compare), reverse=True)

        # Join values
        return ''.join(strings)

    ''' Custom Merge sort '''

    def largestNumber_v0(self, nums: List[int]) -> str:
        """Sort values by comparing values on complimenting criteria

        Time Complexity
            O(n lg(n)) sorting
        Space Complexity
            O(n)
        References:
            https://docs.python.org/3/howto/sorting.html#sortinghowto
        """

        # Convert all values to strings
        strings = list(map(str, nums))

        # Sort the values by our complimenting comparison
        # where we want larger complimenting values first
        strings = self.merge_sort(strings)

        # Remove leading zeros
        ix: int = 0
        n: int = len(strings)
        while ix < n and strings[ix] == '0':
            ix += 1

        # TOOD: can use lstrip on string

        # All zeros
        if ix == n:
            return '0'

        # Only join values without zeros
        return ''.join(strings[ix:])

    def merge_sort(self, arr: List[str]) -> List[str]:
        """Use merge sort to sort values by the largest compliment

        Time Complexity
            O(n lg(n)) sorting
        Space Complexity
            O(n)
        """
        # Convert values to strings
        # strings = list(map(str, nums))

        n: int = len(arr)

        if n == 1:
            return arr

        # Split the array in two pieces
        mid_ix = n // 2

        # Sort the left and right pieces
        left = self.merge_sort(arr[:mid_ix])
        right = self.merge_sort(arr[mid_ix:])

        # Define pointers
        ix: int = 0

        left_ix: int = 0
        right_ix: int = 0

        left_bound: int = len(left)
        right_bound: int = len(right)

        # Compare while we have values to compare
        while left_ix < left_bound and right_ix < right_bound:

            # Compare strings ie: 2, 10
            # 102 vs 210
            if left[left_ix] + right[right_ix] >= right[right_ix] + left[left_ix]:
                arr[ix] = left[left_ix]
                left_ix += 1
            else:
                arr[ix] = right[right_ix]
                right_ix += 1

            # Move array pointer
            ix += 1

        # Add left values
        while left_ix < left_bound:
            arr[ix] = left[left_ix]
            left_ix += 1

            # Move array pointer
            ix += 1

        # Add right values
        while right_ix < right_bound:
            arr[ix] = right[right_ix]
            right_ix += 1

            # Move array pointer
            ix += 1

        return arr


if __name__ == '__main__':

    arr_in = [3, 30, 34, 5, 9]

    obj = Solution()
    # print(obj.largestNumber_v0(arr_in))
    print(obj.largestNumber_v1(arr_in))
