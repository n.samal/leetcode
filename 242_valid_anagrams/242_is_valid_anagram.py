class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        """Determine if an anagram is possible with the given strings
        
        - grab the character counts for each string
        - if the number unique characters match, it's possible
            
            + check each character to see if a valid match can be made

        Time Complexity
            O(n+m)
        Space Complexity
            O(n+m)

        Returns:
            bool: match is possible or not
        """
        
        cnt_s = {}
        cnt_t = {}
        
        # Create counter of s
        for ch in s:
            if ch in cnt_s:
                cnt_s[ch] += 1
            else:
                cnt_s[ch] = 1
        
        # Create counter of t
        for ch in t:
            if ch in cnt_t:
                cnt_t[ch] += 1
            else:
                cnt_t[ch] = 1
        
        # Have different character counts
        if len(cnt_s) != len(cnt_t):
            return False
        
        # Compare character counts
        for key in cnt_s:
            if key not in cnt_t:
                return False
            elif cnt_t[key] < cnt_s[key]:
                return False
        return True
        
        