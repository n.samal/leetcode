/**
 * @param {number[]} nums
 * @return {boolean}
 */

var canJump = function(nums) {
    
    // Track closest position that works
    let closest = nums.length - 1;

    // Iterate through the positions backwards
    for (let ix = closest; ix >= 0; ix--){

        // Check if we can jump to a working spot
        if (ix + nums[ix] >= closest){
            
            // Update our closest working spot
            closest = ix;
        }
    }

    // Did we make it to the beginning
    if (closest == 0){
        return true;
    }
    else{
        return false;
    }

};

// Setup test case
// console.log(canJump([2, 3, 1, 1, 4]));
