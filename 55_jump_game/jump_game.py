from typing import List


''' Backtracking '''


def backtrack(arr: List[int], pos: int = 0):
    """Can we make it to the last position

    Time Complexity
        O(n^n)
    Space Complexity
        Stack dependent..

    Args:
        arr (List[int]): max jump possible from current position
        pos (int): current position
    Returns:
        bool: can we make it
    Notes:
        we end can end up searching the same position multiple times
        use memoization to save values
    """

    # print('pos: ', pos, 'steps:', arr[pos])

    # We can jump to the end
    if pos >= len(arr) - 1:
        return True

    # Steps possible at current position
    steps = arr[pos]

    # Try all steps in between (start with largest step)
    for step in range(steps, 0, -1):

        # Crawl path at next possible position
        if backtrack(arr, pos + step):
            return True

    # Haven't found a way yet
    return False


def can_jump(arr: List[int]) -> bool:
    """Can we make it to the last position

    Args:
        arr (List[int]): max jump possible from current position
    Returns:
        bool: can we make it
    """

    # Start search from initial position
    return search(arr)


''' Backtracking with Memoization '''


class Solution(object):

    def __init__(self):

        self.searched = None

    def canJump(self, arr: List[int]) -> bool:
        """Can we make it to the last position

        Args:
            arr (List[int]): max jump possible from current position
        Returns:
            bool: can we make it
        """
        self.searched = [False for _ in range(len(arr))]

        # Start search from initial position
        return self.backtrack_memo(arr)

    def backtrack_memo(self, arr: List[int], pos: int = 0):
        """Can we make it to the last position

        Add tracking so we don't search the same position multiple
        times (Memoization)

        Args:
            arr (List[int]): max jump possible from current position
            pos (int): current position
        Returns:
            bool: can we make it
        """

        # print('pos: ', pos, 'steps:', arr[pos])

        # We can jump to the end
        if pos >= len(arr) - 1:
            return True

        # We've searched this before and come up short
        if self.searched[pos] is True:
            return False

        # Steps possible at current position
        steps = arr[pos]

        # Try all steps in between (start with largest step)
        for step in range(steps, 0, -1):

            # Crawl path at next possible position
            if self.backtrack_memo(arr, pos + step):
                return True

        # Mark this position as search
        self.searched[pos] = True


''' Bottoms Up: Greedy '''


def backwards(arr: List[int]) -> bool:
    """Work backwards from the goal

    Can we make it to the first position?

    At each positon track if we can make it to a known
    position which gets us to our goal.

    If we've found a new position closer that works, we update
    our minimum necessary position

    Time Complexity
        O(n)
    Space Complexity
        O(1)

    Args:
        arr (List[int]): max jump possible from current position
    Returns:
        bool: can we make it
    """

    # Closest position that let's us go to the end
    # Start with the end
    closest_working = len(arr) - 1

    # Go through steps backwards
    for ix in range(len(arr) - 2, -1, -1):

        # Can we reach the end from here
        # ie: can we jump to next position that we already know is working
        if ix + arr[ix] >= closest_working:
            closest_working = ix

    # Did we make it to the beginning?
    return closest_working == 0


if __name__ == "__main__":

    # print(can_jump([2, 3, 1, 1, 4]))

    s = Solution()

    # print(s.can_jump_memo([2, 3, 1, 1, 4]))
    print(backwards([2, 3, 1, 1, 4]))
