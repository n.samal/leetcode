"""
Inputs
    arr (List[int]): values in unknown order
Outputs
    int: number of longest increasing subsequences
Examples

    Example 1:

        Input: nums = [1,3,5,4,7]
        Output: 2
        Explanation: The two longest increasing subsequences are [1, 3, 4, 7] and [1, 3, 5, 7].

    Example 2:

        Input: nums = [2,2,2,2,2]
        Output: 5
        Explanation: The length of longest continuous increasing subsequence is 1, and there are 5 subsequences' length is 1, so output 5.

Ideas

    - a monotonically increasing stack doesn't help with LIS
        + example case: [1, 3, -1, 4]
        + -1 boots out the valid sequence

    - Longest increasing subsequence
        + maintain an array of chains for each length
        + length 0, length 1, length 2
        + track the smallest value at each chain length
        + at each value we try to build of the longest chain
        with a value smaller than ours

    - LIS finds the longest subsequence but not the number of them
        + we should maintain a count of the number of each sequence length

        + depending on the sequence were adding to the number of additional
        sequeneces changes

Hand Calc

    nums          = [1, 2, 4, 3, 5, 4, 7, 2]
    num_of_chains = [0, 0, 0, 0, 0, 0, 0, 0]
    max_chain_end = [1, 1, 1, 1, 1, 1, 1, 1]

    num = 1
    no previous chain

    num = 2
    can add to 1 => (1, 2)
    nums          = [1, 2, 4, 3, 5, 4, 7, 2]
    num_of_chains = [2, 1, 0, 0, 0, 0, 0, 0]
    max_chain_end = [1, 2, 1, 1, 1, 1, 1, 1]

    num = 4
    can add to either 1 or 2, or 1,2 => (1,4), (2,4), (1,2,4)
    nums          = [1, 2, 4, 3, 5, 4, 7, 2]
    num_of_chains = [2, 1, 1, 0, 0, 0, 0, 0]
    max_chain_end = [1, 2, 3, 1, 1, 1, 1, 1]

    num = 3
    can add to either 1 or 2, or 1,2 => (1,3), (2,3), (1,2,3)
    nums          = [1, 2, 4, 3, 5, 4, 7, 2]
    num_of_chains = [2, 1, 0, 0, 0, 0, 0, 0]
    max_chain_end = [1, 2, 3, 3, 1, 1, 1, 1]

    1
    2  2
    1, 2

References
    https://leetcode.com/problems/number-of-longest-increasing-subsequence/discuss/835323/Python-3-or-DP-or-Explanation
    https://leetcode.com/problems/number-of-longest-increasing-subsequence/discuss/199093/short-python-50ms-beats-100-with-binary-search
"""

from typing import List


class Solution:
    def findNumberOfLIS(self, arr: List[int]) -> int:
        """

        - track the length of the longest chain ending at each value
        - track the number of longest chians ending at each value

        - compare our current value against some previous value
            + if greater than the previous value we can add
            the current value onto those previous chains

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        n: int = len(arr)

        counts = [1 for _ in range(n)]  # counts of longest chain at each value
        length = [1 for _ in range(n)]  # longest chain ending with each value
        len_max: int = 1

        # Try combinations with previous numbers
        for ix_cur in range(n):
            for ix_pre in range(ix_cur):

                # We can extend the previous chain
                if arr[ix_cur] > arr[ix_pre]:

                    new_chain = length[ix_pre] + 1

                    # We've got a new longest chain,
                    # we can build upon each of those previous chains
                    if new_chain > length[ix_cur]:

                        length[ix_cur] = new_chain
                        counts[ix_cur] = counts[ix_pre]

                    # We've got another set of longest chains
                    # add onto those different previous chains
                    elif new_chain == length[ix_cur]:
                        counts[ix_cur] += counts[ix_pre]

                    len_max = max(len_max, new_chain)

        # How many times did we see the longest chain?
        count: int = 0

        for ix in range(n):

            if length[ix] == len_max:
                count += counts[ix]

        return count


if __name__ == '__main__':

    # Example 1 (2)
    nums = [1, 3, 5, 4, 7]

    # Example 2 (5)
    # nums = [2, 2, 2, 2, 2]

    # Example (3)
    # nums = [1, 2, 4, 3, 5, 4, 7, 2]

    obj = Solution()
    print(obj.findNumberOfLIS(nums))
