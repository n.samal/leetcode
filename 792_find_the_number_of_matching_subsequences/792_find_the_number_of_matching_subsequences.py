"""
Inputs
    S (str): main word
    words (List[str]): dictionary of words
Outputs
    int: number of words in the dictionary that are subsequences of S
Goals
    - find the number of words in the dictionary that are subsequences of S
Ideas
    - track indices of each character in the main string
        + for each dictionary word, see if we can find valid indices
        to make your subsquence
        + use binary search to speed up process
"""

import bisect
from collections import defaultdict
from typing import List


class Solution:
    def numMatchingSubseq(self, S: str, words: List[str]) -> int:
        """

        - track the locations of each character in the main string
        - for each word, try to generate a valid subsequence
            using the current index in S

        Time Complexity
            O(n*m*ln(m))

            n = number of characters in S
            m = number of character in word

        Space Complexity
            O(n)

        References:
            https://docs.python.org/3/library/bisect.html
            https://www.w3schools.com/python/ref_func_iter.asp
        """

        ''' Get indices of each character '''

        dct = defaultdict(list)

        for ix, char in enumerate(S):
            dct[char].append(ix)

        ''' Count number of words with valid sequences'''

        count: int = 0

        for word in words:

            n: int = len(word)
            ix: int = -1  # pointer in S
            jx: int = 0  # pointer in word

            # See if we can match the whole sequence
            # going character by character
            while jx < n:

                char = word[jx]

                if char not in dct:
                    break
                else:
                    options = dct[char]

                    # Find the first option greater than our current pointer
                    found = bisect.bisect(options, ix)

                    # Crawl to the next position if available
                    if found == len(options):
                        break
                    else:
                        ix = options[found]
                        jx += 1

            if jx == n:
                count += 1

        return count


if __name__ == "__main__":

    # Example 1
    S = "abcde"
    words = ["a", "bb", "acd", "ace"]

    obj = Solution()
    print(obj.numMatchingSubseq(S, words))
