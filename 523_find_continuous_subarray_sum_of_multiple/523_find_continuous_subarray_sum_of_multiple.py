"""
Inputs
    arr (List[int]): non negative integers in no specified order
Outputs
    bool: does a subarray sum of multiple k exist
Goal
    - does a continuous subarray of at least size 2 exist
    where the sum is equal to a multiple of k

Examples

    Example 1

        Input: [23, 2, 4, 6, 7],  k=6
        Output: True
        Explanation: Because [2, 4] is a continuous subarray of size 2 and sums up to 6.

    Example 2

        Input: [23, 2, 6, 4, 7],  k=6
        Output: True
        Explanation: Because [23, 2, 6, 4, 7] is an continuous subarray of size 5 and sums up to 42.

Ideas
    - use a cummulative sum to compute subarray summations
        + values are non-negative so the cum sum will
        be almost monotonically increasing
        + the value of zero may give us multiple of the same
        value, in which case, we want to save the minimum index

    - in our saved cumsums, search for the compliment that would
    give us a multiple of k
        + we need to search for multiple compliments

        sum_cur - sum_pre = k * c

        or

        sum_pre = sum_cur - k * c

        we only search while is summation is greater than 0

    - Multiple
        + k could be either positive or negative of k

    - if the subarray has multiple zeros, then we will have many of the same summation
        + we want the first occurence for the max possible length

    - use the modulus to check if remainder occurs
        + faster method to find a multiple of k
        + we can use modulus of summations to normalize values

            41 % 10 = 1
            51 % 10 = 1

            if we see two of the same modulus then we know the modulus
            is the difference

Cases
    k = 0
        modulus will fail
    k = negative
        modulus looks as intended
    k = positive
        modulus looks as intended



"""

from typing import List


class Solution:

    ''' Hashmap '''

    def checkSubarraySum(self, arr: List[int], k: int) -> bool:
        """Find the complmenting summation

        - compute the cumsum
        - search for the complimenting pair in previous
        summations
            + review if the subarray meets our length requirement

        Time Complexity
            O(n * c)
        Space Complexity
            O(n)
        """

        n: int = len(arr)

        cum_sum: int = 0
        dct = {}

        ''' Zero case '''

        # Check for two adjacent zeros
        if k == 0:
            for ix in range(1, n):
                if arr[ix] == 0 and arr[ix - 1] == 0:
                    return True
            return False

        ''' General Case '''

        for ix in range(n):

            cum_sum += arr[ix]

            # Find the max possible constant
            # then search for the compliment
            c_max = (cum_sum - arr[0]) // abs(k)

            for c in range(1, c_max + 1):

                sum_pre = cum_sum - (abs(k) * c)

                if sum_pre in dct:
                    jx = dct[sum_pre]

                    if ix - jx + 1 >= 2:
                        return True

            # Save index if current summation hasn't occcured yet
            if cum_sum not in dct:
                dct[cum_sum] = ix

        return False

    def map_modulo(self, arr: List[int], k: int) -> bool:
        """Find the complmenting summation

        - compute the cumsum (exclusive - inclusive)
        - use the modulus to normalize values
            + search for complimenting pairs use the modulus

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        n: int = len(arr)

        cum_sum: int = 0
        dct = {0: -1}

        ''' Zero case '''

        # Check for two adjacent zeros
        if k == 0:
            for ix in range(1, n):
                if arr[ix] == 0 and arr[ix - 1] == 0:
                    return True
            return False

        ''' General Case '''

        for ix in range(n):

            # Add value and normalize summation
            cum_sum = (cum_sum + arr[ix]) % k

            # Search for complimenting index
            # compliment will have the same modulus
            if cum_sum in dct:
                jx = dct[cum_sum]

                if ix - jx >= 2:
                    return True

            # Save index of this summation
            else:
                dct[cum_sum] = ix

        return False

    ''' Loops '''

    def two_loops(self, arr: List[int], k: int) -> bool:
        """Two loops

        - compute cummulative sum (exclusive - inclusive)
        - create summations for all subarrays greater than equal to 2

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        Notes
            - fails on zero case
        """

        n: int = len(arr)

        ''' Compute cummulative sum '''

        # Summations = exclusive - inclusive
        cum_sum = [0 for ix in range(n + 1)]

        for ix in range(1, n + 1):
            cum_sum[ix] = cum_sum[ix - 1] + arr[ix - 1]

        ''' Try all subarrays of length >= 2 '''
        for ix in range(n + 1):
            for jx in range(ix + 2, n + 1):

                delta = cum_sum[jx] - cum_sum[ix]

                if k == 0:
                    if delta == 0:
                        return True

                elif delta % k == 0:
                    return True

        return False


if __name__ == '__main__':

    # Example 1
    # arr = [23, 2, 4, 6, 7]
    # k = 6

    # Example 2
    # arr = [23, 2, 6, 4, 7]
    # k = 6

    # Negative int (True)
    arr = [23, 2, 4, 6, 7]
    k = -6

    # arr = [23, 2, 4, 6, 7]
    # k = 0

    # arr = [1, 0]
    # k = 2

    obj = Solution()
    print(obj.checkSubarraySum(arr, k))
    print(obj.two_loops(arr, k))
    print(obj.map_modulo(arr, k))
