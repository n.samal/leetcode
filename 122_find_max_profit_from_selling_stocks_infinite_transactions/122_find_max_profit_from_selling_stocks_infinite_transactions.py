"""
Inputs
    k (int): number of trades
    prices (List [int]): stock prices
Outputs
    int: maximum profit possible
Notes
    - must sell our stock before we can trade again
    - we cannot engage in multiple transactions at once
    - find the max profit possible with k transactions

Cases

    Monotonically increasing
    Monotonically decreasing
    General (Several peaks and valleys)

Ideas

    - maintain a montonically increasing stack to find peaks and valleys

        + once we a hit a valley, then we compute
        our profit as if we sold at the peak

    - we pretend we buy at every valley, and sell at every peak

        + these can be determined by comparing neighboring values
        + ix vs ix - 1

       P                     P
      /\                    /
     /  \      P      P    /
    /    \    /\      /\  /
          \  /  \    /  \/
           \/    \  /
                  \/

Key Point
    
    - instead of selling at the max price from the lowest cost
    we sell at every increasing price

    [1, 2, 3, 4, 5]

    if we buy at 1 and sell at 5
    total_profit = 5 - 1 = 4

    we could also sell and buy after every price
    profit = 2 - 1 = 1
    profit = 3 - 2 = 1
    profit = 4 - 3 = 1
    profit = 5 - 4 = 1

    total_profit = 4
"""


class Solution:
        
    def one_pass(self, prices: List[int]) -> int:
        """Add every incremental profit

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """        
        
        n: int = len(prices)
        max_profit: int = 0
            
        for ix in range(1, n):
            
            # if potential for profit add it
            if prices[ix] > prices[ix - 1]:
                max_profit += prices[ix] - prices[ix - 1] 
        
        return max_profit
        
        
        
        