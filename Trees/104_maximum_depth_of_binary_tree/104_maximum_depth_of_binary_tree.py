# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

from collections import deque

class Solution:
    def maxDepth(self, root: TreeNode) -> int:
        """Iterative appraoch via dfs

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        max_level: int = 0
        queue = deque([])
        
        if root:
            # (Node, level)
            queue.append((1, root))
        
        while queue:
            
            # Get item from queue
            level, node = queue.pop()

            # print('node:', node.val, 'level:', level)
            
            if node.left:
                queue.append((level + 1, node.left))
            if node.right:
                queue.append((level + 1, node.right))
            
            # Update max
            max_level = max(max_level, level)

        return max_level
