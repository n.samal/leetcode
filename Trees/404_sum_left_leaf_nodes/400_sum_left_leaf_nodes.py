# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

from collections import deque

class Solution:
    def sumOfLeftLeaves(self, root: TreeNode) -> int:
        """Breadth first search
        
        - crawl the tree
        - if we're at a leaf node, and it was a left child
        then we add the value

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
            
        # Null Case
        if not root:
            return 0
        
        # Initialize queue and summation
        total: int = 0
        queue = deque([(root, None)])
        
        while queue:
            
            node, left_side = queue.popleft()
            
            # We're at a leaf node
            if node.left is None and node.right is None and left_side:
                total += node.val
                
            # We have kids, this is on the left
            if node.left:
                queue.append((node.left, True))
            
            if node.right:
                queue.append((node.right, False))
            
        return total
        
        
        
        
