from typing import List


class TreeNode():

    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution():

    ''' Recursion: Index Search '''

    def buildTree(self, preorder: List[int], inorder: List[int]) -> TreeNode:
        """

        - Inorder tells us the location of the root node
        - we split the arrays using information from preorder

        - continue splitting arrays until we're at a single node
        or have no subarrays to explore

        Time Complexity
            O(n) each problem is broken up in to two subproblems
        Space Complexity
            O(n) we store the whole tree

        Notes:
            -finding the root val index can be quite expensive
            potentially searching O(n) each time so O(n^2)
        """

        print(f'inorder: {inorder} preorder: {preorder}')

        # Number of nodes
        n: int = len(preorder)

        # No more values or single value
        if n == 0:
            return None

        elif n == 1:
            return TreeNode(preorder[0])

        # First value of PreOrder (NLR) is our root value        
        root_val = preorder[0]
        root = TreeNode(root_val)

        # Find location of root in InOrder to split tree (LNR)
        root_ix = inorder.index(root_val)

        # The root location splits our InOrder subarray (Left Subtree, Node, Right Subtree)
        # Use the lengths of these subarrays to grab the correct values
        # in the PreOrder subarray

        # Recurse on subarrays
        root.left = self.buildTree(preorder[1:root_ix + 1], inorder[:root_ix])
        root.right = self.buildTree(preorder[root_ix + 1:], inorder[root_ix + 1:])

        return root

    ''' Recursion: Hash Table '''

    def buildTree2(self, preorder: List[int], inorder: List[int]) -> TreeNode:

        # Number of nodes
        self.n: int = len(preorder)

        # No more values or single value
        if self.n == 0:
            return None

        elif self.n == 1:
            return TreeNode(preorder[0])

        # Save values for later use
        self.preorder = preorder
        self.inorder = inorder

        # Save values into table for easy look up O(1)
        self.table = {}
        for ix, val in enumerate(inorder):
            self.table[val] = ix

        return self.recursion(0, self.n - 1, 0, self.n - 1)

    def recursion(self, pre_left_ix: int, pre_right_ix: int, in_left_ix: int, in_right_ix: int):
        """Recursively generate subtrees

        - find the root value from the PreOrder
        - use the root value to split the InOrder array
        into left and right subtrees

        Args:
            pre_left_ix (int): preorder left bound
            pre_right_ix (int): preorder right bound
            in_left_ix (int): inorder left bound
            in_right_ix (int): inorder right bound
        """

        # Preorder is out of range
        if pre_left_ix > pre_right_ix:
            return None

        # Find the location of root value
        root_val = self.preorder[pre_left_ix]
        root_ix = self.table[root_val]
        root = TreeNode(root_val)

        print(f'root_val: {self.preorder[pre_left_ix]}')
        # print(f' left_ix: {left_ix}  right_ix: {right_ix}')
        print(f' preorder {self.preorder[pre_left_ix:pre_right_ix + 1]}')
        print(f' inorder  {self.inorder[in_left_ix:in_right_ix + 1]}')

        # Number of trees in each subtree
        n_left = root_ix - in_left_ix

        # Generate subtrees
        root.left = self.recursion(pre_left_ix + 1, pre_left_ix + n_left, in_left_ix, root_ix - 1)
        root.right = self.recursion(pre_left_ix + 1 + n_left, pre_right_ix, root_ix + 1, in_right_ix)

        return root


if __name__ == "__main__":

    # Failed case
    # preorder = [1, 2]
    # inorder = [2, 1]

    # Failed case
    preorder = [1, 2]
    inorder = [1, 2]

    # Failed case
    # preorder = [1, 2, 3]
    # inorder = [3, 2, 1]

    obj = Solution()
    ans = obj.buildTree2(preorder, inorder)
