"""
Inputs
    root (TreeNode): root of binary search tree
    p (TreeNode): node 1
    q (TreeNode): node 2
Outputs
    TreeNode: lowest common ancestor 

Goals
    - find the lowest common ancestor of both nodes
    - all values will be unique
    - p and q will exist in the tree

Ideas
    - find the nodes whose depth is largest but is still
    a parent to the two nodes
    
    - make use of the binary search tree properties
        + left is smaller, right is larger

Strategies
    
    Iterative Traverse and Check
        
        - traverse the search tree from the root
        - if both values are smaller then current node go left
        - if both values are larger then current node go right
        - if one says left and one says right we stop
        
        Time Complexity
            O(h) 
            
            - go through half the tree if searching for two smallest
            or two largest values
        
        Space Complexity
            O(1)

    Recursive
    	- track when we've seen a node of interest
    	- track the tree using PostOrder (LRN) so we check the parent nodes last
    	- 3 possible locations
    		+ current nodes
    		+ located in left subtree
    		+ located in right subtree

    	- when we've seen both nodes we've found our LCA

    		- either in left and right
    		- either in left or here
    		- either in right or here
    	
    	- save the node of interest

        Time Complexity
        	O(n)        
        Space Complexity
        	O(h)    could be O(n) is the tree is not balanced
"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    
    def iterative(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        """Iterative

        - continue moving down the tree while the nodes
        both agree in a direction to move
        - when the nodes disagree then stop here

        Time Complexity
            O(h)

            if the tree is totally unbalanced then O(n)

        Space Complexity
            O(1)
        """


        # Null case
        if not root:
            return None
        
        # Starting node and lowest common ancestor
        cur_node = root
        lca = root
        
        # print(f'p: {p.val} q: {q.val}')
        
        while cur_node:
            
            # print(f' cur_node: {cur_node.val}')
            
            # Values are smaller go left
            if p.val < cur_node.val and q.val < cur_node.val:
                lca = cur_node
                cur_node = cur_node.left
            
            # Values are larger, go right
            elif p.val > cur_node.val and q.val > cur_node.val:
                lca = cur_node
                cur_node = cur_node.right
            
            # Values don't agree, let's stop here
            else:
                return cur_node
            
        return lca
    
    ''' Recursive '''

    def recursive1(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        """Recursive

        - continue moving down the tree while the nodes
        both agree in a direction to move
        - when the nodes disagree then stop here

        Time Complexity
            O(h)
        Space Complexity
            O(h) recursion stack
        """


        # Null case
        if not root:
            return None
        
        # Values are smaller go left
        if p.val < root.val and q.val < root.val:
            return self.recursive(root.left, p, q)
            
        # Values are larger, go right
        elif p.val > root.val and q.val > root.val:
            return self.recursive(root.right, p, q)
        
        # Values don't agree, let's stop here
        else:
            return root
    
    def recursive2(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        """Recursive

        - add short cuts for names

        Time Complexity
            O(h)
        Space Complexity
            O(h) recursion stack

        Notes
            - this is significantly faster!
            use shortcuts for common comparisons
        """


        # Null case
        if not root:
            return None
        
        # Short cuts for names
        r_val = root.val
        p_val = p.val
        q_val = q.val

        # Values are smaller go left
        if p_val < r_val and q_val < r_val:
            return self.recursive2(root.left, p, q)
            
        # Values are larger, go right
        elif p_val > r_val and q_val > r_val:
            return self.recursive2(root.right, p, q)
        
        # Values don't agree, let's stop here
        else:
            return root
        
        