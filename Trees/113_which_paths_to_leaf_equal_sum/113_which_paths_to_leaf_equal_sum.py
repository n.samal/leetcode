# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def pathSum(self, root: TreeNode, sum: int) -> List[List[int]]:
        
        self.paths = []
        
        # Null case
        if not root:
            return self.paths
    
        # Crawl from root
        self.recurse(root, sum, [])
        
        return self.paths
    
    def recurse(self, node: TreeNode, target: int, path: List[int]):
        """Recursively crawl the path
        
        - include the current node
            + update the target
            + update the path
        
        - if we're a leaf and our target is good, save it
        - update paths with concatentation
        
        Time Complexity
            O(n)
        Space Complexity
            O(h)  height of tree/recursion stack
        
        """
        # Include the current value
        new_target = target - node.val
        new_path = path + [node.val]
        
        # print(f'node: {node.val}  new_target: {new_target}  new_path: {new_path}')
        
        # If at a leaf and meet our goal
        if node.left is None and node.right is None and new_target == 0:
            self.paths.append(new_path)
        
        # We got kids, add the current value and continue
        if node.left:
            self.recurse(node.left, new_target, new_path)
        if node.right:
            self.recurse(node.right, new_target, new_path)
            
        
    def recurse2(self, node: TreeNode, target: int, path: List[int]):
        """Recursively crawl the path
        
        - include the current node (we know it's valid)
            + update the target
            + update the path
        
        - if we're a leaf and our target is good, save it
        - update paths with append
        
        Time Complexity
            O(n)
        Space Complexity
            O(h)  height of tree/recursion stack
        
        """
        # Include the current value
        new_target = target - node.val
        path.append(node.val)
        
        # print(f'node: {node.val}  new_target: {new_target}  new_path: {new_path}')
        
        # If at a leaf and meet our goal
        if node.left is None and node.right is None and new_target == 0:
            self.paths.append(path)
        
        # We got kids, add the current value and continue
        # must send a copy of the path, otherwise this will get modifed along the way
        if node.left:
            self.recurse(node.left, new_target, path[:])
        if node.right:
            self.recurse(node.right, new_target, path[:])
        