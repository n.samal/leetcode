"""
Inputs
    root (TreeNode): the root 
Outputs
    int: max width in tree

Notes
    - width is defined as the length of end nodes on the same level
        + right most non null node - left most non null node
    - find the maximum width found in the tree

Examples

    Example 1
        Input: 

               1
             /   \
            3     2
           / \     \  
          5   3     9 

    Output: 4
    Explanation: The maximum width existing in the third level with the length 4 (5,3,null,9).

    Example 2

        Input: 

          1
         /  
        3    
       / \       
      5   3     

        Output: 2
        Explanation: The maximum width existing in the third level with the length 2 (5,3).

    Example 3:

        Input: 

              1
             / \
            3   2 
           /        
          5      

        Output: 2
        Explanation: The maximum width existing in the second level with the length 2 (3,2).

    Example 4:

        Input: 

              1
             / \
            3   2
           /     \  
          5       9 
         /         \
        6           7

        Output: 8
        Explanation:The maximum width existing in the fourth level with the length 8 (6,null,null,null,null,null,null,7).

Ideas

    - use BFS to travel the tree
        + track the number of nodes in each level
        + use current queue and next queue

    - we can't just track the number of nodes in each level

        in our example the last level has a distance of 4
        but only 3 non null nodes

        ie: [5, 3, 9]

    - we can add nulls to our levels
        + if we add null, we need to continue add nulls to subsequent
        levels
        + we need logic to track when the level is only nulls
        ie: no more actual nodes
        + lots of extra work

    - use array representation of array as a hint
        + left = 2p + 1
        + right = 2p + 2

        + track the index of the parent for each node
        + then grab the left most node index, and right
        most node index

"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

from collections import deque

class Solution:
    def widthOfBinaryTree(self, root: TreeNode) -> int:
        """BFS

        - use BFS to travel the tree
        - track the nodes and indices
        - when done with a level, move to the next
            + also track the max distance between indices
            right most - left most + 1

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        if not root:
            return 0

        queue_cur = deque([(root, 0)])
        queue_next = deque([])
        max_width: int = 1

        while queue_cur or queue_next:

            if not queue_cur:

                width = queue_next[-1][1] - queue_next[0][1] + 1
                max_width = max(max_width, width)

                queue_cur = queue_next
                queue_next = deque([])

            node, p = queue_cur.popleft()

            if node.left:
                queue_next.append((node.left, 2*p + 1))

            if node.right:
                queue_next.append((node.right, 2*p + 2))

        return max_width
