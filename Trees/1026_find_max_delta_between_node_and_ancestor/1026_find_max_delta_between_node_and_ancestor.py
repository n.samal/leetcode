"""
Inputs
    root (TreeNode): the root
Outputs:
    int: maximum difference betweeen A & B
Notes
    - find the max delta: V = |A - B|
        + A = ancestor of B

Ideas

    - the max difference can be computed two ways
        + abs(val - max)
        + abs(val - min)

    - for each node track the min and max value from subtrees
        + then compute the delta both ways
        + track the global max

    - we could alternatively track the min and max ancestor

"""

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    
    def min_max_child(self, root: TreeNode) -> int:

        self.max_delta: int = 0

        def recurse(root: TreeNode):
            """

            - track the smallest and largest children
                + propogate those values up
                + compute the delta using the min/max child

            Time Complexity
                O(n)
            Space Complexity
                O(n)
            """

            if root:

                min_here = root.val
                max_here = root.val

                if root.left:
                    min_l, max_l = recurse(root.left)
                    min_here = min(min_l, min_here)
                    max_here = max(max_l, max_here)

                    delta_min = abs(root.val - min_l)
                    delta_max = abs(root.val - max_l)
                    self.max_delta = max(self.max_delta, delta_min, delta_max)

                if root.right:
                    min_r, max_r = recurse(root.right)
                    min_here = min(min_r, min_here)
                    max_here = max(max_r, max_here)

                    delta_min = abs(root.val - min_r)
                    delta_max = abs(root.val - max_r)
                    self.max_delta = max(self.max_delta, delta_min, delta_max)

                return min_here, max_here

        recurse(root)

        return self.max_delta

    def min_max_ancestor(self, root: TreeNode) -> int:

        self.max_delta: int = 0

        def recurse(root: TreeNode, min_v: int = float('inf'), max_v: int = float('-inf')):
            """

            - track the smallest and largest ancestor
                + propogate those values up
                + compute the delta using the min/max child

            Time Complexity
                O(n)
            Space Complexity
                O(n)
            """

            if root:

                recurse(root.left, min(root.val, min_v), max(root.val, max_v))
                recurse(root.right, min(root.val, min_v), max(root.val, max_v))


                if min_v != float('inf'):
                    self.max_delta = max(self.max_delta, abs(root.val - min_v))

                if max_v != float('-inf'):
                    self.max_delta = max(self.max_delta, abs(root.val - max_v))

        recurse(root)

        return self.max_delta
