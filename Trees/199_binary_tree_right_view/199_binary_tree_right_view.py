"""
Inputs:
    root (TreeNode): root node of the tree
Outputs:
    List[int]: node values for right most node of each level
Goal
    - Given a binary tree, imagine yourself standing on the right side of it
    - return the values of the nodes you can see ordered from top to bottom.

Ideas
    - compute level order traversal
        + save the right most value of each level

Strategies
    Level Order Traversal: whole path saved

        - compute the level order traversal with Breadth First Search
        - after all levels have been explore, grab the right most values

        Time Complexity
            O(n)
        Space Complexity
            O(n)

    Level Order Traversal: Two Queue method

        - use two queues
            + current queue
            + next queue
        - after completing the current node save the current node
        which should be the right most node

        Time Complexity
            O(n)
        Space Complexity
            O(n)

            the size of the last level of the tree is still quite large
            can be of size N/2 which is basically the tree diameter
    
    Recursion: Right Side last

        - recurse the tree but prioritize one side
            + left side first
            + then right

        - let's track the last node we've seen for each level
        - if we've seen a new level add it to the list/hash

        - recompile the information so we 

        # TODO see if this works

    Recursion: Right Side first

        - recurse through the tree with depth first search
            + prioritize right nodes over left nodes

        - when we reach a leaf node, determine if this level is new
            + if so, then save it
            + this node will be the first and right most node at this level
            since we prioritize searching right first

        Time Complexity
            O(n)
        Space Complexity
            O(h) height of stack required for recursion ie: tree height
"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

from collections import deque

class Solution:

    ''' Level Order Traversal: Full path saved '''

    def level_order_full(self, root: TreeNode) -> List[List[int]]:
    
        queue = deque([])
        levels = []
        
        if root:
            queue.append((1, root))  # (Node, level)
        
        while queue:
            
            # Get item from queue (FIFO aka breadth first first)
            level, node = queue.popleft()
            
            print('node:', node.val, 'level:', level)
            
            # Save the new level
            if level > len(levels):
                levels.append(node.val)

            # Update right most view
            else:
                levels[level-1] = node.val

            # Start with left side
            if node.left:
                queue.append((level + 1, node.left))
                # queue.appendleft((level + 1, node.left))

            # We want to search the right side of trees last
            if node.right:
                queue.append((level + 1, node.right))
            
        return levels
   

    ''' Recursion '''

    def recursion(self, root: TreeNode) -> List[List[int]]:

        # Save only the first node seen for each level
        self.levels = []

        # Null case
        if not root:
            return []

        # Node, level
        self.recurse(root, 1)

        return self.levels

    def recurse(self, node: TreeNode, level: int):
        """Recurse through tree starting at this node"""

        if node:

            # We're past the furthest level we've been
            if level > len(self.levels):
                self.levels.append(node.val)

            # Try crawling further (right prioritized first)
            self.recurse(node.right, level + 1)
            self.recurse(node.left, level + 1)
