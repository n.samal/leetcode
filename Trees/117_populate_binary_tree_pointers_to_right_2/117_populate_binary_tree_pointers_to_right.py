"""
Inputs:
    root (Node): root node of binary tree
Outputs:
    Node: root node of modified tree
Goals
    - connect each node the it's right most node on it's level
    - if no such node exists point that to None
    - by default all nodes point to None already

    - only constant space solutions

Ideas
    
    - if a parent has two kids
        + the left kid should point to the right

    - if a parent has a single child
        + use the parents next node, and connect it to the left most node available
        (ie: the first right)

Strategies

    Level Order Traversal
        - same as the previous problem
        - complete the level order traversal via breadth first search
        - before traversing a new level connect
        all nodes to the right

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        doesn't meet problem requirements

    Use the Parents

        - 



"""

from collections import deque

class Node:
    def __init__(self, val: int = 0, left: 'Node' = None, right: 'Node' = None, next: 'Node' = None):
        self.val = val
        self.left = left
        self.right = right
        self.next = next

class Solution:

    
    ''' Level Order Traversal '''

    def lvl_order1(self, root: 'Node') -> 'Node':
        """Level order traversal with bfs
        
        - compute level order traversal
        - after we reach a new level connect all the pointers
        
        - save the entire level order traversal
        """
        
        # Null case
        if not root:
            return None
        
        max_level: int = 0
        
        levels: List['Node'] = []
        cur_queue = deque([(root, 1)])  # (Node, level)
    
        print('Exploring')
        while cur_queue:
            
            # Breadth first search
            node, level = cur_queue.popleft()
            
            print(f'node: {node.val} level: {level}')

            # We've got kids
            if node.left:
                cur_queue.append((node.left, level + 1))
            if node.right:
                cur_queue.append((node.right, level + 1))
        
            # We're at a new level
            if level > max_level:
                max_level = level
                levels.append([node])

            # Save node to current level
            else:
                levels[-1].append(node)

        # Iterate through each level and update pointers
        print('Connecting')
        for level in levels:

            n = len(level)
            
            for ix in range(n - 1):

                print(f'{level[ix].val} -> {level[ix + 1].val}')
                level[ix].next = level[ix + 1]

            print(' ')
                
        return root

    def lvl_order2(self, root: 'Node') -> 'Node':
        """Level order traversal with bfs
        
        - compute level order traversal
        - have a current level queue and a next level queue
            + once the current level is empty switch to the next one
        
        - after we reach a new level connect all the pointers

        Time Complexity
            O(n) must go through all nodes
        Space Complexity
            O(n) size of the last level of the tree which is n/2
        """
        
        # Null case
        if not root:
            return None
        
        cur_queue = deque([(root, 1)])
        next_queue = deque([])
        
        while cur_queue:
            
            # Breadth first search
            node, level = cur_queue.popleft()
            
            print(f'node: {node.val} level: {level}')

            # We've got kids add them to the next level
            if node.left:
                next_queue.append((node.left, level + 1))
            if node.right:
                next_queue.append((node.right, level + 1))

            # Current level queue is empty
            if not cur_queue:

                print(f'New Level started')
                
                # Connect all nodes in the next queue
                n = len(next_queue)
                
                for ix in range(n - 1):
                    print(f'{next_queue[ix][0].val} -> {next_queue[ix + 1][0].val}')
                    next_queue[ix][0].next = next_queue[ix + 1][0]

                # Use the new queue
                cur_queue = next_queue
                next_queue = deque([])
        
        return root

    ''' Constant Space '''

# TODO: incomplete