"""
Inputs:
    root (TreeNode): root node of binary search tree
    k (int): smallest element desired
Outputs:
    int: kth smallest element

Goal
    - find the kth smallest element in binary search tree

Ideas
    - brute force.
        + go through entire BST and keep top k elements
        + grab the kth element at the end
    
    - use binary search tree properties
        + smallest values to the left
        + largest values to the right
        
        + use a LNR order will prioritize smaller elements
            InOrder
Strategies
    
    InOrder
        - use inorder traversal to traverse the smallest values first
        - if we hit a leaf node iterate our node count
        - once we hit the node count output the value
        
        Time Complexity
            O(n)
        Space Complexity
            O(h) recursion height of tree
"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    
    ''' Recursion: save path to k '''

    def kthSmallest(self, root: TreeNode, k: int) -> int:
        
        # Null root
        if not root:
            return root
        

        self.path = []
        self.k = k
        
        self.recurse(root)

        # 1st smallest is index 0, 2nd smallest is index 1
        return self.path[k-1]
        
    def recurse(self, node: TreeNode):
        """Recusively crawl child nodes"""
        
        if node:
            self.recurse(node.left)

            # At current node
            self.path.append(node.val)

            # We've got k elements already, stop recursing
            if len(self.path) >= self.k:
                return
            
            self.recurse(node.right)
    
    ''' Iterative '''

    def iterative(self, root: TreeNode, k: int) -> int:
        """Iterate with preorder (LNR)

        - use preorder traversal to explores subtrees 
        - explore the smallest nodes first and track the number
        of nodes seen
        - once we've seen the kth node, return it

        ie: k = 1, 1st smallest
            k = 2, 2nd smallest

        Time Complexity
            O(h + k)

            - we need to explore h values to find lowest leaf to the left
            - then k more pops to find the kth value

        Space Complexity
            O(~2h)

            - at every step we add potentially add 3 values (left, node, right) and remove 1
            - once at the left most node we have ~2h values
                + if k is within this side of the try we're done
                + if k > h, we start exploring right side of the tree but again
                crawl h to the bottom left most node
        """

        # Null case
        if not root:
            return None

        stack = [(root, False)]

        while stack:

            node, explored = stack.pop()

            # Child node or explored
            if explored or (node.left is None and node.right is None):
                k -= 1
                
                # print(f'node: {node.val} k: {k}')
                
                if k == 0:
                    return node.val
                continue

            # Add values in reverse order since DFS
            # LNR => add in R, N, L

            if node.right:
                stack.append((node.right, False))
            
            stack.append((node, True))

            if node.left:
                stack.append((node.left, False))

    def iterative2(self, root: TreeNode, k: int) -> int:
        """Iterate with preorder (LNR) via suggested method

        - use preorder traversal to explores subtrees 
            + left node
            + current node
            + right node

        - once we've seen the kth node, return it

        ie: k = 1, 1st smallest
            k = 2, 2nd smallest


        Time Complexity
            O(h + k)
            
            - we need to explore h values to find lowest leaf to the left
            - we then need to explore k to find the kth smallest of those
        
        Space Complexity
            O(h + k)
        """

        # Null case
        if not root:
            return None

        stack = []

        while stack or root:

            # Explore left while possible and save breadcrumbs
            while root:
                stack.append(root)
                root = root.left

            # Grab previous leaf
            node = stack.pop()

            k -= 1

            # At kth value
            if k == 0:
                return node.val

            # Explore right trees,after current node
            root = node.right
        