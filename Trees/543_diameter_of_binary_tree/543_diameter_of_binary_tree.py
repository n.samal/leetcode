"""
Inputs:
    root (TreeNode): root of binary tree
Outputs:
    int: diameter of binary tree
Goal
    - compute the length of the diameter
    - diameter of tree is length of longest path between any two nodes
    - The length of path between two nodes is represented by the number of edges between them.

Ideas
    
    - traverse tree to leaf via recursion
    - the diameter is function of max length on right and left
    
Notes
    Height/Depth = number of edges crawled from root
    Diameter = maximum path between nodes
             = function of height

Strategies
    Recursion
        
        - start at root
        - recursively crawl left and right trees
            + if subtree available to crawl increment edge count
            + diameter at node = functions of heights
                
                + grab the deepest node from the left
                + grab the deepst node from the right

                + add those edges and the edges to the children
        
        Time Complexity
            O(n)
        Space Complexity
            O(h) height of tree, so maybe O(n) if unbalanced

"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    
    def height(self, node: TreeNode) -> int:
        """Compute the maximum height/depth at this node
        
            - max number of available edges to crawl below
            - max height is max of left subtree and right subtree
            - diameter is the summation of the left and right
        """
        
        if node:
            
            left = 0
            right = 0
            
            # Edge available, crawl it
            if node.left:
                left = self.height(node.left) + 1
            if node.right:
                right = self.height(node.right) + 1
                
            # print(f' node: {node.val}   left: {left}  right: {right}')

            # Diameter = max depth on left + max depth on right
            diameter = left + right

            # Update max diameter
            self.max = max(diameter, self.max)
            
            return max(left, right)
        else:
            return 0
        
    def diameterOfBinaryTree(self, root: TreeNode) -> int:
        """
        
        - this calculataes the diameter of the root of a binary tree
        but not necessarily the maximum diameter of a tree
        - the max diameter may occur elsewhere???

        """

        self.max = 0

        # Crawl root node for maximum height while also track diameter
        self.height(root)

        return self.max        
        
        
        
