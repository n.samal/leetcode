# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

from collections import deque

class Solution:
    def iterative(self, p: TreeNode, q: TreeNode) -> bool:
        """Compute breadth first search on both trees.
        

        - use bfs on both trees simultaneously
        - compare values at each node for equality
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
        
        """
        
        # Null Null
        if not p and not q:
            return True
        
        # One is empty
        elif not p or not q:
            return False
        
        # Initialize with root nodes
        queue1 = deque([p])
        queue2 = deque([q])
        
        while queue1 and queue2:
            
            # Grab items from stack
            node1 = queue1.popleft()
            node2 = queue2.popleft()
            
            # Both are none
            if node1 is None and node2 is None:
                continue
            
            # One is none
            elif node1 is None or node2 is None:
                return False
        
            # Values aren't equal
            if node1.val != node2.val:
                return False
            
            # Add children
            queue1.append(node1.left)
            queue2.append(node2.left)
            
            queue1.append(node1.right)
            queue2.append(node2.right)
            
            
        # Both queues are empty
        if not queue1 and not queue2:
            return True
        else:
            return False

    def recursive(self, t1: TreeNode, t2: TreeNode) -> bool:
        """Compute breadth first search on both trees.
        

        - recursively check both trees for equality
        - compare values at each node for equality
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
        
        """
        
        # Both are empty
        if not t1 and not t2:
            return True
        
        # One is empty
        elif not t1 or not t2:
            return False
                    
        # Values aren't equal
        if t1.val != t2.val:
            return False

        # All lines up so far, compare the kids (subtrees)
        return self.recursive(t1.left, t2.left) and self.recursive(t1.right, t2.right)
