"""
Inputs
    root (TreeNode): the root!
Outputs
    int: number of univalued subtrees

Notes
    - A uni-value subtree means all nodes of the subtree have the same value.

Examples

    Example 1
        Input: root = [5,1,5,5,5,null,5]
        Output: 4

    Example 2

        Input: root = []
        Output: 0

    Example 3

        Input: root = [5,5,5,5,5,null,5]
        Output: 6

Cases

    No Kids

        5
       / \  this is univalued

    Single Kid

        5
       / \  this is univalued
      5

        5
       / \  this is univalued
          5

    Two kids

        5
       / \  this is univalued
      5   5

"""

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def countUnivalSubtrees(self, root: TreeNode) -> int:
        """Recusively crawl

        - check if the children are univalued
            + if yes, then compare to the current node value

        - when a univalued node/tree is found, then increment
        our global count

        Time Complexity
            O(n)
        Space Complexity
            O(h)

            h = stack height
            where h = n      when unbalanced, 
                  h = log(n) when balanced
        """

        self.count: int = 0

        def recurse(root):

            if root:

                unival_l = recurse(root.left)
                unival_r = recurse(root.right)

                # Two kids
                if root.left and root.right: 

                    if unival_l and unival_r and (root.left.val == root.val) and (root.right.val == root.val):
                        self.count += 1
                        return True
                    else:
                        return False

                # Single child: left
                elif root.left and root.right is None:

                    if unival_l and root.left.val == root.val:
                        self.count += 1
                        return True
                    else:
                        return False

                # Single child: right
                elif root.right and root.left is None:

                    if unival_r and root.right.val == root.val:
                        self.count += 1
                        return True
                    else:
                        return False

                # No kids
                else:
                    self.count += 1
                    return True

            else:
                return True

        recurse(root)

        return self.count