"""
Inputs
    root (TreeNode):
    low (int): valid lower bound
    high (int): valid upper bound
Outputs
    TreeNode: trimmed trim
Notes

    - remove all invalid nodes from the tree
    - do NOT change the relative structure of the elements
        + nodes descendants should remain descdants

Examples

    Example 1

        Input: root = [1,0,2], low = 1, high = 2
        Output: [1,null,2]

    Example 2

        Input: root = [3,0,4,null,2,null,null,1], low = 1, high = 3
        Output: [3,2,null,1]

Ideas

    - crawl the tree and recursively remove values
    - if a node should be removed we must replace it with a valid 
    node if there is one
        + return should be another node or None if no children

    - binary search tree has behavior where rights are larger, lefts are smaller
        + this pattern applies to the children

        ie: if 4 is too large, everything to the right of 4 is larger. 
        we must remove everything to the right and just return the left

    - 

"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def trimBST(self, root: TreeNode, low: int, high: int) -> TreeNode:
        """

        - crawl the tree and remove invalid nodes
        - if node is invalid we can remove either all of left or 
        all of right
        - if node is valid, find it's valid children and update them

        Time Complexity
            O(n)  crawl the whole tree
        Space Complexity
            O(n)  stack height for recursion
        """

        if root:

            # Everything to the left is too small
            if root.val < low:
                return self.trimBST(root.right, low, high)

            # Everything to the right is too large
            elif root.val > high: 
                return self.trimBST(root.left, low, high)

            # Find the valid children for each side
            else:
                root.left = self.trimBST(root.left, low, high)
                root.right = self.trimBST(root.right, low, high)

        return root
