"""
Inputs
    root (TreeNode): root of binary search tree
    p (TreeNode): node 1
    q (TreeNode): node 2
Outputs
    TreeNode: lowest common ancestor 

Goals
    - find the lowest common ancestor of both nodes
    - all values will be unique
    - p and q will exist in the tree

Ideas
    - we want the lowest node that is a parent of p & q

    - check if current node is a parent of p & q
        + if yes dig deeper
        + if no stop
    
    - maintain a path of NLR
        + go through the whole tree and save path
        + iterate through the path backwards
        + once we've seen both nodes, find their parent
        + parent child relation is L = 2p + 1, R = 2p + 2
        
    - use PostOrder traversal (LRN) to crawl tree
        + mark when we've seen either of the nodes
        + when we've seen both then grab the next ancestor

Strategies
    

"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:


    ''' Recursion: Me '''
    def recursion2(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        """
        - Compute the post order traversal of the tree
            + left, right, node
        - mark when we've seen nodes
        - once we've seen both for the first time save it
        """
        
        self.lca = None
        self.p = p
        self.q = q

        self.recurse2(root)
        
        return self.lca

    def recurse2(self, node: TreeNode) -> bool:
        """Compute post order traversal (LRN)
        
        - have we seen both p and q?
            + either here, on left subtree, or right subtree
        - only save the answer the first time we've seee p and q
        """
        
        if node:
            
            seen_p1, seen_q1 = self.recurse2(node.left)
            seen_p2, seen_q2 = self.recurse2(node.right)

            # We saw the target node either to left, right or here
            seen_p = seen_p1 or seen_p2 or node == self.p
            seen_q = seen_q1 or seen_q2 or node == self.q

            print(f'node: {node.val}    seen_p: {seen_p}    seen_q: {seen_q}')

            # We haven't had an answer yet, and we've seen p and q
            if self.lca is None and seen_p and seen_q:
                self.lca = node

            return seen_p, seen_q
            
        # Null case
        else:
            return False, False

    ''' Recursion: Suggested '''

    def recursion1(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        """
        - Compute the post order traversal of the tree
            + left, right, node
        - mark when we've seen nodes
        - once we've seen both grab the first
        """
        
        self.lca = None
        self.p = p
        self.q = q

        self.recurse(root)
        
        return self.lca
    
    def recurse(self, node: TreeNode) -> bool:
        """Compute post order traversal (LRN)
        
        - use post order to search leafs before nodes
        - if we see a node mark it as seen
            + either p, or either q
        - when we've seen both p & q save it as a LCA

        Notes
            - if one of the node occurs in a subtree mark True
            - the first node that has found both has sum >= 2
            - all others subtree        
        """
        
        if node:
        
            # Search the children
            seen_left = self.recurse1(node.left)
            seen_right = self.recurse1(node.right)
            seen_here = node == self.p or node == self.q
            
            print(f'node: {node.val}    seen_here: {seen_here}    seen_1: {seen_left}    seen_2: {seen_right}')
            
            # We've seen both for the first time
            # Make sure this is not updated again
            if (seen_left + seen_right + seen_here) >= 2:
                print(' seen')
                self.lca = node
            
            # Found one of the nodes
            if seen_left or seen_right or seen_here:
                return True
            else:
                return False
            
        # Null case
        else:
            return False

    ''' Iterative '''

    def iterative_ancestry(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        """


        - crawl through the entire tree and track the parents of each node
        - once we have all the parents, search for commonality
        - save the ancestry for one node
        - using the other node start from the bottom and crawl
        up the lineage. Once we fina a commonality stop
        
        Time Complexity
            O(n)

            go through the whole tree to find the parents
            potentially twice for common parents
        
        Space Complexity
            O(n)

            storing parent for each Node
        """

        stack = [root]

        # Root has no parent
        parents = {root: None}

        while stack:

            node = stack.pop()

            # Add the parent for each child, and add it for 
            # further exploration
            if node.left:
                parents[node.left] = node
                stack.append(node.left)

            if node.right:
                parents[node.right] = node
                stack.append(node.right)
        
        # Find the parents of the base nodes
        par_p = p
        
        # Save the ancestry of node p
        ancestors_p = set()

        print('P Ancestry')
        while par_p:

            print(f' p: {par_p.val}')

            # Grab the parent of each node
            # problem statement saves a node is an ancestor of itself
            ancestors_p.add(par_p)
            par_p = parents[par_p]

        # Crawl the ancestory of q
        par_q = q
        
        print('Q Ancestry')
        # Stop moving up while we haven't found a commonality
        while par_q not in ancestors_p:
            print(f' q: {par_q.val}')

            par_q = parents[par_q]

        return par_q