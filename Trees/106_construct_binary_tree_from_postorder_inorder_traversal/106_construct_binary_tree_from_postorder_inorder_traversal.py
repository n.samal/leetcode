# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:

    ''' Recursion: Index Search '''

    def buildTree(self, inorder: List[int], postorder: List[int]) -> TreeNode:
        """
        
        - root value is the last value in our post order
        - use this location to split the inorder into left subtree and right subtree
        
        Time Complexity
            O(n) same as previous problem, breaking into many subproblems half the size
        Space Complexity
            O(n) saving the whole tree

        Notes:
            -finding the root val index can be quite expensive
            potentially searching O(n) each time so O(n^2)
        """
        n: int = len(postorder)

        # No items in array
        if n == 0:
            return None
        elif n == 1:
            return TreeNode(postorder[-1])
            
        # Our root is last value in PostOrder
        root_val = postorder[-1]
        root = TreeNode(root_val)
        
        # Find location to split InOrder (Left, Node, Right)
        root_ix = inorder.index(root_val)
        
        # Grab the correct subarrays for recursion
        root.left = self.buildTree(inorder[:root_ix], postorder[:root_ix])
        root.right = self.buildTree(inorder[root_ix + 1:], postorder[root_ix:n-1])
        
        return root
