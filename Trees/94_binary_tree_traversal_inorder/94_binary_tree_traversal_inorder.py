# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def recursive(self, root: TreeNode) -> List[int]:
        """Inorder = LNR
        
        """
        self.paths = []
        self.recurse(root)
        
        return self.paths
        
    def recurse(self, node: TreeNode):
        """Explore the node if values exists"""
        if node:
            self.recurse(node.left)
            self.paths.append(node.val)
            self.recurse(node.right)
        
    ''' Iterative '''
    def inorderTraversal(self, root: TreeNode) -> List[int]:
        """Use depth first search to travel left subtrees
        first
        
        LNR
        
        - since we're added into a stack, we need to add values in reverse
        to prioritize new values ie: LNR => RNL
        
        """
        
        self.paths = []
        
        # Null case
        if not root:
            return []
        
        # Node, explored state
        stack = [(root, False)]
        
        while stack:
            
            node, explored = stack.pop()
            
            # We've been here before, or it's a leaf
            if explored or node.left is None and node.right is None:
                self.paths.append(node.val)
                continue
            
            # Add values in reverse because of stack
            if node.right:
                stack.append((node.right, False))
            
            stack.append((node, True))
            
            if node.left:
                stack.append((node.left, False))
            
        return self.paths
