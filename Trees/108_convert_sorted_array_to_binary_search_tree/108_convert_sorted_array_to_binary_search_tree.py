"""
Inputs
    nums (List[int]): sorted array 
Outputs
    TreeNode: root node of binary search tree
Goal
    - create a height balanced binary tree
    - height balanced means the depth of two subtrees is no larger
    than 1

Ideas
    - there can be many different trees created from this
    
    - choose the value in the middle as the root
        + values to left must be smaller
        + values to right must be larger

    - create root node and iterate

Strategies
    
    Iterative
        
        - generate a root node with the middle value
        
        - iterate right in array for larger values
        - iterate left in array for smaller values
        
        - create a giant inverted V
            + smaller and smaller values to left
            + larger and larger values to the right
    
    Recursive
    
        - if we can do it iterative we can likely do it
        recursively and generalize the strategy above
        
        - find the middle node of the entire array
            + generate the root
        
        - find the left and right nodes
            + middle of left half
            + middle of right half
        
        - continue this process recursively for subarrays to generate children
            ie:
            
            arr = [-20, -10, -3, 0, 5, 9, 25]
                                 x
                          x             x
                     x        x      x      x
            
            root = 0 (mid point of original)
            left = -10 (mid point of left subarray)
            right = 9 (mid point of right subarray)
            
            at each node 

        Time Complexity
            O(n)
        Space Complexity
            O(h) height of tree for n items

"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def sortedArrayToBST(self, arr: List[int]) -> TreeNode:
        
        # Null case
        if not arr:
            return None
        
        self.arr = arr
        n: int = len(arr)
            
        # Create root node from middle
        mid = n // 2
        root = TreeNode(arr[mid])
        
        # Set children recursively
        root.left = self.set_children(root, 0, mid - 1)
        root.right = self.set_children(root, mid + 1, n - 1)
        
        return root
    
    def set_children(self, node: TreeNode, left_ix: int, right_ix: int) -> TreeNode:
        """Find the children for the current node"""
        
        # Bounds have overlapped ie: no more values in this subarray
        if left_ix > right_ix:
            return None
        
        # Create node in middle of current subarray
        mid = (left_ix + right_ix) // 2
        node_new = TreeNode(self.arr[mid])
        
        # Set children recursively
        node_new.left = self.set_children(node_new, left_ix, mid - 1)
        node_new.right = self.set_children(node_new, mid + 1, right_ix)
        
        return node_new
        
