"""
Inputs
    root (TreeNode): binary search tree
Outputs
    TreeNode: balanced binary search tree

Notes
    - binary search tree has property where
    each node only has smaller values to the left
    and larger values to the right
    - a binary search tree is balanced if and only if
     the depth of the two subtrees of every node never differ by more than 1.
    - return any binary search tree that is balanced

Ideas
    - having all the values in sorted order will help
        + we can traverse the tree in LNR to find
        the smallest values first

        use depth first search

    - once we have the sorted values we can split the array
    in half to find the root values

    recurse, and put the larger values on right
    smaller values on left

    continue splitting the array while we have values

    indices  = [0, 1, 2, 3, 4, 5, 6]
    arr_odd  = [1, 2, 3, 4, 5, 6, 7]


                4
             /     \
            2       6
          /   \   /   \
         1     3 5     7  
    

    - pointers might save space instead of passing arrays to each method

    - should we do inclusive inclusive, or inclusive exclusive bounds
        + some bounds appear to result in repeated nodes
        + check node calc by hand

    arr_even = [1, 2, 3, 4, 5, 6]

    just use n // 2 to split, os it picks one value

"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

class Solution:

    def balanceBST(self, root: TreeNode) -> TreeNode:


        def dfs(root: TreeNode):
            """Preorder Traversal"""
            if root:
                dfs(root.left)
                arr.append(root.val)
                dfs(root.right)

        def split_arr(l: int, r: int):
            """Find the mid point and connect it"""

            if l <= r:
            
                mid = (l + r) // 2
                # print(f'left: {l}   right: {r}  mid: {mid}')

                node = TreeNode(arr[mid])
                node.left = split_arr(l, mid - 1)
                node.right = split_arr(mid + 1, r)

                return node

        # Gather values in sorted order
        arr = []
        dfs(root)
        
        # Split array in half recursively to recreate tree
        root_new = split_arr(0, len(arr) - 1)

        return root_new
