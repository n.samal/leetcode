"""
Inputs:
    root (TreeNode):
Outputs:
    None

Goal
    - flatten the linked list

Example

    For example, given the following tree:

        1
       / \
      2   5
     / \   \
    3   4   6
    The flattened tree should look like:

    1
     \
      2
       \
        3
         \
          4
           \
            5
             \
              6

Idea
    - similar to a linked list, keep track of the last node we saw
    - connect our previous node to our current node
"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

class Solution:
    def flatten(self, root: TreeNode) -> None:
        """
        Do not return anything, modify root in-place instead.
        """
        
        prev_node: TreeNode = None
        stack = [root]
        
        while stack:
            
            # Depth first search
            cur_node = stack.pop()
            
            if prev_node:
                print(f'cur_node: {cur_node.val} prev_node: {prev_node.val}')
            else:
                print(f'cur_node: {cur_node.val}')

            # Copy original pointers
            left_node = cur_node.left
            right_node = cur_node.right
            
            # Add children (last child gets searched first)
            if right_node:
                stack.append(right_node)
            if left_node:
                stack.append(left_node)
            
            # Connect the current node to the previous node
            if prev_node:
                prev_node.right = cur_node
            
            # Save current node as our next previous
            prev_node = cur_node
    
        # Checking method
        cur_node = root

        while cur_node:

            print(f'{cur_node.val}')
            cur_node = cur_node.right
            
    def flatten2(self, root: TreeNode) -> None:
        """Depth first search with a stack

        - prioritize left nodes first, then right
        - on each pop connect the previous value to our 
        current node

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        if root:
            stack = [root]
        else:
            stack = []
        
        node_prev = TreeNode()
        
        while stack:
            
            node = stack.pop()
            
            node_prev.left = None
            node_prev.right = node
            
            if node.right:
                stack.append(node.right)
                
            if node.left:
                stack.append(node.left)
        
            node_prev = node
            
        