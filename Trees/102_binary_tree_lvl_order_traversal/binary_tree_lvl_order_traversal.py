# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

from collections import deque

class Solution:
    def levelOrder(self, root: TreeNode) -> List[List[int]]:
    
        queue = deque([])
        levels = []
        
        if root:
            queue.append((1, root))  # (Node, level)
        
        while queue:
            
            # Get item from queue
            level, node = queue.popleft()
            
            print('node:', node.val, 'level:', level)
            
            # Save node to new level
            if level > len(levels):
                levels.append([node.val])
            
            # Save to current level
            else:
                levels[level-1].append(node.val)

            # Add to the queue
            if node.left:
                queue.append((level + 1, node.left))
            if node.right:
                queue.append((level + 1, node.right))
            
        return levels
   