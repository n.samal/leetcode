"""
Inputs:
    TreeNode: root node of the tree
Outputs:
    List[List[int]]: node values for each level in order
Goals
    - find the zigzag level order traversal of nodes
    - first row is left to right
    - next row if right to left
    - this continutes

Ideas
    - compute level order traversal, then reverse 

Strategies

    Level Order Traversal with Reversal
        - compute normal level order traversal
        - reverse the nodes on every other level
        
        - extra time is spent reversing the nodes
    
        Time Complexity
            O(n)
        Space Complexity
            O(n)
    
    Alternative Popping
        
        - similar logic to level order but with alternation
    
        - create two queues
            + current level
            + next level
        
        - even numbered levels
            + pop items from left
            + add new items: 
                * append right
                * left child, then right child
        
        - odd numbered levels
            + pop items from right
            + add new items
                * append left
                * right child then left child

        Time Complexity
            O(n)
        Space Complexity
            O(n)
"""


from collections import deque

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def zigzagLevelOrder(self, root: TreeNode) -> List[List[int]]:
        """Level order then reverse"""
        # Null case
        if not root:
            return None
        
        # Create a queue and place to store levels
        levels = []
        level_max: int = 0
        queue = deque([(root, 1)])
        
        while queue:
            
            # Grab the node (breadth first style)
            node, level = queue.popleft()
            
            ''' Save the node to the appropriate level '''
            
            # New level 
            if level > level_max:
                levels.append([node.val])
                level_max = level
            
            # Previous level
            else:
                levels[-1].append(node.val)
            
            # Add children to the queue
            if node.left:
                queue.append((node.left, level + 1))
            if node.right:
                queue.append((node.right, level + 1))
        
        # Iterate through levels and reverse every other row
        for ix in range(len(levels)):
            
            # Reverse odd numbered rows
            if ix % 2 !=0:
                levels[ix] = levels[ix][::-1]
            
        return levels
    
def alternate(self, root: TreeNode) -> List[List[int]]:
        """Level order then reverse"""
        
        # Null case
        if not root:
            return None
        
        # Create a queue and place to store levels
        level_cur: int = 0
        level_max: int = -1
        levels = []
        cur_queue = deque([(root, 0)])
        next_queue = deque([])

        while cur_queue:

            # Odd numbered level
            if level_cur % 2 != 0:

                node, level = cur_queue.pop()

                # Add children to the queue (right then left)
                if node.right:
                    next_queue.appendleft((node.right, level + 1))
                if node.left:
                    next_queue.appendleft((node.left, level + 1))

            # Even numbered level
            else:

                node, level = cur_queue.popleft()

                if node.left:
                    next_queue.append((node.left, level + 1))
                if node.right:
                    next_queue.append((node.right, level + 1))

            print(f'level: {level_cur}  node: {node.val}')
                    
            # New level 
            if level > level_max:
                levels.append([node.val])
                level_max = level

            # Previous level
            else:
                levels[-1].append(node.val)
            

            # Queue is empty, move to next level
            if not cur_queue:
                level_cur += 1
                cur_queue = next_queue
                next_queue = deque([])

        return levels