"""
Inputs:
    root (TreeNode): root node of binary tree
Outputs:
    List[int]: values of traversal order
Goal
    - find the post order traversal of nodes
    
    Post Order = LRN

Strategies

    Recursive
        - 
        - generate general recursive function
        - call recursion on the left subtree first
        - then the right subtree
        - then add the node value
    
        Time Complexity
            O(n)
        Space Complexity
            O(n)  saving all the values
    
    Iterative: Modified Pre Order
    
        - PreOrder: NLR
        - PostOrder: LRN
        
        - we we reverse the preorder we're close to the post order path
        - PreOrder Reversed: RLN
        - PostOrder: LRN
            
            + if we modify the the preorder path to prioritize Left instead of right
            we can use this strategy
    
        Time Complexity
            O(n)
        Space Complexity
            O(n)  saving all the values

"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    
    ''' Recursive '''
    
    def recursive(self, root: TreeNode) -> List[int]:
        """Recursive approach"""
        
        # Null case
        if not root:
            return []
        
        self.path = []
        self.recurse(root)
        
        return self.path
        
    def recurse(self, node: TreeNode):
        """Post order recursion
        
        - left subtree
        - right subtree
        - node (current value)
        """
        
        if node.left:
            self.recurse(node.left)
        
        if node.right:
            self.recurse(node.right)
        
        if node:
            self.path.append(node.val)
    
    ''' Iterative '''
            
    def modified_preorder_reversed(self, root: TreeNode) -> List[int]:
        """Iterative approach

        - use a modified preorder
            + Original: NLR
            + Modified: NRL
        
        - reverse the order at the end
            Modified Reverse: LRN
            PostOrder: LRN
        """
        
        # Null case
        if not root:
            return []
        
        path = []
        stack = [root]
        
        while stack:
            
            # Grab newest node and save
            node = stack.pop()
            path.append(node.val)
            
            # We want to explore the left subtree last
            # put this first since we prioritize new nodes
            if node.left:
                stack.append(node.left)
            if node.right:
                stack.append(node.right)
        
        return path[::-1]
        
        
    def postorderTraversal(self, root: TreeNode) -> List[int]:
        """Actual post order traversal
        
        PostOrder: LRN
        
        - using depth first search we can dig deeper in a subtree
        before exploring other parts of the graph
            
            + since we want the order LRN, we add values backwards
            into our stack ie: N, R, L
            
            left subtree will be visited first
        
        - since we want to explore subtrees before the node itself
          we must put nodes back in the stack

            + to prevent duplicate work we need to mark nodes
            as explored or not

            + if we've explored a node's children before just add it
            + if the node has no kids, there's nothing to explore just add it
        
        Time Complexity
            
        
        """
        
        path = []
        
        # Null case
        if not root:
            return path
        
        # Node value, explored state
        stack = [(root, False)]
        
        while stack:
            
            node, explored = stack.pop()
            
            # print(f'node: {node.val} explored: {explored}')
            
            # We've been here before, and already explored the subtrees
            # or this is a leaf, just add the value
            if node.left is None and node.right is None or explored:
                path.append(node.val)
                continue
            
            # If this node has kids we haven't explored, we must explore them first
            # before adding this node value
            # Priority order = LRN, insert into stack in reverse order
            stack.append((node, True))
            
            if node.right:
                stack.append((node.right, False))
            if node.left:
                stack.append((node.left, False))
        
        return path

    def postorderTraversal2(self, root: TreeNode) -> List[int]:
        """Actual post order traversal
        
        PostOrder: LRN

        - modify if statements, to check for None
            + this allows for general appends
        
        """
        
        path = []
        
        # Null case
        if not root:
            return path
        
        # Node value, explored state
        stack = [(root, False)]
        
        while stack:
            
            node, explored = stack.pop()
            
            if node:

                # print(f'node: {node.val} explored: {explored}')
                
                # We've been here before, and already explored the subtrees
                # or this is a leaf, just add the value
                if node.left is None and node.right is None or explored:
                    path.append(node.val)
                    continue
                
                # If this node has kids we haven't explored, we must explore them first
                # before adding this node value
                # Priority order = LRN, insert into stack in reverse order
                stack.append((node, True))
                stack.append((node.right, False))
                stack.append((node.left, False))
            
        return path

        