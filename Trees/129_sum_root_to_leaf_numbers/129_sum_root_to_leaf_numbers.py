
"""

Ideas
    - crawl along a path from the root and add digits
        + add digits like strings
    - once we reach a leaf node convert that to a number and return it
    - sum the values from each path

Strategies

    Recursion: strings
        - crawl along a path from the root and add digits
        - convert digits to a string and concatenate them

            ie: 9 + 1 = 91

        - once we reach a leaf node turn this into a integer
        and add to the total summation
        - if more nodes to explore go explore them

    Recursion: integers
        - crawl along a path from the root and add digits
        - maintain digits as integers

            path = 9, 1

            we can multiply by base of 10 and add it

            value = 9 * 10 + 1 = 91

            path = 9, 1, 5

            path: 9
            path: 91, 1 => 9 * 10 + 1 = 91
            path: 915, 91 * 10 + 5 = 915

        - if more nodes to explore go explore them
        - once we reach a leaf node add to the total summation

        Time Complexity
            O(n)
        Space Complexity
            O(h) size of binary tree recursion stack


"""


class TreeNode:

    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:

    ''' Recursion: digits '''

    def recursion_sum_int(self, root: TreeNode) -> int:
        """Initialize sum and crawl all paths"""
        # No value
        if not root:
            return 0

        # Total summation
        self.sum = 0

        # Begin crawling from root and add paths to leaf
        self.recursion_int(root)
        return self.sum

    def recursion_int(self, root: TreeNode, path_sum: int = 0) -> int:
        """Recursively crawl the paths from this node to leaves

        - crawl all child paths until we hit a leaf
        - once at a leaf, convert the path to a number, then add it
        to our total summation

        Time Complexity
            O(n)

        Space Complexity
            O(h) recursion height for tree
        """

        # print(f'path: {path_sum}')

        # At leaf node, add the current value
        if root.left is None and root.right is None:
            # print(f' => leaf: {path_sum}')
            self.sum += path_sum * 10 + root.val

        # Crawl left child path
        if root.left:
            self.recursion_int(root.left, path_sum * 10 + root.val)

        # Crawl right child path
        if root.right:
            self.recursion_int(root.right, path_sum * 10 + root.val)

    ''' Recursion: strings '''

    def recursion_sum_str(self, root: TreeNode) -> int:
        """Initialize sum and crawl all paths"""
        # No value
        if not root:
            return 0

        # Total summation
        self.sum = 0

        # Begin crawling from root and add paths to leaf
        self.recursion_str(root)
        return self.sum

    def recursion_str(self, root: TreeNode, digits: str = '') -> int:
        """Recursively crawl the paths from this node to leaves

        - crawl all child paths until we hit a leaf
        - once at a leaf, convert the path to a number, then add it
        to our total summation

        Time Complexity
            O(n)

        Space Complexity
            O(h) recursion height for tree
        """

        print(f'path: {digits}')

        # At leaf node, add the value
        if root.left is None and root.right is None:

            digit_new = digits + str(root.val)
            print(f' => leaf: {digit_new}')
            self.sum += int(digits + str(root.val))

        # Crawl left child path
        if root.left:
            self.recursion_str(root.left, digits + str(root.val))

        # Crawl right child path
        if root.right:
            self.recursion_str(root.right, digits + str(root.val))


if __name__ == "__main__":

    t1 = TreeNode(1)
    t1.left = TreeNode(2)
    t1.right = TreeNode(3)

    obj = Solution()
    # print(obj.sumNumbers(t1))
    print(obj.recursion_sum_int(t1))
