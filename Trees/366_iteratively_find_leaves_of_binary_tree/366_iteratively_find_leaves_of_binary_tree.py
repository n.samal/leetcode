"""
Inputs:
    root (TreeNode): root of binary tree
Ouputs:
    List[List[int]]: leave nodes after iteratively removing leaves
Goals
    - Collect the tree leaf nodes are you trim the leaves

Ideas
    
    - crawl through the tree
        + track the leaf node (values and pointers)
        + also track parents of each node
    
    - after the first traversal we know the leaf nodes, and their parents
    - for the next iteration
        + use the pointers from the leaves and crawl to the parents
        + set the previous leaves to equal None
        + check the current pointer location to see if these are leaves
            - if yes, then save them
        
        + continue this until we reach the leaf node
    
    - use heights
        
        - height of 0 = leaf node
        - height of 1 = next leaf
        - height of 2 = next next leaf

Strategy
    
    Height
    
        - calculate the height of each node
        - height is the number of edges from the node to the deepest leaf
        - once we have the height of each node we can put them into groups
        
        ie: 
            height = 0: 4,5,3
            height = 1: 2
            height = 2: 1
            
            Remember the height is to the deepest leaf, so left or right
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)

"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def findLeaves(self, root: TreeNode) -> List[List[int]]:
        """
        
        - initialize empty array of array
        - crawl the tree and node values for each height calculated
        
        ie: 
            Height 0: [4, 5, 3]
        
        """
        # Null case
        if not root:
            return []
        
        # Initial list of lists
        self.heights = [[]]
        
        # Crawl tree and get heights
        self.get_height(root)
        
        return self.heights
        
    def get_height(self, node: TreeNode) -> int:
        
        """Calculate the heigh at the current node
        
        - height is the maximum depth to a leaf node from here
        
        Args:
            node (TreeNode): node to crawl from
        Returns:
            int: height at the current node
        
        """
        
        # Init with no edges to crawl
        left = 0
        right = 0

        if node:
            
            # Crawl the available edge and count it
            if node.left:
                left = self.get_height(node.left) + 1
            
            if node.right:
                right = self.get_height(node.right) + 1
            
            # Grab maximum height of two paths
            height = max(left, right)
            
            # print(f'node: {node.val}  height: {height}')
            # print(f'heights: {self.heights}')
            
            # Save the height for the current node
            # Add to existing list or create a new sublist
            if height > len(self.heights) - 1:
                self.heights.append([node.val])
            else:
                self.heights[height].append(node.val)
            
            # print(f'heights: {self.heights}')
            return height

