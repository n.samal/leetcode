"""
Inputs:
    root (TreeNode): root node of binary tree
Outputs:
    bool: is the tree height balanced

Ideas
    - compute the height of each child
    - compare the height against left subtree and right subtree
        + if equal => True
        + if difference larger than 1 return False

    - seems like recursive type of problem
        + height of tree is max of 

Notes
    - height of tree is the number of edges between the tree's root and its furthest leaf

References
    https://www.hackerrank.com/challenges/tree-height-of-a-binary-tree/problem
    
    
Strategies
    
    Heights
        
        - height of current nodes is a function of childrens height
            height_i = 1 + max(height of left subtree, height of right subtree)
        - get the heights of both children
        - then compare
    
    Balanced
    
        - call the heights function to determine the height of each child
        - if heights differ more than 1 => fail
        - if not, check them for balance
            + we can recursively call the function
        
        Time Complexity
            O(n)
        Space Complexity
            O(h) height of recursion stack aka tree
    
"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

from collections import deque

class Solution:
    
    ''' Get Heights'''
    def get_height(node: TreeNode, level: int = 0) -> int:
        """Get the tree height of the current node
        
        - height is the maximum number of edges from the current
        node to a leaf node
        
        - we continue recursing and adding edges if there's a child to explore
        
        """
        
        left_height: int = 0
        right_height: int = 0
        
        if node.left:
            left_height = self.get_height(node.left, level + 1)
        if node.right:
            right_height = self.get_height(node.right, level + 1)
        
        # Grab the largest height
        return max(left_height, right_height, level)
    
    ''' Check Balance '''
    
    def isBalanced(self, root: TreeNode) -> bool:
        """Recursive approach"""
        
        # No nodes, it's balanced
        if not root:
            return True
        
        # Grab left height and right height
        left_height = self.get_height(root.left)
        right_height = self.get_height(root.right)
        
        # Heights differ too much
        if abs(left_height - right_height) > 1:
            return False
        
        # Check that both children are balanced
        else:
            return self.isBalanced(root.left) and self.isBalanced(root.right)
    
# Fails on leet code: but logic appears to be correct
        
        
