"""
Inputs:
    root (TreeNode): root of binary tree
Output:
    int: minimum depth of a leaf node 

Ideas
    - compute bfs or dfs
    - when we reach the leaf node then update our minimum depth
    
    Time Complexity
        O(n)
    Space Complexity
        O(1)
"""

from collections import deque

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def minDepth(self, root: TreeNode) -> int:
        """Iterative approach via bfs"""

        if not root:
            return 0
        
        min_depth: int = float('inf')
        queue = deque([(root, 1)])
        
        while queue:
            
            node, level = queue.popleft()
            
            # We've reached a leaf
            if node.left is None and node.right is None:
                min_depth = min(min_depth, level)
            
            # Node has kids, explore them
            if node.left:
                queue.append((node.left, level + 1))
            if node.right:
                queue.append((node.right, level + 1))
            
        return min_depth
