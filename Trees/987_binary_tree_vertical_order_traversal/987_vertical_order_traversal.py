"""
Inputs:

Outputs:


Goal
    - Return an list of non-empty reports in order of X coordinate
    - values will be sorted by column, row, then value
        + horizontal position, vertical, then value

Strategies

    Traverse & Sort
    
        - recursively determine the current positions
            + save the left most and right most position
            + track the vertical positions as well as value
            - save the values into a large list (we'll need to sort this later)

        - after aggregating all nodes
            + sort the list by horizontal, vertical then value
            + split the values into sub lists by horizontal position
            by iterating through the list

        Time Complexity
            O(n log(n))

        Space Complexity
            O(n)


        - improvements would be to use a heap or saving values directly into sublevels
        this would reduce the complexity O(n * log(N/k))

        k is the number of columns, so we have subgroups of max size N/k

"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def verticalTraversal(self, root: TreeNode) -> List[List[int]]:
        
        # Null case
        if not root:
            return []
    
        self.nodes = []
    
        self.pos_min = float('inf')
        self.pos_max = float('-inf')
        self.get_horizontal(root)
        
        # Sort the list of tuples by horizontal, vertical, then value
        self.nodes.sort()
        
        # print(f'nodes: {self.nodes}')
        
        # Separate values by horizontal position
        positions = [[] for _ in range(self.pos_min, self.pos_max + 1)]
        
        for horz, vert, value in self.nodes:
            
            rel_pos = horz - self.pos_min
            positions[rel_pos].append(value)
        
        
        # print(f'positions: {positions}')
        
        return positions
    
    def get_horizontal(self, node: TreeNode, horz_pos: int = 0, vert_pos: int = 0):
        """Compute horizontal position for each node
        
        Increment left child by -1
        Increment right child by +1
        
        Args:
            node (TreeNode): 
            pos (int): starting horizontal position
        """

        # Track left and right most nodes
        self.pos_min = min(horz_pos, self.pos_min)
        self.pos_max = max(horz_pos, self.pos_max)

        # Save nodes
        self.nodes.append((horz_pos, vert_pos, node.val))
        
        if node.left:
            self.get_horizontal(node.left, horz_pos - 1, vert_pos + 1)
        
        if node.right:
            self.get_horizontal(node.right, horz_pos + 1, vert_pos + 1)
        
