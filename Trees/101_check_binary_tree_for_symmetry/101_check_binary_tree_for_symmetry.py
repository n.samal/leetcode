"""
Inputs
    root (TreeNode): root node of the binary tree
Outputs
    bool: is this symmetric

Goals
    - check if a cycle is a mirror of itself

Ideas
    - we must add Null children, and check these for equality
    - we can go either recursive or iterative

Examples
        
    Example 1
        1
       / \
      2   2
     / \ / \
    3  4 4  3

    Example 2
         1
       /   \
      2     2
    /  \   / \
        3     3

    Example 3
          1
       /     \
      2       2
    /  \    /  \
   3   4   4    3
  / \ / \  / \  / \
 5  6 7 8  8 7 5  6  

Strategies

    Level Order Traversal then Check
    
        - compute a level order traversal via BFS
        - save the nodes visited, including null values
        - at each new level check the the nodes values were mirrored
        
        - only save the last level of nodes visited

        - this could get messy when dealing with Nulls
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)


    Iteratively
    
        - create a queue using BFS style
        - for the current level we can review the two outer most nodes
            + left most node against right most node
            + values should mirror each other
            
            left_most.left = right_most.right
            left_most.right = right_most.left
            
            + exit the loop early if it doesn't work

        - every level of the tree should have a node count that is a multiple of
        two ie: pairs, except for the root level
            + we can review pairs from outer to inner and check values
            + add their children in the same order and continue the process
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)

    Recursive
        - recursively call the tree checking function
        - similar in concept to the iterative approach

        Time Complexity
            O(n)
        Space Complexity
            O(log(n)) size of binary tree height

        
"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    
    def level_order(self, root: TreeNode) -> bool:
        """Compute level order traversal
        
        - check each level for symmetry after completion
        
        TODO:
            in progress
        
        """
        
        
        level_max: int = 0
        queue = [(1, root)]
        levels = []
        
        while queue:
            
            level, node = queue.pop()
            
            # New level, save the first value
            if level > level_max:

                # Check the last level for symmetry
                mid = len(levels) // 2
                
                if mid > 0 and levels[:mid] != levels[:mid - 1:-1]:
                    return False
                
                # Start new level
                levels = [node.val]
                level_max = level
            
            # Same level, add the values
            else:
                levels.append(node.val)
            
            # Add children to the queue
            queue.append()
            
        return True
    
    
    def iterative(self, root: TreeNode) -> bool:
        """Iterative checking with a queue
        
        - initialize the queue with the two children
            + the root is always itself
            + check that children match
        
        - all levels below contain multiples of 2 ie: pairs
            + add pairs of values for comparison (left most vs right most)
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        
        # Null case
        if not root:
            return True
        
        # Initialize queue with level 2
        queue = [root.left, root.right]
        
        while queue:
            
            left_tree = queue.pop()
            right_tree = queue.pop()
            
            # print(f'left_tree: {left_tree} right_tree: {right_tree}')
            
            # If both are null
            if left_tree is None and right_tree is None:
                continue
            
            # One of the trees is null
            elif left_tree is None or right_tree is None:
                return False
            
            # Check that tree values match
            if left_tree.val != right_tree.val:
                return False
            
            # Add children to the queue in checking order
            # left most against right most
            queue.append(left_tree.left)
            queue.append(right_tree.right)
            
            # Next inner pair
            queue.append(left_tree.right)
            queue.append(right_tree.left)
        
        return True

    ''' Recursive '''

    def recursive(self, root: TreeNode) -> bool:
        """Recusrively check for equality of each tree
        
        - initialize the queue with the two children
            + the root is always itself
            + check that children match
        
        - all levels below contain multiples of 2 ie: pairs
            + add pairs of values for comparison (left most vs right most)
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        
        # Null case
        if not root:
            return True
        
        return self.is_mirror(root.left, root.right)

    def is_mirror(self, left_tree: TreeNode, right_tree: TreeNode) -> bool:
        """Recursively check the left most and right most tree against
        one another

            - the root values must match
            - left most tree must match the right most tree
        """

        # Both trees are none
        if left_tree is None and right_tree is None:
            return True

        # One tree is none
        elif left_tree is None or right_tree is None:
            return False

        # Values don't match
        if left_tree.val != right_tree.val:
            return False

        # Check subtrees against one another
        left_check = self.is_mirror(left_tree.left, right_tree.right)
        right_check = self.is_mirror(left_tree.right, right_tree.left)

        return left_check and right_check
    