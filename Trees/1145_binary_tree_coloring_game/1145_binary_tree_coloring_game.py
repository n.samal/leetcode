"""
Inputs
    root (TreeNode):
    n (int): number of nodes
    x (int): player 1s first move

Outputs
    bool: can player 2 win?

Notes

    - each node in the tree has a unique value

    - player 1 chooses node x
    - we can choose any node after

    - for all subsequent turns each player can
    only play unchoosen nodes that are adjacent to
    one of their colored nodes

    ie: left, right, parent

Ideas

    - gather the count of nodes for each subtree
        + we can gather this recursively

        count = current + left child + right child

    - we can play three moves to block player 1s growth
        + block the parent
        + block the left child
        + block the righ child

    - if we block the parent

        + player 1 can get their left child and right child trees

    - if we block left child
        + we get their left subtree

    - if we block right child
        + we get their right subtree

    - if we can gather more than 50% of the nodes we win

References:
    https://leetcode.com/problems/binary-tree-coloring-game/discuss/350738/Easy-to-understand-for-everyone
"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

class Solution:
    def btreeGameWinningMove(self, root: TreeNode, n: int, x: int) -> bool:
        """

        If player 2 can gather more than 50% of the nodes they win
        - player 2 can block of the neighboring nodes for 'x'
          either a left, right or parent

        Time Complexity
            O(n)
        Space Complexity
            O(h)  height of stack
        """

        # Count number of nodes of player 1s chosen subtree
        chosen = [0 , 0]

        def recurse(root) -> int:
            """Count the number of nodes in each subtree"""
            if root:

                count_l = recurse(root.left)
                count_r = recurse(root.right)

                if root.val == x:
                    chosen[0] = count_l
                    chosen[1] = count_r

                return 1 + count_l + count_r
            else:
                return 0

        count_total = recurse(root)

        # We block one of the children subtrees: either left or right
        # and gather all those nodes
        if chosen[0] > n / 2 or chosen[1] > n / 2:
            return True

        # We block the parent
        # player 1 gets = x + x_left + x_right
        # player 2 gets the rest
        if count_total - (1 + chosen[0] + chosen[1]) > 1 + chosen[0] + chosen[1]:
            return True
        else:
            return False
