# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:

    ''' Iterative '''

    def iterative1(self, root: TreeNode, sum: int) -> bool:
        """Iteratively use depth first search and add values along our path
        
        - add values to our path sum
        - check the summation once at a root node
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        
        if not root:
            return False
        
        # Include the first nodes values
        queue = [(root, root.val)]
        
        while queue:
            
            node, path_sum = queue.pop()
            
            # print(f'node: {node} path_sum: {path_sum}')
            
            # At a leaf node and the summation lines up
            if node.left is None and node.right is None and path_sum == sum:
                return True
            
            # We have kids, keep crawling
            if node.left:
                queue.append((node.left, path_sum + node.left.val))
            
            if node.right:
                queue.append((node.right, path_sum + node.right.val))
        
        return False
            
        
    def iterative2(self, root: TreeNode, sum: int) -> bool:
        """Iteratively use depth first search and add values along our path
        
        - subtract values to our path sum
        - check the summation once at a root node
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
        
        """
        
        if not root:
            return False
        
        # Include the first nodes values
        queue = [(root, sum - root.val)]
        
        while queue:
            
            node, path_sum = queue.pop()
            
            print(f'node: {node} path_sum: {path_sum}')
            
            # At a leaf node and the summation lines up
            if node.left is None and node.right is None and path_sum == 0:
                return True
            
            # We have kids, keep crawling
            if node.left:
                queue.append((node.left, path_sum - node.left.val))
            
            if node.right:
                queue.append((node.right, path_sum - node.right.val))
        
        return False
            
    ''' Recursive '''
    
    def recursive(self, root: TreeNode, sum: int) -> bool:
        """Recursively crawl the tree paths
        
        Time Complexity
            O(n)

        Space Complexity
            O(h) binary tree height
        """

        # Null case
        if not root:
            return False

        # Include current value
        target = sum - root.val

        # Add a leaf node and the target matches
        if root.left is None and root.right is None and target == 0:
            return True

        # Search the left and right paths
        return self.recursive(root.left, target) or self.recursive(root.right, target)
