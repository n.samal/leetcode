"""
Inputs
    root (TreeNode): it's a tree
Outputs
    int: total of every nodes tilt
Notes
    - tilt is the absolute difference between
    the sum of all left subtree node values 
    and all right subtree node values. 

    - if a node does not have a left child, 
    then the sum of the left subtree node values
    is treated as 0

    - same situation for the right side

Examples

    Example 1
        Input: root = [1,2,3]
        Output: 1
        Explanation: 
        Tilt of node 2 : |0-0| = 0 (no children)
        Tilt of node 3 : |0-0| = 0 (no children)
        Tile of node 1 : |2-3| = 1 (left subtree is just left child, so sum is 2; right subtree is just right child, so sum is 3)
        Sum of every tilt : 0 + 0 + 1 = 1

    Example 2

        Input: root = [4,2,9,3,5,null,7]
        Output: 15
        Explanation: 
        Tilt of node 3 : |0-0| = 0 (no children)
        Tilt of node 5 : |0-0| = 0 (no children)
        Tilt of node 7 : |0-0| = 0 (no children)
        Tilt of node 2 : |3-5| = 2 (left subtree is just left child, so sum is 3; right subtree is just right child, so sum is 5)
        Tilt of node 9 : |0-7| = 7 (no left child, so sum is 0; right subtree is just right child, so sum is 7)
        Tilt of node 4 : |(3+5+2)-(9+7)| = |10-16| = 6 (left subtree values are 3, 5, and 2, which sums to 10; right subtree values are 9 and 7, which sums to 16)
        Sum of every tilt : 0 + 0 + 0 + 2 + 7 + 6 = 15

Ideas
    - recurse and compute the sum of the each subtree
        + bring those subtree summations up to the top

"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def findTilt(self, root: TreeNode) -> int:

        self.total: int = 0  # total tilt

        def recurse(root: TreeNode) -> int:
            """

            - initialize summation
            - travel subtrees and grab node values for
            left and right subtrees

            Time Complexity
                O(n)
            Space Complexity
                O(n)
            """

            cur_sum: int = 0

            if root:

                # Gather subtree summations
                left = recurse(root.left)
                right = recurse(root.right)

                # Add tilt to total
                tilt_here = abs(left - right)
                self.total += tilt_here

                cur_sum = root.val + left + right

            return cur_sum

        recurse(root)        

        return self.total
