"""
Inputs
    root1 (TreeNode):
    root2 (TreeNode):
Outputs
    bool: can we flip pieces of the tree to make the trees equivalent

Notes
    
    - we can define a flip operation as follows: choose any node, and swap the left and right child subtrees.

"""

class Solution:
    def flipEquiv(self, root1: TreeNode, root2: TreeNode) -> bool:
        """Recurse and check

        Time Complexity
            O(n)  size of tree, we only traverse until we hit the bottom of the shortest tree
        Space Complexity
            O(h)  height of tree, we only traverse until we hit the bottom of the shortest tree
        """
        
        # One of the nodes is none
        if root1 is None or root2 is None:
            return root1 == root2

        # If the root's aren't equal
        elif root1.val != root2.val:
            return False
        
        # Now check the children
        #  - no flip required
        #  - or children must be flipped
        else:
            return (self.flipEquiv(root1.left, root2.left) and self.flipEquiv(root1.right, root2.right)) or (self.flipEquiv(root1.left, root2.right) and self.flipEquiv(root1.right, root2.left))
