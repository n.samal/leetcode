"""
Inputs
    root (TreeNode): start of a complete tree
Outputs
    int: number of nodes

Notes
    - in a complete binary tree every level, except possibly the last, is completely filled,
    - all nodes in the last level are as far left as possible. It can have between 1 and 2h nodes inclusive at the last level h.

Examples

    Input: 
        1
       / \
      2   3
     / \  /
    4  5 6

    Output: 6

Ideas

    - recurse and count each value that is non null
    - we know the tree structure
        + full everywhere but possibly the last level
        + each level doubles in size, so 2^h for level h

        + on the last level we don't know the value.

        it could be 1, or 2^h

        + additonally because of the layout, we know the location
        of nodes is preferential to the left side

    - full node count = 2 ^ (h + 1) - 1

        ie: 2^0 + 2^1 + 2^2 + ... + 2^h

Key Points

    - utilize the data structure layout to reduce complexity
        + fully levels everywhere but the last level
        + left child prioritized for filling

            * we could only search the left child to find the tree depth

"""

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def countNodes(self, root: TreeNode) -> int:
        """Recursively find the count

        - add a count for the current node and grab the counts
        for the children

        Time Complexity
            O(n)
        Space Complexity
            O(h)

            h = height of recursion stack/tree depth

            for a complete it should be of O(log(n))
        """

        if root:
            return 1 + self.countNodes(root.left) + self.countNodes(root.right)
        else:
            return 0
