"""
Inputs
    root (TreeNode): non empty binary tree
Outputs
    int: maximum path sum
Notes
    - a path is defined as any node seqeunce from a start node to any node
    along parent-child connections
    - the path must contain at least one node and does not need to go through
    the root
Examples

    Example 1

        Input: root = [1,2,3]
        Output: 6

    Example 2

        Input: root = [-10,9,20,null,null,15,7]
        Output: 42

Ideas

    - draw potential paths by hand
        + we can expand left and right from our current node
        + we can also continue a previous path, but must choose
        left or right and the current value

        + if we have a negative value for a child/root, it might be better to ignore it
        ie: -10
            /  \
           9    20

    - recursively crawl

        + track the summations from child paths seperately
        ie: max_below_l, max_below_r

"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def maxPathSum(self, root: TreeNode) -> int:
        """Recursively find the max path summation

        - at each node we have two choices
            + start a new path here, where we can have
            a branch go left and right
            + continue a path from above where we must
            pick either left or right while going through
            the current node

        Time Complexity
            O(n)
        Space Complexity
            O(h)  h = height of recursion stack/tree
            
            h = n      for a skewed tree
            h = log(n) for a balanced tree
        """

        self.max_sum: int = float('-inf')

        def recurse(root: TreeNode):

            if root:

                # Grab the sum for each side or ignore them
                sum_below_l = max(recurse(root.left), 0)
                sum_below_r = max(recurse(root.right), 0)

                # Start a new path here
                sum_new_path = root.val + sum_below_l + sum_below_r
                self.max_sum = max(self.max_sum, sum_new_path)

                # Continue a path by picking the better side
                return root.val + max(sum_below_l, sum_below_r)

            else:
                return 0

        recurse(root)

        return self.max_sum
