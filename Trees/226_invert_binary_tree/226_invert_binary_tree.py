"""
Input:
    root (TreeNode): root tree node
Output:
    Tree: root node of inverted tree
Goals
    - invert the binary tree

Ideas
    - recursively swap tree children, the subtree children etc..
    - use the same strategy using breadth first search


"""

from collections import deque


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def invertTree(self, root: TreeNode) -> TreeNode:
        """Iterative
        
        - use breadth first search to search nodes level by level
        
        Time Complexity
            O(n)
        Space Complexity
            O(n) on the second to last level our queue can be quite large
             size of n / 2
        """
        
        # Null Case
        if not root:
            return None
        
        queue = deque([root])
        
        while queue:
            
            node = queue.popleft()
            
            # Add children to the queue to explore subtrees if applicable
            if node.left:
                queue.append(node.left)
            if node.right:
                queue.append(node.right)
            
            # Swap the kids
            node.left, node.right = node.right, node.left
            
        return root
    
    def recursive1(self, root: TreeNode) -> TreeNode:
        """Recursive
        
        - same logic as iterative approach
        - crawl subtrees if there's something to explore
        
        Time Complexity
            O(n)
        Space Complexity
            O(h)  stack height which is height of tree
        """
        
        if not root:
            return None
        
        # Invert trees then swap
        left = self.invertTree(root.left)
        right = self.invertTree(root.right)
        
        root.left = right
        root.right = left
        
        return root
    
    def recursive2(self, root: TreeNode) -> TreeNode:
        """Recursive
        
        - same logic as iterative approach
        - crawl subtrees if there's something to explore
        
        Time Complexity
            O(n)
        Space Complexity
            O(h)  stack height which is height of tree
        """
        
        if not root:
            return None
        
        # Invert trees then swap
        root.left, root.right = self.invertTree(root.right), self.invertTree(root.left)
        
        return root
