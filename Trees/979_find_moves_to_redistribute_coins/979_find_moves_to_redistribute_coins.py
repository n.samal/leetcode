"""
Inputs
    root (TreeNode): binary tree
Outputs
    int: minimum number of moves necessary for balancing
Notes
    - we need to redistribute the coins that each node
    gets one coin each
    - coins may be allocated at any node
    - we can choose to move a coin between two adjacent nodes
        + either parent to child or child to parent

Examples

    Example 1

        Input: [3,0,0]
        Output: 2
        Explanation: From the root of the tree, we move one coin to its left child, and one coin to its right child.

    Example 2

        Input: [0,3,0]
        Output: 3
        Explanation: From the left child of the root, we move two coins to the root [taking two moves].  Then, we move one coin from the root of the tree to the right child.

    Example 3

        Input: [1,0,2]
        Output: 2

    Example 4

        Input: [1,0,0,null,3]
        Output: 4
Ideas

    - we shall recurse
    - compute the excess number of coins at each node
        + positive: we need to push off coins
        + negative: we need to pull some coins

    - both push and pull count as moves required

    - aggregate the counts from the kids to the parents
        + excess_total = excess_node + excess_l + excess_r
"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def distributeCoins(self, root: TreeNode) -> int:
        """Depth first search to distribute coins

        - recurse to the bottom and distribute coins up
        - track the excess at each node which is a function of
            + current coins
            + coin requirements for both children

        - moves required are a direct function of excess coins

        Time Complexity
            O(n)
        Space Complexity
            O(h)  recursion height
        """

        def recurse(root: TreeNode):

            if root:
                excess_here = root.val - 1
                excess_l = recurse(root.left)
                excess_r = recurse(root.right)

                excess = excess_here + excess_l + excess_r
                self.n_moves += abs(excess)

                return excess

            else:
                return 0

        self.n_moves: int = 0
        recurse(root)

        return self.n_moves