"""
Inputs
    N (int): total number of nodes for the tree
Outputs
    List[TreeNode]: root nodes for all possible full binary trees    

Ideas
    - each node can either have 2 kids or 0 kids
    - since we must have a root, this means that the total number of nodes
    must be odd

    - recurse is the way to go. We should try generating each node with either
    2 children or no children

    - the global root must be tracked, since we need those later
        + we could use recursion to bring that root back

Key Ideas
    - for each value from i = 1 to N - 1 (1 Node for our root)
    we put i nodes to the left tree, then the remaining on the right

References:
    https://leetcode.com/problems/all-possible-full-binary-trees/discuss/718229/Java-Simple-DFS
    https://leetcode.com/problems/all-possible-full-binary-trees/discuss/618779/Python-3-solution-(Recursion-Memoization-DP)-with-explantions
"""

from typing import List


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:

    def allPossibleFBT(self, N: int) -> List[TreeNode]:
        """

        - create a root node
        - combine the root with every possible left subtree
        - combine the root with every possible right subtree

        - the pattern is recursive, so we use the same function
        to define the subtrees
            + our base case is a singular node

        Time complexity
            O(2^n)
        Space complexity
            O(2^n)
        Notes
            - add memo to save time
        """

        # Base case: singular tree
        if N == 1:
            return [TreeNode(0)]

        # All full binary trees are a odd number of nodes
        if N % 2 == 0:
            return []

        results = []

        # Try creating trees where x + y + 1 = n
        #   one node for current root
        #   N - 1 nodes for subtrees = x + y
        #   x = left subtree count
        #   y = right subtree count
        for i in range(1, N, 2):

            for left in self.allPossibleFBT(i):
                for right in self.allPossibleFBT(N - 1 - i):

                    root = TreeNode(0)
                    root.left = left
                    root.right = right

                    results.append(root)

        return results


if __name__ == '__main__':

    obj = Solution()
    trees = obj.allPossibleFBT(7)
    print(trees)
