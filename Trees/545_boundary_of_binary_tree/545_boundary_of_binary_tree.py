"""
Inputs
    root (TreeNode): root of the binary tree
Outputs:
    List[int]: boundary path

Goals

    - Given a binary tree, return the values of its boundary in anti-clockwise direction starting from root. 
    - Boundary includes left boundary, leaves, and right boundary in order without duplicate nodes.  (The values of the nodes may still be duplicates.)

Notes
    - Left boundary is defined as the path from root to the left-most node
    - The left-most node is defined as a leaf node you could reach when you always firstly travel to the left subtree if exists. If not, travel to the right subtree. Repeat until you reach a leaf node.

    - Right boundary is defined as the path from root to the right-most node. If the root doesn't have left subtree or right subtree, then the root itself is left boundary or right boundary.
    - The right-most node is also defined by the same way with left and right exchanged.

Cases
    Null Case
    Single Node (no kids)
    General Case (some kids)

Ideas

    - use slight variation on recursion to get left and right boundaries
        + we only need to explore one subtree not both!
        + only continue exploring until we hit the bottom

    - use N (L or R) to find left path
        + we can ignore the nonchoosen path
        + stop recursion early after finding leaf
    
    - use any node traversal to determine leaves
        + save leaves separately
    
    - use N (R or L) to find the right path
        + we can ignore the nonchoosen path
        + stop recursion early after finding leaf
    
    - combine these three pieces together

Strategy
    3 Pieces

        - run method to get left path
            - starting at the left child of root (ignoring the root)
            - crawl to the leaf most available subtree until we hit a leaf
            - stop crawling when reached the leaf
        - run method to get right path
            - starting at the right child of root (ignoring the root)
            - crawl to the right most available subtree until we hit a leaf
            - stop crawling when reached the leaf
        - run method to get leaves
            - save nodes that have no kids

        - combine this with the root info to get our answer

        Time Complexity
            O(n)
        Space Complexity
            O(n)
"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def boundaryOfBinaryTree(self, root: TreeNode) -> List[int]:
        
        # Null case
        if not root:
            return []

        # Single node case
        if root.left is None and root.right is None:
            return [root.val]

        # Paths for general case
        self.root = [root.val]
        self.path_l = []
        self.path_r = []
        self.path_leaves = []
        
        # Find left path ignore the root and first leaf
        self.recurse_left_path(root.left)

        # Find leaves
        self.recurse_nlr(root)
        
        # Find right path excluding root and first leaf
        self.recurse_right_path(root.right)

        # Reverse right path
        self.path_r = self.path_r[::-1]
        
        print(f'root: {self.root}')
        print(f'left: {self.path_l}')
        print(f'leaves: {self.path_leaves}')
        print(f'right: {self.path_r}')
        
        # Combine the boundaries
        self.root.extend(self.path_l)
        self.root.extend(self.path_leaves)
        self.root.extend(self.path_r)
    
        print(f'combined: {self.root}')
        
        return self.root

    def recurse_nlr(self, node: TreeNode):
        """Recursive traversal of node"""
        
        if node:
            
            # Determine if at a leaf and save
            leaf = node.left is None and node.right is None
            
            if leaf is True:
                self.path_leaves.append(node.val)
            
            self.recurse_nlr(node.left)
            self.recurse_nlr(node.right)
    
    def recurse_right_path(self, node: TreeNode):
        """Recursive traversal of node
        
        - node right left
        - save the right path until we hit our first leaf
        """
        
        if node:
            
            # Determine if at a leaf
            leaf = node.left is None and node.right is None
            
            # Mark for global termination if already found our
            # first leaf
            if leaf is True:
                return
            else:
                self.path_r.append(node.val)
            
            # Only choose one subtree
            if node.right:
                self.recurse_right_path(node.right)
            else:
                self.recurse_right_path(node.left)
    
    def recurse_left_path(self, node: TreeNode):
        """Recursive traversal of node
        
        - node left
        - save the left path until we hit our first leaf
        
        """
        
        if node:
            
            # Determine if at a leaf
            leaf = node.left is None and node.right is None
            
            # Mark for global termination if already found our
            # first leaf
            if leaf is True:
                return
            else:
                self.path_l.append(node.val)
            
            # Only choose one subtree
            if node.left:
                self.recurse_left_path(node.left)
            else:
                self.recurse_left_path(node.right)
    
        