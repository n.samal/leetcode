"""
Inputs
    root (TreeNode): root node of the tree
    target (TreeNode): target node
    K (int): number
Outputs:
    List[int]: values of nodes located k away

Goals
    - Return a list of the values of all nodes that have a distance K from the target node.
    - The answer can be returned in any order.

Notes
    - nodes located upwards must be included as well

Ideas
    
    - crawl from the target node and monitor the number of edges traveled
        + when we are k edges from the target we've got a valid entry
        + when edge count goes beyond k, stop searching
    
    - calculate the distance from root node to target node
        + then search for the remaining difference
        
        ie: root is 
    
    - crawl the tree and grab parents for each node
        - using the parent we know this is 1 edge away. search for nodes k - 1 away
        - using the parents parent we know this is 2 away, search for node k - 2 away
        - we can generalize this and create an adjacency list from each node
        turning this into a graph. 
        - 
    
Strategies
    K Away

        - get the parents for each node
        - use a general method to find nodes a certain distance away


    Modified Breadth First Search

        - grab parents life before
        - use breadth first search to travel from the target
            + increment the number of edges each time we crawl
            + crawl to not only the left and right but also the parent
            + we need to keep track of the nodes we've visited so we don't revisit the same nodes
            multiple times and get stuck in a loop

            + 

"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:


    
    def get_parents(self, root: TreeNode) -> dict:
        """For each node keep track of it's parents
        
        Returbs:
            Dict[TreeNoode, TreeNode]: parent node for each node in the 

        """
        
        # Initialize root node with no parent
        parents = {root: None}

        def recurse(node: TreeNode):
            """Recursively crawl nodes and save parents"""
            
            # Nothing to search
            if not node:
                return
            
            # We've got kids
            if node.left:
                parents[node.left] = node
                recurse(node.left)
                
            if node.right:
                parents[node.right] = node
                recurse(node.right)
                
        recurse(root)
            
        return parents
        

    def get_nodes_k_below(self, root: TreeNode, k: int) -> set:
        """Crawl from the root node and find values k away
        
        Args:
            k (int): number of edges away
        """
    
        s = set([])

        def recurse(node: TreeNode, edges: int):
            """Recursively crawl nodes and count edges
            
            Args:
                edges (int): number of edges to crawl
            """
            
            # Nothing to search
            if not node:
                return
            
            # We've crawled all the edges, save the node
            if edges == 0:
                s.add(node.val)

            # More edges left to crawl
            if edges > 0:

                # We've got kids
                if node.left:
                    recurse(node.left, edges - 1)
                    
                if node.right:
                    recurse(node.right, edges - 1)
        
        # Crawl k edges from here
        recurse(root, k)
        
        return s

    ''' Modified Breadth First Search '''

    def bfs(self, root: TreeNode, target: TreeNode, K: int) -> List[int]:
        """Modified breadth first search

        - crawl the left and right child
        - also crawl the parents
        - keep track of the nodes we've visited to ensure we don't get into
        a loop

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        # No edges to crawl
        if K == 0:
            return [target.val]
        
        # Null case
        if not target or not root:
            return []
        
        # Find nodes below that are 
        parents = self.get_parents(root)
    
        # Node and edges away
        ans = []
        queue = deque([(target, 0)])
        seen = set([])

        while queue:

            node, edge_count = queue.popleft()

            # Found something k away
            if edge_count == K:
                ans.append(node.val)

            # Add to seen
            seen.add(node)

            # Adjacent nodes
            adjancencies = [node.left, node.right, parents.get(node, None)]

            for adj in adjancencies:

                # We haven't been there yet
                if adj and adj not in seen:
                    queue.append((adj, edge_count + 1))

        return an


    
    ''' Iterate to K: Incomplete but close '''

    def distanceK(self, root: TreeNode, target: TreeNode, K: int) -> List[int]:
        
        # No edges to crawl
        if K == 0:
            return [target.val]
        
        # Null case
        if not target or not root:
            return []
        
        # Values that meet our criteria
        self.s = set([])
    
        # Find nodes below that are 
        parents = self.get_parents(root)

        # print('Parents')
        # print(parents)
        
        node = target

        # Crawl edges 0 to K - 1 away
        for k in range(K):

            # Find nodes k away
            s = self.get_nodes_k_below(node, K - k)

            # print(f'node: {node.val}    k: {k}   s: {s}')

            # Add to these to our set
            self.s = self.s.union(s)

            # Grab the parent
            node = parents.get(node, None)

        # Remove root from answers
        if target.val in self.s:
            self.s.remove(target.val)

        # print(f'k_away: {self.s}')
        
        return [val for val in self.s]
