# from collections import deque


class Node(object):
    def __init__(self, val=None, children=None):
        self.val = val
        self.children = children

    def __repr__(self):
        return f'Node({self.val})'


class Codec:
    def serialize(self, root: 'Node') -> str:
        """Encodes a tree to a single string.

        - encoded the tree into a hiarchical string format
            + [1 [3[5 6] 2 4]]
            + 1 has children (3, 2, 4)
            + 3 has children (5, 6)

        Args:
            root (Node): root of n-ary tree
        Returns:
            str: encoded n-ary tree
        """

        if not root:
            return ''

        def recurse(root: 'Node'):
            """Traverse in preorder NLR"""

            if root:
                self.arr.append(f'{root.val}')

                if root.children:
                    self.arr.append('[')

                    for child in root.children:
                        recurse(child)

                    self.arr.append(']')

        self.arr = ['[']
        recurse(root)
        self.arr.append(']')

        self.arr = ' '.join(self.arr)

        return self.arr

    def deserialize(self, data: str) -> 'Node':
        """Decodes your encoded data to tree.

        - encode a string seperated by brackets
            + children are indicated with [kid1 kid2 kid3]

            + [1 [3 2 4]]
            + [1 [3[5 6] 2 4]]
        """

        if not data:
            return None

        children, _ = self.recurse(data, 0)

        return children[0]

    def recurse(self, data: str, ix: int):

        n: int = len(data)
        nodes = []

        while ix < n:

            if data[ix].isdigit():

                # Get the value
                v: int = 0

                while data[ix].isdigit():
                    v = v*10 + int(data[ix])
                    ix += 1

                nodes.append(Node(v))

            elif data[ix] == ' ':
                ix += 1

            elif data[ix] == '[':
                children, ix = self.recurse(data, ix + 1)

                if nodes:
                    nodes[-1].children = children
                else:
                    nodes = children

            elif data[ix] == ']':
                ix += 1
                break

        return nodes, ix


# Your Codec object will be instantiated and called as such:
# codec = Codec()
# codec.deserialize(codec.serialize(root))

if __name__ == '__main__':

    # Example 1
    data = '[1 [3[5 6] 2 4]]'

    obj = Codec()
    root = obj.deserialize(data)

    data_str = obj.serialize(root)
    print(data_str)
