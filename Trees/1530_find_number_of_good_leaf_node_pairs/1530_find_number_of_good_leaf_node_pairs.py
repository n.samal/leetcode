from collections import deque
from typing import List


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

    def __repr__(self):
        return f'TreeNode({self.val})'

class Solution:

    ''' Intuition '''

    def countPairs(self, root: TreeNode, distance: int) -> int:
        """

        - crawl through tree and get the following
            + leaf nodes
            + parents of each node
            + distance of each node from the root (levels)

        - compare all leaf nodes against one another
            + if nodes are on different levels, then equalize them
            + find the lowest common ancestor
            + calculate the distance between the nodes

                A
               / \
              N1  N2

              dist = dist1 + dist2

            + check if the pair is under the distance requirement

        Time Complexity
            O(n^3?)  iterating between pairs then crawling to LCA

            crawling n levels to find LCA for each node pair

        Space Complexity
            O(n)  storing parents and distances

        Notes
            - we spend extra time finding the LCA
        """

        # Find all the leaves
        queue = deque([(root, None, 0)])

        parents = {}
        distances = {}
        leaves = []

        while queue:

            node, par, dist = queue.popleft()

            # Save distances for each node and parents
            # duplicate values exist, so use the node itself
            parents[node] = par
            distances[node] = dist

            if node.left:
                queue.append((node.left, node, dist + 1))

            if node.right:
                queue.append((node.right, node, dist + 1))

            # Lead node
            if node.left is None and node.right is None:
                leaves.append(node)

        ''' Find the distance between the leaves '''

        count: int = 0
        n: int = len(leaves)

        # Compare nodes
        for ix in range(n):
            for jx in range(ix + 1, n):

                cur1 = leaves[ix]
                cur2 = leaves[jx]

                # print(f'leaves: {cur1.val}  {cur2.val}')

                # Get parent nodes at the same level
                dist1 = distances[cur1]
                dist2 = distances[cur2]

                while dist1 != dist2:

                    if dist1 > dist2:
                        cur1 = parents[cur1]
                        dist1 = distances[cur1]
                    else:
                        cur2 = parents[cur2]
                        dist2 = distances[cur2]

                par1 = parents[cur1]
                par2 = parents[cur2]

                while par1 and par2 and par1 != par2:
                    # print(f' parents: {par1.val}  {par2.val}')
                    par1 = parents[par1]
                    par2 = parents[par2]

                # print(f' -> parents: {par1}  {par2}')

                # Found common ancestor
                # grab distance to from node to common parent
                if par1 == par2:                
                    dist1 = distances[leaves[ix]] - distances[par1]
                    dist2 = distances[leaves[jx]] - distances[par1]

                # One of the nodes is at the root
                else:
                    dist1 = distances[leaves[ix]]
                    dist2 = distances[leaves[jx]]

                # print(f' dist: {dist1}  {dist2}')

                dist_sum = dist1 + dist2

                if dist_sum <= distance:
                    count += 1

        return count

    ''' Method 2 '''

    def method2(self, root: TreeNode, distance: int) -> int:
        """Track leaves at each node

        - at each node track the number of leaves a certain distance away
        - at each node count the number of pairs that have leaves
        under the distance requirement
            + we only compare leaves between the left and right subtrees
            ie: at their lower common ancestor

        Time Complexity
            O(n*distance*distance)
        Space Complexity
            O(n*distance)

        References:
            https://leetcode.com/problems/number-of-good-leaf-nodes-pairs/discuss/756068/C%2B%2B-Intiuitive-Commented-With-Diagram-For-each-leaf-on-left
            https://leetcode.com/problems/number-of-good-leaf-nodes-pairs/discuss/755784/Java-Detailed-Explanation-Post-Order-Cache-in-Array
        """

        self.count = 0
        _ = self.crawl(root, distance)
        return self.count

    def crawl(self, root: TreeNode, distance: int):

        # Save number of leaf nodes at each distance
        distances = [0 for _ in range(distance + 1)]

        if not root:
            return distances

        # We are the leaf, at a distance 1 from our previous node
        if root.left is None and root.right is None:
            distances[1] = 1
            return distances

        leaves_l = self.crawl(root.left, distance)
        leaves_r = self.crawl(root.right, distance)

        # Aggregate node pairs that meet our distance requirement
        # 1 node and 2 nodes = 2 pairs
        # 2 node and 2 nodes = 4 pairs
        for l in range(distance + 1):
            for r in range(distance + 1):
                if l + r <= distance:
                    self.count += leaves_l[l] * leaves_r[r]

        # Aggregate leaf node counts from subtrees
        # bump distance of subtrees by 1 edge
        for ix in range(distance):
            distances[ix + 1] += leaves_l[ix] + leaves_r[ix]

        return distances


def get_nodes(arr: List[int]) -> TreeNode:
    """Generate tree nodes for the node list"""

    n: int = len(root)

    for p in range(n - 1, -1, -1):

        left = 2*p + 1
        right = 2*p + 2

        # Connect to left
        if left < n:
            left = arr[left]
        else:
            left = None

        # Connect to right
        if right < n:
            right = arr[right]
        else:
            right = None

        arr[p] = TreeNode(arr[p], left, right)

    return arr[0]


if __name__ == "__main__":

    # Example 1
    # root = TreeNode(val= 1, left= TreeNode(val= 2, left= None, right= TreeNode(val= 4, left= None, right= None)), right= TreeNode(val= 3, left= None, right= None))
    root = [1, 2, 3, 4, 5, 6, 7]
    root = get_nodes(root)
    distance = 3

    obj = Solution()
    print(obj.method2(root, distance))
