"""
Input:
    root (TreeNode):
Output:
    int: next smallest number
    bool: is a smaller number available

Ideas

    - crawl the tree for the smallest number
        + grab the left most node
        + crawl left if a left tree is available
        + otherwise next smallest should be current node
        + after that should be right
    
    - if the tree has more nodes, a smaller number is available
        + root is None => False
        + root is not None => True
    
    - every time we get number we need to remove the value
        + remove the current smallest and update the tree
            * if the value has children connect to the parent
        + seems similar to how a heap works
        + could be bad to modify the value in place

Strategies
    
    Traverse Path & Pop
        - traverse the path that prioritizes the leftmost node
            + Inorder: LNR
            + this prioritizes the smallest values first

        - we can traverse the entire path
            ie: [1, 2, 3, 4, 5]

        - if we have values in our path then return True
        - if we need a value just grab from the left

        Time Complexity
            O(n) crawling the entire path
        Space Complexity
            O(n) saving the entire path

        Notes:
            - this requires traversal of the whole tree upfront
            - can we traverse on the fly?

    Iterative
        - use an iterative approach to generate the path
        - since we're using InOrder traversal we need to use
        depth first search and mark whether nodes have been explored

        - instead of continually iterating until our stack is empty
        we will only iterate until we find a leaf node or explored node

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        we may have to crawl a large amount of the tree to find a leaf or explored value
"""

from collections import deque


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

''' Inorder Traversal: Recursion '''


class BSTIterator:

    def __init__(self, root: TreeNode):
        

        # Traverse the tree via InOrder Traversal
        self.paths = deque([])
        self.recurse(root)


    ''' Recursion '''
    def recurse(self, node: TreeNode):
        """Inorder Traversal"""

        if node:

            self.recurse(node.left)
            self.paths.append(node.val)
            self.recurse(node.right)


    ''' Class Methods '''

    def next(self) -> int:
        """
        @return the next smallest number
        """
        return self.paths.popleft()
        

    def hasNext(self) -> bool:
        """If we have values left, then we can provide a smaller number
        @return whether we have a next smallest number
        """
        return len(self.paths) > 0 
        
        
''' Inorder Traversal: Iterative '''        

class BSTIterator2:

    def __init__(self, root: TreeNode):
        
        # Seed via InOrder Traversal with root

        self.stack = []

        # Only add values that aren't null
        if root:
            self.stack.append((root, False))

    def iterate(self):
        """Iterate aproach to Inorder Traversal
        
        - continue traversal until we find a valid node
            + either a leaf node
            + another node we've explored before

        """

        value_found = False

        while value_found is False: 

            # Grab value from our stack (depth first search)
            node, explored = self.stack.pop()
    
            # We're at a node we've explored or a leaf node
            if explored or node.left is None and node.right is None:
                value_found = True
                continue

            # Add values to our queue in reverse since using depth first search
            # InOrder: LNR => R, N, L
            if node.right:
                self.stack.append((node.right, False))

            # This will be second visit to the node
            self.stack.append((node, True))

            if node.left:
                self.stack.append((node.left, False))

        return node.val


    ''' Class Methods '''

    def next(self) -> int:
        """
        @return the next smallest number
        """
        return self.iterate()
        

    def hasNext(self) -> bool:
        """If we have values left to explore in our stack then there 
        exists a value"""
        return len(self.stack) > 0 



# Your BSTIterator object will be instantiated and called as such:
# obj = BSTIterator(root)
# param_1 = obj.next()
# param_2 = obj.hasNext()