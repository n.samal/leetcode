"""
Inputs
    root (TreeNode): start of tree
Outputs
    None
Notes
    - two nodes of binary search tree are swapped
    - swap those back without changing the BST structure

    - try to do this in O(1) space

Ideas

    - crawl tree and maintain track of valid bounds
        + min bound, max_bound
        + left must be smaller, right must be larger
        + if the value is out of bounds save it

            * this doesn't necessarily tell us which nodes to swap
            ie: root = [3,1,4,null,null,2]

        + can we swap while we propogate?
            * may cause issues and invalidate bounds above


    - leverage the fact the Binary Search Tree must be in an order
        + crawl the tree in order from smallest to largest (LNR)
        + values that are out of order are invalid

        + Time:  O(n)
        + Space: O(n)

    - How to detect out of order elements?

        + compare value location against a sorted list
            O(n*log(n))

        + compare against adjacent values
            * for smaller number of values, many invalid values
            may arise for side and side comparisons

            correct  : [1, 2, 3]

            incorrect: [1, 3, 2]

            incorrect: [2, 1, 3]
            incorrect: [2, 3, 1]

            incorrect: [3, 1, 2]
            incorrect: [3, 2, 1]

            * use 'classical' approach
            https://www.techiedelight.com/sort-array-using-one-swap/

            only compare against prior value, and use assumption of
            only two incorrectly placed values

    - Constant Space solution

        + use same approach as 1D array comparison

            * track the prior value, and how many bad values
            we've seen

            * swap the values for the candidates at the end

"""


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:

    ''' External Array '''

    def recoverTree(self, root: TreeNode) -> None:
        """
        - crawl through through in order (LNR)
        - save values into external array
        - compare nodes to determine where the swap occurs

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        Notes
            Do not return anything, modify root in-place instead.
        """

        self.ans = []

        def inorder(root: TreeNode):
            """Inorder Traversal: LNR"""

            if root:
                inorder(root.left)
                self.ans.append(root)
                inorder(root.right)

        inorder(root)
        # print(self.ans)

        # Iterate across array and find the swapped values
        cand1 = None
        cand2 = None

        n: int = len(self.ans)
        prev = self.ans[0]

        for ix in range(1, n):

            if prev.val > self.ans[ix].val:

                if cand1 is None:
                    cand1 = prev
                    cand2 = self.ans[ix]
                else:
                    cand2 = self.ans[ix]

            prev = self.ans[ix]

        # Swap the two values
        cand1.val, cand2.val = cand2.val, cand1.val

    def detect_swap(self, arr: list):
        """'Classical' swap detection

        - iterate across array
        - if first occurence found, then save
        both values as out of order
        - on second occurence just update the second value

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        n: int = len(arr)

        cand1 = None
        cand2 = None

        prev = arr[0]

        for ix in range(1, n):

            if arr[ix] < prev:

                if cand1 is None:
                    cand1 = prev
                    cand2 = arr[ix]

                else:
                    cand2 = arr[ix]

            prev = arr[ix]

        return cand1, cand2

    ''' Constant space '''

    def constant_space(self, root: TreeNode) -> None:
        """
        - crawl through through in order (LNR)
        - only save prior values instead of saving the whole tree

        Time Complexity
            O(n)
        Space Complexity
            O(1)

        Notes
            Do not return anything, modify root in-place instead.
        """

        # Iterate across array and find the swapped values
        self.prev = None
        self.cand1 = None
        self.cand2 = None

        def inorder(root: TreeNode):
            """Inorder Traversal: LNR"""

            if root:
                inorder(root.left)

                if self.prev and self.prev.val > root.val:
                    if self.cand1 is None:
                        self.cand1 = self.prev
                        self.cand2 = root
                    else:
                        self.cand2 = root

                # Save for comparison
                self.prev = root

                inorder(root.right)

        inorder(root)

        # Swap the two values
        self.cand1.val, self.cand2.val = self.cand2.val, self.cand1.val


if __name__ == '__main__':

    # Detection check
    arr = [3, 5, 6, 9, 8, 7]

    obj = Solution()
    print(obj.detect_swap(arr))
