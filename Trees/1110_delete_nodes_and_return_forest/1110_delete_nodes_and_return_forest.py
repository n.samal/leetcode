# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def delNodes(self, root: TreeNode, to_delete: List[int]) -> List[TreeNode]:
        """Iterate across the forest while updating it

        - iterate across the forest and search for root nodes
        - if we delete a node, it's children will be potentially be roots
            + when we delete a node, we should return None to it's parent since
            it's removed from the forest

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        
        to_delete = set(to_delete)
        self.ans = []
        
        def recurse(root: TreeNode, is_root: bool):
            
            if root:
                                
                root_deleted = root.val in to_delete
                
                # This is a starting point, and it's valid
                if is_root and root_deleted is False:
                    self.ans.append(root)
                
                # Recurse through children
                # if we deleted this node, the next nodes are roots
                root.left = recurse(root.left, root_deleted)
                root.right = recurse(root.right, root_deleted)
                    
                # Did we remove this node?
                if root_deleted:
                    return None
                else:
                    return root
                    
            else:
                return None
            
        
        recurse(root, True)
        
        return self.ans
        