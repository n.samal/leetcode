"""
Inputs:
    root (TreeNode): root of binary search tree
Outputs:
    List[List[int]]: node values for level traversal 
Goals
    - compute bottom up level traversal
        + last level first
    - values from left to right

"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

from collections import deque

class Solution:

    def levl_order_reverse(self, root: TreeNode) -> List[List[int]]:
        """Compute level order traversal then reverse rows
        
        - use breadth first search for level traversing
        - track the current level, max level
            + if new level start new list
            + if old level add to old list
        
        - save values to our array
        - reverse the saved values at the end
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        
        # Null case
        if not root:
            return []
        
        levels: List[List[int]] = []
        max_level: int = 0
        queue = deque([(root, 1)])
        
        while queue:
            
            node, level = queue.popleft()
        
            # Save node to new level traversal
            if level > max_level:
                levels.append([node.val])
                max_level = level
            
            # Save to current level
            else:
                levels[-1].append(node.val)
        
            # Add children
            if node.left:
                queue.append((node.left, level + 1))
            if node.right:
                queue.append((node.right, level + 1))
            
        # Reverse level order to bottom up
        levels = levels[::-1]
        
            
        return levels