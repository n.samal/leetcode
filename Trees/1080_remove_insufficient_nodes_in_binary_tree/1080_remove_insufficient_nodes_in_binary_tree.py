"""
Inputs
    root (TreeNode):
    limit (int): path summation limit
Outputs
    TreeNode: new head node

Notes

Ideas
    - crawl recursively
        + total values from top down or bottom up
        + bottom up unnecessary, so just apply recursively

    + if we're at a leaf node check the limit
        + remove the node if below it

    + if both the left and right child are removed
    remove the current node as well
        + pass up the new nodes
"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def sufficientSubset(self, root: TreeNode, limit: int) -> TreeNode:

        # Compute sum through each node, from root to leaf
        # if the sum is below the limit, then remove it later

        def recurse(root, total: int = 0):
            """Compute sum and check 

            - compute the sum as we travel down

            Time Complexity
                O(n)
            Space Complexity
                O(h)
            """

            if root:

                # Add current value
                total += root.val

                # At leaf node, check limit
                if root.left is None and root.right is None:
                    if total < limit:
                        return None
                    else:
                        return root

                # Update children
                # - remove the node if both paths were removed
                else:                
                    root.left = recurse(root.left, total)
                    root.right = recurse(root.right, total)

                    if root.left is None and root.right is None:
                        return None
                    else:
                        return root
            else:
                return None

        root_new = recurse(root)

        return root_new
