"""
Inputs:
    root (TreeNode): tree node
Outputs:
    List[int]: values of nodes in pre order traversal
Goals
    - pre order traversal (Node, Left, Right)
    
Strategies
    
    Recursion
        - travel along valid nodes
        - first the current node, left node, then right node
    
    Iterative
        - use depth first search to iterate through the stack
        - grab the newest nodes first, specifically the left node first
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
"""

from collections import deque


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    
    ''' Recursive '''
    
    def recursive(self, root: TreeNode) -> List[int]:
        """
        
        Time Complexity
            O(n)
        Space Complexity
            O(n) saving all node values
        """
    
        # Initialize list
        self.path = []
        
        # Crawl from root
        self.recurse(root)
        
        return self.path
    
    def recurse(self, node: TreeNode):
        """Recurse the path in pre order
        
        - current node, left then right (NLR)
        """
        
        if node:
            self.path.append(node.val)
        
            # Left then right
            self.recurse(node.left)
            self.recurse(node.right)
    
    ''' Iterative '''

    def iterative(self, root: TreeNode):
        """Preorder traversal to determine values explored
        
        - Preorder: Node, Left, Right
        - Depth first search
            + we want to explore left nodes first
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        
        path = []
        
        # Null case
        if not root:
            return path
        
        stack = deque([root])
        
        while stack:
            
            # Remove newest node and save it's value
            node = stack.pop()
            path.append(node.val)
            
            # Add children
            # Add left last since we want this explored first
            if node.right:
                stack.append(node.right)
            if node.left:
                stack.append(node.left)
            
        return path
            
