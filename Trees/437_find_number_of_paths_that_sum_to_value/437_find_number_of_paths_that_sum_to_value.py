"""
Inputs
    root (TreeNode): 
Outputs
    int: find number of paths that equal sum
Notes
    - path does not need to start a root
    - path does nto need to end at a leaf
    - path must travel downwards 
    - the tree is binary, only left or right children

Ideas
    - gather all nodes
        - try starting a path from each node
        - add the node value to each current path sum
        - check if the sum meets the target

    - track prefix summation at each node
        - search for a complimenting pair
        - same principle as finding subarrays that equal target
"""

# recurse from each node in tree
# continue path, and start new path



# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

from collections import defaultdict


class Solution:


    ''' Gather & Recurse '''

    def gather_and_recurse(self, root: TreeNode, sum: int) -> int:
        """
        Notes
            - lots of redudant crawling
        """
        
        self.count = 0
        self.nodes = []
        self.gather_nodes(root)        
        
        for node in self.nodes:
            self.recurse(node, 0, sum)
        
        return self.count

    def gather_nodes(self, root: TreeNode):
        
        if root:
            
            self.nodes.append(root)
            self.gather_nodes(root.left)
            self.gather_nodes(root.right)
    
    def recurse(self, root, path_sum, target: int):
        
        if root:
            
            path_sum += root.val
            
            if path_sum == target:
                self.count += 1
        
            if root.left:
                self.recurse(root.left, path_sum, target)

            if root.right:
                self.recurse(root.right, path_sum, target)

    '''  Prefix Sum '''
    
    def prefixes(self, root: TreeNode, target: int) -> int:
        
        self.count = 0
        self.dct = defaultdict(int)  # track count of each summation
        
        self.crawl(root, 0, target)
        
        return self.count

    def crawl(self, root, path_sum, target: int):
        """

        - compute the prefix sum as we travel the tree
        - count the number of subarrays that meet our target
        - remove our path_sum from the history so we don't
        track sums from nodes that wouldn't affect us
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        
        if root:
            
            path_sum += root.val

            # Our sum matches
            if path_sum == target:
                self.count += 1

            # Add count of subarrays meeting target
            comp = path_sum - target
            self.count += self.dct[comp]

            # Add current summation to counter
            self.dct[path_sum] += 1

            self.crawl(root.left, path_sum, target)
            self.crawl(root.right, path_sum, target)

            # Remove node from other subtrees
            #    a
            #   /  \
            #  b    c
            # ie: the a + b count shouldn't be included in paths on the right
            self.dct[path_sum] -= 1
