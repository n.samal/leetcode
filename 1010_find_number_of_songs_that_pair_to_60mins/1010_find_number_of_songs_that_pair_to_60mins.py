"""
Inputs
    arr (List[int]): numbers in unknown order
Outputs
    int: number of song pairs that add up to an hour
Notes
    - Formally, we want the number of indices i, j such that i < j with (time[i] + time[j]) % 60 == 0.

Examples

    Example 1:

        Input: [30,20,150,100,40]
        Output: 3
        Explanation: Three pairs have a total duration divisible by 60:
        (time[0] = 30, time[2] = 150): total duration 180
        (time[1] = 20, time[3] = 100): total duration 120
        (time[1] = 20, time[4] = 40): total duration 60

    Example 2:

        Input: [60,60,60]
        Output: 3
        Explanation: All three pairs have a total duration of 120, which is divisible by 60.

Ideas

    - brute force will take too long
        + store the modulus for each value
        and combine those remainders together

        10 + 50
        20 + 40
        30 + 30

    - when counting pairs we need to ensure we don't
    count values paired with themselves

        + normal pairs
        
            n_pairs = n_values * n_other values
        
        + pairing with itself
            
            n_pairs = (n_values * (n_values - 1)) // 2

            we must prevent duplicate counts

"""

from collections import defaultdict

class Solution:
    def hash_count(self, times: List[int]) -> int:
        """Combine the number of pairs

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        # Store mods
        dct = defaultdict(int)

        for time in times:
            dct[time % 60] += 1

        # Count pairs
        count: int = 0

        used = set([])

        for key in dct.keys():

            comp = (60 - key) % 60

            if key in used or comp in used:
                continue

            # print(f'key: {key}   comp: {comp}')
            # print(f' count: {dct[key]}   comp: {dct[comp]}')

            # Pair with every other but not self
            # and don't double count
            if comp == key:
                n_pairs = (dct[key] * (dct[key] - 1)) // 2

            # Pair with eachother
            elif comp in dct:
                n_pairs = dct[key] * dct[comp]

            else:
                n_pairs = 0

            count += n_pairs

            used.add(key)
            used.add(comp)

        return count

    def array_count(self, times: List[int]) -> int:
        """Combine the number of pairs

        - alternative approach, is we count values as we go
            + this prevents counting pairs with itself

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        # Store remainders
        remainders = [0 for _ in range(60)]
        count: int = 0

        for time in times:

            time = time % 60
            comp = (60 - time) % 60

            # Create pairs with compliments
            # ie: this 10 with all other 50s
            #     this 30 with all other 30s
            count += remainders[comp]

            # Add remainder count
            remainders[time] += 1

        return count
