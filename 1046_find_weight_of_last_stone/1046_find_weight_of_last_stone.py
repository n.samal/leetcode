"""
Inputs
    stones (List[int]): weight of each stone
Outputs
    int: weight of remaining stone
Goal
    - weight of the remaining stone after
    
Notes
    - each turn we choose the two heaviest stones and smash them together
    - if x == y then both stones are destroyed
    - ix x != y, the smaller stone is totally destryoed
        the remaining stone has weight y - x
    
Ideas
    - sort the values from smallest to largest
    - smash the two largest stones and smash them
        + if stones are equal, then pop both
        + if stones are unequal
            
            * remove the heavier stone
            * replace smaller stone with the difference
Notes
    
    - once we find the remaining stone, we must put it back into the correctly
    sorted position!
    
    - we could use a heap to minimize run time further rather than using insertion sort
"""

import bisect


class Solution:
    def lastStoneWeight(self, stones: List[int]) -> int:
        """
        
        Time Complexity
            O(n*log(n))
            
            insort can be expensive if moving n item each time leading to O(n^2)
            
        Space Complexity
            O(1)
        """
    
        n: int = len(stones)
        
        # Null or single stone
        if n == 0:
            return 0
        elif n == 1:
            return stones[0]
        
        # Sort from smallest to largest
        stones = sorted(stones)
        
        # We've got at least two stones
        while len(stones) >= 2:
        
            # print(f'stones: {stones}')
        
            # Look at their values
            y = stones.pop()
            x = stones.pop()
            
            delta = y - x
            
            # print(f'y: {y}    x: {x}    delta: {delta}')
            
            # Stone weights are not equal
            # Place the remaining stone back into sorted position
            if delta != 0:
                bisect.insort(stones, delta)
        
        # print(f'stones: {stones}')
        
        # Return the value of the remaining stone if any
        if not stones:
            return 0
        else:
            return stones[0]
        
