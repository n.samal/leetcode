"""
Inputs
    arr (List[int]): integers in unknown order
Outputs
    int: minimum sum of the lengths of the subarrays
Goals
    - find two non overlapping subarrays equal to the target
    - find the subarrays that don't overlap and have the smallest combined
    length
    - output -1 if we cannot find two subarrays that meet target summation

Notes
    - all array values are greater than 0
    - all arrays are greater than 1
    - all targets are greater tahan 0

Examples
    Example 1

        Input: arr = [3,2,2,4,3], target = 3
        Output: 2
        Explanation: Only two sub-arrays have sum = 3 ([3] and [3]). The sum of their lengths is 2.

    Example 2

        Input: arr = [7,3,4,7], target = 7
        Output: 2
        Explanation: Although we have three non-overlapping sub-arrays of sum = 7 ([7], [3,4] and [7]), but we will choose the first and third sub-arrays as the sum of their lengths is 2.

    Example 3
        Input: arr = [4,3,2,6,2,3,4], target = 6
        Output: -1
        Explanation: We have only one sub-array of sum = 6.

    Example 4
        Input: arr = [5,5,4,4,5], target = 3
        Output: -1
        Explanation: We cannot find a sub-array of sum = 3.

    Example 5
        Input: arr = [3,1,1,1,5,1,2,1], target = 3
        Output: 3
        Explanation: Note that sub-arrays [1,2] and [2,1] cannot be an answer because they overlap.

Ideas

    - compute cummulative sum for easy computation of subarray
    summations
        + while creating the summation, search for it's compliment
        to determine if we have a subarray equal to the target

        + since we have no negative values or zero values, the cumsum
        is only increasing. There won't be any repeat values!

    - track the indices for each subarray
        + indices = (lower bound, upper bound)
        + when searching for overlap we compare the current lower bound
        vs the other window upper bound

        + multiple windows can end at the same upper bound

            * we want the smallest subarray possible so just save the smallest
            * ie: (0, 4) vs (3, 4)

Key Idea

    - compute the cummulative sum in first iteration
        + use summation 0 located at index -1
        + summations calculated (exclusive, inclusive)
        + save summations at each index in set/hash

    - use second iteration to search forwards and backwards
        + find the complimenting pair for our subarray

References:
    https://leetcode.com/problems/find-two-non-overlapping-sub-arrays-each-with-target-sum/discuss/685486/JAVA-O(N)-Time-Two-Pass-Solution
    https://leetcode.com/problems/find-two-non-overlapping-sub-arrays-each-with-target-sum/discuss/687456/Java-2Sum-on-Prefix-Sum
"""

from typing import List


class Solution:

    def minSumOfLengths(self, arr: List[int], target: int) -> int:
        """

        - compute the cummulative summation
        - find all subarrays that match target summation
        - find 2 subarrays that don't overlap

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        Notes
            - too slow
        """
        n: int = len(arr)

        # Null array summation
        if target == 0:
            return 0

        ''' Find subarrays that meet target criteria '''
        cumsum = 0
        table = {0: -1}  # save indices of each summation
        works = []       # save lower and upper bounds of valid subarrays (inclusive, inclusive)

        for ix in range(n):

            # Compute summation and save index
            cumsum += arr[ix]
            table[cumsum] = ix

            # target = upper - lower
            compliment = cumsum - target

            if compliment in table:
                ix_low = table[compliment]
                works.append((ix_low + 1, ix))

            # Value is exactly what we want
            # elif arr[ix] == target:
                # works.append((ix, ix))

        print(f'table: {table}')
        print(f'works: {works}')

        ''' Find non-overlapping subarrays '''

        n: int = len(works)

        # Unable to find two possible subarrays
        if n < 2:
            return -1

        # Note that 'works' will inherently be in order lowest index to largest index
        # We can search for indices larger than the upper bound
        # Binary search should work, lets' try naive for now

        ans = float('inf')

        for ix in range(n):

            length1 = works[ix][1] - works[ix][0] + 1

            for jx in range(ix + 1, n):

                # If no overlap between subarray bounds
                if works[jx][0] > works[ix][1]:

                    length2 = works[jx][1] - works[jx][0] + 1
                    length = length1 + length2

                    ans = min(length, ans)

        if ans == float('inf'):
            return -1
        else:
            return ans

    def two_pass(self, arr: List[int], target: int) -> int:
        """Look foward & backwards for target

        - compute the cummulative summation
            + lower bound index is exclusive
            + upper bound index is inclusive
        - iterate again
            + look backwards to find subarrays that match target
            + also look forward to find subarrays that match target

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        Notes
            - speedy
        """
        n: int = len(arr)

        # Null array summation
        if target == 0:
            return 0

        ''' Compute cummulative sum '''
        cumsum = 0
        table = {0: -1}  # save indices of each summation

        for ix in range(n):

            cumsum += arr[ix]
            table[cumsum] = ix

        ''' Find subarrays that meet target criteria '''
        cumsum = 0                # current cummulative summation
        best_left = float('inf')  # length of shortest subarray to the left
        best_over = float('inf')  # length of shortest subarrays (left + right)

        for ix in range(n):

            # Compute summation and save index
            cumsum += arr[ix]

            # target = upper - lower
            compliment = cumsum - target

            # Search backwards for subarray that meets criteria
            # subarrays are ending here
            if compliment in table:
                ix_low = table[compliment]
                len_cur = ix - ix_low

                print(f' back: ({ix_low}, {ix})  length: {len_cur}')

                best_left = min(len_cur, best_left)

            # Search forward for subarrays that meet criteria
            # subarrays are starting here
            compliment = cumsum + target

            if compliment in table:
                ix_high = table[compliment]
                len_cur = ix_high - ix

                print(f' fwd: ({ix}, {ix_high})  length: {len_cur}')

                best_over = min(len_cur + best_left, best_over)

        if best_over == float('inf'):
            return -1
        else:
            return best_over


if __name__ == '__main__':

    # arr_in = [7, 3, 4, 7]
    # target = 7

    # Failed case
    arr_in = [2, 2, 4, 4, 4, 4, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    target = 20

    obj = Solution()
    # print(obj.minSumOfLengths(arr_in, target))
    print(obj.two_pass(arr_in, target))
    print(obj.two_pass2(arr_in, target))
