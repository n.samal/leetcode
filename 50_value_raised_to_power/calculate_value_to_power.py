def power(value: float, power: int) -> float:
    """Raise a value to the input power

    Args:
        value (float): value to raise
        power (int): power for raise
    Returns:
        float: value raised to power
    Notes:
        - time limit exceeded for large powers
    """

    if power == 0:
        return 1
    elif power == 1:
        return value

    # Init value
    out = 1
    power_left = abs(power)

    # Compute value to power
    while power_left > 0:

        # print('power_left:', power_left, 'out:', out)

        out *= value

        # Decrement power left
        power_left -= 1

    # We had a negative power
    # flip the value
    if power < 0:
        return 1 / out
    else:
        return out


def power2(value: float, power: int) -> float:
    """Raise a value to the input power

    Args:
        value (float): value to raise
        power (int): power for raise
    Returns:
        power: value raised to power
    """

    if power == 0:
        return 1
    elif power == 1:
        return value

    # We had a negative power flip the value
    if power < 0:
        return 1 / power_recursion(value, abs(power))
    else:
        return power_recursion(value, power)


def power_recursion(value: float, power: int):
    """Use recursion to speed up process

    Use match concepts to apply this function recursively
        x^n = (x^2)^n/2          if n is even.
        x^n = x(x^2)^(n - 1)/2   if n is odd.

    Args:
        value (float): value to raise
        power (int): power for raise
    Returns:
        power: value raised to power
    """

    # Base cases
    if power == 0:
        return 1
    elif power == 1:
        return value

    ''' Normal cases '''
    # Even
    if power % 2 == 0:
        return power_recursion(value * value, power / 2)

    # Odd
    else:
        return value * power_recursion(value * value, (power - 1) / 2)


if __name__ == "__main__":

    # Normal power
    # print(power(2, 10))
    # print(power2(2, 10))

    # print(power(2.1, 3))
    # print(power2(2.1, 3))

    # Negative power
    # print(power(2.0, -2))
    # print(power2(2.0, -2))

    # Negative integer
