"""
Inputs
    s (str): input string
Outputs
    int: length of the longest substring
Cases




Strategy
    Sliding Window

        - begin a window at the start of the string
        - start adding characters to a hash or set
        - keep adding characters to the right while we have less than 2 unique chars
        - once we have more than 2, we start closing our window

            - remove the last char, one at a time
            - once there is no more of this character, remove it from our hash

    Sliding Window with jumps

        - we don't need to reduce our window size one by one
        - in many cases we can jump several characters at once

        ie: leeeleeet

        ie: |leeeleeet|
        - once we hit 't' we must reduce the window size
        - instead of closing we go to one past the left most character ie: 'l'

        ie: leeel|eeet|

        - keep in mind this is NOT the index of the last time we saw the tail

        ie: bccbbdd
           |bccbb|dd

           at this location, our tail is at the first 'b'
           - we need to move to 'c' instead of the last time we saw 'b'
           
           |bccbb|dd
           bcc|bb|dd
        

"""


class Solution:

    def lengthOfLongestSubstringTwoDistinct(self, s: str) -> int:
        """Moving tail one by one

        Time Complexity
            O(n)
        Space Complexity
            O(1)
         """
        # Pointers
        head_ix: int = 0
        tail_ix: int = 0
        n: int = len(s)

        # Max
        max_len: int = 0
        d = {}

        # We've only got two characters
        if n <= 2:
            return n

        # Characters are left to explore
        while head_ix < n:

            print(f'{s[tail_ix:head_ix]:<10} len: {len(d):<5} d: {d} ')

            # Less than 2 unique characters
            if len(d) <= 2:

                # Add the character to the dictionary
                char = s[head_ix]

                # Update character counts
                if char in d:
                    d[char] += 1
                else:
                    d[char] = 1

                # Move head up
                head_ix += 1

            # We've got more than 2 characters, start removing from tail
            while len(d) > 2 and tail_ix <= head_ix:

                # Grab the last char
                char = s[tail_ix]

                # Remove character
                d[char] -= 1

                # Remove the key if no counts of that character
                if d[char] <= 0:
                    del d[char]

                # Move the tail up
                tail_ix += 1

            # Update our max, only once we guarantee 2 chars or less
            max_len = max(max_len, head_ix - tail_ix)

        return max_len

    def lengthOfLongestSubstringTwoDistinct2(self, s: str) -> int:
        """Track the last index of when we saw each character

        Time Complexity
            O(n)
        Space Complexity
            O(1)
         """
        # Pointers
        head_ix: int = 0
        tail_ix: int = 0
        n: int = len(s)

        # Max
        max_len: int = 0
        d = {}

        # We've only got two characters
        if n <= 2:
            return n

        # Characters are left to explore
        while head_ix < n:

            print(f'{s[tail_ix:head_ix]:<10} len: {len(d):<5} d: {d} ')

            # Less than 2 unique characters
            if len(d) <= 2:

                # Track the last time we see this character
                d[s[head_ix]] = head_ix

                # Move head up
                head_ix += 1

            # We've got more than 2 characters, start removing from tail
            # We use an if to catch the next character, especially in the
            # case of the last character in the string
            if len(d) > 2:

                # Find out the left most character in our window
                min_ix = min(d.values())
                min_ch = s[min_ix]

                # Remove that character from our hash
                del d[min_ch]

                # Move the tail up, one past the last char
                tail_ix = min_ix + 1

            # Update our max, only once we guarantee 2 chars
            max_len = max(max_len, head_ix - tail_ix)

        return max_len


if __name__ == "__main__":

    # Example: basic
    # s_in = 'eceba'

    # Example 2
    # s_in = 'ccaabbb'

    s_in = 'aac'
    # s_in = 'aacb'

    obj = Solution()
    # print(obj.lengthOfLongestSubstringTwoDistinct(s_in))
    print(obj.lengthOfLongestSubstringTwoDistinct2(s_in))
