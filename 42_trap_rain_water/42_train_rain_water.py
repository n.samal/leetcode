from typing import List


class Solution:

    def trap(self, arr: List[int]) -> int:
        """Determine the amount of rain water trapped

        Args:
            arr (List[int]): height of bins
        Returns:
            int: amount of water
        TODO
            - method is slightly off, but very close
        """

        total: int = 0  # total water count
        n: int = len(arr)

        # Not enough space to capture water
        if n <= 2:
            return 0

        # Initialize indices
        head_ix: int = 0
        tail_ix: int = 1

        head_val = arr[head_ix]

        while head_ix < n - 1:

            blocked: int = 0  # units blocked 

            # No height to trap water
            if head_val == 0:

                # Try next location
                head_ix += 1
                tail_ix = head_ix + 1
                head_val = arr[head_ix]

                continue

            # Crawl to the right until we hit something
            # of equivalent height or taller
            while tail_ix < n and arr[tail_ix] < head_val:
                
                # Track blocks smaller than desired water level
                if arr[tail_ix] < head_val:
                    blocked += arr[tail_ix]

                tail_ix += 1

            # We found something taller or equal
            if tail_ix < n and arr[tail_ix] >= head_val:

                # Calculate area
                # height * width
                total += head_val * (tail_ix - head_ix - 1)

                # Subtract out unused water
                total -= blocked

                # Shift to next position
                head_ix += 1
                head_val = arr[head_ix]
                tail_ix = head_ix + 1

            # Try again with a smaller height
            else:
                head_val -= 1
                tail_ix = head_ix + 1

        return total

    def left_right_max(self, arr: List[int]) -> int:
        """Compute the max moving from each direction

        Step 1
            - maximum height at each index moving left to right
        Step 2
            - maximum height at each index moving right to left
        Step 3
            - find the overlap portion (minimum of the two)
            - the overlap determines the water height
            - water volume = water height - bin height

        Args:
            arr (List[int]):
        Returns:
            int: amount of water
        """

        total: int = 0  # total water count
        n: int = len(arr)

        # No bins to store water
        if n <= 1:
            return 0

        ''' Max Values at Indices '''

        # Maximum values moving from each direction
        max_left = [arr[0]] * n  # moving left to right
        max_right = [arr[-1]] * n  # moving right to left

        # Left to Right: track max height seen so far
        for ix in range(1, n):
            max_left[ix] = max(max_left[ix - 1], arr[ix])

        # Left to Right: track max height seen so far
        for ix in range(n - 2, -1, -1):
            max_right[ix] = max(max_right[ix + 1], arr[ix])

        ''' Overlap '''

        # Water in bin, is the minimum of our maxes

        # Compute overlap at each index
        for ix in range(n):

            height_water = min(max_left[ix], max_right[ix])

            # Water volume = water height - bin height
            total += height_water - arr[ix]

        return total

    def two_pointers(self, arr: List[int]) -> int:
        """One pass version of left right max method

        TODO
            - complete method

        """
        # Init basics
        n: int = len(arr)
        total: int = 0

        # Define pointers
        left: int = 0
        right: int = n - 1

        while left < right:

            # Left is smaller than right
            if arr[left] < arr[right]:
                pass
            # Right is smaller than left
            else:
                pass 

        return total


if __name__ == '__main__':

    s = Solution()

    # Not large enough
    # case = [4, 4]

    # All zeros
    # case = [0, 0, 0, 0, 0]

    # Nothing to the right
    # case = [0, 0, 2, 0, 0]

    # Smaller to right
    # case = [0, 4, 0, 2, 0]

    # Taller to right
    # case = [0, 4, 2, 6, 0]

    # Test case
    case = [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]
    print(s.trap(case))
    print(s.left_right_max(case))
