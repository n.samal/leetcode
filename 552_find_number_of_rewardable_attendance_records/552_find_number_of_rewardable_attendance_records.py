"""
Inputs
    n (int): desired record length
Outputs
    int: the number of rewardable attendance records of length n
Goals
    - find the number of rewardable attendance records
    - if absent 'A' more than once, that is unrewardable
    - if more than 2 continuous 'L' lates, then that is unrewardable

    - 3 choices for attendance record 'A' absent, 'L' late, 'P' present

Examples

    Example 1
        Input: n = 2
        Output: 8 
        Explanation:
        There are 8 records with length 2 will be regarded as rewardable:
        "PP" , "AP", "PA", "LP", "PL", "AL", "LA", "LL"
        Only "AA" won't be regarded as rewardable owing to more than one absent times. 

Ideas

    - track these parameters
        + the number of absents
        + the current attendance record

    - we also need to check the last three appearances
        + if the last 3 appearances are 'L' late then this not rewardable
        + we may not need to save the entire record history, and just save
        the last three

        ie: 'LLL'
            'ALLL'

    - recursively crawl from the top down and generate valid paths
        + alternatively create a queue with paths

        + try tracking just the last 3 records
        + track the absent count

    - instead of tracking the strings, we could represent each path
    using values
        + A = -1
        + L = 0
        + P = 1

        + nvm, we need a queue to understand which items are leaving

        ie: [A, L, P, P]

        we need to know that A is leaving
        and then add P
Strategies

    Recursion/Queue

        - generate a queue, and build upon it
        - add all potential choices to the current path
        - remove invalid paths

        Time Complexity
            O(3^n)
        Space Complexity
            O(3n^n)

    Dynamic Programming

        - review notes

        Time Complexity
            O(n)
        Space Complexity
            O(n)

References:
    https://leetcode.com/problems/student-attendance-record-ii/discuss/101643/Share-my-O(n)-C%2B%2B-DP-solution
    https://leetcode.com/problems/student-attendance-record-ii/discuss/431896/Java-solution-with-intuitive-explanation-(easy-to-understand)
    https://leetcode.com/problems/student-attendance-record-ii/discuss/101650/552.-Student-Attendance-Record-II-C%2B%2B_with-explanation_from-DFS-to-DP

"""

class Solution:
    def checkRecord(self, n: int) -> int:
        """Generate all records using a queue

        Args:
            n (int):
        Notes:
            - too slow
            - we don't need to save all strings, but

        Time Complexity
            O(3^n)  3 options at each node

        Space Complexity
            O(3n^n)  - if almost all 3^n paths are valid they are in the queue
                     - each string is potentially of length n
        """

        count: int = 0
        options = ['A', 'L', 'P']

        queue = [('', 0)]  # attendance record, absent count

        while queue:

            record, n_absent = queue.pop()

            n_records = len(record)

            ''' Unrewardable paths'''

            if n_absent > 1:
                continue

            if n_records >= 3 and record[-3:] == 'LLL':
                continue

            ''' Valid paths '''

            if n_records < n:
                for option in options:
                    if option == 'A':
                        queue.append((record + option, n_absent + 1))
                    else:
                        queue.append((record + option, n_absent))

            ''' Complete paths'''

            if len(record) == n:
                count += 1

        return count
