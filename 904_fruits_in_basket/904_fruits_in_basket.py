"""

Inputs
    arr (List[ints]): array of unsorted values

Outputs
    int: number of good starting indices

Goal:
    - each number in the array represents a type of fruit

    Procedure
    - we can collect one fruit at each tree, if not stop
    - Move to the next tree to the right of the current tree.
      If there is no tree to the right, stop.  

    - find the maximum amount of fruit you can collect with this procedure

Examples

    Example 1:

        Input: [1,2,1]
        Output: 3
        Explanation: We can collect [1,2,1].
    
    Example 2:

        Input: [0,1,2,2]
        Output: 3
        Explanation: We can collect [1,2,2].
        If we started at the first tree, we would only collect [0, 1].
    
    Example 3:

        Input: [1,2,3,2,2]
        Output: 4
        Explanation: We can collect [2,3,2,2].
        If we started at the first tree, we would only collect [1, 2].
    
    Example 4:

        Input: [3,3,3,1,2,1,1,2,3,3,4]
        Output: 5
        Explanation: We can collect [1,2,1,1,2].
        If we started at the first tree or the eighth tree, we would only collect 4 fruits.


Case
    
    Simple
        
        [0,0,0,0,1,1,1,2]

        one we hit 2, we want to start where 1s start,
        [0,0,0,0|1,1,1,2]
        this "could" be one index past the last time we saw the left index val ie: 0
        *that is an incorrect assumption, see the catch below*

    Catch
        [0,0,0,0,1,1,0,2]

        once we hit 2, we want to start where the last 0 is
        [0,0,0,0,1,1|0,2]

        this is not the last time we saw the left index but
        one past the last time we saw the left most value

        the left most value = 1


Strategy
    - Brute Force

        + at each index start a basket ie: set
        + add the current value to the set and increment
        + keep adding to the basket, and moving to the right until we hit a 3rd type of fruit

            * stop the slide here and move to the next position in the array
            * if we've reached the end of the array, no need to reset, exit the loop

    - Sliding window with intelligence

        + problem seems similar to 159_longest_substring_with_2_distinct_chars
        + start at the first index and move right
        + add fruits to our basket

            - track the last time we saw each type of fruit
        + if we ever have 3 fruits in our basket

            - we've got to kick out 1 of the fruits
            - we'll go to the left most fruit, and move once past it

        + use the length difference 

    - Multi Basket

        + create multiple baskets
        + add the current basket while we have 2 types of fruits

            * when we hit a new type of fruit, try starting a new basket with it



"""

from typing import List


class Solution:
    def totalFruit(self, tree: List[int]) -> int:
        """Brute force approach

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        """
        max_fruits: int = 0
        n: int = len(tree)

        for ix in range(n):

            # Create basket starting here
            s = set([])
            fruits = 0

            ''' Start sliding to the next tree '''
            jx = ix

            while jx < n and len(s) <= 2:

                # Only consider a maximum, when we gaurantee only 2 fruits
                max_fruits = max(max_fruits, fruits)

                # Add fruit to the basket
                s.add(tree[jx])
                fruits += 1

                # Move to the next tree
                jx += 1

            # Add remaining check
            if len(s) <= 2:
                max_fruits = max(max_fruits, fruits)

            # We've reached the end
            if jx == n:
                break

        return max_fruits

    def totalFruit2(self, tree: List[int]) -> int:
        """Hash tracking approach

        - keep track of where we last saw each fruit
        - if we have more than 2 fruits
            + remove the fruit located most to the left
        - if less then or equal to 2
            + update our max fruit count

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """
        n: int = len(tree)  # number of trees
        max_fruits: int = 0  # max number of fruits gathered
        left_ix: int = 0  # left pointer

        d: dict = {}

        for right_ix in range(n):

            # Add the current fruit to the basket
            d[tree[right_ix]] = right_ix

            # Do we have less than two fruits?
            if len(d) <= 2:
                count = (right_ix - left_ix) + 1
                max_fruits = max(max_fruits, count)

            # We've got too many fruits
            else:

                # Find the left most fruit
                ix_left_most = min(d.values())

                # Remove that fruit from our basket
                del d[tree[ix_left_most]]

                # Move our left index
                left_ix = ix_left_most + 1

        return max_fruits


if __name__ == '__main__':

    # Example 1, 3 fruits
    # arr_in = [1, 2, 1]

    # Example 4, 4 fruits
    arr_in = [3, 3, 3, 1, 2, 1, 1, 2, 3, 3, 4]

    obj = Solution()
    print(obj.totalFruit(arr_in))
    print(obj.totalFruit2(arr_in))
