"""
Inputs
    arr (List[int]):
    k (int): target pair sum
Outputs
    int: number of pairs that can be made
Examples

    Example 1:

        Input: nums = [1,2,3,4], k = 5
        Output: 2
        Explanation: Starting with nums = [1,2,3,4]:
        - Remove numbers 1 and 4, then nums = [2,3]
        - Remove numbers 2 and 3, then nums = []
        There are no more pairs that sum up to 5, hence a total of 2 operations.

    Example 2:

        Input: nums = [3,1,3,4,3], k = 6
        Output: 1
        Explanation: Starting with nums = [3,1,3,4,3]:
        - Remove the first two 3's, then nums = [1,4,3]
        There are no more pairs that sum up to 6, hence a total of 1 operation.

Ideas

    - maintain a counter of values
        + try to pair values with it's compliment
        + write down all pairs

    - if computing pairs after the fact, we must account for pairs
    of the same value
        + ie: [3, 3, 3] only makes one pair

"""

class Solution:
    def count_then_pair(self, arr: List[int], k: int) -> int:
        """Track counts of each value then pair them to sum to k

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        count: int = 0

        # Count occurences
        dct = {}
        for v in arr:
            dct[v] = dct.get(v, 0) + 1

        # Find pairs
        #   key + comp = k
        for key in dct:

            comp = k - key

            if comp in dct:

                if key != comp:
                    count_here = min(dct[key], dct[comp])
                else:
                    count_here = dct[key] // 2

                count += count_here

                # Remove values
                dct[key] -= count_here
                dct[comp] -= count_here

        return count

    def count_on_the_fly(self, arr: List[int], k: int) -> int:
        """Track counts of pairs while pairing them to sum to k

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        count: int = 0
        
        # Count occurences
        dct = {}
        for v in arr:
            
            comp = k - v
            
            if comp in dct and dct[comp] > 0:
                count += 1
                dct[comp] -= 1
            else:
                dct[v] = dct.get(v, 0) + 1
        
        return count