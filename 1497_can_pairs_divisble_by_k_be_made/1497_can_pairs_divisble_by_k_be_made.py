"""
Inputs:
    arr (List[int]): integers in unknown order
Outputs:
    bool: can the entire array be made into pair sums divisible by k

Notes
    - values are not distinct

Examples

    Example 1:

        Input: arr = [1,2,3,4,5,10,6,7,8,9], k = 5
        Output: true
        Explanation: Pairs are (1,9),(2,8),(3,7),(4,6) and (5,10).
    
    Example 2:

        Input: arr = [1,2,3,4,5,6], k = 7
        Output: true
        Explanation: Pairs are (1,6),(2,5) and(3,4).
    
    Example 3:

        Input: arr = [1,2,3,4,5,6], k = 10
        Output: false
        Explanation: You can try all possible pairs to see that there is no way to divide arr into 3 pairs each with sum divisible by 10.
    
    Example 4:

        Input: arr = [-10,10], k = 2
        Output: true
    
    Example 5:

        Input: arr = [-1,1,-2,2,-3,3,-4,4], k = 3
        Output: true

Ideas

    - sort values
        + then combine pairs??
    
    - use the modulus of each value to easily combine pairs
        + use a counter to track the number of each mod, 
        then pair them up

        + ie: if we have mod = val % k
            we need the same number of complimenting values

            compliment = k - mod

"""

class Solution:
    def canArrange(self, arr: List[int], k: int) -> bool:
        """Pair values by their modulus

        Time Complexity
            O(n)
        Space Complexity
            O(k)
        """
        
        n: int = len(arr)
            
        # Get the modulus of each value
        mods = {ix: 0 for ix in range(k)}
        
        for val in arr:
            mods[val % k] += 1
        
        # Need even number of pairs for 0 mod
        if mods[0] % 2 != 0:
            return False
        
        # We need an equal number of complimenting pairs for
        # each value
        # ie: if k = 5, same number of 2 & 3s, then 1 & 4s
        for ix in range(1, k):
            
            comp = (k - mod)

            # print(f'mod: {mod}   comp: {comp}')
            # print(f'mods: {mods}')

            if mods[comp] != mods[mod]:
                return False                
    
        return True
