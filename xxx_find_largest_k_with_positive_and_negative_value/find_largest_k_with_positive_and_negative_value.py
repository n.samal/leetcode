"""
Inputs
	arr (List[int]): array of integers in unknown order
Outputs
	- find largest positive integer that has both positve and negative existence in the array
	ie: returns the largest integer K > 0 such that both values K and -K exist in array A

	- If there is no such integer, the function should return 0 
Goals
	Write a function that, given an array A of N integers, 

Examples
	
	Example 1:

		Input: [3, 2, -2, 5, -3]
		Output: 3

	Example 2:

		Input: [1, 2, 3, -4]
		Output: 0

Ideas
	- Use a set to track values 
		+ check for the existence of the compliment
	- Sort the array, then use pointers
		+ keep pointers at the ends and bump left and right

References:
	- https://leetcode.com/discuss/interview-question/406031/


"""

