"""
Inputs
    arr (List[int]): integers in unknown order
    k (int): target difference
Outputs:
    number of unique pairs with a difference of k
Notes
    - pairs must be unique

    - i != j
    - a <= b
    - b - a = k

Examples

    Example 1:

        Input: nums = [3,1,4,1,5], k = 2
        Output: 2
        Explanation: There are two 2-diff pairs in the array, (1, 3) and (3, 5).
        Although we have two 1s in the input, we should only return the number of unique pairs.

    Example 2:

        Input: nums = [1,2,3,4,5], k = 1
        Output: 4
        Explanation: There are four 1-diff pairs in the array, (1, 2), (2, 3), (3, 4) and (4, 5).
    Example 3:

        Input: nums = [1,3,1,5,4], k = 0
        Output: 1
        Explanation: There is one 0-diff pair in the array, (1, 1).
    Example 4:

        Input: nums = [1,2,4,4,3,3,0,9,2,3], k = 3
        Output: 2

Ideas

    - looks like a twist on 2sum

    - brute force
        + works but is slow

    - two pointers
        + sort array
        + use left and right pointers to find target value
            * if delta is too small
            * if delta is right, add to count
            * if delta is too larger, move to smaller numbers

        + left and right pointers at opposite ends doesn't work
        in all case ie: left = 0, right = n - 1

            arr = [1, 2, 3, 4, 5], k = 1

            within current method, we would only find one pair

        + use pointers at one end, and caterpillar our way over

            * left = 0, right = 1
            * move right until we meet target delta
            *
    - hash
        + track values we've seen and search for the compliment

"""

from collections import defaultdict
from typing import List


class Solution:

    def brute_force(self, arr: List[int], k: int) -> int:
        """Brute force

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        """

        arr = sorted(arr)
        n: int = len(arr)

        s = set([])

        for ix in range(n - 1):
            for jx in range(ix + 1, n):

                if arr[jx] - arr[ix] == k:
                    s.add((arr[ix], arr[jx]))

        return len(s)

    def hash_and_find(self, arr: List[int], k: int) -> int:
        """Use a hash to find complimenting values

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        dct = defaultdict(int)
        count: int = 0

        # Count occurences
        for v in arr:
            dct[v] += 1

        # Count pairs
        for key in dct:

            comp = key - k

            # Find the complimenting pair
            if k:
                if comp in dct:
                    count += 1

            # Only one instances of the pair
            # the second time ie: [1, 1, 1, 1]
            elif dct[key] > 1:
                count += 1

        return count

    def two_pointers(self, arr: List[int], k: int) -> int:
        """Use two pointers

        - slide right to increase the delta
        - slide left to the reduce the delta
        - if we get a match
            + add it, then move the pointers

        Time Complexity
            O(n*log(n))
        Space Complexity
            O(1)
        """

        count: int = 0
        left: int = 0
        right: int = 1
        n: int = len(arr)

        arr = sorted(arr)

        while left < n and right < n:

            delta = arr[right] - arr[left]

            # Bring pointers back to normal
            if left >= right:
                right += 1
                continue

            elif delta < k:
                right += 1

            elif delta > k:
                left += 1

            # Found a pair, move to the next distinct number
            # ie: [1, 1, 1, 2, 3, 4, 5], k = 1
            else:
                count += 1
                left += 1

                while left < n and arr[left] == arr[left - 1]:
                    left += 1

        return count


if __name__ == '__main__':

    # Example 1 (2)
    # arr = [3, 1, 4, 1, 5]
    # k = 2

    # Test case (1)
    # arr = [1, 1, 1, 1, 1]
    # k = 0

    # Test case (4)
    arr = [1, 1, 1, 2, 3, 4, 5]
    k = 1

    obj = Solution()
    print(obj.brute_force(arr, k))
    print(obj.hash_and_find(arr, k))
    print(obj.two_pointers(arr, k))
