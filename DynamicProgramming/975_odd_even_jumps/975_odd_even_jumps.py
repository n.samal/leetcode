"""
Inputs
    arr (List[ints]): array of unsorted values
Outputs
    int: number of good starting indices

Goal:
    - Find the number of good starting indices
        + good index is one where we can reach the end of the array: n - 1

    - during odd number jumps
        + we can only move to the smallest value larger than or equal to the current value
        + A[i] <= A[j]
        + if multiple indices, than only move to the smallest index

    - during even number jumps
        + we can only move to the smallest value smaller than or equal to the current value
        + A[i] >= A[j]

Strategy

    Sort then Search

        - Sort
            + at each position we can only use values adjacent to our value, this is similar to crawling
            a sorted array

            + for even moves: we can only move to the value left of our current value
            + for odd moves: we can only move to value right of our current value

            Time Complexity:
                O(n*log(n))
            Space Complexity
                O(n)

        - Search

            - at each position we can crawl the next possible position
            - if we're at the ending index, then this position is good

            - to save time, we can track which positions are good
                ie: we can reach the end, starting at index 0 at a even position, then it's a good index

            Time Complexity
                O(n)
            Space Complexity
                O(n)
"""

from typing import List


class Solution:

    def oddEvenJumps(self, arr: List[int]) -> int:
        """Maintain monotonically decreasing stack

        Time Complexity
            O(n*log(n)) sorting of indices
        Space Complexity
            O(n)
        """

        # Sort the indices by value
        n: int = len(arr)

        ix_inc = sorted(range(n), key=lambda i: arr[i], reverse=False)
        ix_dec = sorted(range(n), key=lambda i: arr[i], reverse=True)

        # Find the smallest larger value to the right
        next_odd = self.get_next(ix_inc)

        # Find the largest smaller value to the right
        next_even = self.get_next(ix_dec)

        ''' Determine if a jump is possible from this starting index '''

        # Save if position is possible from even or odd jump
        arr_even = [False for _ in range(n)]
        arr_odd = [False for _ in range(n)]

        # We can always reach the last index
        arr_even[-1] = True
        arr_odd[-1] = True

        for ix in range(n - 2, -1, -1):

            next_odd_hop = next_odd[ix]
            next_even_hop = next_even[ix]

            # Currently an odd jump
            # can the next even position bring us to the end?
            if next_odd_hop:
                arr_odd[ix] = arr_even[next_odd_hop]

            if next_even_hop:
                arr_even[ix] = arr_odd[next_even_hop]

        return sum(arr_odd)

    ''' Monotonic Stacks with unsorted arrays'''
    # This is not what was needed for this problem
    # this finds the next larger/smaller value to the right
    # ie:  [1, *8*, 10, 2]

    # 8 is the next larger value after 1
    
    # we need to the find the smallest larger value to the right
    # ie:  [1, 8, 10, *2*]
    
    # 2 is the smallest larger value after 1

    def get_next_greater(self, arr: List[int]):
        """Maintain monotonically decreasing stack

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        next_hop = [None for _ in range(len(arr))]
        stack = []

        for ix in range(len(arr)):

            while stack and arr[ix] > arr[stack[-1]]:
                jx = stack.pop()
                next_hop[jx] = ix

            stack.append(ix)

        return next_hop

    def get_next_smaller(self, arr: List[int]):
        """Maintain monotonically increasing stack

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        next_hop = [None for _ in range(len(arr))]
        stack = []

        for ix in range(len(arr)):

            while stack and arr[ix] < arr[stack[-1]]:
                jx = stack.pop()
                next_hop[jx] = ix

            stack.append(ix)

        return next_hop

    ''' Monotonic Stacks with sorted arrays '''

    def get_next(self, ix_arr: List[int]):
        """Find the smallest greater value to the right

        For the largest smaller value: A[i] <= A[j]
        For the smallest larger value: A[i] >= A[j]

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        Notes
            - since the indice array is already sorted,
            we already know the next greater or smaller value
        """

        n: int = len(ix_arr)
        next_hop = [None for _ in range(n)]

        stack = []

        for ix in ix_arr:

            # Remove indices to our left, our current value
            # will be the next greater/smaller value to the right
            while stack and stack[-1] < ix:
                jx = stack.pop()
                next_hop[jx] = ix

            stack.append(ix)

        return next_hop

    def top_down(self, start_ix: int, cur_ix: int, odd_count: bool = True):
        """
        Args:
            start_ix (int): starting index
            cur_ix (int): current index
            odd_count (bool): we're currently on an odd numbered jump
        """

        print(f' start_ix: {start_ix}  cur_ix: {cur_ix}  odd_count: {odd_count}')

        # We're at the final position
        if cur_ix == (self.n - 1):
            self.arr_odd[start_ix] = True
            return

        # No next position to explore
        elif cur_ix is None:
            self.arr_odd[start_ix] = False
            return

        # elif cu

        # We're currently on a odd count
        if odd_count:
            # Find the next possible position
            self.top_down(start_ix, self.next_odd[cur_ix], not odd_count)

        # We're currently on an even count
        else:
            self.top_down(start_ix, self.next_even[cur_ix], not odd_count)


if __name__ == '__main__':

    # 2 good spots
    # arr_in = [10, 13, 12, 14, 15]

    # 3 good spots
    arr_in = [2, 3, 1, 1, 4]

    obj = Solution()
    print(obj.oddEvenJumps(arr_in))
    # print(obj.solution(arr_in))
