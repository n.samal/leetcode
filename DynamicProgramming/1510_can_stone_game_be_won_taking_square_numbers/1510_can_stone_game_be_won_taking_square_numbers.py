"""
Inputs
    n (int): starting number of stones
Outputs:
    bool: can alice win?
Notes
    - alice starts the game
    - each player can take a square number of stones
        + 1^2, 2^2, 3^2
    - we assume each player plays optimally

Examples 

    Example 1:

        Input: n = 1
        Output: true
        Explanation: Alice can remove 1 stone winning the game because Bob doesn't have any moves.
        
    Example 2:

        Input: n = 2
        Output: false
        Explanation: Alice can only remove 1 stone, after that Bob removes the last one winning the game (2 -> 1 -> 0).
        
    Example 3:

        Input: n = 4
        Output: true
        Explanation: n is already a perfect square, Alice can win with one move, removing 4 stones (4 -> 0).
        
    Example 4:

        Input: n = 7
        Output: false
        Explanation: Alice can't win the game if Bob plays optimally.
        If Alice starts removing 4 stones, Bob will remove 1 stone then Alice should remove only 1 stone and finally Bob removes the last one (7 -> 3 -> 2 -> 1 -> 0). 
        If Alice starts removing 1 stone, Bob will remove 4 stones then Alice only can remove 1 stone and finally Bob removes the last one (7 -> 6 -> 2 -> 1 -> 0).
        
    Example 5:

        Input: n = 17
        Output: false
        Explanation: Alice can't win the game if Bob plays optimally.

Ideas

    - if we have a square number of stones to start we win

        + take them all

    - if not a square number, we want to put the next player into a position
    where they cannot win

        + try all potential choices, 1^2, 2^2, 3^2
        + if any of those choices force a loss, that's a win for us
        + otherwise that stone count is a loss for us

    - save the states for each game from 1 to n

        + use bottom up approach
"""


class Solution:
    def winnerSquareGame(self, n: int) -> bool:
        """Bottom Up dynamic programming

        - save the game win state for every game
        from 0 to n
        - for every game, try every stone choice
            + see if we can put the next player
            into a position where they loose

        Time Complexity
            O(n*sqrt(n))
        Space Complexity
            O(n)
        """

        # Determine win state for each value from 0 to n
        dp = [False for _ in range(n + 1)]

        # Determine if alice can win at this number
        for ix in range(1, n + 1):

            # Can we put player 2 at a losing number of stones?
            # try taking all potential choices (square numbers)
            for v in range(1, int(ix**0.5) + 1):

                left = ix - v**2

                if dp[left] is False:
                    dp[ix] = True
                    break

        return dp[n]


if __name__ == '__main__':

    # Example 1 (true)
    n = 1

    # Example 2 (false)
    n = 2

    # Example 3 (true)
    # n = 4

    # Example 4 (false)
    # n = 7

    # Example 5 (false)
    # n = 17


    obj = Solution()
    print(obj.winnerSquareGame(n))

