
*****
Case
*****
aaaa

stop 1, start = 0
a

stop 2, start = 0
aa
stop 2, start = 1
a

stop 3, start = 0
aaa
stop 3, start = 1
aa
stop 3, start = 2
a

stop 4, start = 0
aaaa
stop 4, start = 1
aaa
stop 4, start = 2
aa
stop 4, start = 3
a


- every substring is a valid word to use
- we have a new prefix and plus we build on top of our old prefixes

	+ ['a']
	+ ['aa', 'a a']
	+ ['aaa', 'a aa', 'aa a', 'a a a']
	+ ['a aaa', 'aa aa', 'a a aa', 'aaa a', 'a aa a', 'aa a a', 'a a a a']

- every positions have the option to cut or not to cut, just like a recursive tree

    + O(2^n)

