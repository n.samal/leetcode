"""
Inputs:
    s (str): main string to split into pieces
    wordDict(List[str]): words to use
Outputs:
    List[str]: all possible sentences where the main string
    is broken up using only words from the dictionary
Examples
    Example 1

        Input:
            s = "catsanddog"
            wordDict = ["cat", "cats", "and", "sand", "dog"]
            Output:
            [
              "cats and dog",
              "cat sand dog"
            ]

    Example 2

        Input:
            s = "pineapplepenapple"
            wordDict = ["apple", "pen", "applepen", "pine", "pineapple"]
        Output:
            [
              "pine apple pen apple",
              "pineapple pen apple",
              "pine applepen apple"
            ]
            Explanation: Note that you are allowed to reuse a dictionary word.

    Example 3
        Input:
            s = "catsandog"
            wordDict = ["cats", "dog", "sand", "and", "cat"]
        Output:
            []

Ideas

    - top down approach
        + try splitting the main string at every character
        and search for that substring

"""

from typing import List


class Solution:

    """ Without Memo """

    def wordBreak(self, s: str, wordDict: List[str]) -> List[str]:

        self.ans = []

        self.s = s
        self.n: int = len(s)
        self.words = set(wordDict)

        self.top_down()
        return self.ans

    def top_down(self, ix: int = 0, prior: str = ''):
        """Try splitting the rest of the main string
        and every location

        Time Complexity
            O(n^n)
        Space Complexity
            O(n)

        Notes
            - very expensive. for large valid strings
            this is huge
        """
        # print(f'ix: {ix}   prior: {prior}')

        if ix == self.n:
            self.ans.append(prior)
            return prior

        # Split at every location
        for jx in range(ix, self.n + 1):

            substr = self.s[ix:jx]

            if substr in self.words:
                if prior:
                    self.top_down(jx, prior + ' ' + substr)
                else:
                    self.top_down(jx, substr)

    """ With Memo: Incomplete """

    def wordBreak2(self, s: str, wordDict: List[str]) -> List[str]:

        self.memo = {}

        self.s = s
        self.n: int = len(s)
        self.words = set(wordDict)

        ans = self.top_down2()
        return ans

    def top_down2(self, ix: int = 0, prior: str = ''):
        """Try splitting the rest of the main string
        and every location

        Notes
            - work in progress
        """

        # print(f'ix: {ix}   prior: {prior}')

        if ix == self.n:
            self.memo[prior] = [prior]
            return prior

        if prior in self.memo:
            return self.memo[prior]

        else:

            self.memo[prior] = []

            # Split at every location
            for jx in range(ix + 1, self.n + 1):
                substr = self.s[ix:jx]

                if substr in self.words:
                    if prior:
                        ans = self.top_down2(jx, prior + ' ' + substr)
                    else:
                        ans = self.top_down2(jx, substr)

                    if ans:
                        self.memo[prior].append(ans)

            return self.memo[prior]

    ''' Bottom Up '''

    def bottom_up(self, s: str, wordDict: List[str]) -> List[str]:
        """

        - try splitting the phrase at every point
        - save the valid splits from wordDict
            + save word and where it ended
            + combine this subarray with prefixes
            if possible

        Time Complexity
            O(n^2 * prefixes)

            if all valid prefixes, then prefixes = O(2^n)

        Space Complexity
            O(2^n)

        Notes:
            - could save locations of words instead of works, to save space
                + will need to create strings at the end
        """

        n: int = len(s)
        words = set(wordDict)

        # Check if the string has characters we don't have access to
        if set(s) - set(''.join(wordDict)):
            return []

        # Words ending at each index
        dp = [[] for ix in range(n + 1)]
        dp[0] = ['']

        # Try all splits
        for end_ix in range(1, n + 1):
            for start_ix in range(end_ix):

                substr = s[start_ix:end_ix]

                # Combine with each prefix ending at our
                # start point if it exists
                if substr in words:
                    for prefix in dp[start_ix]:

                        if prefix:
                            dp[end_ix].append(prefix + ' ' + substr)
                        else:
                            dp[end_ix].append(substr)

        return dp[-1]


if __name__ == '__main__':

    # Example 1
    s = "catsanddog"
    wordDict = ["cat", "cats", "and", "sand", "dog"]

    # Example
    # s = "catsandog"
    # wordDict = ["cats", "dog", "sand", "and", "cat"]

    # String has character dictionary does not have
    s = "catsandogq"
    wordDict = ["cats", "dog", "sand", "and", "cat"]

    # Every split is a valid prefix
    s = 'aaaaa'
    wordDict = ["a", "aa", "aaa", "and", "cat"]

    obj = Solution()
    # print(obj.wordBreak(s, wordDict))
    # print(obj.wordBreak2(s, wordDict))
    print(obj.bottom_up(s, wordDict))
