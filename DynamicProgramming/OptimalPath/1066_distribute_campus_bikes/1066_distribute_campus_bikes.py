"""
Inputs
    workers (List[List[int]]): location of works
    bikes (List[List[int]]): location of available bikes
Outputs
    - minimum possible sum of Manhattan distances between each worker and their assigned bike
Notes
    - for each worker assign them a bike
    - we want to minimize the overall distance for workers to their assigned bike

    - use the manhattan distance as our distance metric

References:
    https://leetcode.com/problems/campus-bikes-ii/discuss/?currentPage=1&orderBy=most_votes&query=
"""

''' Closest Bike for each worker '''

import heapq

class Solution:
    def assignBikes(self, workers: List[List[int]], bikes: List[List[int]]) -> int:
        """

        - for each worker compute the distance to every bike
        - maintain a heap with the closest bike for each worker
        - grab the closest bike from the heap, and assign it
        to the relvant worker
            + if the bike is taken, then populate the heap
            with the second closest bike

        Notes
            - doesn't work for all cases
        """

        total: int = 0
        n: int = len(bikes)
        used_bike = [False for _ in range(n)]

        # Compute the distances to each bike for each worker
        # notate the bike index for each distance
        # place the smallest bikes at the rear
        distances = []

        for w, worker in enumerate(workers):
            distances.append([(self.distance(worker, bike), w, b) for b, bike in enumerate(bikes)])
            distances[-1] = sorted(distances[-1], reverse=True)

        # Place the closest bikes for each worker in a heap
        # node = (distance, worker_ix, bike_ix)
        heap = []
        for ix in range(n):
            heapq.heappush(heap, distances[ix].pop())

        # Grab the smallest distances possible globally
        # - if a worker has a bike assigned don't place any more of their bikes in the heap
        # - if a bike is already used populate the heap with the next closest bike for that worker
        count: int = 0
        while count < n:

            dist, w, b = heapq.heappop(heap)

            print(f'dist: {dist} worker: {w}  bike: {b}')

            if used_bike[b] is False:
                count += 1
                total += dist
                used_bike[b] = True

                print(f' picking bike: {b}')

            # Grab the next closest bike for that worker
            else:
                print(f' next closest for worker: {w}')
                heapq.heappush(heap, distances[w].pop())

        return total

    def distance(self, loc1, loc2) -> int:
        return abs(loc1[0] - loc2[0]) + abs(loc1[1] - loc2[1])

''' Back Tracking: Too Slow '''

class Solution:
    def assignBikes(self, workers: List[List[int]], bikes: List[List[int]]) -> int:
        """

        Notes
            Works but is to slow, lets try memo
        """

        # How do we assign bikes so the distance is minimized?
        # for each worker assign them the closest bike
        n: int = len(bikes)
        self.used = [False for _ in range(n)]
        self.min_ans = float('inf')

        self.recurse(workers, bikes, 0, 0)
        return self.min_ans

    def recurse(self, workers, bikes, worker_ix: int, cost: int):

        if worker_ix == len(workers):
            self.min_ans = min(self.min_ans, cost)
            return

        if cost > self.min_ans:
            return

        for b, bike in enumerate(bikes):

            if self.used[b] is False:

                self.used[b] = True
                self.recurse(workers, bikes, worker_ix + 1, cost + self.distance(workers[worker_ix], bike))
                self.used[b] = False

    def distance(self, loc1, loc2) -> int:
        return abs(loc1[0] - loc2[0]) + abs(loc1[1] - loc2[1])

''' Backtracking with memo '''

class Solution:
    def assignBikes(self, workers: List[List[int]], bikes: List[List[int]]) -> int:
        """Backtracking with memo for the min_cost

        """

        n: int = len(bikes)
        used = [False for _ in range(n)]
        self.cache = {}

        min_total = self.get_min(workers, bikes, 0, used)
        return min_total

    def get_min(self, workers, bikes, worker_ix: int, used: List[int]):
        """
        - try all available bikes
        - save the min_cos for each worker and possible bike state

        Time Complexity
            O(w * 2^b ?)  number of workers and used bike states
        Space Complexity
            O(w * 2^b ?)
        Notes
            - could we use a bitmask to represent the bike states
        """

        # No more workers to assign
        if worker_ix == len(workers):
            return 0

        # Extract previous computation
        key = (worker_ix, tuple(used))

        if key in self.cache:
            return self.cache[key]

        min_cost = float('inf')

        # Try assigning every available bike
        for b, bike in enumerate(bikes):

            if used[b] is False:
                used[b] = True
                cur_min = self.get_min(workers, bikes, worker_ix + 1, used) + self.distance(workers[worker_ix], bike)
                used[b] = False

                min_cost = min(min_cost, cur_min)

        # Save the best choice
        self.cache[key] = min_cost

        return min_cost


    def distance(self, loc1, loc2) -> int:
        return abs(loc1[0] - loc2[0]) + abs(loc1[1] - loc2[1])
