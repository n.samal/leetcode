"""
Inputs
    arr (List[int]): points for n sided polygon
Outputs:
    int: minimum triangulation score possible

Notes
    - n points provided in clockwise order
    - these points create a convex polygon which is not self intersecting

    - we want to triangulate the polygon to triangles
        - n points will create n - 2 triangles

    - our goal is to triangulate the points to minimize the triangulation score

        + total score = product of vertice labels
        +

Examples

    Example 1
        Input: [1,2,3]
        Output: 6
        Explanation: The polygon is already triangulated, and the score of the only triangle is 6.

    Example 2

        Input: [3,7,4,5]
        Output: 144
        Explanation: There are two triangulations, with possible scores: 3*7*5 + 4*5*7 = 245, or 3*4*5 + 3*4*7 = 144.  The minimum score is 144.

    Example 3
        Input: [1,3,1,4,1,5]
        Output: 13
        Explanation: The minimum score triangulation has score 1*1*3 + 1*1*4 + 1*1*5 + 1*1*1 = 13.

Ideas

    - specific vertice orders will create overlapping triangles
        + we need to specify valid triangle vertices 

            i < j < k

        + triangle does not need to include it's adjacent points
        however it can include points adjacent for the geometry not just the array.

            [0, 1, 2, 3]
            ie: connect 0 and 3

    - creating valid triplets

        + we need 3 vertices for a triangle => (i, j, k)

            i < j < k

        + we can pick i & j  then pick k, where k < n
        + or can pick i & k, then pick j between

        + selection of i & k is more beneficial to us, since we don't need to track the upper bound
        + additionally, when we slice a convex polygon with a triangle, the interior triangles left
        are defined by the start and stop indices

            ie: 
                vertices = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

                triangle = [0, 5, 9]

                this creates 2 polygons to explore further

                polygon1 = [0, 5]
                polygon2 = [5, 9]

            without use of an upperbound of n - 1, the upperbound could be 0

            ie:
                triangle = [0, 1, 5]
                polygon1 = [1, 5]
                polygon2 = [5, 0]

        + choosing start = 0, stop = n - 1, we break the whole polygon in to pieces, with
        minimal tracking of bounds

    - looks like dynamic programming problem
        + we can compute the score for every possible valid triplet of points
        + combine the score from that triplet with the max possible score of remaining triangles

        + score = score_here + score_reminaing triangles

References
    https://leetcode.com/problems/minimum-score-triangulation-of-polygon/discuss/286753/C%2B%2B-with-picture
    https://leetcode.com/problems/minimum-score-triangulation-of-polygon/discuss/294265/Python-In-depth-Explanation-DP-For-Beginners
    https://leetcode.com/problems/minimum-score-triangulation-of-polygon/discuss/286754/C%2B%2BPython-O(n3)-DP-explanation-%2B-diagrams

"""
from typing import List


class Solution:
    def minScoreTriangulation(self, arr: List[int]) -> int:

        self.arr = arr
        self.dp = {}

        n: int = len(arr)

        return self.score(0, n - 1)

    def score(self, start: int, stop: int) -> int:
        """Find the min score for triangulating the polygon defined by
        start, stop (inclusive, inclusive)

        Args:
            start: starting allowable index of polygon triangulation
            stop: ending allowable index of polygon triangulation
        Returns:
            int: minimum triangulation score for polygon encompassed by
            points from start to stop

        Time Complexity
            O(n^3)

            - for every pair of points we explore n midpoints
        Space Complexity
            O(n^2)

            - we have n^2 pairs of start and stop points
        """

        state = (start, stop)

        if state not in self.dp:

            # Not enough points
            # ie: [1, 2, 3] tries to create to other polygons
            # between 1 & 2, and 2 & 3
            if stop - start < 2:
                return 0

            min_score: int = float('inf')

            for mid in range(start + 1, stop):

                cur_score = self.arr[start] * self.arr[mid] * self.arr[stop]
                total_score = cur_score + self.score(start, mid) + self.score(mid, stop)

                min_score = min(min_score, total_score)

            self.dp[state] = min_score

        return self.dp[state]


if __name__ == '__main__':

    # Example 1
    # arr = [1, 2, 3]

    # Example 2
    # arr = [3, 7, 4, 5]

    # Example 3
    arr = [1, 3, 1, 4, 1, 5]

    obj = Solution()
    print(obj.minScoreTriangulation(arr))
