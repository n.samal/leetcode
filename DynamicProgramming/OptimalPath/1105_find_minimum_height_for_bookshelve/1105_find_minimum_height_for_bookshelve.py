"""
Inputs
    books ()
    shelf_width (int):
Outputs
    int: minimum height for all books

Notes
    - books must be added in their order
    - books dimensions are provided (thickness, height)

        + for each row we're limited by the tallest book

    - we are limited by the shelf width 

Ideas

    - try both routes for each book
        + add the book to the current shelf
        + create a new shelf with the book
"""

''' Original '''

class Solution:
    def minHeightShelves(self, books: List[List[int]], shelf_width: int) -> int:

        # Try top down with recursion
        # choices, add onto current row
        # start a new row with book

        self.cache = {}
        self.shelf_width = shelf_width

        self.min_height = float('inf')
        self.recurse(books, 0, 0, 0, 0)

        return self.min_height

    def recurse(self, books, ix: int, cur_width: int, cur_max_height: int, total_height: int):
        """Try each route

        Args:
            cur_width (int): width taken up so far on this shelve
            cur_max_height (int): largest height in current row
            total_height (int): height of all the rows

        Notes
            - see how we can reduce the state space
        """

        key = (ix, cur_width, cur_max_height, total_height)

        if key in self.cache:
            return self.cache[key]

        # No more books left
        if ix == len(books):
            self.min_height = min(self.min_height, total_height + cur_max_height)
            return total_height + cur_max_height

        add_to_current = float('inf')

        # Add the book if there's space
        if cur_width + books[ix][0] <= self.shelf_width:
            add_to_current = self.recurse(books, ix + 1, cur_width + books[ix][0], max(books[ix][1], cur_max_height), total_height)

        # Try starting a new row
        add_to_next = self.recurse(books, ix + 1, books[ix][0], books[ix][1], total_height + cur_max_height)

        best = min(add_to_current, add_to_next)

        self.cache[key] = best

        return best

''' Reduced State Space '''

class Solution:
    def minHeightShelves(self, books: List[List[int]], shelf_width: int) -> int:
        """Maintain a cache and compute the minimum book shelve height"""

        # Try top down with recursion
        # choices, add onto current row
        # start a new row with book

        self.cache = {}
        self.shelf_width = shelf_width

        min_height = self.recurse(books, 0, 0, 0)

        return min_height

    def recurse(self, books, ix: int, cur_width: int, cur_max_height: int):
        """

        - try both routes for each book if applicable
            + add book to the current shelf
            + start a new shelf with the book

        - track the minimum remaining height for the current state

        Time Complexity

        Space Complexity
            O(n*shelf_width*max_height)

        """

        key = (ix, cur_width, cur_max_height)

        # No more books left
        if ix == len(books):
            return cur_max_height

        if key in self.cache:
            return self.cache[key]

        add_to_current = float('inf')

        # Add the book if there's space
        if cur_width + books[ix][0] <= self.shelf_width:
            add_to_current = self.recurse(books, ix + 1, cur_width + books[ix][0], max(books[ix][1], cur_max_height))

        # Try starting a new row
        add_to_next = cur_max_height + self.recurse(books, ix + 1, books[ix][0], books[ix][1])

        self.cache[key] = min(add_to_current, add_to_next)

        return self.cache[key]
