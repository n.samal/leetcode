"""

Inputs
    arr (List[List[int]]): 2D matrix of zeros or ones
Outputs
    int: the area of the maximal square of all ones

Strategy

    - Brute Force

        + at each index grow the size of the square

            * check each of the new adjacent cells
            * row_check will be everything in the next row
            * col check will be everyhting in the next column
            * diagnol

        + save the current size of the square
        + Notes

            * we may be able to save time by not checking elements we've already checked

    - Dynamic Programming

        + at each position, the maximum square is a function of multiple values

            * current position (must be a 1)
            * row above (must have a value 1)
            * column to left (must have a value 1)
            * diagnol
        + we compute the maximal square ending and the current position (bottom right corner)

            * current = min(diag, left, up) + 1
            
            if any adjacent elements are 0, then our maximum square ending here is 1 aka single element
            if any adjacent elements had a previous square, then we increase that by 1

        Example 1

            Array    Square Array
            [1, 0]   [1, 0]
            [1, 1]   [0, 0]

            no square possible, other than single element.

        Example 2
            Array    Square Array
            [1, 1]   [1, 1]
            [1, 1]   [1, 0]

            - first column and first row are original values from our array
            - the diagnol at (1,1) depends our adjacents

        Example 3
            Array            DP
            [0, 1, 1, 1, 0]    [0, 1, 1, 1, 0]
            [1, 1, 1, 1, 0]    [1, 1, 2, 2, 0]
            [0, 1, 1, 1, 1]    [0, 1, 2, 3, 1]
            [0, 1, 1, 1, 1]    [0, 1, 2, 3, 2]
            [0, 0, 1, 1, 1]    [0, 0, 1, 2, 3]

Cases
    Null Case
    Single Element
    All Zeros
    All Ones
    General case: hybrid values
"""

from typing import List


class Solution:

    def maximalSquare(self, arr: List[List[str]]) -> int:
        """Brute force approach

        - at each position, try to make the square bigger
        - compute a new area if all values are 0
        - compare that against our best

        Time Complexity
            O((m*n)^2)
        Space Complexity
            O(1)
        """

        # Null case
        if not arr:
            return 0

        max_area: int = 0
        n_rows: int = len(arr)
        n_cols: int = len(arr[0])

        # Go through each cell
        for row_ix in range(n_rows):
            for col_ix in range(n_cols):

                # Zero, not a possible starting position
                if arr[row_ix][col_ix] == "1":

                    # Update max
                    max_area = max(1, max_area)

                    # Start bounds at current location (left corner)
                    row_end = row_ix + 1
                    col_end = col_ix + 1

                    # Try increasing the area
                    while row_end < n_rows and col_end < n_cols:

                        # Define new elements to check
                        new_diag = arr[row_end][col_end]
                        new_row = arr[row_end][col_ix:col_end + 1]
                        new_col = [arr[ix][col_end] for ix in range(row_ix, row_end + 1)]

                        # A zero exists in our square
                        if new_diag == "0" or "0" in new_col or "0" in new_row:
                            break

                        # Compute the starting area
                        area = (row_end - row_ix + 1) * (col_end - col_ix + 1)
                        max_area = max(area, max_area)

                        # Increase the square by one
                        row_end += 1
                        col_end += 1

        return max_area

    def dynamic(self, arr: List[List[str]]) -> int:
        """Dynamic programming approach

        - at each position, compute the max square ending here
        - function
        - compare that against our best

        Notes
            - we don't need the whole array of square values
              we could just use the previous diagnol, column, row

        Time Complexity
            O((m*n))
        Space Complexity
            O(m*n)
        """

        # Null case
        if not arr:
            return 0

        # Start with initial element (for single element case)
        max_sq: int = int(arr[0][0])
        n_rows: int = len(arr)
        n_cols: int = len(arr[0])

        # Create empty matrix for storing square sizes
        arr_v = [[0 for col_ix in range(n_cols)] for row_ix in range(n_rows)]

        # Compute row and track max square for single row
        for col_ix in range(n_cols):
            arr_v[0][col_ix] = int(arr[0][col_ix])
            max_sq = max(max_sq, arr_v[0][col_ix])

        # Compute column and track max square for single column
        for row_ix in range(n_rows):
            arr_v[row_ix][0] = int(arr[row_ix][0])
            max_sq = max(max_sq, arr_v[row_ix][0])

        # Go through each cell
        # Skip the first column and row since, square can't end there
        for row_ix in range(1, n_rows):
            for col_ix in range(1, n_cols):

                # Zero, not a possible starting position
                if arr[row_ix][col_ix] == "1":

                    # Square size is function of adjacent elements
                    arr_v[row_ix][col_ix] = min(arr_v[row_ix - 1][col_ix - 1],  arr_v[row_ix][col_ix - 1], arr_v[row_ix - 1][col_ix]) + 1

                    # Save max square size
                    max_sq = max(arr_v[row_ix][col_ix], max_sq)

        # Calculate the area
        return max_sq ** 2

    def dynamic2(self, arr: List[List[str]]) -> int:
        """Dynamic programming approach

        - at each position, compute the max square ending here
        - only maintain the last row and column

        Time Complexity
            O((m*n))
        Space Complexity
            O(n)
        TODO:
            work in progress
        """

        # Null case
        if not arr:
            return 0

        # Start with initial element (for single element case)
        max_sq: int = 0
        n_rows: int = len(arr)
        n_cols: int = len(arr[0])

        # Store square sizes
        prev_dia = int(arr[0][0])
        prev_col = [int(arr[row_ix][0]) for row_ix in range(n_rows)]
        prev_row = [int(arr[0][col_ix]) for col_ix in range(n_cols)]

        max_sq = max(prev_dia, max(prev_col), max(prev_row))

        # TODO: need to finish rest

        # Go through each cell
        for row_ix in range(1, n_rows):
            for col_ix in range(1, n_cols):

                # Zero, not a possible starting position
                if arr[row_ix][col_ix] == "1":

                    # Square size is function of adjacent elements
                    cur_sq = min(arr_v[row_ix - 1][col_ix - 1],  prev_row[col_ix], prev_col[row_ix]) + 1

                    # Save max square size
                    max_sq = max(cur_sq, max_sq)

        # Calculate the area
        return max_sq ** 2


if __name__ == "__main__":

    s = Solution()

    arr_in = [["1", "0", "1", "0", "0"],
            ["1", "0", "1", "1", "1"],
            ["1", "1", "1", "1", "1"],
            ["1", "0", "0", "1", "0"]]

    # print(s.maximalSquare(arr_in))
    print(s.dynamic(arr_in))
