"""
Inputs
    nums1 ([List[int]]): sorted array
    nums2 ([List[int]]): sorted array
Outputs
    int: max sum of all valid pathss
Examples

    Example 1:
        Input: nums1 = [2,4,5,8,10], nums2 = [4,6,8,9]
        Output: 30
        Explanation: Valid paths:
        [2,4,5,8,10], [2,4,5,8,9], [2,4,6,8,9], [2,4,6,8,10],  (starting from nums1)
        [4,6,8,9], [4,5,8,10], [4,5,8,9], [4,6,8,10]    (starting from nums2)
        The maximum is obtained with the path in green [2,4,6,8,10].

    Example 2:

        Input: nums1 = [1,3,5,7,9], nums2 = [3,5,100]
        Output: 109
        Explanation: Maximum sum is obtained with the path [1,3,5,100].

    Example 3:

        Input: nums1 = [1,2,3,4,5], nums2 = [6,7,8,9,10]
        Output: 40
        Explanation: There are no common elements between nums1 and nums2.
        Maximum sum is obtained with the path [6,7,8,9,10].

    Example 4:

        Input: nums1 = [1,4,5,8,9,11,19], nums2 = [2,3,4,11,12]
        Output: 61

Notes
    - both arrays are sorted
    - you must traverse from left to right
    - if the same value is present in the other array
    we can change to that location, however we don't
    get the sum for both duplicate values, just one
    - score is defined a unique sum of values
"""

from typing import List


class Solution:

    ''' Top down '''

    def maxSum(self, nums1: List[int], nums2: List[int]) -> int:
        """Top down with memo

        - crawl from the start of both paths
        - if the node is common to both we can switch
        paths
        - track the max sum possible to the right from any
        given node

        Time Complexity
            O(max(m*n) * 2)
        Space Complexity
            O(max(m*n) * 2)
        """

        n: int = len(nums1)
        m: int = len(nums2)

        # Save the max remaining sum possible
        memo = {}  # (ix, path)

        # Save locations of each value
        # (our teleports)
        dct1 = {v: ix for ix, v in enumerate(nums1)}
        dct2 = {v: ix for ix, v in enumerate(nums2)}

        def top_down(ix: int, path1: bool, cur_sum: int = 0):

            state = (ix, path1)

            if state in memo:
                return memo[state]

            # None left to travel
            if path1 and ix == n:
                return cur_sum

            if not path1 and ix == m:
                return cur_sum

            route_swp = 0

            # Swap paths and take value
            if path1 and nums1[ix] in dct2:
                jx = dct2[nums1[ix]]
                route_swp = nums1[ix] + cur_sum + top_down(jx + 1, not path1)

            if not path1 and nums2[ix] in dct1:
                jx = dct1[nums2[ix]]
                route_swp = cur_sum + nums2[ix] + top_down(jx + 1, not path1)

            # Can move along current path
            if path1:
                adder = nums1[ix]
            else:
                adder = nums2[ix]

            route_same = adder + top_down(ix + 1, path1, 0)

            best = max(route_same, route_swp)

            memo[state] = best

            return best

        best1 = top_down(0, True)
        best2 = top_down(0, False)
        best = max(best1, best2)

        print(f'b1: {best1}  b2: {best2}')

        return best % (10**9 + 7)

    ''' TwoPointers '''

    def two_pointers(self, nums1: List[int], nums2: List[int]) -> int:
        """Use two pointers to crawl both arrays

        - crawl both paths simultaneously
            + first grab the smaller value
            + continue to crawl until we hit the end or a common value
            + add all values on both arrays until checkpoint

        - at the check point we could've come from either path
            + assume we were previously along the path with the better sum
            + reset path sums, and try again

        Time Complexity
            O(m + n)
        Space Complexity
            O(1)
        """

        n: int = len(nums1)
        m: int = len(nums2)

        ix: int = 0
        jx: int = 0

        sum1: int = 0
        sum2: int = 0
        res: int = 0

        while ix < n and jx < m:

            # num1 smaller
            if nums1[ix] < nums2[jx]:
                sum1 += nums1[ix]
                ix += 1

            # num2 smaller
            elif nums2[jx] < nums1[ix]:
                sum2 += nums2[jx]
                jx += 1

            # Numbers are the same, assume we were
            # on the better of the two paths previously
            else:

                res += max(sum1, sum2) + nums1[ix]

                ix += 1
                jx += 1

                sum1 = 0
                sum2 = 0

        # Grab remaining values
        while ix < n:
            sum1 += nums1[ix]
            ix += 1

        while jx < m:
            sum2 += nums2[jx]
            jx += 1

        # Grab the better of the two paths
        res += max(sum1, sum2)

        return res % (10**9 + 7)


if __name__ == "__main__":

    # Example 1
    # nums1 = [2, 4, 5, 8, 10]
    # nums2 = [4, 6, 8, 9]

    nums1 = [1, 3, 5, 7, 9]
    nums2 = [3, 5, 100]

    obj = Solution()
    print(obj.two_pointers(nums1, nums2))
