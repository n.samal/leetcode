"""
Inputs
    coins (List[int]): 
    amount (int): total amount of money
Outputs
    int: find the fewest number of coins needed to make the amount
Notes
    - if money cannot be made up, then return 0

Examples

    Example 1

        Input: coins = [1, 2, 5], amount = 11
        Output: 3 
        Explanation: 11 = 5 + 5 + 1

    Example 2
        Input: coins = [2], amount = 3
        Output: -1

Ideas
    - knapsack variation

    - apply dynamic programming
        + build up solutions from the ground up
        + at each iteration determine if this denomination
        can be made, or made better using the previously
        computed results

"""

from typing import List


class Solution:
    def coinChange(self, coins: List[int], amount: int) -> int:
        """Build from the bottom up

        - find the best coins to use for each amount up until
        the desired value
        - if it's invalid, skip
        - save our best amounts in the array, and leave the rest
        as -1

        Time Complexity
            O(A*C)  for each amount we try each coin
        Space Complexity
            O(A)  save all amounts up to total amount
        """

        # Place coins from smallest to largest
        coins.sort()

        # Coins needed for each amount
        arr = [-1 for ix in range(amount + 1)]

        # Add base case (no coins needed for zero)
        arr[0] = 0

        # Compute other amounts
        for amount_cur in range(1, amount + 1):

            min_coins = float('inf')

            for coin in coins:

                amount_rem = amount_cur - coin

                # Larger amounts won't work either
                if amount_rem < 0:
                    break

                elif arr[amount_rem] != -1:
                    coins_cur = 1 + arr[amount_rem] 
                    min_coins = min(coins_cur, min_coins)

            if min_coins != float('inf'):
                arr[amount_cur] = min_coins

        return arr[amount]


if __name__ == '__main__':

    # Example 1
    # coins = [1, 2, 5]
    # amount = 11

    # Example 2
    coins = [2]
    amount = 3

    obj = Solution()
    print(obj.coinChange(coins, amount))
