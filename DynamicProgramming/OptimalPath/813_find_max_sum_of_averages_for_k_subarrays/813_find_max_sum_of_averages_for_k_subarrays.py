"""
Inputs
	arr (List[int]): 
	k (int): number of adjacent subgroups
Outputs
	int: maximum sum of averages
Notes
	- our partition must use every number in A
	- scores are not necessarily integers.

Examples
	Input: 
		A = [9,1,2,3,9]
		K = 3
	Output: 20
		Explanation: 
		The best choice is to partition A into [9], [1, 2, 3], [9]. The answer is 9 + (1 + 2 + 3) / 3 + 9 = 20.
		We could have also partitioned A into [9, 1], [2], [3, 9], for example.
		That partition would lead to a score of 5 + 2 + 6 = 13, which is worse.

Ideas
    - use prefix sum to compute summations and averages
    more quickly

    - binary search with guess and check?
        + checking method is tricky..

    - dynamic programming

		+ try computing each subarray grouping
		and combine it with the best of the remaining values

        + try building off smaller values
        + use top down with memo
"""

from typing import List


class Solution:
    def largestSumOfAverages(self, arr: List[int], K: int) -> float:
    	"""Top down with memo"""

        self.n: int = len(arr)
        self.cum_sum = self.get_cum_sum(arr)
        self.memo = [[0 for ix in range(self.n)] for _ in range(K + 1)]

        # Every value is its own subarray
        if K >= self.n:
            return self.cum_sum[-1]

        return self.max_remaining(K)

    def max_remaining(self, k: int, ix: int = 0) -> float:
        """Find the max average for k groups starting at this position

        Args:
            k: number of remaining groups
            ix: starting position in array

        Time Complexity
            O(n * k)
        Space Complexity
            O(n * k)
        """

        # Invalid subarray
        if ix >= self.n:
            return 0

        # Grab saved result
        if self.memo[k][ix] != 0:
            return self.memo[k][ix]

        # Only one group left
        if k == 1:
            self.memo[k][ix] = self.average(self.cum_sum, ix, self.n)
            return self.memo[k][ix]

        # Try creating groups at every junction, then add that to the
        # best of the remaining groups
        for jx in range(ix + 1, self.n + 1):

            sum_here = self.average(self.cum_sum, ix, jx)
            current = sum_here + self.max_remaining(k - 1, jx)
            self.memo[k][ix] = max(self.memo[k][ix], current)

        return self.memo[k][ix]

    ''' Utility '''

    def get_cum_sum(self, arr) -> List[int]:
        """Calculate cummulative summation"""

        n = len(arr) + 1
        cum_sum = [0 for _ in range(n)]

        for ix in range(1, n):
            cum_sum[ix] = cum_sum[ix - 1] + arr[ix - 1]

        return cum_sum

    def average(self, cum_sum: List[int], i: int, j: int) -> float:
        """Calculate the average of a subarray

        Args:
            i: inclusive starting position
            j: exclusive ending position
        Returns:
            float: average of subarray
        """
        return (cum_sum[j] - cum_sum[i]) / (j - i)


def check_averages(arr):
    obj = Solution()
    cum_sum = obj.get_cum_sum(arr)

    for ix in range(1, len(arr) + 1):
        print(obj.average(cum_sum, 0, ix))


if __name__ == '__main__':

    # Example 1 (20)
    arr = [9, 1, 2, 3, 9]
    K = 3

    # arr = [1, 2, 3]
    # K = 3

    obj = Solution()
    print(obj.largestSumOfAverages(arr, K))
    # print(obj.largestSumOfAverages(arr, 1))
    # print(obj.largestSumOfAverages(arr, 2))
    # print(obj.largestSumOfAverages(arr, 3))
