"""
Inputs
    s (str): non encoded string
    k (int): number of character deletions available
Outputs
    int: smallest encoding length possible
Notes
    - a string compression method that works by replacing consecutive identical characters (repeated 2 or more times)
     with the concatenation of the character and the number marking the count of the characters (length of the run)
    - to compress the string "aabccc" we replace "aa" by "a2" and replace "ccc" by "c3". 
        + the compressed string becomes "a2bc3".
    - we are not adding '1' after single characters.
    - you can delete at most k characters

Examples

    Example 1
        Input: s = "aaabcccd", k = 2
        Output: 4
        Explanation: Compressing s without deleting anything will give us "a3bc3d" of length 6. 
        Deleting any of the characters 'a' or 'c' would at most decrease the length of the compressed string to 5, 
        for instance delete 2 'a' then we will have s = "abcccd" which compressed is abc3d. 
        Therefore, the optimal way is to delete 'b' and 'd', then the compressed version of s will be "a3c3" of length 4.

    Example 2

        Input: s = "aabbaa", k = 2
        Output: 2
        Explanation: If we delete both 'b' characters,
        the resulting compressed string would be "a4" of length 2.

    Example 3

        Input: s = "aaaaaaaaaaa", k = 0
        Output: 3
        Explanation: Since k is zero, we cannot delete anything.
        The compressed string is "a11" of length 3.


Ideas
    - create a list of tuples aggregating consecutive characters
        + ie: [('a', 3), ('b', 1), ('c', 3), ('d', 1)]
        + list of list may be more appropriate since we can update it

    - reducing the encoding occurs when
        - removing a character entirely ('b', 1)
        - removing a character to a smaller digit length  ('b', 10) => ('b', 9) or ('b', 100) => '(b', 99)
        - removing an in between character    (a, 2), ('b', 2), (a, 3) => ('a', 5)

    - at each character in the string
        + we keep the character
        + or delete the character

    - dynamic programming likely the way to go
        + current character
        + current character 'run'
        + current encoding length, or previous path
        + number of deletions left

    - how to create encoding length on the fly?

References:
    https://leetcode.com/problems/string-compression-ii/discuss/767602/Python-Clear-top-down-recursive-approach-with-detailed-comments
    https://leetcode.com/problems/string-compression-ii/discuss/757363/Java-Easy-and-fast-DP-solution-with-comments-50ms-Space-O(n2)
    https://leetcode.com/problems/string-compression-ii/discuss/755991/Java-Recursive-solution-50ms
"""
from typing import List
import math


def count_digits(n: int):
    """Get number of digits """
    return math.floor(math.log(n, 10) + 1)


class Solution:

    ''' Brainstorming '''

    def get_counts(self, s) -> List[List[object]]:
        """Gather counts of each consecutive character

        s = "aaabcccd", k = 2

        [['a', 3], ['b', 1], ['c', 3], ['d', 1]]
        """

        n: int = len(s)

        # Save character and count
        counts = [[s[0], 1]]

        # Gather counts for each character
        for ix in range(1, n):

            if counts[-1][0] == s[ix]:
                counts[-1][1] += 1
            else:
                counts.append([s[ix], 1])

        return counts

    ''' Top Down '''

    def top_down(self, s: str, k: int) -> int:
        """Top down with memo

        Time Complexity
            O(n*26*n*k*4)
        Space Complexity
            O(n*26*n*k*4)
        Notes
            - too slow
            - state space may be too large
        """

        n: int = len(s)
        memo = {}

        def helper(ix: int, prev_char: str, prev_run: int, deletions: int, enc_len: int = 0) -> int:
            """

            Args:
                ix: pointer in main string
                prev_char: previous character
                prev_run: length of previous character run
                deletions: number of character deletions left
                enc_len: current encoding length
            Returns:
                int: minimum encoding length
            """

            state = (ix, prev_char, prev_run, deletions, enc_len)

            if state in memo:
                return memo[state]

            if ix == n:
                if prev_run == 0:
                    enc_adder = 0
                elif prev_run == 1:
                    enc_adder = 1
                else:
                    enc_adder = 1 + len(str(prev_run))

                return enc_len + enc_adder

            # We can continue our prior run or start a new one
            if s[ix] == prev_char:
                route_cont = helper(ix + 1, prev_char, prev_run + 1, deletions, enc_len)

            # Start a new run, calculate the addition to the encoding length
            else:

                if prev_run == 0:
                    enc_adder = 0
                elif prev_run == 1:
                    enc_adder = 1
                else:
                    enc_adder = 1 + len(str(prev_run))

                route_cont = helper(ix + 1, s[ix], 1, deletions, enc_len + enc_adder)

            # Delete the new character
            if deletions > 0:
                route_del = helper(ix + 1, prev_char, prev_run, deletions - 1, enc_len)
            else:
                route_del = float('inf')

            min_route = min(route_cont, route_del)
            memo[state] = min_route

            return min_route

        return helper(0, '', 0, k)

    def top_down2(self, s: str, k: int) -> int:
        """Top down with memo

        - reduced size of state space
            + removed encoding length, and added it directly
            to the route
        - leverage problem constraints
            + encoding length only change 3 cases
                0 to 1
                9 to 10
                99 to 100

        Time Complexity
            O(n*26*n*k)
        Space Complexity
            O(n*26*n*k)
        References:
            https://leetcode.com/problems/string-compression-ii/discuss/755970/Python-dynamic-programming
        """

        n: int = len(s)
        memo = {}

        def helper(ix: int, prev_char: str, prev_run: int, deletions: int) -> int:
            """
            Args:
                ix: pointer in main string
                prev_char: previous character
                prev_run: length of previous character run
                deletions: number of character deletions left
            Returns:
                int: minimum encoding length
            """

            state = (ix, prev_char, prev_run, deletions)

            if state in memo:
                return memo[state]

            if ix == n:
                return 0

            # Continue our previous run
            if s[ix] == prev_char:

                # Add extra length in encoding
                # a to a2, a9 to a10, a99 to a100
                if prev_run in {1, 9, 99}:
                    enc_adder = 1
                else:
                    enc_adder = 0

                route_cont = enc_adder + helper(ix + 1, prev_char, prev_run + 1, deletions)

            # Start a new run, adding the encoding length
            # for just the new character ie: 'a'
            else:
                route_cont = 1 + helper(ix + 1, s[ix], 1, deletions)

            # Delete the new character
            if deletions > 0:
                route_del = helper(ix + 1, prev_char, prev_run, deletions - 1)
            else:
                route_del = float('inf')

            min_route = min(route_cont, route_del)
            memo[state] = min_route

            return min_route

        return helper(0, '', 0, k)


if __name__ == "__main__":

    # Example 1
    # s = "aaabcccd"
    # k = 2

    # Example 2
    # s = "aabbaa"
    # k = 2

    # Example 3
    # s = "aaaaaaaaaaa"
    s = "aaaa"
    k = 0

    # TLE
    # s = "abcdefghijklmnopqrstuvwxyz"
    # k = 16

    obj = Solution()
    # print(obj.getLengthOfOptimalCompression(s, k))
    print(obj.top_down(s, k))
    print(obj.top_down2(s, k))
