"""
Inputs
    arr (List[List[int]]): integer matrix
Ouputs
    int: find the length of the longest increasing path

Notes
    - from each cell you can go left, right, up or down
    - no wrap around allowed

Ideas

    - try starting from the smallest nodes first
        + crawl paths

    - at each position save the
        + longest increasing path starting here
        + or longest decreasing path starting here
        + build upon the best paths next to us

           chain left  = [2, 5, 7]
           chain right = [2, 3, 4]
           chain above  = [1]
           chain down  = [-2]

           prefix onto chians later than us

"""

from typing import List


class Solution:
    def longestIncreasingPath(self, arr: List[List[int]]) -> int:
        """ At each node, call depth first search 

        - find the longest length chain starting at each position
        using depth first search
        - at each node try to crawl to a larger node with an existing chain
            + current = 1 + longest_chain at larger value

        Time Complexity
            O(m*n)

        Space Complexity
            O(n*m)
        """

        def dfs(row: int, col: int) -> int:

            if memo[row][col] > 0:
                return memo[row][col]

            best = 1

            for dy, dx in dirs:

                row_new = row + dy
                col_new = col + dx

                if 0 <= row_new < n_rows and 0 <= col_new < n_cols and arr[row_new][col_new] > arr[row][col]:

                    best = max(best, dfs(row_new, col_new) + 1)

            memo[row][col] = best

            return best

        if not arr:
            return 0

        n_rows: int = len(arr)
        n_cols: int = len(arr[0])
        ans: int = 0

        dirs = [(0, 1), (0, -1), (1, 0), (-1, 0)]

        memo = [[0 for col_ix in range(n_cols)] for row_ix in range(n_rows)]

        for row_ix in range(n_rows):
            for col_ix in range(n_cols):

                ans = max(ans, dfs(row_ix, col_ix))

        return ans


if __name__ == '__main__':

    # Example 1
    grid = [
          [9,9,4],
          [6,6,8],
          [2,1,1]
        ] 

    # Example 2
    # grid = [
    #       [3,4,5],
    #       [3,2,6],
    #       [2,2,1]
    #     ] 

    obj = Solution()
    print(obj.longestIncreasingPath(grid))
