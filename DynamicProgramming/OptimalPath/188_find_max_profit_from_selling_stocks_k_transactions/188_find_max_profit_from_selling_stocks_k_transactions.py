"""
Inputs
    k (int): number of trades
    prices (List [int]): stock prices
Outputs
    int: maximum profit possible
Notes
    - must sell our stock before we can trade again
    - we cannot engage in multiple transactions at once
    - find the max profit possible with k transactions

Examples

    Example 1:

        Input: k = 2, prices = [2,4,1]
        Output: 2
        Explanation: Buy on day 1 (price = 2) and sell on day 2 (price = 4), profit = 4-2 = 2.

    Example 2:

        Input: k = 2, prices = [3,2,6,5,0,3]
        Output: 7
        Explanation: Buy on day 2 (price = 2) and sell on day 3 (price = 6), profit = 6-2 = 4. Then buy on day 5 (price = 0) and sell on day 6 (price = 3), profit = 3-0 = 3.

Cases

    - all positive

        ie: [1, 3, 5, 7]

    - all negative

        ie: [10, 9, 8, 7]

    - mixed

    - not enough days for sales
        k > n


Ideas
    - track number of transactions used

    - track min so far
        + we need to reinitialize our min after making a sale

    - track max profit so far

    - dynamic programming?
        + on every day we have two options
            * buy stock
            * sell stock

            * buy stock can only be done when no stock in hand
            * sell stock can only be done with stock in hand

    - try applying the two transaction strategy k times

        + compute the max profit so far from left to right (transaction 1)

            * buying before day i, and selling on or before day i

        + combine with the max profit from i to j

Hand Calc

    Example 2

        index  = [0,1,2,3,4,5]
        prices = [3,2,6,5,0,3]
        k = 2

        index = 0
        profit_here = NA
        min_so_far = 3

        index = 1
        profit_here = 2 - 3 = -1
        min_so_far = 2

        index = 2
        profit_here = 6 - 2 = 4
        min_so_far = 2

References:
    - https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/discuss/135704/Detail-explanation-of-DP-solution
    - https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iv/discuss/54117/Clean-Java-DP-solution-with-comment    

"""

from typing import List


class Solution:

    def intuition(self, k: int, prices: List[int]) -> int:
        """

        - save the max profit for every number of transactions
        and sales on day ix

        Time Complexity
            O(n^2*k)
        Space Complexity
            O(n*k)
        """

        n: int = len(prices)

        # Max profits
        # column = max profit for selling on day i
        # row = number of transactions
        dp = [[0 for _ in range(n)] for _ in range(k + 1)]

        for n_trans in range(1, k + 1):

            # Try every buy and sell day combination
            # combine that with the max profit with 1 less transaction
            # where we sold before this buy date

            for ix_buy in range(n - 1):
                for ix_sell in range(ix_buy + 1, n):

                    if ix_buy > 0:
                        profit_pre = max(dp[n_trans - 1][:ix_buy])
                    else:
                        profit_pre = 0

                    profit_cur = prices[ix_sell] - prices[ix_buy]
                    dp[n_trans][ix_sell] = max(dp[n_trans][ix_sell], profit_pre + profit_cur)

        return max(dp[k])

    def intuition2(self, k: int, prices: List[int]) -> int:
        """Improved intuition

        - save the max profit for every number of transactions
        and sales on day ix

        Time Complexity
            O(n^2*k)
        Space Complexity
            O(n*k)
        """

        n: int = len(prices)

        # Max profits
        # column = max profit for selling on day i
        # row = number of transactions
        dp = [[0 for _ in range(n)] for _ in range(k + 1)]

        for n_trans in range(1, k + 1):

            # Try every buy and sell day combination
            # combine that with the max profit with 1 less transaction
            # where we sold before this buy date
            for ix_sell in range(1, n):

                profit_pre = 0

                for ix_buy in range(ix_sell):

                    if ix_buy > 0:
                        profit_pre = max(profit_pre, dp[n_trans - 1][ix_buy - 1])

                    profit_cur = prices[ix_sell] - prices[ix_buy]
                    dp[n_trans][ix_sell] = max(dp[n_trans][ix_sell], profit_pre + profit_cur)

        return max(dp[k])


if __name__ == '__main__':

    # Example 1 (2)
    k = 2
    prices = [2, 4, 1]

    # Example 2 (7)
    k = 2
    prices = [3, 2, 6, 5, 0, 3]

    obj = Solution()
    print(obj.intuition(k, prices))
    print(obj.intuition2(k, prices))
