"""
Inputs
    A (List[int]):
    B (List[int]):
Outputs
    int: minimum swaps needed to make both arrays increasing

Notes
    - we can only swap elements at the same index
        + ie: A[i] with B[i]

    - At the end of swaps we want the arrays strictly increasing
    - It is guaranteed that the given input always makes it possible.

Example

    Example:
        Input: A = [1,3,5,4], B = [1,2,3,7]
        Output: 1
        Explanation: 
        Swap A[3] and B[3].  Then the sequences are:
        A = [1, 3, 5, 7] and B = [1, 2, 3, 4]
        which are both strictly increasing.
"""

class Solution:
    def minSwap(self, A: List[int], B: List[int]) -> int:

        self.cache = {}
        return self.get_min_swaps(A, B, 0, float('-inf'), float('-inf'))

    def get_min_swaps(self, A, B, index, prevA, prevB) -> int:
        """Try both routes

        - if values don't need to be swapped move forward
        - if swap would be valid, swap them and move forward

        - track the minimum swap at each interface

        Time Complexity
            O(2^n)
        Space Complexity
            O(n)
        References:
            https://leetcode.com/problems/minimum-swaps-to-make-sequences-increasing/discuss/262241/Recursive-with-memoization-(beats-100.00-):
        """

        # Made it to the end
        if ix == len(A):
            return 0

        key = (ix, swap_state)

        if key in self.cache:
            return self.cache[key]

        min_swaps = float('inf')

        if A[ix] > prevA and B[ix] > prevB:
            swaps = self.get_min_swaps(A, B, ix + 1, A[ix], B[ix], False)
            min_swaps = min(min_swaps, swaps)

        if B[ix] > prevA and A[ix] > prevB:
            swaps = self.get_min_swaps(A, B, ix + 1, B[ix], A[ix], True) + 1
            min_swaps = min(min_swaps, swaps)

        min_swaps = min(min_swaps, swaps)

        self.cache[key] = min_swaps

        return min_swaps
