"""
Inputs
    arr (List[int]): money at each house
Outputs
    int: maximum money possible
Notes
    - we cannot rob adjacent houses
    - houses are ordered in a circle so
    the last house is adjacent to the first house

Ideas
    - dynamic programming approach
        + each house can be robbed or skipped
        + same approach as before

    - for houses near the end of the array we have a special
    wraparound case

        + ix = n - 1, we cannot choose ix = 1 or vice versa, 
            * bound our search range: 0 to n - 1 (exclusive)

        + ix = n - 2
            * if we start 1, we can go to the end
"""

class Solution:
    def rob(self, arr: List[int]) -> int:
        """
        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        n: int = len(arr)

        if not arr:
            return 0

        if n == 1:
            return arr[0]

        prior1: int = 0
        prior2: int = 0
        max_here: int = 0

        # Start from first house
        # so we cannot also rob the last house
        for ix in range(0, n - 1):

            rob_here = arr[ix] + prior2
            skip_here = prior1
            max_here = max(skip_here, rob_here)

            # Propogate
            prior2 = prior1
            prior1 = max_here

        # Overall max
        max_over = max_here

        # Start from second house
        # so we can rob the last house

        prior1 = 0
        prior2 = 0
        for ix in range(1, n):

            rob_here = arr[ix] + prior2
            skip_here = prior1
            max_here = max(skip_here, rob_here)

            prior2 = prior1
            prior1 = max_here

        max_over = max(max_here, max_over)

        return max_over
