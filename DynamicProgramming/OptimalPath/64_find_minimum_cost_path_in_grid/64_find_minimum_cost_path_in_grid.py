"""

Cases

    Inputs

        grid

Strategy

    Dynamic Programming (bottom up)

        - minimum path is dependent on previous positions (d)

            arr[row_ix][col_ix] = grid[row_ix][col_ix] + min(up, left)


"""

from typing import List


class Solution:

    def minPathSum(self, grid: List[List[int]]) -> int:
        """Path is dependent on previous minimum paths we've calculated

        dynamic programming approach
        """

        # Get array bounds
        n_rows = len(grid)
        n_cols = len(grid[0])

        # Initialize array
        arr = [[0 for col_ix in range(n_cols)] for row_ix in range(n_rows)]

        # Iterate through the array
        for row_ix in range(n_rows):
            for col_ix in range(n_cols):

                # Compute current uniques = top position + left position
                # cur_val = grid[row_ix][col_ix]

                # Grab minimum path from up and left
                if row_ix - 1 >= 0 and col_ix - 1 >= 0:
                    arr[row_ix][col_ix] = grid[row_ix][col_ix] + min(arr[row_ix - 1][col_ix], arr[row_ix][col_ix - 1])

                # Only left path exists
                elif col_ix - 1 >= 0:
                    arr[row_ix][col_ix] = grid[row_ix][col_ix] + arr[row_ix][col_ix - 1]
                
                # Only top path exists
                elif row_ix - 1 >= 0:
                    arr[row_ix][col_ix] = grid[row_ix][col_ix] + arr[row_ix - 1][col_ix]
                
                else:
                    arr[row_ix][col_ix] = grid[row_ix][col_ix]

        return arr[row_ix][col_ix]

    def inplace(self, grid: List[List[int]]) -> int:
        """Save values in place since we don't need other previous values"""

        # Get array bounds
        n_rows = len(grid)
        n_cols = len(grid[0])

        # Iterate through the array
        for row_ix in range(n_rows):
            for col_ix in range(n_cols):

                # Compute current uniques = top position + left position
                # cur_val = grid[row_ix][col_ix]

                # Grab minimum path from up and left
                if row_ix - 1 >= 0 and col_ix - 1 >= 0:
                    grid[row_ix][col_ix] = grid[row_ix][col_ix] + min(grid[row_ix - 1][col_ix], grid[row_ix][col_ix - 1])

                # Only left path exists
                elif col_ix - 1 >= 0:
                    grid[row_ix][col_ix] = grid[row_ix][col_ix] + grid[row_ix][col_ix - 1]

                # Only top path exists
                elif row_ix - 1 >= 0:
                    grid[row_ix][col_ix] = grid[row_ix][col_ix] + grid[row_ix - 1][col_ix]

        return grid[row_ix][col_ix]

    def inplace_two_fors(self, grid: List[List[int]]) -> int:
        """Faster runtime since no if statements to check"""

        # Get array bounds
        n_rows = len(grid)
        n_cols = len(grid[0])

        # Must initialize for case where only a single column or row
        row_ix = 0
        col_ix = 0

        # Iterate through the first row (only left priors)
        for col_ix in range(1, n_cols):
            grid[0][col_ix] = grid[0][col_ix] + grid[0][col_ix - 1]

        # Iterate through the first column (only top priors)
        for row_ix in range(1, n_rows):
            grid[row_ix][0] = grid[row_ix][0] + grid[row_ix - 1][0]

        # Iterate through the rest of the array
        for row_ix in range(1, n_rows):
            for col_ix in range(1, n_cols):

                # Grab minimum path from up and left
                grid[row_ix][col_ix] = grid[row_ix][col_ix] + min(grid[row_ix - 1][col_ix], grid[row_ix][col_ix - 1])

        return grid[row_ix][col_ix]

    def inplace_two_fors(self, grid: List[List[int]]) -> int:
        """No if statements and used python shorthand"""

        # Get array bounds
        n_rows = len(grid)
        n_cols = len(grid[0])

        # Must initialize for case where only a single column or row
        row_ix = 0
        col_ix = 0

        # Iterate through the first row (only left priors)
        for col_ix in range(1, n_cols):
            grid[0][col_ix] += grid[0][col_ix - 1]

        # Iterate through the first column (only top priors)
        for row_ix in range(1, n_rows):
            grid[row_ix][0] += grid[row_ix - 1][0]

        # Iterate through the rest of the array
        for row_ix in range(1, n_rows):
            for col_ix in range(1, n_cols):

                # Grab minimum path from up and left
                grid[row_ix][col_ix] +=  min(grid[row_ix - 1][col_ix], grid[row_ix][col_ix - 1])

        return grid[row_ix][col_ix]


if __name__ == "__main__":

    s = Solution()

    grid_in = [[1,3,1], [1,5,1], [4,2,1]]

    # print(s.minPathSum(grid_in))
    print(s.inplace(grid_in))
