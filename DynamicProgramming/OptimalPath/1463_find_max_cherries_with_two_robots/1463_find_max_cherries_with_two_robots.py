"""
Inputs
    grid (List[List[int]]): cherries location at each position
Outputs
    int: maximum number of cherries possible
Notes

    - robot 1 starts at top left
    - robot 2 starts at top right
    - from a cell, robots can move to 3 options
        + (i+1, j - 1) down and left
        + (i+1, j    ) down
        + (i+1, j + 1) down and right

    - when any robot passes through, it picks up all cherries and the
    cell becomes zero
    + only one robot can pick up cherries from a cell if on the same cell
    + robots must reach the bottom most row

Ideas

    - try both options and grab max
        + find best path with robot 1, then have robot 2 go
        + find best path with robot 2, then have robot 1 go
        + have robots alternate turns
            + 1st try, r1 first
            + 2nd try, r2 first

    - when choosing thest best path, have tiebreakers
        + have the rightmost robot choose right paths
        when counts are equal
        + similarly have the left most robot prefer the left path

    - if using one robot through the entire grid, then
    we need to trace which path the first robot took, since
    this will change the cherries for the second robot

    - greedy approach at each step likely will not work,
    we need to track the best option
        + example 2 showcases that
        + track the max cherries at each position based upon it's priors

    - use dynamic programming to build from bottom up
    but track both robots
        + add an extra dimension for position of robot 2 (row, column 1, column 2)
        + max @ location = R1 prior + R2 prior + current

            current = R1 cherries + R2 cherries if different locations
            current = R1 cherries if robots at the same location

    - robots are restricted to how far they can go
        + left robot: right most postion is row_ix
        + right robot: left most position is n_cols - 1 - row_ix

References:
    - https://leetcode.com/problems/cherry-pickup-ii/discuss/688218/C%2B%2B-DP-Bottom-up-solution-memory-efficient-(explained)
    - https://leetcode.com/problems/cherry-pickup-ii/discuss/660867/Python3-DFS-with-Memo-easy-to-understand

"""

from typing import List

''' Top Down '''

class Solution:
    def cherryPickup(self, grid: List[List[int]]) -> int:

        n_rows: int = len(grid)
        n_cols: int = len(grid[0])

        self.max_ans: int = 0

        # robots can move down, then left, right or straight
        count = grid[0][0] + grid[0][n_cols - 1]
        self.recurse(0, 0, n_cols - 1, count, grid)

        return self.max_ans

    def recurse(self, r: int, c1: int, c2: int, count: int, grid):
        """

        Args:
            r (int): row
            c1 (int): robot 1 column
            c2 (int): robot 2 column
            count (int): number of cherries so far
            grid (List[List][int]): 2D grid

        Notes
            - this is too slow
            - we can use memo to prevent repeated work
        """

        dirs = [(1, -1), (1, 0), (1, 1)]
        n_rows: int = len(grid)
        n_cols: int = len(grid[0])

        self.max_ans = max(self.max_ans, count)

        # Try all directions for robot 1
        for dr, dc1 in dirs:

            r_n = r + dr
            c1_n = c1 + dc1

            # Try all directions for robot 2
            if r_n < n_rows and 0 <= c1_n < n_cols:
                for dr2, dc2 in dirs:

                    c2_n = c2 + dc2

                    if 0 <= c2_n < n_cols:

                        if c1_n == c2_n:
                            self.recurse(r_n, c1_n, c2_n, count + grid[r_n][c1_n], grid)
                        else:
                            self.recurse(r_n, c1_n, c2_n, count + grid[r_n][c1_n] + grid[r_n][c2_n], grid)

''' Bottom Up '''

class Solution:
    def single_robot(self, grid: List[List[int]]) -> int:
        """Find best path for a single robot"""

        n_rows: int = len(grid)
        n_cols: int = len(grid[0])

        # Max cherries at each position
        arr = [[0 for col_ix in range(n_cols)] for row_ix in range(n_rows)]
        arr[0][0] = grid[0][0]

        for row_ix in range(1, n_rows):
            for col_ix in range(n_cols):

                count_max = 0

                for row_pre, col_pre in self.get_priors(row_ix, col_ix):
                    if 0 <= row_pre < n_rows and 0 <= col_pre < n_cols:

                        count_max = max(count_max, arr[row_pre][col_pre])

                # Save our best prior + current cherries
                arr[row_ix][col_ix] = count_max + grid[row_ix][col_ix]

        raise NotImplementedError()

    def cherryPickup2(self, grid: List[List[int]]) -> int:
        """Find the best past for two robots

        - find the max cherries possible from our robots paths
        - build upon the previous best each time
        - note that our robots can only go so far right, or left
            + keep this in mind to reduce work

        Time Complexity
            O(n*m*m)
        Space Complexity
            O(n*m*m)

        Notes
            - we only need the previous row for each iteration.
            We can reduce the matrix from 3D to 2D
        """

        n_rows: int = len(grid)
        n_cols: int = len(grid[0])
        max_global: int = 0

        # Max cherries at each position
        arr = [[[0 for col_ix in range(n_cols)] for col_ix in range(n_cols)] for row_ix in range(n_rows)]

        # Start robots at their respective locations
        if n_cols == 1:
            arr[0][0][n_cols - 1] = grid[0][0]
        else:
            arr[0][0][n_cols - 1] = grid[0][0] + grid[0][n_cols - 1]

        for row_ix in range(1, n_rows):
            for c1 in range(min(n_cols, row_ix + 1)):
                for c2 in range(max(0, n_cols - 1 - row_ix), n_cols):

                    count_max = 0

                    for row_pre, c1_pre, c2_pre in self.get_priors2(row_ix, c1, c2):
                        if 0 <= row_pre < n_rows and 0 <= c1_pre < n_cols and 0 <= c2_pre < n_cols:

                            count_max = max(count_max, arr[row_pre][c1_pre][c2_pre])

                    print(f' row: {row_ix}   c1: {c1}   c2: {c2}   count_max: {count_max}')

                    # Robots can't pick up the same cherries
                    if c1 != c2:
                        current = grid[row_ix][c1] + grid[row_ix][c2]
                    else:
                        current = grid[row_ix][c1]

                    # Save our best prior + current cherries
                    arr[row_ix][c1][c2] = count_max + current

        # Find the best from the last row
        for c1 in range(n_cols):
            for c2 in range(n_cols):
                max_global = max(max_global, arr[n_rows - 1][c1][c2])

        return max_global

    def get_priors1(self, row: int, col: int):
        """Gather priors from a single robots position"""
        dirs = [(-1, 0), (-1, -1), (-1, 1)]

        for dy, dx in dirs:
            yield row + dy, col + dx

    def get_priors2(self, row: int, col1: int, col2: int):
        """Gather priors from a two robots position

        R1 is at position 1
            R2 is at postion 1
            R2 is at postion 2
            R2 is at postion 3
        R1 is at position 2
            R2 is at postion 1
            R2 is at postion 2
            R2 is at postion 3

        etc..

        Args:
            row (int): current row for both robots
            col1 (int): column for robot 1
            col2 (int): column for robot 2
        """
        dirs = [0, -1, 1]

        for dx1 in dirs:
            for dx2 in dirs:
                yield row - 1, col1 + dx1, col2 + dx2

if __name__ == '__main__':

    # Example 1
    grid = [[3,1,1],[2,5,1],[1,5,5],[2,1,1]]

    # Example 2
    # grid = [[1,0,0,0,0,0,1],[2,0,0,0,0,3,0],[2,0,9,0,0,0,0],[0,3,0,5,4,0,0],[1,0,2,3,0,0,6]]

    # Example 3
    # grid = [[1,0,0,3],[0,0,0,3],[0,0,3,3],[9,0,3,3]]

    # Example 4
    # grid = [[1,1],[1,1]]

    # Row 1 has many cherries w ecan't get
    # grid = [[4,1,5,7,1],[6,0,4,6,4],[0,9,6,3,5]]

    # Failed case
    # grid = [[0,8,7,10,9,10,0,9,6],[8,7,10,8,7,4,9,6,10],[8,1,1,5,1,5,5,1,2],[9,4,10,8,8,1,9,5,0],[4,3,6,10,9,2,4,8,10],[7,3,2,8,3,3,5,9,8],[1,2,6,5,6,2,0,10,0]]

    obj = Solution()
    # obj.cherryPickup(grid)
    print(obj.cherryPickup2(grid))
