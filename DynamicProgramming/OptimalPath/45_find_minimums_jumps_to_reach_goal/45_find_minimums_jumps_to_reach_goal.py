"""

Inputs:
    arr (list[int]): maximum jump at the current position
Outputs:
    int: minimum number of jumps required to reach the end

Goals
    - Your goal is to reach the last index in the minimum number of jumps.
"""


import math
from typing import List

class Solution():

    ''' O(n^2) '''

    def jump(self, nums: List[int]) -> int:
        """Determine the Minimum number of jumps to reach the last position

        - work backwards for the working position
        - at each index determine the minimum jumps required to reach the working position

        Args:
            nums (List[int]): max hop available at each position
        Returns:
            int: minimum number of jumps to reach end

        Space Complexity
            O(n)
        Time Complexity
            O(n^2)

        Times:
            Times out => too slow
        """

        self.arr = nums
        n = len(nums)

        # We're at the last position
        if n <= 1:
            return 0

        # Hops needed to reach the last position
        jumps: List[int] = [math.inf] * n
        jumps[-1] = 0

        # Iterate from last position to first position
        for ix in range(n - 2, -1, -1):

            # Jump capability
            # ie: dist = 4
            dist: int = self.arr[ix]

            # Find minimum number of jumps necessary to reach end
            # that is within range
            min_jump = math.inf

            # Try each jump distance
            # 4, 3, 2, 1
            for jx in range(dist, 0, -1):

                # We can reach the end
                if ix + jx >= n - 1:
                    min_jump = 0
                    break
                else:
                    min_jump = min(min_jump, jumps[ix + jx])

            # Save minimum jumps necessary
            jumps[ix] = min_jump + 1

        # Save minimum number of jumps
        return jumps[0]

    def backwards(self, arr: List[int]) -> int:
        """Iterate backwards from a position we know works
        find the leftmost position that works

        - At position n - 1 we don't need any jumps
        - find the left most position that can jumpt to n -1

        - mark this new position as our position requirement
        - find the left most position that can make it to this new spot
        - each time we jump, update the jump count

        Time Complexity
            O(n^2)
        Space Complexity
            O(1)

        Notes:
            - still too slow
        """

        n: int = len(arr)

        # We're already at the end
        if n <= 1:
            return 0

        count: int = 0           # number of jumps taken
        working_ix: int = n - 1  # position we know gets us to the end

        while working_ix != 0:

            for ix in range(working_ix):

                # We can make it to the end from here
                if ix + arr[ix] >= working_ix:
                    count += 1
                    working_ix = ix
                    break

        return count

    ''' Linear Time '''

    def greedy(self, arr: List[int]) -> int:
        """

        - crawl the current ladder until we hit it's end
            + track ladders that allow us to get to the highest possible
            position
            + save the best back up ladder we find

        - after our current ladder ends jump to our best back up ladder
            + increment the count

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        n: int = len(arr)

        # We're already at the end
        if n <= 1:
            return 0

        count: int = 1       # number of jumps taken
        end_current_ladder: int = 0 + arr[0]  # farthest index we can reach on current ladder
        end_backup_ladder: int = 0   # farthest index we can reach from back up ladder

        print(f'end_current_ladder: {end_current_ladder}')
        print(f'end_backup_ladder: {end_backup_ladder}')

        # Try jumps at each step
        for ix in range(1, n):

            # We can make it to the end
            if end_current_ladder >= n - 1:
                return count

            # Location we can jump to from here
            # ix = 0, jump = 4 => get to 4
            current_jump = ix + arr[ix]

            # Track the ladder that gets us the furthest
            end_backup_ladder = max(end_backup_ladder, current_jump)

            print(f'  pos: {ix}  current_jump: {current_jump}  end_current_ladder: {end_current_ladder}  end_backup_ladder: {end_backup_ladder}')

            # We've reached the end of our current ladder
            if ix == end_current_ladder:

                print(f' switching')

                # Jump to our backup ladder
                count += 1
                end_current_ladder = end_backup_ladder

        return count


if __name__ == '__main__':

    s = Solution()

    # No positions
    # case = []

    # Single position
    # case = [1]

    # Double position
    # case = [2, 1]

    # Triple position
    # case = [4, 1, 2]

    # General case: 2 jumps to reach end
    case = [2, 3, 1, 1, 4]

    # print(s.jump(case))
    print(s.greedy(case))

"""
    ix = 4
    ind   = [0, 1, 2, 3, 4]
    arr   = [2, 3, 1, 1, '4']
    jumps = [0, 0, 0, 0, 0]

    ix = 3
    ind   = [0, 1, 2, 3, 4]
    arr   = [2, 3, 1, '1', 4]
    jumps = [0, 0, 0, 1, 0]

    ix = 2
    ind   = [0, 1, 2, 3, 4]
    arr   = [2, 3, '1', 1, 4]
    jumps = [0, 0, 2, 1, 0]

    ix = 1
    ind   = [0, 1, 2, 3, 4]
    arr   = [2, '3', 1, 1, 4]
    jumps = [0, 0, 2, 1, 0]

    ix = 0
    ind   = [0, 1, 2, 3, 4]
    arr   = ['2', 3, 1, 1, 4]
    jumps = [0, 0, 2, 1, 0]
"""
