"""
Inputs:
    grid (List[List[int]]): 2D grid of cherries and thorns
Outputs:
    int: max number of cherries possible
Notes:
    - grid can have 3 possible values
        + thorn: -1
        + open: 0
        + cherry: +1
    - we can move
    - starting and ending locations are guaranteed to not be -1
    - grid is n by n, 1 <= n <= 50
Ideas
    - dynamic programming
        + find max path to bottom
        + determine the best path to take
        + modify those cells to zero
        + then find the best path back to start while considering
        new cherries to pick up

    - best path down, may not allow be part of the best total path

    - reframe the problem
        + if this was a maximum sum, we could start at the bottom and go up
        or start at the top and go down, both should result in the same answer

        + think of this as 2 separate people going from the start to the goal
        one after the other

            [S  2  2]
            [1  x  2]
            [1  x  2]
            [1  1  G]

            * we can both take the same path, or take two different paths
            * we can't pick up the same cherries twice

    - the number of steps taken for either path is restricted
        + the manhattan distance for both should be equal
        + we have to cover the same number of steps to reach the goal
        + https://en.wikipedia.org/wiki/Taxicab_geometry

        r1 + c1 = constant
        r2 + c2 = constant

        r1 + c1 = r2 + c2

        so r2 = r1 + c1 - c2
"""

from typing import List


class Solution:

    ''' Single Robot: Brainstorming '''

    def cherryPickup_single(self, grid: List[List[int]]) -> int:

        n_rows: int = len(grid)
        n_cols: int = len(grid[0])

        ''' Find best paths to bottom right'''

        dp = [[0 for c in range(n_cols)] for r in range(n_rows)]

        for row_ix in range(n_rows):
            for col_ix in range(n_cols):

                max_prev = 0

                if grid[row_ix][col_ix] != -1:
                    for row_pre, col_pre in self.get_priors_down(row_ix, col_ix):

                        if 0 <= row_pre < n_rows and 0 <= col_pre < n_cols and grid[row_pre][col_pre] != -1:
                            max_prev = max(max_prev, dp[row_pre][col_pre])

                    dp[row_ix][col_ix] = grid[row_ix][col_ix] + max_prev

        ''' Determine which path to take '''
        # for row_ix in range(n_rows - 1, -1, -1):
        #     for col_ix in range(n_cols, -1, -1):
        #         pass

        # return dp[n_rows - 1][n_cols - 1]
        raise NotImplementedError()

    def get_priors_down(self, row_ix: int, col_ix: int):
        """Grab previous locations"""

        dirs = [(-1, 0), (0, -1)]

        for dy, dx in dirs:
            yield row_ix + dy, col_ix + dx

    ''' Double Robot '''

    def cherryPickup(self, grid: List[List[int]]) -> int:
        """Find the maximum number of cherries we can pick up

        - track the most cherries possible at each position
        - assume 2 paths going from top to bottom
        - use the manhattan distance restriction
        to reduce dimensionality
            ie: r2 = r1 + c1 - c2

        Time Complexity
            O(n*m*m)
        Space Complexity
            O(n*m*m)

        Notes
            - could iterate our on constant defined
                r + c = constant
            - passes 90% of test cases 
        """

        n_rows: int = len(grid)
        n_cols: int = len(grid[0])

        ''' Find best paths to bottom right'''

        dp = [[[-1 for c1 in range(n_cols)] for c2 in range(n_cols)] for r1 in range(n_rows)]

        for r1 in range(n_rows):
            for c1 in range(n_cols):

                if grid[r1][c1] == -1:
                    continue

                for c2 in range(n_cols):

                    r2 = r1 + c1 - c2

                    if r2 < 0 or r2 >= n_rows or grid[r2][c2] == -1:
                        continue

                    # Find best of prior paths
                    max_prev = 0

                    for r1_pre, c1_pre, c2_pre in self.get_priors_down2(r1, c1, c2):

                        r2_pre = r1_pre + c1_pre - c2_pre

                        if 0 <= r1_pre < n_rows and 0 <= c1_pre < n_cols and 0 <= r2_pre < n_rows and 0 <= c2_pre < n_cols:
                            max_prev = max(max_prev, dp[r1_pre][c1_pre][c2_pre])

                            # print(f' robot1_pre: {r1_pre},{c1_pre}   robot_2_pre: {r2_pre},{c2_pre}')

                    # No valid previous path
                    if max_prev == 0 and r1 > 0 and c1 > 0:
                        continue

                    # We can only pick the same cherry up once
                    # r2 = r1 when c1 = c2
                    if c1 == c2:
                        current = grid[r1][c1]
                    else:
                        current = grid[r1][c1] + grid[r2][c2]

                    dp[r1][c1][c2] = current + max_prev

                    # print(f'robot1: {r1},{c1}   robot2: {r2},{c2}   r1_c: {grid[r1][c1]}   r2_c: {grid[r2][c2]}')

        return dp[n_rows - 1][n_cols - 1][n_cols - 1]

    def get_priors_down2(self, row1_ix: int, col1_ix: int, col2_ix: int):
        """Grab previous locations for two robots"""

        dirs = [(-1, 0), (0, -1)]

        for dy, dx in dirs:
            for dy2, dx2 in dirs:
                yield row1_ix + dy, col1_ix + dx, col2_ix + dx2


if __name__ == '__main__':

    # Example 1
    # grid = [[0, 1, -1], [1, 0, -1], [1, 1,  1]]

    # Example made up
    # grid = [
    #     [0, 1, +0, 1], 
    #     [0, 1, -1, 1], 
    #     [1, 0, -1, 1]]

    # Impossible case
    grid = [[1, 1, -1],
            [1, -1, 1],
            [-1, 1, 1]]

    # Impossible case
    grid = [[1,-1,1,-1,1,1,1,1,1,-1],[-1,1,1,-1,-1,1,1,1,1,1],[1,1,1,-1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1,1,1],[-1,1,1,1,1,1,1,1,1,1],[1,-1,1,1,1,1,-1,1,1,1],[1,1,1,-1,1,1,-1,1,1,1],[1,-1,1,-1,-1,1,1,1,1,1],[1,1,-1,-1,1,1,1,-1,1,-1],[1,1,-1,1,1,1,1,1,1,1]]

    obj = Solution()
    print(obj.cherryPickup(grid))
