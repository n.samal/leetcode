"""
Inputs
    word (List[str]): list of words, each word consists of English lowercase letters
Outputs
    int: length of the longest word chain
Goal
    Let's say word1 is a predecessor of word2 if and only if we can add exactly one letter
    anywhere in word1 to make it equal to word2.  
    
    For example, "abc" is a predecessor of "abac".
    
    A word chain is a sequence of words [word_1, word_2, ..., word_k] with k >= 1,
    where word_1 is a predecessor of word_2, word_2 is a predecessor of word_3, and so on


Examples

    Example 1
        Input: ["a","b","ba","bca","bda","bdca"]
        Output: 4
        Explanation: one of the longest word chain is "a","ba","bda","bdca".

Ideas
    - create a hash map of words
        + key = length of each word
        
    - the next word in our word chain will be of length + 1
        + next_word_len = cur_word_len + 1
        + we still need to check if next word has correct
        characters and
            
            * try splitting the potential word at each character, if
            the left substring + right substring is a key in our dict
            we've got a possibility
            
            ie: bdca
        
        + we could alternatively work backwards, start with the largest word
            search for smaller words
            
    - use edit distance between words
        + if edit distance is 1 via insertion then we've got a possbility
        + could be too complicated
Strategies

    Dynamic Programming
        
        - create an empty hash table
            + key = word
            + value = chain length
        
        - sort our words from smallest length to largest length
        
        - for each word, try splitting the word at each character
            + if our split word exists in our dp table, then extend that chain
            + if no previous chain to use, then just start a chain here
            ie: len 1
        
        - keep track of the longest chain we can find
        
        Time Complexity
            
            O(n*log*n + n*m*m)
            
            sorting O(n*log(n))
            splitting each n words at each m characters
                build a new character of m length

        Space Complexity
            O(n)

"""

class Solution:
    def longestStrChain(self, words: List[str]) -> int:
        """Dynamic Programming approach
        
        - organize words by their length
        - iterate through smallest words first
        - try splitting the current word in pieces
            + search for chains that include words that are
            only on character different
            + if no previous chain, chain is length 1
            + if previous chain we just added 1 word
        """
        # Null case
        if not words:
            return 0
        
        # Sort words by length
        words = sorted(words, key=len)
        
        max_global: int = 0
        dp = {}
        
        # Try extending a chain with the current word
        for word in words:
            
            # Try ignoring each character in the current word
            # ie: bdca -> dca, bca, bda, dbc
            # Extend the length of the longest previous chain
            # if no previous chians, then the chain is of length 1
            
            max_prev_chain = 0
            for ix in range(len(word)):
                split_word = word[:ix] + word[ix+1:]
                
                max_prev_chain = max(dp.get(split_word, 0) + 1, max_prev_chain)
            
            # Save the current chain length
            dp[word] = max_prev_chain
                
            # Save our global max
            max_global = max(max_prev_chain, max_global)
            
        return max_global
