"""
Inputs
    word1 (str): starting word
    word2 (str): target word
Outputs
    int: find the minimum number of operators to convert word1 to word2
Goals
    - 3 options are permitted
        + insert a character
        + delete a character
        + replace a character

Examples

    Example 1
        Input: word1 = "horse", word2 = "ros"
        Output: 3
        Explanation: 
        horse -> rorse (replace 'h' with 'r')
        rorse -> rose (remove 'r')
        rose -> ros (remove 'e')

    Example 2

        Input: word1 = "intention", word2 = "execution"
        Output: 5
        Explanation: 
        intention -> inention (remove 't')
        inention -> enention (replace 'i' with 'e')
        enention -> exention (replace 'n' with 'x')
        exention -> exection (replace 'n' with 'c')
        exection -> execution (insert 'u')

Ideas

    - use top down
        + try all three routes and save the best route
        + use memoization to save time

"""


class Solution:

    ''' Top Down '''

    def top_down(self, word1: str, word2: str) -> int:
        """Top down without memo

        Time Complexity
            O(3^c)

            3 routes for each of c characters in source/target word
            c = max(m, n)

        Space Complexity
            O(n)

        """

        n_src: int = len(word1)
        n_tgt: int = len(word2)

        def helper(ix: int = 0, jx: int = 0, cost: int = 0):

            # Match characters
            while ix < n_src and jx < n_tgt and word1[ix] == word2[jx]:
                ix += 1
                jx += 1

            # Source word complete
            # -> both done
            # -> or just add the remaining target chars
            if ix >= n_src:

                if jx >= n_tgt:
                    return cost
                else:
                    return cost + (n_tgt - jx)

            # Target word complete, delete the remaining source chars
            elif jx >= n_tgt:
                return cost + (n_src - ix)

            # Modification necessary
            else:

                # Insert a character to match
                route_1 = helper(ix, jx + 1, cost)

                # Replace character to match target
                route_2 = helper(ix + 1, jx + 1, cost)

                # Delete a character in source word
                route_3 = helper(ix + 1, jx, cost)

                min_route = min(route_1, route_2, route_3) + 1

                return min_route

        return helper()

    def top_down_memo(self, word1: str, word2: str) -> int:
        """Top down with memo

        Time Complexity
            O(n * m)
        Space Complexity
            O(n*m)
        """

        n_src: int = len(word1)
        n_tgt: int = len(word2)
        self.memo = [[None for _ in range(n_tgt + 1)] for __ in range(n_src + 1)]

        def helper(ix: int = 0, jx: int = 0, cost: int = 0):

            # state = (ix, jx)
            if self.memo[ix][jx]:
                return self.memo[ix][jx]

            # Source word complete
            # -> both done
            # -> or just add the remaining target chars
            if ix >= n_src:

                if jx >= n_tgt:
                    return cost
                else:
                    return cost + (n_tgt - jx)

            # Target word complete, delete the remaining source chars
            elif jx >= n_tgt:
                return cost + (n_src - ix)

            # Match characters
            if word1[ix] == word2[jx]:
                min_cost = helper(ix + 1, jx + 1, cost)

            # Modification necessary
            else:

                # Try all routes
                #   1) Insert a character to match
                #   2) Replace character to match target
                #   3) Delete a character in source word    
                route_1 = helper(ix, jx + 1, cost)
                route_2 = helper(ix + 1, jx + 1, cost)
                route_3 = helper(ix + 1, jx, cost)

                min_cost = min(route_1, route_2, route_3) + 1

            # Save for later
            self.memo[ix][jx] = min_cost

            return min_cost

        return helper()

    ''' Bottom up '''

    def bottom_up(self, word1: str, word2: str) -> int:
        """Bottom up

        - same approach as top down with memo
        - if characters match, continue the prior cost
        - if no match, then use the best option from previous
        string comparisons
            + insertion
            + deletion
            + replace

        Time Complexity
            O(n * m)

            n = chars in word1
            m = chars in word2

        Space Complexity
            O(n * m)
        Notes
            - use hints from top down with memo
            - slower because we go through all comparisons
        """

        n_src: int = len(word1)
        n_tgt: int = len(word2)

        # Empty strings
        if not n_src and n_tgt:
            return n_tgt

        if not n_tgt and n_src:
            return n_src

        dp = [[float('inf') for _ in range(n_tgt + 1)] for __ in range(n_src + 1)]

        ''' Base cases '''
        dp[0][0] = 0  # match on null vs null

        # target = null (all deletions)
        for ix in range(1, n_src + 1):
            dp[ix][0] = ix

        # source = null (all insertions)
        for jx in range(1, n_tgt + 1):
            dp[0][jx] = jx

        ''' Compare strings '''

        for ix in range(1, n_src + 1):
            for jx in range(1, n_tgt + 1):

                # Continue prior cost
                if word1[ix - 1] == word2[jx - 1]:
                    dp[ix][jx] = dp[ix - 1][jx - 1]

                # Apply an edit function to bring to current state
                # use the minimum cost from prior adjacencies
                else:
                    dp[ix][jx] = min(dp[ix - 1][jx], dp[ix][jx - 1], dp[ix - 1][jx - 1]) + 1

        return dp[-1][-1]


if __name__ == '__main__':

    # Example 1
    # word1 = "horse"
    # word2 = "ros"

    # Example 2
    word1 = "intention"
    word2 = "execution"

    # Null case
    # word1 = ""
    # word2 = "a"

    # Tiny case
    # word1 = "sea"
    # word2 = "eat"

    obj = Solution()
    print(obj.top_down(word1, word2))
    print(obj.top_down_memo(word1, word2))
    print(obj.bottom_up(word1, word2))
