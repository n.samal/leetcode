"""
Inputs
    nums(List[int]): money at each house
Outputs
    int: maximum amount of money we can rob

Notes
    - each house a certain amount of money
    - we cannot rob two ajacent houses

Examples

    - Example 1:

        Input: nums = [1,2,3,1]
        Output: 4
        Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
                     Total amount you can rob = 1 + 3 = 4.

    Example 2:

        Input: nums = [2,7,9,3,1]
        Output: 12
        Explanation: Rob house 1 (money = 2), rob house 3 (money = 9) and rob house 5 (money = 1).
                     Total amount you can rob = 2 + 9 + 1 = 12.

Ideas

    - at each step we can rob the house or not rob the house
        + if we rob then we couldn't have robbed the previous house 
        pos - 1, but we could have robbed the house before pos - 2

        skip_here =  1 house prior
        rob_here = 2 houses prior + here

    - use dynamic programming
        + we don't need all values, just the last two numbers
        + almost like fibonacii 
"""

class Solution:
    def rob(self, nums: List[int]) -> int:
        """

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        prior1 = 0
        prior2 = 0
        max_here = 0

        for val in nums:

            # Rob the house or skip it
            cur_house_rob = val + prior2
            cur_house_skip = prior1

            max_here = max(cur_house_rob, cur_house_skip)

            # Save values for next iterations
            prior2 = prior1
            prior1 = max_here

        return max_here
