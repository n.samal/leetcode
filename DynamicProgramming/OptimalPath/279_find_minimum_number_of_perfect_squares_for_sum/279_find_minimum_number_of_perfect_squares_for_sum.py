"""
Inputs
    int: positive integer
Outputs
    int: least number of perfect square numbers that sum to n
Notes
    - find the least number of perfect square numbers 
    - an example of perfect squares (1, 4, 9, 16, ...)

Examples

    Example 1:

        Input: n = 12
        Output: 3 
        Explanation: 12 = 4 + 4 + 4.

    Example 2:

        Input: n = 13
        Output: 2
        Explanation: 13 = 4 + 9.

Ideas

    - taking the largest perfect square may not work
        + may be better to find other 

    - seems similar to knapsack problem
        + dynamic programming likely way to go
        + build from the bottom up

    - use a perfect square on the sum
    then find the best way to find the remaining amount
        + if we build from bottom up, we always have
        the remaining amount

"""

from math import sqrt


class Solution:

    def numSquares(self, n: int) -> int:
        """Bottom up approach

        - for each value from 1 to n
        - find the minimum perfects squares needed to compute
        the value

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        Notes
            - on the slow end. try precomputing squares
        """

        # Minimum squares needed for each value
        # base case of 0
        arr = [float('inf') for _ in range(n + 1)]
        arr[0] = 0

        # Try building each target value with
        # previous computations
        # ie: 100 - 1^2, 100 - 2^2, 100 - 3^2
        for val in range(1, n + 1):
            for base in range(1, n + 1):

                rem = val - base**2

                if rem < 0:
                    break

                arr[val] = min(arr[val], arr[rem] + 1)

        return arr[n]

    def numSquares2(self, n: int) -> int:
        """Bottom up approach with precompute

        - for each value from 1 to n
        - find the minimum perfects squares needed to compute
        the value

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)
        Notes
            - no significant time boost
        """

        # Minimum squares needed for each value
        # base case of 0
        arr = [float('inf') for _ in range(n + 1)]
        arr[0] = 0

        squares = [v**2 for v in range(1, int(sqrt(n)) + 1)]

        # Try building each target value with
        # previous computations
        # ie: 100 - 1^2, 100 - 2^2, 100 - 3^2
        for val in range(1, n + 1):
            for square in squares:

                rem = val - square

                if rem < 0:
                    break

                arr[val] = min(arr[val], arr[rem] + 1)

        return arr[n]


if __name__ == '__main__':

    # Example 1
    n = 12

    # Example 2
    n = 13

    # Example 2
    # n = 1

    obj = Solution()
    print(obj.numSquares(n))
