"""
Inputs
    word (str): word to type
Outputs
    int: minimum total distance to type

Notes
    - we have a keyboard mapped as a 2D grid
    - we want to type out a word using the minimum number of movements

    - distance between coordinates
        + distance = |x1 - x2| + |y1 - y2|
    - the two initial positions can be at any position
    - the fingers do not have to the start at the first letter
    or second two letters
Examples

    Example 1

        Input: word = "CAKE"
        Output: 3
        Explanation:
        Using two fingers, one optimal way to type "CAKE" is:
        Finger 1 on letter 'C' -> cost = 0
        Finger 1 on letter 'A' -> cost = Distance from letter 'C' to letter 'A' = 2
        Finger 2 on letter 'K' -> cost = 0
        Finger 2 on letter 'E' -> cost = Distance from letter 'K' to letter 'E' = 1
        Total distance = 3

    Example 2
        Input: word = "HAPPY"
        Output: 6
        Explanation:
        Using two fingers, one optimal way to type "HAPPY" is:
        Finger 1 on letter 'H' -> cost = 0
        Finger 1 on letter 'A' -> cost = Distance from letter 'H' to letter 'A' = 2
        Finger 2 on letter 'P' -> cost = 0
        Finger 2 on letter 'P' -> cost = Distance from letter 'P' to letter 'P' = 0
        Finger 1 on letter 'Y' -> cost = Distance from letter 'A' to letter 'Y' = 4
        Total distance = 6

    Example 3
        Input: word = "NEW"
        Output: 3

    Example 4
        Input: word = "YEAR"
        Output: 7

Ideas
    - create a dictionary of locations for each character
    - distance to type is summation on manhattan distance
    from character to character
    - we likely want to start from one of the characters in the string

    - maybe similar to have two robots moving through a path
        + at every step we can choose to type with
        + left finger at one position, the right at another
        + where we move from the current location to the next position

    - could we use the median to minimize the distance to characters?
        + use the median of two groupings

    - track these parameters
        + distance traveled so far
        + current position of left finger
        + current position of right finger
        + index in word to match
References:
    https://leetcode.com/problems/minimum-distance-to-type-a-word-using-two-fingers/discuss/477659/4%2B-DP-Solutions
    https://leetcode.com/problems/minimum-distance-to-type-a-word-using-two-fingers/discuss/812969/Java-3D2D-Top-DownMemoization-DP-Solution
"""


class Solution:

    ''' Utility '''

    def get_keymap(self) -> dict:
        """ Gather map locations of each character """

        dct = {}

        n_cols: int = 6

        for ix, char in enumerate('ABCDEFGHIJKLMNOPQRSTUVWXYZ'):

            div, mod = divmod(ix, n_cols)
            dct[char] = (div, mod)

        return dct

    def distance(self, start: str, stop: str) -> int:
        """Compute manhattan distance from character to character

        - if no start location, then we assume we start at the character
        """

        if not start or start == '?':
            return 0

        else:
            row_start, col_start = self.dct[start]
            row_stop, col_stop = self.dct[stop]

            return abs(row_stop - row_start) + abs(col_stop - col_start)

    def distance2(self, start: str, stop: str) -> int:
        """Compute manhattan distance from character to character

        - simplify distance using only numbers
        """

        if start == 26:
            return 0
        else:
            row_start, col_start = divmod(start, 6)
            row_stop, col_stop = divmod(stop, 6)

            return abs(row_stop - row_start) + abs(col_stop - col_start)

    ''' Main '''

    def min_dist_top_down(self, word: str) -> int:

        self.memo = {}
        self.word = word
        self.dct = self.get_keymap()

        return self.top_down(0, None, None)

    def top_down(self, ix: int, left: str, right: str) -> int:
        """Top down with memo

        - try using the left finger or right figner
        to press the current character
        - grab the minimum distance for any given route

        Args:
            ix: location in main string
            left: location of left finger
            right: location of right finger
        Returns:
            int: minimum distance to travel

        Without Memo
            Time Complexity
                O(2^n)
            Space Complexity
                O(2^n)
        """

        if ix == len(self.word):
            return 0

        if (ix, left, right) in self.memo:
            return self.memo[(ix, left, right)]

        char = self.word[ix]

        # Move finger 1 or finger 2
        route1 = self.top_down(ix + 1, char, right) + self.distance(left, char)
        route2 = self.top_down(ix + 1, left, char) + self.distance(right, char)
        ans = min(route1, route2)

        self.memo[(ix, left, right)] = ans

        return ans

    ''' Bottom Up '''

    def min_dist_3D(self, word: str) -> int:
        """Bottom up with all prior states

        - use 3D array to store
            + location in word
            + left char picked
            + right char picked
        - use one additional state for 'hover' or not selected yet

        Time Complexity
            O(27*27*n)

            n characters in word
            27 options for left finger
            27 options for right finger

        Space Complexity
            O(27*27*n)
        Notes
            - much slower
                + here we try building the new state with every previous state
            - we really only need the previous state, and current state
        """

        self.dct = self.get_keymap()

        # (index in word, left character, right character)
        dp = [[[float('inf') for l in range(27)] for r in range(27)] for ix in range(len(word))]

        for ix in range(len(word)):

            char = word[ix]
            char_ix = ord(char) - ord('A')

            for l, char_l in enumerate('ABCDEFGHIJKLMNOPQRSTUVWXYZ?'):
                for r, char_r in enumerate('ABCDEFGHIJKLMNOPQRSTUVWXYZ?'):

                    # Try moving left finger only, or right finger only from prior state
                    if ix > 0:
                        dp[ix][char_ix][r] = min(dp[ix][char_ix][r], dp[ix - 1][l][r] + self.distance(char_l, char))
                        dp[ix][l][char_ix] = min(dp[ix][l][char_ix], dp[ix - 1][l][r] + self.distance(char_r, char))

                    # No distance for picking first character
                    else:
                        dp[ix][char_ix][r] = 0
                        dp[ix][l][char_ix] = 0

        # Find minimum steps for last index
        min_cost = float('inf')

        for l in range(27):
            for r in range(27):
                min_cost = min(min_cost, dp[-1][l][r])

        return min_cost

    def min_dist_3D_v2(self, word: str) -> int:
        """Bottom up with reduced state space

        - improve the memory by only storing current state,
        and previous state

        Time Complexity
            O(27*27*n)

            n characters in word
            27 options for left finger
            27 options for right finger

        Space Complexity
            O(27*27*2)
        """

        dp_pre = None

        for ix in range(len(word)):

            dp_cur = [[float('inf') for l in range(27)] for r in range(27)]
            char_ix = ord(word[ix]) - ord('A')

            min_cost = float('inf')

            for l in range(27):
                for r in range(27):

                    # Try moving left finger only, or right finger only from prior state
                    if dp_pre:
                        dp_cur[char_ix][r] = min(dp_cur[char_ix][r], dp_pre[l][r] + self.distance2(l, char_ix))
                        dp_cur[l][char_ix] = min(dp_cur[l][char_ix], dp_pre[l][r] + self.distance2(r, char_ix))

                    # No distance for picking first character
                    else:
                        dp_cur[char_ix][r] = 0
                        dp_cur[l][char_ix] = 0

                    min_cost = min(min_cost, dp_cur[char_ix][r], dp_cur[l][char_ix])

            dp_pre = dp_cur

            print(f'{word[ix]}: {min_cost}')

        return min_cost


if __name__ == "__main__":

    # Example 1: 3
    # word = 'CAKE'

    # Example 2: 6
    word = 'HAPPY'

    obj = Solution()
    print(obj.min_dist_top_down(word))
    print(obj.min_dist_3D(word))
    print(obj.min_dist_3D_v2(word))
