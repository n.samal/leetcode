"""
Inputs
    root (TreeNode)
Outputs
    int: maximum amount of money possible

Notes
    - police will be contacted if two houses that are robbed
    are directly linked

Ideas
    - same principle as previous versions
        - rob current house or skip current house

        - rob current = current + prior2
        - skip current = prior1

    - crawl down tree and aggregate counts from subtrees
        + our starting point must be from the bottom, instead
        of from the top

        so we can aggregate subcounts from leaves
        otherwise paths are only considered individually

            1
          2   4
         3     5


		starting from top considers
			path 1 to 3 (1 + 3 or 2)
			path 1 to 5 (1 + 5 or 4)

			individually

		wheres aggregating from the bottom considers 

		2 + 4
		  or
		1 + 3 + 5
"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def rob(self, root: TreeNode) -> int:
        """Recurse and rob

        - we aggregate from the bottom up, to ensure
        that all potential houses are visitied... or robbed =D

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        prior1, prior2 = self.recurse(root)
        return max(prior1, prior2)

    def recurse(self, root):

        if root:

            prior1_l, prior2_l = self.recurse(root.left)
            prior1_r, prior2_r = self.recurse(root.right)

            # If we rob here we gotta skip the previous house
            rob_here = root.val + prior2_l + prior2_r
            skip_here = prior1_l + prior1_r

            max_here = max(skip_here, rob_here)

            # max_here -> prior1
            # prior1   -> prior2

            return max_here, skip_here

        else:
            return 0, 0
