"""
Inputs
    triangle (List[List[int]]): cost at each node
Ouputs:
    int: minimum cost to reach the bottom row

Ideas
    - seems similar to minimum/max path in a grid
        + instead of left and top both paths
        are from the row above
        + adjacent to left or adjacent to the right

    - Adjacent Index Triangle
         [0],
        [0,1],
       [0,1,2],
      [0,1,2,3]

      next adjacents are itself or +1
      previous adjacents are itself, or -1

"""

class Solution:
    def minimumTotal(self, triangle: List[List[int]]) -> int:
        """Crawl from the previous min

        - use the minimum cost node from the previous position 
        to move to the current node
        - save the current row so it be used for the next iteration

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        n_rows: int = len(triangle)

        # Base cases
        if n_rows == 0:
            return 0
        elif n_rows == 1:
            return triangle[0][0]

        # Save the first row
        prev_row = triangle[0][:]
        prev_n = 1

        # Iterate over triangle
        for row_ix in range(1, n_rows):

            n_cols = len(triangle[row_ix])
            cur_row = [float('inf') for _ in range(n_cols)]

            for col_ix in range(n_cols):

                min_adder = float('inf')

                # Grab the minimum cost from previous adjacents
                # if they exist
                if col_ix - 1 >= 0:
                    min_adder = min(min_adder, prev_row[col_ix - 1])

                if col_ix <= prev_n - 1:
                    min_adder = min(min_adder, prev_row[col_ix])

                cur_row[col_ix] = triangle[row_ix][col_ix] + min_adder

            # Save for next iteration
            prev_row = cur_row
            prev_n = n_cols

        return min(cur_row)
