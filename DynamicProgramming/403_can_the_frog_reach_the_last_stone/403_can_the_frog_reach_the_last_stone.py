"""
Inputs
    stones (List[int]): position of stones in units
Outputs
    bool: can we reach the final stone

Notes
    - starting stone is always 0
    - each stone is a positive integer
    - stones are given in sorted ascending order
    - if the current jump is k units the next jump can be
        + k - 1 units
        + k units
        + k + 1 units
    - the first jump must be 1 unit

Examples
    Example 1
        [0,1,3,5,6,8,12,17]

        There are a total of 8 stones.
        The first stone at the 0th unit, second stone at the 1st unit,
        third stone at the 3rd unit, and so on...
        The last stone at the 17th unit.

        Return true. The frog can jump to the last stone by jumping 
        1 unit to the 2nd stone, then 2 units to the 3rd stone, then 
        2 units to the 4th stone, then 3 units to the 6th stone, 
        4 units to the 7th stone, and 5 units to the 8th stone.

    Example 2

        [0,1,2,3,4,8,9,11]

        Return false. There is no way to jump to the last stone as 
        the gap between the 5th and 6th stone is too large.

Ideas
    - it benefits use to take large leaps when possible
        + at each stone find the largest k that can be used
        to get there

        + this is not correct, because k is not a range, but
        just an option.
        + we need exact numbers to reach specific stones
        + we want to use every k possible to reach a stone

    - if we can't reach the first stone, it's not possible
        + 1st jump is always of unit 1
        + and we always have more than 2 stones

    - use depth first search
        + this would work but could be very expensive

            with out memory
                time: O(3^n)
                space: O(n) recursion tree height
            with memory
                time:  O(n^3)
                space: O(n^2)

                at each index we save the result of each k used

        + need to track what k was used for each step

    - build a map of k values used to reach each stone
        + an array of unit positions would easy to reference

        ie: index 0 = unit 0
            index 1 = unit 1

        + an array would be sparse for very big stone gaps
            ie: [0, 1, 9999999999]

            instead of 9999999999 entries we use a set to
            store 3
"""

from typing import List


class Solution:
    def canCross_old(self, stones: List[int]) -> bool:
        """Original

        - create an empty array of unit locations
        - from the first stone, try each possible jump
            + mark the max K that was used to reach each stone
            + we only mark stone positions
                * water is marked as -1
                * stones we cannot reached are left as 0

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        Notes
            - we need every K, not just the max K!
        """
        last_stone: int = stones[-1] + 1

        # Create an array of unit positions
        # save the max k used to get to each position
        arr = [-1 for ix in range(last_stone)]

        # Mark all stone positions as valid
        for stone in stones:
            arr[stone] = 0

        # If a stone exists at 1
        # then our first jump is of k=1
        if arr[1] == 0:
            arr[1] = 1

        for ix in range(1, last_stone):

            print(f'arr: {arr}')

            # No stone, or haven't reached here
            if arr[ix] <= 0:
                continue

            # Try all jumps possible
            # k-1, k, k + 1
            for k in range(arr[ix] - 1, arr[ix] + 2):
                pos_new = ix + k

                # Save the best k used to reach that stone
                if pos_new < last_stone and arr[pos_new] != -1:
                    arr[pos_new] = max(arr[pos_new], k)

        print(f'arr: {arr}')

        # Did we get to the last stone
        return arr[-1] >= 1

    def canCross(self, stones: List[int]) -> bool:
        """All k stored at each stone

        - create an empty hash of unit locations
        - only store the stone locations
            + all stones initially marked as empty
            + everything else is water
        - from the first stone, try each possible jump
            + add the all Ks that were used to reach each stone
            + we only mark stone positions

        Time Complexity
            O(n^2)

            at every unit, we potentially need to try all k

        Space Complexity
            O(n^2)

            at every unit, we potentially need to try all k

        Notes
            - logic apppears to match solution, yay!
            - using a set instead of list to save appending time
        """
        last_stone: int = stones[-1]

        # Create an hash map of unit positions
        # save all k used to get to each position
        # Mark all stone positions as valid
        dct = {stone: set() for stone in stones}

        # If a stone exists at 1
        # then our first jump is of k=1
        # otherwise we can't go anywhere
        if 1 in dct:
            dct[1].add(1)
        else:
            return False

        for ix in stones[:-1]:

            print(f'dct: {dct}')

            k_arr = dct.get(ix, set())

            # Try all jumps possible
            for k_base in k_arr:

                # Try all positive jump variations
                # k-1, k, k + 1
                for k in range(k_base - 1, k_base + 2):

                    if k > 0:
                        pos_new = ix + k

                        # Save all k used to reach a stone if present
                        if pos_new in dct:
                            dct[pos_new].add(k)

        print(f'dct: {dct}')

        # Did we get to the last stone
        return len(dct[last_stone]) > 0


if __name__ == '__main__':

    # Example 1
    # stones = [0, 1, 3, 5, 6, 8, 12, 17]

    # Example 2
    # stones = [0, 1, 2, 3, 4, 8, 9, 11]

    # Failed case
    stones = [0, 1, 3, 6, 10, 13, 15, 18]

    obj = Solution()
    print(obj.canCross(stones))
