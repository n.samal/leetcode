def top_down(string: str, ix: int = 0, prior: str = '') -> list:
    """Find all possible combinations

    n = characters with 3 choices
    m = characters with 4 choices

    Time Complexity
        O(3^n * 4^m)
    Space Complexity
        O(3^n * 4^m)

    Args:
        string (str): numbers
        ix (int): character pointer in string
        prior (str): current combination
    Returns:
        list: possible letter mappings
    References:
        https://leetcode.com/problems/letter-combinations-of-a-phone-number/
    """

    # Pointer beyond string length
    if ix > len(string) - 1:
        return [prior]

    # print('ix:', ix, 'str:', string[ix], 'prior:', prior)

    # Conversion dictionary
    table = {'2': ['a', 'b', 'c'],
             '3': ['d', 'e', 'f'],
             '4': ['g', 'h', 'i'],
             '5': ['j', 'k', 'l'],
             '6': ['m', 'n', 'o'],
             '7': ['p', 'q', 'r', 's'],
             '8': ['t', 'u', 'v'],
             '9': ['w', 'x', 'y', 'z'],
             }

    # Possible combinations
    arr: list = []

    # Number has valid letters
    if string[ix] in table:

        # Iterate through strings
        for char in table[string[ix]]:

            arr += top_down(string, ix + 1, prior + char)

    return arr


if __name__ == '__main__':

    # s = "23"
    # s = "29"
    s = "298"

    print(top_down(s))
