from typing import List


class Solution(object):

    def find_valid_groups(self, arr: List[int], target: int) -> List[List[int]]:
        """Find all groups that sum to the target value

        Repetition of values allowed

        Args:

        Returns:
            List[List[int]]: all valid groups that sum to the target value

        """

        self.groups = []

        # Sort array
        arr = sorted(arr)

        # Start our search
        self.top_down(arr, [], target)

        return self.groups

    def top_down(self, choices: List[int], path: List[int], cur_sum: int, choice_ix: int = 0):
        """Recursively crawl through all choices for our current grouping

        Args:
            choices: available options for grouping
            path: choices used in current path / set
            cur_sum: summation left to gather
            choice_ix: index of explored choices
        Returns:
            List[int]
        """

        # We've reach our target summation, save it!
        if cur_sum == 0:
            self.groups.append(path)

        # Only explore paths with greater numbers
        # the smaller valued numbers will include the greater numbers in their path
        for ix in range(choice_ix, len(choices)):

            num = choices[ix]

            # Value is too big
            if num > cur_sum:
                break
            else:
                # Try this path
                self.top_down(choices, path + [num], cur_sum - num, ix)


if __name__ == "__main__":

    s = Solution()

    print(s.find_valid_groups([2, 3, 6, 7], target=7))
