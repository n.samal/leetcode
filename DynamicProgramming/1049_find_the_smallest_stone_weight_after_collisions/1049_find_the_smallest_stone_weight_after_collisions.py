"""
Inputs
    stones (List[int]): weight of each stone
Outputs
    int: find the smallest weight
Goal
    - after only 1 stone remains find the smallest stone weight possible
    - return 0 if no stones left

Notes
    - each turn we choose the any two stones and smash them together
    - if x == y then both stones are destroyed
    - ix x != y, the smaller stone is totally destryoed
        the remaining stone has weight y - x
    - at the end there is at most 1 stone left

Ideas

    - sort stones from smallest to largest??
    - we may want stone collisions to have some remainder, so we
    can use them to remove another stone later
    - the final stone is a summation of all stones with either a positive
    or negative sign

        ie: arr = [a1, a2, a3]

        a1: (a1 - a2) - a3
        a1: (a1 - a3) - a2

        a2: (a2 - a1) - a3
        a2: (a2 - a3) - a1

        a3: (a3 - a1) - a2
        a3: (a3 - a2) - a1

        we want to find the summation that is smallest

Ideas Other

    The problem can be interpreted as the following:

    Divide the stones into two groups such that the difference
    of the sum of weights of each group is minimum.

    For the problem at the description:
        [2,7,4,1,8,1]

    Those groups could be:
        Group 1: 1,1,2,7 (sum is 11)
        Group 2:     4,8 (sum is 12)

        Difference: 1

    We know the total sum is 23

        if the sum of subset 1 is 11
        we know the sum of subset 2 is 23 - 11 = 12

    Notice the remaining sum can be computed using the groupSum and totalSum

        total - groupSum1 * 2 = 23 - 11 * 2 = 23 - 22 = 1
        total - groupSum2 * 2 = 23 - 12 * 2 = 23 - 24 = -1

    if totalSum is the sum of all stones, a groupSum is a sum of a group
    the difference of the sums of each group is:
        Math.abs(groupSum1 - groupSum2)
        Math.abs((totalSum - groupSum2) - groupSum2)

    we can apply this strategy via dynamic programming for all possible groupSums
    then track the smallest difference when using all stones

    - dynamic programming
        + build minimum stone from the bottom up
        + try apply knapsack style methodology

        Knapsack 

References:
    https://leetcode.com/problems/last-stone-weight-ii/discuss/294881/Another-kind-of-coin-change-problem
    https://leetcode.com/problems/last-stone-weight-ii/discuss/298664/How-is-it-a-Knapsack-problem
    https://leetcode.com/problems/last-stone-weight-ii/discuss/294888/JavaC%2B%2BPython-Easy-Knapsacks-DP
    https://leetcode.com/discuss/interview-question/356433/
"""

from typing import List


class Solution:
    def lastStoneWeightII(self, stones: List[int]) -> int:
        """

        Notes
            - 
        """
        total_sum = sum(stones)
        target_sum = total_sum // 2
        n: int = len(stones)

        # Zero or one stone
        if n == 0:
            return 0
        elif n == 1:
            return stones[0]

        # Build a table similar to knapsack
        # rows: inclusion of stones (null to all)
        # cols: target summation (0 to target sum)
        n_rows = 1 + n
        n_cols = 1 + target_sum

        dp = [[False for col_ix in range(n_cols)] for row_ix in range(n_rows)]

        # Base case: with no items we can only create a target sum of 0
        dp[0][0] = True

        # Base case: if we include any items we can't create a target of 0

        # Try building target summations
        for row_ix in range(1, n_rows):
            for col_ix in range(1, n_cols):

                # Remainder = target_sum - current stone
                rem_sum = col_ix - stones[row_ix - 1]

                # Can we make the target sum with the stone, or without the stone
                if rem_sum >= 0:
                    dp[row_ix][col_ix] = dp[row_ix - 1][rem_sum] or dp[row_ix - 1][col_ix]

                # Can we make the same target sum without the current stone
                else:
                    dp[row_ix][col_ix] = dp[row_ix - 1][col_ix]

        # Find the largest summation near half our total sum
        # we can make with all the stones
        for col_ix in range(n_cols - 1, -1, -1):
            if dp[-1][col_ix] is True:
                break

        # Calculate the difference using the largest target summation possible
        diff = abs(total_sum - 2 * col_ix)

        return diff


if __name__ == '__main__':

    arr_in = [2, 7, 4, 1, 8, 1]

    obj = Solution()
    print(obj.lastStoneWeightII(arr_in))
