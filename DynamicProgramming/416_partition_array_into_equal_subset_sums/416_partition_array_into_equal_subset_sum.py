"""
Inputs:
    arr (List[int]): positive integers in unknown order
Outputs:
    bool: can we split the array into two equal subsets
Goals:
    - find if the array can be partitioned into two subsets such that the sum of elements in both subsets is equal.

Ideas
    
    - Our target sum for both subsets is total / 2
        + if our subset sum is greater than that value, we could backtrack
        + if any value is greater than our target sum we can't split it evenly
        + if the 

    - sort our values then use recursive approach
        + backtracking can improve non essential paths
        + memoiziation may improve our complexity
            
            * 

    - dynamic programming
        - from the bottom determine if we can compute the target sum
            + 0 to target value
            + we know we can 

Strategies
    
    Top Down
        
        - recursively try 
            + including the value in Subset 1
            + including the value in Subset 2
            
            + when we have no remaining items and the subset is equal
            then we're done
        
        Time Complexity
            O(2^n)
        Space Complexity
            O(tree height)

References:
    https://leetcode.com/problems/partition-equal-subset-sum/discuss/90592/01-knapsack-detailed-explanation
    https://leetcode.com/problems/partition-equal-subset-sum/discuss/90608/Very-easy-to-understand-C%2B%2B-with-explanation
"""

class Solution:
    def canPartition(self, arr: List[int]) -> bool:
        """

        - for each target sum from 0 to total_sum / 2
        see if we can meet our goal by 
            + including the current item
            + excluding the current item 

        - build a table of our states

        Time Complexity
            O(n * sum(arr))
        Space Complexity
            O(n * sum(arr))

        Notes:
        """

        # Null case
        if not arr:
            return True
        
        n: int = len(arr)
            
        # Determine the total sum
        # Our target will be half the total
        total = sum(arr)
        target_sum = total // 2
        
        # We can only split an array evenly if the total sum is even
        # Positive integers cannot add up to a fractional value
        # ie: [1, 2, 3, 5] => total = 11, target = 11/2 = 5.5
        if total % 2 != 0:
            return False

        # If any item is larger than our target sum we can't split our array evenly
        for val in arr:
            if val > target_sum:
                return False

        # Build a table from the bottom up
        # Can we create a subset sum of 0, 1, to .. n -1
        # rows = including item i (no items to including all items)
        # cols = target sum (0 to target sum )
        n_rows = n + 1
        n_cols = target_sum + 1
        dp = [[False for col_ix in range(n_cols)] for row_ix in range(n_rows)]
        
        # We know we can build a target sum of 0 with no items
        # Column 0 = target sum of 0
        for row_ix in range(n_rows):
            dp[row_ix][0] = True
        
        # We cannot build a target sum greater than 0 with no items
        for col_ix in range(1, n_cols):
            dp[0][col_ix] = False
        
        # Incremently ask this question
        # If we include the item can we get our target sum?
        # If we exclude the item can we still get our target sum?
        for row_ix in range(1, n_rows):
            for col_ix in range(1, n_cols):
                
                # By including the item, we only need part of our target sum
                # previous goal = 10, item = 6, new_goal = 10 - 6 = 4
                inclusion_target = col_ix - arr[row_ix - 1]
                
                if inclusion_target >= 0:
                    dp[row_ix][col_ix] = dp[row_ix - 1][inclusion_target] or dp[row_ix - 1][col_ix]

                # Excluding item is the previous row
                else:
                    dp[row_ix][col_ix] = dp[row_ix - 1][col_ix]
                
        # Grab the boolean for 
        # col = target summation
        # row = including all items
        return dp[-1][-1]
        
