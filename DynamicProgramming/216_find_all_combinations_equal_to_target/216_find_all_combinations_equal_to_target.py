"""
Inputs
    k (int): number of values for summation
    target (int): target summation
Outputs
    List[List[int]]: all valid combinations
Notes
    - find all valid combinations of k digits that sum to the target value
    - values 1 through 9 are used and can only be used once

Ideas
    - use backtracking with markers if necessary
    - track thse
        + number of values used
        + current sum
        + which remaining values are left
        + current path

    + break summation early if summation is larger than
    target
    + break when no more values left to try
"""

class Solution:
    def combinationSum3(self, k: int, target: int) -> List[List[int]]:

        self.combos = []
        self.values = [v for v in range(1, 10)]
        self.n = len(self.values)
        self.target = target

        self.recurse(0, [], 0, k)

        return self.combos

    def recurse(self, ix: int, cur_path: List[int], cur_sum: int, count: int):
        """

        - only travel remaining open values when
            + we have numbers left to add
            + our cur_sum is less than the target

        Args:
            ix: pointer in values
            cur_path: values in the path
            cur_sum: summation of values in path
            count: number of values left to add into combination

        Time Complexity
            O(9 choose k)
            combination of k values from 9 choices

        Space Complexity
            O(k)  max of k values in recursion stack and path
        """

        # Save paths that work
        if count == 0:
            if cur_sum == self.target:
                self.combos.append(cur_path)
            else:
                return

        for jx in range(ix, self.n):

            if cur_sum + self.values[jx] <= self.target:
                self.recurse(jx + 1, cur_path + [self.values[jx]], cur_sum + self.values[jx], count - 1)

            # Don't travel paths bigger than our target                    
            else:
                break
