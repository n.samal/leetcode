"""

Inputs
    List[int]: integers in unknown order
Outputs

Cases
    Null
    Single Value
    General Case

Ideas

    Sorting
        - sort the values
        - compute a cummulative sum array
            + iterate through array and remove values

    Divisible by 3
        - determine the maximum values divisible by 3
            + is the sum of this values divisible by 3 as well?
                appears to be so
            + ie: 3 + 9 = 12
                  3^1 + 3^3 = 3 ^4
            + ie: 27 + 60 = 87
                  3^4 + 3*(20)

            + sum of digits must be divisble by 3 for this to work
                2 + 7 = 9 => divisible
                6 + 0 = 6 => divisible
                8 + 7 = 15 => divisible
            + this won't work for all cases...
                * some cases will require combining values
         
                Example 1: nums = [3,6,5,1,8]

                largest divisible by 3 => 3, 6
                we need 3, 6, (1+8)
    Divisble Buckets

        - compute rem = value % 3
        - store the value in one of 3 buckets
            + rem = 0
            + rem = 1
            + rem = 2

        - with rem =0, we can just use these to compute a summation
            + these values can be summed and will be divisible by 3

        - for the remaining values we can combine them

            + grab 3 values from rem1
            + grab 3 values from rem2
            + grab 1 value from rem 1, 1 from rem2

    Divisible Buckets 2
        - store values into 2 buckets:
            rem 1
            rem 2
        - compute total sum of values
            - if there's 1 remainder then, remove the smallest 1 remainder value
            - if there's 2 remainder then, remove the smallest 2 remainder value



Example

    Example 3
        nums = [1, 2, 3, 4, 4]
        cum_sum = [1, 3, 6, 10, 14]

        try removing, smallest value
            14 - 1 = 13
            14 - 2 = 12

            => works

Strategies

    Sort & Sum
        - sort the array from smallest to largest
        - compute the sum or cummulative sum for each value

        - see if the cummulative sum is divisible by 3

            + if so we're done
            + if not, try removing the values
                * starting with the smallest number
                * check the sum at each step
    Math
        - find the largest values divisible by 3 and sum them?

    Dynamic Programming
        - use divisible buckets combined with dynamic programming

        - if the value is divisible by 3:
            remainder 0: keep it in bucket index 0
            remainder 1: keep it in bucket index 1
            remainder 2: keep it in bucket index 2

        - instead of storing values, we'll store summations
            index 0: max total with remainder 0
            index 1: max total with remainder 1
            index 2: max total with remainder 2

        - these max totals are functions of each other
            index 0:
                    subtotal_0 + value with remainder 0
                    or
                    subtotal_1 + value with remainder 2
                    or
                    subtotal_2 + value with remainder 1

        - whenever we encounter a new value, we have can keep it or ignore it
        if we choose to keep it, this will change our remainder type

            + sub_total_new = sub_total_i + value

            ie:
                arr = [3, 6, 5, 1, 8]
                buckets = [9, 0, 0]
                value = 5

                if we try adding 5 to each subtotal

                b0: 9 + 5 = 14 => remainder of 2
                b1: 0 + 5 = 5 => remainder of 2
                b2: 0 + 5 = 5 => remainder of 2

        + we want to maximize the values in each particular bucket

            + only keep the value if it improves our summation

            ie:
                adding 5 to our first bucket improves our remainder 2
                subtotal, we keep that as our best subtotal with a remainder
                of 2

        Reference:
        https://leetcode.com/problems/greatest-sum-divisible-by-three/discuss/439097/Javascript-and-C%2B%2B-solutions
        https://leetcode.com/problems/greatest-sum-divisible-by-three/discuss/431785/C%2B%2B-Commented-DP-solution-that-actually-makes-sense.

"""

import heapq as hq
from typing import List


class Solution:

    def heap(self, arr: List[int]) -> int:
        """Find the three largest values divisible by three

        - store all the divisible values in a heap
        - track the largest divisible values
        - output their sum

        Time Complexity
            O(n(log(3)))
        Space Complexity
            O(1)

        Notes
            - this won't work for all cases. We need to consider
            combining numbers with remainders of 1, and numbers
            with remainders of 2
        """

        # Empty heap
        three = []

        for val in arr:

            # It's divisble by three
            if val % 3 == 0:

                if len(three) < 3:
                    hq.heappush(three, val)

                elif val > three[-1]:
                    hq.heapreplace(three, val)

        # Sum values in array
        if three:
            return sum(three)
        else:
            return 0

    def buckets(self, arr: List[int]) -> int:
        """Find the three largest values divisible by three

        - compute the total sum
        - if the value is divisible by 3, done
        - if remainder is 1 remove one of the 1 remainder numbers
        - if remainder is 2 remove one of the 2 remainder numbers

        Time Complexity
            O(n)
        Space Complexity
            O(1)

        Notes
            - this won't work for all cases
                ie: [2, 2, 2, 6, 7]
                answer = 15, method => 12
        """

        total: int = 0
        rem1 = float('inf')
        rem2 = float('inf')

        for val in arr:

            total += val

            rem = val % 3

            # Save the smallest remainder values
            if rem == 1:
                rem1 = min(val, rem1)

            elif rem == 2:
                rem2 = min(val, rem2)

        total_rem = total % 3

        # Remove values with the smallest remainder
        if total_rem == 0:
            return total
        elif total_rem == 1:
            return total - rem1
        elif total_rem == 2:
            return total - rem2
        else:
            return 0

    def bottom_up(self, arr: List[int]) -> int:
        """Bottom up tabulation

        - track the best total summation for values with
        each remainder type

        Best subtotal with
        [rem = 0, rem = 1, rem = 2]

        - For each subtotal try adding the current value, and see
        if it improves our best summation for a particular remainder

        - at the end just grab the maximal value with no remainder

        Time Complexity
            O(3n)
        Space Complexity
            O(1)

        Notes:
            this method can be expanded to values divisble by an
            arbitrary number
        """

        # Bucket for each type of remainder
        buckets = [0, 0, 0]

        for val in arr:

            # Copy the bucket
            tmp = buckets[:]

            # Add the value to each bucket and see how
            # the subtotal turns out
            for ix, bucket in enumerate(tmp):

                total = bucket + val
                rem = total % 3

                # Store the subtotal that maximizes
                # the value in our current remainder slot
                buckets[rem] = max(buckets[rem], total)

        # Max sub divisble by 3
        return buckets[0]


if __name__ == "__main__":

    # arr = [3, 6, 5, 1, 8]
    # arr = [4]
    # arr = [1, 2, 3, 4, 4]

    arr = [2, 6, 2, 2, 7]

    obj = Solution()
    # print(obj.maxSumDivThree(arr))
    # print(obj.buckets(arr))
    print(obj.bottom_up(arr))
