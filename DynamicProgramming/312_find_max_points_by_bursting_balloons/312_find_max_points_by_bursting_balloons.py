"""
Inputs
    arr (List[int]): balloons indexed from 0 to n -1
Outputs
    int: find the max coins you can collect by bursting balloons
Notes
    - we need to burst all balloons
    - each time we burst a balloon you get coins 

        n_coins = balloon_left * balloon_i * balloon_right

    - balloon_left and balloon_right are adjacent balloons
        + if no adjacents exists, then just multiply by 1
Example

    Input: [3,1,5,8]
    Output: 167 
    Explanation: nums = [3,1,5,8] --> [3,5,8] -->   [3,8]   -->  [8]  --> []
                 coins =  3*1*5      +  3*5*8    +  1*3*8      + 1*8*1   = 167

Ideas

    - array seems like obvious representation
        + this would get expensive when removing elements

            O(n) after each removal

        + pointers may not be possible

            arr = [3,1,5,8,4,2]

            if we pop 1 then 8, how to handle that?

    - the order of operations is important

        + this will change the landscape

    - seems like we want keep our large valued balloons
    for as long as possible
        + these will be used in later products eventually

    - dynamic programming seems like approach to use
        + 

References:
    https://leetcode.com/problems/burst-balloons/discuss/76263/My-readable-Python-~500ms-accepted-solution-with-explanation
"""

class Solution:

    def maxCoins(self, nums: List[int]) -> int:
