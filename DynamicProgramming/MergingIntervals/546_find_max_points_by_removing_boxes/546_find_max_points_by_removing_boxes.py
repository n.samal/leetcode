"""
Inputs
    boxes (List[int]): boxes with different colors represented by differnet values
Outputs
    int: maximum number of points
Notes
    - each round you can choose continous boxes
    with the same label/color (k >= 1)
    - when you remove k boxes you get k*k points
    - what is the maximum number of points

Examples

    Input: boxes = [1,3,2,2,2,3,4,3,1]
    Output: 23
    Explanation:
    [1, 3, 2, 2, 2, 3, 4, 3, 1]               Start
    ----> [1, 3, 3, 4, 3, 1] (3*3=9 points)   Removed three 2s
    ----> [1, 3, 3, 3, 1] (1*1=1 points)      Removed one   4
    ----> [1, 1] (3*3=9 points)               Removed three 3s
    ----> [] (2*2=4 points)                   Removed two   1s

Ideas
    - dynamic programming the way to go
        + utilize a state space defined as
            start
            stop
            similar boxes to the left
        + refer to notes on the right

References
    https://leetcode.com/problems/remove-boxes/discuss/101310/Java-top-down-and-bottom-up-DP-solutions
    https://leetcode.com/problems/remove-boxes/discuss/714434/Java-DP-solution-faster-than-99.77
    https://leetcode.com/problems/remove-boxes/discuss/101328/Java-Preprocessing-DFS-%2B-Memoization-less-space-needed
"""

from typing import List


class Solution:
    def removeBoxes(self, boxes: List[int]) -> int:
        """Top down approach

        - state space
            i = starting interval (inclusive)
            j = ending interval   (inclusive)
            k = number of similar boxes to left as box_i

        Time Complexity
            O(n^3)
        Space Complexity
            O(n^3)

        """

        n: int = len(boxes)

        dp = [[[-1 for i in range(n)] for j in range(n)] for k in range(n)]

        def recurse(start: int, stop: int, k: int) -> int:
            """Crawl the state space

            Args:
                start (int): starting interval range (inclusive)
                stop (int): ending interval range (inclusive)
                k (int): number of similar boxes to the right
            Returns:
                int: max number of points possible
            """

            # print(f'start: {start}   stop: {stop}   k: {k}')

            # Invalid box range
            if start > stop:
                return 0

            # Existing computation
            if dp[start][stop][k] >= 0:
                return dp[start][stop][k]

            # Count the number of continuous similar boxes in our range
            # [1, 1, 1, 1, 2, 2] => 4 similar boxes
            count: int = 1
            index: int = start

            while index + 1 <= stop and boxes[index + 1] == boxes[start]:
                count += 1
                index += 1

            # Compute the total score for this current range
            # assuming no similar boxes to the right
            # score = similar boxes + boxes_to_right
            res = (k + count)**2 + recurse(index + 1, stop, 0)

            # Try combining this group of boxes later with a similar box m, over on the right
            # score = current box + boxes in middle + boxes to right
            for m in range(index + 1, stop + 1):

                if boxes[m] == boxes[start]:

                    boxes_in_middle = recurse(index + 1, m - 1, 0)
                    boxes_to_right = recurse(m, stop, k + count)

                    res = max(res, boxes_in_middle + boxes_to_right)

            # Save the result
            dp[start][stop][k] = res
            return res

        return recurse(0, n - 1, 0)


if __name__ == '__main__':

    # Example 1
    boxes = [1, 3, 2, 2, 2, 3, 4, 3, 1]

    obj = Solution()
    print(obj.removeBoxes(boxes))
