########################
Problem
########################


******************
Initial Definition
******************

Let's define the maximum points gathered from a range as function T(i, j)

	i = index of starting interval (inclusive)
	j = index of ending   interval (inclusive)

To compute the max points for our entire array, we would use
	
	Bounds
		i = 0
		j = n - 1
	Call
		T(0, n -1)

=====
Cases
=====

General Case
	T(i,j)

Single Box
	if j = i, then we have a single box
	the most points we can gather here is 1

Invalid Interval

	- if j < i the interval is invalid
	- we're unable to gather any points from this so T(i, j) = 0

============================
Choosing a box: Now vs Later
============================
If we remove some box in the interval, let's say the first box ie: i = 0, then 

	 0 1               j 
	|x| | | | | | | | | |
	T(i, j) = 1 + boxes to the right
	        = 1 + T(i+1, j)

This however may not be the best decision since there may be some other boxes over
on the right that we could group with box[i]. 

Since get more points for removing groups of boxes it may be smarter to remove this later.
Maybe we can remove this when there is a box of the same color adjacent to us. Let's call that box[m]

Let's leave box[m] alone until we can get it adjacent to i

	 i       m         j 
	| | | | |?| | | | | |

Now we've got 3 box ranges
	
	T(0    , i - 1)
	T(i + 1, m - 1)
	T(m    , j)
	
We can use to function T(i + 1, m - 1) to gather the points from removing boxes in the middle.
To compute the max points possible for the other intervals we need additional information.

We don't know how many of the same boxes occur on the right, and viceversa for the left.

*The current problem definition does not contain all the information, and therefore needs to be reframed*

********************
Reframing Definition
********************

Let's modify our definition to contain all the information we need to calculate the max score

T(i, j, k)
	i = starting interval (inclusive)
	j = ending   interval (inclusive)
	k = number of adjacent similar boxes on the left

We can start our solution in the same manner using
	
	i = 0
	j = n - 1
	k = 0 (no similar boxes on left)

	T(0, n-1, 0)

=====
Cases
=====

Similar to before


----------------------------
No similar boxes to the left
----------------------------

	k = 0, we can only get the similar boxes in our current range
	T(i, j, k=0) 

----------
Single Box
----------

	if j = i, then we have a single box
	the most boxes we can gather here is 1 + the number of boxes to the left of us

	T(i, j=i, k) = (k + 1)^2

----------------
Invalid Interval
----------------

	if j < i the interval is invalid
	we're unable to gather any points from this so T(i, j) = 0
	there are no boxes to attach to the left
	
	T(i, j<i, k) = 0


------------
General Case
------------

	T(i, j, k)

	for any range of boxes we have the option to either
	- remove a box now
	- removal later (in the hopes we can attach it to some similar grouping)

^^^^^^^^^^
Remove Now
^^^^^^^^^^

	If we remove one box located at i, boxes[i] and combine it with those on our left

	our total points, T(i, j, k)

	T(i, j, k) = (k + 1)^2 + boxes to right
               = (k + 1)^2 + T(i + 1, j, k=0)

^^^^^^^^^^^^
Remove Later
^^^^^^^^^^^^

	If we save this boxes[i] for later combination with some other similar color, boxes[m]

	 i       m         j 
	| | | | |?| | | | | |

	T(i, j, k) = boxes from i to m - 1 + boxes from m to j
	           = T(i + 1, m - 1)       + T(m, j, k + 1)

	our box located at i, is now combined with those in the range m to j, so we get that bump in k

	since there are many boxes located in the range m to j, and we don't know exactly where m is
	we try all combinations i < m <= j, and grab the best choice

	we also check whether boxes[i] == boxes[m]


	



########################
References
########################

- https://leetcode.com/problems/remove-boxes/discuss/101310/Java-top-down-and-bottom-up-DP-solutions

