"""
Inputs
    arr (List[List[int]]): binary grid
Outputs
    int: how many square submatrices have all ones

Notes
    - we only want square submatrices!!

Examples

    Example 1:

        Input: matrix =
        [
          [0,1,1,1],
          [1,1,1,1],
          [0,1,1,1]
        ]
        Output: 15
        Explanation: 
        There are 10 squares of side 1.
        There are 4 squares of side 2.
        There is  1 square of side 3.
        Total number of squares = 10 + 4 + 1 = 15.

Ideas
    - try dynamic programming style approach with number of rectangles
        + pre process the array then count
        + ... this may be overkill

    - use approach similar to 221 maximal square
        + count the size of square possible, by using the minimum 
        of all adjacencies

        [1, 1]
        [1, 1]

        compute the largest square ending at (r,c) and coming from the left

    - then just count the number of squares found

"""

class Solution:
    def countSquares(self, arr: List[List[int]]) -> int:
        """Count square sizes possible at each square

        - count the size of square possible at each (r,c)
            + if a large square is possible we add that number of squares

            ie: a square of size 3, could also be of size 1, or size 2 ending there
    
        Time Complexity
            O(r*c)
        Space Complexity
            O(r*c)
        """
        
        n_rows: int = len(arr)
        n_cols: int = len(arr[0])
        
        grid = [[0 for _ in range(n_cols)] for _ in range(n_rows)]
        
        count: int = 0
        
        # Preprocess array
        for r in range(n_rows):
            for c in range(n_cols):
                
                if arr[r][c] == 1:
                    
                    grid[r][c] = 1
                    
                    # Grab square dependencies
                    if r - 1 >= 0 and c - 1 >= 0:
                        grid[r][c] = 1 + min(grid[r - 1][c], grid[r][c - 1], grid[r - 1][c - 1])
                    
                    count += grid[r][c]
        
        # for row in grid:
            # print(row)
    
        return count
                
        