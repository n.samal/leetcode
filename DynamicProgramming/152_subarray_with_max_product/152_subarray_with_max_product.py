"""

Cases

    Negative * Negative = Positive
    Positive * Positive = Positive
    Negative * Positive = Negative
    
Ideas
    
    - our max product may involve a negative times a negative
    - similar to the max sum, track the previous best product
        + track the minimum product previously
        + track the maximum product previously
    
    - the minimum and maximum can be functions of previous values
        + prev_min * current_value
        + prev_max * current_value
        + current value

        if the current value is positive
            we likely want to multiple by the previous max
            
            ie: 9 * 10 = 90
        
        if the current value is negative
            we likely want to multiple by the previous min
            
            ie: -9 * -10 = 90
"""


class Solution:
    def maxProduct(self, arr: List[int]) -> int:
        """Track the current minimum and maximum
        
        - use the 
        
        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """
        n: int = len(arr)
            
        # Single or null case
        if n == 0:
            return 0
        elif n == 1:
            return arr[0]
        
        # Compute the product at each index
        min_prev = arr[0]
        max_prev = arr[0]
        
        max_global = arr[0]
        
        for ix in range(1, n):
            
            # Maximum and minimum here
            max_here = max(arr[ix], max_prev * arr[ix], min_prev * arr[ix])
            min_here = min(arr[ix], max_prev * arr[ix], min_prev * arr[ix])
            
            # Save global max
            max_global = max(max_here, max_global)
            
            # print(f' value: {arr[ix]}    min_prev: {min_prev * arr[ix]}    max_prev: {max_prev * arr[ix]}    global: {max_global}')
            
            # Save previous max and min
            max_prev = max_here
            min_prev = min_here
        
        return max_global
