"""

Cases

    Inputs

        m (number of columns)
            will be positive, 1 
        n (number rows)

            null?

            single column
            single row

            multi row, multi column
Strategy

    Count Array
        - initialize array
            set all values to zeros
            set the starting point, 0, 0 at 1
        - iterate through array

            add counts from previous positions

            arr[row_ix][col_ix] = arr[row_ix - 1][col_ix] (up) + arr[row_ix][col_ix - 1] (left)


"""


class Solution:

    def uniquePaths(self, n_cols: int, n_rows: int) -> int:

        # Initialize array
        arr = [[0 for col_ix in range(n_cols)] for row_ix in range(n_rows)]

        # Set starting position
        if n_cols >= 1 and n_rows >= 1:
            arr[0][0] = 1

        # Iterate through the array
        for row_ix in range(n_rows):
            for col_ix in range(n_cols):

                # Compute current uniques = top position + left position

                # If position to the left exists
                if row_ix - 1 >= 0:
                    arr[row_ix][col_ix] += arr[row_ix - 1][col_ix]

                # If position above exists
                if col_ix - 1 >= 0:
                    arr[row_ix][col_ix] += arr[row_ix][col_ix - 1]

        return arr[row_ix][col_ix]


if __name__ == "__main__":

    s = Solution()

    # Answer = 3
    # print(s.uniquePaths(3, 2))

    # Answer = 3
    # print(s.uniquePaths(7, 3))

    # Single row
    # print(s.uniquePaths(7, 1))

    # Single column
    # print(s.uniquePaths(1, 7))
