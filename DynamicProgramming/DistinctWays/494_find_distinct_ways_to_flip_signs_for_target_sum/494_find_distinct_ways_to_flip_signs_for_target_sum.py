"""
Inputs
    nums (List[int]): non negative integers
    S (int): target sum
Outputs
    int: number of ways to assign symbols and make target sum
Notes
    - all numbers are non-negative integers
    - we can apply + or - symbol to each integer

Examples

    Example 1
        Input: nums is [1, 1, 1, 1, 1], S is 3 
        Output: 5
        Explanation: 

        -1+1+1+1+1 = 3
        +1-1+1+1+1 = 3
        +1+1-1+1+1 = 3
        +1+1+1-1+1 = 3
        +1+1+1+1-1 = 3

        There are 5 ways to assign symbols to make the sum of nums be target 3.
Ideas
    - looks like distinct ways problem
    - bottom up strategy may work
        - build up the ways to calculate each number

    - we need to use all numbers instead of just some like with knapsack
        + our options are - and + of each number, where it can only be used once

    - track these
        + current sum
        + parameters used

    - brute force method
        + try all options
        + time: O(2^n)
"""

from typing import List
from collections import defaultdict


class Solution:

    ''' Top Down '''

    def top_down(self, arr: List[int], S: int) -> int:
        """Top down

            - try adding both versions of each number
            to the current summation

            Time Complexity
                O(2^n)
            Space Complexity
                O(n)  for recursion stack height

            Notes
                - use memo to save time
        """

        def helper(ix: int = 0, cur_sum: int = 0):

            if ix == len(arr):
                if cur_sum == S:
                    self.count += 1
            else:
                helper(ix + 1, cur_sum + arr[ix])
                helper(ix + 1, cur_sum - arr[ix])

        self.count: int = 0
        helper()

        return self.count

    def top_down_memo(self, arr: List[int], S: int) -> int:
        """Top down with memo

            - save good and bad traversals
            - refer to them to prevent repeated work

            Time Complexity
                O(n*s)

                s = range of summation values possible (this is an upperbound, we're unlikely to create all summation values)
                n = length of array

            Space Complexity
                O(n*s)  for storing memo

            Notes
                - review bottom up approach for clarity on time/space complexity
        """

        def helper(ix: int = 0, cur_sum: int = 0):

            if (ix, cur_sum) in self.memo:
                return self.memo[(ix, cur_sum)]

            # Completed traversal
            if ix == len(arr):
                if cur_sum == S:
                    return True
                else:
                    return False

            else:
                route1 = helper(ix + 1, cur_sum + arr[ix])
                route2 = helper(ix + 1, cur_sum - arr[ix])

                # Save counts
                self.memo[(ix, cur_sum)] = route1 + route2

                return self.memo[(ix, cur_sum)]

        self.memo = {}

        return helper()

    ''' Bottom Up '''

    def bottom_up(self, arr: List[int], S: int) -> int:
        """Bottom up

            - use 2D array to save state results
            - similar appraoch as topdown with memo
            - we need to allow space for the entire
            range of possible summations
                + ie: -sum & + sum

            Time Complexity
                O(n*s)

                s = range of summation values possible
                n = length of array

            Space Complexity
                O(n*s)  for storing memo
            Notes
                - use dictionary instead of array for easier referencing
                - likely slower since we iterate across whole range of summations
                - could improve space complexity by only using summations
                at the last index ie: at index 1, we only neeed summations at 0
        """

        sum_tot = sum(arr)
        n: int = len(arr)

        # Store counts for each summation at each index
        dp = defaultdict(int)

        # Base cases
        # use: += to avoid zero case
        dp[(0, +arr[0])] += 1
        dp[(0, -arr[0])] += 1

        for ix in range(1, len(arr)):
            for sum_cur in range(-sum_tot, sum_tot + 1):
                for multiplier in [-1, +1]:

                    sum_new = sum_cur + (multiplier * arr[ix])

                    # Adding ways that were used to create previous summation
                    # ie: 4 ways to make sum of 10 at the last index
                    dp[(ix, sum_new)] += dp[(ix - 1, sum_cur)]

        return dp[(n - 1, S)]


if __name__ == '__main__':

    # Example 1
    # arr = [1, 1, 1, 1, 1]
    # S = 3

    # Failed case
    arr = [0, 0, 0, 0, 0, 0, 0, 0, 1]
    S = 1

    obj = Solution()
    print(obj.top_down(arr, S))
    print(obj.top_down_memo(arr, S))
    print(obj.bottom_up(arr, S))
