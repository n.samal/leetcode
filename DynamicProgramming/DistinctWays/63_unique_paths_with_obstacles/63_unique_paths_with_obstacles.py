"""

Cases

    Inputs

        m (number of columns)
            will be positive, 1 
        n (number rows)

            null?

            single column
            single row

            multi row, multi column
Strategy

    Count Array
        - initialize array
            set all values to zeros
            set the starting point, 0, 0 at 1
        - iterate through array

            add counts from previous positions

            arr[row_ix][col_ix] = arr[row_ix - 1][col_ix] (up) + arr[row_ix][col_ix - 1] (left)

        - the starting position is closed off, no paths available
        - if the ending position is closed off, also no paths available

        Time Complexity
            O(m*n)
        Space Complexity
            O(m*n)

        Note:
            in this solution we only really use the previous row and column
            if we need to save space, we can store only those values

            we could also store our values in the given array
"""
from typing import List


class Solution:
    def uniquePathsWithObstacles(self, grid: List[List[int]]) -> int:

        # Determine boundaries
        n_rows = len(grid)
        n_cols = len(grid[0])

        # Create an path count array
        arr = [[0 for col_ix in range(n_cols)] for row_ix in range(n_rows)]

        # Set starting position if it's open
        if n_cols >= 1 and n_rows >= 1 and grid[0][0] == 0:
            arr[0][0] = 1

        # Iterate through the array
        for row_ix in range(n_rows):
            for col_ix in range(n_cols):

                # Blocked, nothing can go here
                if grid[row_ix][col_ix] == 1:
                    continue

                # Compute current uniques = top position + left position

                # If position to the left exists and grid open
                if row_ix - 1 >= 0:
                    arr[row_ix][col_ix] += arr[row_ix - 1][col_ix]

                # If position above exists and grid open
                if col_ix - 1 >= 0:
                    arr[row_ix][col_ix] += arr[row_ix][col_ix - 1]

        return arr[row_ix][col_ix]

    def inplace(self, grid: List[List[int]]) -> int:
        """Store the values in place
        """
        # Determine boundaries
        n_rows = len(grid)
        n_cols = len(grid[0])

        # Set starting position if it's open, and ending is also open
        if (
            n_cols >= 1
            and n_rows >= 1
            and grid[0][0] == 0
            and grid[n_rows - 1][n_cols - 1] == 0
        ):
            grid[0][0] = 1
        else:
            return 0

        # Iterate through the array
        for row_ix in range(n_rows):
            for col_ix in range(n_cols):

                # Starting position
                if row_ix == 0 and col_ix == 0:
                    continue

                # Blocked, nothing can go here
                if grid[row_ix][col_ix] == 1:

                    # Set paths to zero if blocked, and not the starting point
                    grid[row_ix][col_ix] = 0

                    continue

                # Compute current uniques = top position + left position

                # If position to the left exists and grid open
                if row_ix - 1 >= 0:
                    grid[row_ix][col_ix] += grid[row_ix - 1][col_ix]

                # If position above exists and grid open
                if col_ix - 1 >= 0:
                    grid[row_ix][col_ix] += grid[row_ix][col_ix - 1]

        return grid[row_ix][col_ix]


if __name__ == "__main__":

    s = Solution()

    # grid_in = [[0,0,0],[0,1,0],[0,0,0]]
    # grid_in = [[0, 1]]
    grid_in = [[0, 0], [1, 0]]

    print(s.inplace(grid_in))
