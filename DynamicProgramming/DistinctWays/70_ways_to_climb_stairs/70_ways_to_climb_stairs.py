"""

Question

    How many ways to climb a stair case of n steps?
    We can climb either 1 or 2 steps at once.

Inputs

    n (int): positive integer representing number of stairs to climb

Outputs


Strategy

    Use a dynamic programming type of approach
    Base Cases
        0: 1 way to climb 0 stairs
        1: 1 way to climb (1 step)
        2: 2 ways (1,1 & 2)
        3: (1, 1, 1 | 2, 1 & 1 & 2) 3 ways
    
    Save the logic for each base case, and iterate towards the larger value (n)

    For each step, the count is a function of the previous values

    Save values that we need for those computations

        - use an array of size 0 to n - 1
        - we not need to save all values only the 2 priors (1 and 2 steps less)
"""


class Solution:

    def climbStairs(self, n: int) -> int:
        """Compute the number of steps which are a function
        of previously calculated values

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        Args:
            n (int): number of stairs to climb
        """

        # Create empty array
        arr = [0 for ix in range(n + 1)]

        # Save base cases
        arr[0] = 1

        for ix in range(n + 1):

            print(f'Step: {ix}')

            # Save base case
            if ix == 0:
                arr[ix] = 0
            elif ix == 1:
                arr[ix] = 1
            elif ix == 2:
                arr[ix] = 2
            else:

                n_ways = 0

                # Try stepsizes of 1 and 2
                for step_size in range(1, 3):

                    print(f' sub step: {step_size}')

                    # Add ways to take remaining amount of steps
                    rem = ix - step_size
                    n_ways += arr[rem]

                # Save the number of ways
                arr[ix] = n_ways

        # Save last value
        return arr[-1]

    def climbStairs2(self, n: int) -> int:
        """Save base cases up front without if statements

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        Args:
            n (int): number of stairs to climb
        Notes:
            not significantly faster
        """

        # Create array and save base cases
        # Save base cases
        # ie: 0, 1, 2
        arr = [ix for ix in range(n + 1)]

        # We've got a base case
        if n <= 2:
            return arr[n]

        # Iterate through the range
        for ix in range(3, n + 1):

            n_ways = 0

            # Try stepsizes of 1 and 2
            for step_size in range(1, 3):

                # Add ways to take remaining amount of steps
                rem = ix - step_size
                n_ways += arr[rem]

            # Save the number of ways
            arr[ix] = n_ways

        # Save last value
        return arr[-1]

    def fibonacci(self, n: int) -> int:
        """Remove the for loop and use addition only

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        Args:
            n (int): number of stairs to climb
        Notes:
            not significantly faster
        """

        # Create array and save base cases
        # Save base cases
        # ie: 0, 1, 2
        arr = [ix for ix in range(n + 1)]

        # We've got a base case
        if n <= 2:
            return arr[n]

        # Iterate through the range
        for ix in range(3, n + 1):

            # Save the number of ways
            # dependent on ways to use step remaining steps
            arr[ix] = arr[ix - 1] + arr[ix - 2]

        # Save last value
        return arr[-1]

    def constant_space(self, n: int) -> int:
        """Only save the necessary values

        We really only need 3 previous values
            current step, current step - 1, current step - 2

        Time Complexity
            O(n)
        Space Complexity
            O(1)

        Args:
            n (int): number of stairs to climb
        Notes:
            not significantly faster
        """

        # We've got a base case
        # ie: 0, 1, 2
        if n <= 2:
            return n

        # Save previous steps
        p1 = 2 # 1 step prior
        p2 = 1 # 2 steps prior

        # Iterate through the range
        for ix in range(3, n + 1):

            # Save the number of ways
            # 1 step + 2 step
            current = p1 + p2

            # Save values and update for the next iterations
            # 1 step => 2 steps
            # current => 1 step
            p2 = p1
            p1 = current

        # Save last value
        return current


if __name__ == "__main__":

    s = Solution()

    # Base cases
    # print(s.constant_space(1))
    # print(s.constant_space(2))

    # Example case
    # print(s.constant_space(3))
    print(s.constant_space(4))
    # print(s.constant_space(5))
