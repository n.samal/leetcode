"""
Inputs
    n (int): length of digit to dial
Outputs
    int: number of unique numbers possible
Notes
    - movements can only be in L style (2 by 1, or 1 by 2)
    similar to a knight from chess
    - we can only stand on numeric values
    - we can start on any numeric cell intially
    - for very large numbers return 10^9 + 7

Examples
    Example 1:

        Input: n = 1
        Output: 10
        Explanation: We need to dial a number of length 1, so placing the knight over any numeric cell of the 10 cells is sufficient.

    Example 2:

        Input: n = 2
        Output: 20
        Explanation: All the valid number we can dial are [04, 06, 16, 18, 27, 29, 34, 38, 40, 43, 49, 60, 61, 67, 72, 76, 81, 83, 92, 94]

    Example 3:

        Input: n = 3
        Output: 46
        Example 4:

    Example 4:
        Input: n = 4
        Output: 104

Ideas

    - when n = 1, we can reach anynumber
    since we can start anywhere
        + ie: 0 to 9

    - we could recursively build numbers
    from a queue and track unique numbers
        + this would be expensive for large n
        + may not need to track which numbers
        have been visited

    - some numbers have no next possible positions

        0: 4, 6
        1: 6, 8
        2: 7, 9
        3: 4, 8
        4: 0, 3, 9
        5: none
        6: 0, 1, 7
        7: 2, 6
        8: 1, 3
        9: 2, 4

    - if we have the last digit, we know the number of permutations
    possible using that seed

        ie: value = 0, digit = 0
        we can add 2 values to that 
        04, 06

        ie: value = 04, digit = 4

        we can build 3 numbers of that

    - perhaps build from the bottom up, the number of values
    ending in a particular digit
        + almost all values have 2 permutations ie: 0 => 4, 6
        + 4 & 6 have 3 permutations ie: 4 => 0, 3, 9

        + create a table of values ending in this digit for each n

            col_ix = digits [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
            row_ix = length of dialed number

            initially all values have 1 possible

        + we just need the last row, not the whole array of values
"""


class Solution:

    def knightDialer_generalized(self, n: int) -> int:
        """Generalized version of a dialer

        - use the grid to determine which other digits are accessible

        Time Complexity
            O(n*9*3)
            
            9 digits
            3 neighbors max

        Space Complexity
            O(1)
        """
        
        ''' Generate Map '''
        grid = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [None, 0, None]]
        n_rows: int = len(grid)
        n_cols: int = len(grid[0])
             
        dct = defaultdict(list)
                
        for r in range(n_rows):
            for c in range(n_cols):
                
                v = grid[r][c]
                
                if v is None:
                    continue
                
                for r_new, c_new in [
                    [r + 2, c + 1], 
                    [r + 2, c - 1], 
                    [r - 2, c + 1], 
                    [r - 2, c - 1], 
                    [r + 1, c + 2], 
                    [r + 1, c - 2], 
                    [r - 1, c + 2], 
                    [r - 1, c - 2]]:
                
                    if 0 <= r_new < n_rows and 0 <= c_new < n_cols and grid[r_new][c_new] != None:
                        dct[v].append(grid[r_new][c_new])
                
        
        if n == 1:
            return 10
        
        
        def recurse(digit: int, combos: int, n_left: int):
            """Recursively generate - this is too slow if used"""
            if n_left == 0:
                self.count += combos
            else:
                
                for neighbor in dct[digit]:
                    recurse(neighbor, combos, n_left - 1)
        
        # 1 Number of length 1
        count_prev = [1 for _ in range(10)]
        
        # For each number length
        # build distinct ways off the previous numbers
        for _ in range(1, n):
            
            count_new = [0 for _ in range(10)]
            
            for digit in range(10):
                for neighbor in dct[digit]:
                    count_new[digit] += count_prev[neighbor]
            
            count_prev = count_new
            
        mod = 10**9 + 7
        
        return sum(count_new) % mod


    ''' Case Specific '''

    def knightDialer(self, N: int) -> int:
        """Hand calc intuition

        - if the number ends in 0
             we know we can add take that and create
             create permutations ending in 4 & 6

             0 => 04, 06

        - if we have 40 numbers of length=2 ending in 0
            then we can generate 40 more permutations ending in 4 for length=3
            then we can generate 40 more permutations ending in 6 for length=3
        """

        # Count of 1 digit values ending in each number
        count = [1 for _ in range(10)]

        # Generate new range of numbers ending in each digit
        for n in range(N - 1):

            count_new = [0 for _ in range(10)]

            for digit in range(10):

                if digit == 0:
                    count_new[4] += count[digit]
                    count_new[6] += count[digit]

                elif digit == 1:
                    count_new[6] += count[digit]
                    count_new[8] += count[digit]

                elif digit == 2:
                    count_new[7] += count[digit]
                    count_new[9] += count[digit]

                elif digit == 3:
                    count_new[4] += count[digit]
                    count_new[8] += count[digit]

                elif digit == 4:
                    count_new[0] += count[digit]
                    count_new[3] += count[digit]
                    count_new[9] += count[digit]

                elif digit == 5:
                    pass

                elif digit == 6:
                    count_new[0] += count[digit]
                    count_new[1] += count[digit]
                    count_new[7] += count[digit]

                elif digit == 7:
                    count_new[2] += count[digit]
                    count_new[6] += count[digit]

                elif digit == 8:
                    count_new[1] += count[digit]
                    count_new[3] += count[digit]

                elif digit == 9:
                    count_new[2] += count[digit]
                    count_new[4] += count[digit]

            count = count_new

            print(f'count: {count}')

        return sum(count) % (10**9 + 7)

    def knightDialer2(self, N: int) -> int:
        """Hand calc intuition improved

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        # Count of 1 digit values ending in each number
        count = [1 for _ in range(10)]

        # Generate new range of numbers ending in each digit
        for n in range(N - 1):

            count_new = [0 for _ in range(10)]

            count_new[0] = count[4] + count[6]
            count_new[1] = count[6] + count[8]
            count_new[2] = count[7] + count[9]
            count_new[3] = count[4] + count[8]
            count_new[4] = count[0] + count[3] + count[9]
            count_new[5] = 0
            count_new[6] = count[0] + count[1] + count[7]
            count_new[7] = count[2] + count[6]
            count_new[8] = count[1] + count[3]
            count_new[9] = count[2] + count[4]

            count = count_new

            # print(f'count: {count}')

        return sum(count) % (10**9 + 7)

    def knightDialer3(self, N: int) -> int:
        """Hand calc without array
        
        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        # Count of 1 digit values ending in each number
        num0 = 1
        num1 = 1
        num2 = 1
        num3 = 1
        num4 = 1
        num5 = 1
        num6 = 1
        num7 = 1
        num8 = 1
        num9 = 1

        # Generate new range of numbers ending in each digit
        # Use multi assign to avoid tmp vars
        for n in range(N - 1):

            num0, num1, num2, num3, num4, num5, num6, num7, num8, num9 = num4 + num6, num6 + num8, num7 + num9, num4 + num8, num0 + num3 + num9, 0, num0 + num1 + num7, num2 + num6, num1 + num3, num2 + num4

        return (num0 + num1 + num2 + num3 + num4 + num5 + num6 + num7 + num8 + num9) % (10**9 + 7)


if __name__ == '__main__':

    # Example 1
    # n = 1

    # n = 2

    n = 3

    obj = Solution()
    print(obj.knightDialer(n))
    print(obj.knightDialer2(n))
    print(obj.knightDialer3(n))
