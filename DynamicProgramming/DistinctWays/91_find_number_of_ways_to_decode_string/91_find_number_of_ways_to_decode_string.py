"""
Inputs
    s (str): non-empty string containing only digits
Outputs
    int: number of ways to decode the string
Notes
    - mappings go from 1 to 26
        1  -> A
        2  -> B
        26 -> Z

Example 

    Example 1

        Input: "12"
        Output: 2
        Explanation: It could be decoded as "AB" (1 2) or "L" (12).

    Example 2
        Input: "226"
        Output: 3
        Explanation: 
            It could be decoded as 
            "BZ" (2 26), 
            "VF" (22 6), or 
            "BBF" (2 2 6).

Ideas
    - use the length of the string to determine potential ways to split
        + length = 1: 1 way
        + length = 2: multiple

            ie: 22 => 22, 2 & 2

            only works for some valid numbers

        + length = 3: same concepts
            cut the string in multiple ways

            looking at only 1 digit at a time

    - could look at different ways to cut a string.. seems overly complicated
    - dynamic programming likely the way to go

Hand Calc

    "226"

    2  -> 1 way
    22 -> 2 ways
        22
        or
        2 & 2

"""

class Solution:
    def numDecodings(self, s: str) -> int:
