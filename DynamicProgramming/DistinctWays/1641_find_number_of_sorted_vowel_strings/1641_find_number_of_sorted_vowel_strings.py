"""
Input
    n (int): length of number of 
Output
    int: number of lexicographically sorted values

Ideas
    - seems similar to the knight dialer.
        + recursion would work, but be expensive

    - determine number of possibilities for each ending character

        + A can be combined with 'a', 'e', 'i', 'o', 'u' 
        + E can be combined with 'e', 'i', 'o', 'u' 
        + I can be combined with 'i', 'o', 'u' 
        + O can be combined with 'o', 'u' 
        + U can be combined with 'u' 
"""

class Solution:
    def countVowelStrings(self, n: int) -> int:
        """

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        dct = {}

        # Counts of values ending in
        for char in 'aeiou':
            dct[char] = 1

        # Compute new counts of values ending in
        for v in range(n - 1):

            dct_new = {}
            dct_new['a'] = dct['a']                       # combine with a
            dct_new['e'] = dct['a'] + dct['e']            # combine with a,e
            dct_new['i'] = dct['a'] + dct['e'] + dct['i'] # combine with a,e,i
            dct_new['o'] = dct['a'] + dct['e'] + dct['i'] + dct['o']
            dct_new['u'] = dct['a'] + dct['e'] + dct['i'] + dct['o'] + dct['u']
            dct = dct_new

        # Grab the counts
        count: int = 0
        for key in dct:
            count += dct[key]

        return count
