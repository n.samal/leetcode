"""
Inputs
    nums (List[int]): choice of values
    target (int): target summations
Outputs
    int: number of combinations possible

Notes
    - find the number of possible combinations that add up to a positive integer target.
    - we can use each value multiple times

Ideas
    - try adding values from top down
        + track curren sum

    - sorted values so we get values in order
        + can break paths with sums that are
        too large

    - seems similar to coin change problem
        + infinite knapsack
        + build combinations for each value from the bottom up

"""

from typing import List


class Solution:

    ''' Top Down: without memo '''

    def top_down(self, nums: List[int], target: int) -> int:
        """

        Notes
            - works but is too slow
        """
        nums = sorted(nums)

        self.count = 0
        self.recurse(nums, target)

        return self.count

    def recurse(self, nums: int, target: int, cur_sum: int = 0):

        # Target found
        if cur_sum == target:
            self.count += 1

        # Searching remaining numbers (allowing for repeats)
        for ix in range(len(nums)):

            new_sum = cur_sum + nums[ix]

            if new_sum <= target:
                self.recurse(nums, target, new_sum)
            else:
                break

    def bottom_up(self, nums: List[int], target: int) -> int:
        """Coin change with infinite knapsack

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        # Number of ways to make each value
        n_ways = [0 for _ in range(target + 1)]
        n_ways[0] = 1

        # Try making each summation
        # Add the number of ways to make the remaining sum
        for cur_target in range(target + 1):

            for v in nums:
                remaining = cur_target - v

                if remaining >= 0:
                    n_ways[cur_target] += n_ways[remaining]

        return n_ways[target]


if __name__ == '__main__':

    # Example 1
    # nums = [1, 2, 3]
    # target = 4

    # TLE
    nums = [4, 2, 1]
    target = 32

    obj = Solution()
    # print(obj.top_down(nums, target))
    print(obj.bottom_up(nums, target))
