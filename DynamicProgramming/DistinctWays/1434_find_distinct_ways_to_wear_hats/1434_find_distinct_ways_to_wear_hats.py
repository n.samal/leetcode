"""
Inputs
    hats (List[List[int]]): preferred hats for each person
Outputs:
    int: number of ways to wear distinct hats

Notes
    - a hat can only be worn by one person
    - each person has a set of preferred hats to wear
    - hats are labeled from 1 to 40
    - the answer can be large, so use modulo

Examples

    Example 1:

        Input: hats = [[3,4],[4,5],[5]]
        Output: 1
        Explanation: There is only one way to choose hats given the conditions. 
        First person choose hat 3, Second person choose hat 4 and last one hat 5.

    Example 2:

        Input: hats = [[3,5,1],[3,5]]
        Output: 4
        Explanation: There are 4 ways to choose hats
        (3,5), (5,3), (1,3) and (1,5)

    Example 3:

        Input: hats = [[1,2,3,4],[1,2,3,4],[1,2,3,4],[1,2,3,4]]
        Output: 24
        Explanation: Each person can choose hats labeled from 1 to 4.
        Number of Permutations of (1,2,3,4) = 24.

    Example 4:

        Input: hats = [[1,2,3],[2,3,5,6],[1,3,7,9],[1,8,9],[2,5,7]]
        Output: 111

Ideas
    
    - distinct ways
        + track which hats have been used
        + track global count

    - try top down with memo

    - try bottom up ??

Key Ideas
    - use bit manipulations for reducing time & space complexity

    + bit masks represent whether a person has been assigned a hat or not

        instead of [True, False, True, False] we use [1, 0, 1, 0]
        which can be represented as the integer 12

        if everyone no one gets a hat, subset_mask = 0000 = 0
        if some ppl get hats           subset_mask = 1010 = 12
        if everyone gets a hat         subset_mask = 1111 = 15

    + to check whether a specific person will be assigned a hat
    we want to see if 1 occurs at the ith position aka person i
    in our subset

        we create a mask for each person

        person 0 = 1 * 2^0 = 1 << 0 = 1 = 0001
        person 1 = 1 * 2^1 = 1 << 1 = 2 = 0010
        person 2 = 1 * 2^2 = 1 << 2 = 4 = 0100
        person 3 = 1 * 2^3 = 1 << 3 = 8 = 1000

        we check if person i is 1 in the subset using &

        person_mask & subset_mask 

        person 0 and everyone gets a hat
        0001 & 1111 = 0001
    
References:
    https://leetcode.com/problems/number-of-ways-to-wear-different-hats-to-each-other/discuss/608599/Yeah-well.-Just-make-me-cry-during-the-interview-An-Optimised-Iterative-DP
    https://leetcode.com/problems/number-of-ways-to-wear-different-hats-to-each-other/discuss/608937/Let-the-hats-choose
    https://leetcode.com/problems/number-of-ways-to-wear-different-hats-to-each-other/discuss/609221/C%2B%2B-DP-solution-with-lots-of-comments.
    https://leetcode.com/problems/number-of-ways-to-wear-different-hats-to-each-other/discuss/608686/c-bit-masks-and-bottom-up-dp

    https://timseverien.github.io/binary-cheatsheet/
    https://wiki.python.org/moin/BitwiseOperators
"""

from collections import defaultdict
from typing import List


class Solution:
    def numberWays(self, hats: List[List[int]]) -> int:
        """Top down without memo

        - use backtracking to use the hat for
        subtrees, then free the hat after crawling those

        Notes
            - too slow
        """

        self.counts: int = 0
        self.hat_pref = hats
        self.n = len(hats)
        self.hats_used = [False for _ in range(40)]
        mod = 10**9 + 7

        def crawl(ix: int):

            if ix == self.n:
                self.counts += 1
                return

            # Try picking an unused hat for each person
            # Use the hat, then make it available
            for hat_pref in self.hat_pref[ix]:

                # Shift to get within 0 to n - 1
                hat_pref -= 1

                if self.hats_used[hat_pref] is False:
                    self.hats_used[hat_pref] = True
                    crawl(ix + 1)
                    self.hats_used[hat_pref] = False

        crawl(0)

        return self.counts % mod

    def bottom_up(self, hats: List[List[int]]) -> int:
        """

        - a person can have a hat or not
            + each state will be represented by either be 0 or 1
            + total number of combinations = 2 ^ number of people

        - use a bit mask to represent whether a hat has been
        assigned to a person
            ie: 1010 = 12

        - every hat can be used or not used in a particular subset
            + if the hat is used we must assign to a person
            ie: we must update the bit mask

        Time Complexity
            O(2^n * 41 * n)

            if every hat is applicable to every person
        Space Complexity
            O(2^n * 41)
        References:
            https://timseverien.github.io/binary-cheatsheet/
        """

        n: int = len(hats)       # number of people
        n_hats: int = 40         # number of hats
        n_subsets: int = 2 ** n  # number of subsets
        mod = 10**9 + 7

        # Track potential persons for each hat
        hat_dct = defaultdict(list)

        for person, hat_prefs in enumerate(hats):
            for hat in hat_prefs:
                hat_dct[hat].append(person)

        # Store the number of ways to assign a hat to n people
        # dp[hat][mask] 
        # the bit mask refers to whether person i has been assigned a hat
        dp = [[0 for mask in range(n_subsets)] for h in range(n_hats + 1)]

        # Base case
        # Only one way possible for no hats, no people
        dp[0][0] = 1

        # Try assigning a particular hat to each subset
        # mask/subset = person 1 has a hat, person 2 no hat, etc..
        for h in range(1, n_hats + 1):
            for mask in range(n_subsets):

                print(f'subset: {mask:010b}')

                # Don't use the hat
                # - add ways without this hat
                dp[h][mask] = (dp[h][mask] + dp[h - 1][mask]) % mod

                # Assign the hat
                # - for all the people that would want this hat
                # - assign the hat if the person gets assigned a hat in this subset
                # - mask of person p = 1 << person
                for person in hat_dct[h]:

                    mask_person = 1 << person
                    print(f' person: {mask_person:010b}')

                    if mask & mask_person:

                        # Add ways where hat h isn't used and
                        # person p hasn't yet been assigned a hat
                        # but all others in the subset have
                        # ie: mask ^ mask_person
                        result2 = mask ^ mask_person
                        print(f' assign: {result2:010b}')

                        dp[h][mask] = (dp[h][mask] + dp[h - 1][mask ^ mask_person]) % mod

        return dp[n_hats][-1]


if __name__ == '__main__':

    # Example 1
    # hats = [[3,4],[4,5],[5]]

    # Example 2
    # hats = [[3,5,1],[3,5]]

    # Example 3
    hats = [[1,2,3,4],[1,2,3,4],[1,2,3,4],[1,2,3,4]]

    # Example 4
    # hats = [[1,2,3],[2,3,5,6],[1,3,7,9],[1,8,9],[2,5,7]]

    obj = Solution()
    print(obj.bottom_up(hats))
