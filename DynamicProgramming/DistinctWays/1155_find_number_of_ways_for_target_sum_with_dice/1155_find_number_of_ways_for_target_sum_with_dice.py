"""
Inputs
	d (int): number of dice
	f (int): number of faces on each dice
	target (int): target summation
Outputs
	int: number of ways to make the target value

Examples

	Example 1:

		Input: d = 1, f = 6, target = 3
		Output: 1
		Explanation: 
		You throw one die with 6 faces.  There is only one way to get a sum of 3.

	Example 2:

		Input: d = 2, f = 6, target = 7
		Output: 6
		Explanation: 
		You throw two dice, each with 6 faces.  There are 6 ways to get a sum of 7:
		1+6, 2+5, 3+4, 4+3, 5+2, 6+1.

	Example 3:

		Input: d = 2, f = 5, target = 10
		Output: 1
		Explanation: 
		You throw two dice, each with 5 faces.  There is only one way to get a sum of 10: 5+5.

	Example 4:

		Input: d = 1, f = 2, target = 3
		Output: 0
		Explanation: 
		You throw one die with 2 faces.  There is no way to get a sum of 3.

	Example 5:

		Input: d = 30, f = 30, target = 500
		Output: 222616187
		Explanation: 
		The answer must be returned modulo 10^9 + 7.

Ideas

	- if one dice and 6 face

       + we can get the value only 1 time if the value is within 1 -> number of faces

	- if the value is not within a range we cannot make it

		+ too small (ie: lowest value is 1 for each dice)
		+ too large (ie: largest value is f for each dice)

	- apply coin change like strategy and build upon previous results

"""

class Solution:
    def numRollsToTarget(self, d: int, f: int, target: int) -> int:
    	"""

		- save the bases cases for creating values with 1 dice
		- for each additional dice, compute how we could
		build upon the previous result to make the current value

			+ current_roll + previous_total = new_total

		- only iterate over possible values with the dice

		Time Complexity
			O(d*f*v)

			d = number of dice
			f = number of faces
			v = value range 

		Notes
			- we only need the previous row of the dp table for each
			computation

    	"""

        modulo = 10**9 + 7

        bound_low = d
        bound_high = d * f

        # Target is too small
        if target < bound_low:
            return 0

        # Target is too big to meet
        elif target > bound_high:
            return 0

        # Compute the number of ways to calculate for each number of dice
        # number of dice by target value
        dp = [[0 for _ in range(bound_high + 1)] for __ in range(d + 1)]

        # Save base cases
        for v in range(1, f + 1):
            dp[1][v] = 1

        for n_dice in range(2, d + 1):

            # Amounts possible with dice
            bound_low = n_dice
            bound_high = n_dice * f

            # print(f'n_dice: {n_dice} low: {bound_low}  high: {bound_high}')

            for total in range(bound_low, bound_high + 1):
                for v in range(1, f + 1):

                    # previous_total + current_roll = current_total
                    # previous_total = current_total - current_roll
                    comp = total - v

                    if comp >= 0:
                        dp[n_dice][total] += dp[n_dice - 1][comp]

        # Find the number of ways to compute the target
        ans = dp[d][target] % modulo
        return ans