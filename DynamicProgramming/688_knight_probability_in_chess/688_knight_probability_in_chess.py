"""
Inputs
    N (int): number of spaces on board
    K (int): number of moves for the knight
    row (int): starting position
    col (int): starting position
Outputs
    int: probability that a knight will land in a valid spot

Notes
    - A chess knight has 8 possible moves it can make
        + Each move is two squares in a cardinal direction, 
        then one square in an orthogonal direction.

    - The rows and columns are 0 indexed, 
    so the top-left square is (0, 0), and the bottom-right square is (N-1, N-1)

    - Each time the knight is to move, it chooses one of eight possible moves uniformly
    at random (even if the piece would go off the chessboard) and moves there.

    - The knight continues moving until it has made exactly K moves or has moved off the chessboard. 
    Return the probability that the knight remains on the board after it has stopped moving.
    
Ideas
    - from each position search the 8 directions for K moves
        + 8^K potential routes
        + many will likely lead off the map
        + many will likely end in the same location

    - don't continue dead ends

"""


from collections import deque

''' Breadth First Search '''
class Solution:

    def knightProbability(self, N: int, K: int, row: int, col: int) -> float:
        """Breadth first search

        Notes
            - works but is too slow, likely repeated work

        Time Complexity
            O(8^K)
        Space Complexity
            O(8^K?) size of the queue
        """
        
        dirs = [(1, 2), (1, -2), (-1, 2), (-1, -2), (2, 1), (2, -1), (-2, 1), (-2, -1)]
        
        queue = deque([(row, col, K)])
        valid_moves: int = 0
        
        while queue:
            
            r, c, k = queue.popleft()
            
            if k == 0:
                valid_moves += 1
            else:
                
                for dx, dy in dirs:
                    c_new = c + dx
                    r_new = r + dy
                    
                    if 0 <= c_new < N and 0 <= r_new < N:
                        queue.append((r_new, c_new, k - 1))
                    
        return valid_moves / 8**K

''' Depth First Search with Memo '''

class Solution:
    def knightProbability(self, N: int, K: int, row: int, col: int) -> float:
        
        self.cache = {}
        valid_moves = self.recurse(row, col, K, N)
        
        return valid_moves / 8**K
        
    def recurse(self, r: int, c: int, k: int, N: int):
        """
            
        Time Complexity
            O(r*c*k)
        Space Complexity
            O(r*c*k)
        """
    
        dirs = [(1, 2), (1, -2), (-1, 2), (-1, -2), (2, 1), (2, -1), (-2, 1), (-2, -1)]
        key = (r, c, k)
        
        valid_moves: int = 0
        
        if key in self.cache:
            return self.cache[key]
        
        # No more moves and in a valid spot
        if k == 0:
            return 1
            
        else:

            for dx, dy in dirs:
                c_new = c + dx
                r_new = r + dy

                if 0 <= c_new < N and 0 <= r_new < N:
                    valid_moves += self.recurse(r_new, c_new, k - 1, N)
            
            self.cache[key] = valid_moves
            return valid_moves
