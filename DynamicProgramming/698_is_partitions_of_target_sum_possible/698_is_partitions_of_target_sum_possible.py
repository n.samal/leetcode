"""

Ideas

    - if sum of array can be split, then there will
    be no leftovers
        + ie: sum % k != 0

    - backtracking with marking
        + track the current summation
        + track values used
        + number of values left

"""

from typing import List


class Solution:
    def canPartitionKSubsets(self, arr: List[int], k: int) -> bool:

        arr_sum = sum(arr)

        # Can't split the sum equally
        if arr_sum % k != 0:
            return False

        target = arr_sum // k

        self.open = [True for _ in arr]  # value is open for use
        self.arr = arr

        return self.backtrack2(k, target)

    def backtrack1(self, k_left: int, target: int, ix: int = 0, cur_sum: int = 0):
        """

        Args:
            k_left: groups left
            target: target summation
            ix: pointer in arr
            cur_sum: current group sum
        Notes
            - too slow
        """

        # Summation matches
        if cur_sum == target:
            k_left -= 1
            cur_sum = 0

        elif cur_sum > target:
            return False

        # All groupings made
        if k_left == 0:
            return True

        for jx in range(len(self.arr)):

            if self.open[jx]:
                self.open[jx] = False
                if self.backtrack1(k_left, target, jx + 1, cur_sum + self.arr[jx]):
                    return True
                self.open[jx] = True

        return False

    ''' Modified backtracking '''

    def backtrack2(self, k_left: int, target: int, ix: int = 0, cur_sum: int = 0):
        """

        - once the target summation is found, restart
        search at the beginning of the array
            + otherwise only search the remaining values

        Args:
            k_left: groups left
            target: target summation
            ix: pointer in arr
            cur_sum: current group sum

        Time Complexity
            O(~k^n)

            to create k subsets, we can go through the array potentially n times
        Space Complexity
            O(n)
        """

        # Summation matches
        if cur_sum == target:
            return self.backtrack2(k_left - 1, target, 0, 0)

        elif cur_sum > target:
            return False

        # All groupings made
        if k_left == 0:
            return True

        for jx in range(ix, len(self.arr)):

            if self.open[jx]:
                self.open[jx] = False
                if self.backtrack2(k_left, target, jx + 1, cur_sum + self.arr[jx]):
                    return True
                self.open[jx] = True

        return False


if __name__ == '__main__':

    # Example 1
    nums = [4, 3, 2, 3, 5, 2, 1]
    k = 4

    obj = Solution()
    # print(obj.canPartitionKSubsets(nums, k))
    print(obj.canPartitionKSubsets(nums, k))
