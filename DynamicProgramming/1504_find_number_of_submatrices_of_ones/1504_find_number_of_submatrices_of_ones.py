"""
Inputs
    grid (List[List[int]]): binary grid
Outputs
    int: number of submatrices of all ones

Ideas

    - crawl every row and crawl right

        + find the number of consecutive 1s in the row
        + notice that large patterns also contain smaller patterns

        1, 1       => 1 by 2
        1, 1, 1    => 1 by 3
                      is also 2, 1 by 2
        
        1, 1, 1, 1 => 1 by 4
                      is 2, 1 by 3
                      is 3, 1 by 2

        + aggregate these values to larger count
        + perform the same approach with columns

    - how to handle 2D rectangles?
        + find large 2D bound

        1 1
        1 1
        1 1

        is 3 by 2

            is also 6, 1 by 1s
            is also 3, 1 by 2s
            is also 2, 3 by 1s
            is also 2, 2 by 2s
            is also 

    - compute consecutives

        Input: mat = [[0,1,1,0],
                      [0,1,1,1],
                      [1,1,1,0]]

        Track consecutives 1s aka width

                arr = [0, 1, 2, 0]
                      [0, 1, 2, 3]
                      [1, 2, 3, 0]

Strategy

    Count Encompassing Rectangles ending here

        - iterate through each row and column position

        on each row, count the number of submatrices that encompass
        the current position

        + first compute the number of submatrices starting at (r,c)
        and ending to the left of us (either in the same row, or another row)

        + move to the next row, and see if the next row can also include
        (r,c)

Hand Calc

    at (0, 0)
        val = 0, no submatrix can start here

    at (0, 1)
        val = 1, potential columns can start here

        start moving down, to see if we can create columns

        col 0: 1  => 1 by 1 (height, width)   add 1 submatrix
        col 1: 1  => 2 by 1                   add 1 submatrix
        col 2: 2  => 3 by 1                   add 1 submatrix

        On column 2 since our width is smaller at the top we're limited
        by that value. width = min(initial row, current_row)

        if column 2 was 0, we wouldn't be able to create a submatrix

    at (0, 2)
        val = 2, then we know width of 2 is possible on 0th row

        col 0: 2 => 1 by 2 (height, width)   we can add 1 by 1 and 1 by 2 (2 submatrices)
        col 1: 2 => 2 by 2 ()                we can add 2 by 1 and 2 by 2 (2 submatrices)
        col 2: 3 => 3 by 2                   we can add 3 by 1 and 3 by 2 (2 submatrices)

        pattern occurs, where the number of submatrices to add is equal to the
        bounding width

References
    Dynamic Programming
        - https://leetcode.com/problems/count-submatrices-with-all-ones/discuss/725108/Python-3-Dynamic-Programming
        - https://leetcode.com/problems/count-submatrices-with-all-ones/discuss/720097/Java-DP-solution-O(m*mn)
        - https://leetcode.com/problems/count-submatrices-with-all-ones/discuss/838982/Java-DP-solution-time-cost-O(row2*column)-space-O(row)

    Stacks
        - https://leetcode.com/problems/count-submatrices-with-all-ones/discuss/721999/Python3-O(MN)-histogram-model
        - https://leetcode.com/problems/count-submatrices-with-all-ones/discuss/720265/Java-Detailed-Explanation-From-O(MNM)-to-O(MN)-by-using-Stack
"""

from typing import List


class Solution:
    def numSubmat(self, grid: List[List[int]]) -> int:
        """

        - compute number of consecutive ones in each row ie: width
        - find submatrices to the left that encompass the current positon
            + move down row by row, while a larger grid is possible

        Time Complexity
            O(r*c*c)
        Space Complexity
            O(r*c)
        """

        n_rows: int = len(grid)
        n_cols: int = len(grid[0])

        count: int = 0

        # Compute consecutive ones in each row
        for r in range(n_rows):
            for c in range(1, n_cols):

                if grid[r][c]:
                    grid[r][c] = 1 + grid[r][c - 1]

        # Crawl grid
        # find submatrices that start at (r,c)
        # and end to the left
        for r in range(n_rows):
            for c in range(n_cols):

                if grid[r][c]:

                    # Move down and find grids encompassing
                    # the start position, and current row
                    # ie: if width below is 0, no add
                    #     if width below is 1, we're limited by the smaller width
                    row_bound = r
                    width_min = grid[r][c]

                    while row_bound < n_rows and grid[row_bound][c]:

                        width_min = min(width_min, grid[row_bound][c])
                        count += width_min

                        row_bound += 1

        return count


if __name__ == '__main__':

    # Example 1
    grid = [[1,0,1],
          [1,1,0],
          [1,1,0]]

    # Example 2
    grid = [[0,1,1,0],
          [0,1,1,1],
          [1,1,1,0]]

    # Example 4
    # grid = [[1,0,1],[0,1,0],[1,0,1]]

    obj = Solution()
    print(obj.numSubmat(grid))