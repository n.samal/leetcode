class Solution(object):

    def parentheses(self, s, n_left, n_right):
        """Recursively build a string of balanced parentheses until there are
        no more characters left

        Args
            s (str): string to add to
            n_left (int): number of left parentheses left
            n_right (int): number of right parentheses left
        Returns:
        str: string of balanced parentheses
        """

        # No characters left, save it!
        if (n_left == 0) and (n_right == 0):
            self.groups.append(s)

        # Use a left (
        if n_left > 0:
            self.parentheses(s + '(', n_left - 1, n_right)

        # Use a right )
        if (n_right > 0) and (n_left < n_right):
            self.parentheses(s + ')', n_left, n_right - 1)

    def find_combinations(self, n):
        """ Output all valid combinations of n parentheses
        Args:
            n (int): number of each type of parentheses
        """

        self.groups = []

        print('Combinations of {} parentheses'.format(n))

        # Run method
        self.parentheses('', n, n)

        return self.groups


if __name__ == "__main__":

    s = Solution()

    # Basic: ()
    print(s.find_combinations(3))

