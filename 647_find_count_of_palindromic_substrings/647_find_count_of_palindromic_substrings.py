"""
Inputs
    s (str): string
Outputs
    int: number of palindromic substrings
Goals
    - find the number of palindromic substrings
    - substrings with different start indexes or end indexes are counted as different substrings even they consist of same characters
    (reference example 2)

Examples

    Example 1

        Input: "abc"
        Output: 3
        Explanation: Three palindromic strings: "a", "b", "c".

    Example 2

        Input: "aaa"
        Output: 6
        Explanation: Six palindromic strings: "a", "a", "a", "aa", "aa", "aaa".

Cases

    Even Numbered
        aa
        abba

    Odd Numbered
        a
        aaa
        bbabb

Ideas
    - sliding window approach??
        "abba"

    - manual check 
        + around each character, move left and right while the end points match
        + at each n, we travel potentially all characters O(n^2)
        + this works for odd numbered palindromes but not even

    - each character by itself is a palindrome
        + we can use the length of the string as a baseline
        + ie: 'a', 'a', 'a'
        + ie: 'a', 'b', 'c'

    - maybe can we build around around previously found palindromes
        + ie: 'bb'
        + ie: 'abba'

"""

class Solution:
    def countSubstrings(self, s: str) -> int:
        """Expanding window at each character

        - at each chacter expand our window left and right
        - add counts, while we find palindromes

        Time Complexity
            O(n^2)
        Space Complexity
            O(1)
        """

        count: int = 0
        n: int = len(s)

        for ix, char in enumerate(s):

            # Count odd numbered palindromes
            # ie: 'aba', 'aabaa'
            left = ix
            right = ix

            while left >= 0 and right < n and s[left] == s[right]:
                count += 1
                left -= 1
                right += 1

            # Count even numbered palindromes
            # ie: 'aa', 'aaaa'
            left = ix
            right = ix + 1

            while left >= 0 and right < n and s[left] == s[right]:
                count += 1
                left -= 1
                right += 1

        return count
