
''' Recursive solution '''


def search_for_peak(arr: list, left: int, right: int):
    """Recursively search for a peak using binary search

    Search for a peak value, where a peak value
    is one that is greater than it's neighbors

    *note*: if definition was >= neighbors, we could modify our
    algorithmic strategy to use binary search as is

    Basic idea, is use binary search in a divide and conquer type of approach
    half the search space and trying searching the half of the array
    with has a positive slope

    Args:
        arr (int): array of integers in unknown order
        left (int): starting indice of array range
        right (int): stopping indice of array range
    Returns:
        int: indice of peak value
    """

    n: int = len(arr)

    while left <= right:

        mid = (left + right) // 2

        print(f"left: {left} right: {right} mid: {mid}")

        # Peak at end
        if mid == n - 1 and arr[mid] > arr[mid - 1]:
            return mid

        # Peak at beginning
        elif mid == 0 and arr[mid] > arr[mid + 1]:
            return mid

        # Peak in middle
        elif arr[mid] > arr[mid - 1] and arr[mid] > arr[mid + 1]:
            return mid

        # Search right half
        route_r = search_for_peak(arr, mid + 1, right)

        # Found a valid route
        if route_r > 0:
            return route_r

        # Search left half
        return search_for_peak(arr, left, mid - 1)

    return -1


def get_peak_index(arr: list):
    """Find the index of a peak element

    Peak point can occur at 3 locations
        left, middle, right

    if a pivot exists, all values next to it are greater

    Args:
        arr (list): sorted and rotated array of numbers
    Returns:
        int: location of pivot element in the array
    """

    n = len(arr)

    # Single element
    if n == 1:
        return 0

    # No elements
    elif n == 0:
        return -1

    # Search for peak
    return search_for_peak(arr, 0, n - 1)


''' Binary Search '''


def get_peak_index2(arr: list):
    """
    may not work
    """

    n: int = len(arr)
    left: int = 0
    right: int = n - 1

    while left < right:

        mid = (left + right) // 2

        print(f"left: {left} right: {right} mid: {mid}")

        # Search left half
        if arr[mid] > arr[mid + 1]:

            right = mid + 1

        # Search right half
        else:
            left = mid

    return -1


if __name__ == '__main__':

    # Mid point peak
    # arr_in = [3, 4, 5, 1, 2]

    # Two peak: left and right
    # arr_in = [7, 0, 1, 2, 3, 5, 6]

    # Left peak only (descending)
    # arr_in = [7, 6, 5, 3]

    # Right peak only (ascending)
    # arr_in = [3, 4, 5, 9]

    # No peaks
    arr_in = [1, 1, 1, 1, 1, 1]

    # Alternating in left half
    # arr_in = [1, 2, 1, 1, 1, 1, 1]

    # Alternating in right half
    # arr_in = [1, 1, 1, 1, 2, 1, 1]

    # Pivot on right
    # arr_in = [4, 5, 6, 7, 0, 1, 2]
    # arr_in = [2, 3, 4, 5, 6, 7, 8, 0, 1]
    # arr_in = [2, 3, 4, 5, 6, 7, 8, 0]

    # print(get_peak_index(arr_in))
    print(get_peak_index2(arr_in))


