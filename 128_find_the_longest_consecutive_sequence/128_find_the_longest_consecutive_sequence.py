"""
Inputs
    arr (List[int]): unsorted integers
Outputs
    int: length of longest consecutive sequence
Notes
    - find the longest consecutive sequence of elements
    - algorithm should be O(n)

Examples

    Input: [100, 4, 200, 1, 3, 2]
    Output: 4
    Explanation: The longest consecutive elements sequence is [1, 2, 3, 4]. Therefore its length is 4.

Ideas

    - if sorted, then just add onto smaller chains

        [1]
        [1, 2]
        [1, 2, 3]

    - add onto smaller chains

        [3, 4]

        2 -> [3, 4]

    - add onto larger chains

Example

    val = 100

    chains_ending_at = {100: 1}
    chains_starts_at = {100: 1}

    val = 4

    chains_ending_at = {100: 1, 4: 1}
    chains_starts_at = {100: 1, 4: 1}

    val = 200

    chains_ending_at = {100: 1, 4: 1, 200}
    chains_starts_at = {100: 1, 4: 1, 200}

    val = 1

    chains_ending_at = {100: 1, 4: 1, 200, 1: 1}
    chains_starts_at = {100: 1, 4: 1, 200, 1: 1}

    val = 3

    try adding onto 2 => doesn't exist
    try adding onto 4

    chains_ending_at = {100: 1, 4: 1, 200, 1: 1, 3: 1}
    chains_starts_at = {100: 1, 4: 1, 200, 1: 1, 3: 2}

    chains_starts_at[3] = chains_starts_at[4] + 1
    chains_ending_at[3] = chains_ending_at[2] + 1

    val = 2

    try adding onto 1 => 1 exists
    try adding onto 3 => 3 exists

    chains_ending_at = {100: 1, 4: 1, 200, 1: 1, 3: 1}
    chains_starts_at = {100: 1, 4: 1, 200, 1: 1, 3: 2}

    chains_starts_at[2] = chains_starts_at[1] + 1
    chains_ending_at[3] = chains_ending_at[2] + 1

"""

from typing import List


class Solution:

    def sort_then_count(self, arr: List[int]) -> int:
        """ Sort then count

        - if numbers are 1 apart, increment chain length
        - if same consecutive numbers, just slide and ignore them

        Time Complexity
            O(n*ln(n))
        Space Complexity
            O(n)
        """

        n: int = len(arr)

        if n <= 1:
            return n

        arr = sorted(arr)

        ix: int = 0
        longest = 1

        while ix < n:

            count = 1

            while ix + 1 < n:

                if arr[ix + 1] - arr[ix] == 1:
                    count += 1
                    ix += 1
                    longest = max(longest, count)
                elif arr[ix + 1] == arr[ix] == 1:
                    ix += 1
                else:
                    break

            ix += 1

        return longest

    def set_then_count(self, arr: List[int]) -> int:
        """ Use a set to check for existence

        - store values in set for quick checks
        - crawl from the smallest values only

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        n: int = len(arr)

        if n <= 1:
            return n

        s = set(arr)

        longest = 1

        for v in arr:

            # Was part of a previous chain
            if v - 1 in s:
                continue

            # Crawl up to larger values
            count = 0

            while v in s:
                count += 1
                longest = max(longest, count)
                v += 1

        return longest


if __name__ == '__main__':

    # Example 1
    # arr_in = [100, 4, 200, 1, 3, 2]
    arr_in = [1, 2, 0, 1]

    obj = Solution()
    print(obj.sort_then_count(arr_in))
    print(obj.set_then_count(arr_in))
