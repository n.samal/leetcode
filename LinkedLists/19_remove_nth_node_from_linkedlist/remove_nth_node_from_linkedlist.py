# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

    def __str__(self) -> str:
        """Output nodes in our list ie: 1 -> 2 -> 3"""

        head = self
        s = ""

        while head:

            # Output current value
            s += str(head.val) + " -> "

            # Move to next node
            head = head.next

        return s

    def __repr__(self) -> str:
        """Object representation"""

        if self.next is None:
            return f"ListNode({self.val})"
        else:
            return f"ListNode({self.val}) -> ListNode({self.next.val})"


class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        """Remove the nth node from the end of a linked list (modify in place)

        Let's iterate through the list until we hit end of the list to
        determine the length

        Iterate until we hit the nth from end node

        We'll unclip the nth node, and connect to the next value if available

            Case 0: Remove node 0
            Case 1: Remove node 1 to n - 2
                a: Next node(s)
                a: No node(s)
            Case 2: Remove last node (n - 1)

        Size of LinkedList
            Case 0: length 1
            Case 2: length 2
            Case 3: length 3 to n

        Args:
            head (ListNode): singly linked list of integers
            n (int): index of the node to remove
        Returns:
            ListNode: linked list with removed node
        """

        # Initialize values
        ix: int = -1
        ix_end: int = 0

        # Initialize current node
        node_cur: ListNode = head

        # Iterate until we find the last node
        while node_cur.next:

            node_cur = node_cur.next
            ix_end += 1

        ''' Start at the head node again '''

        # Initialize current node
        node_cur: ListNode = head
        node_prv: ListNode = None

        ''' Find the nth from last node '''
        while ix < ix_end - n:

            node_prv = node_cur
            node_cur = node_cur.next
            ix += 1

        ''' We're at the nth from last node '''

        # We have more nodes
        if node_cur.next:

            # Connect the last node to the next node
            if node_prv:
                node_prv.next = node_cur.next

            # No previous nodes, so just start at next node
            else:
                return node_cur.next

        # No more nodes
        else:

            # We have previous nodes
            if node_prv:
                node_prv.next = None
            else:
                return None

        return head

    def removeNthFromEnd2(self, head: ListNode, n: int) -> ListNode:
        """Remove the nth node from the end of a linked list (modify in place)

        Let's iterate through the list until we hit end of the list to
        determine the length

        Iterate until we hit the nth from end node

        We'll unclip the nth node, and connect to the next value if available

            Case 0: Remove node 0
            Case 1: Remove node 1 to n - 2
                a: Next node(s)
                a: No node(s)
            Case 2: Remove last node (n - 1)

        Size of LinkedList
            Case 0: length 1
            Case 2: length 2
            Case 3: length 3 to n

        Args:
            head (ListNode): singly linked list of integers
            n (int): index of the node to remove
        Returns:
            ListNode: linked list with removed node
        """

        # Initialize values
        ix: int = 0
        size: int = 0

        # Initialize current node
        node: ListNode = head

        # Iterate until we find the last node
        while node:
            node = node.next
            size += 1

        # Only one node, can only remove this
        if size <= 1:
            return None

        ''' Start at the head node again '''
        node: ListNode = head
        ix_goal = size - n

        # Remove the first node if our target
        if ix_goal == 0:
            return node.next

        ''' Find the nth - 1 from last node '''
        while node:

            # We're almost at the skip node
            if ix + 1 == ix_goal:
                node.next = node.next.next
                return head

            node = node.next
            ix += 1

        return head


if __name__ == "__main__":

    print("Running test case")

    # [1]
    # l1 = ListNode(1)
    # print("l1:", l1)

    # [1, 2]
    # l1 = ListNode(1)
    # l1.next = ListNode(2)
    # print("l1:", l1)

    # [1, 2, 3, 4, 5]
    l1 = ListNode(1)
    l1.next = ListNode(2)
    l1.next.next = ListNode(3)
    l1.next.next.next = ListNode(4)
    l1.next.next.next.next = ListNode(5)
    print("l1:", l1)

    # Merge lists
    s = Solution()

    # Remove last node
    # ln = s.removeNthFromEnd(l1, 1)
    # ln = s.removeNthFromEnd2(l1, 1)
    # print("ln:", ln)

    # ln = s.removeNthFromEnd(l1, 2)
    # ln = s.removeNthFromEnd2(l1, 2)
    # print("ln:", ln)

    # Remove first node
    # ln = s.removeNthFromEnd(l1, 5)
    ln = s.removeNthFromEnd2(l1, 4)
    print("ln:", ln)

