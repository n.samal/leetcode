class Solution{

    public ListNode removeNthFromEnd(ListNode head, int n) {
            /* Remove the nth node from the end of a linked list (modify in place)

            Let's iterate through the list until we hit end of the list to
            determine the length

            Iterate until we hit the nth from end node

            We'll unclip the nth node, and connect to the next value if available

                Case 0: Remove node 0
                Case 1: Remove node 1 to n - 2
                    a: Next node(s)
                    a: No node(s)
                Case 2: Remove last node (n - 1)

            Size of LinkedList
                Case 0: length 1
                Case 2: length 2
                Case 3: length 3 to n

            Args:
                head (ListNode): singly linked list of integers
                n (int): index of the node to remove
            Returns:
                ListNode: linked list with removed node
            */

            // Initialize values
            int ix = -1;
            int ix_end = 0;

            // Initialize current node
            ListNode node_cur = head;

            // Iterate until we find the last node
            while (node_cur.next != null){

                node_cur = node_cur.next;
                ix_end += 1;
            }
            
            /* Start at the head node again */

            // Initialize current node
            node_cur = head;
            ListNode node_prv = null;

            /* Find the nth from last node */
            while (ix < ix_end - n){

                node_prv = node_cur;
                node_cur = node_cur.next;
                ix += 1;
            }
            
            /* We're at the nth from last node */

            // We have more nodes
            if (node_cur.next != null){

                // Connect the last node to the next node
                if (node_prv != null){
                    node_prv.next = node_cur.next;
                }

                // No previous nodes, so just start at next node
                else{
                    return node_cur.next;
                }
            }
            // No more nodes
            else{

                // We have previous nodes
                if (node_prv != null) {
                    node_prv.next = null;
                }
                else{
                    return null;
                }
            }

            return head;
    }


    public ListNode removeNthFromEnd2(ListNode head, int n) {
        /* Remove the nth node from the end of a linked list (modify in place)

        Let's iterate through the list until we hit end of the list to
        determine the length

        Iterate until we almost hit the nth from end node

        We'll unclip the nth node, and connect to the next value

            Case 0: Remove node 0
            Case 1: Remove node 1 to n - 2
                a: Next node(s)
                a: No node(s)
            Case 2: Remove last node (n - 1)

        Size of LinkedList
            Case 0: length 1
            Case 2: length 2
            Case 3: length 3 to n

        Args:
            head (ListNode): singly linked list of integers
            n (int): index of the node to remove
        Returns:
            ListNode: linked list with removed node
        */

        // Initialize values
        int ix = 0;
        int size = 0;

        // Initialize current node
        ListNode node = head;

        // Iterate until we find the last node
        while (node != null){

            node = node.next;
            size += 1;
        }
        
        // If we have 1 node
        if (size == 1){
            return null;
        }

        /* Start at the head node again */
        node = head;
        int ix_goal = size - n;

        // If we remove first node
        if (ix_goal == 0){
            return node.next;
        }

        // Iterate through list
        while (node != null){

            // We're almost at the skip node
            // Remember the last node still has a next value ie: None
            if (ix + 1 == ix_goal){
                node.next = node.next.next;
                return head;
            }

            node = node.next;
            ix += 1;
        }

        return head;
    }
}
