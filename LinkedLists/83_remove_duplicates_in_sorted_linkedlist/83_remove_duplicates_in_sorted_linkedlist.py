# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        
        cur_node = head
        
        # Create dummy previous node
        prev_node = ListNode(None)
        
        while cur_node:
            
            # Skip the current not if not unique
            if prev_node.val == cur_node.val:
                prev_node.next = cur_node.next
            
            # Save as our next previous, if unique
            else:
                prev_node = cur_node
            
            # Grab the next in the chain
            cur_node = cur_node.next
            
        return head