class Node:
    def __init__(self, val=None, prev=None, next=None, child=None):
        self.val = val
        self.prev = prev
        self.next = next
        self.child = child

    def __repr__(self):

        s = ''

        if self.prev:
            s += f'{self.prev.val}'

        s += f' <-> {self.val} <-> '

        if self.next:
            s += f'{self.next.val}'

        return s

    def __str__(self):
        return f'{self.val} <-> {self.next}'


class Solution:
    def flatten(self, head: 'Node') -> 'Node':
        """Crawl the head node"""

        # Null case
        if not head:
            return None

        # Create dummy node
        dummy = Node(None, None, None, None)

        # Create global variables
        self.prev_node = None
        self.cur_node = dummy

        # self.walk(head)
        self.walk2(head)

        # Remove connection of first real node to dummy
        dummy.next.prev = None

        return dummy.next

    def walk(self, head: 'Node'):
        """Recursively crawl the head node"""

        cur_node = head

        print(f'head: {head.val}')

        while cur_node:

            # Copy original, and node for global use
            original = cur_node
            cur_node_cpy = Node(cur_node.val, self.cur_node, None, None)

            # Add the node to our global list
            # and slide over nodes
            self.cur_node.next = cur_node_cpy
            self.prev_node = self.cur_node
            self.cur_node = cur_node_cpy

            # We've got kids, explore
            if original.child:
                self.walk(original.child)

            # Crawl down original path
            cur_node = original.next

    def walk2(self, head: 'Node'):
        """Recursively crawl the head node"""

        cur_node = head

        while cur_node:

            # Save pointers before we modify
            chld_node = cur_node.child
            next_node = cur_node.next

            # Modify node for saving
            cur_node.child = None
            cur_node.prev = self.cur_node
            cur_node.next = None

            # Add the node to our global list
            # and slide over nodes
            self.cur_node.next = cur_node
            self.prev_node = self.cur_node
            self.cur_node = cur_node

            # We've got kids, explore
            if chld_node:
                self.walk2(chld_node)

            # Crawl down original path
            cur_node = next_node

    def dfs(self, head: 'Node') -> 'Node':
        """Apply depth first search

        Time Complexity
            O(N)
        Space Complexity
            O(N) stack of all nodes
        """

        # Null case
        if not head:
            return None

        # Create dummy node
        dummy = Node(None, None, None, None)

        # Create global variables
        prev_node = dummy
        stack = [head]

        while stack:

            # Grab off stack
            cur_node = stack.pop()

            # Connect to previous
            # ie: prev <-> current
            prev_node.next = cur_node
            cur_node.prev = prev_node

            # Add next node
            if cur_node.next:
                stack.append(cur_node.next)

            # Add children (we want to dive into this first)
            if cur_node.child:
                stack.append(cur_node.child)

                # Remove child pointer
                cur_node.child = None

            # Slide over
            prev_node = cur_node

        # Remove connection of first real node to dummy
        dummy.next.prev = None

        return dummy.next


def generate_example1() -> 'Node':

    # Odd numbered case
    ll = Node(1)
    ll.next = Node(2, ll)

    ll.next.next = Node(3, ll.next)
    ll.next.next.next = Node(4, ll.next.next)
    ll.next.next.next.next = Node(5, ll.next.next.next)
    ll.next.next.next.next.next = Node(6, ll.next.next.next.next)

    # Tier 2
    t2 = Node(7)
    t2.next = Node(8, t2)
    t2.next.next = Node(9, t2.next)
    t2.next.next.next = Node(10, t2.next.next)

    ll.next.next.child = t2

    # Tier 3
    t3 = Node(11)
    t3.next = Node(12, t3)
    t2.next.child = t3

    return ll


if __name__ == '__main__':

    # Even numbered case
    # ll = ListNode(4)
    # ll.next = ListNode(2)
    # ll.next.next = ListNode(1)
    # ll.next.next.next = ListNode(3)

    ll = generate_example1()

    obj = Solution()
    # ans = obj.flatten(ll)
    ans = obj.dfs(ll)

    print('Forward crawl')
    print(ans)

    # Find the last node
    cur_node = ans

    while cur_node:
        prev_node = cur_node
        cur_node = cur_node.next

    print(f'Last Node: {prev_node}')

    print('Backwards crawl')
    # Crawl towards front
    cur_node = prev_node
    while cur_node:
        print(f'{cur_node.val} <-> ', end='')
        cur_node = cur_node.prev
