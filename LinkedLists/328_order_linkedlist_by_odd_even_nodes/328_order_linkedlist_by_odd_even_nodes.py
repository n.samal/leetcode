"""
Inputs:
    head (ListNode): start of linked list
Outputs:
    ListNode: start of new list
Goal:
    - group all odd numbered nodes followed by all even numbered nodes

Strategies

    Even List & Odd List
        - create an even list
        - create an odd list
        - add values to them as we see

        - at the end connect the even to the start of the odd list
    
        Time Complexity
            O(n)
        Space Complexity
            O(1)

"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def oddEvenList(self, head: ListNode) -> ListNode:
        
        # Dummy heads
        even_head = ListNode(None)
        odd_head = ListNode(None)
        
        # Current positions in new lists
        cur_even = even_head
        cur_odd = odd_head
        
        # Current position in normal list
        cur_node = head
        ix: int = 1
            
        while cur_node:
            
            # Even Node
            if ix % 2 == 0:
                cur_even.next = cur_node
                cur_even = cur_node
            
            # Odd node
            else:
                cur_odd.next = cur_node
                cur_odd = cur_node
            
            # Crawl down original path
            ix += 1
            cur_node = cur_node.next
        
        # Connect the last odd to the evens start
        cur_odd.next = even_head.next
        
        # Disconnect remaining pointers
        cur_even.next = None
        
        return odd_head.next
            
            
        
        