"""

Cases
    
    Inputs

        LinkedList

            null       : no nodes
            single node: 
            multi node : general case

        k (rotation):

            0     : no rotation, leave as is
            1 to n: general case
            > n   : rotation multiple times, we can just take the modulus and rotate by the remaining amount

Strategy

    Extra Space
        - iterate through the linked list, and save numbers into an array
        - slide from the back to the starting position and rebuild the linked list

    Maintain List

        - iterate through linked list and notate when we hit the end ie: .next = None
                count the length ie: n
        - hook the end of the list to the beginning of the list
                slide over to the new start ie: n - k + 1
                [1, 2, 3, 4, 5] if k = 2

                new_head = 4

                [4, 5, 1, 2, 3]

        - slide over n nodes
                set the last node to be the end ie: .next = None

                new_tail = 3
"""

# Definition for singly-linked list.

from typing import List


class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


def print_linked_list(linked_list_head):

    # Define head node
    current_node = linked_list_head

    print('Checking nodes')

    # Check links
    while current_node:

        # Output node value
        print(f'{current_node.val} -> ', end='')

        # Define next node
        current_node = current_node.next

    print('')


def create_linked_list(arr: List[int]):
    """ Create a linked list from an array"""
    # Create head node
    head = ListNode(arr[0])
    node = head

    # Create the next node
    for val in arr[1:]:

        # Create new node
        new_node = ListNode(val)

        # Linked last node to this
        node.next = new_node

        # Update our current node
        node = new_node

    return head


class Solution:

    def rotateRight(self, head: ListNode, k: int) -> ListNode:
        """Slide the linked list over by k values

        Args:
            head (ListNode): start of the linked list
            k (int): number of values to slide over
        """

        # If no nodes
        if not head:
            return head

        # No changes
        if k == 0:
            return head

        # Initialize node count and starting position
        n: int = 1
        node: ListNode = head

        print('Finding the end of list')

        # Iterate while we have values
        while node.next is not None:

            # Increment the count
            # move to the next position
            n += 1
            node = node.next

        print(f' n: {n}')

        # Remove extra loops
        # k = 10, n = 5 (two unnecessary loops)
        # k = 13, n = 5 => k = 3(remove two unnecessary loops)
        if k >= n:
            k = k % n

        ''' Start sliding over to new head'''

        # We have a multiple cycle, lets start from the beginning instead
        if k == 0:
            return head

        # Hook the last node to the first node
        node.next = head

        # Keep track of how many nodes we've slid over
        ix: int = 1
        node = head

        print('Finding new head')

        # Slide over to our new head
        # we must shift over one more value
        while ix <= n - k:

            print(' ', node.val)
            node = node.next
            ix += 1

        head_new = node

        ''' Set the new end of our list '''

        # Reset count and slide to the list length
        ix = 1

        print('Finding new end')

        # Slide to our last node
        while ix < n:
            print(' ', node.val)
            node = node.next
            ix += 1

        node.next = None

        return head_new

    def rotateRight2(self, head: ListNode, k: int) -> ListNode:
        """Slide the linked list over by k values

        Save the prior node during our iteration process to node: n - k + 1
        This will save us the time of iterating n more nodes to set
        the new cut off

        Args:
            head (ListNode): start of the linked list
            k (int): number of values to slide over
        """

        # If no nodes
        if not head:
            return head

        # No changes
        if k == 0:
            return head

        # Initialize node count and starting position
        n: int = 1
        node: ListNode = head

        print('Finding the end of list')

        # Iterate while we have values
        while node.next is not None:

            # Increment the count
            # move to the next position
            n += 1
            node = node.next

        print(f' n: {n}')

        # Remove extra loops
        # k = 10, n = 5 (two unnecessary loops)
        # k = 13, n = 5 => k = 3(remove two unnecessary loops)
        if k >= n:
            k = k % n

        ''' Start sliding over to new head'''

        # We have a multiple cycle, lets start from the beginning instead
        if k == 0:
            return head

        # Hook the last node to the first node
        node.next = head

        # Keep track of how many nodes we've slid over
        ix: int = 1
        node = head

        print('Finding new head')

        # Slide over to our new head
        # we must shift over one more value
        while ix < n - k + 1:

            print(' ', node.val)

            # Save old node
            node_prev = node

            # Grab next node
            node = node.next
            ix += 1

        head_new = node

        # Set the previous node as the end
        node_prev.next = None

        return head_new


if __name__ == "__main__":

    arr = [1, 2, 3, 4, 5]

    head = create_linked_list(arr)
    # print_linked_list(head)

    s = Solution()
    # new_head = s.rotateRight(head, 2)
    new_head = s.rotateRight2(head, 2)

    print_linked_list(new_head)

