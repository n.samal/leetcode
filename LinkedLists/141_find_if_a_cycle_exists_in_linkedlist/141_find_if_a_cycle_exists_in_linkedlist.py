"""
Inputs:
    head (ListNode): start of a linked list
Outputs:
    bool: is there a cycle?
Goal
    - determine if there's a cycle

Cases
    
    Null
    Single
    2 to n

Strategy

    Memory via Hash Table
        - keep track of where we have been
        - if we ever revist the same node (same hash value)
        then we have been there before
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
    
    Fast & Slow
        - run two pointers at different speeds
            + imagine two runners on a track, if there's a loop somewhere
            the fast runner will eventually overlap / intersect witht the slow runner

        - if the two nodes are equal before the fast node reaches the 
        end then there's a cycle
            
        Time Complexity
            O(n)
        Space Complexity
            O(1)
"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def hash_table(self, head: ListNode) -> bool:
        """Use a hash to remember where we've been"""
        
        s = set()
        
        cur_node = head
        
        while cur_node:
            
            # Have we been here before?
            if cur_node in s:
                return True
            else:
                s.add(cur_node)
            
            cur_node = cur_node.next
        
        # We reached the end
        return False
    

    def fast_slow1(self, head: ListNode) -> bool:
        """Use slow pointer, and a fast pointer
        
        - start the fast pointer one ahead, so we don't check on the 
        first node

        Notes:
        	is a bit slower to reach convergence
        """
        
        # Null case, or single case
        if not head or not head.next:
            return False
        
        # Start the fast one ahead
        slow = head
        fast = head.next
        
        # We have 2 fast places
        while fast and fast.next:
            
            if slow == fast:
                return True

            # Move the pointers forward
            slow = slow.next
            fast = fast.next.next
            
        # We reached the end
        return False


    def fast_slow2(self, head: ListNode) -> bool:
        """Use slow pointer, and a fast pointer
        
        - only check once we've moved past the first node

        Notes:
        	quite fast!
        """
        
        # Null case, or single case
        if not head or not head.next:
            return False
        
        # Start the pointers at the same place
        slow = head
        fast = head
        
        # We have 2 more nodes to check
        while fast and fast.next:
            
            # Move the pointers forward
            slow = slow.next
            fast = fast.next.next
            
            # Check after we move past the first node          
            if slow == fast:
                return True

        # We reached the end
        return False
