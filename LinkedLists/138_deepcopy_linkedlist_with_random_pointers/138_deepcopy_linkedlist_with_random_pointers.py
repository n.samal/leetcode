"""
Inputs
    head (Node): head node of linked list 
Outputs
    Node: 
Goals
    - make deep copy of the items

Ideas
    - use deepcopy (=D)
    
    - pointers of the old objects won't cut it
        + we need to create new nodes that mimic that same exact values
    
    - create a copy of values for newly encountered nodes
        + save these so we can refernce them again later

    - for nodes that we need to reference
        + save the hash for the old object
        + save the hash for the new object
        + when we were supposed to reference the old object
        connect the link to the newly created object instead

"""


"""
# Definition for a Node.
class Node:
    def __init__(self, x: int, next: 'Node' = None, random: 'Node' = None):
        self.val = int(x)
        self.next = next
        self.random = random
"""

class Solution:
    def copyRandomList(self, head: 'Node') -> 'Node':
        """Iteratively create copies of the current node
        
        Time Complexity
            O(n) run through all the nodes once
        Space Complexity
            O(n) save all the nodes once
        """
        
        # Null case
        if not head:
            return None
        
        cur_node_old = head
        self.seen = {}
        
        while cur_node_old:
            
            # Get a copy of the new node or create it
            cur_node_new = self.create_node_copy(cur_node_old)
            
            # Grab the next nodes
            cur_node_old = cur_node_old.next
            cur_node_new = cur_node_new.next
        
        # Output the copy of the new head
        return self.seen[head]
    
    def create_node_copy(self, old_node: 'Node') -> 'Node':
        """Recursively save copies of the old nodes
        
        - save the value
        - save a new copy of the pointers
        - save references to the new nodes
        """
        # Null case
        if not old_node:
            return None
        
        # We've never seen this node before
        if old_node not in self.seen:
            
            # Save the new node, using the old nodes ID
            new_node = Node(old_node.val, None, None)
            self.seen[old_node] = new_node

            # Create a copy of associated values if they exist
            if old_node.next:
                new_next = self.create_node_copy(old_node.next)
                new_node.next = new_next
            
            if old_node.random:
                new_rand = self.create_node_copy(old_node.random)
                new_node.random = new_rand
        
        return self.seen[old_node]
