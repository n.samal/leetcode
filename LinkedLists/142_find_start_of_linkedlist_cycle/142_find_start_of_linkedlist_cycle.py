"""
Inputs:
    head (ListNode): beginning of linked list
Outputs:
    ListNode: location the cycle begins if it exists
Goals
    - determine the starting node of the cycle
    - if it doesn't exist return null
    
Ideas
    - use Floyds aka tortoise & hare method to determine if a cycle exists
    - use a hash table to save where we've been before

Strategies

    Extra Space
        - use a set to track where we've been before
        - iterate over the list
        - if we reach the end
            + no cycle exists
        
        - if we find somewhere we've been before, here's the start of the cycle
        
    
        Time Complexity
            O(n)
        Space Complexity
            O(n)

    Constant Space
        - find if a cycle exists with floyds
            + if no cycle, return null
        
        - if a cycle exists reference our calculation
            + we know the intersection point occurs when F = b
            + reset the pointers
                
                * one at the global head
                * one at the intersection point
            
            + when the nodes meet again, we're at the cycle start
        
        Time Complexity
            O(n)
        Space Complexity
            O(1)
"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def extra_space(self, head: ListNode) -> ListNode:
        """Extra Space
        
        - save where we've been before
        - if we've been there this is our cycle start
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        s = set([])
        
        cur_node = head
        
        while cur_node:
            
            # We've been here before
            if cur_node in s:
                return cur_node
            
            # Leave some breadcrumbs
            else:
                s.add(cur_node)
                
            # Advance pointer
            cur_node = cur_node.next
            
        # No cycle found            
        return None

    def detectCycle(self, head: ListNode) -> ListNode:
        """Constant Space"""
        
        ''' Phase 1: Floyds Detection '''
        
        slow = head
        fast = head
        int_p = None  # intersection point
        
        while fast and fast.next:
            
            slow = slow.next
            fast = fast.next.next
            
            if slow == fast:
                int_p = slow
                break

        # No intersection found
        if int_p is None:
            return None
        
        ''' Phase 2: Find start of cycle '''
        # From our calculations we know: F = b
        # Restart pointers at the begining and
        # one at the intersection point
        
        slow1 = head
        slow2 = int_p
        
        # The next time they meet is the start of the cycle
        while slow1 != slow2:
            
            slow1 = slow1.next
            slow2 = slow2.next
        
        return slow1
        
        