"""
Inputs:
    headA (ListNode): start of first linked list
    headB (ListNode): start of second linked list
Outputs:
    ListNode: where the intersection occurs if it does
Goal:
    - find the intersection of two linked lists

Strategy
    Find Length & Offset
        
        - find the length of listA
        - find the length of listB
        
        - compute the difference in length between both lists
        
        - start pointers at the head of both lists
            + give the longer list a head start by the difference
        
        - check if each node is equal
        
        Time Complexity
            O(n+m)
        Space Complexity
            O(1)

    Connect the loops
        - combine the loops together
        - when A reaches it's end connect it to B
        - when B reaches it's end connect it to A
        
        Example
        
            A: a1 -> a2 -> c1 -> c2 -> c3
            B: b1 -> b2 -> b3 -> c1 -> c3
            
            nodes match at c3

            Connect the ends

            A: a1 -> a2 -> c1 -> c2 -> c3 -> None -> b1   -> b2 -> b3 -> c1 -> c2 -> c3
            B: b1 -> b2 -> b3 -> c1 -> c2 -> c3   -> None -> a1 -> a2 -> c1 -> c2 -> c3
            
            they should either match at a node or match when both are None
            - their lenghths compliment each other
            
            total_length = a + b
            total_length = b + a
        
        Time Complexity
            O(n+m)
        Space Complexity
            O(1)
"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def offset(self, headA: ListNode, headB: ListNode) -> ListNode:
        """Offset intersection
        
        - find the length of each list
        - find the offset between lists
        - give the long list a head start
        
        - check if an intersection occurs between each nodes
        
        Time Complexity
            O(m+n)
        Space Complexity
            O(1)
        """
        n: int = 0
        m: int = 0
        
        cur_nodeA = headA
        cur_nodeB = headB
        
        while cur_nodeA:
            n += 1
            cur_nodeA = cur_nodeA.next
        
        while cur_nodeB:
            m += 1
            cur_nodeB = cur_nodeB.next
        
        # Compute difference
        diff = n - m
        ix: int = 0
        
        # Start pointers at beginning
        cur_nodeA = headA
        cur_nodeB = headB

        # List A longer than B, advance A ahead
        if diff > 0:
            
            while diff != 0:
                cur_nodeA = cur_nodeA.next
                diff -= 1
            
        # List B is longer than List A, advanced B ahead
        else:
                        
            while diff != 0:
                cur_nodeB = cur_nodeB.next
                diff += 1
            
        # Advance both pointers
        while cur_nodeA and cur_nodeB:
            
            # Found an equivalent node
            if cur_nodeA == cur_nodeB:
                return cur_nodeA
            
            # Advance both pointers
            cur_nodeA = cur_nodeA.next
            cur_nodeB = cur_nodeB.next
        
        # No intersection found
        return None
    
    def complimenting_loops(self, headA: ListNode, headB: ListNode) -> ListNode:
        """Connect the loops to one another

        - when A reaches it's end connect it to B
        - when B reaches it's end connect it to A
        
        - check if the values match
            + if no intersection they'll meet at the end at the same time
            + if an intersection they'll meet at the common node
        
        Time Complexity
            O(m+n)
        Space Complexity
            O(1)
        """
        
        # One of the lists is empty
        if not headA or not headB:
            return None
        
        # Start headers
        cur_nodeA = headA
        cur_nodeB = headB
        
        # Advance loops until they match
        while cur_nodeA != cur_nodeB:
            
            # print(f'curA: {cur_nodeA}')
            # print(f'curB: {cur_nodeB}')

            # A ended, hook up to B
            if cur_nodeA is None:
                cur_nodeA = headB

            # Keep crawling down A
            else:
                cur_nodeA = cur_nodeA.next
            
            # B ended, hook up to A
            if cur_nodeB is None:
                cur_nodeB = headA

            # Keep crawling down B
            else:
                cur_nodeB = cur_nodeB.next
        
        # Save intersection point
        return cur_nodeA
