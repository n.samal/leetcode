# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


def swap_pairs(head: ListNode) -> ListNode:
    """Swap alternate pairs in a singly linked list

    Time Complexity
        O(n)
    Space Complexity
        O(n)

    Args:
        head (ListNode): initial node in the linked list
    Returns:
        ListNode: list with swapped pairs
    References:
        https://leetcode.com/problems/swap-nodes-in-pairs/
    """

    arr = []
    ix: int = 0

    # Initialize node
    node = head

    while isinstance(node, ListNode):

        # Add value to array
        arr.append(node.val)

        # Odd indices
        if ix % 2 != 0:

            # Swap values
            arr[ix], arr[ix - 1] = arr[ix - 1], arr[ix]

        # Grab the next node
        node = node.next

        # Increment index
        ix += 1

    # Iterate through the array and create a new list
    print(arr)

    for i in range(ix):

        print(i)

        # Create head node
        if i == 0:
            link = ListNode(arr[i])

            # Current node
            node = link
        else:

            # Link the next node
            node.next = ListNode(arr[i])

            # Change the current node
            node = node.next

    return link


def swap_pairs2(head: ListNode) -> ListNode:
    """Swap alternate pairs in a singly linked list

    Time Complexity
        O(n)
    Space Complexity
        O(n)

    Args:
        head (ListNode): initial node in the linked list
    Returns:
        ListNode: list with swapped pairs
    References:
        https://leetcode.com/problems/swap-nodes-in-pairs/
    """

    # Looks like only a single node or less
    if not head or not head.next:
        return head

    # Init
    arr = []
    ix: int = 0

    # Initialize node
    node = head
    prev_node = None

    while isinstance(node, ListNode):

        # print(node.val)

        # Add value to array
        arr.append(node.val)

        # We have at least 2 elements
        if len(arr) > 1:

            if prev_node is None:

                # Grab latest element first
                head_new = ListNode(arr.pop())
                head_new.next = ListNode(arr.pop())

                # Store link to previous node
                prev_node = head_new.next

            else:

                # Grab latest element first
                prev_node.next = ListNode(arr.pop())
                prev_node.next.next = ListNode(arr.pop())

                # Set new previous
                prev_node = prev_node.next.next

        # Grab the next node
        node = node.next

    # Check for remaing elements in array
    if len(arr) > 0:
        prev_node.next = ListNode(arr.pop())

    return head_new


def swap_pairs3(head: ListNode) -> ListNode:
    """Swap alternate pairs in a singly linked list

    - maintain a list of two values
    - once we get two, swap the order using a stack

    Time Complexity
        O(n)
    Space Complexity
        O(1)

    Args:
        head (ListNode): initial node in the linked list
    Returns:
        ListNode: list with swapped pairs
    References:
        https://leetcode.com/problems/swap-nodes-in-pairs/
    """

    dummy = ListNode()
    node_new = dummy
    
    arr = []
    cur_node = head
    
    while cur_node:
        
        arr.append(cur_node)
        next_node = cur_node.next
        
        # Two nodes found, add in reverse order
        if len(arr) == 2:
            node_new.next = arr.pop()
            node_new = node_new.next
            
            node_new.next = arr.pop()
            node_new = node_new.next
            node_new.next = None
            
        # Move down original chain
        cur_node = next_node
    
    # Add any straglers
    if arr:
        arr[-1].next = None
        node_new.next = arr.pop() 
        
    return dummy.next


def swap_pairs4(head: ListNode) -> ListNode:
    """Swap alternate pairs in a singly linked list

    - maintain a list of two values
    - once we get two, swap the order using a stack

    Time Complexity
        O(n)
    Space Complexity
        O(1)

    Args:
        head (ListNode): initial node in the linked list
    Returns:
        ListNode: list with swapped pairs
    References:
        https://leetcode.com/problems/swap-nodes-in-pairs/
    """

    dummy = ListNode()
    dummy.next = head

    node_prev = dummy

    while head and head.next:

        # Rename for ease
        node1 = head
        node2 = head.next

        # Swap
        # Previous -> 2
        #        2 -> 1
        #        1 -> original next node of 2
        node_prev.next = node2
        node1.next = node2.next
        node2.next = node1

        # Realign for next iteration
        node_prev = node1
        head = node1.next

    return dummy.next



if __name__ == "__main__":

    ll = ListNode(1)
    ll.next = ListNode(2)
    ll.next.next = ListNode(3)
    ll.next.next.next = ListNode(4)
    ll.next.next.next.next = ListNode(5)

    # check = swap_pairs(ll)
    check = swap_pairs2(ll)

    # Check the node
    node = check

    while isinstance(node, ListNode):

        print(node.val, '-> ', end='')
        node = node.next

    print('')
