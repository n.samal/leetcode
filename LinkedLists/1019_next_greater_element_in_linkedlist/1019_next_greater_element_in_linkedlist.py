"""

Inputs
    head (ListNode): head of linked list
Outputs
    List[int]: values of the next greater value in the linked list
    
Goal
    - find the next greater value for each node in the list

Cases
    Null
        - no greater element possible
    Single Case
        - no greater element possible
    General
    Increasing Values
        - next value is greater element
    Decreasing Values
        - no greater element possible

Strategies

    Stack
        - use a monotonically decreasing stack to determine
        the next greater element for each 'index' in the linkedlist
        - we may need to iterate once through the list to determine
        the length of the array, then fill it on the second pass

"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def nextLargerNodes(self, head: ListNode) -> List[int]:
        """Monotonically decreasing stack
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        
        ''' First Pass '''
        n: int = 0
        node = head
        
        while node:
            n += 1
            node = node.next
        
        # Create an empty array
        arr = [0 for _ in range(n)]
        
        ''' Second Pass '''
        ix: int = 0
        node = head
        stack = []
        
        while node:
            
            # Found a larger value
            while stack and node.val > stack[-1][0]:
                
                # Mark the next greater value as current
                ix_prev = stack.pop()[1]
                arr[ix_prev] = node.val
            
            # Add the current value and index to the stack
            stack.append((node.val, ix))
                
            # Move to the next node
            ix += 1
            node = node.next
        
        return arr
            
            
            
