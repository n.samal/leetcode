from collections import deque

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:

    def out_of_place(self, head: ListNode) -> ListNode:
        """Out of place storage of values

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        Notes
            - can be improved, we spend alot of time iterating, reversing
            and iterating again

        """

        node = head
        arr = []

        # Grab next node
        while node:
            arr.append(node.val)
            node = node.next
        
        # Reverse array
        arr = arr[::-1]
            
        # Create new head node
        if arr:
            head_new = ListNode(arr[0])
            node_prev = head_new
        else:
            return None
        
        # Create current node
        for val in arr[1:]:
            node = ListNode(val)
            node_prev.next = node
            
            # Current node is the next nodes previous
            node_prev = node
        
        return head_new
            
        
    def in_place(self, head: ListNode) -> ListNode:
        """In place storage of values

        Time Complexity
            O(n)
        Space Complexity
            O(n)

        Notes
            - can be improved, we spend alot of time iterating, reversing
            and iterating again

        """

        cur_node = head
        prev_node = None

        # Grab next node
        while cur_node:

            # Save the original next node
            # 1.next -> 2
            next_node = cur_node.next

            # Connect current node to the previous node
            # since we're reversing
            # 1 -> None
            cur_node.next = prev_node

            # Save the current node for next time
            # current is our next previous node
            prev_node = cur_node

            # Continue with the original intended next node
            # cur_node = 2
            cur_node = next_node
        
        # Connect o the 
        return prev_node
            
        