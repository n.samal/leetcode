"""
Inputs
    head (ListNode): start of linked list
Outputs
    ListNode: middle node of the linked list
Goal
    - find the middle node of the linked list

Cases
    Null
    Single
    Double
    3 to n

    Odd Case
        1 -> 2 -> 3 -> 4 -> 5
      
         we want the middle, so n // 2 + 1
    
    Even Case

        1 -> 2 -> 3 -> 4 -> 5 -> 6
        
        we want to split after 3,
        
        so n // 2 + 1

Strategies

    Count then Find
        - crawl down the list once to find the length
        - calculate the middle as n // 2 + 1
        - crawl again until we are at the middle node
        
        Time Complexity
            O(n)
        Space Complexity
            O(1)
    
    Fast & Slow
        - use one fast pointer and one slow pointer
        - crawl the fast pointer at double speed
        - when the fast pointer has reached the end,
        the slow will be at the middle node
        
        Time Complexity
            O(n)
        Space Complexity
            O(1)
"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def fast_and_slow(self, head: ListNode) -> ListNode:
        """Fast & Slow pointers"""
        slow = head
        fast = head
        
        # We have more nodes to crawl to on the fast track
        while fast and fast.next:
            
            # Crawl to the next node
            slow = slow.next
            fast = fast.next.next
        
        return slow

    def middleNode(self, head: ListNode) -> ListNode:
        """Crawl once to find the list length, then again to find the middle"""
    
        # Count the length of the list
        n: int = 0
        cur_node = head
        
        while cur_node:
            n += 1
            cur_node = cur_node.next
        
        # Define the middle node index
        ix_mid = n // 2 + 1
        
        # Crawl to the middle node
        ix: int = 1
        cur_node = head
        
        while ix != ix_mid:
            ix += 1
            cur_node = cur_node.next 
        return cur_node
            
