"""
Inputs:
    ListNode: linked list in unknown order
Outputs:
    ListNode: newly sorted linked list
Goal:
    - reorder the linked list to the desired order

Cases

    Null
        []
    Single Node
        1
    Double Node
        1 -> 2
    
    Multinode (3 or more)
        Even Node Count
            
            1 -> 2 -> 3 -> 4
        
        Odd Node Count
        
            1 -> 2 -> 3 -> 4 -> 5
Strategy
    
    Queue
        - create a queue
        - iterate through the list and save pointers in the queue
        - iterate through the queue and pop from left and right
            
            + connect the nodes as we go
            + use an index to determine what side to grab from
        
        - on the last node, make sure it ends
            + node.next = None
            + otherwise we'll end in a cycle, since it wants to connect ot the original
            next value
    
        input: [1, 2, 3, 4, 5]
        goal:  [1, 5, 2, 4, 3]
        
        popleft = 1
        1.next

        Time Complexity
            O(n)
        Space Complexity
            O(n)

    Reverse Half

        - iterate through the list once to determine the length
        - iterate once again to find the middle node ie: n // 2
        
            + if an even count, start at mid + 1

                1 -> 2 -> 3 -> 4 -> 5 -> 6

            + if an odd count

                1 -> 2 -> 3 -> 4 -> 5

                start half2 at node 3 or 4, can use mid + 1

        - reverse the second half starting at node mid + 1

            1 -> 2 -> 3 -> 4 -> 5 -> 6

            creates

            1 -> 2 -> 3 ->
            4 -> 5 -> 6 ->

            reverse

            1 -> 2 -> 3 ->
            6 -> 5 -> 4 ->

        - connect left and right

            even index grabs from half 1
            odd index grabs from half 2

        Time Complexity
            O(n)
        Space Complexity
            O(1)
    Zig Zag

        - similar to the revese linked list method, let's merge two lists
        together by tracking our original intent

        original list: 1 -> 2 -> 3 -> 4 -> 5 -> 6
        mid_node = 4

        when we reverse the second list, the middle now points to None
        This creates the following

        list1: 1 -> 2 -> 3 -> 4
        list2: 6 -> 5 -> 4

        Notice that list 2 is short, and when can end when list2 has no next,
        since this is included in list1.next

        Example
        start with headers at each list

        half1 = head1 = 1
        half2 = head2 = 6

        keep crawling while the second list has items. we may not have cut off the pointer
        for the last node in half 1 ie: 3, so it may point to 4

        Loop 1

            save original nexts
                next1 = 2
                next2 = 5

            connect half1 to half2
                1 -> 6

            connect half2 to half1_next
                1 -> 6 -> 2

                half2.next = next1

        Loop 2
            Start nodes at their next intended position
            half1 = next1 = 2
            half2 = next2 = 5

            save originals
            next1 = half1.next = 3
            next2 = half2.next = 4

            connect half1 to half2

            1 -> 6 -> 2 -> 5

            connect half2 to half1_next

            1 -> 6 -> 2 -> 5 -> 3

        Loop 3

            half1 = 3
            half1.next = 4
            half2 = 4
            half2.next = None

            loop doesn't start since the nodes naturally end

        Time Complexity
            O(n)
        Space Complexity
            O(1)
"""

from collections import deque


class ListNode:
    """ Definition for singly-linked list."""

    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

    def __repr__(self):
        return f'{self.val} -> {self.next}'


class Solution:

    ''' Utility '''

    def reverse_linkedlist(self, head: ListNode) -> ListNode:
        """Reverse a linked list

        1 -> 2 -> 3 -> 4 -> 5

        goes to

        1 -> 2 -> 3 -> 4 -> 5

        Time Complexity
            O(n)
        Space Complexity
            O(1)

        Notes:
            this modifies the original list, which
            can be undesirable in production systems
        """

        cur_node: ListNode = head
        prev_node: ListNode = None

        while cur_node:

            # Original next node
            # 1 -> 2
            next_node = cur_node.next

            # Connect current to previous
            # 1 -> None
            cur_node.next = prev_node

            # Set current as our next previous
            # next_prev = 1
            prev_node = cur_node

            # Crawl down original next node
            # 2
            cur_node = next_node

        return prev_node

    ''' Utility: Find the middle node '''

    def get_middle_node1(self, head: ListNode, goal_ix: int) -> ListNode:
        """Crawl to the half way point

        - we stop right before the goal node
        - so our current node will be the goal node
         """

        ix: int = 1
        cur_node = head

        while ix < goal_ix:

            ix += 1
            cur_node = cur_node.next

        print(f'mid_node_val: {cur_node.val}')

        return cur_node

    def get_middle_node2(self, head: ListNode) -> ListNode:
        """Use a fast and slow pointer to find the midpoint

        - the fast pointer moves 2 points
        - when the fast can't move anymore, the slow
        should be at our goal node

        Example

        Even length
            let's track the indices for each loop

                1 -> 2 -> 3 -> 4 -> 5 -> 6
        slow:   0    1    2    3
        fast:   0         1         2    3

        Odd length
                1 -> 2 -> 3 -> 4 -> 5
        slow:   0    1    2
        fast:   0         1         2
        """

        slow = head
        fast = head

        while fast and fast.next:

            slow = slow.next
            fast = fast.next.next

        return slow

    ''' Extra Space '''

    def extra_space(self, head: ListNode) -> ListNode:
        """Save the pointers to a queue

        - alternate popping from the left or right side of the queue

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        # Save node pointers
        queue = deque([])
        node = head

        while node:
            queue.append(node)
            node = node.next

        n = len(queue)

        # Null or single node
        if n <= 2:
            return head

        # Grab a node from the left
        node = queue.popleft()
        head_new = node
        ix: int = 0

        while queue:

            # Connect current to the right most
            if ix % 2 == 0:
                node.next = queue.pop()

            # Grab left most
            else:
                node.next = queue.popleft()

            # Increment count
            ix += 1
            node = node.next

        # End the linked list
        node.next = None

        return head_new

    ''' Constant Space '''

    def reverse_half(self, head: ListNode) -> ListNode:
        """Reverse the second half of the list

        - alternate between grabbing from
        the first half and the second half


        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        ''' Count length of list '''
        cur_node = head
        n: int = 0

        while cur_node:
            n += 1
            cur_node = cur_node.next

        # Small node cases
        if n <= 2:
            return head

        mid_node_ix = n // 2 + 1
        print(f'n: {n}  mid_node: {mid_node_ix} ')

        mid_node = self.get_middle_node1(head, mid_node_ix)
        print(f'mid_node_val: {mid_node.val}')

        # Reverse half the list
        mid_node = self.reverse_linkedlist(mid_node)

        ''' Zig zag connect '''

        cur_node = head
        half1: ListNode = head.next
        half2: ListNode = mid_node
        ix = 1

        while ix < n:

            print(f'cur_node: {cur_node.val}')

            # Odd, grab from half 2
            if ix % 2 != 0:
                cur_node.next = half2
                half2 = half2.next

            # Even grab from half 1
            else:
                cur_node.next = half1
                half1 = half1.next

            cur_node = cur_node.next
            ix += 1

        # Ensure there's no loop
        cur_node.next = None

        return head

    def constant_space(self, head: ListNode) -> ListNode:
        """Use ideal methods to create a zigzag list

        - find the middle node with a fast and slow node
        - reverse the linked list
        - merged two linked lists together

        """

        # Find middle point
        mid_node = self.get_middle_node2(head)

        # Reverse second linked list
        # Note: this also breaks the first chain short!!
        # since we redirect the middle node to None

        # Even case:
        # list 1 original: 1 -> 2 -> 3 -> 4 -> 5 -> 6 ->
        # list 1 new     : 1 -> 2 -> 3 -> 4 ->
        # Odd case:
        # list 1 original: 1 -> 2 -> 3 -> 4 -> 5 ->
        # list 1 new     : 1 -> 2 -> 3 ->
        mid_node = self.reverse_linkedlist(mid_node)

        # Initialize pointers at start of first and second half
        half1 = head
        half2 = mid_node

        # Create a zigzag node while we have a valid node
        # Half2.next is key!
        while half2 and half2.next:

            # Save originals
            next_node1 = half1.next
            next_node2 = half2.next

            # Connect first to second head
            # Connect the second to original head1
            # 1 -> 6,  6 -> 2
            half1.next = half2
            half2.next = next_node1

            # Crawl down original lists
            half1 = next_node1
            half2 = next_node2

        return head


if __name__ == "__main__":

    # Odd case, even case
    # arr = [1, 2, 3, 4, 5]
    # arr = [1, 2, 3, 4, 5, 6]

    ll = ListNode(1)
    ll.next = ListNode(2)
    ll.next.next = ListNode(3)
    ll.next.next.next = ListNode(4)
    ll.next.next.next.next = ListNode(5)
    # ll.next.next.next.next.next = ListNode(6)

    obj = Solution()
    ans = obj.constant_space(ll)
