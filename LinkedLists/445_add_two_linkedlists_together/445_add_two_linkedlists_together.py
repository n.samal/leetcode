"""

Inputs
    
Outputs

Goals
    - compute the summation of the two 'values'
    - don't modify the lists if possible

Strategies

    Reverse lists and then add

    Extra Space: String Edition

        - extract values as strings, then convert to an int
        - combine the integers
        - convert into a string
        - create a new linked list

        Time Complexity
            O(n+m)
        Space Complexity
            O(n+m)

    Extra Space: List
        - store values into an array as we get them

            7 -> 2 -> 4 -> 3
            [7, 2, 4, 3]

            5 -> 6 -> 4

            [5, 6, 4]

        - compute the addition and carry over from right to left

            smallest digits first ie: ones, tens, hundreds

            arr1 = [7, 2, 4, 3]
            arr2 = [5, 6, 4]

            4 + 3 = 7
                digit = 7
                no carry over
            6 + 4 = 10
                digit = 0
                carry over = 1
            5 + 2 + 1 = 8
                digit = 8
                carry over = 0
            7 + 0 + 0 = 7
                digit = 7
                carry over = 0

            digits = [7, 0, 8, 7]
            digits = [ones, tens, hundreds, thousands]

            - create a linked list

                + grabs the largest digit places first
                + we can just pop off the largest values

        Time Complexity
            O(n+m)
        Space Complexity
            O(n+m)

"""

from typing import List

# Definition for singly-linked list.


class ListNode:

    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:

    ''' String Method '''

    def strings(self, l1: ListNode, l2: ListNode) -> ListNode:
        """Convert into strings, integers, then back into a string, then integer"""

        # Convert the numbers
        val = self.linkedlist_to_int(l1) + self.linkedlist_to_int(l2)

        # Convert back into a linked list
        s = str(val)

        head_dummy = ListNode()
        node = head_dummy

        for char in s:

            node.next = ListNode(int(char))
            node = node.next

        return head_dummy.next

    def linkedlist_to_int(self, head: ListNode) -> int:
        """Convert the linked list into an integer"""
        node: ListNode = head
        s: str = ''

        while node:

            s += str(node.val)
            node = node.next

        return int(s)

    ''' List Method '''

    def by_lists(self, l1: ListNode, l2: ListNode) -> ListNode:
        """Convert linkedlists into arrays, then sum"""

        # Turn linked lists into arrays
        arr1 = self.linkedlist_to_list(l1)
        arr2 = self.linkedlist_to_list(l2)

        ''' Add digit arrays together '''

        n: int = len(arr1)
        m: int = len(arr2)
        ix: int = n - 1
        jx: int = m - 1

        digits = []
        carry_over = 0

        # Iterate through the values backwards (smallest digit first)
        while ix >= 0 or jx >= 0:

            # Add the digits together
            sum = carry_over

            # Grab right digit from number 1
            if ix >= 0:
                sum += arr1[ix]
                ix -= 1

            # Grab right digit from number 2
            if jx >= 0:
                sum += arr2[jx]
                jx -= 1

            # Compute current digit, and carry over for next digit
            digits.append(sum % 10)
            carry_over = sum // 10

        if carry_over:
            digits.append(carry_over)

        # Create a linked list from digits (largest digits first)
        dummy_head = ListNode(None)
        node = dummy_head

        # grab the largest value first
        while digits:

            node.next = ListNode(digits.pop())
            node = node.next

        return dummy_head.next

    def linkedlist_to_list(self, head: ListNode) -> List[int]:
        """Convert the linked list into an array of integer"""
        node: ListNode = head
        arr = []

        while node:

            arr.append(node.val)
            node = node.next

        return arr

    def right_to_left(self, l1: ListNode, l2: ListNode) -> ListNode:
        """Use arithmetic to create numbers and then split into digits

        - grab values from each list
        - add the numbers
        - generate the new linked list from
        rightmost digit to leftmost

        Time Complexity
            O(m+n)
        Space Complexity
            O(~n)
        """
        
        # Get the first number
        node = l1
        num1: int = 0
            
        while node:
            num1 = num1*10 + node.val
            node = node.next
        
        # Get the second number
        node = l2
        num2: int = 0
            
        while node:
            num2 = num2*10 + node.val
            node = node.next
        
        ''' Calculate the new number '''
        num = num1 + num2
        
        if num == 0:
            return ListNode(0)
        
        # Find the digits backwards
        # then build the linked list from right to left
        next_node = None
        
        while num:
            
            div, mod = divmod(num, 10)
            
            # Connect this to the value on right
            node = ListNode(mod)
            node.next = next_node
            
            # Save for next iteration
            next_node = node
            num = div
        
        return next_node

