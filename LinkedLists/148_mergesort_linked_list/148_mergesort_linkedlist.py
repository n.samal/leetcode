

class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

    def __repr__(self):
        return f'{self.val} -> {self.next}'

    def __str__(self):
        return f'{self.val} -> {self.next}'


class Solution:

    def merge_sort_linked_list(self, head: ListNode) -> ListNode:
        """Apply merge sort to a linked list

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        ''' Find the half way point of the list '''

        slow = head
        fast = head
        prev_node = None

        while fast and fast.next:

            prev_node = slow

            slow = slow.next
            fast = fast.next.next

        # Our half way point: n //2 + 1
        mid_node = slow

        # If we have a single node, the mid_node = head
        if mid_node == head:
            return head

        print('Finding Midpoint')
        print(f'  prev_node: {prev_node.val}')
        print(f'  mid_node: {mid_node.val}')

        ''' Recurse on left and right halfs '''

        # Unhook the node pointing to our mid node
        prev_node.next = None

        print('Unsorted')
        print(f'  left: {head}')
        print(f'  right: {mid_node}')

        # Sort the halfs
        left = self.merge_sort_linked_list(head)
        right = self.merge_sort_linked_list(mid_node)

        print('Sorted')
        print(f'  left: {left}')
        print(f'  right: {right}')

        ''' Recombine nodes '''
        dummy_head = ListNode(None)
        cur_node = dummy_head

        while left and right:

            # Left is smaller, connect it
            if left.val <= right.val:
                cur_node.next = left

                cur_node = left
                left = left.next

            # Right is smaller
            else:
                cur_node.next = right

                cur_node = right
                right = right.next

        # Connect the remaining points
        if left:
            cur_node.next = left
        else:
            cur_node.next = right

        # Output head of sorted linked list
        return dummy_head.next


if __name__ == '__main__':

    # Even numbered case
    # ll = ListNode(4)
    # ll.next = ListNode(2)
    # ll.next.next = ListNode(1)
    # ll.next.next.next = ListNode(3)

    # Odd numbered case
    ll = ListNode(-1)
    ll.next = ListNode(5)
    ll.next.next = ListNode(3)
    ll.next.next.next = ListNode(4)
    ll.next.next.next.next = ListNode(0)

    obj = Solution()
    print(obj.merge_sort_linked_list(ll))
