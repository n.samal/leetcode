
"""

Inputs
    lists (List[ListNode]): array of sorted linked lists
Outputs
    ListNode: sorted linked list

Goal
    - merge the sorted linked lists

Strategy

    Heap

        - create an empty heap
        - create an empty link list
        - iterate through the lists
        - add values from each list if applicable
        - pop the smallest value
        - add it to the list

        Time O(n *log(k))
            n values total
            log(k) for each insertion
        Space O(n)
            O(k) for heap
            O(k) for lists of current k nodes
            O(n) for new linked list

    Sort then create

        - sorting O(n*log(n))
        - Space: O(n)
"""
from typing import List
import heapq


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:

    ''' Add all values then Sort '''

    def mergeKLists(self, lists: List[ListNode]) -> ListNode:
        """Heap based approach

        Time Complexity
            O(n*log(k))
        Space Complexity
            O(n) for creating the new linked list
        Notes:
            python doesn't limit the size of the heap, so we maintain all elements
        References:
            https://en.wikipedia.org/wiki/Heapsort
            https://www.geeksforgeeks.org/heap-sort/
        """

        h = []            # heap
        head = None       # head node
        node_prev = None  # previous node

        # Current node for each list
        # start with head of each linked list
        nodes = [node for node in lists]

        # While we have nodes to explore and values in our heap
        while any(nodes):

            # Go through all the lists
            for ix, link in enumerate(nodes):

                # print(f'List: {ix}')

                # Grab the current node value
                if link:

                    # print(f' {link.val}')

                    # Add values to the heap
                    heapq.heappush(h, link.val)

                    # Move down the linked list
                    nodes[ix] = link.next

            # Grab the smallest value
            node = ListNode(heapq.heappop(h))

            # Add to previous linked list
            if node_prev is None:
                head = node

            # Link to the last node
            else:
                node_prev.next = node

            # Save this for the next node
            node_prev = node

        # Continue building on the linked list if more elements
        while h:

            # Grab the smallest value and create a node
            node = ListNode(heapq.heappop(h))

            # Link to the last node
            node_prev.next = node

            # Save this for the next node
            node_prev = node

        return head

    def mergeKLists2(self, lists: List[ListNode]) -> ListNode:
        """Brute force

        Time Complexity
            O(n*log(n)) for sorting
        Space Complexity
            O(n) for new linked list
        """

        values = []

        # Go through each list
        for node in lists:

            while node:

                # Store the current value
                values.append(node.val)

                # Grab the next node
                node = node.next

        # Sort the values
        values = sorted(values)

        # Create a dummy head node
        head = ListNode()
        node_prev = head

        # Go through the rest of the values
        for value in values:

            # Create current node
            node = ListNode(value)

            # Link to last node
            node_prev.next = node

            # Save for next node
            node_prev = node

        # Our first node was a dummy, use the next one
        return head.next

    ''' Heap/Queue of Smallest k values '''

    def heap_of_k(self, lists: List[ListNode]) -> ListNode:
        """Maintain a heap of size k

        - maintain a heap of the k smallest values
        - grab the smallest item from the heap along with the list
        group index. Add that node to our global list
        - when we take from list i, we move that linked list 
        down to the next node, and add it to our heap for the next round
        - continue while we have more items to explore

        Time Complexity
            O(n*ln(k))
        Space Complexity
            O(n)

        Notes
            - to improve complexity we could use the node in place

        """

        heap = []
        dummy = ListNode()
        node_prev = dummy

        # Save heads of all lists, along with indice
        for ix, node in enumerate(lists):
            if node:
                heapq.heappush(heap, (node.val, ix))

        # We got more things to add
        while heap:

            value, ix = heapq.heappop(heap)

            # Connect to the main linked list
            node_cur = ListNode(value)
            node_prev.next = node_cur

            node_prev = node_cur

            # Add the next item from the linked list
            lists[ix] = lists[ix].next

            if lists[ix]:
                heapq.heappush(heap, (lists[ix].val, ix))                

        return dummy.next


if __name__ == '__main__':

    nodes = [None, None, 3]
    nodes2 = [None, None, None]
    nodes3 = [None, 5, None]

    print(any(nodes))
    print(any(nodes2))
    print(any(nodes3))
