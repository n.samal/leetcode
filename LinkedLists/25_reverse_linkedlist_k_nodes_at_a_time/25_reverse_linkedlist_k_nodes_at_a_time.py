"""
Inputs:
    head (ListNode): head node of linked list
    k (int): size of groupings
Outputs:
    ListNode: head node of new linked list
Goal:
    - reverse nodes of list k at a time
    - only constant memory is allowed

Ideas
    - create dummy node
    - apply reversal strategy for k nodes
        + call this iteratively
    
    - we may need to iterate k nodes first
        
        + we don't want partial iterations of k
    
    - we alteratively may want to determin n first,
    then determine the groupings

Strategies
    Iterative
        - create auxilarly function for reversal
        - connect reversed nodes to our new list
        - call the auxilarly function while we have
        
        Time Complexity
            O(n)
        Space Complexity
            O(1)

"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def reverseKGroup(self, head: ListNode, k: int) -> ListNode:
        
        # Null case
        if not head or k <= 1:
            return head
        
        # Calculate list length
        cur_node = head
        n: int = 0
        
        while cur_node:
            n += 1
            cur_node = cur_node.next
    
        # Calculate number of full groupings
        n_grps = n // k
        
        # Create dummy node
        dummy = ListNode(None)
        prev_good = dummy
        
        # Start back at the head
        cur_node = head
        
        for g_ix in range(n_grps):
            
            # print(f'Group: {g_ix}')
            
            prev_node = None
            rev_start = cur_node
            
            # Reverse next k nodes
            for ix in range(1, k + 1):
                
                # print(f' cur_node: {cur_node.val}  prev: {prev_node}')
                
                # Save original next node
                next_node = cur_node.next
                
                # Connect current to previous
                cur_node.next = prev_node
                
                # Move down original path
                prev_node = cur_node
                cur_node = next_node
            
            # Connect chain to last node for reversal
            # ie: 4
            # original = 1 -> 2 -> 3 -> 4
            # reversed = 4 -> 3 -> 2 -> 1
            prev_good.next = prev_node
            
            # Update our last good node, as the last node in the reversal
            # ie: start of reversal, 1
            prev_good = rev_start
            
        # Connect the remaining nodes if any
        # print(f'Remaining node: {cur_node.val}')
        prev_good.next = cur_node
        
        return dummy.next
