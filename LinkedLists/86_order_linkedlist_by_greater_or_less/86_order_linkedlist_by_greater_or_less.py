"""
Inputs:
    head (ListNode): start of linked list
Outputs:
    ListNode: start of organized list
Goals:
    - put all nodes less than x before all nodes greater than or equal to x
    - keep nodes in their relative order

Strategies

    Two Lists
        
        - create two lists
            + one less than x
            + one greater or equal to x
        
        - iterate through the chain and connect nodes
        based upon their node value
        
        - at the end connect the two list together
        - ensure no dangling pointers on the last node

        Time Complexity
            O(n)
        Space Complexity
            O(1)
"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def partition(self, head: ListNode, x: int) -> ListNode:
        """Two Pointers"""
        
        # New list headers
        less_head = ListNode(None)
        more_head = ListNode(None)
        
        # New list, current positions
        cur_less = less_head
        cur_more = more_head
        
        # Current list, current position
        cur_node = head
        
        while cur_node:
            
            # Value is smaller than x
            if cur_node.val < x:
            
                cur_less.next = cur_node
                cur_less = cur_node
            
            # Value is larger or equal to x
            # Connect links and advance
            else:
                cur_more.next = cur_node
                cur_more = cur_node
            
            # Move along original list
            cur_node = cur_node.next
            
        # Connect last less node to first greater node
        cur_less.next = more_head.next
        
        # Remove dangling pointers from last greater node
        cur_more.next = None
        
        return less_head.next
