"""

Inputs:
    root (ListNode): start of linked list
    k (int): number of splits
Outputs:
    List[ListNode]: array of linked lists
Goals
    - split a linked list into k parts
    - link lengths should be as equal as possible
    - no two parts should be different than more than 1
    - some parts can be null
    - parts should be in order of their occurence

Example
    
    Example 1
        root = 1 -> 2 -> 3
        k = 5
        
        output = 1, 2, 3, null, null

    Example 2
        root = 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10
        k = 3
        
        output
        1 -> 2 -> 3 -> 4
        5 -> 6 -> 7
        8 -> 9 -> 10
        
Ideas
    - determine the amount of nodes for each group
        n = total length of list
        general_amount = n // k
        remainder = n % k
        
        we want to sprinkle the remainder across the groups
        
        starting at the left most group drop off 1 of the remainders
    
    - seems similar in concept to text justification
        
Strategy

    Count & Distribute
        - iterate to determine the length of the list, n
        - determine the general amount per list, and the remainder
        
        - at each list take the general amount + 1 extra node if their is
        some remainder left
        
        - store the head of the linked list in the array
            + store the original next_node, our next starting point
            + remove the dangling pointer
                
                ie: 4 typically points to 5
                 if this is our split point break it
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)


"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def splitListToParts(self, root: ListNode, k: int) -> List[ListNode]:
        
        # Create k empty lists
        arr = [None for _ in range(k)]
        
        # Null list
        if not root:
            return arr

        # Get array length
        cur_node = root
        n: int = 0
        
        while cur_node:
            n += 1
            cur_node = cur_node.next
        
        # Calculate base amount and remainder
        n_base = n // k
        n_rem = n % k
        
        # Iterate through the linked list
        cur_node = root
        node_ix: int = 1
        
        # Calculate the goal amount for the first group
        # Take a remainder off, if any exists
        if n_rem > 0:
            goal_ix = n_base + 1
            n_rem -= 1
        else:
            goal_ix = n_base
        
        arr_ix: int = 0
        
        while cur_node:
            
            # Save original next node
            next_node = cur_node.next
            
            # First node in group, save the head pointer
            if node_ix == 1:
                arr[arr_ix] = cur_node
            
            # We're at node limit, cutoff pointer
            # Determine values for the next group
            if node_ix == goal_ix:
                
                cur_node.next = None
                
                node_ix = 0
                arr_ix += 1
                
                # Determine next groups goal count
                if n_rem > 0:
                    goal_ix = n_base + 1
                    n_rem -= 1
                else:
                    goal_ix = n_base

            # Advance pointer
            cur_node = next_node
            node_ix += 1

        return arr
