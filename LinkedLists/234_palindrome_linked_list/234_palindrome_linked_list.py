"""
Inputs:
    head: head node of linked list
Output:
    bool: is a palindrome
Goal:
    determine if this is a palindrome

Examples

    Example 1
        Input: 1 -> 2
        Output: false
    Example 2
        Input: 1 -> 2 -> 2 -> 1
        Output: true
    
    Example 3:
        
        Input: 1 -> 2 -> 3 -> 1 ->


Strategies
    
    Save & Check
        - iterate and save all values into an array
            + compute the length n
        - save all values into an array
        - check if the back end of the values matches the front
            + this can be done by a check with only the array
                return arr = arr[::-1]
            + we can also iterate through the linked list again
            and check against the top of the array
            
                l = 1 -> 2 -> 3 -> 4
                arr = [1, 2, 3, 4]
                
                if we pop from the end of the array, it's going in reverse
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
    
    Constant Space: Reverse Half
        - run through the list once to determine n
        - run through the list and grab the half way node
            + odd number of nodes
            
                1 -> 2 -> 3

                    or 

                1 -> 2 -> 3 -> 2 -> 4

                ignore the middle node ie: 2 in case 1, 3 in case 2

                the middle node is n // 2 + 1

                half1 = 1 to n // 2 inclusive
                half2 = n // 2 + 2 to end
            
            + even number of nodes
                
                we can split half and half ie: n // 2
            
                1 -> 2 -> 3 -> 2 -> 4 -> 5

                1 -> 2 -> 3 -> none

                5 -> 4 -> 2 -> none

        - reverse the second half of the linked list

            + even

                list = 1 -> 2 -> 3 -> 2 -> 4 -> 5

                half1     = 1 -> 2 -> 3 -> none
                half2     = 2 -> 4 -> 5 -> none

                half2 rev =  5 -> 4 -> 2 -> none

        - for each index check if it matches the mirror
            + if we reach the end with no problems, then we're good
        
        Time Complexity
            O(n?)
        Space Complexity
            O(1)

    Constant Space: 



"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def extra_space(self, head: ListNode) -> bool:
        """Save the values into a list, check the linked against this
        
        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """
        arr = []
        node = head
        
        while node:
            
            # Save current value, and iterate
            arr.append(node.val)
            node = node.next
        
        # Iterate through the list again, and check against
        # the reverse 
        node = head
        
        while node:
            
            if node.val != arr[-1]:
                return False
            else:
                arr.pop()
                node = node.next
        
        return True
    
    def constant_space(self, head: ListNode) -> bool:
        """Reverse half the linked list
        
        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """
        node = head
        n: int = 0
        
        # Increment node and iterate
        while node:
            n += 1
            node = node.next
        
        # Null case and single case
        if n <= 1:
            return True

        # Even: this is the last node in half1
        # Odd: this is the node right before the middle
        goal_ix = n // 2

        # Iterate through the list again, and find the half way point
        node = head
        ix: int = 1
        
        while ix < goal_ix:
            ix += 1
            node = node.next

        # Even case
        if n % 2 == 0:

            # Starting point for half 2
            half2_start = node.next
            
            # Early end to the half 1
            # node.next = None
        
        # Odd case: move one past middle node
        else:

            half2_start = node.next.next

        # Reverse the second half
        half2_start = self.reverse_linked_list(half2_start)

        # Iterate through halfs and check values
        node1 = head
        node2 = half2_start

        while node1 and node2:

            if node1.val != node2.val:
                return False
            else:
                node1 = node1.next
                node2 = node2.next
        
        return True

    def reverse_linked_list(self, head: ListNode) -> ListNode:
        """Reverse the linked list

        1 -> 2 -> 3 -> 4 -> 5 -> Null
        5 -> 4 -> 3 -> 2 -> 1 -> Null
        
        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        cur_node = head
        prev_node = None

        while cur_node:

            # Save original next node
            # 1 -> 2, next is 2
            next_node = cur_node.next

            # Overwrite the current nodes position
            # 1 -> null
            cur_node.next = prev_node

            # Save the current as our next previous
            # prev = 1
            prev_node = cur_node

            # Continue down original chain
            # current = 2
            cur_node = next_node

        return prev_node
        
        