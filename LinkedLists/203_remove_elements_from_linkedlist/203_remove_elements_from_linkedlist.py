"""
Inputs
    head (ListNode): start of linked list with unknown order
Outputs
    ListNode: new linked list without excluded value
Goal
    - remove nodes that have the specified value

Strategy

    Connect Desired Values
        - create a dummy header
        - crawl along the linked list
        - add values to our new list that are not the excluding values
        
        Time Complexity 
            O(n)
        Space Complexity 
            O(1)
"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def removeElements(self, head: ListNode, val: int) -> ListNode:
        """Create a new header with only desired values

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """
        # New linked list
        dummy_head = ListNode(None)
        prev_node = dummy_head
        
        # Original linked list
        cur_node = head
        
        while cur_node:
            
            # Not our excluded value, hook them up
            if cur_node.val != val:
                
                prev_node.next = cur_node
                prev_node = cur_node

            # Crawl down original
            cur_node = cur_node.next
        
        # Break any remaining links
        prev_node.next = None
        
        # Start of the new list
        return dummy_head.next


    def removeElements2(self, head: ListNode, val: int) -> ListNode:
        """Preserve the original next node

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """
        dummy = ListNode()
        node_new = dummy
        
        node = head
        
        while node:
            
            # Save original next node
            next_node = node.next
            
            # Add if not the value, but break the chain
            if node.val != val:
                node_new.next = node
                
                node_new = node_new.next
                node_new.next = None
            
            node = next_node
        
        return dummy.next