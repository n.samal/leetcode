"""
Inputs:
    head (Node): head of sorted circular linked list
    insertVal (int): value to insert into the list
Outputs:
    Node: pointer to head of new list

Goal:
    - insert a new node into the list so it stays sorted
    - output a null if the node is empty

Notes
    - the start of the list may not be the smallest value
    - the list can have duplicates

Cases
    
    - there is no current node (Null Case)
        + we must add it and create a cycle

    - insertVal is less than equal to min (Min Case)
    
        + insert the value before the first node
        + connect the last node to this node

    - insertVal is greater or equal to than max (Max Case)

        + insert the value after the max node
        + connect the new node to the min node
        + this works the same as the case above
        
    - insertVal fits inbetween (General Case)
        
        + find the insertion point then insert
        + here left <= value <= right
    

Example


    3 -> 4 -> 4 -> 1 -> 2 -> cycle

    check left <= value <= right

    Insert 0: Less than min

        0 >= 3 no and 0 <= 4 yes
        0 >= 3 no and 0 <= 4 yes


    Insert 2: General Case

    Insert 5: Greater than max

        original: 3 -> 4 -> 4 -> 1 -> 2 ->
        new     : 3 -> 4 -> 4 -> 1 -> 2 ->

        5 > 3 yes, 5 < 4 no
        5 > 4 yes, 5 < 4 no

        5 >= 4 and <= 1

Strategy
    Find Min Node, then insert

        - iterate once to find the smallest node
            + keep a pointer to this node
        - start iterating through the cycle again
        - find where the insert val is <= current value

            + if true, then insert here
            + if false, continue

        - there may be multiple mins, we want the first one!
            ie: 1 -> 1 -> 1 -> 2

        Time Complexity
            O(n)
        Space Complexity
            O(1)

    Left Right Compare

        - make sure we go through the entire cycle
            + this can be tricky, so we may need to use a while True type of
            statement with a exit out later

        - we can insert the value when

            left <= insertVal <= right

            + set left as the current node
            + set right as the next node

        - if after 1 cycle no values are satisfied, then it must be between
        the min and max nodes
            + the min should be the left most min
            + the max should be the right most max

        Time Complexity
            O(n)
        Space Complexity
            O(1)
"""


class Node:
    def __init__(self, val=None, next=None):
        self.val = val
        self.next = next

    def __repr__(self):
        return f'{self.val} -> {self.next}'


class Solution:
    def insert(self, head: 'Node', insertVal: int) -> 'Node':
        """Find the min and max values while finding the insertion

        Time Complexity
            O(n)
        Space Complexity
            O(1)

        Notes
            - we waste an iteration finding the min and max.
            the insertion point could be found in that time
        """
        # Initialize new node
        new_node = Node(insertVal)

        # No nodes currently, insert it
        if not head:
            new_node.next = new_node
            return new_node

        ''' Find the smallest and largest nodes '''
        min_node = head
        max_node = head

        # Start past the head
        prev_node = head
        cur_node = head.next
        inserted: bool = False

        # We haven't made a full cycle
        while not inserted:

            print(f'prev_node: {prev_node.val}  cur_node: {cur_node.val}')

            # Track min and max
            if cur_node.val < min_node.val:
                min_node = cur_node
            if cur_node.val >= max_node.val:
                max_node = cur_node

            # We found our insertion point
            if prev_node and prev_node.val <= insertVal <= cur_node.val:

                inserted = True
                prev_node.next = new_node
                new_node.next = cur_node

            # Advance forward
            else:
                prev_node = cur_node
                cur_node = cur_node.next

            # We've made a full loop
            if prev_node == head:
                break

        # We reached the last node and haven't inserted anything
        if not inserted:
            max_node.next = new_node
            new_node.next = min_node

        return head

    def insert2(self, head: 'Node', insertVal: int) -> 'Node':
        """Find rotation point while finding the insertion

        - rotation point is where it goes from max to min

        Time Complexity
            O(n)
        Space Complexity
            O(1)

        Notes
            - be wary of the single node case
            - or some case we exit the loop without insertion
        """
        # Initialize new node
        new_node = Node(insertVal)

        # No nodes currently, insert it
        if not head:
            new_node.next = new_node
            return new_node

        # Start past the head
        prev_node = head
        cur_node = head.next

        # We haven't made a full cycle
        while True:

            print(f'prev_node: {prev_node.val}  cur_node: {cur_node.val}')

            # We found our insertion point
            if prev_node.val <= insertVal <= cur_node.val:
                break

            # Found rotation point
            # ie: 5 -> 5 -> 1
            elif prev_node.val > cur_node.val:

                # Large case or small case
                # 6 > 5 or 0 < 1
                if insertVal > prev_node.val or insertVal < cur_node.val:
                    break

            # Advance forward
            prev_node = cur_node
            cur_node = cur_node.next

            # We've made a full loop with no insertion
            if prev_node == head:
                break

        # Connect the nodes
        prev_node.next = new_node
        new_node.next = cur_node

        return head


if __name__ == '__main__':

    ll = Node(1)
    ll.next = Node(3)
    ll.next.next = Node(4)
    ll.next.next.next = Node(5)
    ll.next.next.next.next = ll

    obj = Solution()

    # General Case
    # print(obj.insert(ll, 2))

    # Min case
    print(obj.insert(ll, 0))

    # Max case
    # obj.insert(ll, 6)
