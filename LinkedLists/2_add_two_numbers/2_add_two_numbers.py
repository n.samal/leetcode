"""
Inputs:
    l1 (ListNode): linked list representing first number
    l2 (ListNode): linked list representing second number
Outputs:
    ListNode: summation of both numbers
Goal:
    - 

Ideas
    - length of numbers may be different ie: 99 + 1
    - the summation may have an additional digit ie: 99 + 1 = 100
    - we need to track the carry over between digits
    - the order of the linked list is beneficial
        + we get the ones digit first, tens, hundrends etc...
        + we can apply standard addition by hand
Strategies
    
    Extract Numbers then Combine
        - iterate through the linked list to determine the number
        - ie: 2 -> 4 -> 3 = 342
        - ie: 5 -> 6 -> 4 = 465
        - add the values as integers
        - create a new linked list from the values
        
    Simultaneous Addition
        - at each digit track the summation and the carry over
        - apply standard addition by hand

"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    
    def addTwoNumbers1(self, l1: ListNode, l2: ListNode) -> ListNode:
        """Apply hand based arithmetic
        
        
        Time Complexity
            O(m+n)
        Space Complexity
            O(~n)
        """
        head = ListNode(None)
        node = head
        carry_over: int = 0
        
        while l1 or l2:
            
            current: int = 0
            
            # Add current digit if it exists
            if l1:
                current += l1.val
                l1 = l1.next
            
            if l2:
                current += l2.val
                l2 = l2.next
            
            # Add carry over
            current += carry_over
            
            # Determine remainder
            if current >= 10:
                carry_over = 1
                digit = current - 10
            
            else:
                carry_over = 0
                digit = current
                
            # Save value into a new node
            # link it to our current node
            new = ListNode(digit)
            node.next = new
            
            # Set this as our next value
            node = new
        
        # Check for any remaining carry over
        if carry_over > 0:
            node.next = ListNode(carry_over)
        
        return head.next
        
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        """Apply hand based arithmetic
        
        - used modulus for tracking carry_over and digit
        
        Time Complexity
            O(m+n)
        Space Complexity
            O(~n)
        """
        head = ListNode(None)
        node = head
        carry_over: int = 0
        
        while l1 or l2:
            
            current: int = 0
            
            # Add current digit if it exists
            if l1:
                current += l1.val
                l1 = l1.next
            
            if l2:
                current += l2.val
                l2 = l2.next
            
            # Add carry over
            current += carry_over

            # Determine current digit and remainder
            # 16 -> 6, carry over = 1
            digit = current % 10
            carry_over = current // 10
                
            # Save value into a new node
            # link it to our current node
            new = ListNode(digit)
            node.next = new
            
            # Set this as our next value
            node = new
        
        # Check for any remaining carry over
        if carry_over > 0:
            node.next = ListNode(carry_over)
        
        return head.next
