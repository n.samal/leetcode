"""
Inputs:
    head (ListNode): start of sorted linked list
Outputs:
    ListNode: sorted linked list excluding any duplicate values
Goal:
    - remove values that show up in the list more than once

Ideas

    - if a value doesn't equal it's prior then add it
        + this can be tricky

        1 -> 1 -> 2

        here we add 1, since there is no prior.
        However on the next node, we need to get rid of it...

        It may be better to add a node once we've reached a different value

        ie: we we reach 2, check if 1 was actually a unique value

Cases

    Null
    Single
    Double

    All Duplicates
    All Unique (easy case)

    Some Unique (General Case)

Strategies
    Extra Space
        - iterate through the linked list and track the counts
        of each value we see in a hash
        - create a new linked list
        - iterate through the original linked list again
            + if the node is unique add it
            + if not ignore the node

        Time Complexity
            O(1)
        Space Complexity
            O(n)

    Constant Space: Track Counts

        - track a count of how many times we've seen a value
        - once we encounter a new node
            check if the previous value was unique, ie: seen = 1

            if unique add it to our new list

        - otherwise ignore it, and continue down the original list

        Time Complexity
            O(n)
        Space Complexity
            O(1)

    Compare to Left and Right

        - since the list is sorted, we know a value is unique only
        when it is different from it's left and right value

            1 -> 1 -> 2

        some values may not have a right, so we can just compare that to it's previous

        Time Complexity
            O(n)
        Space Complexity
            O(1)


    Rollback
        - add new values as we get them
        - if we see that value again, then we roll back to the
        previous good value

            + save 2 previous good nodes
            + when we roll back we cut the pointer from the "bad" node

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        
"""

# Definition for singly-linked list.


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        """Constant space: track counts

        - track the number of times we've seen a value
        - upon seen a new value, check if the previous value was unique
        """
        # New list
        prev_node_new = ListNode(None)
        dummy_head = prev_node_new
        seen: int = 0

        # Original list
        prev_node = ListNode(None)
        cur_node = head

        while cur_node:

            # Increment our count
            if cur_node.val == prev_node.val:
                seen += 1

            # Found a new value
            else:

                # If the old previous is unique, connect it
                if seen == 1:

                    prev_node_new.next = prev_node
                    prev_node_new = prev_node

                # Reset the count to 1, we've seen the current new value
                # once
                seen = 1

            # Move along original list and save current as next previous
            prev_node = cur_node
            cur_node = cur_node.next

        # Check if the last node was unique
        if seen == 1:
            prev_node_new.next = prev_node

        # Ensure the new list points to null
        else:
            prev_node_new.next = None

        # Grab first original node
        return dummy_head.next

    def left_right_check(self, head: ListNode) -> ListNode:
        """Compare the current node against the left and right nodes

        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """

        # New list
        dummy_head = ListNode(None)
        prev_good = dummy_head

        # Original List
        cur_node = head
        prev_node = ListNode(None)

        while cur_node:

            # Our left is different
            if cur_node.val != prev_node.val:

                # Is our right different?
                if cur_node.next is None or cur_node.val != cur_node.next.val:
                    prev_good.next = cur_node
                    prev_good = cur_node

            # Crawl down original path, save current as next previous
            prev_node = cur_node
            cur_node = cur_node.next

        # Disconnect any remaining pointers
        prev_good.next = None

        return dummy_head.next

    def rollback(self, head: ListNode) -> ListNode:
        """Save one extra previous, use this to rollback
        to a good known state
        """

        # New list
        dummy_head = ListNode(None)
        prev_good = dummy_head  # last good node
        prev_prev_good = dummy_head  # 2nd to last good node

        # Original List
        cur_node = head
        prev_node = ListNode(None)

        while cur_node:

            # Found a potential unique value, add it
            if cur_node.val != prev_node.val:

                # Slide over the new values, and connect to the current node
                # 1 previous -> 2 previous
                # current -> previous good
                prev_good.next = cur_node
                prev_prev_good = prev_good

                prev_good = cur_node

            # Found a non unique, roll back to a good value
            # and cut off the pointer
            else:

                prev_good = prev_prev_good
                prev_good.next = None

            # Save last node and crawl down path
            prev_node = cur_node
            cur_node = cur_node.next

        # Current off lingering node
        prev_good.next = None

        return dummy_head.next
