"""
Inputs:
    head
    m (int): start of node reversal (inclusive)
    n (int): end of node reversal (inclusive)
Outputs:
    head: start of linked list with reversal
Goal:
    - reverse part of the linked list

Strategies
    Find Reversal Start
    
        - crawl down the list until we find the starting reversal pointer
        
        1 -> 2 -> 3 -> 4 -> 5 -> 
        
        - here let's save the original next node
        
            original next = 2
        
        - start reversing nodes from m to n ie: 2 to 4
        - once we hit n, then we stop reversing
        
        - hook the original list to our reversed
        
            1 => 4 -> 3 -> 2
            
        - hook end of the reversed link to the original intent
        
            1 => 4 -> 3 -> 2
            
            2 => 5
            
            2 is the original next of 1
            5 is the original next of 4

    Time Complexity
        O(n)
    Space Complexity
        O(1)

"""


# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:

    def iterate1(self, head: ListNode, m: int, n: int) -> ListNode:
        """Iterative approach
        
        - find the starting reversal node
        - start reversing until we hit the ending reversal node
        - find the ending reversal node
        
        """
        
        
        # Create a new header
        dummy_head = ListNode(None)
        prev_good = dummy_head
        
        # Original list
        cur_node = head
        ix: int = 1
        
        # Find the node to start our reversal, until then hook up nodes like normal
        while cur_node and ix != m:
            
            # Connect to our header
            prev_good.next = cur_node
            prev_good = cur_node
            
            # Move to next node
            cur_node = cur_node.next
            ix += 1
            
        # print(f'cur_node: {cur_node.val}')
        # print('Reversal starts')
        
        # We're at start where we want to reverse
        rev_start = cur_node
        prev_node = None
        
        # Reverse nodes until our end point
        while cur_node and ix != n + 1:
            
            # print(f' cur_node: {cur_node.val}')
            
            # Save original next node
            next_node = cur_node.next
            
            # Hook to previous
            cur_node.next = prev_node
            
            # Move down original path, and save position
            prev_node = cur_node
            cur_node = next_node
            ix += 1
        
#         print('Reversal Ended')
#         print(f' cur_node: {cur_node.val}')
#         print(f' prev_node: {prev_node.val}')
#         print(f' next_node: {next_node.val}')
        
        # Hook 'normal' links to reversed links
        prev_good.next = prev_node
        
        # Hook end of 'reversed' links to rest of normal links
        rev_start.next = next_node
        
        return dummy_head.next
        
    def iterate2(self, head: ListNode, m: int, n: int) -> ListNode:
        """Iteratively connect nodes to a dummy head
        
        - try to clean up loops using fors instead
        
        Time Complexity
            O(n)
        Space Complexity
            O(1)
        """
                
        dummy_head = ListNode(None)
        prev_good = dummy_head
        
        # Original list
        cur_node = head
        
        # Find the node to start our reversal, until then hook up nodes like normal
        for ix in range(1, m):

            # Connect to our header
            prev_good.next = cur_node
            prev_good = cur_node
            
            # Move to next node
            cur_node = cur_node.next
            
        # print(f'cur_node: {cur_node.val}')
        # print('Reversal starts')
        
        # We're at start where we want to reverse
        rev_start = cur_node
        prev_node = None
        
        # Reverse nodes until our end point (inclusive)
        for ix in range(m, n + 1):

            # print(f' cur_node: {cur_node.val}')
            
            # Save original next node
            next_node = cur_node.next
            
            # Hook to previous
            cur_node.next = prev_node
            
            # Move down original path, and save position
            prev_node = cur_node
            cur_node = next_node
        
#         print('Reversal Ended')
#         print(f' cur_node: {cur_node.val}')
#         print(f' prev_node: {prev_node.val}')
#         print(f' next_node: {next_node.val}')
        
        # Hook 'normal' links to reversed links
        prev_good.next = prev_node
        
        # Hook end of 'reversed' links to rest of normal links
        rev_start.next = next_node
        
        return dummy_head.next
