"""
Inputs
    head (ListNode): head of linked list
Outputs
    ListNode: head of sorted linked list

Notes
    - apply insertion sort to linked list

"""
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def insertionSortList(self, head: ListNode) -> ListNode:
        """

        - create a dummy var to hold the started of sorted list
        - for each new node, crawl along the linked list and find
        the respective insertion point
            + insert the node by linked to the previous node
            and the next node

        Time Complexity
            O(n^2)
        Space Complexity
            O(1)
        """

        dummy = ListNode()
        node = head

        while node:

            # Save original next node
            nxt_node = node.next

            # Iterate across the sorted list and find the new starting point
            node_prev = dummy
            node_srt = dummy.next  # start of sorted list

            while node_srt:

                if node.val > node_srt.val:
                    node_prev = node_srt
                    node_srt = node_srt.next
                else:
                    break

            # Insert current node after smaller sorted node
            node_prev.next = node
            node.next = node_srt

            # Move to next original node        
            node = nxt_node

        return dummy.next
