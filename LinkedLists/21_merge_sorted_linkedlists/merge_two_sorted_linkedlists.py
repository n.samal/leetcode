# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

    def __str__(self) -> str:
        """Output nodes in our list ie: 1 -> 2 -> 3"""

        head = self
        s = ""

        while head:

            # Output current value
            s += str(head.val) + " -> "

            # Move to next node
            head = head.next

        return s

    def __repr__(self) -> str:
        """Object representation"""

        if self.next is None:
            return f"ListNode({self.val})"
        else:
            return f"ListNode({self.val}) -> ListNode({self.next.val})"


class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        """Sorted two singly linked lists (ascending order)

        Compare the items in the two lists and grab the smallest value

        Case 1:
            Values in L1 and L2
        Case 2:
            Values in L1, none in L2
        Case 2:
            Values in L2, none in L1

        if smaller item in L1 or no L2 items => pick L1
        if smaller item in L2 or no L1 items => pick L2

        Returns:
            ListNode: sorted list of integers
        """

        # Define the start of our merged linked list
        head = None

        # We have values
        while l1 or l2:

            # No L1 values
            if l1 is None:
                # Grab value and move to next node
                val = l2.val
                l2 = l2.next

            # No L2 values
            elif l2 is None:
                val = l1.val
                l1 = l1.next

            # L1 smaller value in L2
            elif l1.val <= l2.val:
                val = l1.val
                l1 = l1.next
            else:
                val = l2.val
                l2 = l2.next

            # Store next value
            if head is None:

                head = ListNode(val)
                node = head

            else:
                # Connect to the next node
                node.next = ListNode(val)

                # Store a pointer to the old node
                node = node.next

            # print(head)

        return head


    def mergeTwoLists2(self, l1: ListNode, l2: ListNode) -> ListNode:
        """Compare heads and join
        
        - compare the heads and grab the smallest value if it exists
        - uses a dummy head node
        
        Time Complexity
            O(n+m)
        Space Complexity
            O(n+m)
        """
        head = ListNode()
        node = head
        
        while l1 or l2:
            
            # Compare the head of each list
            
            # Only l1 exists
            if not l2 :
                value = l1.val
                l1 = l1.next
            
            # Only l2 exists
            elif not l1:
                value = l2.val    
                l2 = l2.next
                
            # L1 is smaller
            elif l1.val <= l2.val:
                value = l1.val
                l1 = l1.next
                
            # L2 is smaller
            else:
                value = l2.val    
                l2 = l2.next
            
            
            # Create the next node
            node.next = ListNode(value)
            
            # Set the new current
            node = node.next
            
        # Return the actual start node
        return head.next

    def mergeTwoLists3(self, l1: ListNode, l2: ListNode) -> ListNode:
        """Compare heads and join
        
        - uses a dummy head node
        - uses modify loop logic
        
        Time Complexity
            O(n+m)
        Space Complexity
            O(n+m)
        """
        head = ListNode()
        node = head
        
        while l1 and l2:
            
            # Compare the head of each list
            # L1 is smaller
            if l1.val <= l2.val:
                value = l1.val
                l1 = l1.next
                
            # L2 is smaller
            else:
                value = l2.val    
                l2 = l2.next
            
            
            # Create the next node
            node.next = ListNode(value)
            
            # Set the new current
            node = node.next
            
        # Determine what the next link should be
        if l1:
            node.next = l1
        else:
            node.next = l2

        # Return the actual start node
        return head.next
        


if __name__ == "__main__":

    print("Running test case")

    # 1 -> 2 -> 4
    l1 = ListNode(1)
    l1.next = ListNode(2)
    l1.next.next = ListNode(4)
    print("l1:", l1)

    # 1 -> 2 -> 4
    l2 = ListNode(1)
    l2.next = ListNode(3)
    l2.next.next = ListNode(4)
    print("l2:", l2)

    # Merge lists
    s = Solution()
    l3 = s.mergeTwoLists(l1, l2)
