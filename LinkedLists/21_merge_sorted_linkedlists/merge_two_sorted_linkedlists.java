/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */

class Solution {



    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        
        // Initialize values
        ListNode head = null;
        ListNode node = null;
        int val = 0;

        // We have values
        while (l1 != null || l2 != null){

            // No L1 values
            if (l1 == null){
                // Grab value and move to next node
                val = l2.val;
                l2 = l2.next;
            }
            // No L2 values
            else if (l2 == null){
                val = l1.val;
                l1 = l1.next;
            }
            // L1 smaller value in L2
            else if (l1.val <= l2.val){
                val = l1.val;
                l1 = l1.next;
            }
            else{
                val = l2.val;
                l2 = l2.next;
            }

            // Store next value
            if (head == null){

                head = new ListNode(val);
                node = head;
            }

            else{
                // Connect to the next node
                node.next = new ListNode(val);

                // Store a pointer to the old node
                node = node.next;
            }
            // print(head)
        }
        
        return head;
    }
}