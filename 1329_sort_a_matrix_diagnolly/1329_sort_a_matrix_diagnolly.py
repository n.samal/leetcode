"""
Inputs
    arr (List[List[int]]): matrix in unknown order 
Outputs
    List[List[int]]: sorted matrix with diagnols in ascending order
Notes
    - start sorting from left to right

Ideas

    - gather vaues in a diagnol group, then sort

    - compute diagnols using r - c
        + we need to start all fills at adjacent bounds

        # Indices
        #    0  1  2  3  4  5
        #  0
        #  1
        #  2
        #  3
        #  4
        #  5

"""

from typing import List


class Solution:
    def diagonalSort(self, arr: List[List[int]]) -> List[List[int]]:
        """

        Time Complexity
            O(r*c) + O(d*g*log(g))
        Space Complexity
            O(r*c)
        Notes
            - could make faster by only storing and sorting
            the current diagnol
        """

        n_rows: int = len(arr)
        n_cols: int = len(arr[0])

        arr_new = [[None for c in range(n_cols)] for r in range(n_rows)]

        # Store information for each diagnol
        diags = {}

        for r in range(n_rows):
            for c in range(n_cols):

                d = r - c

                if d not in diags:
                    diags[d] = []

                diags[d].append(arr[r][c])

        # Define the start of each diagnol
        starts = [(r, 0) for r in range(n_rows)] + [(0, c) for c in range(1, n_cols)]

        # Organize the diagnols
        for r_start, c_start in starts:

            d = r_start - c_start
            values = sorted(diags[d], reverse=True)

            r = r_start
            c = c_start

            while values:

                arr_new[r][c] = values.pop()

                r += 1
                c += 1

        return arr_new


if __name__ == '__main__':

    # Examples
    arr_in = [[3, 3, 1, 1], [2, 2, 1, 2], [1, 1, 1, 2]]

    obj = Solution()
    print(obj.diagonalSort(arr_in))
