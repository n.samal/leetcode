from typing import List


def is_group_valid(arr: List[str]) -> bool:
    """Check if the given group is valid

    Valid Instances
        - only one instance of each number
        - empty spaces are ignored
    Args:
        arr (List[str]):
    Returns:
        bool: valid or invalid sequence
    """

    # print("is_group_valid")
    # print(" ", arr, end="")

    # TODO: use a set

    # Init vars
    s = []

    for char in arr:

        # Empty space
        if char == ".":
            continue

        # Found a duplicate characters
        if char in s:
            print(" => False")
            return False
        else:
            s.append(char)

    # print(" => True")
    # All valid characters
    return True


def is_suduko_valid(arr: List[List[str]]) -> bool:
    """Check if the suduko chart is valid

    Args:
        arr (List[List[str]]): suduko grid
    Returns:
        bool: if the current grid is a valid puzzle
    """

    n_rows: int = len(arr)
    n_cols: int = len(arr[0])

    """ Check rows """
    rows_valid = all(is_group_valid(arr[ix]) for ix in range(n_rows))

    if rows_valid is False:
        return False

    """ Check columns """
    cols_valid = all(
        is_group_valid([arr[row_ix][col_ix] for row_ix in range(n_rows)])
        for col_ix in range(n_cols)
    )

    if cols_valid is False:
        return False

    """ Check groupings """

    # Define index groups
    g1 = [0, 1, 2]
    g2 = [3, 4, 5]
    g3 = [6, 7, 8]
    groups = [g1, g2, g3]

    # Build each box
    for gi in groups:
        for gj in groups:

            # Build array for groups
            arr_i = [arr[row_ix][col_ix] for row_ix in gi for col_ix in gj]

            # Check for validity
            if is_group_valid(arr_i) is False:
                return False

    return True


''' Attempt 2 '''

class Solution(object):
    
    def isValidSudoku(self, board: List[List[str]]) -> bool:
        """Check each grouping for uniqueness

        - check row groupings
        - check column groups
        - check each quadrant grouping
        """
        
        n_rows: int = len(board)
        n_cols: int = len(board[0])
            
            
        # Check every row for uniqueness
        for r in range(n_rows):
            
            row_set = set([])
            
            for c in range(n_cols):
                
                char = board[r][c]
                
                if char == '.':
                    continue
                
                if char in row_set:
                    return False
                else:
                    row_set.add(char)

        # Check every column for uniqueness
        for c in range(n_cols):
            
            col_set = set([])
            
            for r in range(n_rows):
                
                char = board[r][c]
                
                if char == '.':
                    continue
                
                if char in col_set:
                    return False
                else:
                    col_set.add(char)
        
        # Check groupings
        col_shift = 0
        row_shift = 0
        
        for col_shift in range(0, 9, 3):
            for row_shift in range(0, 9, 3):
        
                # print(f'col_shift: {col_shift}   row_shift: {row_shift}')
        
                g_set = set([])

                for c in range(col_shift, col_shift + 3):
                    for r in range(row_shift, row_shift + 3):

                        char = board[r][c]
                        # print(f'r: {r}  c: {c}  char: {char}')

                        if char == '.':
                            continue

                        if char in g_set:
                            return False
                        else:
                            g_set.add(char)
        
        return True


if __name__ == "__main__":

    # Valid
    arr_in = [
        ["5", "3", ".", ".", "7", ".", ".", ".", "."],
        ["6", ".", ".", "1", "9", "5", ".", ".", "."],
        [".", "9", "8", ".", ".", ".", ".", "6", "."],
        ["8", ".", ".", ".", "6", ".", ".", ".", "3"],
        ["4", ".", ".", "8", ".", "3", ".", ".", "1"],
        ["7", ".", ".", ".", "2", ".", ".", ".", "6"],
        [".", "6", ".", ".", ".", ".", "2", "8", "."],
        [".", ".", ".", "4", "1", "9", ".", ".", "5"],
        [".", ".", ".", ".", "8", ".", ".", "7", "9"],
    ]

    # Invalid
    arr_in = [
        ["8", "3", ".", ".", "7", ".", ".", ".", "."],
        ["6", ".", ".", "1", "9", "5", ".", ".", "."],
        [".", "9", "8", ".", ".", ".", ".", "6", "."],
        ["8", ".", ".", ".", "6", ".", ".", ".", "3"],
        ["4", ".", ".", "8", ".", "3", ".", ".", "1"],
        ["7", ".", ".", ".", "2", ".", ".", ".", "6"],
        [".", "6", ".", ".", ".", ".", "2", "8", "."],
        [".", ".", ".", "4", "1", "9", ".", ".", "5"],
        [".", ".", ".", ".", "8", ".", ".", "7", "9"],
    ]

    print(is_suduko_valid(arr_in))
