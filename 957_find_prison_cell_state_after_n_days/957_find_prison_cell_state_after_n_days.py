"""
Inputs
    cells (List[int]): initial state of prison cells
    N (int): number of days
Outputs
    List[int]: cell state after N days
Notes
    - There are 8 prison cells in a row, and each cell is either occupied or vacant.
    - each day the occupancy changes according the following

        + If a cell has two adjacent neighbors that are both occupied or both vacant, then the cell becomes occupied.
        + Otherwise, it becomes vacant.

    - 

Examples

    Example 1:

        Input: cells = [0,1,0,1,1,0,0,1], N = 7
        Output: [0,0,1,1,0,0,0,0]
        Explanation: 
        The following table summarizes the state of the prison on each day:
        Day 0: [0, 1, 0, 1, 1, 0, 0, 1]
        Day 1: [0, 1, 1, 0, 0, 0, 0, 0]
        Day 2: [0, 0, 0, 0, 1, 1, 1, 0]
        Day 3: [0, 1, 1, 0, 0, 1, 0, 0]
        Day 4: [0, 0, 0, 0, 0, 1, 0, 0]
        Day 5: [0, 1, 1, 1, 0, 1, 0, 0]
        Day 6: [0, 0, 1, 0, 1, 1, 0, 0]
        Day 7: [0, 0, 1, 1, 0, 0, 0, 0]

    Example 2:

        Input: cells = [1,0,0,1,0,0,1,0], N = 1000000000
        Output: [0,0,1,1,1,1,1,0]

Ideas

    - try brute force method
        + generate new state using previous values

    - see if patterns exists in the state space
        + could use bit representation of state to save space potentially

Key Point:

    - for this case there are 2 ^ 8 = 256 possible states
    - after 256 cycles we're guaranteed to encounter a previous state, and in this case we start observing a cycle
        + https://en.wikipedia.org/wiki/Pigeonhole_principle

    - find where the desired day is with respect to the cycle start, and use this instead

"""

from typing import List


class Solution:

    def brute_force(self, cells: List[int], N: int) -> List[int]:
        """Iterate through the cells states

        - compute the state for N days
        - return the final state

        Time Complexity
            O(N)
        Space Complexity
            O(1)
        Notes
            - times out
        """

        cur = cells  # current cell state
        n: int = len(cur)

        for day in range(N):

            next_row = [0 for _ in range(n)]

            # Compare adjacent values and update next state
            for ix in range(1, n - 1):

                if cur[ix - 1] == cur[ix + 1]:
                    next_row[ix] = 1
                else:
                    next_row[ix] = 0

            cur = next_row

        return cur

    def find_next_state(self, cells: List[int]):
        """Compare adjacent values and update next state

        |1|0|1| => |1|1|1|
        |0|0|0| => |0|1|0|
        """
        n: int = len(cells)

        next_row = [0 for _ in range(n)]

        for ix in range(1, n - 1):
            next_row[ix] = int(cells[ix - 1] == cells[ix + 1])

        return next_row

    def cycle_search(self, cells: List[int], N: int) -> List[int]:
        """Search for a cycle then use that knowledge

        - compute the state for N days
        - save previous state, and track if a cycle occurs
        - convert the query date to a day within our cycle
        window

        Time Complexity
            O(1)
        Space Complexity
            O(1)
        """

        cur = cells  # current cell state
        dct = {}

        for day in range(N):

            # print(f'day: {day} cur: {cur}')

            # Check for save state
            key = tuple(cur)

            # We've seen this state before, so a cycle occurs
            # find the relevant day within 1 cycle period
            # we only need to do (N - day) steps from here
            if key in dct:
                day_rel = (N - day) % (day - dct[key])
                return self.cycle_search(cur, day_rel)

            # Save the starting state for this day
            # and compute the next days state
            else:
                dct[key] = day
                cur = self.find_next_state(cur)

        return cur


if __name__ == '__main__':

    # Example 1
    # cells = [0, 1, 0, 1, 1, 0, 0, 1]
    # N = 7
    # ans = [0, 0, 1, 1, 0, 0, 0, 0]

    # Example 2
    # cells = [1, 0, 0, 1, 0, 0, 1, 0]
    # N = 1000000000
    # ans = [0, 0, 1, 1, 1, 1, 1, 0]

    # Example 
    # cells = [1,1,0,1,1,0,1,1]
    # N = 6
    # ans = [0,0,1,0,0,1,0,0]

    # Example
    cells = [0, 0, 0, 1, 1, 0, 1, 0]
    N = 574
    ans = [0, 0, 0, 1, 1, 0, 1, 0]

    obj = Solution()
    result = obj.cycle_search(cells, N)

    assert result == ans
