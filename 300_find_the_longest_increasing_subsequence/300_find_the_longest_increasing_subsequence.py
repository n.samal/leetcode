"""
Inputs
    arr (List[int]): unsorted array of integers
Outputs:
    int: length of longest increasing subsequence
Goals
    - find the length of the longest increasing subsequence
Examples
    Example 1
        Input: [10,9,2,5,3,7,101,18]
        Output: 4 
        Explanation: The longest increasing subsequence is [2,3,7,101], therefore the length is 4. 

Ideas

    - naive
        - from each starting point crawl down the path

        ie: top down

    - build upon previous paths

        ie: at 10, we know the longest path is 1, there are no previous paths
                [10]

            at 9, there are no previous smaller paths, we must start a new path
            our best path is 1
                [9]

            at 2, there are no previous smaller paths, must start a new path
                [2]

            at 5, there's one previous path available, let's add onto it
                [2, 5]

            at 3 there's one previous path available, let's add onto it
                [2, 3]

            at 7 there's one 3 possible paths available
                [2], [2, 5], [2,3]

                each of these will be bounded by the same value of 7.
                We can choose the path that results in the longest length

            at 101
                we have many paths available, choose the path that results
                in the longest length

            at 18
                many paths available choose the path that results in the longest length

    - at each new value, we want to find the longest subsequence just smaller than our current number. This looks like sorting ie: insertion sort

        + let's keep seed values for each

    - potentially build a monotonically increasing stack from starting point

Hand Calc

    [100, 2, 5, 3, 70, 101, 18]

    value = 100
        piles: [100]
    value = 2
        piles: [100], [2]
    value = 5
        piles: [100], [2], [2, 5]
    value = 3
        piles: [100], [2], [2, 5], [2, 3]

        ** we couldn't add onto [2,5] but start with [2]

"""

import bisect
from typing import List


class Solution:

    def lengthOfLIS(self, arr: List[int]) -> int:
        """Two loops

        - initialize an array for the Longest sequence ending
        at each value
        - at each position, see if there's a chain that ended
        at a smaller value
            + if so, let's add to that chain and see if it beats
            our previous best
        - track the length of the longest chains

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)

        Notes
            - search time at each value is expensive, can be make this faster?
            perhaps binary search
        """

        n: int = len(arr)
        ans: int = 1

        # Length is too small
        # ie: 0, 1
        if n <= 1:
            return n

        # Length of subsequences ending with the value from input
        # ie: [10, 9, 2, 5]
        piles = [1 for _ in range(n)]

        for ix in range(1, n):

            # Look at previous values
            # if it's smaller, then we can add onto that chain
            for jx in range(0, ix):

                if arr[ix] > arr[jx]:
                    piles[ix] = max(piles[jx] + 1, piles[ix])

            # Update our overall answer
            ans = max(piles[ix], ans)

        return ans

    def bisect_tails(self, arr: List[int]):
        """Use binary search to find chains
        the longest chain for a value smaller than our current value

        - for each possible chain length, store the smallest
        value that could end that chain
            ie: [1, 2], [1, 100] both of length 2.
            we store 2 since it's smaller

        - use binary search to find the longest subsequence with a smaller value

        Time Complexity
            O(n*log(n))
        Space Complexity
            O(n)
        """

        n: int = len(arr)
        ans: int = 1

        # Small lists ie: 0, 1
        if n <= 1:
            return n

        # For each possible chain length, store the last value in the chain
        # we want to store the smallest value there
        # ie: [length 1, length 2]
        # ie: [0, 100], [0, 2] are both chains of length 2
        # [0,2] is better for building chains
        chains = [float('inf') for _ in arr]

        for ix in range(n):

            # print(f'value: {arr[ix]}  chains: {chains}')

            # Store the smallest value for chain length of 1
            chains[0] = min(arr[ix], chains[0])

            ''' Find the longest chain, with a value smaller '''
            left = 0
            right = ix  # longest chain possible is bounded by our index

            while left <= right:

                mid = (left + right) // 2

                # We can add onto this chain, let's try a longer chain
                if chains[mid] < arr[ix]:
                    left = mid + 1
                else:
                    right = mid - 1

            # Store the smallest ending value
            # ie: [1, 2] instead of [1, 100]
            # note: our last left is 1 greater then our working location
            chains[left] = min(arr[ix], chains[left])

            # Store the longest chain so far
            # we must bump the index by 1
            # since index 0 represent length 1
            ans = max(left + 1, ans)

        print(f'chains: {chains}')

        return ans


if __name__ == '__main__':

    # Example 1
    arr_in = [10, 9, 2, 5, 3, 7, 101, 18]

    obj = Solution()
    print(obj.lengthOfLIS(arr_in))
    print(obj.bisect_tails(arr_in))
