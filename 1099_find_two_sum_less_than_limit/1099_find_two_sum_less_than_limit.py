"""
Inputs
    arr (List[int]): values in unknown order
    K (int): summation limit
Outputs
    int: max summation below limit

Notes
    - find the max summation for a pair of elements (i, j)
        + i < j
        + i + j < K

    - if no pair exists then return -1

Ideas

    - hashtable too expensive
        + we need to search backwards for every compliment to ensure
        a summation less than K

    - sorting may help us
        + use two pointers

            * can be helpful for a finding a singular value but
            not necessarily the best value

        + binary search

    - order is not critical here although stated
        + since we're considering pairs 

        (i,j) with i < j

        is nearly identical to (j, i). We will find
        the mirrored pair eventually, and in essence
        we really only care about the summation

Hand Calc

    arr     = [34, 23, 1, 24, 75, 33, 54, 8]
    K = 60

    arr_srt = [1, 8, 23, 24, 33, 34, 54, 75]
"""

class Solution:

    def brute_force(self, arr: List[int], K: int) -> int:
        """

        Time Complexity
            O(n^2)
        Space Complexity
            O(1)
        """        

        n: int = len(arr)
        max_sum: int = -1

        for ix in range(0, n - 1):
            for jx in range(ix, n):

                cur_sum = arr[ix] + arr[jx]

                if cur_sum < K:
                    max_sum = max(max_sum, cur_curm)

        return max_sum

    def twoSumLessThanK(self, arr: List[int], K: int) -> int:
        """Binary search on each value, search for the compliment

        - sort values
        - search for complimenting pair

        Time Complexity
            O(n*log(n))
        Space Complexity
            O(1)
        """

        arr = sorted(arr)
        n: int = len(arr)

        max_sum: int = -1

        for ix in range(n):

            # Search for the complimenting pair
            low: int = ix + 1
            high: int = n - 1

            while low <= high:

                mid = (low + high) // 2
                cur_sum = arr[mid] + arr[ix]

                # Found best pair
                if cur_sum < K:
                    max_sum = max(max_sum, cur_sum)
                    low = mid + 1

                # Too large
                else:
                    high = mid - 1

        return max_sum

