"""
Inputs
    s (str): main string
    p (str): anagram target
Outputs
    List[int]: starting indices of anagrams
Goals
    - an anagram is a word or phrase formed by reordering the letters of another word or phrase
    - find all start indices of p anagrams in s
Examples

    Example 1

        Input:
        s: "cbaebabacd" p: "abc"

        Output:
        [0, 6]

        Explanation:
        The substring with start index = 0 is "cba", which is an anagram of "abc".
        The substring with start index = 6 is "bac", which is an anagram of "abc".

    Example 2

        Input:
        s: "abab" p: "ab"

        Output:
        [0, 1, 2]

        Explanation:
        The substring with start index = 0 is "ab", which is an anagram of "ab".
        The substring with start index = 1 is "ba", which is an anagram of "ab".
        The substring with start index = 2 is "ab", which is an anagram of "ab".

Ideas

    - create a counter
        + determine how many of each character we need for the anagram

    - use a sliding window in the mainstring
        + move the sliding window and pop characters in and out
        + when the character count matches our target then 
        save our start index

    - be mindful of the character checking process
        + each time we add and remove characters we don't
        need to review all of the characters, just the ones that
        were added and removed

        + only track characters we need, and nothing else
        + maybe track the # needed, instead of the # we have
         then we can sum the number needed

            * when needed = 0. we match

"""

from typing import List


class Solution:
    def findAnagrams(self, s: str, p: str) -> List[int]:
        """Track characters in our pattern and hash

        - update the hash map as we slide our window
        - update the counts when we have a match

        Time Complexity
            O(s + p)
        Space Complexity
            O(p)
        """

        ans = []

        # Gather target string character counts
        counts_tgt = {}
        n_tgt: int = len(p)

        for char in p:
            counts_tgt[char] = counts_tgt.get(char, 0) + 1

        # Initialize sliding window
        counts_win = {}
        n_str: int = len(s)

        for char in s[:n_tgt]:
            if char in counts_tgt:
                counts_win[char] = counts_win.get(char, 0) + 1

        # Pointers for sliding window (inclusive, inclusive)
        start_ix = 0
        end_ix = n_tgt - 1

        while end_ix < n_str:

            # print(f'window: {s[start_ix:end_ix + 1]}')

            if counts_win == counts_tgt:
                ans.append(start_ix)

            # Remove trailing character
            if s[start_ix] in counts_tgt:
                counts_win[s[start_ix]] -= 1

            # Add leading character
            if end_ix + 1 < n_str and s[end_ix + 1] in counts_tgt:
                ch_w = s[end_ix + 1]
                counts_win[ch_w] = counts_win.get(ch_w, 0) + 1

            start_ix += 1
            end_ix += 1

        return ans


if __name__ == '__main__':

    # Example

    # Failed case
    s = "bpaa"
    p = "aa"

    obj = Solution()
    print(obj.findAnagrams(s, p))
