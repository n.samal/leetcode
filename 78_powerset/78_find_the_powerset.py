"""

Inputs
    nums (List[int]): array of values in unknown order
Outputs
    List[List[int]]: all subsets that can be created using the input values

Notes
    - output should not contain duplicate subsets

Strategy

    Queue

        - start a queue with a null set

        - pop the last item from the queue
        - iterate through all choices
            - new item = seed + current choices
            - add the new item to our queue, and our answers
            - if the new item is of length n, we're already as big as our array

                + don't add it to our queue
                + additionally don't add items to our queue 

                  if new_item[-1] > current choice

                  we'll use this to prevent duplicate subset values
                  ie: [1, 3] vs [3, 1], or [1, 2, 3] vs [3, 2, 1], [2, 3, 1], etc.

    Recursion
        - start a seed with a null set
        - start a empty list for our answers

        - iteration through our choices and add the current value to our seed
        - maintain the index of the current choice, to prevent it from being used again

            ie:
                initial: backtrack(seed, current_index)
                next:    backtrack(seed + [current], current_index + 1)

        - this uses the same principle as the queue to prevent duplicate subsets,
        but with a recursive function

    Bits

        - we know number of combinations/permutations possible
            + 2**n
            + every value will be included on excluded, ie: 0 or 1

        - iterate from 0 to the number of combinations (exclusive)
        - convert the current iteration to a number
        - the bit representation of the decimal number can be used to toggle on and off values

        index 0: 0000000000000000
        index 1: 0000000000000001
        index 2: 0000000000000011

        we grab values from our input array that correspond to 1, and add it to answer

        - we need a bitmask that is the samelength as our input array
            + extra processing can be used to match this ie: string.zfill()

        - at the end we should have all possible combinations

"""
from typing import List


class Solution:

    def subsets_queue(self, nums: List[int]) -> List[List[int]]:
        """ Queue based power set

        Time Complexity
            O(2^n * n)

            for 2^n combinations
            iterate through n choices

        Space Complexity
            O(2^n * n)

            we store all the permutations of max possible length n
        Notes:
            this method is quite fast
        """

        # Initialize an answer and queue
        ans = [[]]  # null is always a subset
        queue = [[]]
        n = len(nums)

        while queue:

            # Grab item off the queue
            seed = queue.pop()
            seed_len = len(seed)

            # Go through each seed
            for num in nums:

                # Only use a seed if the value is greater than
                # the last value in our array
                # **this avoids duplicates seeds ie: [1, 3], [3, 1]
                if seed_len > 0 and num > seed[-1]:

                    # Create the new seed
                    # ie: [] + [2]
                    # ie: [2] + [2]
                    seed_new = seed + [num]

                    # Save the new seed
                    ans.append(seed_new)

                    # Populate our queue if applicable
                    if len(seed_new) < n:
                        queue.append(seed_new)

                elif seed_len == 0:
                    # Create the new seed
                    # ie: [] + [2]
                    # ie: [2] + [2]
                    seed_new = seed + [num]

                    # Save the new seed
                    ans.append(seed_new)

                    queue.append(seed_new)
        return ans

    def subsets_bit(self, nums: List[int]) -> List[List[int]]:
        """Used bit representation of a number to create the power set

        Time Complexity
            O(2^n * n)

            iterate through n buckets
            for 2^n combinations

        Space Complexity
            O(2^n * n)

            we store all the permutations of max possible length n

        Notes:
            - we could shift the iteration limits to avoid having to create a bit array
            to match the array length ie: range(2**n, 2 **(n + 1))
        """

        ans = []
        n: int = len(nums)

        # Every number has two possible states: off or on
        # we incude or exclude the values
        n_combos: int = 2 ** n

        for ix in range(n_combos):

            sub = []

            # Create the bit representation of decimal number
            # and ignore the '0b' portion of bit string
            # ie: 0 = '0b0', 2 = '0b10'
            bit_str = bin(ix)[2:]

            # We need our bit mask to match our input array length
            # Zero padded to the left
            # ie: 0 = '0b0' we need '0b000'
            bit_mask = bit_str.zfill(n)

            # Manual version of zfill
            # prepend 0s
            # bit_mask = bit_str
            # while len(bit_mask) < n:
            #     bit_mask = '0' + bit_mask

            # Include toggled on bits
            sub = []

            for jx in range(n):
                if bit_mask[jx] == '1':
                    sub.append(nums[jx])

            # Add sub item to our subset
            ans.append(sub)

        return ans

    def subsets_bit2(self, nums: List[int]) -> List[List[int]]:
        """Used bit representation of a number to create the power set

        Time Complexity
            O(2^n * n)

            iterate through n buckets
            for 2^n combinations

        Space Complexity
            O(2^n * n)

            we store all the permutations of max possible length n

        Notes:
            - we could shift the iteration limits to avoid having to create a bit array
            to match the array length ie: range(2**n, 2 **(n + 1))
            - tied with the queue for the fastest method
        """

        ans = []
        n: int = len(nums)

        # Every number has two possible states: off or on
        # we incude or exclude the values
        n_combos: int = 2 ** n

        # Create a bit string 1 value beyond our bounds
        # ie: 8 instead of 0 to 7, we'll 4 bin buckets instead of 3
        # this value also has a leading 1 to ensure, 4 bin buckets are used
        # if a leading 0, those values are removed from the bit string
        # ie: 0b0 instead of 0b000
        # nth_bit = 1 << n

        for ix in range(n_combos):

            sub = []

            ''' Create the bit representation of decimal number '''
            # Use the OR operator on a extra long bit array with a leading '1'
            # to grab values with '1'. This ensures we have a long bit array
            # ie: 0b1000 | 0b0_ _ _=> 0b1_ _ _

            # Ignore the '0b' portion of bit string
            # and the leading 1
            bit_mask = bin(ix | n_combos)[3:]

            # Include toggled on bits
            sub = []

            for jx in range(n):
                if bit_mask[jx] == '1':
                    sub.append(nums[jx])

            # Add sub item to our subset
            ans.append(sub)

        return ans

    def subsets_backtrack(self, nums: List[int]) -> List[List[int]]:
        """Use backtracking to create permutations

        Notes:
            - second fastest of the methods
        """

        ans = [[]]
        n: int = len(nums)

        def backtrack(seed: List[int], ix_start: int):
            """Add remaining values to our seed value

            - Only consider values larger than ours to prevent duplicate tuples
            ie: [1, 3] instead of [3, 1]


            Time Complexity
                O(2^n * n)

                for 2^n combinations
                iterate through n choices

            Space Complexity
                O(2^n * n)

                we store all the permutations of max possible length n

            Args:
                seed: value to combine with
                ix_start: starting index in choices
            """

            # We're at the array length, stop!
            if len(seed) >= n:
                return

            # Iterate through remaining choices
            for ix in range(ix_start, n):

                # Combine value with seed
                new = seed + [nums[ix]]

                # Save it
                ans.append(new)

                # Add the new value as a seed and crawl
                # only include larger values to avoid repetition
                # ie: [1, 3] instead of [3, 1]
                backtrack(new, ix + 1)

        # Seed with an empty list and first index
        backtrack([], 0)

        return ans


if __name__ == '__main__':

    arr_in = [1, 2, 3]

    obj = Solution()
    print(obj.subsets_queue(arr_in))
    # print(obj.subsets_bit(arr_in))
    # print(obj.subsets_bit2(arr_in))
    # print(obj.subsets_backtrack(arr_in))
