"""
Inputs:
    source (List[str]): rows of C++ program

Outputs:
    List(str): cleaned string

Goals:
    - clean the C++ input string, and ignore comments

Notes:
    - There will be no control characters, single quote, or double quote characters. For example, source = "string s = "/* Not a comment. */";" will not be a test case.
    - It is guaranteed that every open block comment will eventually be closed, so /* outside of a line or block comment always starts a new comment.
    - implicit newline characters can be deleted by block comments.

Cases

    Single Line Comment
        // Hello there
    Multi Line Comment: All of line
        /* Close
        This 
        for me */
    Multi Line Comment: Partial line
        Hello /* Close
        This 
        for me */There
    
    Single line within a multiline comment?
        
        - only search for closing(*/) after we've found an opening 
    
Ideas
    
    - search for /, then look for the other characters
        + every comment contains a / in the pattern
        + // => ignore the rest of the current line
        + /* => start ignore pattern
        + */ => ending ignore pattern

    - Only look forward in the string!!!

        + in the case we have a /*/ we could double count the same pattern
        + first /* starts the pattern
        + next / closes the pattern even though * was in the previous pattern
"""


class Solution:
    def removeComments(self, source: List[str]) -> List[str]:
        """Remove comments from the input array
        
        - iterate through the string and find ignore patterns
        - only add characters when not in an ignore pattern

        Args:
            source (List(str)): array of strings
        Returns:
            List(str):

        Time Complexity
            O(S)
        Space Complexity
            O(S)
        """
        output = []
        ignore: bool = False 
        row_clean = ''     # cleaned string (we need this across rows)
        
        for row in source:
            
            n: int = len(row)  # length of string 
            ix: int = 0        # character index
            
            while ix < n:
                
                # Found start of single line comment: //
                # then ignore the rest of the row
                if not ignore and row[ix] == '/' and ix + 1 < n and row[ix+1] == '/':
                    break
                    
                # Found start of comment: /*
                elif not ignore and row[ix] == '/' and ix + 1 < n and row[ix+1] == '*':
                    ix += 1
                    ignore = True
                    
                # Found end of comment: */
                elif ignore and row[ix] == '*' and ix + 1 < n and row[ix + 1] == '/':
                    ignore = False
                    ix += 1

                # Not in a comment
                elif ignore is False:
                    row_clean += row[ix]
                
                # Move to next character
                ix += 1
            
            # Add the clean row, if it has information
            if row_clean and not ignore:
                output.append(row_clean)
                row_clean = ''
            
        return output

    def removeComments2(self, source: List[str]) -> List[str]:
        """Remove comments from the input array
        
        - store cleaned strings in an array

        Args:
            source (List(str)): array of strings
        Returns:
            List(str):

        Time Complexity
            O(S)
        Space Complexity
            O(S)
        Notes
            - not significantly faster
        """
        output = []
        ignore: bool = False 
        row_clean = []     # cleaned string (we need this across rows)
        
        for row in source:
            
            n: int = len(row)  # length of string 
            ix: int = 0        # character index
            
            while ix < n:
                
                # Found start of single line comment: //
                # then ignore the rest of the row
                if not ignore and row[ix] == '/' and ix + 1 < n and row[ix+1] == '/':
                    break
                    
                # Found start of comment: /*
                elif not ignore and row[ix] == '/' and ix + 1 < n and row[ix+1] == '*':
                    ix += 1
                    ignore = True
                    
                # Found end of comment: */
                elif ignore and row[ix] == '*' and ix + 1 < n and row[ix + 1] == '/':
                    ignore = False
                    ix += 1

                # Not in a comment
                elif ignore is False:
                    row_clean.append(row[ix])
                
                # Move to next character
                ix += 1
            
            # Add the clean row, if it has information
            if row_clean and not ignore:
                output.append("".join(row_clean))
                row_clean = []
            
        return output


if __name__ == '__main__':

    # Case 1
    source_in = ["/*Test program */", "int main()", "{ ", "  // variable declaration ", "int a, b, c;", "/* This is a test", "   multiline  ", "   comment for ", "   testing */", "a = b + c;", "}"]

    # Case 2
    source_in = ["a/*comment", "line", "more_comment*/b"]

    # Case Fail
    source_in = ["struct Node{", "    /*/ declare members;/**/", "    int size;", "    /**/int val;", "};"]

    obj = Solution()
    print(obj.removeComments(source_in))
