import itertools

class Solution:
    def largestTimeFromDigits(self, arr: List[int]) -> str:

        max_time = float('-inf')

        for d1, d2, d3, d4 in itertools.permutations(arr, 4):
            hour = d1 * 10 + d2
            minute = d3 * 10 + d4

            if minute > 59 or hour > 23:
                continue

            time = hour * 60 + minute

            if time > max_time:
                max_time = time
                hour = str(hour).zfill(2)
                minute = str(minute).zfill(2)
                time_str = f'{hour}:{minute}'

        if max_time == float('-inf'):
            return ""
        else:
            return time_str
