"""
Inputs
    arr (List[int]): array of 0s and 1s
Outputs
    int: find max length of contiguous subarray with equal 0 and 1
Ideas
    - use cumsum, then manually check all indice bounds
    - the balance subarray must be of even length

    - use -1 & +1 to identify balance
        + same strategy as balanced parenthesis
        + use hash set to identify location of last balance point

Hand Calcs

    arr     = [0,  1,  0, 1,  0,  0]
    cur_sum = [-1, 0, -1, 0, -1, -2]

    arr     = [0,   0,  0,  1,  1,  1,  0]
    cur_sum = [-1, -2, -3, -2, -1,  0, -1]

    length goes from
        left = -3 (inclusive)
        right = -2 (inclusive)

        or

        left = -2 (exclusive)
        right = -2 (inclusive)

    length goes from
        left = -2 (inclusive)
        right = -1 (inclusive)

    length goes from
        left = -1 (inclusive)
        right = 0 (inclusive)


    arr     = [ 0,  0,  1,  0,  0,  0,  1,  1]
    cum_sum = [-1, -2, -1, -2, -3, -4, -3, -2]

    left is not always -1 away. See below

    Exclusive to inclusive is the
    better approach

    arr     = [ 1,  1,  0,  1,  1,  0,  0,  1]
    cum_sum = [ 1,  2,  1,  2,  3,  2,  1,  2]
"""

from typing import List


class Solution:
    def findMaxLength(self, arr: List[int]) -> int:
        """Use integers to balance our summation

        - look for summations 

        Time Complexity
            O(n)
        Space Complexity
            O(n)
        """

        dct = {0: -1}  # base case for null
        cur_sum: int = 0
        max_len: int = 0

        for ix, val in enumerate(arr):

            if arr[ix] == 1:
                cur_sum += 1
            else:
                cur_sum -= 1

            # Only save the left most position
            # of each sum
            if cur_sum not in dct:
                dct[cur_sum] = ix

            else:
                # Find the balancing point (inclusive - exclusive)
                left = dct[cur_sum]

                cur_length = ix - left
                max_len = max(max_len, cur_length)

        return max_len


if __name__ == '__main__':

    # Example 1
    # arr = [0, 1]
    # arr = [0, 0, 0, 1, 1, 1, 0]

    # Example
    arr = [0, 0, 1, 0, 0, 0, 1, 1]

    obj = Solution()
    print(obj.findMaxLength(arr))
