"""
Inputs
    s (str): main string
    dict (List[str]): words 
Outputs
    str: string with query words bolded
Notes

    - add a closed pair of bold tag <b> and </b> to wrap the substrings in s that exist in dict
    - if two such substrings overlap, you need to wrap them together by only one pair of closed bold tag.
    - if two substrings wrapped by bold tags are consecutive, you need to combine them.

Examples

    Example 1:

        Input: 
        s = "abcxyz123"
        dict = ["abc","123"]
        Output:
        "<b>abc</b>xyz<b>123</b>"

    Example 2:

        Input: 
        s = "aaabbcc"
        dict = ["aaa","aab","bc"]
        Output:
        "<b>aaabbc</b>c"

"""

from collections import defaultdict
from typing import List


class Solution:
    def addBoldTag(self, s: str, dict: List[str]) -> str:
        """

        - search for each word in the main string
            + save the start and ending indices (inclusive, exclusive)
            + find all occurences of the query in the string

        - sort the intervals
            + merge adjacent/overlapping intervals
        - bold all the intervals, and add the other
        characters before and after
        """

        ans = []

        # Find the range of existence for each word
        # search for each query multiple times
        intervals = []

        for word in dict:

            start = s.find(word, 0)

            while start != -1:
                intervals.append([start, start + len(word)])
                start = s.find(word, start + 1)

        # No queries found
        if not intervals:
            return s

        # Sort intervals and merge them together
        intervals = sorted(intervals)
        merged = [intervals[0]]

        for start, stop in intervals[1:]:

            if start <= merged[-1][1]:
                merged[-1][1] = max(stop, merged[-1][1])
            else:
                merged.append([start, stop])

        # Values before first interval
        prev_stop = 0

        # Grab all intervals
        # Add values before and during current interval
        for start, stop in merged:

            ans.append(f"{s[prev_stop:start]}")
            ans.append(f"<b>{s[start:stop]}</b>")

            prev_stop = stop

        # Add remaining chars
        ans.append(s[prev_stop:])

        return ''.join(ans)


if __name__ == '__main__':

    # Example 1
    s = "abcxyz123"
    dictionary = ["abc", "123"]

    # Example 2
    s = "aaabbcc"
    dictionary = ["aaa", "aab", "bc"]

    # Multiple mergers
    s = "aaabbcc"
    dictionary = ["aaa", "aab", "bc", "aaabbcc"]

    # No queries
    s = "aaabbcc"
    dictionary = []

    # Adjacent intervals
    s = "aaabbcc"
    dictionary = ["a","b","c"]

    obj = Solution()
    print(obj.addBoldTag(s, dictionary))
