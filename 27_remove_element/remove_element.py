
def remove_element(arr: list, element: int) -> int:
    """Remove a specific value from an array

    Time Complexity
        O(n)
    Space Complexity
        O(1)

    Arg:
        arr (list): integers in unknown order
        element (int): element to remove
    Returns:
        int: number of values that aren't the element
    References:
        https://leetcode.com/problems/remove-element/
    """

    # No items in array
    if len(arr) == 0:
        return 0

    # Define indices
    ix_uq: int = 0  # index of non element values

    for ix in range(len(arr)):

        # Value isn't the element
        if arr[ix] != element:

            # Overwrite value with non element value
            arr[ix_uq] = arr[ix]

            # Increment unique count
            ix_uq += 1

    return ix_uq


if __name__ == "__main__":

    arr_in = [0, 1, 2, 2, 3, 0, 4, 2]
    n = remove_element(arr_in, 2)

    # arr_in = [2]
    # arr_in = [2, 4, 5]
    # n = remove_element(arr_in, 2)
    # n = remove_element(arr_in, 3)

    print(arr_in[:n])
