from typing import List


class Solution:

    def maxArea_naive(self, height: List[int]) -> int:
        """ Find the maximum area between two endpoints

        Try all combinations and store the max area

        Space Complexity
            O(1)
        Time Complexity:
            O(n^2)

        Args:
            height (List[int]): height of buckets
        Returns:
            int: maximum area between endpoints
        References:
            https://leetcode.com/problems/container-with-most-water/
        """

        # Grab number of bins
        n = len(height)
        max_area = 0

        # Not enough lines to hold water
        if n < 2:
            return max_area

        # Iterate overall
        for ix in range(n - 1):
            for jx in range(ix + 1, n):

                # Compute area between points
                area = (jx - ix) * min([height[ix], height[jx]])

                # Keep the largest area
                max_area = max(area, max_area)

        return max_area

    def maxArea(self, height: List[int]) -> int:
        """ Find the maximum area between two endpoints

        - Start pointers at the end points
        - Calculate the area at the given indices
        - Move the pointer inwards for the smaller endpoint (limiting point)

        Space Complexity
            O(1)
        Time Complexity:
            O(n)

        Args:
            height (List[int]): height of buckets
        Returns:
            int: maximum area between endpoints
        References:
            https://leetcode.com/problems/container-with-most-water/
        """

        # Grab number of bins
        n = len(height)
        max_area = 0
        ix = 0  # head pointer
        jx = n - 1  # tail pointer

        # Not enough lines to hold water
        if n < 2:
            return max_area

        while ix < jx:

            # Compute area between points
            area = (jx - ix) * min([height[ix], height[jx]])

            # Keep the largest area
            max_area = max(area, max_area)

            # Move the pointer that is smaller
            # Hopefully the next point is taller
            if height[ix] < height[jx]:
                ix += 1
            else:
                jx -= 1

        return max_area


if __name__ == '__main__':

    s = Solution()

    heights = [1, 8, 6, 2, 5, 4, 8, 3, 7]
    print(s.maxArea(heights))

