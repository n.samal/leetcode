"""
Inputs
    arr (List[int]): number in unknown order
    target (int): target summation
Outputs
    int: number of subsequences matching criteria
Notes
    - subsequences must be non-empty
    - sum is of the minimum and maximum element in subsequence
        + count the summation if it's less than or equal to the target
    - if the answer is too large return modulo 10^9 + 7

Examples

    Example 1:

        Input: nums = [3,5,6,7], target = 9
        Output: 4
        Explanation: There are 4 subsequences that satisfy the condition.
        [3] -> Min value + max value <= target (3 + 3 <= 9)
        [3,5] -> (3 + 5 <= 9)
        [3,5,6] -> (3 + 6 <= 9)
        [3,6] -> (3 + 6 <= 9)

    Example 2:

        Input: nums = [3,3,6,8], target = 10
        Output: 6
        Explanation: There are 6 subsequences that satisfy the condition. (nums can have repeated numbers).
        [3] , [3] , [3,3], [3,6] , [3,6] , [3,3,6]

    Example 3:

        Input: nums = [2,3,3,4,6,7], target = 12
        Output: 61
        Explanation: There are 63 non-empty subsequences, two of them don't satisfy the condition ([6,7], [7]).
        Number of valid subsequences (63 - 2 = 61).

    Example 4:

        Input: nums = [5,2,4,1,7,6,8], target = 16
        Output: 127
        Explanation: All non-empty subset satisfy the condition (2^7 - 1) = 127

Key Ideas
    - the number of possible subsequences is a function of the number of elements

        n_sequences = 2^(number of elements)

        this could also be computed using bit shifting

        1 << num of elements

    - since we only care about the smallest value and largest value in our summation
      all other values can somewhat be ignored

      sorting allows us to easily understand the min and max values in a particular
      subsequence

    - sorting the array does not modify the number of sequences
        or the elements in the subsequences, just the order

        Three element example

        original = [4, 1, 2]
        sorted   = [1, 2, 4]

        original subsequences
        4
        1
        2

        4, 1
        4, 2

        1, 2

        4, 1, 2

        sorted subsequences
        1
        2
        4

        1, 2
        1, 4
        2, 4

        1, 2, 4

        the subsequences are not in the same order but contain the same elements

        Four elements Example

        sorted = [1, 2, 3, 4]

        1
        2
        3
        4

        1, 2
        1, 3
        1, 4
        2, 3
        2, 4
        3, 4

        1, 2, 3
        1, 2, 4
        1, 3, 4
        2, 3, 4

        1, 2, 3, 4

        original = [3, 4, 1, 2]

        3
        4
        1
        2

        3, 4
        3, 1
        3, 2
        4, 1
        4, 2
        1, 2

        etc..

References:
    - https://leetcode.com/problems/number-of-subsequences-that-satisfy-the-given-sum-condition/discuss/713698/Python-Two-pointers-Complete-Explanation
    - https://www.onlinemath4all.com/number-of-subsets-of-a-set.html
"""

from typing import List


class Solution:

    def numSubseq(self, nums: List[int], target: int) -> int:
        """

        Notes
            - too slow
        """

        self.count: int = 0
        n: int = len(nums)

        def crawl(ix, cur_min: int = float('inf'), cur_max: int = float('-inf')):

            # print(f' val')

            # Update cur_min and max
            cur_min = min(cur_min, nums[ix])
            cur_max = max(cur_max, nums[ix])

            # Less than target
            if cur_min + cur_max <= target:
                self.count += 1

            # Crawl rest of subsequence
            for jx in range(ix + 1, n):
                crawl(jx, cur_min, cur_max)

        for ix in range(n):
            crawl(ix)

        return self.count

    def two_pointers(self, arr: List[int], target: int) -> int:
        """

        - sort the values
        - maintain two pointer at min and max value
            + if the elements are too big, move to smaller values
            + count the number of subsequences using
            the elements inbetween

        Time Complexity
            O(n*ln(n))
        Space Complexity
            O(1)
        """

        count: int = 0

        arr = sorted(arr)
        n: int = len(arr)
        mod = 10**9 + 7

        l: int = 0
        r: int = n - 1

        while l <= r:

            if arr[l] + arr[r] > target:
                r -= 1
            else:
                count += pow(2, r - l, mod)
                count = count % mod

                l += 1

        return count
