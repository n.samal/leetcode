"""
Inputs
    S (str): goal string
    words (List[str]): potential match strings
Outputs
    int: number of matches

Goal
    - can we match the current word with our goal word?
    - we can extend characters in the current word
        + character repetitions must be at least 3 or more to be valid
    
    - count how many words we can match with extension

Cases
    
    No words to match
        - nothing to match with
    Goal string smaller than word

Strategy

    Manual: Check Matches
        - create a function check_match
        - iterate through the words in the word list
        - if the words match then add to our match count

    Manual: Check Match

    - iterate character by character
    - if our characters match
        - move to the next character in both strings
    - if no match

        - see if we can use the previous character in our word instead
        - keep matching the goal string, until we hit a mismatch
        - if the character extension is too small, fail!
        - if it worked, continue the main search
    
    Character Tracking
        - for each word track the count of each character and the order we see them in
        - do the same character count for the pattern

        - compare the counts to determine if an extension is possible

            + if characters don't match then fail
            + if characters match

                * lengths extension too short => fail
                * lengths


"""

import itertools
from typing import List


class Solution:
    def expressiveWords(self, S: str, words: List[str]) -> int:
        """Search words for string extension"""
        count: int = 0
        self.m: int = len(S)

        print(f'pattern: {S}')

        for word in words:

            print(f'word: {word}')

            if self.match_string(word, S):
                count += 1
                print('-> match')

        return count

    def match_string(self, word: str, goal: str) -> bool:
        """Check if our word can match the goal

        # TODO: currently incorrect
        """
        n: int = len(word)

        ix: int = 0  # word pointer
        jx: int = 0  # goal pointer

        # Goal word is too small
        if self.m < n:
            return False

        # Characters left to check one of the strings
        while ix < n or jx < self.m:

            # Characters match!
            if ix < n and jx < self.m and word[ix] == goal[jx]:

                print(f' w: {word[ix]}  g: {goal[jx]}')

                ix += 1
                jx += 1

            # We have another character to check
            elif ix - 1 >= 0:

                extension = 1

                # Try an extension on our previous character
                while jx < self.m and word[ix - 1] == goal[jx]:

                    print(f'  w: {word[ix - 1]}  g: {goal[jx]}')

                    # Move to the next character in our goal
                    extension += 1
                    jx += 1

                # Not enough characters
                # TODO: this fails when previous 2 characters matched
                if extension < 3:
                    return False

                # Reset our extension for next time
                extension = 0

            # No other characters to check
            else:
                return False

        # We exited
        return True

    ''' Character Counts '''

    def expressiveWords2(self, S: str, words: List[str]) -> int:
        """Search words for string extension

        - using character counts method
        """
        count: int = 0

        # print(f' pattern: {S}')

        match_count = self.get_character_counts2(S)
        n: int = len(match_count)

        for word in words:

            # print(f' word: {word}')

            # Grab counts for current word
            word_count = self.get_character_counts2(word)
            m: int = len(word_count)

            ix: int = 0

            # Not enough unique characters
            if m != n:
                continue

            while ix < n:

                # Difference in character delta
                delta = match_count[ix][1] - word_count[ix][1]

                # Characters don't match
                if match_count[ix][0] != word_count[ix][0]:
                    break

                # Character group has at least 3 characters
                # or we have a perfect match
                elif delta == 0 or delta >= 2:
                    ix += 1

                # Character group isn't large enough
                # or we need to subtract characters
                elif match_count[ix][1] < 3 or delta < 0:
                    break
                else:
                    ix += 1

            # Made it to the end of both arrays
            if ix == n:
                # print(' -> match')
                count += 1
            # else:
                # print(f' -> failed on {word_count[ix]}')

        return count

    def expressiveWords3(self, S: str, words: List[str]) -> int:
        """Search words for string extension

        - using itertools character counts method
        - simplified loop logic

        Notes
            - not significantly faster
        """
        count: int = 0

        match_count = self.get_character_counts2(S)
        n: int = len(match_count)

        for word in words:

            # Grab counts for current word
            word_count = self.get_character_counts2(word)
            m: int = len(word_count)
            valid: bool = True

            # Not enough unique characters
            if m != n:
                continue

            for s, w in zip(match_count, word_count):

                # 1) Characters don't match
                # 2) need to subtract characters to match
                # 3) match requirement is less than 3 and we don't match perfectly
                if s[0] != w[0] or s[1] < w[1] or (s[1] < 3 and w[1] != s[1]):
                # if s[0] != w[0] or s[1] < w[1] or w[1] < s[1] < 3:
                    valid = False
                    break

            # Made it to the end
            if valid:
                count += 1

        return count

    def get_character_counts(self, s: str) -> List[List]:
        """Grab the order characters counts """

        # Track the consecutie count of each character
        arr = []

        if not s:
            return arr

        # Add the first character
        arr.append([s[0], 1])

        for ix, char in enumerate(s[1:]):

            # Compare against previous character
            # Increment the count
            if char == arr[-1][0]:
                arr[-1][1] += 1

            # New character encountered, store it
            else:
                arr.append([char, 1])

        return arr

    def get_character_counts2(self, s: str) -> List[List]:
        """Grab the consecutive characters counts

        - this is faster than my method
         """

        # return [(k, list(v)) for k, v in itertools.groupby(s)]
        return [(k, len(list(v))) for k, v in itertools.groupby(s)]


if __name__ == "__main__":

    # Base case
    # S = "heeellooo"
    # words = ["hello", "hi", "helo"]

    # Expected: 3
    S = "dddiiiinnssssssoooo"
    words = ["dinnssoo","ddinso","ddiinnso","ddiinnssoo","ddiinso","dinsoo","ddiinsso","dinssoo","dinso"]

    obj = Solution()
    print(obj.expressiveWords(S, words))
    # print(obj.expressiveWords2(S, words))

    # https://leetcode.com/problems/expressive-words/discuss/122660/C%2B%2BJavaPython-2-Pointers-and-4-pointers
