"""
Inputs
    str1 (str):
    str2 (str):
Outputs
    str: largest common divisor
Notes
    - string 't' can divide string 's'
    if we can make all of 's' with 't'

        + s = t + ... + t
    - find the largest string that can divide both strings

Examples

    Example 1:

        Input: str1 = "ABCABC", str2 = "ABC"
        Output: "ABC"

    Example 2:

        Input: str1 = "ABABAB", str2 = "ABAB"
        Output: "AB"

    Example 3:

        Input: str1 = "LEET", str2 = "CODE"
        Output: ""

    Example 4:

        Input: str1 = "ABCDEF", str2 = "ABC"
        Output: ""

Ideas
    
    - the string must make up all of both strings
    - if there are no common letters than False

    - see if we can create the larger string with the shorter string

        + 'ABABAB' with 'AB'

        + if no match at all, then GCD = 0
        + if we have a match, then we need to check that rest of
        string can be made up with the substring 'AB'

        'ABABAB' => 'AB' match found
        '  ABAB' => 'AB' match found
        '    AB' => 'AB' match found

    - we need to track which string is the substring

Hand Calc

    str1 = "ABABAB", str2 = "ABAB"

    "ABAB" is in string1, we still need to find the remainig "AB"

    str1 = 'AB'  str2 = 'ABAB'

    search for 'AB' in string2, we found it
    search for remaining

    str1 = 'AB'  str2 = 'AB'

    the strings match, so return either string
"""


class Solution:
    def gcdOfStrings(self, str1: str, str2: str) -> str:
        """Find the greatest common divisor

        Time Complexity
            O(m)

            m = max(len(str1), len(str2))
        Space Complexity
            O() size of recursion stack
        """
        
        # Make one of them the substring
        # Make string 2 the substring
        if len(str1) < len(str2):
            str1, str2 = str2, str1
        
        n1: int = len(str1)
        n2: int = len(str2)
        
        # Check if they match        
        if n1 == n2:
            if str1 == str2:
                return str1
            else:
                return ''
        
        else:
        
            # Does string 2 occur in the first portion of string1?
            # if so try matching the rest of the string
            if str1[:n2] == str2:
                return self.gcdOfStrings(str1[n2:], str2)                 
            else:
                return ''
