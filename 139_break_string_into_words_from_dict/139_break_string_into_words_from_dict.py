"""
Inputs:
    s (str): string to break into pieces
    wordDict (List[str]): dictionary of words
Outputs:
    bool: can the word be split
    
Goal
    - determine if s can be segmented into a space-separated sequence of one or more dictionary words.

Notes
    - The same word in the dictionary may be reused multiple times in the segmentation.
    - You may assume the dictionary does not contain duplicate words.

Examples

    Example 1:

        Input: s = "leetcode", wordDict = ["leet", "code"]
        Output: true
        Explanation: Return true because "leetcode" can be segmented as "leet code".

    Example 2:

            Input: s = "applepenapple", wordDict = ["apple", "pen"]
            Output: true
            Explanation: Return true because "applepenapple" can be segmented as "apple pen apple".
                         Note that you are allowed to reuse a dictionary word.
        Example 3:

            Input: s = "catsandog", wordDict = ["cats", "dog", "sand", "and", "cat"]
            Output: false

Strategies
    
    Top Down
    
        - try splitting at each character.
        - if this substring is in our dictionary, see if the remaining words
        can be broke into dictionary pieces
        - keep crawling until we've used all characters
            - if we're found all characters then this is complete
        
        Time Complexity
            O(n^n)
        Space Complexity
            O(n)
        
        Notes
            - is too slow, add memoization for potential speed up

    Top Down with memo

        - Add memoization to previous appraoch
        - only compute new values

        leetcode
        
        initial split
        l|eetcode
          e|etcode
          ee|tcode
          eet|code
          eetc|ode
          eetco|de
          eetcod|e
          eetcode|

          and their repsective subsplits

        On second character
        le|etcode
            we've already tried this split, and know it the answer
        lee|tcode
            we've already tried this split, and know it the answer => False
        leet|code
            we've already tried this split, and know it the answer => True

        Time Complexity
            O(n^2)  For every starting index, the search can go until the end of the string.
        Space Complexity
            O(n)
"""

class Solution:

    ''' Recursion with Memoization '''

    def wordBreak(self, s: str, wordDict: List[str]) -> bool:
    
        # Set for easy checks
        self.words = set(wordDict)
        self.s = s
        self.n = len(s)
        
        # print(f'words: {self.words}')
        
        return self.top_down()
        
    def top_down(self, start_ix: int = 0):
        """
        Args:
            start_ix (int):
        Returns:
            bool: can the string be broken into words from
            our word dictionary
        Time Complexity:
            O(n^n)  more branches at each character
        Space Complexity:
            O(n)    set by recursion stack
        """
        
        # Base case: completion
        if start_ix == self.n:
            return True
        
        # Try all substrings from here
        # 0 -> 1, 0 -> 2, 0 -> 3
        for end_ix in range(start_ix + 1, self.n + 1):
            
            # print(f'word: {self.s[start_ix:end_ix]}')
            
            # Check if current substring in the word dict
            # And remaining word breaks return true
            if self.s[start_ix:end_ix] in self.words and self.top_down_memo(end_ix):
                # print(f'word: {self.s[start_ix:end_ix]}')
                return True
        
        # Made it to the end with no working solution
        return False
        
        

    ''' Recursion with memoization '''

    def wordBreak_memo(self, s: str, wordDict: List[str]) -> bool:
    
        # Set for easy checks
        self.words = set(wordDict)
        self.s = s
        self.n = len(s)
        self.cache = {}
        
        # print(f'words: {self.words}')
        
        return self.top_down()
        
    def top_down_memo(self, start_ix: int = 0):
        """
        Args:
            start_ix (int):
        Returns:
            bool: can the string be broken into words from
            our word dictionary
        Time Complexity:
            O(n^2)  each of the n prefixes can be split n times
        Space Complexity:
            O(n)    set by recursion stack
        """
        
        # Base case: completion
        if start_ix == self.n:
            return True
        
        # Use previously computed values
        elif start_ix in self.cache:
            return self.cache[start_ix]
        
        # Try all substrings from here
        # 0 -> 1, 0 -> 2, 0 -> 3
        for end_ix in range(start_ix + 1, self.n + 1):
            
            # print(f'word: {self.s[start_ix:end_ix]}')
            
            # Check if current substring in the word dict
            # And remaining word breaks return true
            if self.s[start_ix:end_ix] in self.words and self.top_down_memo(end_ix):
                # print(f'word: {self.s[start_ix:end_ix]}')
                self.cache[start_ix] = True
                return True
        
        # Made it to the end with no working solution
        self.cache[start_ix] = False
        return False
        
        