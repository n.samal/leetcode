from typing import List
import bisect

# Example

# Initial idea is to use binary search to find location of element
# Questions. do we have all values in the range?
# if: [1, 2, 3, 4, 4, 5]
# is searching for 4, is 3 gauranteed to exist?
# is 5 guaranteed to exist?

# Idea:
# 1) binary search, then crawl up and down to find min and max element

# Idea 2:
# binary search to find element, use binary search to explore remaining bounds


""" Strategy 1 """


def binary_search(arr: List[int], low: int, high: int, target: int) -> int:
    """Search for a target within the given range

    Args:
        nums (List[int]): sorted list of integers
        low (int): lower bound of search (inclusive)
        high (int): upper bound of search (exclusive)
        target (int): value to search for
    Returns:
        int: location of where the target is found
    """

    # Indice where target was found
    found: int = None

    if low < 0:
        return None
    if high > len(arr):
        return None

    while low < high:

        # Compute mid point
        mid = low + (high - low) // 2  # Avoid overflow

        # Found the target
        if arr[mid] == target:
            return mid

        # Value is smaller
        if target < arr[mid]:
            high = mid

        # Value is larger
        else:
            low = mid + 1

    return found


def binary_search_tres(nums: List[int], target: int) -> List[int]:
    """Exhaustively search the array for the target

    Time Complexity
        O(log(n))
    Space Complexity
        O(1)

    Returns:
        nums (List[int]): sorted list of integers
        target (int): value to search for
    Args:
        List[int]: indices of target value
    """

    n: int = len(nums)

    # Search for target
    ix = binary_search(nums, low=0, high=n, target=target)

    # Unable to find target
    if ix is None:
        return [-1, -1]

    else:

        start = ix
        stop = ix

        # Search lower bounds for target
        ix_low = binary_search(nums, low=0, high=ix, target=target)

        # Continue search
        while ix_low is not None:

            start = min(ix_low, start)
            ix_low = binary_search(nums, low=0, high=ix_low, target=target)

        # Search upper bounds for target
        ix_high = binary_search(nums, low=ix + 1, high=n, target=target)

        # Continue search
        while ix_high is not None:
            stop = max(ix_high, stop)
            ix_high = binary_search(nums, low=ix_high + 1, high=n, target=target)

        return [start, stop]


""" Strategy 2 """


def binary_search_and_walk(nums: List[int], target: int) -> List[int]:
    """Find the first and last indices of the target value in a sorted array

    Find the target using binary search
    Then crawl up and down the array while we still have the target value

    Time Complexity
        O(log(n) + k)
    Space Complexity
        O(1)

    Returns:
        nums (List[int]): sorted list of integers
        target (int): value to search for
    Args:
        List[int]: indices of target value
    """

    start: int = -1
    stop: int = -1
    found: bool = False

    # Indices of search band
    low: int = 0
    high: int = len(nums)

    while low < high:

        # Compute mid point
        mid = low + (high - low) // 2  # Avoid overflow

        # Found the target
        if nums[mid] == target:

            # Save the location
            start = mid
            stop = mid

            found = True

            break

        # Value is smaller
        if target < nums[mid]:
            high = mid

        # Value is larger
        else:
            low = mid + 1

    if found:

        # Start from where target was found
        ix = mid + 1

        # Crawl up from target
        while ix < high and nums[ix] == target:

            # Update max index location
            stop = ix

            # See if there's more values
            ix += 1

        # Start from where target was found
        ix = mid - 1

        # Crawl down from target
        while ix >= low and nums[ix] == target:

            # Update min index location
            start = ix

            # See if there's more values
            ix -= 1

    else:
        start = -1
        stop = -1

    return [start, stop]


if __name__ == "__main__":

    # print(binary_search_and_walk([5, 7, 7, 8, 8, 10], 8))
    # print(binary_search_and_walk([5, 7, 7, 8, 8, 10], 6))
    # print(binary_search_and_walk([2, 2], 2))
    print(binary_search_and_walk([1, 3], 1))

    # print(binary_search_tres([5, 7, 7, 8, 8, 10], 8))
    # print(binary_search_tres([5, 7, 7, 8, 8, 10], 6))
    # print(binary_search_tres([7, 7, 7, 7, 7, 7], 7))
    # print(binary_search_tres([2, 2], 2))
    # print(binary_search_tres([2], 2))
