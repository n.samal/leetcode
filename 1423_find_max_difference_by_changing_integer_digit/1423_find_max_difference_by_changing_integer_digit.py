"""
Inputs
    num (int)
Outputs
    int: max difference possible
Examples

    Example 1:

        Input: num = 555
        Output: 888
        Explanation: The first time pick x = 5 and y = 9 and store the new integer in a.
        The second time pick x = 5 and y = 1 and store the new integer in b.
        We have now a = 999 and b = 111 and max difference = 888
    
    Example 2:

        Input: num = 9
        Output: 8
        Explanation: The first time pick x = 9 and y = 9 and store the new integer in a.
        The second time pick x = 9 and y = 1 and store the new integer in b.
        We have now a = 9 and b = 1 and max difference = 8
    
    Example 3:

        Input: num = 123456
        Output: 820000
    
    Example 4:

        Input: num = 10000
        Output: 80000
    
    Example 5:

        Input: num = 9288
        Output: 8700

Notes
    - we can choose any new number between 0 <= x <= 9 for our new numbers
    - cannot have leading zeros in the new number

Ideas
    - seems like we want to modify the most significant digit if possible

        + for maximizing, move the left most digit to 9 if not already 9
            + then modify the remaining values
        + for minimizing, move the left most digit to 1 if not already 1
            + if not move to the next most significant digit

            + if the second digit is the same as the first digit, we can't convert
            to zero because of the leading zeros
            + if the digit is already a zero, no point in changing that
            try the next value

    - conversion to an array or string may help with iterating through number
    more easily
"""

class Solution:
    def maxDiff(self, num: int) -> int:

        num = str(num)
        n: int = len(num)

        if n == 1:
            return 9 - 1

        ''' Maximize number '''
        num_max = num[:]

        # Find the first digit that's not 9
        ix: int = 0
        while ix < n and num[ix] == '9':
            ix += 1

        if ix < n:
            num_max = num_max.replace(num[ix], '9')

        ''' Minimize number '''
        num_min = num[:]

        # Minimize first digit to 1 if possible
        if num[0] != '1':
            num_min = num_min.replace(num[0], '1')

        # Minimize second most significant digit when not equal to the first character
        # ie: 111111 (can't convert the second 1 to zeros)
        else:

            # Find the first digit that's not the leading digit
            # or equal to zero
            ix: int = 1
            while ix < n and (num[ix] == num[0] or num[ix] == '0'):
                ix += 1

            if ix < n:
                num_min = num_min.replace(num[ix], '0')

        # print(f' max: {num_max}   min: {num_min}')

        return int(num_max) - int(num_min)
