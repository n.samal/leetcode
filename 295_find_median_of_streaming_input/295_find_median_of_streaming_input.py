"""

Goal
    Find the mean of input number stream

Inputs
    number (int): number to add

Output
    float: median value of current population

Cases

    Null: []
    All Same: 1, 1, 1, 1, 1
    Hybrid: [1, 1, 2, 2, 4, 4, 999]

Notes

    - Median Calculation

        + if odd take the middle value

        [0, 1, 2, 3, 4] has 5 elements total

        n = 5
        middle value = (n - 1) / 2 = (5-1)/2 = 2

        + if even number of values, average the two middle values


        [0, 1, |2, 3| 4, 5] has 6 elements total

        n = 6
        median value = average of middle values
        middle value
             first : (n / 2) => (6/2) = 3
             second: (n / 2) - 1 => (6/2) - 1 = 2
Strategy

    Insertion Sort

        - we want to maintain a sorted list
        - find the insertion point

            + using binary search O(log(n))
            + using loop O(n)

        - insert the value

            - we may need to slide over all the values O(n)

        Time Complexity
            O(n^2)
        Space Complexity
            O(n)

    Two Heaps

        - we need to maintain a two heaps and try to keep them balanced in size

            + left: lower half
            + right: upper half

        - as we add values on side may need to be a little unbalanced

            + let's keep the unbalanced side consistent
            + use the right side for this

        Balanced Example

            values: [1, 1, 2] [3, 4, 5]

                left_max: 2
                right_min = 3

            - if we need to insert value: 0 we should get a full array like so

            values: [0, 1, 1, 2 | 3, 4, 5]

            - insert on the right when value >= right_min
            - insert on the left when value < left_max

                + then rebalance so right side is 'larger' side
                + move one value to the right

            values: [0, 1, 1] [2, 3, 4, 5]

        Unbalanced Example

            values: [0, 1, 1] [2, 3, 4, 5]

            - for a new value

            - insert on the left if value <= left_max (trying to rebalance)
            - insert to right otherwise

                + grab the new right_min and move that to the left
"""

# Deque is much slower implmentation
# from collections import deque
# import itertools
import heapq


class MedianFinder:

    def __init__(self):
        """"""

        # Initialize empty array
        # self.arr = deque([])
        self.arr = []

    def binary_search(self, target, low, high) -> int:
        """Find the insertion index for our target value"""

        # Binary search
        while low <= high:

            mid = (low + high) // 2
            # print(f' low: {self.arr[low]}   high: {self.arr[high]}   mid: {arr[mid]}   target: {target}')

            # Matching target (place value after first)
            if target == self.arr[mid]:
                return mid

            # Value is smaller than midpoint
            elif target < self.arr[mid]:
                high = mid - 1

            # Value is larger than midpoint
            else:
                low = mid + 1

        return low

    def addNum(self, num: int) -> None:
        """Add the number to the array"""

        # Find insertion index
        n = len(self.arr)
        ix = self.binary_search(num, 0, n - 1)

        # print(f' ix: {ix}')

        # We should have converged on a location
        # Must use slicing function for deque
        # left = list(itertools.islice(self.arr, 0, ix, 1))
        # right = list(itertools.islice(self.arr, ix, n, 1))
        
        # left = list(itertools.islice(self.arr, 0, ix, 1))
        # right = list(itertools.islice(self.arr, ix, n, 1))

        # print(f' left: {left}')
        # print(f' right: {right}')

        # Insert the value at the calculated index, and push everything to the right
        self.arr = self.arr[:ix] + [num] + self.arr[ix:]

        # print(self.arr)

    def findMedian(self) -> float:
        """Compute the median value"""

        # Count the length of the array
        n = len(self.arr)

        # Determine if even or odd number of elements

        # Even element count
        # Grab middle two elements
        # [0, 1, *2*, *3*, 4, 5]
        if n % 2 == 0:
            ix_mid1 = n // 2
            ix_mid2 = ix_mid1 - 1

            median = (self.arr[ix_mid1] + self.arr[ix_mid2]) / 2.0

        # Odd element count
        # Grab the middle value
        # [0, 1, 2, *3*, 4, 5, 6]: 6/2 = 3
        # [0, 1, *2*, 3, 4]: 4/2 = 2
        # [0, *1*, 2]: 2/2 = 1
        else:
            ix_mid = (n - 1) // 2

            median = self.arr[ix_mid]

        return median


class MedianFinderHeaps:

    def __init__(self):
        """
        initialize your data structure here.

        Notes:
            - python uses min heaps
            - we'll multiply values by -1 for max heap
        """

        # Initialize empty array
        self.left = []  # max heap
        self.right = []  # min heap

        # heapq.heapify(self.right)

    def isBalanced(self) -> bool:
        """Are the heaps balanced"""
        return len(self.left) == len(self.right)

    def addNum(self, num: int) -> None:
        """Add the number to the array

        Reference:
            https://docs.python.org/3/library/heapq.html
        """

        # No values on any side yet
        if not self.right:
            heapq.heappush(self.right, num)
            return

        # No values on left side
        if not self.left:

            right = min(self.right)

            if num <= right:
                heapq.heappush(self.left, -num)

            # Add value to right, and rebalance
            else:
                extra = heapq.heappushpop(self.right, num)
                heapq.heappush(self.left, -extra)

            return

        left_max = min(self.left) * -1
        right_min = min(self.right)

        # If balanced (even number of elements)
        if self.isBalanced() is True:

            # Push to right side (right will be imbalanced)
            if num >= right_min:
                heapq.heappush(self.right, num)
            else:

                # Add value to left, and pop max value
                extra = -heapq.heappushpop(self.left, -num)

                # Push new value to right side (imbalanced side)
                heapq.heappush(self.right, extra)

        # Odd number of elements (right side has more elements)
        else:

            # Push to left side to balance
            if num <= left_max:
                heapq.heappush(self.left, -num)
            else:

                # Add value to right, and grab min value
                extra = heapq.heappushpop(self.right, num)

                # Push new value to right side (imbalanced side)
                heapq.heappush(self.left, -extra)

    def findMedian(self) -> float:
        """Compute the median value"""

        # If balanced (even number of elements)
        if self.isBalanced() is True:

            left_max = min(self.left) * -1
            right_min = min(self.right)

            return (left_max + right_min) / 2.0

        # Odd number of elements
        else:
            # Grab the small value from imbalanced side
            return min(self.right)


# Your MedianFinder object will be instantiated and called as such:

obj = MedianFinder()
obj2 = MedianFinderHeaps()
# arr = [6, 10, 2, 6, 5, 0, 6, 3, 1, 0, 0]
arr = [-1, -2, -3, -4, -5]

for val in arr:
    obj.addNum(val)
    obj2.addNum(val)

    print('  ', obj2.left, obj2.right)

    m1 = obj.findMedian()
    m2 = obj2.findMedian()

    if m1 != m2:
        print(f'{m1} != {m2}')
